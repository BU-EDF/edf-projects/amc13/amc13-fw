# Non-project mode
# Generate bistream files

# set the variant to build, default is HCAL
set top AMC13_T1_
if {$argc > 0 } {
    regexp {build/AMC13_T1_(.*)/} [lindex $argv 0] match type
    append top $type
} else {
    append top HCAL 
}

set outputDir ./$top
#
# STEP#5: generate a bitstream and mcs file
#
read_checkpoint $outputDir/post_route.dcp
link_design -top AMC13_T1 -part xc7k325tffg900-2
write_bitstream -force ../bit/$top.bit
write_cfgmem -format mcs -size 16 -loadbit "up 0x0 ../bit/$top.bit" -file ../mcs/$top.mcs -force
