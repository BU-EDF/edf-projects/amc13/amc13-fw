# Non-project mode
# run placement

# set the variant to build, default is HCAL
set top AMC13_T1_
if {$argc > 0 } {
    regexp {build/AMC13_T1_(.*)/} [lindex $argv 0] match type
    append top $type
} else {
    append top HCAL 
}


set outputDir ./$top

#
# STEP#3: run placement and logic optimization, report utilization and timing
#estimates, write checkpoint design
#
read_checkpoint $outputDir/post_synth.dcp
link_design -top AMC13_T1 -part xc7k325tffg900-2
opt_design
power_opt_design
place_design
phys_opt_design
write_checkpoint -force $outputDir/post_place
report_timing_summary -file $outputDir/post_place_timing_summary.rpt
