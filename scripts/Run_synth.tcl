# Non-project mode
# collect files
# run synthesis


# set the variant to build, default is HCAL
set top AMC13_T1_
if {$argc > 0 } {
    append top [lindex $argv 0] 
} else {
    append top HCAL 
}
#set the file added for the top level vhdl file

set top_file ../src/top/AMC13_T1.vhd
#set the file added for the primary timing constraints
set top_xdc_file ../src/top/
append top_xdc_file $top .xdc
#set the file added for versioning of the variant
set top_file_version ../src/top/
append top_file_version $top _version.vhd

puts "Setting top level to $top"

#recursive glob http://wiki.tcl.tk/1474
proc glob-r {{dir .} args} {
    set res {}
    foreach i [lsort [glob -nocomplain -dir $dir *]] {
	if {[file isdirectory $i]} {
	    eval [list lappend res] [eval [linsert $args 0 glob-r $i]]
	} else {
	    if {[llength $args]} {
		foreach arg $args {
		    if {[string match $arg $i]} {
			lappend res $i
			break
		    }
		}
	    } else {
		lappend res $i
	    }
	}
    }
    return $res
} ;# JH


#
# STEP#0: define output directory area.
#
set outputDir ./$top
file mkdir $outputDir

#
# STEP#0.6: build project stuff for Mr. Wu
set projectDir ../proj/$top
file mkdir $projectDir
if {[file isfile $projectDir/$top.xpr]} {
    puts "Re-creating project file."
} else {
    puts "Creating project file."
}
create_project -force -part xc7k325tffg900-2 $top $projectDir

#
# STEP#1: setup design sources and constraints
#
#add top file
read_vhdl $top_file
read_vhdl $top_file_version
#read common directories for vhd and verilog files
#read_vhdl [ glob-r ../src/common *.vhd ]
#read_verilog [ glob-r ../src/common *.v ]

#read user specified locations for vhd and verilog files
for {set i 1} {$i < $argc} {incr i} {
    set new_folder [lindex $argv $i]
    puts "Adding files from directory: $new_folder "
    
    set vhdl_list [ glob-r $new_folder *.vhd ]
    for {set j 0} {$j < [llength $vhdl_list ] } {incr j} {
	read_vhdl [lindex $vhdl_list $j]
	puts [lindex $vhdl_list $j]
    }
    
    set verilog_list [ glob-r $new_folder *.v ]
    for {set j 0} {$j < [llength $verilog_list ] } {incr j} {
	read_verilog [lindex $verilog_list $j]
	puts [lindex $verilog_list $j]
    }

    
    set dcp_list [ glob-r $new_folder *.dcp ]
    for {set j 0} {$j < [llength $dcp_list ] } {incr j} {
	read_checkpoint [lindex $dcp_list $j]
	puts [lindex $dcp_list $j]
    }
    
    set ngc_list [ glob-r $new_folder *.ngc ]
    for {set j 0} {$j < [llength $ngc_list ] } {incr j} {
	read_edif [lindex $ngc_list $j]
	puts [lindex $ngc_list $j]
    }

    set xdc_list [ glob-r $new_folder *.xdc ]
    for {set j 0} {$j < [llength $xdc_list ] } {incr j} {
	read_xdc [lindex $xdc_list $j]
	puts [lindex $xdc_list $j]
    }

    set xci_list [ glob-r $new_folder *.xci ]
    for {set j 0} {$j < [llength $xci_list ] } {incr j} {
	read_ip [lindex $xci_list $j]
	puts [lindex $xci_list $j]
    }

#
#    set new_folder [lindex $argv $i]
#    set vhdl_list [ glob-r $new_folder *.vhd ]
#    set verilog_list [ glob-r $new_folder *.v ]
#    if { [ llength $vhdl_list] > 0 } { 
#	read_vhdl $vhdl_list
#    }
#    if { [ llength $verilog_list] > 0} {
#	read_verilog $verilog_list
#    }
#    puts "Adding files from directory: $new_folder "
}

#add ISE generated IP cores (ngc files)
#read_edif [ glob-r ../src *.ngc ]

#add constraint files
read_xdc $top_xdc_file
read_xdc [glob ../src/common/DDR/ddr*.xdc]

#
# STEP#2: run synthesis, report utilization and timing estimates, write checkpoint design
#
synth_design -top AMC13_T1 -part xc7k325tffg900-2 -flatten rebuilt
write_checkpoint -force $outputDir/post_synth
report_timing_summary -file $outputDir/post_synth_timing_summary.rpt
report_power -file $outputDir/post_synth_power.rpt
