
VIVADO_FLAGS=-notrace -mode batch

MCS_TCL=scripts/Run_mcs.tcl
ROUTE_TCL=scripts/Run_route.tcl
PLACEMENT_TCL=scripts/Run_placement.tcl
SYNTHESIS_TCL=scripts/Run_synth.tcl

COMMON_PATH=../src/common
HCAL_TRIG_PATH=../src/HCAL_trig
CMS_DAQ_PATH=../src/CMS_DAQ_if
CMS_DAQ_COMMON_PATH=$(CMS_DAQ_PATH)/common
CMS_DAQ_10Gb_PATH=$(CMS_DAQ_PATH)/10Gb
CMS_DAQ_5Gb_PATH=$(CMS_DAQ_PATH)/5Gb
TCPIP_PATH=../src/TCPIP_if
COMMON_10G_PATH=../src/common_10G
#AMC_DAQ_LINK_PATH=../src/AMC_DAQ_LINK

COMMON_FILES=$(shell find ./src/$(COMMON_PATH) | grep -e "\.v\|\.ngc\|\.xdc")

HCAL_TOP=./src/top/AMC13_T1_HCAL*
HCAL_10G_TOP=./src/top/AMC13_T1_HCAL10G*
CMS_TOP=./src/top/AMC13_T1_CMS*
CMS_10G_TOP=./src/top/AMC13_T1_CMS10G*
G2_TOP=./src/top/AMC13_T1_g2*

HCAL_TRIG_FILES=$(shell find ./src/$(HCAL_TRIG_PATH) | grep -e "\.v\|\.ngc\|\.xdc") 
CMS_DAQ_COMMON_FILES=$(shell find ./src/$(CMS_DAQ_COMMON_PATH) | grep -e "\.v\|\.ngc\|\.xdc")
CMS_DAQ_5Gb_FILES=$(shell find ./src/$(CMS_DAQ_5Gb_PATH) | grep -e "\.v\|\.ngc\|\.xdc")
CMS_DAQ_10Gb_FILES=$(shell find ./src/$(CMS_DAQ_10Gb_PATH) | grep -e "\.v\|\.ngc\|\.xdc")
TCPIP_FILES=$(shell find ./src/$(TCPIP_PATH) | grep -e "\.v\|\.ngc\|\.xdc")
COMMON_10G_FILES=$(shell find ./src/$(COMMON_10G_PATH) | grep -e "\.v\|\.ngc\|\.xdc")

.SECONDARY:

.PHONY: clean HCAL HCAL10G CMS CMS10G g2 doc

#all: doc HCAL10G CMS10G g2 #fakeAMC 
all: HCAL10G CMS10G g2

#HCAL : mcs/AMC13_T1_HCAL.mcs 
HCAL10G : mcs/AMC13_T1_HCAL10G.mcs
#CMS : mcs/AMC13_T1_CMS.mcs
CMS10G : mcs/AMC13_T1_CMS10G.mcs
g2 : mcs/AMC13_T1_g2.mcs 
#fakeAMC : bin/AMC13_T1_fakeAMC.mcs

clean:
	@echo "Cleaning up"
	@rm -fr ./build  2> /dev/null 
	@rm -f ./bit/* ./mcs/* 2> /dev/null
#	@svn --force rm ./doc/html > /dev/null 2>&1
	@rm -fr ./doc/html 2> /dev/null


doc :  $(COMMON_FILES) $(HCAL_TRIG_FILES) $(CMS_DAQ_COMMON_FILES) $(COMMON_10G_FILES) $(CMS_DAQ_10Gb_FILES)  $(HCAL_10G_TOP) $(CMS_10G_TOP)  $(TCP_IP_FILES) $(G2_TOP)
	@echo "Generating doxygen"
	@rm -fr ./doc/html 2> /dev/null
	@cd doc && ../tools/doxygen-patched ./AMC13_doc.cfg  > /dev/null 2>&1
#	svn --force add ./doc/html  > /dev/null 2>&1

#specific rules for the post_synth step to add in version specific files
#build/AMC13_T1_HCAL/post_synth.dcp : $(COMMON_FILES) $(HCAL_TRIG_FILES) $(CMS_DAQ_COMMON_FILES) $(CMS_DAQ_5Gb_FILES) $(HCAL_TOP)
#	mkdir -p build && cd build && vivado $(VIVADO_FLAGS)  -source ../$(SYNTHESIS_TCL) -tclargs HCAL $(COMMON_PATH) $(HCAL_TRIG_PATH) $(CMS_DAQ_COMMON_PATH) $(CMS_DAQ_5Gb_PATH)

build/AMC13_T1_HCAL10G/post_synth.dcp : $(COMMON_FILES) $(HCAL_TRIG_FILES) $(CMS_DAQ_COMMON_FILES) $(COMMON_10G_FILES) $(CMS_DAQ_10Gb_FILES)  $(HCAL_10G_TOP)
	mkdir -p build && cd build && vivado $(VIVADO_FLAGS)  -source ../$(SYNTHESIS_TCL) -tclargs HCAL10G $(COMMON_PATH) $(HCAL_TRIG_PATH) $(COMMON_10G_PATH) $(CMS_DAQ_COMMON_PATH) $(CMS_DAQ_10Gb_PATH)

#build/AMC13_T1_CMS/post_synth.dcp : $(COMMON_FILES)  $(CMS_DAQ_COMMON_FILES) $(CMS_DAQ_5Gb_FILES) $(CMS_TOP)
#	mkdir -p build && cd build && vivado  $(VIVADO_FLAGS) -source ../$(SYNTHESIS_TCL) -tclargs CMS $(COMMON_PATH) $(CMS_DAQ_COMMON_PATH) $(CMS_DAQ_5Gb_PATH)

build/AMC13_T1_CMS10G/post_synth.dcp : $(COMMON_FILES) $(CMS_DAQ_COMMON_FILES) $(COMMON_10G_FILES) $(CMS_DAQ_10Gb_FILES)  $(CMS_10G_TOP)
	mkdir -p build && cd build && vivado $(VIVADO_FLAGS)  -source ../$(SYNTHESIS_TCL) -tclargs CMS10G $(COMMON_PATH) $(COMMON_10G_PATH) $(CMS_DAQ_COMMON_PATH) $(CMS_DAQ_10Gb_PATH)

build/AMC13_T1_g2/post_synth.dcp : $(COMMON_FILES) $(COMMON_10G_FILES) $(TCP_IP_FILES) $(G2_TOP) 
	mkdir -p build && cd build && vivado $(VIVADO_FLAGS) -source ../$(SYNTHESIS_TCL) -tclargs g2 $(COMMON_PATH) $(COMMON_10G_PATH) $(TCPIP_PATH)

#build/AMC13_T1_fakeAMC/post_synth.dcp : $(COMMON_FILES) $(AMC_DAQ_LINK_FILES)
#	mkdir -p build && cd build && vivado -mode batch -source ../$(SYNTHESIS_TCL) -tclargs fakeAMC $(AMC_DAQ_LINK_PATH)


#rules for the post synth steps.  These are shared between all the steps
build/AMC13_T1_%/post_place.dcp : build/AMC13_T1_%/post_synth.dcp  $(PLACEMENT_TCL)
	cd build && vivado $(VIVADO_FLAGS) -source ../$(PLACEMENT_TCL) -tclargs $<

build/AMC13_T1_%/post_route.dcp : build/AMC13_T1_%/post_place.dcp  $(ROUTE_TCL)
	cd build && vivado $(VIVADO_FLAGS) -source ../$(ROUTE_TCL) -tclargs $<

mcs/AMC13_T1_%.mcs : build/AMC13_T1_%/post_route.dcp  $(MCS_TCL)
	cd build && vivado $(VIVADO_FLAGS) -source ../$(MCS_TCL) -tclargs $<
