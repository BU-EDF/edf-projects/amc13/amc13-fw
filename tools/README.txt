2016-05-03
Basic instructions for a user on volta.bu.edu
- Check out/update the directory above trunk so you get trunk, tags, and tools: http://gauss.bu.edu/svn/amc13-firmware
- Mr. Wu should have just committed his new version to trunk, so go into trunk and follow the README for the build instructions.
  You should now have sucessfully generated the HCAL10G,CMS10G, and g-2 bit and mcs files.  The doxygen should have also been regenerated (with svn add/removes)
- Now commit the newly generated files, but not the project files, using the following: svn commit Makefile bit mcs doc --message="committing to trunk for tag v008"
- Now you can make a tag of trunk.
  If you are anywhere in the checked out svn, you can do: svn cp ^/trunk ^/tags/v008 --message="Tag for firmwares 0x6050 and 0x2254"
  Elsewhere you'll have to use the full svn paths.  The commit message must always start with "Tag for firmwares " followed by the firmware numbers in hex that Mr. Wu released.
  You might not always have a release of each type of firmware.
- Now update your copy of the tags folder, which will pull your new tag, and go into the root path of that tag (ex. v008/).
- Now run the following script that is in the tools directory to push and rename the bit,mcs, and release notes into my (dgastler) public_html folder (ohm.bu.edu/~dgastler/CMS/AMC13-Firmware/)
  command: ../../tools/push_files.py "HCAL10G/0x6050" "CMS10G/0x2254"
  The arguments to this commmand are the fw flavor (if you make a mistake it will tell you the valid flavors) and the fw number for that flavor.
  This is designed for me (dgastler) to work on my machine (volta.bu.edu), so you may have to hack the python script a bit. 


Other notes:
  A patched version of doxygen is included in the tools folder that works on volta.bu.edu.  It probably won't work on other machinee, but I will add the source to tools in the future. 
