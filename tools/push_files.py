#!/usr/bin/env python

import os.path #isfile()
import paramiko #scp
import argparse

fw_types =["HCAL10G","HCAL","CMS10G","CMS","g2"]

class BadFile(Exception):
    def __init__(self, filename):
        self.value = "Missing file: %s\n", filename
    def __str__(self):
        return repr(self.value)

class BadTypeString(Exception):
    def __init__(self, value):
        self.value = "Bad fw type/version %s\n Version must be in hex, type must be from %s\n", value,fw_types
    def __str__(self):
        return repr(self.value)

    

def scp_files(fw_type,fw_version,T2_fw_version,dest_serv,dest_user,dest_path):
    #create filenames
#    source_bit_filename=  os.path.expanduser("./bit/AMC13_T1_"   + fw_type + ".bit")
    source_bit_filename=  os.path.expanduser("/home/dan/work/CMS/amc13-firmware/trunk/bit/AMC13_T1_"   + fw_type + ".bit")
    source_mcs_filename=  os.path.expanduser("./mcs/AMC13_T1_"   + fw_type + ".mcs")
    source_notes_filename=os.path.expanduser("./notes/ReleaseNotes_" + fw_type + ".txt")
    
    dest_bit_filename  =dest_path + "AMC13T1v"       + fw_version + "_7k325t.bit"
    dest_mcs_filename  =dest_path + "AMC13T1v"       + fw_version + "_7k325t.mcs"
    dest_notes_filename=dest_path + "AMC13XGspecK"   + fw_version + "S" + T2_fw_version + ".txt"
    
    #test for all files to exist
    if not os.path.isfile(source_bit_filename):
        raise BadFile(source_bit_filename)
    if not os.path.isfile(source_mcs_filename):
        raise BadFile(source_mcs_filename)
    if not os.path.isfile(source_notes_filename):
        raise BadFile(source_notes_filename)

    
    #send files (from: https://stackoverflow.com/a/69596/2774155)
#    ssh = paramiko.SSHClient()
    #don't uncomment the next line
    #    ssh.set_missing_host_key_policy(paramiko.client.AutoAddPolicy()) #uncomment if you don't want to freak out for unknown server keys
#    ssh.load_host_keys(os.path.expanduser(os.path.join("~",".ssh","known_hosts")))
#    ssh.connect(dest_serv,username=dest_user)
    # magic to use your ssh-agent
#    ssh_session = ssh.get_transport().open_session()
#    paramiko.agent.AgentRequestHandler(ssh_session) #connec to agent
#    sftp = ssh.open_sftp()
    bit_cmd  = "scp "+source_bit_filename   +" "+dest_user+"@"+dest_serv+":"+dest_bit_filename
    mcs_cmd  = "scp "+source_mcs_filename   +" "+dest_user+"@"+dest_serv+":"+dest_mcs_filename
    notes_cmd= "scp "+source_notes_filename +" "+dest_user+"@"+dest_serv+":"+dest_notes_filename

    os.system(bit_cmd)
    os.system(mcs_cmd)
    os.system(notes_cmd)
    
#    sftp.chdir(dest_path)
#    sftp.put(source_bit_filename,dest_bit_filename)
#    sftp.put(source_bit_filename,  dest_path + dest_bit_filename)
#    sftp.put(source_mcs_filename,  dest_path + dest_mcs_filename)
#    sftp.put(source_notes_filename,dest_path + dest_notes_filename)
#    sftp.close()
#    ssh.close()

    

def main():
    parser = argparse.ArgumentParser(description="Push new amc13 files to server")
    parser.add_argument("versions", nargs='+', help="type/version to be sent (ex: HCAL_10G/0x6049)")
    parser.add_argument("--T2",dest='T2_version',default="0x2e")
    parser.add_argument("--server",dest='server',default="ohm.bu.edu")
    parser.add_argument("--username",dest='user',default="dgastler")
    parser.add_argument("--path",dest='path',default="/home/dgastler/public_html/CMS/AMC13-Firmware/")

    parsed_args = parser.parse_args()
    
    dest_serv = parsed_args.server
    dest_user = parsed_args.user
    dest_path = parsed_args.path    
    for version in parsed_args.versions:
        #parse the version string for the correct and valid format
        fw_type = version.split('/')

        #check parsed size
        if len(fw_type) != 2:
            raise BadTypeString(version)

        #check fw type
        valid_fw_type = False
        for fw_type_check in fw_types:
            if fw_type[0] == fw_type_check:
                valid_fw_type = True
        if not valid_fw_type:
            raise BadTypeString(version)

        #check fw version number
        if fw_type[1][0:2] != "0x":
            raise BadTypeString(version)

#        try:
        scp_files(fw_type[0],fw_type[1],parsed_args.T2_version,dest_serv,dest_user,dest_path)
 #           break
#        except IOError as e:
#            print e
        

if __name__ == "__main__":
    main()
