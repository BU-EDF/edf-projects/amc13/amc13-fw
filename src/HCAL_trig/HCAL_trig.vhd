----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:06:56 07/09/2014 
-- Design Name: 
-- Module Name:    HCAL_trig - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--  Byte
--   0    Comma character (0xBC k-char)
--   1    BX0 [7] VER[6:4]=1 BX ID [11:8]
--   2    Bx ID [7:0]
--   3    local trigger word
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity HCAL_trig is
    Port ( TTC_clk : in  STD_LOGIC;
           DRPCLK : in  STD_LOGIC;
           reset : in  STD_LOGIC;
--           en_HCAL_trig : in  STD_LOGIC;
           SFP_ABS : in  STD_LOGIC;
           BC0 : in  STD_LOGIC;
           BC0_dl : out  STD_LOGIC;
           triggerOut : out  STD_LOGIC;
           Trigdata : in  array12x8;
           TTC_lock : in  STD_LOGIC;
					 AMC_en	: in  STD_LOGIC_VECTOR(11 downto 0);
					 BC0_lock	: in  STD_LOGIC_VECTOR(11 downto 0);
					 BX_offset2SC : out  STD_LOGIC_VECTOR(11 downto 0);
 --	ipbus signals
					 ipb_clk : in  STD_LOGIC;
					 ipb_write : in  STD_LOGIC;
					 ipb_strobe	: in  STD_LOGIC;
					 ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_wdata : in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0);
           GTX_REFCLKp : in  STD_LOGIC;
           GTX_REFCLKn : in  STD_LOGIC;
           GTX_RXp : in  STD_LOGIC;
           GTX_RXn : in  STD_LOGIC;
           GTX_TXp : out  STD_LOGIC;
           GTX_TXn : out  STD_LOGIC);
end HCAL_trig;

architecture Behavioral of HCAL_trig is
COMPONENT RAM32x6Db
	PORT(
		wclk : IN std_logic;
		di : IN std_logic_vector(5 downto 0);
		we : IN std_logic;
		wa : IN std_logic_vector(4 downto 0);
		ra : IN std_logic_vector(4 downto 0);          
		do : OUT std_logic_vector(5 downto 0)
		);
END COMPONENT;
component uHTR_trigPD_init 
generic
(
    -- Simulation attributes
    EXAMPLE_SIM_GTRESET_SPEEDUP    : string    := "FALSE";    -- Set to 1 to speed up sim reset
    EXAMPLE_SIMULATION             : integer   := 0;          -- Set to 1 for simulation
    STABLE_CLOCK_PERIOD            : integer   := 20;    --Period of the stable clock driving this state-machine, unit is [ns]
    EXAMPLE_USE_CHIPSCOPE          : integer   := 0           -- Set to 1 to use Chipscope to drive resets

);
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_IN                           : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y12)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    GT0_CPLLFBCLKLOST_OUT                   : out  std_logic;
    GT0_CPLLLOCK_OUT                        : out  std_logic;
    GT0_CPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_CPLLRESET_IN                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    GT0_GTREFCLK0_IN                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT0_DRPCLK_IN                           : in   std_logic;
    GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT0_DRPEN_IN                            : in   std_logic;
    GT0_DRPRDY_OUT                          : out  std_logic;
    GT0_DRPWE_IN                            : in   std_logic;
    ------------------------------ Power-Down Ports ----------------------------
    GT0_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT0_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT0_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT0_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT0_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT0_RXUSRCLK_IN                         : in   std_logic;
    GT0_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT0_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT0_RXPRBSERR_OUT                       : out  std_logic;
    GT0_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT0_RXPRBSCNTRESET_IN                   : in   std_logic;
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    GT0_RXDISPERR_OUT                       : out  std_logic_vector(3 downto 0);
    GT0_RXNOTINTABLE_OUT                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    GT0_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT0_GTXRXN_IN                           : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT0_GTRXRESET_IN                        : in   std_logic;
    GT0_RXPMARESET_IN                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    GT0_RXCHARISK_OUT                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT0_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT0_GTTXRESET_IN                        : in   std_logic;
    GT0_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT0_TXUSRCLK_IN                         : in   std_logic;
    GT0_TXUSRCLK2_IN                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT0_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT0_GTXTXN_OUT                          : out  std_logic;
    GT0_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT0_TXOUTCLK_OUT                        : out  std_logic;
    GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT0_TXCHARISK_IN                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT0_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT0_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
   

    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    GT0_GTREFCLK0_COMMON_IN                 : in   std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    GT0_QPLLLOCK_OUT                        : out  std_logic;
    GT0_QPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_QPLLRESET_IN                        : in   std_logic


);
end component;
signal version : std_logic_vector(2 downto 0) := "001";
signal MaskedTrigdata : array24x32 := (others => (others => '0'));
signal mask : array24x32 := (others => (others => '0'));
signal threshold : array8x4 := (others => (others => '0'));
signal NonZeroByteCnt : array16x4 := (others => (others => '0'));
signal NonZeroByte : std_logic_vector(95 downto 0) := (others => '0');
signal TrigDataOut : std_logic_vector(7 downto 0) := (others => '0');
signal RXDATA : std_logic_vector(31 downto 0) := (others => '0');
signal TXDATA : std_logic_vector(31 downto 0) := (others => '0');
signal Sample : std_logic := '0';
signal SampleSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal RXBUF_we : std_logic := '0';
signal RXBUF_wa : std_logic_vector(4 downto 0) := (others => '0');
signal RXBUF_di : std_logic_vector(29 downto 0) := (others => '0');
signal RXBUF : std_logic_vector(29 downto 0) := (others => '0');
signal PRBSSEL : std_logic_vector(2 downto 0) := (others => '0');
signal PRBSERR_cnt : std_logic_vector(7 downto 0) := (others => '0');
signal RXCHARISK : std_logic_vector(3 downto 0) := (others => '0');
signal txfsmresetdone : std_logic := '0';
signal DATA_VALID : std_logic := '0';
signal CPLLLOCK : std_logic := '0';
signal CPLLRESET : std_logic := '0';
signal REFCLK : std_logic := '0';
signal PRBSERR : std_logic := '0';
signal GTRXRESET : std_logic := '0';
signal RXRESETDONE : std_logic := '0';
signal GTTXRESET : std_logic := '0';
signal bcnt : std_logic_vector(11 downto 0) := (others => '0');
signal trigmask : std_logic_vector(7 downto 0) := (others => '0');
signal reset_trig_dl_wa : std_logic := '0';
signal BC0_dl_i : std_logic := '0';
signal chk_lock : std_logic := '0';
signal chk_lock_q : std_logic := '0';
signal trig_dl_ra : std_logic_vector(11 downto 0) := (others => '0');
signal trig_dl_wa : std_logic_vector(11 downto 0) := (others => '0');
signal trig_dl_DI : std_logic_vector(3 downto 0) := (others => '0');
signal trig_dl_DO : std_logic_vector(3 downto 0) := (others => '0');
signal en_HCAL_trig : std_logic := '0';
signal ec_BX_offset : std_logic := '0';
signal BC0_locked : std_logic_vector(1 downto 0) := (others =>'0');
signal BX_offset_a : std_logic_vector(11 downto 0) := (others =>'0');
signal BX_offset_b : std_logic_vector(11 downto 0) := (others =>'0');
signal BX_offset_sum : std_logic_vector(12 downto 0) := (others =>'0');
signal LockLossCntr : std_logic_vector(3 downto 0) := (others =>'0');
signal BX_offset : std_logic_vector(11 downto 0) := (others =>'0');
signal en_BC0_gated : std_logic := '0';
signal got_BC0 : std_logic := '0';
signal catchBC0 : std_logic := '0';
signal BC0_gated : std_logic := '0';
signal BC0_gated_dl : std_logic := '0';
signal GT0_RXPD_IN : std_logic_vector(1 downto 0) := (others =>'0');
signal GT0_TXPD_IN : std_logic_vector(1 downto 0) := (others =>'0');
signal SFP_ABS_q : std_logic_vector(3 downto 0) := (others => '0');
signal reset_cntr : std_logic_vector(20 downto 0) := (others => '0');
signal soft_reset : std_logic := '0';
signal reset_cntr20_q : std_logic := '0';
begin
BC0_dl <= BC0_dl_i;
triggerOut <= trig_dl_DO(0);
GT0_RXPD_IN <= "00" when SFP_ABS = '0' else "11";
GT0_TXPD_IN <= "00" when SFP_ABS = '0' else "11";
-- this compensates 5 TTC clock delay in HCAL_trig(2) and ttc_if(3)
i_reset_trig_dl_wa : SRL16E
	port map (
		Q => reset_trig_dl_wa,       -- SRL data output
		A0 => '0',     -- Select[0] input
		A1 => '0',     -- Select[1] input
		A2 => '1',     -- Select[2] input
		A3 => '0',     -- Select[3] input
		CE => '1',     -- Clock enable input
		CLK => TTC_clk,   -- Clock input
		D => BC0_dl_i        -- SRL data input
		);
process(TTC_clk)
variable dif : std_logic_vector(11 downto 0);
begin
	dif := x"dec" - BX_offset;
	if(TTC_clk'event and TTC_clk = '1')then
		if(BC0 = '1')then
			trig_dl_ra <= (others => '0');
		else
			trig_dl_ra <= trig_dl_ra + 1;
		end if;
		if(reset_trig_dl_wa = '1')then
			trig_dl_wa <= (others => '0');
		else
			trig_dl_wa <= trig_dl_wa + 1;
		end if;
		if(trig_dl_ra = BX_offset)then
			BC0_dl_i <= '1';
		else
			BC0_dl_i <= '0';
		end if;
		chk_lock <= BC0_dl_i;
		chk_lock_q <= chk_lock;
		if(TTC_lock = '0' or LockLossCntr(3) = '1')then
			BC0_locked <= "00";
		elsif(chk_lock = '1')then
			BC0_locked <= BC0_locked(0) & and_reduce(BC0_lock or (not AMC_en));
		end if;
		if(TTC_lock = '0' or LockLossCntr(3) = '1')then
			BX_offset_a <= x"fff";
		elsif(en_HCAL_trig = '0' and chk_lock_q = '1' and BC0_locked = "01")then
			BX_offset_a <= BX_offset;
		end if;
		if(TTC_lock = '0' or LockLossCntr(3) = '1')then
			BX_offset_b <= x"fff";
		elsif(en_HCAL_trig = '0' and chk_lock_q = '1' and BC0_locked = "10")then
			BX_offset_b <= BX_offset + 1;
		end if;
		BX_offset_sum <= ('0' & BX_offset_a) + ('0' & BX_offset_b);
		if(TTC_lock = '0' or LockLossCntr(3) = '1')then
			en_HCAL_trig <= '0';
		elsif(and_reduce(BX_offset_a) = '0' and and_reduce(BX_offset_b) = '0')then
			en_HCAL_trig <= '1';
		end if;
		if(en_HCAL_trig = '0')then
			LockLossCntr <= (others => '0');
		elsif(chk_lock_q = '1' and BC0_locked(1) = '0')then
			LockLossCntr <= LockLossCntr + 1;
		end if;
		if(TTC_lock = '0')then
			ec_BX_offset <= '0';
		elsif(chk_lock = '1')then
			ec_BX_offset <= '1';
		else
			ec_BX_offset <= '0';
		end if;
		if(TTC_lock = '0' or LockLossCntr(3) = '1')then
			BX_offset <= x"deb";
		elsif(en_HCAL_trig = '1')then
			BX_offset <= BX_offset_sum(12 downto 1);
		elsif(ec_BX_offset = '1')then
			if(BX_offset = x"000")then
				BX_offset <= x"deb";
			else
				BX_offset <= BX_offset - 1;
			end if;
		end if;
		if(BX_offset(11) = '1')then
			BX_offset2SC <= '1' & dif(10 downto 0);
		else
			BX_offset2SC <= BX_offset;
		end if;
	end if;
end process;
i_trig_dl : BRAM_SDP_MACRO
   generic map (
      BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 4,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 4)     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
   port map (
      DO => trig_dl_DO,         -- Output read data port, width defined by READ_WIDTH parameter
      DI => trig_dl_DI,         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => trig_dl_ra, -- Input read address, width defined by read port depth
      RDCLK => TTC_Clk,   -- 1-bit input read clock
      RDEN => '1',     -- 1-bit input read port enable
      REGCE => '1',   -- 1-bit input read output register enable
      RST => '0',       -- 1-bit input reset 
      WE => "1",         -- Input write enable, width defined by write port depth
      WRADDR => trig_dl_wa, -- Input write address, width defined by write port depth
      WRCLK => TTC_Clk,   -- 1-bit input write clock
      WREN => '1'      -- 1-bit input write port enable
   );
process(TTC_Clk)
begin
	if(TTC_Clk'event and TTC_Clk = '1')then
		if(BC0_dl_i = '1')then
			bcnt <= x"000";
		else
			bcnt <= bcnt + 1;
		end if;
		for i in 0 to 7 loop
			if(en_HCAL_trig = '1' and (NonZeroByteCnt(i*2) + NonZeroByteCnt(i*2+1)) > threshold(i))then
				TrigDataOut(i) <= '1';
			else
				TrigDataOut(i) <= '0';
			end if;
		end loop;
		DATA_VALID <= not RXBUF_di(24);
		if(PRBSSEL = "000")then
			PRBSERR_cnt <= (others => '0');
		elsif(PRBSERR = '1')then
			PRBSERR_cnt <= PRBSERR_cnt + 1;
		end if;
		trig_dl_DI(0) <= en_HCAL_trig and or_reduce(trigmask and TrigDataOut);
		SampleSyncRegs <= SampleSyncRegs(2 downto 0) & Sample;
		if(SampleSyncRegs(3) /= SampleSyncRegs(2))then
			RXBUF_we <= '1';
		elsif((CatchBC0 = '0' and and_reduce(RXBUF_wa) = '1') or BC0_gated_dl = '1')then
			RXBUF_we <= '0';
		end if;
		if(RXBUF_we = '0')then
			RXBUF_wa <= "00010";
		elsif(and_reduce(RXBUF_wa) = '1')then
			RXBUF_wa <= "00010";
		else
			RXBUF_wa <= RXBUF_wa + 1;
		end if;
		if(got_BC0 = '1' or RXBUF_we = '0')then
			en_BC0_gated <= '0';
		elsif(RXBUF_wa(4) = '1')then
			en_BC0_gated <= '1';
		end if;
		if(RXBUF_we = '0')then
			got_BC0 <= '0';
		elsif(BC0_gated = '1')then
			got_BC0 <= '1';
		end if;
		BC0_gated <= RXDATA(15) and en_BC0_gated;
	end if;
end process;
i_BC0_gated_dl : SRL16E
   port map (
      Q => BC0_gated_dl,       -- SRL data output
      A0 => '0',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => TTC_Clk,   -- Clock input
      D => BC0_gated        -- SRL data input
   );
g_RXBUF : for i in 0 to 4 generate	 
	i_ipbus_rbuf : RAM32x6Db PORT MAP(
		wclk => TTC_Clk,
		di => RXBUF_di(i*6+5 downto i*6),
		we => RXBUF_we,
		wa => RXBUF_wa,
		ra => ipb_addr(4 downto 0),
		do => RXBUF(i*6+5 downto i*6)
	);
end generate;
TXDATA <= TrigDataOut & bcnt(7 downto 0) & chk_lock & version & bcnt(11 downto 8) & x"bc";
RXBUF_di(24) <= '0' when RXDATA(7 downto 0) = x"bc" and RXCHARISK = x"1" else '1';
RXBUF_di(23 downto 0) <= RXDATA(31 downto 24) & RXDATA(15 downto 8) & RXDATA(23 downto 16);
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 4) = x"100" and ipb_write = '1' and ipb_strobe = '1')then
			mask(conv_integer(ipb_addr(3 downto 0))) <= ipb_wdata;
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 4) = x"101" and ipb_write = '1' and ipb_strobe = '1')then
			if(ipb_addr(3) = '0')then
				mask(16+conv_integer(ipb_addr(2 downto 0))) <= ipb_wdata;
			else
				threshold(conv_integer(ipb_addr(2 downto 0))) <= ipb_wdata(3 downto 0);
			end if;
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1020" and ipb_write = '1' and ipb_strobe = '1')then
			PRBSSEL <= ipb_wdata(2 downto 0);
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1021" and ipb_write = '1' and ipb_strobe = '1')then
			Sample <= Sample xor ipb_wdata(0);
			CatchBC0 <= ipb_wdata(1);
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1040" and ipb_write = '1' and ipb_strobe = '1')then
			trigmask <= ipb_wdata(7 downto 0);
		end if;
	end if;
end process;
process(ipb_addr, mask, threshold)
begin
	if(ipb_addr(15 downto 7) = "000100000")then
		if(ipb_addr(6 downto 0) = "1000000")then
			ipb_rdata <= x"000000" & trigmask;
		elsif(ipb_addr(5 downto 4) = "00")then
			ipb_rdata <= mask(conv_integer(ipb_addr(3 downto 0)));
		elsif(ipb_addr(5 downto 3) = "010")then
			ipb_rdata <= mask(16+conv_integer(ipb_addr(2 downto 0)));
		elsif(ipb_addr(5 downto 3) = "011")then
			ipb_rdata <= x"0000000" & threshold(conv_integer(ipb_addr(2 downto 0)));
		elsif(ipb_addr(5 downto 0) = "100000")then
			ipb_rdata <= x"0000000" & '0' & PRBSSEL;
		elsif(ipb_addr(5 downto 0) = "100001")then
			ipb_rdata <= x"000000" & PRBSERR_cnt;
		elsif(ipb_addr(5) = '1')then
			ipb_rdata <= RXBUF(24) & "0000000" & RXBUF(23 downto 0);
		else
			ipb_rdata <= (others => '0');
		end if;
	else
		ipb_rdata <= (others => '0');
	end if;
end process;
g_MaskedTrigdata : for i in 0 to 7 generate
	g_MaskedTrigdataJ : for j in 0 to 2 generate
		g_MaskedTrigdataK : for k in 0 to 3 generate
			MaskedTrigdata(i*3+j)(k*8+7 downto k*8) <= not mask(i*3+j)(k*8+7 downto k*8) and Trigdata(j*4+k);
		end generate;
	end generate;
end generate;
g_NonZeroByte : for i in 0 to 23 generate
	g_NonZeroByteJ : for j in 0 to 3 generate
		NonZeroByte(i*4+j) <= '0' when MaskedTrigdata(i)(j*8+7 downto j*8) = x"00" else '1';
	end generate;	
end generate;
g_NonZeroByteCnt_i : for i in 0 to 1 generate
	g_NonZeroByteCnt_j : for j in 0 to 7 generate
   i_bitcount0 : ROM64X1
   generic map (
      INIT => X"6996966996696996") -- bit 0
   port map (
      O => NonZeroByteCnt(j*2+i)(0),   -- ROM output
      A0 => NonZeroByte(i*6+j*12), -- ROM address[0]
      A1 => NonZeroByte(i*6+1+j*12), -- ROM address[1]
      A2 => NonZeroByte(i*6+2+j*12), -- ROM address[2]
      A3 => NonZeroByte(i*6+3+j*12), -- ROM address[3]
      A4 => NonZeroByte(i*6+4+j*12), -- ROM address[4]
      A5 => NonZeroByte(i*6+5+j*12)  -- ROM address[5]
   );
   i_bitcount1 : ROM64X1
   generic map (
      INIT => X"8117177e177e7ee8") -- bit 1
   port map (
      O => NonZeroByteCnt(j*2+i)(1),   -- ROM output
      A0 => NonZeroByte(i*6+j*12), -- ROM address[0]
      A1 => NonZeroByte(i*6+1+j*12), -- ROM address[1]
      A2 => NonZeroByte(i*6+2+j*12), -- ROM address[2]
      A3 => NonZeroByte(i*6+3+j*12), -- ROM address[3]
      A4 => NonZeroByte(i*6+4+j*12), -- ROM address[4]
      A5 => NonZeroByte(i*6+5+j*12)  -- ROM address[5]
   );
   i_bitcount2 : ROM64X1
   generic map (
      INIT => X"fee8e880e8808000") -- bit 2
   port map (
      O => NonZeroByteCnt(j*2+i)(2),   -- ROM output
      A0 => NonZeroByte(i*6+j*12), -- ROM address[0]
      A1 => NonZeroByte(i*6+1+j*12), -- ROM address[1]
      A2 => NonZeroByte(i*6+2+j*12), -- ROM address[2]
      A3 => NonZeroByte(i*6+3+j*12), -- ROM address[3]
      A4 => NonZeroByte(i*6+4+j*12), -- ROM address[4]
      A5 => NonZeroByte(i*6+5+j*12)  -- ROM address[5]
   );
	end generate;
end generate;
process(DRPclk)
begin
	if(DRPclk'event and DRPclk = '1')then
		SFP_ABS_q <= SFP_ABS_q(2 downto 0) & SFP_ABS;
		if(SFP_ABS_q(3 downto 2) = "10")then
			reset_cntr <= (others => '0');
		elsif(reset_cntr(20) = '0')then
			reset_cntr <= reset_cntr + 1;
		end if;
		reset_cntr20_q <= reset_cntr(20);
		soft_reset <= not reset_cntr20_q and reset_cntr(20);
	end if;
end process;
uHTR_trig_init_i : uHTR_trigPD_init
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     =>      "TRUE",
        EXAMPLE_SIMULATION              =>      0,
        STABLE_CLOCK_PERIOD             =>      20,
        EXAMPLE_USE_CHIPSCOPE           =>      0
    )
    port map
    (
        SYSCLK_IN                       =>      DRPCLK,
        SOFT_RESET_IN                   =>      soft_reset,
        DONT_RESET_ON_DATA_ERROR_IN     =>      '0',
        GT0_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone,
        GT0_RX_FSM_RESET_DONE_OUT       =>      open,
        GT0_DATA_VALID_IN               =>      DATA_VALID,

  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X1Y12)

        --------------------------------- CPLL Ports -------------------------------
        GT0_CPLLFBCLKLOST_OUT           =>      open,
        GT0_CPLLLOCK_OUT                =>      CPLLLOCK,
        GT0_CPLLLOCKDETCLK_IN           =>      DRPCLK,
        GT0_CPLLRESET_IN                =>      CPLLRESET,
        -------------------------- Channel - Clocking Ports ------------------------
        GT0_GTREFCLK0_IN                =>      REFCLK,
        ---------------------------- Channel - DRP Ports  --------------------------
        GT0_DRPADDR_IN                  =>      (others => '0'),
        GT0_DRPCLK_IN                   =>      DRPCLK,
        GT0_DRPDI_IN                    =>      (others => '0'),
        GT0_DRPDO_OUT                   =>      open,
        GT0_DRPEN_IN                    =>      '0',
        GT0_DRPRDY_OUT                  =>      open,
        GT0_DRPWE_IN                    =>      '0',
        ------------------------------ Power-Down Ports ----------------------------
        GT0_RXPD_IN                     =>      GT0_RXPD_IN,
        GT0_TXPD_IN                     =>      GT0_TXPD_IN,
        --------------------- RX Initialization and Reset Ports --------------------
        GT0_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT0_EYESCANDATAERROR_OUT        =>      open,
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT0_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT0_RXUSRCLK_IN                 =>      TTC_clk,
        GT0_RXUSRCLK2_IN                =>      TTC_clk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT0_RXDATA_OUT                  =>      RXDATA,
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT0_RXPRBSERR_OUT               =>      PRBSERR,
        GT0_RXPRBSSEL_IN                =>      PRBSSEL,
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT0_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT0_RXDISPERR_OUT               =>      open,
        GT0_RXNOTINTABLE_OUT            =>      open,
        --------------------------- Receive Ports - RX AFE -------------------------
        GT0_GTXRXP_IN                   =>      GTX_RXp,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT0_GTXRXN_IN                   =>      GTX_RXn,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT0_GTRXRESET_IN                =>      '0',
        GT0_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT0_RXCHARISK_OUT               =>      RXCHARISK,
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT0_RXRESETDONE_OUT             =>      RXRESETDONE,
        --------------------- TX Initialization and Reset Ports --------------------
        GT0_GTTXRESET_IN                =>      '0',
        GT0_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT0_TXUSRCLK_IN                 =>      TTC_Clk,
        GT0_TXUSRCLK2_IN                =>      TTC_Clk,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_TXDATA_IN                   =>      TXDATA,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  =>      GTX_TXn,
        GT0_GTXTXP_OUT                  =>      GTX_TXp,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT0_TXOUTCLK_OUT                =>      open,
        GT0_TXOUTCLKFABRIC_OUT          =>      open,
        GT0_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT0_TXCHARISK_IN                =>      x"1",
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT0_TXRESETDONE_OUT             =>      open,
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT0_TXPRBSSEL_IN                =>      PRBSSEL,




    --____________________________COMMON PORTS________________________________
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GT0_GTREFCLK0_COMMON_IN         =>      '0',
        ------------------------- Common Block - QPLL Ports ------------------------
        GT0_QPLLLOCK_OUT                =>      open,
        GT0_QPLLLOCKDETCLK_IN           =>      '0',
        GT0_QPLLRESET_IN                =>      '0'

    );

i_REFCLK : IBUFDS_GTE2  
    port map
    (
        O               => 	REFCLK,
        ODIV2           =>  open,
        CEB             => 	'0',
        I               => 	GTX_REFCLKp,
        IB              => 	GTX_REFCLKn
    );
end Behavioral;

