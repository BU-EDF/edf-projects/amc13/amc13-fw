
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use work.amc13_pack.all;
use work.mydefs.all;


library UNISIM;
use UNISIM.VComponents.all;

entity DaqLSCXG10G is
    Port ( sys_reset : in  STD_LOGIC; -- active high reset of all logic but GTX
           sys_clk : in  STD_LOGIC;
           sfp_pd : in  array3x2;
           DRP_clk : in  STD_LOGIC;
           LinkWe : in  STD_LOGIC_VECTOR (2 downto 0);
           LinkCtrl : in  STD_LOGIC_VECTOR (2 downto 0);
           LinkData : in  array3x64;
           srcID : in array3x16;
           LinkDown : out  STD_LOGIC_VECTOR (2 downto 0);
           LinkFull : out  STD_LOGIC_VECTOR (2 downto 0);
			  --
--           ack_cnt : out  STD_LOGIC_VECTOR (2 downto 0);        -- 1 ck pulse (txusrclk) indicating a received acknowledge
--           pckt_cnt : out  STD_LOGIC_VECTOR (2 downto 0);       -- 1 ck pulse (txusrclk) indicating a transmit packet
--           retransmit_cnt : out  STD_LOGIC_VECTOR (2 downto 0); -- 1 ck pulse (txusrclk) indicating a retransmit packet
--           event_cnt : out  STD_LOGIC_VECTOR (2 downto 0);      -- 1 ck pulse (sys_clk) indicating a sent event
			  sync_loss : out STD_LOGIC_VECTOR (2 downto 0);		  -- goes to '1' (rxusrclk) when SERDES is out of synch
			  status_ce : in std_logic_VECTOR (2 downto 0);        -- not implemented yet
			  status_addr : in STD_LOGIC_VECTOR (15 downto 0); -- not implemented yet
			  status_port : out array3x64; -- first 32 bits are hard-wired
			  --
			  txusrclk_o : out STD_LOGIC;  -- reconstructed tx clock, to be used to clock sending circuitry
			  rxusrclk_o : out STD_LOGIC;  -- reconstructed rx clock, to be used to clock receiving circuitry
			  --
			  gtx_reset : in std_logic;    -- full reset of GTX only
			  gtx_refclk_p : in std_logic; -- iob for refclk neg
			  gtx_refclk_n : in std_logic; -- iob for refclk neg
			  sfp_rxn : in std_logic_VECTOR (2 downto 0);      -- sfp iobs
			  sfp_rxp : in std_logic_VECTOR (2 downto 0);
			  sfp_txn : out std_logic_VECTOR (2 downto 0);
			  sfp_txp : out std_logic_VECTOR (2 downto 0)
			 );
end DaqLSCXG10G;

architecture Behavioral of DaqLSCXG10G is
COMPONENT SLINK_opt_XGMII
port (
	reset						: in std_logic;
	-- FED interface
	SYS_CLK					: in std_logic;
	LINKWe					: in std_logic;
	LINKCtrl				: in std_logic;
	LINKData				: in std_logic_vector(63 downto 0);
	src_ID					: in std_logic_vector(15 downto 0);
	inject_err				: in std_logic_vector(17 downto 0);
	read_CE					: in std_logic;
	Addr					: in std_logic_vector(15 downto 0);
	status_data 			: out std_logic_vector(63 downto 0);
	LINKDown				: out std_logic;
	LINK_LFF				: out std_logic;
	
	-- interface SERDES
	clock					: in std_logic;
	serdes_init				: in std_logic;
	SD_Data_o				: out std_logic_vector(63 downto 0);
	SD_Kb_o					: out std_logic_vector(7 downto 0);
	clock_r					: in std_logic;	
	SD_Data_i				: in std_logic_vector(63 downto 0);
	SD_Kb_i					: in std_logic_vector(7 downto 0);
	
	Serdes_status			: in std_logic_vector(31 downto 0) 

	);
END COMPONENT;
COMPONENT XGMII_serdes_wapper
	generic(N_SFP : integer := 3);
	PORT(
		DRPclk : IN std_logic;
		reset : IN std_logic;
		gtx_reset : IN std_logic;
    sfp_pd : in  array3x2;
		SFP0_RXN : IN std_logic;
		SFP0_RXP : IN std_logic;
		SFP1_RXN : IN std_logic;
		SFP1_RXP : IN std_logic;
		SFP2_RXN : IN std_logic;
		SFP2_RXP : IN std_logic;
		SFP_REFCLK_P : IN std_logic;
		SFP_REFCLK_N : IN std_logic;
		xgmii_txd : IN array3x64;
		xgmii_txc : IN array3x8;          
		SFP0_TXN : OUT std_logic;
		SFP0_TXP : OUT std_logic;
		SFP1_TXN : OUT std_logic;
		SFP1_TXP : OUT std_logic;
		SFP2_TXN : OUT std_logic;
		SFP2_TXP : OUT std_logic;
		clk156 : OUT std_logic;
		PCS_lock : OUT std_logic_vector(2 downto 0);
		gtx_rxresetdone : OUT std_logic_vector(2 downto 0);
		xgmii_rxd : OUT array3x64;
		xgmii_rxc : OUT array3x8
		);
END COMPONENT;

-- Local signals

signal sys_reset_bar : std_logic;
signal serdes_core_clk156_out : std_logic;
signal txdata, rxdata : array3x64;
signal rxcharisk, txcharisk, rxchariscomma, gtx_rxnotintable : array3x8;
signal gtx_rxresetdone : std_logic_vector(2 downto 0);
signal PCS_lock : std_logic_vector(2 downto 0);
signal serdes_status : array3x32 := (others => (others => '0'));
begin
txusrclk_o <= serdes_core_clk156_out;
rxusrclk_o <= serdes_core_clk156_out;
sync_loss <= not PCS_lock;
g_SLINK_opt : for i in 0 to 2 generate
	Inst_SLINK_opt: SLINK_opt_XGMII
PORT MAP(  
-- FROM FED logic  
      reset => sys_reset_bar, -- needs an active low reset
      SYS_CLK => sys_clk,
-- DATA interface from FED
      LINKWe => not LinkWe(i),
      LINKCtrl => LinkCtrl(i),
      LINKData => LinkData(i),
      src_ID => srcID(i),
      inject_err => (others =>'0'),
      read_CE => '0',
      Addr => status_addr,
      status_data => status_port(i),
      serdes_status => serdes_status(i),
      LINKDown => LinkDown(i),
      LINK_LFF => LinkFull(i),
-- SERDES interface    
      clock => serdes_core_clk156_out, --clk_156_service,             -- clk tx from SERDES
      serdes_init => serdes_status(i)(0),   -- status that comes back from GTX
      SD_Data_o => TXDATA(i),       -- data sent to serdes (64 bit)
      SD_Kb_o => TXCHARISK(i),         -- control K associated to SD_Data_o (8 bits)
      clock_r => serdes_core_clk156_out,         -- reconstructed clock from SERDES
      SD_Data_i => RXDATA(i),          -- return data from SERDES 64 bit
      SD_Kb_i => RXCHARISK(i)          -- return control K associated to SD_Data_i (8 bits)
   );
	serdes_status(i)(0) <= PCS_lock(i);
	serdes_status(i)(1) <= gtx_rxresetdone(i);
end generate;
sys_reset_bar <= not(sys_reset);
i_XGMII_serdes_wapper: XGMII_serdes_wapper PORT MAP(
				DRPclk => DRP_clk,
				reset => sys_reset,
				gtx_reset => gtx_reset,
				sfp_pd => sfp_pd,
				SFP0_RXN => sfp_rxn(0),
				SFP0_RXP => sfp_rxp(0),
				SFP1_RXN => sfp_rxn(1),
				SFP1_RXP => sfp_rxp(1),
				SFP2_RXN => sfp_rxn(2),
				SFP2_RXP => sfp_rxp(2),
				SFP0_TXN => sfp_txn(0),
				SFP0_TXP => sfp_txp(0),
				SFP1_TXN => sfp_txn(1),
				SFP1_TXP => sfp_txp(1),
				SFP2_TXN => sfp_txn(2),
				SFP2_TXP => sfp_txp(2),
				SFP_REFCLK_P => gtx_refclk_p,
				SFP_REFCLK_N => gtx_refclk_n,
				clk156 => serdes_core_clk156_out,
				PCS_lock => PCS_lock,
				gtx_rxresetdone => gtx_rxresetdone,
				xgmii_txd => TXDATA,
				xgmii_txc => TXCHARISK,
				xgmii_rxd => RXDATA,
				xgmii_rxc => RXCHARISK
			);
end Behavioral;

