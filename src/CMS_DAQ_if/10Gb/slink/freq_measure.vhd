------------------------------------------------------
-- Frequency Clock detection
--
--  Ver 1.00
--
-- Dominique Gigi Jan 2015
------------------------------------------------------
--   Measure the clock frequency used by FED
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
library work;

USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.mydefs.all;
 

entity freq_measure is
port (
	reset				: in std_logic;
	
	sysclk			: in std_logic;-- clock used by the FED to send data and to measure the backpressure
	base_clk			: in std_logic;-- clock base used to measure the sysclk
	
	frequency		: out std_logic_vector(31 downto 0)-- measure of the frequency)
);
end freq_measure;

architecture behavioral of freq_measure is


component reset_resync is
port (
	reset				: in std_logic;
	clock				: in std_logic; 

	Reset_sync		: out std_logic
	);
end component;


signal counter_base			: std_logic_vector(31 downto 0);
signal counter_measure		: std_logic_vector(31 downto 0);
signal measure					: std_logic_vector(31 downto 0);
signal latch_value			: std_logic;
signal reset_cnt				: std_logic;
signal reset_cnt_rsyc		: std_logic;
--*********************************************************
--************         CODE START HERE     ****************
--*********************************************************
begin

-- counter base
process(base_clk)
begin
	if rising_edge(base_clk) then
		-- base on the base_clk of 156.25 MHz  => 6.4ns  => 156250 x to reach 1ms
		latch_value			<= '0';
		if counter_base = freq_used then 
			counter_base 	<= (others => '0');
			latch_value		<= '1';
		else
			counter_base 	<= counter_base + '1';
		end if;
	end if;
end process;


-- counter measure
process(sysclk,reset_cnt_rsyc)
begin
	if reset_cnt_rsyc = '0' then
		counter_measure <= (others => '0');
	elsif rising_edge(sysclk) then
		counter_measure <= counter_measure + '1';
	end if;
end process;	

resync_i1:reset_resync
port map(
	reset				=> reset_cnt,
	clock				=> sysclk, 
	Reset_sync		=> reset_cnt_rsyc
	);
	
-- latch the frequency
process(base_clk)
begin
	if rising_edge(base_clk) then
		reset_cnt		<= '1'; -- reset the counter measure when the measure is latched
		if latch_value = '1' then
			measure 		<= counter_measure;
			reset_cnt	<= '0';
		end if;
	end if;
end process;
	
frequency	<= measure;
	
end behavioral;



