------------------------------------------------------
-- XAUI 64-bit alignment or reorder
--
--  Ver 1.00
--
-- Dominique Gigi May 2015
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.mydefs.all;

entity xaui_wd_align is

port (
	reset	: in std_logic;
	clock	: in std_logic;
	
	data_i : in std_logic_vector(63 downto 0);
	ctrl_i	: in std_logic_vector(7 downto 0);
	data_o	: out std_logic_vector(63 downto 0);
	ctrl_o	: out std_logic_vector(7 downto 0)
	);	
end xaui_wd_align;
architecture behavioral of xaui_wd_align is


signal mem_r_d	: std_logic_vector(63 downto 0);
signal mem_r_c	: std_logic_vector(7 downto 0);
signal reg_d	: std_logic_vector(63 downto 0);
signal reg_c	: std_logic_vector(7 downto 0);
signal shift	: std_logic;

--attribute mark_debug : string;

--attribute mark_debug of shift : signal is "true";
--attribute mark_debug of reg_d, mem_r_d : signal is "true";
--attribute mark_debug of reg_c : signal is "true";

begin 

process(reset,clock)
begin
if reset = '0' then
	shift <= '0';
elsif rising_edge(clock) then
	
	if FPGA_Brand = "ALTERA" then
		if shift = '1' then
			reg_d(63 downto 56) 	<=  data_i(55 downto 48);
			reg_c(7)					<=  ctrl_i(6);
			reg_d(55 downto 48) 	<=  data_i(39 downto 32); 
			reg_c(6)					<=  ctrl_i(4);
			reg_d(47 downto 40) 	<=  data_i(23 downto 16); 
			reg_c(5)					<=  ctrl_i(2);
			reg_d(39 downto 32) 	<=  data_i(07 downto 00);
			reg_c(4)					<=  ctrl_i(0);
			
			reg_d(31 downto 24) 	<=  mem_r_d(63 downto 56);
			reg_c(3)					<=  mem_r_c(7);
			reg_d(23 downto 16) 	<=  mem_r_d(47 downto 40); 
			reg_c(2)					<=  mem_r_c(5);
			reg_d(15 downto  8) 	<=  mem_r_d(31 downto 24); 
			reg_c(1)					<=  mem_r_c(3);
			reg_d( 7 downto  0) 	<=  mem_r_d(15 downto 08);
			reg_c(0)					<=  mem_r_c(1);	
		else 
			reg_d(63 downto 56) 	<=  data_i(63 downto 56);
			reg_c(7)					<=  ctrl_i(7);
			reg_d(55 downto 48) 	<=  data_i(47 downto 40); 
			reg_c(6)					<=  ctrl_i(5);
			reg_d(47 downto 40) 	<=  data_i(31 downto 24); 
			reg_c(5)					<=  ctrl_i(3);
			reg_d(39 downto 32) 	<=  data_i(15 downto 08);
			reg_c(4)					<=  ctrl_i(1);
			reg_d(31 downto 24) 	<=  data_i(55 downto 48);
			reg_c(3)					<=  ctrl_i(6);
			reg_d(23 downto 16) 	<=  data_i(39 downto 32); 
			reg_c(2)					<=  ctrl_i(4);
			reg_d(15 downto  8) 	<=  data_i(23 downto 16); 
			reg_c(1)					<=  ctrl_i(2);
			reg_d( 7 downto  0) 	<=  data_i(07 downto 00);
			reg_c(0)					<=  ctrl_i(0);	
		end if;
		
		if 		data_i( 7 downto 0) = x"FB" and ctrl_i(0) = '1' then
			shift 					<= '0';
		elsif 	data_i(15 downto 8) = x"FB" and ctrl_i(1) = '1' then
			shift 					<= '1';
		end if;		
		
		
	elsif FPGA_Brand = "XILINX" then
		if shift = '1' then
			reg_d(63 downto 32) 	<=  data_i(31 downto 00);
			reg_c(7 downto 4)		<=  ctrl_i(3 downto 0);
			
			reg_d(31 downto 0) 	<=  mem_r_d(63 downto 32);
			reg_c(3 downto 0)		<=  mem_r_c(7 downto 4);
		else 
			reg_d						<=  mem_r_d;--data_i;
			reg_c						<=  mem_r_c;--ctrl_i;
		end if;	
		
		if 		data_i( 7 downto 0) = x"FB" and ctrl_i(0) = '1' then
			shift 					<= '0';
		elsif 	data_i(39 downto 32) = x"FB" and ctrl_i(4) = '1' then
			shift 					<= '1';
		end if;
		
	end if;	



	mem_r_d 						<= data_i;
	mem_r_c						<= ctrl_i;
	
end if;
end process;

data_o							<= reg_d;
ctrl_o							<= reg_c;

end behavioral;


