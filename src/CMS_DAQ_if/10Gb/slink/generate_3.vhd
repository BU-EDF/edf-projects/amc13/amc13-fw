-- PROGRAM		"Quartus II"
-- VERSION		"Version 10.1 Build 197 01/19/2011 Service Pack 1 SJ Full Version"
-- CREATED		"Mon Jul 08 17:43:34 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
 
ENTITY generate_3 IS 
	PORT
	(
		clock 			:  IN  STD_LOGIC;
		START 			:  IN  STD_LOGIC;
		LOAD_SEED 		:  IN  STD_LOGIC;
		SEED 				:  IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		rnd 				:  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END generate_3;

ARCHITECTURE behavioral OF generate_3 IS 


signal result 			: std_logic_vector(7 downto 0);
signal adder			: std_logic_vector(7 downto 0);
signal multi			: std_logic_vector(10 downto 0);
signal interm			: std_logic_vector(7 downto 0);

signal del_ld_seed	: std_logic;

--*****************************************************************************
BEGIN 

process(clock)
begin
	if rising_edge(clock) then
		del_ld_seed <= LOAD_SEED;
	end if;
end process;

-- initial x(i)  or x(n+1)
process(clock)
begin
	if rising_edge(clock) then
		if 	LOAD_SEED = '1' then
			interm <= SEED;
		elsif START = '1' then
			interm <= adder;
		end if;		
	end if;
end process;

-- the rnd is build  x(n) <=  x(n-1) * 5 + 3 
-- multiplication
process(interm)
	variable term				: integer;
	variable produit			: integer;
begin
	term			:= 	TO_INTEGER(UNSIGNED(interm));
	produit		:= 	term	*	5;
	multi 		<= 	STD_LOGIC_VECTOR( TO_SIGNED(produit,11) );
end process;

-- adder
process(multi)
begin
	adder <= multi(7 downto 0) + x"03";
end process;


-- out the result
process(clock)
begin
	if rising_edge(clock) then
		if del_ld_seed = '1' or START = '1' then
			result		<=  adder(7 downto 0);
		end if;
	end if;
end process;

rnd 		<= result;

END behavioral;