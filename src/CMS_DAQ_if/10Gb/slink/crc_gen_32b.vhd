--------------------------------------------------------------------------------
-- Copyright (C) 1999-2008 Easics NV.
-- This source file may be used and distributed without restriction
-- provided that this copyright statement is not removed from the file
-- and that any derivative work contains the original copyright notice
-- and the associated disclaimer.
--
-- THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
-- WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
--
-- Purpose : synthesizable CRC function
--   * polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
--   * data width: 64
--
-- Info : tools@easics.be
--        http://www.easics.com
--
--  D. Gigi info 2011
-- data start with lower 32 bit (bit empty set to '1' for the first word
-- continu with all 64 bit data word
--
-- data start all the time with 64 bit and can finish aligned on 16 bit 32 bit 48 bit or 64 bit
--------------------------------------------------------------------------------
LIBRARY IEEE ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_arith.all ;
USE ieee.std_logic_unsigned.all ;

ENTITY crc_gen_32b IS 
   PORT(           
				clock      	: IN  STD_LOGIC; 
				reset      	: IN  STD_LOGIC; 
				data       	: IN  STD_LOGIC_VECTOR(63 DOWNTO 0); 
				data_valid 	: IN  STD_LOGIC; 
				eoc        	: IN  STD_LOGIC; 
		   
				crc        	: OUT STD_LOGIC_VECTOR(31 DOWNTO 0); 
				crc_valid  	: OUT STD_LOGIC 
       );
END crc_gen_32b; 

ARCHITECTURE behave OF crc_gen_32b IS 

 SIGNAL crc_rT		  	: STD_LOGIC_VECTOR(31 DOWNTO 0); -- Total CRC (32 bit)
 SIGNAL crc_8T		   	: STD_LOGIC_VECTOR(31 DOWNTO 0);
 SIGNAL crc_i		   	: STD_LOGIC_VECTOR(31 DOWNTO 0);
 SIGNAL End_of_crc		: STD_LOGIC;
 

 
BEGIN 

 
crc_i <= crc_rT;

crc_8T(0) <= data(63) xor data(61) xor data(60) xor data(58) xor data(55) xor data(54) xor data(53) xor data(50) xor data(48) xor data(47) xor data(45) xor data(44) xor data(37) xor data(34) xor data(32) xor data(31) xor data(30) xor data(29) xor data(28) xor data(26) xor data(25) xor data(24) xor data(16) xor data(12) xor data(10) xor data(9) xor data(6) xor data(0) xor crc_i(0) xor crc_i(2) xor crc_i(5) xor crc_i(12) xor crc_i(13) xor crc_i(15) xor crc_i(16) xor crc_i(18) xor crc_i(21) xor crc_i(22) xor crc_i(23) xor crc_i(26) xor crc_i(28) xor crc_i(29) xor crc_i(31);
crc_8T(1) <= data(63) xor data(62) xor data(60) xor data(59) xor data(58) xor data(56) xor data(53) xor data(51) xor data(50) xor data(49) xor data(47) xor data(46) xor data(44) xor data(38) xor data(37) xor data(35) xor data(34) xor data(33) xor data(28) xor data(27) xor data(24) xor data(17) xor data(16) xor data(13) xor data(12) xor data(11) xor data(9) xor data(7) xor data(6) xor data(1) xor data(0) xor crc_i(1) xor crc_i(2) xor crc_i(3) xor crc_i(5) xor crc_i(6) xor crc_i(12) xor crc_i(14) xor crc_i(15) xor crc_i(17) xor crc_i(18) xor crc_i(19) xor crc_i(21) xor crc_i(24) xor crc_i(26) xor crc_i(27) xor crc_i(28) xor crc_i(30) xor crc_i(31);
crc_8T(2) <= data(59) xor data(58) xor data(57) xor data(55) xor data(53) xor data(52) xor data(51) xor data(44) xor data(39) xor data(38) xor data(37) xor data(36) xor data(35) xor data(32) xor data(31) xor data(30) xor data(26) xor data(24) xor data(18) xor data(17) xor data(16) xor data(14) xor data(13) xor data(9) xor data(8) xor data(7) xor data(6) xor data(2) xor data(1) xor data(0) xor crc_i(0) xor crc_i(3) xor crc_i(4) xor crc_i(5) xor crc_i(6) xor crc_i(7) xor crc_i(12) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(23) xor crc_i(25) xor crc_i(26) xor crc_i(27);
crc_8T(3) <= data(60) xor data(59) xor data(58) xor data(56) xor data(54) xor data(53) xor data(52) xor data(45) xor data(40) xor data(39) xor data(38) xor data(37) xor data(36) xor data(33) xor data(32) xor data(31) xor data(27) xor data(25) xor data(19) xor data(18) xor data(17) xor data(15) xor data(14) xor data(10) xor data(9) xor data(8) xor data(7) xor data(3) xor data(2) xor data(1) xor crc_i(0) xor crc_i(1) xor crc_i(4) xor crc_i(5) xor crc_i(6) xor crc_i(7) xor crc_i(8) xor crc_i(13) xor crc_i(20) xor crc_i(21) xor crc_i(22) xor crc_i(24) xor crc_i(26) xor crc_i(27) xor crc_i(28);
crc_8T(4) <= data(63) xor data(59) xor data(58) xor data(57) xor data(50) xor data(48) xor data(47) xor data(46) xor data(45) xor data(44) xor data(41) xor data(40) xor data(39) xor data(38) xor data(33) xor data(31) xor data(30) xor data(29) xor data(25) xor data(24) xor data(20) xor data(19) xor data(18) xor data(15) xor data(12) xor data(11) xor data(8) xor data(6) xor data(4) xor data(3) xor data(2) xor data(0) xor crc_i(1) xor crc_i(6) xor crc_i(7) xor crc_i(8) xor crc_i(9) xor crc_i(12) xor crc_i(13) xor crc_i(14) xor crc_i(15) xor crc_i(16) xor crc_i(18) xor crc_i(25) xor crc_i(26) xor crc_i(27) xor crc_i(31);
crc_8T(5) <= data(63) xor data(61) xor data(59) xor data(55) xor data(54) xor data(53) xor data(51) xor data(50) xor data(49) xor data(46) xor data(44) xor data(42) xor data(41) xor data(40) xor data(39) xor data(37) xor data(29) xor data(28) xor data(24) xor data(21) xor data(20) xor data(19) xor data(13) xor data(10) xor data(7) xor data(6) xor data(5) xor data(4) xor data(3) xor data(1) xor data(0) xor crc_i(5) xor crc_i(7) xor crc_i(8) xor crc_i(9) xor crc_i(10) xor crc_i(12) xor crc_i(14) xor crc_i(17) xor crc_i(18) xor crc_i(19) xor crc_i(21) xor crc_i(22) xor crc_i(23) xor crc_i(27) xor crc_i(29) xor crc_i(31);
crc_8T(6) <= data(62) xor data(60) xor data(56) xor data(55) xor data(54) xor data(52) xor data(51) xor data(50) xor data(47) xor data(45) xor data(43) xor data(42) xor data(41) xor data(40) xor data(38) xor data(30) xor data(29) xor data(25) xor data(22) xor data(21) xor data(20) xor data(14) xor data(11) xor data(8) xor data(7) xor data(6) xor data(5) xor data(4) xor data(2) xor data(1) xor crc_i(6) xor crc_i(8) xor crc_i(9) xor crc_i(10) xor crc_i(11) xor crc_i(13) xor crc_i(15) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(22) xor crc_i(23) xor crc_i(24) xor crc_i(28) xor crc_i(30);
crc_8T(7) <= data(60) xor data(58) xor data(57) xor data(56) xor data(54) xor data(52) xor data(51) xor data(50) xor data(47) xor data(46) xor data(45) xor data(43) xor data(42) xor data(41) xor data(39) xor data(37) xor data(34) xor data(32) xor data(29) xor data(28) xor data(25) xor data(24) xor data(23) xor data(22) xor data(21) xor data(16) xor data(15) xor data(10) xor data(8) xor data(7) xor data(5) xor data(3) xor data(2) xor data(0) xor crc_i(0) xor crc_i(2) xor crc_i(5) xor crc_i(7) xor crc_i(9) xor crc_i(10) xor crc_i(11) xor crc_i(13) xor crc_i(14) xor crc_i(15) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(22) xor crc_i(24) xor crc_i(25) xor crc_i(26) xor crc_i(28);
crc_8T(8) <= data(63) xor data(60) xor data(59) xor data(57) xor data(54) xor data(52) xor data(51) xor data(50) xor data(46) xor data(45) xor data(43) xor data(42) xor data(40) xor data(38) xor data(37) xor data(35) xor data(34) xor data(33) xor data(32) xor data(31) xor data(28) xor data(23) xor data(22) xor data(17) xor data(12) xor data(11) xor data(10) xor data(8) xor data(4) xor data(3) xor data(1) xor data(0) xor crc_i(0) xor crc_i(1) xor crc_i(2) xor crc_i(3) xor crc_i(5) xor crc_i(6) xor crc_i(8) xor crc_i(10) xor crc_i(11) xor crc_i(13) xor crc_i(14) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(22) xor crc_i(25) xor crc_i(27) xor crc_i(28) xor crc_i(31);
crc_8T(9) <= data(61) xor data(60) xor data(58) xor data(55) xor data(53) xor data(52) xor data(51) xor data(47) xor data(46) xor data(44) xor data(43) xor data(41) xor data(39) xor data(38) xor data(36) xor data(35) xor data(34) xor data(33) xor data(32) xor data(29) xor data(24) xor data(23) xor data(18) xor data(13) xor data(12) xor data(11) xor data(9) xor data(5) xor data(4) xor data(2) xor data(1) xor crc_i(0) xor crc_i(1) xor crc_i(2) xor crc_i(3) xor crc_i(4) xor crc_i(6) xor crc_i(7) xor crc_i(9) xor crc_i(11) xor crc_i(12) xor crc_i(14) xor crc_i(15) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(23) xor crc_i(26) xor crc_i(28) xor crc_i(29);
crc_8T(10) <= data(63) xor data(62) xor data(60) xor data(59) xor data(58) xor data(56) xor data(55) xor data(52) xor data(50) xor data(42) xor data(40) xor data(39) xor data(36) xor data(35) xor data(33) xor data(32) xor data(31) xor data(29) xor data(28) xor data(26) xor data(19) xor data(16) xor data(14) xor data(13) xor data(9) xor data(5) xor data(3) xor data(2) xor data(0) xor crc_i(0) xor crc_i(1) xor crc_i(3) xor crc_i(4) xor crc_i(7) xor crc_i(8) xor crc_i(10) xor crc_i(18) xor crc_i(20) xor crc_i(23) xor crc_i(24) xor crc_i(26) xor crc_i(27) xor crc_i(28) xor crc_i(30) xor crc_i(31);
crc_8T(11) <= data(59) xor data(58) xor data(57) xor data(56) xor data(55) xor data(54) xor data(51) xor data(50) xor data(48) xor data(47) xor data(45) xor data(44) xor data(43) xor data(41) xor data(40) xor data(36) xor data(33) xor data(31) xor data(28) xor data(27) xor data(26) xor data(25) xor data(24) xor data(20) xor data(17) xor data(16) xor data(15) xor data(14) xor data(12) xor data(9) xor data(4) xor data(3) xor data(1) xor data(0) xor crc_i(1) xor crc_i(4) xor crc_i(8) xor crc_i(9) xor crc_i(11) xor crc_i(12) xor crc_i(13) xor crc_i(15) xor crc_i(16) xor crc_i(18) xor crc_i(19) xor crc_i(22) xor crc_i(23) xor crc_i(24) xor crc_i(25) xor crc_i(26) xor crc_i(27);
crc_8T(12) <= data(63) xor data(61) xor data(59) xor data(57) xor data(56) xor data(54) xor data(53) xor data(52) xor data(51) xor data(50) xor data(49) xor data(47) xor data(46) xor data(42) xor data(41) xor data(31) xor data(30) xor data(27) xor data(24) xor data(21) xor data(18) xor data(17) xor data(15) xor data(13) xor data(12) xor data(9) xor data(6) xor data(5) xor data(4) xor data(2) xor data(1) xor data(0) xor crc_i(9) xor crc_i(10) xor crc_i(14) xor crc_i(15) xor crc_i(17) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(22) xor crc_i(24) xor crc_i(25) xor crc_i(27) xor crc_i(29) xor crc_i(31);
crc_8T(13) <= data(62) xor data(60) xor data(58) xor data(57) xor data(55) xor data(54) xor data(53) xor data(52) xor data(51) xor data(50) xor data(48) xor data(47) xor data(43) xor data(42) xor data(32) xor data(31) xor data(28) xor data(25) xor data(22) xor data(19) xor data(18) xor data(16) xor data(14) xor data(13) xor data(10) xor data(7) xor data(6) xor data(5) xor data(3) xor data(2) xor data(1) xor crc_i(0) xor crc_i(10) xor crc_i(11) xor crc_i(15) xor crc_i(16) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(22) xor crc_i(23) xor crc_i(25) xor crc_i(26) xor crc_i(28) xor crc_i(30);
crc_8T(14) <= data(63) xor data(61) xor data(59) xor data(58) xor data(56) xor data(55) xor data(54) xor data(53) xor data(52) xor data(51) xor data(49) xor data(48) xor data(44) xor data(43) xor data(33) xor data(32) xor data(29) xor data(26) xor data(23) xor data(20) xor data(19) xor data(17) xor data(15) xor data(14) xor data(11) xor data(8) xor data(7) xor data(6) xor data(4) xor data(3) xor data(2) xor crc_i(0) xor crc_i(1) xor crc_i(11) xor crc_i(12) xor crc_i(16) xor crc_i(17) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(22) xor crc_i(23) xor crc_i(24) xor crc_i(26) xor crc_i(27) xor crc_i(29) xor crc_i(31);
crc_8T(15) <= data(62) xor data(60) xor data(59) xor data(57) xor data(56) xor data(55) xor data(54) xor data(53) xor data(52) xor data(50) xor data(49) xor data(45) xor data(44) xor data(34) xor data(33) xor data(30) xor data(27) xor data(24) xor data(21) xor data(20) xor data(18) xor data(16) xor data(15) xor data(12) xor data(9) xor data(8) xor data(7) xor data(5) xor data(4) xor data(3) xor crc_i(1) xor crc_i(2) xor crc_i(12) xor crc_i(13) xor crc_i(17) xor crc_i(18) xor crc_i(20) xor crc_i(21) xor crc_i(22) xor crc_i(23) xor crc_i(24) xor crc_i(25) xor crc_i(27) xor crc_i(28) xor crc_i(30);
crc_8T(16) <= data(57) xor data(56) xor data(51) xor data(48) xor data(47) xor data(46) xor data(44) xor data(37) xor data(35) xor data(32) xor data(30) xor data(29) xor data(26) xor data(24) xor data(22) xor data(21) xor data(19) xor data(17) xor data(13) xor data(12) xor data(8) xor data(5) xor data(4) xor data(0) xor crc_i(0) xor crc_i(3) xor crc_i(5) xor crc_i(12) xor crc_i(14) xor crc_i(15) xor crc_i(16) xor crc_i(19) xor crc_i(24) xor crc_i(25);
crc_8T(17) <= data(58) xor data(57) xor data(52) xor data(49) xor data(48) xor data(47) xor data(45) xor data(38) xor data(36) xor data(33) xor data(31) xor data(30) xor data(27) xor data(25) xor data(23) xor data(22) xor data(20) xor data(18) xor data(14) xor data(13) xor data(9) xor data(6) xor data(5) xor data(1) xor crc_i(1) xor crc_i(4) xor crc_i(6) xor crc_i(13) xor crc_i(15) xor crc_i(16) xor crc_i(17) xor crc_i(20) xor crc_i(25) xor crc_i(26);
crc_8T(18) <= data(59) xor data(58) xor data(53) xor data(50) xor data(49) xor data(48) xor data(46) xor data(39) xor data(37) xor data(34) xor data(32) xor data(31) xor data(28) xor data(26) xor data(24) xor data(23) xor data(21) xor data(19) xor data(15) xor data(14) xor data(10) xor data(7) xor data(6) xor data(2) xor crc_i(0) xor crc_i(2) xor crc_i(5) xor crc_i(7) xor crc_i(14) xor crc_i(16) xor crc_i(17) xor crc_i(18) xor crc_i(21) xor crc_i(26) xor crc_i(27);
crc_8T(19) <= data(60) xor data(59) xor data(54) xor data(51) xor data(50) xor data(49) xor data(47) xor data(40) xor data(38) xor data(35) xor data(33) xor data(32) xor data(29) xor data(27) xor data(25) xor data(24) xor data(22) xor data(20) xor data(16) xor data(15) xor data(11) xor data(8) xor data(7) xor data(3) xor crc_i(0) xor crc_i(1) xor crc_i(3) xor crc_i(6) xor crc_i(8) xor crc_i(15) xor crc_i(17) xor crc_i(18) xor crc_i(19) xor crc_i(22) xor crc_i(27) xor crc_i(28);
crc_8T(20) <= data(61) xor data(60) xor data(55) xor data(52) xor data(51) xor data(50) xor data(48) xor data(41) xor data(39) xor data(36) xor data(34) xor data(33) xor data(30) xor data(28) xor data(26) xor data(25) xor data(23) xor data(21) xor data(17) xor data(16) xor data(12) xor data(9) xor data(8) xor data(4) xor crc_i(1) xor crc_i(2) xor crc_i(4) xor crc_i(7) xor crc_i(9) xor crc_i(16) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(23) xor crc_i(28) xor crc_i(29);
crc_8T(21) <= data(62) xor data(61) xor data(56) xor data(53) xor data(52) xor data(51) xor data(49) xor data(42) xor data(40) xor data(37) xor data(35) xor data(34) xor data(31) xor data(29) xor data(27) xor data(26) xor data(24) xor data(22) xor data(18) xor data(17) xor data(13) xor data(10) xor data(9) xor data(5) xor crc_i(2) xor crc_i(3) xor crc_i(5) xor crc_i(8) xor crc_i(10) xor crc_i(17) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(24) xor crc_i(29) xor crc_i(30);
crc_8T(22) <= data(62) xor data(61) xor data(60) xor data(58) xor data(57) xor data(55) xor data(52) xor data(48) xor data(47) xor data(45) xor data(44) xor data(43) xor data(41) xor data(38) xor data(37) xor data(36) xor data(35) xor data(34) xor data(31) xor data(29) xor data(27) xor data(26) xor data(24) xor data(23) xor data(19) xor data(18) xor data(16) xor data(14) xor data(12) xor data(11) xor data(9) xor data(0) xor crc_i(2) xor crc_i(3) xor crc_i(4) xor crc_i(5) xor crc_i(6) xor crc_i(9) xor crc_i(11) xor crc_i(12) xor crc_i(13) xor crc_i(15) xor crc_i(16) xor crc_i(20) xor crc_i(23) xor crc_i(25) xor crc_i(26) xor crc_i(28) xor crc_i(29) xor crc_i(30);
crc_8T(23) <= data(62) xor data(60) xor data(59) xor data(56) xor data(55) xor data(54) xor data(50) xor data(49) xor data(47) xor data(46) xor data(42) xor data(39) xor data(38) xor data(36) xor data(35) xor data(34) xor data(31) xor data(29) xor data(27) xor data(26) xor data(20) xor data(19) xor data(17) xor data(16) xor data(15) xor data(13) xor data(9) xor data(6) xor data(1) xor data(0) xor crc_i(2) xor crc_i(3) xor crc_i(4) xor crc_i(6) xor crc_i(7) xor crc_i(10) xor crc_i(14) xor crc_i(15) xor crc_i(17) xor crc_i(18) xor crc_i(22) xor crc_i(23) xor crc_i(24) xor crc_i(27) xor crc_i(28) xor crc_i(30);
crc_8T(24) <= data(63) xor data(61) xor data(60) xor data(57) xor data(56) xor data(55) xor data(51) xor data(50) xor data(48) xor data(47) xor data(43) xor data(40) xor data(39) xor data(37) xor data(36) xor data(35) xor data(32) xor data(30) xor data(28) xor data(27) xor data(21) xor data(20) xor data(18) xor data(17) xor data(16) xor data(14) xor data(10) xor data(7) xor data(2) xor data(1) xor crc_i(0) xor crc_i(3) xor crc_i(4) xor crc_i(5) xor crc_i(7) xor crc_i(8) xor crc_i(11) xor crc_i(15) xor crc_i(16) xor crc_i(18) xor crc_i(19) xor crc_i(23) xor crc_i(24) xor crc_i(25) xor crc_i(28) xor crc_i(29) xor crc_i(31);
crc_8T(25) <= data(62) xor data(61) xor data(58) xor data(57) xor data(56) xor data(52) xor data(51) xor data(49) xor data(48) xor data(44) xor data(41) xor data(40) xor data(38) xor data(37) xor data(36) xor data(33) xor data(31) xor data(29) xor data(28) xor data(22) xor data(21) xor data(19) xor data(18) xor data(17) xor data(15) xor data(11) xor data(8) xor data(3) xor data(2) xor crc_i(1) xor crc_i(4) xor crc_i(5) xor crc_i(6) xor crc_i(8) xor crc_i(9) xor crc_i(12) xor crc_i(16) xor crc_i(17) xor crc_i(19) xor crc_i(20) xor crc_i(24) xor crc_i(25) xor crc_i(26) xor crc_i(29) xor crc_i(30);
crc_8T(26) <= data(62) xor data(61) xor data(60) xor data(59) xor data(57) xor data(55) xor data(54) xor data(52) xor data(49) xor data(48) xor data(47) xor data(44) xor data(42) xor data(41) xor data(39) xor data(38) xor data(31) xor data(28) xor data(26) xor data(25) xor data(24) xor data(23) xor data(22) xor data(20) xor data(19) xor data(18) xor data(10) xor data(6) xor data(4) xor data(3) xor data(0) xor crc_i(6) xor crc_i(7) xor crc_i(9) xor crc_i(10) xor crc_i(12) xor crc_i(15) xor crc_i(16) xor crc_i(17) xor crc_i(20) xor crc_i(22) xor crc_i(23) xor crc_i(25) xor crc_i(27) xor crc_i(28) xor crc_i(29) xor crc_i(30);
crc_8T(27) <= data(63) xor data(62) xor data(61) xor data(60) xor data(58) xor data(56) xor data(55) xor data(53) xor data(50) xor data(49) xor data(48) xor data(45) xor data(43) xor data(42) xor data(40) xor data(39) xor data(32) xor data(29) xor data(27) xor data(26) xor data(25) xor data(24) xor data(23) xor data(21) xor data(20) xor data(19) xor data(11) xor data(7) xor data(5) xor data(4) xor data(1) xor crc_i(0) xor crc_i(7) xor crc_i(8) xor crc_i(10) xor crc_i(11) xor crc_i(13) xor crc_i(16) xor crc_i(17) xor crc_i(18) xor crc_i(21) xor crc_i(23) xor crc_i(24) xor crc_i(26) xor crc_i(28) xor crc_i(29) xor crc_i(30) xor crc_i(31);
crc_8T(28) <= data(63) xor data(62) xor data(61) xor data(59) xor data(57) xor data(56) xor data(54) xor data(51) xor data(50) xor data(49) xor data(46) xor data(44) xor data(43) xor data(41) xor data(40) xor data(33) xor data(30) xor data(28) xor data(27) xor data(26) xor data(25) xor data(24) xor data(22) xor data(21) xor data(20) xor data(12) xor data(8) xor data(6) xor data(5) xor data(2) xor crc_i(1) xor crc_i(8) xor crc_i(9) xor crc_i(11) xor crc_i(12) xor crc_i(14) xor crc_i(17) xor crc_i(18) xor crc_i(19) xor crc_i(22) xor crc_i(24) xor crc_i(25) xor crc_i(27) xor crc_i(29) xor crc_i(30) xor crc_i(31);
crc_8T(29) <= data(63) xor data(62) xor data(60) xor data(58) xor data(57) xor data(55) xor data(52) xor data(51) xor data(50) xor data(47) xor data(45) xor data(44) xor data(42) xor data(41) xor data(34) xor data(31) xor data(29) xor data(28) xor data(27) xor data(26) xor data(25) xor data(23) xor data(22) xor data(21) xor data(13) xor data(9) xor data(7) xor data(6) xor data(3) xor crc_i(2) xor crc_i(9) xor crc_i(10) xor crc_i(12) xor crc_i(13) xor crc_i(15) xor crc_i(18) xor crc_i(19) xor crc_i(20) xor crc_i(23) xor crc_i(25) xor crc_i(26) xor crc_i(28) xor crc_i(30) xor crc_i(31);
crc_8T(30) <= data(63) xor data(61) xor data(59) xor data(58) xor data(56) xor data(53) xor data(52) xor data(51) xor data(48) xor data(46) xor data(45) xor data(43) xor data(42) xor data(35) xor data(32) xor data(30) xor data(29) xor data(28) xor data(27) xor data(26) xor data(24) xor data(23) xor data(22) xor data(14) xor data(10) xor data(8) xor data(7) xor data(4) xor crc_i(0) xor crc_i(3) xor crc_i(10) xor crc_i(11) xor crc_i(13) xor crc_i(14) xor crc_i(16) xor crc_i(19) xor crc_i(20) xor crc_i(21) xor crc_i(24) xor crc_i(26) xor crc_i(27) xor crc_i(29) xor crc_i(31);
crc_8T(31) <= data(62) xor data(60) xor data(59) xor data(57) xor data(54) xor data(53) xor data(52) xor data(49) xor data(47) xor data(46) xor data(44) xor data(43) xor data(36) xor data(33) xor data(31) xor data(30) xor data(29) xor data(28) xor data(27) xor data(25) xor data(24) xor data(23) xor data(15) xor data(11) xor data(9) xor data(8) xor data(5) xor crc_i(1) xor crc_i(4) xor crc_i(11) xor crc_i(12) xor crc_i(14) xor crc_i(15) xor crc_i(17) xor crc_i(20) xor crc_i(21) xor crc_i(22) xor crc_i(25) xor crc_i(27) xor crc_i(28) xor crc_i(30);


crc_gen_process : PROCESS(clock) 
BEGIN                                    
 IF rising_edge(clock) THEN 
	IF reset = '1' then
		crc_rT <= (others => '1') ;
    ELSIF(data_valid = '1') THEN 
		crc_rT <= crc_8T;
    END IF; 
    End_of_crc <= eoc and data_valid;

 END IF;    
END PROCESS crc_gen_process;      
    
crc_valid		<= End_of_crc;
crc 			<= crc_rT;
 
 
END behave;