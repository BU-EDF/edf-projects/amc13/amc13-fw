library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;



entity CRC_SLINKx is
    Port ( 
		D 				: in std_logic_vector(63 downto 0);
		CRC_out 		: out std_logic_vector(15 downto 0);
		clk 			: in std_logic;
		clear 		: in std_logic;
		enable 		: in std_logic);
end CRC_SLINKx;

architecture Behavioral of CRC_SLINKx is
	signal C			: STD_LOGIC_VECTOR (15 downto 0);
 
begin

process (clk)
	variable NewCRC: STD_LOGIC_VECTOR (15 downto 0);
begin
 
if rising_edge(clk) then
   if clear = '1'  then
		C 			<= (Others => '1');
	elsif  enable = '1' then

    NewCRC(0) := D(63) xor D(62) xor D(61) xor D(60) xor D(55) xor D(54) xor 
                 D(53) xor D(52) xor D(51) xor D(50) xor D(49) xor D(48) xor 
                 D(47) xor D(46) xor D(45) xor D(43) xor D(41) xor D(40) xor 
                 D(39) xor D(38) xor D(37) xor D(36) xor D(35) xor D(34) xor 
                 D(33) xor D(32) xor D(31) xor D(30) xor D(27) xor D(26) xor 
                 D(25) xor D(24) xor D(23) xor D(22) xor D(21) xor D(20) xor 
                 D(19) xor D(18) xor D(17) xor D(16) xor D(15) xor D(13) xor 
                 D(12) xor D(11) xor D(10) xor D(9) xor D(8) xor D(7) xor 
                 D(6) xor D(5) xor D(4) xor D(3) xor D(2) xor D(1) xor 
                 D(0) xor C(0) xor C(1) xor C(2) xor C(3) xor C(4) xor 
                 C(5) xor C(6) xor C(7) xor C(12) xor C(13) xor C(14) xor 
                 C(15);
    NewCRC(1) := D(63) xor D(62) xor D(61) xor D(56) xor D(55) xor D(54) xor 
                 D(53) xor D(52) xor D(51) xor D(50) xor D(49) xor D(48) xor 
                 D(47) xor D(46) xor D(44) xor D(42) xor D(41) xor D(40) xor 
                 D(39) xor D(38) xor D(37) xor D(36) xor D(35) xor D(34) xor 
                 D(33) xor D(32) xor D(31) xor D(28) xor D(27) xor D(26) xor 
                 D(25) xor D(24) xor D(23) xor D(22) xor D(21) xor D(20) xor 
                 D(19) xor D(18) xor D(17) xor D(16) xor D(14) xor D(13) xor 
                 D(12) xor D(11) xor D(10) xor D(9) xor D(8) xor D(7) xor 
                 D(6) xor D(5) xor D(4) xor D(3) xor D(2) xor D(1) xor 
                 C(0) xor C(1) xor C(2) xor C(3) xor C(4) xor C(5) xor 
                 C(6) xor C(7) xor C(8) xor C(13) xor C(14) xor C(15);
    NewCRC(2) := D(61) xor D(60) xor D(57) xor D(56) xor D(46) xor D(42) xor 
                 D(31) xor D(30) xor D(29) xor D(28) xor D(16) xor D(14) xor 
                 D(1) xor D(0) xor C(8) xor C(9) xor C(12) xor C(13);
    NewCRC(3) := D(62) xor D(61) xor D(58) xor D(57) xor D(47) xor D(43) xor 
                 D(32) xor D(31) xor D(30) xor D(29) xor D(17) xor D(15) xor 
                 D(2) xor D(1) xor C(9) xor C(10) xor C(13) xor C(14);
    NewCRC(4) := D(63) xor D(62) xor D(59) xor D(58) xor D(48) xor D(44) xor 
                 D(33) xor D(32) xor D(31) xor D(30) xor D(18) xor D(16) xor 
                 D(3) xor D(2) xor C(0) xor C(10) xor C(11) xor C(14) xor 
                 C(15);
    NewCRC(5) := D(63) xor D(60) xor D(59) xor D(49) xor D(45) xor D(34) xor 
                 D(33) xor D(32) xor D(31) xor D(19) xor D(17) xor D(4) xor 
                 D(3) xor C(1) xor C(11) xor C(12) xor C(15);
    NewCRC(6) := D(61) xor D(60) xor D(50) xor D(46) xor D(35) xor D(34) xor 
                 D(33) xor D(32) xor D(20) xor D(18) xor D(5) xor D(4) xor 
                 C(2) xor C(12) xor C(13);
    NewCRC(7) := D(62) xor D(61) xor D(51) xor D(47) xor D(36) xor D(35) xor 
                 D(34) xor D(33) xor D(21) xor D(19) xor D(6) xor D(5) xor 
                 C(3) xor C(13) xor C(14);
    NewCRC(8) := D(63) xor D(62) xor D(52) xor D(48) xor D(37) xor D(36) xor 
                 D(35) xor D(34) xor D(22) xor D(20) xor D(7) xor D(6) xor 
                 C(0) xor C(4) xor C(14) xor C(15);
    NewCRC(9) := D(63) xor D(53) xor D(49) xor D(38) xor D(37) xor D(36) xor 
                 D(35) xor D(23) xor D(21) xor D(8) xor D(7) xor C(1) xor 
                 C(5) xor C(15);
    NewCRC(10) := D(54) xor D(50) xor D(39) xor D(38) xor D(37) xor D(36) xor 
                  D(24) xor D(22) xor D(9) xor D(8) xor C(2) xor C(6);
    NewCRC(11) := D(55) xor D(51) xor D(40) xor D(39) xor D(38) xor D(37) xor 
                  D(25) xor D(23) xor D(10) xor D(9) xor C(3) xor C(7);
    NewCRC(12) := D(56) xor D(52) xor D(41) xor D(40) xor D(39) xor D(38) xor 
                  D(26) xor D(24) xor D(11) xor D(10) xor C(4) xor C(8);
    NewCRC(13) := D(57) xor D(53) xor D(42) xor D(41) xor D(40) xor D(39) xor 
                  D(27) xor D(25) xor D(12) xor D(11) xor C(5) xor C(9);
    NewCRC(14) := D(58) xor D(54) xor D(43) xor D(42) xor D(41) xor D(40) xor 
                  D(28) xor D(26) xor D(13) xor D(12) xor C(6) xor C(10);
    NewCRC(15) := D(63) xor D(62) xor D(61) xor D(60) xor D(59) xor D(54) xor 
                  D(53) xor D(52) xor D(51) xor D(50) xor D(49) xor D(48) xor 
                  D(47) xor D(46) xor D(45) xor D(44) xor D(42) xor D(40) xor 
                  D(39) xor D(38) xor D(37) xor D(36) xor D(35) xor D(34) xor 
                  D(33) xor D(32) xor D(31) xor D(30) xor D(29) xor D(26) xor 
                  D(25) xor D(24) xor D(23) xor D(22) xor D(21) xor D(20) xor 
                  D(19) xor D(18) xor D(17) xor D(16) xor D(15) xor D(14) xor 
                  D(12) xor D(11) xor D(10) xor D(9) xor D(8) xor D(7) xor 
                  D(6) xor D(5) xor D(4) xor D(3) xor D(2) xor D(1) xor 
                  D(0) xor C(0) xor C(1) xor C(2) xor C(3) xor C(4) xor 
                  C(5) xor C(6) xor C(11) xor C(12) xor C(13) xor C(14) xor 
                  C(15);


   C 					<= NewCRC; 
	end if;
end if;

end process;

CRC_out 				<= C;
	

end Behavioral;
