------------------------------------------------------
-- Component FIFO
--
--  Ver 1.00
--
-- Dominique Gigi May 2015
------------------------------------------------------
--   This file contain un instatiation of a FIFO (ALTERA  or XILINX)
--  with he almost FUll signal 
--   
--  
------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 

entity FIFO_sync is
generic (	fifo_deep : integer := 6
	);
port 
	(
		aclr		: in std_logic; -- active low
		clk_w		: in std_logic;
		wen		: in std_logic;
		dataw		: in std_logic_vector(65 downto 0);
		almost_f	: out std_logic;	-- active low
		clk_r		: in std_logic;
		datar		: out std_logic_vector(65 downto 0);
		ren		: in std_logic;
		empty		: out std_logic  -- active low
	);
end FIFO_sync;

architecture behavioral of FIFO_sync is



--***********************************************************
--**********************  ALTERA DC FIFO  *******************
--***********************************************************

-- component LPM_FIFO_v14
	-- PORT
	-- (
		-- aclr		: IN STD_LOGIC  := '0';
		-- data		: IN STD_LOGIC_VECTOR (65 DOWNTO 0);
		-- rdclk		: IN STD_LOGIC ;
		-- rdreq		: IN STD_LOGIC ;
		-- wrclk		: IN STD_LOGIC ;
		-- wrreq		: IN STD_LOGIC ;
		-- q		: OUT STD_LOGIC_VECTOR (65 DOWNTO 0);
		-- rdempty		: OUT STD_LOGIC ;
		-- wrusedw		: OUT STD_LOGIC_VECTOR (5 DOWNTO 0)
	-- );
-- end component;


COMPONENT lpm_fifo_dc     -- !!!!!!!!!!!!!!!  XILINX ISE and VIVADO VERSION  !!!!!!!!!!!!!
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(65 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
  );
END COMPONENT;



signal almost_full_reg		: std_logic;
signal resetp					: std_logic;
signal word_used				: std_logic_vector(fifo_deep-1 downto 0);


--***********************************************************
--**********************  BEGIN  ****************************
--***********************************************************
begin

resetp <= '1' when aclr = '0' else '0';


-- fifo_dc:LPM_FIFO_v14    -- !!!!!!!!!!!!!  ALTERA VERSION	
-- port MAP(
	-- DATA 				=> dataw,
	-- wrclk 			=> clk_w,
	-- rdclk 			=> clk_r,
	-- WRREQ 			=> wen,
	-- RDREQ 			=> ren,
	-- ACLR 				=> resetp,
	-- Q 					=> datar,
	-- WRUSEDW 			=> word_used,
	-- RDEMPTY 			=> empty
	-- );

fifo_dc : lpm_fifo_dc -- !!!!!!!!!!!!!!!  XILINX ISE and Vivado VERSION  !!!!!!!!!!!!!
  PORT MAP (
    rst => resetp,
    wr_clk => clk_w,
    din => dataw,	 
    wr_en => wen,	 
    wr_data_count => word_used,	 
    rd_clk => clk_r,
    rd_en => ren,
    dout => datar,
  -- full => full,
    empty => empty

  );




process(aclr,clk_w)
begin
if aclr = '0' then
	almost_full_reg 		<= '1';
elsif rising_edge(clk_w) then
	if word_used >= "110000" then  --enable almostFull when reaches   48 data in FIFO of 64
		almost_full_reg 	<= '0';
	elsif word_used < "100101" then -- realize almostFull below 37 data in FIFO of 64
		almost_full_reg 	<= '1';
	end if;
end if;
end process;

almost_f <= almost_full_reg;

end behavioral;