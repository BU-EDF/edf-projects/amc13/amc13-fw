library IEEE;
library WORK;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

package mydefs is
	constant version : std_logic_vector(31 downto 0) := x"5E100003";
	--this constant is defined to have 1 ms with a 156.25 Mhz
	constant freq_used: std_logic_vector(31 downto 0) := x"0002625a"; 
	constant	FPGA_Brand: string	:= "XILINX";
end package mydefs;

-- version .......
--
--*****************************************************************
-- version "5E100003"	29/04/2016
-- error on retransmit signal  / change the reset of the xaui_align (to be sync with the clock)
-- increase timeout x1E0 to x200
--*****************************************************************
-- version "5E100002"	25/04/2016
-- Bug on Timeout
-- add debug on event_ongoing and DUP Header and Trailer 
--*****************************************************************
-- version "5E100001";
-- first version for 10Gb
--*****************************************************************
-- version "00000000";
-- Beta version used by HCAL and TCDS
--*****************************************************************
