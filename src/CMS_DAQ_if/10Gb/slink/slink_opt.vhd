------------------------------------------------------
-- SLINK Optical interface sender
--
--  Ver 1.00
--
-- Dominique Gigi May 2015
------------------------------------------------------
--   INstatiate the differents components for sender optical SLINK
--   Bit 31 of the "cmd" specifies if it is a write '1' or a read '0'
--   Command DAQON/DAQOFF	cmd = 0x10006 	Func(6)  	DT30 = 1 DAQ OFF	=0 DAQON  
--														DT31 = 1 TEST mode 	=0 No TEST mode
--  
------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
 

entity SLINK_opt_XGMII is
port (
	reset						: in std_logic;
	-- FED interface
	SYS_CLK					: in std_logic;
	LINKWe					: in std_logic;
	LINKCtrl					: in std_logic;
	LINKData					: in std_logic_vector(63 downto 0);
	src_ID					: in std_logic_vector(15 downto 0);
	inject_err				: in std_logic_vector(17 downto 0);
	read_CE					: in std_logic;
	Addr						: in std_logic_vector(15 downto 0);
	status_data 			: out std_logic_vector(63 downto 0);
	LINKDown					: out std_logic;
	LINK_LFF					: out std_logic;
	
	-- interface SERDES
	clock						: in std_logic;
	serdes_init				: in std_logic;
	SD_Data_o				: out std_logic_vector(63 downto 0);
	SD_Kb_o					: out std_logic_vector(7 downto 0);
	clock_r					: in std_logic;	
	SD_Data_i				: in std_logic_vector(63 downto 0);
	SD_Kb_i					: in std_logic_vector(7 downto 0);
	
	Serdes_status			: in std_logic_vector(31 downto 0) 

	);
end SLINK_opt_XGMII;

architecture behavioral of SLINK_opt_XGMII is

component fed_itf
generic (generator : boolean := true);
port (
	reset_sysCLK			: in std_logic;
	Greset_sysCLK			: in std_logic;
	sys_clk					: in std_logic;
	LinkWe 					: in  STD_LOGIC;
	LinkCtrl 				: in  STD_LOGIC;
	LinkData 				: in  STD_LOGIC_VECTOR (63 downto 0);
	LinkAlmostFull 		: out  STD_LOGIC;
	LinkDown 				: out  STD_LOGIC;
	src_ID 					: in  STD_LOGIC_VECTOR (15 downto 0);
	inject_err 				: in  STD_LOGIC_VECTOR (17 downto 0);
	read_ce 					: in  STD_LOGIC;
	addr 						: in  STD_LOGIC_VECTOR (15 downto 0);
	status_data 			: out  STD_LOGIC_VECTOR (63 downto 0);
	reset_CLK				: in std_logic;
	Greset_CLK				: in std_logic;	
	clock						: in std_logic; -- clock from internal logic
	block_free				: in std_logic;	-- almost one block is free
	data_fed					: out std_logic_vector(63 downto 0);
	block_sz_fed			: out std_logic_vector(15 downto 0);
	wr_ena					: out std_logic;
	start_evt				: out std_logic;  
	stop_evt					: out std_logic;  
	end_blk_fed				: out std_logic;
	wr_cmd					: in std_logic;  
	func						: in std_logic_vector(31 downto 0);
	data_wr					: in std_logic_vector(31 downto 0);
	data_rd					: out std_logic_vector(63 downto 0);
	cnt_evt					: out std_logic;			-- pulse for each event (on sys_clk)
	cnt_pckt_rcv			: in std_logic_vector(31 downto 0);
	cnt_pckt_snd			: in std_logic_vector(31 downto 0);
			-- status
	retransmit_ena			: in std_logic;
	status_state_build_p	: in std_logic_vector(31 downto 0);
	status_state_core		: in std_logic_vector(31 downto 0);
	Serdes_status			: in std_logic_vector(31 downto 0)
	);
end component;

component Core_logic  
port (
	reset_clk				: in std_logic;
	Greset_clk				: in std_logic;
	clock						: in std_logic;
		-- interface from the FED block	
	data_fed					: in std_logic_vector(63 downto 0);
	wen						: in std_logic;
	start_evt				: in std_logic;
	stop_evt					: in std_logic;
	block_sz_fed			: in std_logic_vector(15 downto 0);
	end_blk_fed				: in std_logic;
	block_free				: out std_logic;
	src_ID					: in std_logic_vector(15 downto 0);
	req_reset_resync		: out	std_logic;
		-- interface to the SERDES OUT (send part)
	start_pckt				: out std_logic; -- trigger the packet send
	init_pckt				: out std_logic; -- indicates that the packet is a Init packet
	ack_pckt					: out std_logic; -- indicates that the packet is a acknoldge packet
	data_pckt				: out std_logic; -- indicates that the packet is a data packet
	data_evt					: out std_logic_vector(63 downto 0);	--data for data packet
	status					: out std_logic_vector(63 downto 0); --read_bck for acknowledge packet
	card_ID					: out std_logic_vector(15 downto 0); -- CARD_ID
	Seq_nb					: out std_logic_vector(30 downto 0); -- sequence number
	len_pckt					: out std_logic_vector(15 downto 0); -- length of the packet (for data packet only) other 0
	cmd						: out std_logic_vector(63 downto 0); -- command bit for data packet only
	rd_dt						: in std_logic;
	end_snd_pckt			: in std_logic;
	idle_state				: in std_logic;
	serdes_init				: in std_logic;
		-- interface to the SERDES IN (receiver part)
	cmd_rcv					: in std_logic_vector(31 downto 0);	-- command from MOL
	data_rcv					: in std_logic_vector(31 downto 0);	-- data from MOL
	ena_cmd					: in std_logic; -- validate command
	sta_dt					: in std_logic_vector(63 downto 0);	-- value return in an ack packet
	ena_ack					: in std_logic;		
	seqnb_rcv 				: in std_logic_vector(30 downto 0);	-- seq numb from cmd (need an ack)
	card_ID_rcv				: in std_logic_vector(15 downto 0); 
	retransmit				: out std_logic;
	wr_cmd					: out std_logic;  
	func						: out std_logic_vector(31 downto 0);
	data_wr					: out std_logic_vector(31 downto 0);
	data_rd					: in std_logic_vector(63 downto 0);
	status_state			: out std_logic_vector(31 downto 0)
	);
end component;

component build_pckt_s_XGMII  
port (
	reset_CLK				: in std_logic;
	Greset_CLK				: in std_logic;
	clock						: in std_logic;
	start_pckt				: in std_logic;	-- trigger the packet send
	init_pckt				: in std_logic; -- indicates that the packet is a Init packet
	ack_pckt					: in std_logic; -- indicates that the packet is a acknoldge packet
	data_pckt				: in std_logic; -- indicates that the packet is a data packet
	data_evt					: in std_logic_vector(63 downto 0);	--data for data packet
	read_bck					: in std_logic_vector(63 downto 0); --data back for acknowledge packet
	card_ID					: in std_logic_vector(15 downto 0); -- CARD_ID
	Seq_nb					: in std_logic_vector(30 downto 0); -- sequence number
	len_pckt					: in std_logic_vector(15 downto 0); -- length of the packet (for data packet only) other 0
	cmd						: in std_logic_vector(63 downto 0); -- command bit for data packet only
	error_gen				: in std_logic_vector(3 downto 0);
	rd_dt						: out std_logic;					-- request data for data packet only
	end_pckt					: out std_logic;
	datao						: out std_logic_vector(63 downto 0); --- data and K bit send to SERDES
	k_byte					: out std_logic_vector( 7 downto 0);
	idle_state				: out std_logic;
	cnt_pckt_snd			: out std_logic_vector(31 downto 0);
	status_state			: out std_logic_vector(31 downto 0)  	);
end component;

component xaui_wd_align is
port (
	reset	: in std_logic;
	clock	: in std_logic;
	
	data_i : in std_logic_vector(63 downto 0);
	ctrl_i	: in std_logic_vector(7 downto 0);
	data_o	: out std_logic_vector(63 downto 0);
	ctrl_o	: out std_logic_vector(7 downto 0)
	);	
end component;

component rcv_pckt_s_XGMII 
port (
	reset_clk				: in std_logic;
	reset_clkT				: in std_logic;
	Greset_clk				: in std_logic;
	Greset_clkT				: in std_logic;
	clock						: in std_logic;
	clock_t					: in std_logic;
	datai						: in std_logic_vector(63 downto 0); --- data and K bit send from SERDES
	k_byte					: in std_logic_vector( 7 downto 0);
	error_gen				: in std_logic;
	cmd						: out std_logic_vector(31 downto 0);	-- command from MOL
	data						: out std_logic_vector(31 downto 0);	-- data from MOL
	ena_cmd					: out std_logic; -- validate command
	status					: out std_logic_vector(63 downto 0);	-- value return from read command request
	seqnb 					: out std_logic_vector(30 downto 0);	-- seq numb from cmd (need an ack)
	ena_ack					: out std_logic;		
	card_ID					: out std_logic_vector(15 downto 0);
	cnt_pckt_rcv			: out std_logic_vector(31 downto 0) 	);
end component;

component   reset_resync 
port (
	reset						: in std_logic;
	clock						: in std_logic; 
	Reset_sync				: out std_logic
	);
end component;

SIGNAL	data_rcv					:std_logic_vector(31 downto 0);
SIGNAL	ena_cmd					:std_logic;
SIGNAL	sta_dt					:std_logic_vector(63 downto 0);
SIGNAL	ena_ack					:std_logic;
SIGNAL	seqnb_rcv				:std_logic_vector(30 downto 0);
SIGNAL	idle_state				:std_logic;
SIGNAL	end_snd_pckt			:std_logic;
SIGNAL	rd_dt						:std_logic;
SIGNAL	cmd_rcv					:std_logic_vector(31 downto 0);
SIGNAL	cmd						:std_logic_vector(63 downto 0);
SIGNAL	len_pckt					:std_logic_vector(15 downto 0);
SIGNAL	Seq_nb					:std_logic_vector(30 downto 0);
SIGNAL	card_ID_rcv				:std_logic_vector(15 downto 0);
SIGNAL	card_ID_snd				:std_logic_vector(15 downto 0);
SIGNAL	read_bck					:std_logic_vector(63 downto 0);
SIGNAL	data_evt					:std_logic_vector(63 downto 0);
SIGNAL	data_pckt				:std_logic;
SIGNAL	ack_pckt					:std_logic;
SIGNAL	init_pckt				:std_logic;
SIGNAL	start_pckt				:std_logic;
SIGNAL	end_blk_fed				:std_logic;
SIGNAL	stop_evt					:std_logic;
SIGNAL	start_evt				:std_logic;
SIGNAL	wr_ena					:std_logic;
SIGNAL	block_sz_fed			:std_logic_vector(15 downto 0);
SIGNAL  data_fed					:std_logic_vector(63 downto 0);
SIGNAL  block_free				:std_logic;
		-- interface slave to read and write
SIGNAL  wr_cmd						: std_logic;  
SIGNAL  func						: std_logic_vector(31 downto 0);
SIGNAL  data_wr					: std_logic_vector(31 downto 0);
SIGNAL  data_rd					: std_logic_vector(63 downto 0);

SIGNAL cnt_pckt_rcv				: std_logic_vector(31 downto 0);
SIGNAL cnt_pckt_snd				: std_logic_vector(31 downto 0);
SIGNAL status_state_build_p	: std_logic_vector(31 downto 0);
SIGNAL status_state_core		: std_logic_vector(31 downto 0);
 
SIGNAL DATAo_unswapped			: std_logic_vector(31 downto 0);
SIGNAL CTRLo_unswapped			: std_logic_vector( 3 downto 0);
SIGNAL DATAi_unswapped			: std_logic_vector(31 downto 0);
SIGNAL CTRLi_unswapped			: std_logic_vector( 3 downto 0);
SIGNAL req_reset_resync			: std_logic;
SIGNAL retransmit					: std_logic;
--
SIGNAL reg_datai					: std_logic_vector(63 downto 0);
SIGNAL req_uctrli					: std_logic;
SIGNAL req_weni					: std_logic;

SIGNAL G_reset						: std_logic;


SIGNAL GRst_sysckl				: std_logic;
SIGNAL GRst_T_ckl					: std_logic;
SIGNAL GRst_R_ckl					: std_logic;
SIGNAL Rst_sysckl					: std_logic;
SIGNAL Rst_T_ckl					: std_logic;
SIGNAL Rst_R_ckl					: std_logic;
SIGNAL LINKDown_s					: std_logic;

signal datai_aligned				: std_logic_vector(63 downto 0);
signal k_byte_aligned			: std_logic_vector(7 downto 0);

--******************************************************************
--*******************  BEGIN  **************************************
--******************************************************************
begin
--#################################################
-- reset resync
--#################################################
-- reset coming form FED
Rst_sysckl_i1:reset_resync 
port map(
	reset				=> reset,
	clock				=> SYS_CLK,
	Reset_sync		=> Rst_sysckl
	);

Rst_T_ckl_i1:reset_resync 
port map(
	reset				=> reset,
	clock				=> clock,
	Reset_sync		=> Rst_T_ckl
	);
	
Rst_R_ckl_i1:reset_resync 
port map(
	reset				=> reset,
	clock				=> clock_r,
	Reset_sync		=> Rst_R_ckl
	);	
-- reset coming form FED or from DAQ
G_reset		<= '0' when	reset = '0' or req_reset_resync = '0' else '1';	
	
GRst_sysckl_i1:reset_resync 
port map(
	reset				=> G_reset,
	clock				=> SYS_CLK,
	Reset_sync		=> GRst_sysckl
	);

GRst_T_ckl_i1:reset_resync 
port map(
	reset				=> G_reset,
	clock				=> clock,
	Reset_sync		=> GRst_T_ckl
	);
	
GRst_R_ckl_i1:reset_resync 
port map(
	reset				=> G_reset,
	clock				=> clock_r,
	Reset_sync		=> GRst_R_ckl
	);	
--#################################################
-- registers FED interface 
--#################################################
process(LINKDown_s,SYS_CLK)
begin
	if LINKDown_s = '0' then
		req_weni				<= '1';
	elsif rising_edge(SYS_CLK) then
		reg_datai			<= LINKData;
		req_uctrli			<= LINKCtrl;
		req_weni				<= LINKWe;
	end if;
end process;

LINKDown 					<= LINKDown_s;
i1:fed_itf
port map(
	reset_sysCLK			=> Rst_sysckl,
	Greset_sysCLK			=> GRst_sysckl,
	sys_clk					=> SYS_CLK,
	LinkWe 					=> req_weni,
	LinkCtrl 				=> req_uctrli,
	LinkData 				=> reg_datai,
	LinkAlmostFull			=> LINK_LFF,
	LinkDown 				=> LINKDown_s,
	src_ID 					=> src_ID,
	inject_err 				=> inject_err,
	read_ce 					=> read_CE,
	addr 						=> Addr,
	status_data 			=> status_data,
	reset_CLK				=> Rst_T_ckl,
	Greset_CLK				=> GRst_T_ckl,
	clock						=> clock,
	block_free				=> block_free,
	data_fed					=> data_fed,
	block_sz_fed			=> block_sz_fed,
	wr_ena					=> wr_ena,
	start_evt				=> start_evt,
	stop_evt					=> stop_evt,
	end_blk_fed				=> end_blk_fed,
	wr_cmd					=> wr_cmd,  
	func						=> func,
	data_wr					=> data_wr,
	data_rd					=> data_rd,
	--cnt_evt					=> cnt_evt,
	cnt_pckt_rcv			=> cnt_pckt_rcv,
	cnt_pckt_snd			=> cnt_pckt_snd,
	retransmit_ena			=> retransmit,
	status_state_build_p	=> status_state_build_p,
	status_state_core		=> status_state_core,
	Serdes_status			=> Serdes_status	);

i2:Core_logic  
port map(
	reset_clk				=> Rst_T_ckl,
	Greset_clk				=> GRst_T_ckl,
	clock						=> clock,
		-- interface from the FED block	
	data_fed					=> data_fed,
	wen						=> wr_ena,
	start_evt				=> start_evt,
	stop_evt					=> stop_evt,
	block_sz_fed			=> block_sz_fed,
	end_blk_fed				=> end_blk_fed,
	block_free				=> block_free,
	src_ID					=> src_ID,
	req_reset_resync		=> req_reset_resync,
		-- interface to the SERDES OUT (send part)
	start_pckt				=> start_pckt,
	init_pckt				=> init_pckt,
	ack_pckt					=> ack_pckt,
	data_pckt				=> data_pckt,
	data_evt					=> data_evt,
	status					=> read_bck,
	card_ID					=> card_ID_snd,
	Seq_nb					=> Seq_nb,
	len_pckt					=> len_pckt,
	cmd						=> cmd,
	rd_dt						=> rd_dt,
	end_snd_pckt			=> end_snd_pckt,
	idle_state				=> idle_state,
	serdes_init				=> serdes_init,
		-- interface to the SERDES IN (receiver part)
	cmd_rcv					=> cmd_rcv,
	data_rcv					=> data_rcv,
	ena_cmd					=> ena_cmd,
	sta_dt					=> sta_dt,
	ena_ack					=> ena_ack,
	seqnb_rcv 				=> seqnb_rcv,
	card_ID_rcv				=> card_ID_rcv,
	retransmit				=> retransmit,
	wr_cmd					=> wr_cmd,  
	func						=> func,
	data_wr					=> data_wr,
	data_rd					=> data_rd,
	status_state			=> status_state_core	); 

i3:build_pckt_s_XGMII  
port map(
	reset_CLK				=> Rst_T_ckl,
	Greset_CLK				=> GRst_T_ckl,
	clock						=> clock,	
	start_pckt				=> start_pckt,
	init_pckt				=> init_pckt,
	ack_pckt					=> ack_pckt,
	data_pckt				=> data_pckt,
	data_evt					=> data_evt,
	read_bck					=> read_bck,
	card_ID					=> card_ID_snd,
	Seq_nb					=> Seq_nb,
	len_pckt					=> len_pckt,
	cmd						=> cmd,
	error_gen				=> inject_err(4 downto 1),
	rd_dt						=> rd_dt,
	end_pckt					=> end_snd_pckt,
					
	datao						=> SD_Data_o,
	k_byte					=> SD_Kb_o,
	idle_state				=> idle_state,
	status_state			=> status_state_build_p,
	cnt_pckt_snd			=> cnt_pckt_snd	);

i0:xaui_wd_align  
port map(
	reset		=> GRst_T_ckl,
	clock		=> clock,
	
	data_i 	=> SD_Data_i,
	ctrl_i	=> SD_Kb_i,
	data_o	=> datai_aligned, 
	ctrl_o	=> k_byte_aligned 
	);		
	
i4:rcv_pckt_s_XGMII
port map(
	reset_clk				=> Rst_R_ckl,
	reset_clkT				=> Rst_T_ckl,
	Greset_clk				=> GRst_R_ckl,
	Greset_clkT				=> GRst_T_ckl,
	clock						=> clock_r,
	clock_t					=> clock,
	datai						=> datai_aligned,
	k_byte					=> k_byte_aligned,
	error_gen				=> inject_err(5),
	cmd						=> cmd_rcv,
	data						=> data_rcv,
	ena_cmd					=> ena_cmd,
	status					=> sta_dt,
	ena_ack					=> ena_ack,
	seqnb 					=> seqnb_rcv,
	card_ID					=> card_ID_rcv,
	cnt_pckt_rcv			=> cnt_pckt_rcv	);
	
	
end behavioral;


