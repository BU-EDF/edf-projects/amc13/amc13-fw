----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:05:44 12/07/2015 
-- Design Name: 
-- Module Name:    XGMII_serdes_wapper - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity XGMII_serdes_wapper is
		generic(N_SFP : integer := 3);
    Port (
           DRPclk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           gtx_reset : in  STD_LOGIC;
           sfp_pd : in  array3x2;
-- SFP ports
           SFP0_RXN : in  STD_LOGIC;
           SFP0_RXP : in  STD_LOGIC;
           SFP1_RXN : in  STD_LOGIC;
           SFP1_RXP : in  STD_LOGIC;
           SFP2_RXN : in  STD_LOGIC;
           SFP2_RXP : in  STD_LOGIC;
           SFP0_TXN : out  STD_LOGIC;
           SFP0_TXP : out  STD_LOGIC;
           SFP1_TXN : out  STD_LOGIC;
           SFP1_TXP : out  STD_LOGIC;
           SFP2_TXN : out  STD_LOGIC;
           SFP2_TXP : out  STD_LOGIC;
           SFP_REFCLK_P : in  STD_LOGIC;
           SFP_REFCLK_N : in  STD_LOGIC;
           clk156 : out  STD_LOGIC;
					 PCS_lock : out  STD_LOGIC_VECTOR(2 downto 0);
					 gtx_rxresetdone : out  STD_LOGIC_VECTOR(2 downto 0);
           xgmii_txd : in  array3x64;
           xgmii_txc : in  array3x8;
           xgmii_rxd : out  array3x64;
           xgmii_rxc : out  array3x8);
end XGMII_serdes_wapper;

architecture Behavioral of XGMII_serdes_wapper is
COMPONENT XGbEPCS32
	PORT(
			 reset : IN  std_logic;
			 clk2x : IN  std_logic;
			 clk : IN  std_logic;
			 TXUSRCLK : IN  std_logic;
			 TX_high : IN  std_logic;
			 RXUSRCLK : IN  std_logic;
			 RXRESETDONE : IN std_logic;
			 inh_TX : IN std_logic;
			 RESET_TXSync : IN std_logic;
			 GTX_TXD : OUT  std_logic_vector(31 downto 0);
			 GTX_TXHEADER : OUT  std_logic_vector(1 downto 0);
			 GTX_TX_PAUSE : IN  std_logic;
			 GTX_RXD : IN  std_logic_vector(31 downto 0);
			 GTX_RXDVLD : IN  std_logic;
			 GTX_RXHEADER : IN  std_logic_vector(1 downto 0);
			 GTX_RXHEADERVLD : IN  std_logic;
			 GTX_RXGOOD : OUT  std_logic;
			 GTX_RXGEARBOXSLIP_OUT : OUT  std_logic;
			 EmacPhyTxC : IN  std_logic_vector(3 downto 0);
			 EmacPhyTxD : IN  std_logic_vector(31 downto 0);
			 PhyEmacRxC : OUT  std_logic_vector(3 downto 0);
			 PhyEmacRxD : OUT  std_logic_vector(31 downto 0)
			);
END COMPONENT;
component SFP3_v2_7_init
generic
(
		N_SFP																		: integer		:= 3;
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";          -- simulation setting for GT SecureIP model
    EXAMPLE_SIMULATION                      : integer   := 0;               -- Set to 1 for simulation
    STABLE_CLOCK_PERIOD                     : integer   := 20;               --Period of the stable clock driving this state-machine, unit is [ns]
    EXAMPLE_USE_CHIPSCOPE                   : integer   := 0                -- Set to 1 to use Chipscope to drive resets

);
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_IN                           : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_DATA_VALID_IN                       : in   std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y12)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT0_DRPCLK_IN                           : in   std_logic;
    GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT0_DRPEN_IN                            : in   std_logic;
    GT0_DRPRDY_OUT                          : out  std_logic;
    GT0_DRPWE_IN                            : in   std_logic;
    ------------------------------- Loopback Ports -----------------------------
    GT0_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ----------------------------
    GT0_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT0_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT0_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT0_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT0_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT0_RXUSRCLK_IN                         : in   std_logic;
    GT0_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT0_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT0_RXPRBSERR_OUT                       : out  std_logic;
    GT0_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT0_RXPRBSCNTRESET_IN                   : in   std_logic;
    --------------------------- Receive Ports - RX AFE -------------------------
    GT0_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT0_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT0_RXBUFRESET_IN                       : in   std_logic;
    GT0_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    GT0_RXOUTCLK_OUT                        : out  std_logic;
    ---------------------- Receive Ports - RX Gearbox Ports --------------------
    GT0_RXDATAVALID_OUT                     : out  std_logic;
    GT0_RXHEADER_OUT                        : out  std_logic_vector(1 downto 0);
    GT0_RXHEADERVALID_OUT                   : out  std_logic;
    --------------------- Receive Ports - RX Gearbox Ports  --------------------
    GT0_RXGEARBOXSLIP_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT0_GTRXRESET_IN                        : in   std_logic;
    GT0_RXPMARESET_IN                       : in   std_logic;
    ------------------ Receive Ports - RX Margin Analysis ports ----------------
    GT0_RXLPMEN_IN                          : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT0_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT0_GTTXRESET_IN                        : in   std_logic;
    GT0_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT0_TXUSRCLK_IN                         : in   std_logic;
    GT0_TXUSRCLK2_IN                        : in   std_logic;
    --------------- Transmit Ports - TX Configurable Driver Ports --------------
    GT0_TXDIFFCTRL_IN                       : in   std_logic_vector(3 downto 0);
    GT0_TXINHIBIT_IN                        : in   std_logic;
    GT0_TXMAINCURSOR_IN                     : in   std_logic_vector(6 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT0_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT0_GTXTXN_OUT                          : out  std_logic;
    GT0_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT0_TXOUTCLK_OUT                        : out  std_logic;
    GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT0_TXHEADER_IN                         : in   std_logic_vector(1 downto 0);
    GT0_TXSEQUENCE_IN                       : in   std_logic_vector(6 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT0_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT0_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

    --GT1  (X1Y13)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT1_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT1_DRPCLK_IN                           : in   std_logic;
    GT1_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT1_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT1_DRPEN_IN                            : in   std_logic;
    GT1_DRPRDY_OUT                          : out  std_logic;
    GT1_DRPWE_IN                            : in   std_logic;
    ------------------------------- Loopback Ports -----------------------------
    GT1_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ----------------------------
    GT1_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT1_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT1_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT1_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT1_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT1_RXUSRCLK_IN                         : in   std_logic;
    GT1_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT1_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT1_RXPRBSERR_OUT                       : out  std_logic;
    GT1_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT1_RXPRBSCNTRESET_IN                   : in   std_logic;
    --------------------------- Receive Ports - RX AFE -------------------------
    GT1_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT1_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT1_RXBUFRESET_IN                       : in   std_logic;
    GT1_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    GT1_RXOUTCLK_OUT                        : out  std_logic;
    ---------------------- Receive Ports - RX Gearbox Ports --------------------
    GT1_RXDATAVALID_OUT                     : out  std_logic;
    GT1_RXHEADER_OUT                        : out  std_logic_vector(1 downto 0);
    GT1_RXHEADERVALID_OUT                   : out  std_logic;
    --------------------- Receive Ports - RX Gearbox Ports  --------------------
    GT1_RXGEARBOXSLIP_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT1_GTRXRESET_IN                        : in   std_logic;
    GT1_RXPMARESET_IN                       : in   std_logic;
    ------------------ Receive Ports - RX Margin Analysis ports ----------------
    GT1_RXLPMEN_IN                          : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT1_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT1_GTTXRESET_IN                        : in   std_logic;
    GT1_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT1_TXUSRCLK_IN                         : in   std_logic;
    GT1_TXUSRCLK2_IN                        : in   std_logic;
    --------------- Transmit Ports - TX Configurable Driver Ports --------------
    GT1_TXDIFFCTRL_IN                       : in   std_logic_vector(3 downto 0);
    GT1_TXINHIBIT_IN                        : in   std_logic;
    GT1_TXMAINCURSOR_IN                     : in   std_logic_vector(6 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT1_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT1_GTXTXN_OUT                          : out  std_logic;
    GT1_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT1_TXOUTCLK_OUT                        : out  std_logic;
    GT1_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT1_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT1_TXHEADER_IN                         : in   std_logic_vector(1 downto 0);
    GT1_TXSEQUENCE_IN                       : in   std_logic_vector(6 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT1_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT1_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

    --GT2  (X1Y14)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT2_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT2_DRPCLK_IN                           : in   std_logic;
    GT2_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT2_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT2_DRPEN_IN                            : in   std_logic;
    GT2_DRPRDY_OUT                          : out  std_logic;
    GT2_DRPWE_IN                            : in   std_logic;
    ------------------------------- Loopback Ports -----------------------------
    GT2_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ----------------------------
    GT2_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT2_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT2_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT2_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT2_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT2_RXUSRCLK_IN                         : in   std_logic;
    GT2_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT2_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT2_RXPRBSERR_OUT                       : out  std_logic;
    GT2_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT2_RXPRBSCNTRESET_IN                   : in   std_logic;
    --------------------------- Receive Ports - RX AFE -------------------------
    GT2_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT2_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT2_RXBUFRESET_IN                       : in   std_logic;
    GT2_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    GT2_RXOUTCLK_OUT                        : out  std_logic;
    ---------------------- Receive Ports - RX Gearbox Ports --------------------
    GT2_RXDATAVALID_OUT                     : out  std_logic;
    GT2_RXHEADER_OUT                        : out  std_logic_vector(1 downto 0);
    GT2_RXHEADERVALID_OUT                   : out  std_logic;
    --------------------- Receive Ports - RX Gearbox Ports  --------------------
    GT2_RXGEARBOXSLIP_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT2_GTRXRESET_IN                        : in   std_logic;
    GT2_RXPMARESET_IN                       : in   std_logic;
    ------------------ Receive Ports - RX Margin Analysis ports ----------------
    GT2_RXLPMEN_IN                          : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT2_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT2_GTTXRESET_IN                        : in   std_logic;
    GT2_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT2_TXUSRCLK_IN                         : in   std_logic;
    GT2_TXUSRCLK2_IN                        : in   std_logic;
    --------------- Transmit Ports - TX Configurable Driver Ports --------------
    GT2_TXDIFFCTRL_IN                       : in   std_logic_vector(3 downto 0);
    GT2_TXINHIBIT_IN                        : in   std_logic;
    GT2_TXMAINCURSOR_IN                     : in   std_logic_vector(6 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT2_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT2_GTXTXN_OUT                          : out  std_logic;
    GT2_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT2_TXOUTCLK_OUT                        : out  std_logic;
    GT2_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT2_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT2_TXHEADER_IN                         : in   std_logic_vector(1 downto 0);
    GT2_TXSEQUENCE_IN                       : in   std_logic_vector(6 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT2_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT2_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);


    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    GT0_GTREFCLK0_COMMON_IN                 : in   std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    GT0_QPLLLOCK_OUT                        : out  std_logic;
    GT0_QPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_QPLLRESET_IN                        : in   std_logic


);
end component;
signal gtx_resetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_REFCLK : std_logic := '0';
signal REFCLK2XPLLRST : std_logic := '0';
signal refclk2x_in : std_logic := '0';
signal ClientClk2x_dcm : std_logic := '0';
signal ClientClk2x : std_logic := '0';
signal ClientClk_dcm : std_logic := '0';
signal ClientClk : std_logic := '0';
signal ClientClk_lock : std_logic := '0';
signal txusrclk : std_logic := '0';
signal TX_high : std_logic := '0';
signal qplllock : std_logic := '0';
signal qpllreset : std_logic := '0';
signal TXSEQ_cntr : std_logic_vector(6 downto 0) := (others => '0');
signal inh_TX : std_logic_vector(2 downto 0) := (others => '0');
signal inh_TX_q : std_logic_vector(2 downto 0) := (others => '0');
signal reset_TXSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal ClientClkToggle : std_logic := '0';
signal ClientClkToggle_q : std_logic := '0';
signal SFP_TXOUTCLK : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_TXSEQUENCE : array3X7 := (others => (others => '0'));
signal SFP_rxoutclk : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxusrclk : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_txresetdone : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_txuserrdy : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxresetdone : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxuserrdy : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_drprdy : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_drpen : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_drpwe : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxdfeagchold : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_adapt_done : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxmonitor : array3X7 := (others => (others => '0'));
signal SFP_drpdo : array3X16 := (others => (others => '0'));
signal SFP_rxmonitorsel : array3X2 := (others => (others => '0'));
signal SFP_drpaddr : array3X9 := (others => (others => '0'));
signal SFP_drpdi : array3X16 := (others => (others => '0'));
signal GTX_TX_PAUSE : std_logic := '0';
signal SFP_LOOPBACK_IN : array3X3 := (others => (others => '0'));
signal SFP_RX_FSM_RESET_DONE : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_TX_FSM_RESET_DONE : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXDVLD : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXHEADERVLD : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXGEARBOXSLIP : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXPRBSERR_OUT : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXPRBSSEL_IN : array3X3 := (others => (others => '0'));
signal SFP_TXPRBSSEL_IN : array3X3 := (others => (others => '0'));
signal SFP_EYESCANDATAERROR_OUT : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXGOOD : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_TXD : array3X32 := (others => (others => '0'));
signal SFP_TXD_inv : array3X32 := (others => (others => '0'));
signal SFP_TXHEADER : array3X2 := (others => (others => '0'));
signal SFP_RXD : array3X32 := (others => (others => '0'));
signal SFP_RXD_inv : array3X32 := (others => (others => '0'));
signal SFP_RXHEADER : array3X2 := (others => (others => '0'));
signal SFP_EmacPhyTxD : array3X32 := (others => (others => '0'));
signal SFP_EmacPhyTxC : array3X4 := (others => (others => '0'));
signal SFP_PhyEmacRxD : array3X32 := (others => (others => '0'));
signal SFP_PhyEmacRxC : array3X4 := (others => (others => '0'));
signal SFP_PhyEmacRxD_q : array3X32 := (others => (others => '0'));
signal SFP_PhyEmacRxC_q : array3X4 := (others => (others => '0'));
signal xgmii_rxd_i : array3X64 := (others => (others => '0'));
signal xgmii_rxc_i : array3X8 := (others => (others => '0'));
signal SFP_pd_q : array3X4 := (others => (others => '0'));
signal soft_reset : std_logic := '0';
signal reset_cntr20_q : std_logic := '0';
signal reset_cntr : std_logic_vector(20 downto 0) := (others => '0');

COMPONENT chipscope1
	PORT(
		clk : IN std_logic;
		Din : IN std_logic_vector(303 downto 0)       
		);
END COMPONENT;
signal cs : std_logic_vector(303 downto 0) := (others => '0');

begin
--i_chipscope1: chipscope1 PORT MAP(
--		clk => ClientClk2X,
--		Din => cs
--	);
-- trigger starts at cs(288)
process(ClientClk)
begin
	if(ClientClk'event and ClientClk = '1')then
		if(xgmii_rxc_i(0)(0) = '1' and xgmii_rxd_i(0)(7 downto 0) = x"fb")then
			cs(288) <= '1';
		elsif(xgmii_rxc_i(0)(4) = '1' and xgmii_rxd_i(0)(39 downto 32) = x"fb")then
			cs(288) <= '1';
		else
			cs(288) <= '0';
		end if;
	end if;
end process;
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		if(SFP_EmacPhyTxc(0)(0) = '1' and SFP_EmacPhyTxd(0)(7 downto 0) = x"fb")then
			cs(289) <= '1';
		else
			cs(289) <= '0';
		end if;
		if(SFP_EmacPhyTxc(0)(0) = '1' and SFP_EmacPhyTxd(0)(7 downto 0) = x"fe")then
			cs(290) <= '1';
		elsif(SFP_EmacPhyTxc(0)(1) = '1' and SFP_EmacPhyTxd(0)(15 downto 8) = x"fe")then
			cs(290) <= '1';
		elsif(SFP_EmacPhyTxc(0)(2) = '1' and SFP_EmacPhyTxd(0)(23 downto 16) = x"fe")then
			cs(290) <= '1';
		elsif(SFP_EmacPhyTxc(0)(3) = '1' and SFP_EmacPhyTxd(0)(31 downto 24) = x"fe")then
			cs(290) <= '1';
		else
			cs(290) <= '0';
		end if;
	end if;
end process;


-- chipscope data
cs(63 downto 0) <= xgmii_rxd_i(0);
cs(71 downto 64) <= xgmii_rxc_i(0);
cs(135 downto 72) <= xgmii_txd(0);
cs(143 downto 136) <= xgmii_txc(0);
cs(175 downto 144) <= SFP_EmacPhyTxd(0);
cs(179 downto 176) <= SFP_EmacPhyTxc(0);
cs(211 downto 180) <= SFP_PhyEmacRxd(0);
cs(215 downto 212) <= SFP_PhyEmacRxc(0);





xgmii_rxd <= xgmii_rxd_i;
xgmii_rxc <= xgmii_rxc_i;
clk156 <= ClientClk;
gtx_rxresetdone <= SFP_RXRESETDONE;
process(ClientClk,gtx_reset,ClientClk_lock)
begin
	if(gtx_reset = '1' or ClientClk_lock = '0')then
		gtx_resetSyncRegs <= (others => '1');
	elsif(ClientClk'event and ClientClk = '1')then
		gtx_resetSyncRegs <= gtx_resetSyncRegs(1 downto 0) & '0';
	end if;
end process;
process(txusrclk)
begin
	if(txusrclk'event and txusrclk = '1')then
		if(TXSEQ_cntr = "1000001")then
			TXSEQ_cntr <= (others => '0');
		else
			TXSEQ_cntr <= TXSEQ_cntr + 1;
		end if;
		if(TXSEQ_cntr(0) = '1')then
			GTX_TX_PAUSE <= and_reduce(TXSEQ_cntr(5 downto 1));
		end if;
		if(inh_TX(0) = '1')then
			SFP_TXSEQUENCE(0) <= (others => '0');
		elsif(TXSEQ_cntr(0) = '1')then
			SFP_TXSEQUENCE(0) <= '0' & TXSEQ_cntr(6 downto 1);
		end if;
		if(inh_TX(1) = '1')then
			SFP_TXSEQUENCE(1) <= (others => '0');
		elsif(TXSEQ_cntr(0) = '1')then
			SFP_TXSEQUENCE(1) <= '0' & TXSEQ_cntr(6 downto 1);
		end if;
		if(inh_TX(2) = '1')then
			SFP_TXSEQUENCE(2) <= (others => '0');
		elsif(TXSEQ_cntr(0) = '1')then
			SFP_TXSEQUENCE(2) <= '0' & TXSEQ_cntr(6 downto 1);
		end if;
	end if;
end process;
process(TXUSRCLK,gtx_reset)
begin
	if(gtx_reset = '1')then
		reset_TXSyncRegs <= (others => '1');
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		reset_TXSyncRegs <= reset_TXSyncRegs(1 downto 0) & '0';
	end if;
end process;
process(TXUSRCLK,SFP_TX_FSM_RESET_DONE(0))
begin
	if(SFP_TX_FSM_RESET_DONE(0) = '0')then
		inh_TX(0) <= '1';
		inh_TX_q(0) <= '1';
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		if(TXSEQ_cntr(0) = '1' and TXSEQ_cntr(6) = '1')then
			inh_TX(0) <= '0';
		end if;
		inh_TX_q(0) <= inh_TX(0);
	end if;
end process;
process(TXUSRCLK,SFP_TX_FSM_RESET_DONE(1))
begin
	if(SFP_TX_FSM_RESET_DONE(1) = '0')then
		inh_TX(1) <= '1';
		inh_TX_q(1) <= '1';
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		if(TXSEQ_cntr(0) = '1' and TXSEQ_cntr(6) = '1')then
			inh_TX(1) <= '0';
		end if;
		inh_TX_q(1) <= inh_TX(1);
	end if;
end process;
process(TXUSRCLK,SFP_TX_FSM_RESET_DONE(2))
begin
	if(SFP_TX_FSM_RESET_DONE(2) = '0')then
		inh_TX(2) <= '1';
		inh_TX_q(2) <= '1';
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		if(TXSEQ_cntr(0) = '1' and TXSEQ_cntr(6) = '1')then
			inh_TX(2) <= '0';
		end if;
		inh_TX_q(2) <= inh_TX(2);
	end if;
end process;
g_XGbEPCS : for i in 0 to 2 generate
	i_XGbEPCS: XGbEPCS32 PORT MAP (
		reset => gtx_resetSyncRegs(2),
		clk2x => ClientClk2X,
		clk => ClientClk,
		TXUSRCLK => txusrclk,
		TX_high => TX_high,
		RXUSRCLK => SFP_RXUSRCLK(i),
		RXRESETDONE => SFP_RXRESETDONE(i),
		inh_TX => inh_TX(i),
		RESET_TXSync => reset_TXSyncRegs(2),
		GTX_RXGEARBOXSLIP_OUT => SFP_RXGEARBOXSLIP(i),
		GTX_TXD => SFP_TXD(i),
		GTX_TXHEADER => SFP_TXHEADER(i),
		GTX_TX_PAUSE => GTX_TX_PAUSE,
		GTX_RXD => SFP_RXD(i),
		GTX_RXDVLD => SFP_RXDVLD(i),
		GTX_RXHEADER => SFP_RXHEADER(i),
		GTX_RXHEADERVLD => SFP_RXHEADERVLD(i),
		GTX_RXGOOD => SFP_RXGOOD(i),
		EmacPhyTxC => SFP_EmacPhyTxc(i),
		EmacPhyTxD => SFP_EmacPhyTxd(i),
		PhyEmacRxC => SFP_PhyEmacRxC(i),
		PhyEmacRxD => SFP_PhyEmacRxD(i)
	);
end generate;
process(ClientClk)
begin
	if(ClientClk'event and ClientClk = '1')then
		ClientClkToggle <= not ClientClkToggle;
		for i in 0 to 2 loop
			xgmii_rxd_i(i) <= SFP_PhyEmacRxd(i) & SFP_PhyEmacRxd_q(i);
			xgmii_rxc_i(i) <= SFP_PhyEmacRxc(i) & SFP_PhyEmacRxc_q(i);
		end loop;
	end if;
end process;
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		ClientClkToggle_q <= ClientClkToggle;
		SFP_PhyEmacRxd_q <= SFP_PhyEmacRxd;
		SFP_PhyEmacRxc_q <= SFP_PhyEmacRxc;
		TX_high <= ClientClkToggle_q xnor ClientClkToggle;
		for i in 0 to 2 loop
			if(TX_high = '1')then
				SFP_EmacPhyTxd(i) <= xgmii_txd(i)(31 downto 0);
				SFP_EmacPhyTxc(i) <= xgmii_txc(i)(3 downto 0);
			else
				SFP_EmacPhyTxd(i) <= xgmii_txd(i)(63 downto 32);
				SFP_EmacPhyTxc(i) <= xgmii_txc(i)(7 downto 4);
			end if;
		end loop;
	end if;
end process;
process(DRPclk)
begin
	if(DRPclk'event and DRPclk = '1')then
		for i in 0 to 2 loop
			SFP_pd_q(i) <= SFP_pd_q(i)(2 downto 0) & SFP_pd(i)(0);
		end loop;
		if(SFP_pd_q(0)(3 downto 2) = "10" or SFP_pd_q(1)(3 downto 2) = "10" or SFP_pd_q(2)(3 downto 2) = "10")then
			reset_cntr <= (others => '0');
		elsif(reset_cntr(20) = '0')then
			reset_cntr <= reset_cntr + 1;
		end if;
		reset_cntr20_q <= reset_cntr(20);
		soft_reset <= not reset_cntr20_q and reset_cntr(20);
	end if;
end process;
i_SFP3_init : SFP3_v2_7_init
		generic map(N_SFP => N_SFP)
    port map
    (
        SYSCLK_IN                       =>      DRPclk,
        SOFT_RESET_IN                   =>      soft_reset,
        DONT_RESET_ON_DATA_ERROR_IN     =>      '0',
        GT0_TX_FSM_RESET_DONE_OUT       =>      SFP_TX_FSM_RESET_DONE(0),
        GT0_RX_FSM_RESET_DONE_OUT       =>      PCS_lock(0),
        GT0_DATA_VALID_IN               =>      SFP_RXGOOD(0),
        GT1_TX_FSM_RESET_DONE_OUT       =>      SFP_TX_FSM_RESET_DONE(1),
        GT1_RX_FSM_RESET_DONE_OUT       =>      PCS_lock(1),
        GT1_DATA_VALID_IN               =>      SFP_RXGOOD(1),
        GT2_TX_FSM_RESET_DONE_OUT       =>      SFP_TX_FSM_RESET_DONE(2),
        GT2_RX_FSM_RESET_DONE_OUT       =>      PCS_lock(2),
        GT2_DATA_VALID_IN               =>      SFP_RXGOOD(2),

  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X1Y12)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT0_DRPADDR_IN                  => SFP_drpaddr(0),
        GT0_DRPCLK_IN                   => DRPclk,
        GT0_DRPDI_IN                    => SFP_drpdi(0),
        GT0_DRPDO_OUT                   => SFP_drpdo(0),
        GT0_DRPEN_IN                    => SFP_drpen(0),
        GT0_DRPRDY_OUT                  => SFP_drprdy(0),
        GT0_DRPWE_IN                    => SFP_drpwe(0),
        ------------------------------- Loopback Ports -----------------------------
        GT0_LOOPBACK_IN                 => SFP_LOOPBACK_IN(0),
        ------------------------------ Power-Down Ports ----------------------------
        GT0_RXPD_IN                     => SFP_pd(0),
        GT0_TXPD_IN                     => SFP_pd(0),
        --------------------- RX Initialization and Reset Ports --------------------
        GT0_RXUSERRDY_IN                => SFP_rxuserrdy(0),
        -------------------------- RX Margin Analysis Ports ------------------------
        GT0_EYESCANDATAERROR_OUT        => SFP_EYESCANDATAERROR_OUT(0),
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT0_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT0_RXUSRCLK_IN                 => SFP_RXUSRCLK(0),
        GT0_RXUSRCLK2_IN                => SFP_RXUSRCLK(0),
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT0_RXDATA_OUT                  => SFP_RXD_inv(0),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT0_RXPRBSERR_OUT               => SFP_RXPRBSERR_OUT(0),
        GT0_RXPRBSSEL_IN                => SFP_RXPRBSSEL_IN(0),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT0_RXPRBSCNTRESET_IN           =>      '0',
        --------------------------- Receive Ports - RX AFE -------------------------
        GT0_GTXRXP_IN                   => SFP0_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT0_GTXRXN_IN                   => SFP0_RXN,
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT0_RXBUFRESET_IN               => '0',
        GT0_RXBUFSTATUS_OUT             => open,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        GT0_RXOUTCLK_OUT                => SFP_rxoutclk(0),
        ---------------------- Receive Ports - RX Gearbox Ports --------------------
        GT0_RXDATAVALID_OUT             => SFP_RXDVLD(0),
        GT0_RXHEADER_OUT                => SFP_RXHEADER(0),
        GT0_RXHEADERVALID_OUT           => SFP_RXHEADERVLD(0),
        --------------------- Receive Ports - RX Gearbox Ports  --------------------
        GT0_RXGEARBOXSLIP_IN            => SFP_RXGEARBOXSLIP(0),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT0_GTRXRESET_IN                => '0',
        GT0_RXPMARESET_IN               => '0',
        ------------------ Receive Ports - RX Margin Analysis ports ----------------
        GT0_RXLPMEN_IN                  => '0',
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT0_RXRESETDONE_OUT             => SFP_rxresetdone(0),
        --------------------- TX Initialization and Reset Ports --------------------
        GT0_GTTXRESET_IN                => '0',
        GT0_TXUSERRDY_IN                => SFP_txuserrdy(0),
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT0_TXUSRCLK_IN                 => txusrclk,
        GT0_TXUSRCLK2_IN                => txusrclk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT0_TXDIFFCTRL_IN               => "1110",
        GT0_TXINHIBIT_IN                => '0',
        GT0_TXMAINCURSOR_IN             => (others => '0'),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_TXDATA_IN                   => SFP_TXD_inv(0),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  => SFP0_TXN,
        GT0_GTXTXP_OUT                  => SFP0_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT0_TXOUTCLK_OUT                => SFP_TXOUTCLK(0),
        GT0_TXOUTCLKFABRIC_OUT          => open,
        GT0_TXOUTCLKPCS_OUT             => open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT0_TXHEADER_IN                 => SFP_TXHEADER(0),
        GT0_TXSEQUENCE_IN               => SFP_TXSEQUENCE(0),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT0_TXRESETDONE_OUT             => SFP_txresetdone(0),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT0_TXPRBSSEL_IN                => SFP_TXPRBSSEL_IN(0),


  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1  (X1Y13)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT1_DRPADDR_IN                  => SFP_drpaddr(1),
        GT1_DRPCLK_IN                   => DRPclk,
        GT1_DRPDI_IN                    => SFP_drpdi(1),
        GT1_DRPDO_OUT                   => SFP_drpdo(1),
        GT1_DRPEN_IN                    => SFP_drpen(1),
        GT1_DRPRDY_OUT                  => SFP_drprdy(1),
        GT1_DRPWE_IN                    => SFP_drpwe(1),
        ------------------------------- Loopback Ports -----------------------------
        GT1_LOOPBACK_IN                 => SFP_LOOPBACK_IN(1),
        ------------------------------ Power-Down Ports ----------------------------
        GT1_RXPD_IN                     => SFP_pd(1),
        GT1_TXPD_IN                     => SFP_pd(1),
        --------------------- RX Initialization and Reset Ports --------------------
        GT1_RXUSERRDY_IN                => SFP_rxuserrdy(1),
        -------------------------- RX Margin Analysis Ports ------------------------
        GT1_EYESCANDATAERROR_OUT        => SFP_EYESCANDATAERROR_OUT(1),
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT1_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT1_RXUSRCLK_IN                 => SFP_RXUSRCLK(1),
        GT1_RXUSRCLK2_IN                => SFP_RXUSRCLK(1),
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT1_RXDATA_OUT                  => SFP_RXD_inv(1),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT1_RXPRBSERR_OUT               => SFP_RXPRBSERR_OUT(1),
        GT1_RXPRBSSEL_IN                => SFP_RXPRBSSEL_IN(1),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT1_RXPRBSCNTRESET_IN           =>      '0',
        --------------------------- Receive Ports - RX AFE -------------------------
        GT1_GTXRXP_IN                   => SFP1_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT1_GTXRXN_IN                   => SFP1_RXN,
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT1_RXBUFRESET_IN               => '0',
        GT1_RXBUFSTATUS_OUT             => open,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        GT1_RXOUTCLK_OUT                => SFP_rxoutclk(1),
        ---------------------- Receive Ports - RX Gearbox Ports --------------------
        GT1_RXDATAVALID_OUT             => SFP_RXDVLD(1),
        GT1_RXHEADER_OUT                => SFP_RXHEADER(1),
        GT1_RXHEADERVALID_OUT           => SFP_RXHEADERVLD(1),
        --------------------- Receive Ports - RX Gearbox Ports  --------------------
        GT1_RXGEARBOXSLIP_IN            => SFP_RXGEARBOXSLIP(1),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT1_GTRXRESET_IN                => '0',
        GT1_RXPMARESET_IN               => '0',
        ------------------ Receive Ports - RX Margin Analysis ports ----------------
        GT1_RXLPMEN_IN                  => '0',
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT1_RXRESETDONE_OUT             => SFP_rxresetdone(1),
        --------------------- TX Initialization and Reset Ports --------------------
        GT1_GTTXRESET_IN                => '0',
        GT1_TXUSERRDY_IN                => SFP_txuserrdy(1),
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT1_TXUSRCLK_IN                 => txusrclk,
        GT1_TXUSRCLK2_IN                => txusrclk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT1_TXDIFFCTRL_IN               => "1110",
        GT1_TXINHIBIT_IN                => '0',
        GT1_TXMAINCURSOR_IN             => (others => '0'),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT1_TXDATA_IN                   => SFP_TXD_inv(1),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT1_GTXTXN_OUT                  => SFP1_TXN,
        GT1_GTXTXP_OUT                  => SFP1_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT1_TXOUTCLK_OUT                => open,
        GT1_TXOUTCLKFABRIC_OUT          => open,
        GT1_TXOUTCLKPCS_OUT             => open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT1_TXHEADER_IN                 => SFP_TXHEADER(1),
        GT1_TXSEQUENCE_IN               => SFP_TXSEQUENCE(1),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT1_TXRESETDONE_OUT             => SFP_txresetdone(1),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT1_TXPRBSSEL_IN                => SFP_TXPRBSSEL_IN(1),
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT2  (X1Y14)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT2_DRPADDR_IN                  => SFP_drpaddr(2),
        GT2_DRPCLK_IN                   => DRPclk,
        GT2_DRPDI_IN                    => SFP_drpdi(2),
        GT2_DRPDO_OUT                   => SFP_drpdo(2),
        GT2_DRPEN_IN                    => SFP_drpen(2),
        GT2_DRPRDY_OUT                  => SFP_drprdy(2),
        GT2_DRPWE_IN                    => SFP_drpwe(2),
        ------------------------------- Loopback Ports -----------------------------
        GT2_LOOPBACK_IN                 => SFP_LOOPBACK_IN(2),
        ------------------------------ Power-Down Ports ----------------------------
        GT2_RXPD_IN                     => SFP_pd(2),
        GT2_TXPD_IN                     => SFP_pd(2),
        --------------------- RX Initialization and Reset Ports --------------------
        GT2_RXUSERRDY_IN                => SFP_rxuserrdy(2),
        -------------------------- RX Margin Analysis Ports ------------------------
        GT2_EYESCANDATAERROR_OUT        => SFP_EYESCANDATAERROR_OUT(2),
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT2_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT2_RXUSRCLK_IN                 => SFP_RXUSRCLK(2),
        GT2_RXUSRCLK2_IN                => SFP_RXUSRCLK(2),
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT2_RXDATA_OUT                  => SFP_RXD_inv(2),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT2_RXPRBSERR_OUT               => SFP_RXPRBSERR_OUT(2),
        GT2_RXPRBSSEL_IN                => SFP_RXPRBSSEL_IN(2),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT2_RXPRBSCNTRESET_IN           =>      '0',
        --------------------------- Receive Ports - RX AFE -------------------------
        GT2_GTXRXP_IN                   => SFP2_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT2_GTXRXN_IN                   => SFP2_RXN,
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT2_RXBUFRESET_IN               => '0',
        GT2_RXBUFSTATUS_OUT             => open,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        GT2_RXOUTCLK_OUT                => SFP_rxoutclk(2),
        ---------------------- Receive Ports - RX Gearbox Ports --------------------
        GT2_RXDATAVALID_OUT             => SFP_RXDVLD(2),
        GT2_RXHEADER_OUT                => SFP_RXHEADER(2),
        GT2_RXHEADERVALID_OUT           => SFP_RXHEADERVLD(2),
        --------------------- Receive Ports - RX Gearbox Ports  --------------------
        GT2_RXGEARBOXSLIP_IN            => SFP_RXGEARBOXSLIP(2),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT2_GTRXRESET_IN                => '0',
        GT2_RXPMARESET_IN               => '0',
        ------------------ Receive Ports - RX Margin Analysis ports ----------------
        GT2_RXLPMEN_IN                  => '0',
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT2_RXRESETDONE_OUT             => SFP_rxresetdone(2),
        --------------------- TX Initialization and Reset Ports --------------------
        GT2_GTTXRESET_IN                => '0',
        GT2_TXUSERRDY_IN                => SFP_txuserrdy(2),
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT2_TXUSRCLK_IN                 => txusrclk,
        GT2_TXUSRCLK2_IN                => txusrclk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT2_TXDIFFCTRL_IN               => "1110",
        GT2_TXINHIBIT_IN                => '0',
        GT2_TXMAINCURSOR_IN             => (others => '0'),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT2_TXDATA_IN                   => SFP_TXD_inv(2),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT2_GTXTXN_OUT                  => SFP2_TXN,
        GT2_GTXTXP_OUT                  => SFP2_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT2_TXOUTCLK_OUT                => open,
        GT2_TXOUTCLKFABRIC_OUT          => open,
        GT2_TXOUTCLKPCS_OUT             => open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT2_TXHEADER_IN                 => SFP_TXHEADER(2),
        GT2_TXSEQUENCE_IN               => SFP_TXSEQUENCE(2),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT2_TXRESETDONE_OUT             => SFP_txresetdone(2),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT2_TXPRBSSEL_IN                => SFP_TXPRBSSEL_IN(2),


    --____________________________COMMON PORTS________________________________
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GT0_GTREFCLK0_COMMON_IN         => SFP_REFCLK,
        ------------------------- Common Block - QPLL Ports ------------------------
        GT0_QPLLLOCK_OUT                => qplllock,
        GT0_QPLLLOCKDETCLK_IN           => DRPclk,
        GT0_QPLLRESET_IN                => qpllreset

    );
process(SFP_TXD,SFP_RXD,SFP_RXD_inv)
	begin
		for j in 0 to 2 loop
			for i in 0 to 31 loop
				SFP_TXD_inv(j)(i) <= SFP_TXD(j)(31-i);
				SFP_RXD(j)(i) <= SFP_RXD_inv(j)(31-i);
			end loop;
		end loop;
end process;
i_REFCLK : IBUFDS_GTE2 port map(O => SFP_REFCLK, ODIV2 => open, CEB => '0', I => SFP_REFCLK_P, IB => SFP_REFCLK_N);
i_txusrclk : BUFG port map (I => SFP_TXOUTCLK(0), O => txusrclk);
g_SFP_rxusrclk : for i in 0 to 2 generate
	i_SFP_rxusrclk : BUFG port map (I => SFP_RXOUTCLK(i), O => SFP_rxusrclk(i));
end generate;
i_REFCLK2X_in: bufg port map(i => SFP_REFCLK, o => REFCLK2X_in);
i_ClientClk2X : BUFG port map (I => ClientClk2X_dcm, O => ClientClk2X);
i_ClientClk : BUFG port map (I => ClientClk_dcm, O => ClientClk);
i_REFCLK2XPLLRST : SRL16 generic map(INIT => x"ffff")
   port map (
      Q => REFCLK2XPLLRST,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CLK => REFCLK2X_in,   -- Clock input
      D => '0'        -- SRL data input
   );
i_REFCLK2XPLL : PLLE2_BASE
   generic map (
      BANDWIDTH => "OPTIMIZED",  -- OPTIMIZED, HIGH, LOW
      CLKFBOUT_MULT => 8,        -- Multiply value for all CLKOUT, (2-64)
      CLKFBOUT_PHASE => 0.0,     -- Phase offset in degrees of CLKFB, (-360.000-360.000).
      CLKIN1_PERIOD => 6.4,      -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT0_DIVIDE => 4,
      DIVCLK_DIVIDE => 1,        -- Master division value, (1-56)
      REF_JITTER1 => 0.0,        -- Reference input jitter in UI, (0.000-0.999).
      STARTUP_WAIT => "FALSE"    -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
   )
   port map (
      -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
      CLKOUT0 => ClientClk2X_dcm,
     -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
      CLKFBOUT => ClientClk_dcm, -- 1-bit output: Feedback clock
      -- Status Port: 1-bit (each) output: PLL status ports
      LOCKED => ClientClk_lock,     -- 1-bit output: LOCK
      -- Clock Input: 1-bit (each) input: Clock input
      CLKIN1 => REFCLK2X_in,     -- 1-bit input: Input clock
      -- Control Ports: 1-bit (each) input: PLL control ports
      PWRDWN => '0',     -- 1-bit input: Power-down
      RST => REFCLK2XPLLRST,           -- 1-bit input: Reset
      -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
      CLKFBIN => ClientClk    -- 1-bit input: Feedback clock
   );


end Behavioral;

