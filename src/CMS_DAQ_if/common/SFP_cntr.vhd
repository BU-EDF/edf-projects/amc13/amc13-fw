----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:54:09 02/16/2016 
-- Design Name: 
-- Module Name:    TTC_cntr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity SFP_cntr is
	 generic (N : integer := 3);
   Port ( sysclk : in  STD_LOGIC;
          clk125 : in  STD_LOGIC;
          ipb_clk : in  STD_LOGIC;
          resetCntr : in  STD_LOGIC;
          DB_cmd : in  STD_LOGIC;
          Cntr_DATA : in  STD_LOGIC_VECTOR(31 downto 0);
          Cntr_ADDR : out  STD_LOGIC_VECTOR(6 downto 0);
          ipb_addr : in  STD_LOGIC_VECTOR(15 downto 0);
          ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0));
end SFP_cntr;
architecture Behavioral of SFP_cntr is
signal resetCntr_SyncRegs : std_logic_vector(2 downto 0) := (others =>'0');
signal div : std_logic_vector(6 downto 0) := (others =>'0');
signal div_l : std_logic_vector(6 downto 0) := (others =>'0');
signal CntrRstCycle : std_logic := '0';
signal counter_wa : std_logic_vector(9 downto 0) := (others => '0');
signal counter_ra : std_logic_vector(9 downto 0) := (others => '0');
signal counter_DIA : std_logic_vector(31 downto 0) := (others => '0');
signal counter_DOA : std_logic_vector(31 downto 0) := (others => '0');
signal we_counter : std_logic_vector(3 downto 0) := (others => '0');
signal startSyncRegs : std_logic_vector(3 downto 0) := (others =>'0');
signal DataType : std_logic_vector(1 downto 0) := (others => '0');
signal DataType_q : std_logic_vector(1 downto 0) := (others => '0');
signal ec_div : std_logic := '0';
signal start : std_logic := '0';
signal data : std_logic_vector(31 downto 0) := (others => '0');
signal DB_cmd_l : std_logic := '0';
signal carry : std_logic := '0';
signal DB_en : std_logic := '0';
signal sr : std_logic_vector(3 downto 0) := (others => '0');
signal toggle : std_logic := '0';
signal toggle_q : std_logic := '0';
signal ec_rdata : std_logic := '0';
signal ec_wdata : std_logic := '0';

begin
Cntr_ADDR <= div;
start <= sr(0);
process(sysclk, CntrRstCycle)
begin
	if(CntrRstCycle = '1')then
		data <= (others => '0');
		startSyncRegs <= (others => '0');
		ec_div <= '0';
		div <= (others => '0');
		div_l <= (others => '0');
		DataType <= "00";
	elsif(sysclk'event and sysclk = '1')then
		startSyncRegs <= startSyncRegs(2 downto 0) & start;
		if(startSyncRegs(3 downto 2) = "01")then
			ec_div <= '1';
		else
			ec_div <= '0';
		end if;
		if(ec_div = '1')then
			div <= div + 1;
			div_l <= div;
			data <= Cntr_Data;
			if(div(6 downto 3) = "0111" and div(2 downto 1) /= "00")then
				DataType <= "11";-- 56 bit counter
			elsif(div(6) = '0' and div(5 downto 4) /= "11")then
				DataType <= "00";-- register
			elsif(div(6 downto 3) = "1010" and (div(2) = '0' or div(1 downto 0) = "00"))then
				DataType <= "00";-- register
			elsif(div(6 downto 4) = "111")then
				DataType <= "00";-- register
			else
				DataType <= "10";-- 32 bit counter
			end if;
		end if;
	end if;
end process;
i_counter : BRAM_TDP_MACRO
   generic map (
      BRAM_SIZE => "36Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_MODE_A => "READ_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
      READ_WIDTH_A => 32,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH_B => 32,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      WRITE_WIDTH_A => 32, -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      WRITE_WIDTH_B => 32) -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
   port map (
      DOA => counter_DOA,       -- Output port-A data, width defined by READ_WIDTH_A parameter
      DOB => ipb_rdata,       -- Output port-B data, width defined by READ_WIDTH_B parameter
      ADDRA => counter_wa,   -- Input port-A address, width defined by Port A depth
      ADDRB => counter_ra,   -- Input port-B address, width defined by Port B depth
      CLKA => clk125,     -- 1-bit input port-A clock
      CLKB => clk125,     -- 1-bit input port-B clock
      DIA => counter_DIA,       -- Input port-A data, width defined by WRITE_WIDTH_A parameter
      DIB => x"00000000",       -- Input port-B data, width defined by WRITE_WIDTH_B parameter
      ENA => '1',       -- 1-bit input port-A enable
      ENB => ec_rdata,       -- 1-bit input port-B enable
      REGCEA => '0', -- 1-bit input port-A output register enable
      REGCEB => '0', -- 1-bit input port-B output register enable
      RSTA => '0',     -- 1-bit input port-A reset
      RSTB => '0',     -- 1-bit input port-B reset
      WEA => we_counter,       -- Input port-A write enable, width defined by Port A depth
      WEB => "0000"        -- Input port-B write enable, width defined by Port B depth
   );
counter_ra(8 downto 0) <= '0' & ipb_addr(15) & ipb_addr(6 downto 0);
counter_ra(9) <= '0' when ipb_addr(14 downto 7) = LSC_addr(14 downto 7) else '1';
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		toggle <= not toggle;
	end if;
end process;
process(clk125)
begin
	if(clk125'event and clk125 = '1')then
		toggle_q <= toggle;
		ec_rdata <= toggle xor toggle_q;
		if(DB_cmd = '1' and ec_wdata = '1')then
			DB_cmd_l <= '1';
		elsif(sr(0) = '1' and and_reduce(counter_wa(6 downto 0)) = '1')then
			DB_cmd_l <= '0';
		end if;
		if(sr(0) = '1' and and_reduce(counter_wa(6 downto 0)) = '1')then
			DB_en <= DB_cmd_l;
		end if;
		if(sr(0) = '1')then
			DataType_q <= DataType;
		end if;
		if(resetCntr = '1')then
			counter_wa <= (others => '0');
		elsif(CntrRstCycle = '1')then
			counter_wa(7 downto 0) <= counter_wa(7 downto 0) + 1;
		elsif(sr(0) = '1')then
			counter_wa(7 downto 0) <= '0' & div_l;
		elsif(sr(3) = '1')then
			counter_wa(7) <= '1';
		end if;
		if(resetCntr = '1')then
			CntrRstCycle <= '1';
		elsif(and_reduce(counter_wa(7 downto 0)) = '1')then
			CntrRstCycle <= '0';
		end if;
		if(CntrRstCycle = '1')then
			sr <= "0000";
		elsif(sr(2 downto 0) = "000")then
			sr <= "0001";
		else
			sr <= sr(2 downto 0) & '0';
		end if;
		if(CntrRstCycle = '1' or sr(2) = '1')then
			carry <= '0';
		elsif(sr(3) = '1' and DataType_q = "11" and counter_DIA(31) = '0' and counter_DOA(31) = '1')then
			carry <= '1';
		end if;
		if(CntrRstCycle = '1')then
			counter_DIA <= (others => '0');
		elsif(sr(0) = '1')then
			counter_DIA <= data;
		elsif(sr(2) = '1')then
			if(DataType_q = "10")then
				if(counter_DOA(9 downto 0) > counter_DIA(9 downto 0))then
					counter_DIA(31 downto 10) <= counter_DOA(31 downto 10) + 1;
				else
					counter_DIA(31 downto 10) <= counter_DOA(31 downto 10);
				end if;
			elsif(DataType_q = "11")then
				if(counter_wa(0) = '1')then
					counter_DIA(23 downto 0) <= counter_DOA(23 downto 0) + carry;
					counter_DIA(31 downto 24) <= x"00";
				elsif(counter_DOA(24 downto 0) > counter_DIA(24 downto 0))then
					counter_DIA(31 downto 25) <= counter_DOA(31 downto 25) + 1;
				else
					counter_DIA(31 downto 25) <= counter_DOA(31 downto 25);
				end if;
			end if;
		end if;
		if(sr(2) = '1' or (sr(3) = '1' and DB_en = '1') or CntrRstCycle = '1')then
			we_counter <= x"f";
		else
			we_counter <= x"0";
		end if;
	end if;
end process;
i_ec_wdata : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q => ec_wdata,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '0',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk125,   -- Clock input
      D => ec_rdata        -- SRL data input
   );
end Behavioral;

