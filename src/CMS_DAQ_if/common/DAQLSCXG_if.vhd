----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:07:47 10/07/2013 
-- Design Name: 
-- Module Name:    TCPIP_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use IEEE.numeric_std.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity DAQLSCXG_if is
	generic(N_SFP : integer := 1);
    Port ( sysclk : in  STD_LOGIC;
           clk125 : in  STD_LOGIC;
           DRPclk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           daq_reset : in  STD_LOGIC;
           gtx_reset : in  STD_LOGIC;
           rstCntr : in  STD_LOGIC;
           Dis_pd : in  STD_LOGIC;
           test : in  STD_LOGIC;
					 DB_cmd : IN std_logic;
					 enSFP : IN std_logic_vector(3 downto 0);
					 SFP_ABS : IN std_logic_vector(2 downto 0);
					 LSC_ID : IN std_logic_vector(15 downto 0);
					 SFP_down : OUT std_logic_vector(2 downto 0);
					 LinkFull : OUT std_logic_vector(2 downto 0);
           inc_ddr_pa : in  STD_LOGIC;
-- event data in
					 evt_data_rdy : in std_logic_vector(2 downto 0);
					 EventData_in : in array3X67;
					 EventData_we : in   std_logic_VECTOR(2 downto 0);
					 EventData_re : out   std_logic_VECTOR(2 downto 0); -- 
					 evt_buf_full : out std_logic_vector(2 downto 0);
					 buf_rqst : in std_logic_vector(3 downto 0);
					 WaitMonBuf : IN std_logic;
           MonBufOverWrite : in  STD_LOGIC;
           TCPBuf_avl : out  STD_LOGIC;
           MonBuf_avl : out  STD_LOGIC;
           MonBuf_empty : out  STD_LOGIC;
           MonBufOvfl : out  STD_LOGIC;
					 mon_evt_cnt : out std_logic_vector(31 downto 0);
					 WrtMonBlkDone : in  STD_LOGIC_VECTOR(2 downto 0);
					 WrtMonEvtDone : in  STD_LOGIC_VECTOR(2 downto 0);
-- ddr wportA status
					 wport_rdy : in std_logic_vector(2 downto 0);
					 wport_FIFO_full : in std_logic_vector(2 downto 0);
-- signal to ddr_if, AMC_if to start moving data
					 EventBufAddr_we : out std_logic_VECTOR(2 downto 0);
					 EventBufAddr : out array3X14;
-- SFP ports
           SFP0_RXN : in  STD_LOGIC;
           SFP0_RXP : in  STD_LOGIC;
           SFP1_RXN : in  STD_LOGIC;
           SFP1_RXP : in  STD_LOGIC;
           SFP2_RXN : in  STD_LOGIC;
           SFP2_RXP : in  STD_LOGIC;
           SFP0_TXN : out  STD_LOGIC;
           SFP0_TXP : out  STD_LOGIC;
           SFP1_TXN : out  STD_LOGIC;
           SFP1_TXP : out  STD_LOGIC;
           SFP2_TXN : out  STD_LOGIC;
           SFP2_TXP : out  STD_LOGIC;
           SFP_REFCLK_P : in  STD_LOGIC;
           SFP_REFCLK_N : in  STD_LOGIC;
           GbE_REFCLK : in  STD_LOGIC;
--	ipbus signals
					 ipb_clk : in  STD_LOGIC;
					 ipb_write : in  STD_LOGIC;
					 ipb_strobe : in  STD_LOGIC;
					 ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_wdata : in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_ack : out  STD_LOGIC
					 );
end DAQLSCXG_if;

architecture Behavioral of DAQLSCXG_if is
COMPONENT DaqLSCXG10G
	generic(N_SFP : integer := 1);
    Port ( sys_reset : in  STD_LOGIC; -- active high reset of all logic but GTX
           sys_clk : in  STD_LOGIC;
           sfp_pd : in array3x2;
           DRP_clk : in  STD_LOGIC;
           LinkWe : in  STD_LOGIC_VECTOR (2 downto 0);
           LinkCtrl : in  STD_LOGIC_VECTOR (2 downto 0);
           LinkData : in  array3x64;
           srcID : in array3x16;
           LinkDown : out  STD_LOGIC_VECTOR (2 downto 0);
           LinkFull : out  STD_LOGIC_VECTOR (2 downto 0);
			  sync_loss : out STD_LOGIC_VECTOR (2 downto 0);		  -- goes to '1' (rxusrclk) when SERDES is out of synch
			  status_ce : in std_logic_VECTOR (2 downto 0);        -- not implemented yet
			  status_addr : in STD_LOGIC_VECTOR (15 downto 0); -- not implemented yet
			  status_port : out array3x64; -- first 32 bits are hard-wired
			  --
			  txusrclk_o : out STD_LOGIC;  -- reconstructed tx clock, to be used to clock sending circuitry
			  rxusrclk_o : out STD_LOGIC;  -- reconstructed rx clock, to be used to clock receiving circuitry
			  --
			  gtx_reset : in std_logic;    -- full reset of GTX only
			  gtx_refclk_p : in std_logic; -- iob for refclk neg
			  gtx_refclk_n : in std_logic; -- iob for refclk neg
			  sfp_rxn : in std_logic_VECTOR (2 downto 0);      -- sfp iobs
			  sfp_rxp : in std_logic_VECTOR (2 downto 0);
			  sfp_txn : out std_logic_VECTOR (2 downto 0);
			  sfp_txp : out std_logic_VECTOR (2 downto 0)
			 );
END COMPONENT;
COMPONENT DaqLSCXG
	generic(N_SFP : integer := 1);
    Port ( sys_reset : in  STD_LOGIC; -- active high reset of all logic but GTX
           sys_clk : in  STD_LOGIC;
           sfp_pd : in array3x2;
           DRP_clk : in  STD_LOGIC;
           LinkWe : in  STD_LOGIC_VECTOR (2 downto 0);
           LinkCtrl : in  STD_LOGIC_VECTOR (2 downto 0);
           LinkData : in  array3x64;
           srcID : in array3x16;
           LinkDown : out  STD_LOGIC_VECTOR (2 downto 0);
           LinkFull : out  STD_LOGIC_VECTOR (2 downto 0);
			  sync_loss : out STD_LOGIC_VECTOR (2 downto 0);		  -- goes to '1' (rxusrclk) when SERDES is out of synch
			  status_ce : in std_logic_VECTOR (2 downto 0);        -- not implemented yet
			  status_addr : in STD_LOGIC_VECTOR (15 downto 0); -- not implemented yet
			  status_port : out array3x64; -- first 32 bits are hard-wired
			  --
			  txusrclk_o : out STD_LOGIC;  -- reconstructed tx clock, to be used to clock sending circuitry
			  rxusrclk_o : out STD_LOGIC;  -- reconstructed rx clock, to be used to clock receiving circuitry
			  --
			  gtx_reset : in std_logic;    -- full reset of GTX only
			  gtx_refclk : in std_logic; -- iob for refclk neg
			  sfp_rxn : in std_logic_VECTOR (2 downto 0);      -- sfp iobs
			  sfp_rxp : in std_logic_VECTOR (2 downto 0);
			  sfp_txn : out std_logic_VECTOR (2 downto 0);
			  sfp_txp : out std_logic_VECTOR (2 downto 0)
			 );
END COMPONENT;
COMPONENT cmsCRC64
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		crc_init : IN std_logic;
		trailer : IN std_logic;
		crc_d : IN std_logic_vector(63 downto 0);
		crc_ce : IN std_logic;          
		crc : OUT std_logic_vector(15 downto 0);
		crc_err : OUT std_logic;
		dout : OUT std_logic_vector(63 downto 0);
		dout_vld : OUT std_logic
		);
END COMPONENT;
COMPONENT check_event
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		en_stop : IN std_logic_vector(4 downto 0);
		cmsCRC_err : IN std_logic_vector(2 downto 0);
		EventData_in : IN array3X67;
		EventData_we : IN std_logic_vector(2 downto 0);          
		inc_err : OUT array3X5;
		stop : OUT std_logic
		);
END COMPONENT;
COMPONENT SFP_cntr
	PORT(
		sysclk : IN std_logic;
		clk125 : IN std_logic;
		ipb_clk : IN std_logic;
		resetCntr : IN std_logic;
		DB_cmd : IN std_logic;
		Cntr_DATA : IN std_logic_vector(31 downto 0);
		ipb_addr : IN std_logic_vector(15 downto 0);          
		Cntr_ADDR : OUT std_logic_vector(6 downto 0);
		ipb_rdata : OUT std_logic_vector(31 downto 0)
		);
END COMPONENT;
function A_GT_B (A, B : std_logic_vector(10 downto 0)) return boolean is
variable tmp : std_logic_vector(10 downto 0);
begin
	tmp := A - B;
	if(tmp(10) = '0' and or_reduce(tmp(9 downto 0)) = '1')then
		return true;
	else
		return false;
	end if;
end A_GT_B;
function A_GE_B (A, B : std_logic_vector(10 downto 0)) return boolean is
variable tmp : std_logic_vector(10 downto 0);
begin
	tmp := A - B;
	if(tmp(10) = '0')then
		return true;
	else
		return false;
	end if;
end A_GE_B;
signal resetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal daq_resetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal inc_ddr_paSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal bldr2SFP_sel : array3X2 := (others => (others => '0'));
signal SFP2bldr_sel : array3X2 := (others => (others => '0'));
signal EventBufAddr_we_i : std_logic_vector(2 downto 0) := (others => '0');
signal ReadBusy : std_logic_vector(2 downto 0) := (others => '0');
signal sysDIV2 : std_logic := '0';
--signal MonBuf_avl_i : std_logic := '0';
signal MonBuf_wa : std_logic_vector(10 downto 0) := (others => '0');
signal MonBuf_ra : std_logic_vector(10 downto 0) := (others => '0');
signal MonBufUsed : std_logic_vector(9 downto 0) := (others => '0');
signal NXT_MonBuf : array3X11 := (others => (others => '0'));
signal Written_MonBuf : array4X11 := (others => (others => '0'));
signal Written_MonBufMatch : std_logic_vector(2 downto 0) := (others => '0');
signal AddrOffset : array3X10 := (others => (others => '0'));
signal header : std_logic_vector(2 downto 0) := (others => '0');
signal header_q : std_logic_vector(2 downto 0) := (others => '0');
signal sfp_rxn : std_logic_vector(2 downto 0) := (others => '0');
signal sfp_rxp : std_logic_vector(2 downto 0) := (others => '0');
signal sfp_txn : std_logic_vector(2 downto 0) := (others => '0');
signal sfp_txp : std_logic_vector(2 downto 0) := (others => '0');
signal sync_loss : std_logic_vector(2 downto 0) := (others => '0');
signal LinkFull_n : std_logic_vector(2 downto 0) := (others => '0');
signal LinkFull_i : std_logic_vector(2 downto 0) := (others => '0');
signal LinkCtrl : std_logic_vector(2 downto 0) := (others => '0');
signal LinkWe : std_logic_vector(2 downto 0) := (others => '0');
signal LinkDown : std_logic_vector(2 downto 0) := (others => '0');
signal DaqLSC_status : array3X64 := (others => (others => '0'));
signal LinkData : array3X65 := (others => (others => '0'));
signal sync_lossSync : array3X3 := (others => (others => '0'));
signal sync_loss_cntr : array3X32 := (others => (others => '0'));
signal SFP_we : std_logic_vector(2 downto 0) := (others => '0');
signal EoB : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_evt_cntr : array3X32 := (others => (others => '0'));
signal SFP_blk_cntr : array3X32 := (others => (others => '0'));
signal SFP_word_cntr : array3X32 := (others => (others => '0'));
signal event_cntr : array3X32 := (others => (others => '0'));
signal word_cntr : array3X32 := (others => (others => '0'));
signal EventBufAddr_we_cntr : array3X32 := (others => (others => '0'));
signal src_ID : array3X16 := (others => (others => '0'));
signal txusrclk : std_logic := '0';
signal LinkDatap : array3X64 := (others => (others => '0'));
signal sfp_pd : array3X2 := (others => (others => '0'));
signal WrtMonEvtDoneCntr : array3X8 := (others => (others => '0'));
signal chk_MonBuf_avl : std_logic := '0';
signal FirstBlkAddrDo : array2x3x12 := (others => (others => (others => '0')));
signal FirstBlkAddr_ra : array2x3x5 := (others => (others => (others => '0')));
signal FirstBlkAddr_re : array2X3 := (others => (others => '0'));
signal WrtMonEvtDone_l : std_logic_vector(2 downto 0) := (others => '0');
signal MonEvtQueued : std_logic_vector(2 downto 0) := (others => '0');
signal FirstBlkAddrDoValid : array2X3 := (others => (others => '0'));
signal FirstBlkAddr_wa : std_logic_vector(4 downto 0) := (others => '0');
signal FirstBlkAddrDi : std_logic_vector(11 downto 0) := (others => '0');
signal FirstBlkAddr_we : std_logic := '0';
signal MonBuf_full : std_logic := '0';
signal mon_evt_cnt_i : std_logic_vector(10 downto 0) := (others => '0');
signal status_addr : std_logic_vector(15 downto 0) := (others => '0');
signal cmsCRC_initp : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_init : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_err : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_errCntr : array3X32 := (others => (others => '0'));
signal EvtLength_errCntr : array3X32 := (others => (others => '0'));
signal AMClength_errCntr : array3X32 := (others => (others => '0'));
signal AMCvalid_errCntr : array3X32 := (others => (others => '0'));
signal AMCcrc_errCntr : array3X32 := (others => (others => '0'));
signal BackPressureCntr : array3X32 := (others => (others => '0'));
signal TotalEvtLengthCntr : array3X32 := (others => (others => '0'));
signal BackPressure31 : std_logic_vector(2 downto 0) := (others => '1');
signal BackPressure31_q : std_logic_vector(2 downto 0) := (others => '1');
signal IsBackPressure : std_logic := '0';
signal StopOverWrite : std_logic := '0';
signal en_stop : std_logic_vector(4 downto 0) := (others => '1');
signal stop : std_logic := '0';
signal Cntr2ms : std_logic_vector(18 downto 0) := (others => '1');
signal LiveTimeCntr : array3x19 := (others => (others => '0'));
signal LiveTime : array3x8 := (others => (others => '0'));
signal inc_err : array3x5 := (others => (others => '0'));
signal Cntr_DATA : std_logic_vector(31 downto 0) := (others => '0');
signal Cntr_ADDR : std_logic_vector(6 downto 0) := (others => '0');
component chipscope1 is
		generic (N : integer := 5);
    Port ( clk : in  STD_LOGIC;
           Din : in  STD_LOGIC_VECTOR (303 downto 0));
end component;
signal cs : std_logic_vector(303 downto 0) := (others => '0');
begin
--i_chipscope: chipscope1
--    Port map( clk => sysclk, Din => cs);
TCPBuf_avl <= '1';
MonBufOvfl <= '0';
EventBufAddr_we <= EventBufAddr_we_i;
mon_evt_cnt(31 downto 11) <= (others => '0');
mon_evt_cnt(10 downto 0) <= mon_evt_cnt_i;
ipb_ack <= '0';
LinkFull <= LinkFull_i;
sfp_pd(0) <= "00" when Dis_pd = '1' or SFP_ABS(0) = '0' else "11";
sfp_pd(1) <= "00" when Dis_pd = '1' or SFP_ABS(1) = '0' else "11";
sfp_pd(2) <= "00" when Dis_pd = '1' or SFP_ABS(2) = '0' else "11";
SFP_down(2) <= not LinkDown(2) and not enSFP(3) and enSFP(2);
SFP_down(1) <= not LinkDown(1) and not enSFP(3) and enSFP(1);
SFP_down(0) <= not LinkDown(0) and not enSFP(3) and enSFP(0);
LinkFull_i(2) <= not LinkFull_n(2) and not enSFP(3) and enSFP(2);
LinkFull_i(1) <= not LinkFull_n(1) and not enSFP(3) and enSFP(1);
LinkFull_i(0) <= not LinkFull_n(0) and not enSFP(3) and enSFP(0);
g_10g : if (lsc_speed = 10) generate
	i_DaqLSCXG: DaqLSCXG10G PORT MAP(
		sys_reset => daq_resetSyncRegs(2),
		sys_clk => sysclk,
		sfp_pd => sfp_pd,
		DRP_clk => DRPclk,
		LinkWe => LinkWe,
		LinkCtrl => LinkCtrl,
		LinkData => LinkDatap,
		srcID => src_ID,
		LinkDown => LinkDown,
		LinkFull => LinkFull_n,
		sync_loss => sync_loss,
		status_ce => "000",
		status_addr => status_addr,
		status_port => DaqLSC_status,
		txusrclk_o => txusrclk,
		rxusrclk_o => open,
		gtx_reset => gtx_reset,
		gtx_refclk_p => SFP_REFCLK_P,
		gtx_refclk_n => SFP_REFCLK_N,
		sfp_rxn => sfp_rxn,
		sfp_rxp => sfp_rxp,
		sfp_txn => sfp_txn,
		sfp_txp => sfp_txp
	);
end generate g_10g;
g_5g : if (lsc_speed = 5) generate
	i_DaqLSCXG: DaqLSCXG PORT MAP(
		sys_reset => daq_resetSyncRegs(2),
		sys_clk => sysclk,
		sfp_pd => sfp_pd,
		DRP_clk => DRPclk,
		LinkWe => LinkWe,
		LinkCtrl => LinkCtrl,
		LinkData => LinkDatap,
		srcID => src_ID,
		LinkDown => LinkDown,
		LinkFull => LinkFull_n,
		sync_loss => sync_loss,
		status_ce => "000",
		status_addr => status_addr,
		status_port => DaqLSC_status,
		txusrclk_o => txusrclk,
		rxusrclk_o => open,
		gtx_reset => gtx_reset,
		gtx_refclk => GbE_REFCLK,
		sfp_rxn => sfp_rxn,
		sfp_rxp => sfp_rxp,
		sfp_txn => sfp_txn,
		sfp_txp => sfp_txp
	);
end generate g_5g;
status_addr <= x"000" & Cntr_ADDR(3 downto 0);
LinkDatap(0) <= LinkData(0)(63 downto 0);
LinkDatap(1) <= LinkData(1)(63 downto 0);
LinkDatap(2) <= LinkData(2)(63 downto 0);
src_ID(0) <= LSC_ID(15 downto 2) & "00";
src_ID(1) <= LSC_ID(15 downto 2) & "01";
src_ID(2) <= LSC_ID(15 downto 2) & "10";
sfp_rxn(0) <= SFP0_RXN;
sfp_rxp(0) <= SFP0_RXP;
SFP0_TXN <= sfp_txn(0);
SFP0_TXP <= sfp_txp(0);
sfp_rxn(1) <= SFP1_RXN;
sfp_rxp(1) <= SFP1_RXP;
SFP1_TXN <= sfp_txn(1);
SFP1_TXP <= sfp_txp(1);
sfp_rxn(2) <= SFP2_RXN;
sfp_rxp(2) <= SFP2_RXP;
SFP2_TXN <= sfp_txn(2);
SFP2_TXP <= sfp_txp(2);
process(sysclk,daq_reset)
begin
	if(daq_reset = '1')then
		daq_resetSyncRegs <= (others => '1');
	elsif(sysclk'event and sysclk = '1')then
		daq_resetSyncRegs <= daq_resetSyncRegs(1 downto 0) & '0';
	end if;
end process;
process(sysclk,reset)
begin
	if(reset = '1')then
		resetSyncRegs <= (others => '1');
	elsif(sysclk'event and sysclk = '1')then
		resetSyncRegs <= resetSyncRegs(1 downto 0) & '0';
	end if;
end process;
g_FirstBlkAddr: for j in 0 to 1 generate
	g1_FirstBlkAddr: for i in 0 to 5 generate
   i_FirstBlkAddr : RAM32M
   port map (
      DOA => FirstBlkAddrDo(j)(0)(i*2+1 downto i*2), -- Read port A 2-bit output
      DOB => FirstBlkAddrDo(j)(1)(i*2+1 downto i*2), -- Read port B 2-bit output
      DOC => FirstBlkAddrDo(j)(2)(i*2+1 downto i*2), -- Read port C 2-bit output
      DOD => open, -- Read/Write port D 2-bit output
      ADDRA => FirstBlkAddr_ra(j)(0),   -- Read port A 5-bit address input
      ADDRB => FirstBlkAddr_ra(j)(1),   -- Read port B 5-bit address input
      ADDRC => FirstBlkAddr_ra(j)(2),   -- Read port C 5-bit address input
      ADDRD => FirstBlkAddr_wa,   -- Read/Write port D 5-bit address input
      DIA => FirstBlkAddrDi(i*2+1 downto i*2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRA
      DIB => FirstBlkAddrDi(i*2+1 downto i*2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRB
      DIC => FirstBlkAddrDi(i*2+1 downto i*2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRC
      DID => "00", -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRD
      WCLK => sysclk,  -- Write clock input
      WE => FirstBlkAddr_we       -- Write enable input
   );
	end generate;
end generate;
FirstBlkAddrDi <= '0' & MonBuf_wa;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			chk_MonBuf_avl <= '1';
		elsif(buf_rqst(0) = '1')then
			chk_MonBuf_avl <= buf_rqst(3);
		end if;
		if(resetSyncRegs(2) = '1')then
			MonBuf_avl <= '1';
		elsif(MonBufOverWrite = '1')then
			MonBuf_avl <= not StopOverWrite;
--		elsif(WaitMonBuf = '1' or MonBufOverWrite = '1')then
		elsif(WaitMonBuf = '1')then
			if((MonBuf_wa(10) /= MonBuf_ra(10) and MonBuf_wa(9 downto 0) = MonBuf_ra(9 downto 0)))then
				MonBuf_avl <= '0';
			else
				MonBuf_avl <= '1';
			end if;
		elsif(chk_MonBuf_avl = '1')then
			if(MonBufUsed(9 downto 8) = "11")then
				MonBuf_avl <= '0';
			else
				MonBuf_avl <= '1';
			end if;
		end if;
		if(MonBufOverWrite = '1')then
			mon_evt_cnt_i <= MonBuf_full & Written_MonBuf(3)(9 downto 0);
		else
			mon_evt_cnt_i <= Written_MonBuf(3) - MonBuf_ra;
		end if;
		if(MonBufOverWrite = '0')then
      MonBuf_empty <= not or_reduce(mon_evt_cnt_i);
    elsif(Written_MonBuf(3) /= MonBuf_ra or MonBuf_full = '1')then
      MonBuf_empty <= '0';
    else
      MonBuf_empty <= '1';
    end if;
		if(resetSyncRegs(2) = '1')then
			ReadBusy <= (others => '0');
			EventData_re <= (others => '0');
			EventBufAddr_we_i <= (others => '0');
			MonBuf_wa <= (others => '0');
			MonBuf_ra <= (others => '0');
			MonBufUsed <= (others => '0');
			Written_MonBufMatch <= (others => '1');
			Written_MonBuf <= (others => (others => '0'));
			NXT_MonBuf <= (others => (others => '0'));
			inc_ddr_paSyncRegs <= (others => '0');
			FirstBlkAddr_we <= '0';
			FirstBlkAddr_re <= (others => (others => '0'));
			FirstBlkAddrDoValid <= (others => (others => '0'));
			WrtMonEvtDone_l <= (others => '0');
			WrtMonEvtDoneCntr <= (others => (others => '0'));
			MonEvtQueued <= (others => '0');
			FirstBlkAddr_wa <= (others => '0');
			FirstBlkAddr_ra <= (others => (others => (others => '0')));
			MonBuf_full <= '0';
		else
			for i in 0 to 2 loop
				if(EVENTdata_in(i)(64) = '1' and EVENTdata_we(i) = '1')then
					ReadBusy(i) <= '0';
				elsif(evt_data_rdy(i) = '1' and wport_rdy(i) = '1')then
					ReadBusy(i) <= '1';
				end if;
				EventData_re(i) <= evt_data_rdy(i) and wport_rdy(i) and not ReadBusy(i);
				if(EventData_in(i)(66) = '0' and evt_data_rdy(i) = '1' and wport_rdy(i) = '1' and ReadBusy(i) = '0')then
					EventBufAddr_we_i(i) <= '1';
				else
					EventBufAddr_we_i(i) <= '0';
				end if;
			end loop;
		  if(MonBufOverWrite = '1' and Written_MonBuf(3)(10) = '1')then
        MonBuf_full <= '1';
      end if;
			for j in 0 to 1 loop
				for i in 0 to 2 loop
					if(FirstBlkAddr_re(j)(i) = '1')then
						FirstBlkAddr_ra(j)(i) <= FirstBlkAddr_ra(j)(i) + 1;
					end if;
					if(FirstBlkAddr_ra(j)(i) = FirstBlkAddr_wa)then
						FirstBlkAddrDoValid(j)(i) <= '0';
					else
						FirstBlkAddrDoValid(j)(i) <= '1';
					end if;
				end loop;
			end loop;
			if(FirstBlkAddr_we = '1')then
				FirstBlkAddr_wa <= FirstBlkAddr_wa + 1;
			end if;
			for i in 0 to 2 loop
				if(WrtMonEvtDone(i) = '1')then
					WrtMonEvtDoneCntr(i) <= WrtMonEvtDoneCntr(i) + 1;
				end if;
				if(WrtMonEvtDone(i) = '1')then
					WrtMonEvtDone_l(i) <= '1';
				elsif(FirstBlkAddrDoValid(0)(i) = '1')then
					WrtMonEvtDone_l(i) <= '0';
				end if;
				FirstBlkAddr_re(0)(i) <= FirstBlkAddrDoValid(0)(i) and WrtMonEvtDone_l(i);
				if(EventData_we(i) = '1' and EventData_in(i)(66 downto 65) = "01")then
					MonEvtQueued(i) <= '1';
				elsif(FirstBlkAddrDoValid(1)(i) = '1')then
					MonEvtQueued(i) <= '0';
				end if;
				FirstBlkAddr_re(1)(i) <= FirstBlkAddrDoValid(1)(i) and MonEvtQueued(i);
			end loop;
--			FirstBlkAddr_we <= buf_rqst(3) and buf_rqst(0) and MonBuf_avl_i;
			FirstBlkAddr_we <= buf_rqst(3) and buf_rqst(0);
--			if(buf_rqst(0) = '1' and MonBuf_avl_i = '1')then
			if(buf_rqst(0) = '1')then
				MonBuf_wa <= MonBuf_wa + 1;
			end if;
			MonBufUsed <= MonBuf_wa(9 downto 0) - MonBuf_ra(9 downto 0);
      if(inc_ddr_paSyncRegs(3 downto 2) = "10" or (MonBufOverWrite = '1' and StopOverWrite = '0' and and_reduce(MonBufUsed) = '1' and buf_rqst(0) = '1'))then
        MonBuf_ra <= MonBuf_ra + 1;
      end if;
			if(Written_MonBuf(0) = Written_MonBuf(3) and WrtMonEvtDone_l(0) = '0')then
				Written_MonBufMatch(0) <= '1';
			else
				Written_MonBufMatch(0) <= '0';
			end if;
			if((EnSFP(2 downto 0) = "111" or EnSFP(2 downto 0) = "011" or EnSFP(2 downto 0) = "110" or EnSFP(2 downto 0) = "101") and Written_MonBuf(1) = Written_MonBuf(3) and WrtMonEvtDone_l(1) = '0')then
				Written_MonBufMatch(1) <= '1';
			else
				Written_MonBufMatch(1) <= '0';
			end if;
			if(EnSFP(2 downto 0) = "111" and Written_MonBuf(2) = Written_MonBuf(3) and WrtMonEvtDone_l(2) = '0')then
				Written_MonBufMatch(2) <= '1';
			else
				Written_MonBufMatch(2) <= '0';
			end if;
			if(Written_MonBufMatch = "000" and sysDIV2 = '0')then
				Written_MonBuf(3) <= Written_MonBuf(3) + 1;
			end if;
			for i in 0 to 2 loop
				if(FirstBlkAddr_re(0)(i) = '1')then
					Written_MonBuf(i) <= FirstBlkAddrDo(0)(i)(10 downto 0);
				elsif(WrtMonBlkDone(i) = '1')then
					Written_MonBuf(i) <= Written_MonBuf(i) + 1;
				end if;
				if(FirstBlkAddr_re(1)(i) = '1')then 
					NXT_MonBuf(i) <= FirstBlkAddrDo(1)(i)(10 downto 0);
				elsif(EventBufAddr_we_i(i) = '1' and EventData_in(i)(66) = '0')then
					NXT_MonBuf(i) <= NXT_MonBuf(i) + 1;
				end if;
			end loop;
			inc_ddr_paSyncRegs <= inc_ddr_paSyncRegs(2 downto 0) & inc_ddr_pa;
		end if;
		case EnSFP(2 downto 0) is
			when "011" | "101" | "110" => AddrOffset(0)(9 downto 6) <= x"8";
			when "111" => AddrOffset(0)(9 downto 6) <= x"5";
			when others => AddrOffset(0)(9 downto 6) <= x"0";
		end case;
		AddrOffset(1)(9 downto 6) <= x"0";
		AddrOffset(2)(9 downto 6) <= x"a";
		for i in 0 to 2 loop
			EventBufAddr(i) <= NXT_MonBuf(i)(9 downto 0) & AddrOffset(i)(9 downto 6);
		end loop;
	end if;
end process;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		sysDIV2 <= not sysDIV2;
		for i in 0 to 2 loop
			if(resetSyncRegs(2) = '1')then
				header(i) <= '1';
				header_q(i) <= '0';
			elsif(EventData_we(i) = '1')then
				header(i) <= EventData_in(i)(65) or (header(i) and not header_q(i));
				header_q(i) <= header(i);
			end if;
		end loop;
		if(EnSFP(2 downto 1) = "00")then
			SFP_we(0) <= EventData_we(0);
			EoB(0) <= EventData_in(0)(64);
		else
			SFP_we(0) <= EventData_we(1) and EnSFP(0);
			EoB(0) <= EventData_in(1)(64);
		end if;
		if(EnSFP(2 downto 0) = "110")then
			SFP_we(1) <= EventData_we(1);
			EoB(1) <= EventData_in(1)(64);
		else
			SFP_we(1) <= EventData_we(0) and EnSFP(1);
			EoB(1) <= EventData_in(0)(64);
		end if;
		if(EnSFP(2 downto 0) = "111")then
			SFP_we(2) <= EventData_we(2);
			EoB(2) <= EventData_in(2)(64);
		else
			SFP_we(2) <= EventData_we(0) and EnSFP(2);
			EoB(2) <= EventData_in(0)(64);
		end if;
		if(EnSFP(2 downto 0) = "001")then
			LinkData(0) <= EventData_in(0)(65) & EventData_in(0)(63 downto 0);
			LinkCtrl(0) <= not(header(0) or EventData_in(0)(65));
			LinkWe(0) <= EventData_we(0) and not EnSFP(3);
		else
			LinkData(0) <= EventData_in(1)(65) & EventData_in(1)(63 downto 0);
			LinkCtrl(0) <= not(header(1) or EventData_in(1)(65));
			LinkWe(0) <= EventData_we(1) and EnSFP(0) and not EnSFP(3);
		end if;
		if(EnSFP(2 downto 0) = "110")then
			LinkData(1) <= EventData_in(1)(65) & EventData_in(1)(63 downto 0);
			LinkCtrl(1) <= not(header(1) or EventData_in(1)(65));
			LinkWe(1) <= EventData_we(1) and not EnSFP(3);
		else
			LinkData(1) <= EventData_in(0)(65) & EventData_in(0)(63 downto 0);
			LinkCtrl(1) <= not(header(0) or EventData_in(0)(65));
			LinkWe(1) <= EventData_we(0) and EnSFP(1) and not EnSFP(3);
		end if;
		if(EnSFP(2 downto 0) = "111")then
			LinkData(2) <= EventData_in(2)(65) & EventData_in(2)(63 downto 0);
			LinkCtrl(2) <= not(header(2) or EventData_in(2)(65));
			LinkWe(2) <= EventData_we(2) and not EnSFP(3);
		else
			LinkData(2) <= EventData_in(0)(65) & EventData_in(0)(63 downto 0);
			LinkCtrl(2) <= not(header(0) or EventData_in(0)(65));
			LinkWe(2) <= EventData_we(0) and EnSFP(2) and not EnSFP(3);
		end if;
		case EnSFP(2 downto 0) is
			when "010" | "011" | "111" => SFP2bldr_sel(0) <= "01";
			when "100" | "101" | "110" => SFP2bldr_sel(0) <= "10";
			when others => SFP2bldr_sel(0) <= "00";
		end case;
		case EnSFP(2 downto 0) is
			when "010" | "011" | "101" | "111" => SFP2bldr_sel(1) <= "00";
			when others => SFP2bldr_sel(1) <= "01";
		end case;
		case EnSFP(2 downto 0) is
			when "100" | "110" => SFP2bldr_sel(2) <= "00";
			when "101" => SFP2bldr_sel(2) <= "01";
			when others => SFP2bldr_sel(2) <= "10";
		end case;
		evt_buf_full(0) <= ((not LinkFull_n(conv_integer(SFP2bldr_sel(0))) or not LinkDown(conv_integer(SFP2bldr_sel(0)))) and EnSFP(conv_integer(SFP2bldr_sel(0))) and not EnSFP(3)) or wport_FIFO_full(0);
		if(SFP2bldr_sel(1)(0) = '0')then
			evt_buf_full(1) <= ((not LinkFull_n(0) or not LinkDown(0)) and EnSFP(0) and not EnSFP(3)) or wport_FIFO_full(1);
		else
			evt_buf_full(1) <= ((not LinkFull_n(1) or not LinkDown(1)) and EnSFP(1) and not EnSFP(3)) or wport_FIFO_full(1);
		end if;
		evt_buf_full(2) <= ((not LinkFull_n(conv_integer(SFP2bldr_sel(2))) or not LinkDown(conv_integer(SFP2bldr_sel(2)))) and EnSFP(conv_integer(SFP2bldr_sel(2))) and not EnSFP(3))  or wport_FIFO_full(2);
		if(Cntr2ms(18 downto 17) = "11" and Cntr2ms(14) = '1')then
			Cntr2ms <= (others => '0');
			LiveTimeCntr <= (others => (others => '0'));
			for i in 0 to 2 loop
				LiveTime(i) <= '0' & LiveTimeCntr(i)(18 downto 12);
			end loop;
		else
			Cntr2ms <= Cntr2ms + 1;
			for i in 0 to 2 loop
				if(LinkFull_n(i) = '1')then
					LiveTimeCntr(i) <= LiveTimeCntr(i) + 1;
				end if;
			end loop;
		end if;
	end if;
end process;
process(sysclk,rstCntr)
begin
	if(rstCntr = '1')then
		sync_loss_cntr <= (others => (others => '0'));
		sync_lossSync <= (others => (others => '0'));
	elsif(sysclk'event and sysclk = '1')then
		for i in 0 to 2 loop
			sync_lossSync(i) <= sync_lossSync(i)(1 downto 0) & sync_loss(i);
			if(sync_lossSync(i)(2) = '1')then
				sync_loss_cntr(i)(9 downto 0) <= sync_loss_cntr(i)(9 downto 0) + 1;
			end if;
		end loop;
	end if;
end process;
process(sysclk,rstCntr)
begin
	if(rstCntr = '1')then
		cmsCRC_errCntr <= (others => (others => '0'));
		EvtLength_errCntr <= (others => (others => '0'));
		AMCLength_errCntr <= (others => (others => '0'));
		AMCvalid_errCntr <= (others => (others => '0'));
		AMCcrc_errCntr <= (others => (others => '0'));
		BackPressure31 <= (others => '0');
		BackPressure31_q <= (others => '0');
		BackPressureCntr <= (others => (others => '0'));
		IsBackPressure <= '0';
		StopOverWrite <= '0';
	elsif(sysclk'event and sysclk = '1')then
		for i in 0 to 2 loop
			if(inc_err(i)(0) = '1')then
				cmsCRC_errCntr(i)(9 downto 0) <= cmsCRC_errCntr(i)(9 downto 0) + 1;
			end if;
			if(inc_err(i)(1) = '1')then
				EvtLength_errCntr(i)(9 downto 0) <= EvtLength_errCntr(i)(9 downto 0) + 1;
			end if;
			if(inc_err(i)(2) = '1')then
				AMClength_errCntr(i)(9 downto 0) <= AMClength_errCntr(i)(9 downto 0) + 1;
			end if;
			if(inc_err(i)(3) = '1')then
				AMCvalid_errCntr(i)(9 downto 0) <= AMCvalid_errCntr(i)(9 downto 0) + 1;
			end if;
			if(inc_err(i)(4) = '1')then
				AMCcrc_errCntr(i)(9 downto 0) <= AMCcrc_errCntr(i)(9 downto 0) + 1;
			end if;
			if(inc_err(i)(4) = '1')then
				AMCcrc_errCntr(i)(9 downto 0) <= AMCcrc_errCntr(i)(9 downto 0) + 1;
			end if;
			if(BackPressure31_q(i) = '1' and BackPressure31(i) = '0')then
				BackPressureCntr(i)(9 downto 0) <= BackPressureCntr(i)(9 downto 0) + 1;
			end if;
			if(IsBackPressure = '1' and Cntr_ADDR(0) = '0')then
				BackPressure31(i) <= DaqLSC_status(i)(31);
			end if;
		end loop;
		if(Cntr_ADDR(3 downto 0) = x"9")then
			IsBackPressure <= '1';
		else
			IsBackPressure <= '0';
		end if;
		BackPressure31_q <= BackPressure31;
		if(stop = '1')then
		  StopOverWrite <= '1';
		end if;
	end if;
end process;
i_check_event: check_event PORT MAP(
		clk => sysclk,
		reset => resetSyncRegs(2),
		en_stop => en_stop,
		cmsCRC_err => cmsCRC_err,
		EventData_in => EventData_in,
		EventData_we => EventData_we,
		inc_err => inc_err,
		stop => stop
	);
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		if(ipb_addr(15 downto 7) = LSC_addr(15 downto 7) and ipb_addr(6 downto 0) = "1110000" and ipb_write = '1' and ipb_strobe = '1')then
			en_stop <= ipb_wdata(4 downto 0);
		end if;
	end if;
end process;
process(Cntr_ADDR)
begin
	case Cntr_ADDR(6 downto 4) is
		when "000" =>
						Cntr_DATA <= DaqLSC_status(0)(31 downto 0);
		when "001" =>
						Cntr_DATA <= DaqLSC_status(1)(31 downto 0);
		when "010" =>
						Cntr_DATA <= DaqLSC_status(2)(31 downto 0);
		when "011" =>
			case Cntr_ADDR(3 downto 0) is
				when x"0" => Cntr_DATA <= sync_loss_cntr(0);
				when x"1" => Cntr_DATA <= sync_loss_cntr(1);
				when x"2" => Cntr_DATA <= sync_loss_cntr(2);
				when x"3" => Cntr_DATA <= cmsCRC_errCntr(0);
				when x"4" => Cntr_DATA <= cmsCRC_errCntr(1);
				when x"5" => Cntr_DATA <= cmsCRC_errCntr(2);
				when x"6" => Cntr_DATA <= EvtLength_errCntr(0);
				when x"7" => Cntr_DATA <= EvtLength_errCntr(1);
				when x"8" => Cntr_DATA <= EvtLength_errCntr(2);
				when x"a" => Cntr_DATA <= TotalEvtLengthCntr(0);
				when x"c" => Cntr_DATA <= TotalEvtLengthCntr(1);
				when x"e" => Cntr_DATA <= TotalEvtLengthCntr(2);
				when others => Cntr_DATA <= (others => '0');
			end case;
		when "100" =>
			case Cntr_ADDR(3 downto 0) is
				when x"0" => Cntr_DATA <= SFP_evt_cntr(0);
				when x"1" => Cntr_DATA <= SFP_evt_cntr(1);
				when x"2" => Cntr_DATA <= SFP_evt_cntr(2);
				when x"4" => Cntr_DATA <= SFP_word_cntr(0);
				when x"5" => Cntr_DATA <= SFP_word_cntr(1);
				when x"6" => Cntr_DATA <= SFP_word_cntr(2);
				when x"8" => Cntr_DATA <= SFP_blk_cntr(0);
				when x"9" => Cntr_DATA <= SFP_blk_cntr(1);
				when x"a" => Cntr_DATA <= SFP_blk_cntr(2);
				when others => Cntr_DATA <= (others => '0');
			end case;
		when "101" =>
			case Cntr_ADDR(3 downto 0) is
				when x"0" => Cntr_DATA <=  "00000" & Written_MonBuf(1) & "00000" & Written_MonBuf(0);
				when x"1" => Cntr_DATA <=  "00000" & Written_MonBuf(3) & "00000" & Written_MonBuf(2);
				when x"2" => Cntr_DATA <= "00000" & NXT_MonBuf(1) & "00000" & NXT_MonBuf(0);
				when x"3" => Cntr_DATA <= "00000" & MonBuf_wa & "00000" & NXT_MonBuf(2);
				when x"4" => Cntr_DATA <= x"00" & '0' & header & '0' & LinkFull_i & '0' & ReadBusy & '0' & evt_data_rdy & '0' & wport_FIFO_full & '0' & wport_rdy;
				when x"5" => Cntr_DATA <= event_cntr(0);
				when x"6" => Cntr_DATA <= event_cntr(1);
				when x"7" => Cntr_DATA <= event_cntr(2);
				when x"8" => Cntr_DATA <= word_cntr(0);
				when x"9" => Cntr_DATA <= word_cntr(1);
				when x"a" => Cntr_DATA <= word_cntr(2);
				when x"b" => Cntr_DATA <= EventBufAddr_we_cntr(0);
				when x"c" => Cntr_DATA <= EventBufAddr_we_cntr(1);
				when x"d" => Cntr_DATA <= EventBufAddr_we_cntr(2);
				when others => Cntr_DATA <= (others => '0');
			end case;
		when "110" =>
			case Cntr_ADDR(3 downto 0) is
				when x"0" => Cntr_DATA <= AMClength_errCntr(0);
				when x"1" => Cntr_DATA <= AMClength_errCntr(1);
				when x"2" => Cntr_DATA <= AMClength_errCntr(2);
				when x"4" => Cntr_DATA <= AMCvalid_errCntr(0);
				when x"5" => Cntr_DATA <= AMCvalid_errCntr(1);
				when x"6" => Cntr_DATA <= AMCvalid_errCntr(2);
				when x"8" => Cntr_DATA <= AMCcrc_errCntr(0);
				when x"9" => Cntr_DATA <= AMCcrc_errCntr(1);
				when x"a" => Cntr_DATA <= AMCcrc_errCntr(2);
				when x"c" => Cntr_DATA <= BackPressureCntr(0);
				when x"d" => Cntr_DATA <= BackPressureCntr(1);
				when x"e" => Cntr_DATA <= BackPressureCntr(2);
				when others => Cntr_DATA <= (others => '0');
			end case;
		when others => 
			case Cntr_ADDR(3 downto 0) is
				when x"0" => Cntr_DATA <= x"000000" & "000" & en_stop;
				when x"1" => Cntr_DATA <= "000" & FirstBlkAddr_ra(1)(1) & "000" & FirstBlkAddr_ra(0)(1) & "000" & FirstBlkAddr_ra(1)(0) & "000" & FirstBlkAddr_ra(0)(0);
				when x"2" => Cntr_DATA <= x"00" & "000" & FirstBlkAddr_wa & "000" & FirstBlkAddr_ra(1)(2) & "000" & FirstBlkAddr_ra(0)(2);
				when x"3" => Cntr_DATA <= x"0" & FirstBlkAddrDo(1)(0) & x"0" & FirstBlkAddrDo(0)(0);
				when x"4" => Cntr_DATA <= x"0" & FirstBlkAddrDo(1)(1) & x"0" & FirstBlkAddrDo(0)(1);
				when x"5" => Cntr_DATA <= x"0" & FirstBlkAddrDo(1)(2) & x"0" & FirstBlkAddrDo(0)(2);
				when x"6" => Cntr_DATA <= x"0000" & '0' & MonEvtQueued & '0' & WrtMonEvtDone_l & '0' & FirstBlkAddrDoValid(1) & '0' & FirstBlkAddrDoValid(0);
				when x"7" => Cntr_DATA <= x"00" & WrtMonEvtDoneCntr(2) & WrtMonEvtDoneCntr(1) & WrtMonEvtDoneCntr(0);
				when x"8" => Cntr_DATA <= x"000000" & LiveTime(0);
				when x"9" => Cntr_DATA <= x"000000" & LiveTime(1);
				when x"a" => Cntr_DATA <= x"000000" & LiveTime(2);
				when x"b" => Cntr_DATA <= "00000" & MonBuf_wa & "00000" & MonBuf_ra;
				when others => Cntr_DATA <= (others => '0');
			end case;
	end case;
end process;
i_SFP_cntr: SFP_cntr PORT MAP(
		sysclk => sysclk,
		clk125 => clk125,
		ipb_clk => ipb_clk,
		resetCntr => rstCntr,
		DB_cmd => DB_cmd,
		Cntr_DATA => Cntr_DATA,
		Cntr_ADDR => Cntr_ADDR,
		ipb_addr => ipb_addr(15 downto 0),
		ipb_rdata => ipb_rdata
	);
g_cmsCRC : for i in 0 to 2 generate
 i_cmsCRC: cmsCRC64 PORT MAP(
		clk => sysclk,
		reset => '0',
		crc_init => cmsCRC_init(i),
		trailer => LinkData(i)(64),
		crc_d => LinkData(i)(63 downto 0),
		crc_ce => LinkWe(i),
		crc => open,
		crc_err => cmsCRC_err(i),
		dout => open,
		dout_vld => open
	);
end generate;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			cmsCRC_initp <= "000";
			cmsCRC_init <= "111";
		  SFP_blk_cntr <= (others => (others => '0'));
      SFP_evt_cntr <= (others => (others => '0'));
      SFP_word_cntr <= (others => (others => '0'));
      event_cntr <= (others => (others => '0'));
      word_cntr <= (others => (others => '0'));
      EventBufAddr_we_cntr <= (others => (others => '0'));
      TotalEvtLengthCntr <= (others => (others => '0'));
		else
			for i in 0 to 2 loop
				cmsCRC_initp(i) <= LinkData(i)(64) and LinkWe(i);
			  if(LinkWe(i) = '1')then
          word_cntr(i)(9 downto 0) <= word_cntr(i)(9 downto 0) + 1;
        end if;
        if(LinkWe(i) = '1' and LinkData(i)(64) = '1')then
          event_cntr(i)(9 downto 0) <= event_cntr(i)(9 downto 0) + 1;
        end if;
        if(SFP_we(i) = '1' and EoB(i) = '1')then
          SFP_blk_cntr(i)(9 downto 0) <= SFP_blk_cntr(i)(9 downto 0) + 1;
        end if;
        if(SFP_we(i) = '1' and LinkData(i)(64) = '1')then
          SFP_evt_cntr(i)(9 downto 0) <= SFP_evt_cntr(i)(9 downto 0) + 1;
        end if;
        if(SFP_we(i) = '1')then
          SFP_word_cntr(i)(9 downto 0) <= SFP_word_cntr(i)(9 downto 0) + 1;
        end if;
        if(EventBufAddr_we_i(i) = '1')then
          EventBufAddr_we_cntr(i)(9 downto 0) <= EventBufAddr_we_cntr(i)(9 downto 0) + 1;
        end if;
				if(EventData_we(i) = '1')then
          if(EventData_in(i)(65) = '1')then
            TotalEvtLengthCntr(i)(24 downto 0) <= TotalEvtLengthCntr(i)(24 downto 0) + ('0' & EventData_in(i)(55 downto 32));
          end if;
        end if;
			end loop;
			cmsCRC_init <= cmsCRC_initp;
		end if;
	end if;
end process;
end Behavioral;

