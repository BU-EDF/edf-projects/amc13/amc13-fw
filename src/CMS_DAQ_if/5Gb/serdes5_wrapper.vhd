----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:12:42 12/19/2013 
-- Design Name: 
-- Module Name:    serdes5_wrapper - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity serdes5_wrapper is
    Port ( refclk : in  STD_LOGIC;
           DRPclk : in  STD_LOGIC;
					 sfp_pd : in array3x2;
           txusrclk_out : out  STD_LOGIC;
           qplllock : out  STD_LOGIC;
           gtx_reset : in  STD_LOGIC;
           data_valid : in  STD_LOGIC_VECTOR (2 downto 0);
           sfp_rxp : in  STD_LOGIC_VECTOR (2 downto 0);
           sfp_rxn : in  STD_LOGIC_VECTOR (2 downto 0);
           txfsmresetdone : out  STD_LOGIC_VECTOR (2 downto 0);
           rxfsmresetdone : out  STD_LOGIC_VECTOR (2 downto 0);
           rxcdrlock : out  STD_LOGIC_VECTOR (2 downto 0);
           rxnotintable : out array3x4;
           rxmcommaalignen : in  STD_LOGIC_VECTOR (2 downto 0);
           rxpcommaalignen : in  STD_LOGIC_VECTOR (2 downto 0);
           rxbyteisaligned : out  STD_LOGIC_VECTOR (2 downto 0);
           rxbyterealign : out  STD_LOGIC_VECTOR (2 downto 0);
           rxcommadet : out  STD_LOGIC_VECTOR (2 downto 0);
           rxchariscomma : out array3x4;
           rxcharisk : out array3x4;
           rxresetdone : out  STD_LOGIC_VECTOR (2 downto 0);
           txresetdone : out  STD_LOGIC_VECTOR (2 downto 0);
           txcharisk : in array3x4;
					 txdata : in array3x32;
					 rxdata : out array3x32;
           sfp_txp : out  STD_LOGIC_VECTOR (2 downto 0);
           sfp_txn : out  STD_LOGIC_VECTOR (2 downto 0));
end serdes5_wrapper;

architecture Behavioral of serdes5_wrapper is
component serdes5GpdProd_init 
generic
(
    -- Simulation attributes
    EXAMPLE_SIM_GTRESET_SPEEDUP    : string    := "FALSE";    -- Set to 1 to speed up sim reset
    EXAMPLE_SIMULATION             : integer   := 0;          -- Set to 1 for simulation
    STABLE_CLOCK_PERIOD            : integer   := 20;    --Period of the stable clock driving this state-machine, unit is [ns]
    EXAMPLE_USE_CHIPSCOPE          : integer   := 0           -- Set to 1 to use Chipscope to drive resets

);
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_IN                           : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_DATA_VALID_IN                       : in   std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y12)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT0_DRPCLK_IN                           : in   std_logic;
    GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT0_DRPEN_IN                            : in   std_logic;
    GT0_DRPRDY_OUT                          : out  std_logic;
    GT0_DRPWE_IN                            : in   std_logic;
    ------------------------------ Power-Down Ports ----------------------------
    GT0_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT0_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT0_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT0_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT0_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    GT0_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT0_RXUSRCLK_IN                         : in   std_logic;
    GT0_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT0_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    GT0_RXDISPERR_OUT                       : out  std_logic_vector(3 downto 0);
    GT0_RXNOTINTABLE_OUT                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    GT0_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT0_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT0_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    GT0_RXBYTEISALIGNED_OUT                 : out  std_logic;
    GT0_RXBYTEREALIGN_OUT                   : out  std_logic;
    GT0_RXCOMMADET_OUT                      : out  std_logic;
    GT0_RXMCOMMAALIGNEN_IN                  : in   std_logic;
    GT0_RXPCOMMAALIGNEN_IN                  : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT0_GTRXRESET_IN                        : in   std_logic;
    GT0_RXPMARESET_IN                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    GT0_RXCHARISCOMMA_OUT                   : out  std_logic_vector(3 downto 0);
    GT0_RXCHARISK_OUT                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT0_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT0_GTTXRESET_IN                        : in   std_logic;
    GT0_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT0_TXUSRCLK_IN                         : in   std_logic;
    GT0_TXUSRCLK2_IN                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT0_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT0_GTXTXN_OUT                          : out  std_logic;
    GT0_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT0_TXOUTCLK_OUT                        : out  std_logic;
    GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT0_TXCHARISK_IN                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT0_TXRESETDONE_OUT                     : out  std_logic;
   
    --_________________________________________________________________________
    --GT1  (X1Y13)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT1_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT1_DRPCLK_IN                           : in   std_logic;
    GT1_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT1_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT1_DRPEN_IN                            : in   std_logic;
    GT1_DRPRDY_OUT                          : out  std_logic;
    GT1_DRPWE_IN                            : in   std_logic;
    ------------------------------ Power-Down Ports ----------------------------
    GT1_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT1_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT1_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT1_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT1_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    GT1_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT1_RXUSRCLK_IN                         : in   std_logic;
    GT1_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT1_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    GT1_RXDISPERR_OUT                       : out  std_logic_vector(3 downto 0);
    GT1_RXNOTINTABLE_OUT                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    GT1_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT1_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT1_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    GT1_RXBYTEISALIGNED_OUT                 : out  std_logic;
    GT1_RXBYTEREALIGN_OUT                   : out  std_logic;
    GT1_RXCOMMADET_OUT                      : out  std_logic;
    GT1_RXMCOMMAALIGNEN_IN                  : in   std_logic;
    GT1_RXPCOMMAALIGNEN_IN                  : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT1_GTRXRESET_IN                        : in   std_logic;
    GT1_RXPMARESET_IN                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    GT1_RXCHARISCOMMA_OUT                   : out  std_logic_vector(3 downto 0);
    GT1_RXCHARISK_OUT                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT1_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT1_GTTXRESET_IN                        : in   std_logic;
    GT1_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT1_TXUSRCLK_IN                         : in   std_logic;
    GT1_TXUSRCLK2_IN                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT1_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT1_GTXTXN_OUT                          : out  std_logic;
    GT1_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT1_TXOUTCLK_OUT                        : out  std_logic;
    GT1_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT1_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT1_TXCHARISK_IN                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT1_TXRESETDONE_OUT                     : out  std_logic;
   
    --_________________________________________________________________________
    --GT2  (X1Y14)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT2_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT2_DRPCLK_IN                           : in   std_logic;
    GT2_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT2_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT2_DRPEN_IN                            : in   std_logic;
    GT2_DRPRDY_OUT                          : out  std_logic;
    GT2_DRPWE_IN                            : in   std_logic;
    ------------------------------ Power-Down Ports ----------------------------
    GT2_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT2_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT2_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT2_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT2_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------- Receive Ports - Clock Correction Ports -----------------
    GT2_RXCLKCORCNT_OUT                     : out  std_logic_vector(1 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT2_RXUSRCLK_IN                         : in   std_logic;
    GT2_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT2_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    GT2_RXDISPERR_OUT                       : out  std_logic_vector(3 downto 0);
    GT2_RXNOTINTABLE_OUT                    : out  std_logic_vector(3 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    GT2_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT2_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT2_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    GT2_RXBYTEISALIGNED_OUT                 : out  std_logic;
    GT2_RXBYTEREALIGN_OUT                   : out  std_logic;
    GT2_RXCOMMADET_OUT                      : out  std_logic;
    GT2_RXMCOMMAALIGNEN_IN                  : in   std_logic;
    GT2_RXPCOMMAALIGNEN_IN                  : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT2_GTRXRESET_IN                        : in   std_logic;
    GT2_RXPMARESET_IN                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    GT2_RXCHARISCOMMA_OUT                   : out  std_logic_vector(3 downto 0);
    GT2_RXCHARISK_OUT                       : out  std_logic_vector(3 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT2_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT2_GTTXRESET_IN                        : in   std_logic;
    GT2_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT2_TXUSRCLK_IN                         : in   std_logic;
    GT2_TXUSRCLK2_IN                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT2_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT2_GTXTXN_OUT                          : out  std_logic;
    GT2_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT2_TXOUTCLK_OUT                        : out  std_logic;
    GT2_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT2_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT2_TXCHARISK_IN                        : in   std_logic_vector(3 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT2_TXRESETDONE_OUT                     : out  std_logic;
   

    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    GT0_GTREFCLK0_COMMON_IN                 : in   std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    GT0_QPLLLOCK_OUT                        : out  std_logic;
    GT0_QPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_QPLLRESET_IN                        : in   std_logic


);
end component;
signal txusrclk : std_logic := '0';
signal gt0_txoutclk : std_logic := '0';
--signal  : std_logic := '0';
--signal  : std_logic_vector(2 downto 0) := (others => '0');

begin
    serdes5Gpd_init_i : serdes5GpdProd_init
    port map
    (
        SYSCLK_IN                       =>      DRPclk,
        SOFT_RESET_IN                   =>      '0',
        DONT_RESET_ON_DATA_ERROR_IN     =>      '0',
        GT0_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(0),
        GT0_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(0),
        GT0_DATA_VALID_IN               =>      data_valid(0),
        GT1_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(1),
        GT1_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(1),
        GT1_DATA_VALID_IN               =>      data_valid(1),
        GT2_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(2),
        GT2_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(2),
        GT2_DATA_VALID_IN               =>      data_valid(2),

  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X1Y12)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT0_DRPADDR_IN                  =>      (others => '0'),
        GT0_DRPCLK_IN                   =>      DRPclk,
        GT0_DRPDI_IN                    =>      (others => '0'),
        GT0_DRPDO_OUT                   =>      open,
        GT0_DRPEN_IN                    =>      '0',
        GT0_DRPRDY_OUT                  =>      open,
        GT0_DRPWE_IN                    =>      '0',
        ------------------------------ Power-Down Ports ----------------------------
        GT0_RXPD_IN                     =>      sfp_pd(0),
        GT0_TXPD_IN                     =>      sfp_pd(0),
        --------------------- RX Initialization and Reset Ports --------------------
        GT0_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT0_EYESCANDATAERROR_OUT        =>      open,
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT0_RXCDRLOCK_OUT               =>      rxcdrlock(0),
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT0_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT0_RXUSRCLK_IN                 =>      txusrclk,
        GT0_RXUSRCLK2_IN                =>      txusrclk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT0_RXDATA_OUT                  =>      rxdata(0),
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT0_RXDISPERR_OUT               =>      open,
        GT0_RXNOTINTABLE_OUT            =>      rxnotintable(0),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT0_GTXRXP_IN                   =>      sfp_rxp(0),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT0_GTXRXN_IN                   =>      sfp_rxn(0),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT0_RXBUFSTATUS_OUT             =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT0_RXBYTEISALIGNED_OUT         =>      rxbyteisaligned(0),
        GT0_RXBYTEREALIGN_OUT           =>      rxbyterealign(0),
        GT0_RXCOMMADET_OUT              =>      rxcommadet(0),
        GT0_RXMCOMMAALIGNEN_IN          =>      rxmcommaalignen(0),
        GT0_RXPCOMMAALIGNEN_IN          =>      rxpcommaalignen(0),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT0_GTRXRESET_IN                =>      gtx_reset,
        GT0_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT0_RXCHARISCOMMA_OUT           =>      rxchariscomma(0),
        GT0_RXCHARISK_OUT               =>      rxcharisk(0),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT0_RXRESETDONE_OUT             =>      rxresetdone(0),
        --------------------- TX Initialization and Reset Ports --------------------
        GT0_GTTXRESET_IN                =>      gtx_reset,
        GT0_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT0_TXUSRCLK_IN                 =>      txusrclk,
        GT0_TXUSRCLK2_IN                =>      txusrclk,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_TXDATA_IN                   =>      txdata(0),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  =>      sfp_txn(0),
        GT0_GTXTXP_OUT                  =>      sfp_txp(0),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT0_TXOUTCLK_OUT                =>      gt0_txoutclk,
        GT0_TXOUTCLKFABRIC_OUT          =>      open,
        GT0_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT0_TXCHARISK_IN                =>      txcharisk(0),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT0_TXRESETDONE_OUT             =>      txresetdone(0),


  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1  (X1Y13)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT1_DRPADDR_IN                  =>      (others => '0'),
        GT1_DRPCLK_IN                   =>      DRPclk,
        GT1_DRPDI_IN                    =>      (others => '0'),
        GT1_DRPDO_OUT                   =>      open,
        GT1_DRPEN_IN                    =>      '0',
        GT1_DRPRDY_OUT                  =>      open,
        GT1_DRPWE_IN                    =>      '0',
        ------------------------------ Power-Down Ports ----------------------------
        GT1_RXPD_IN                     =>      sfp_pd(1),
        GT1_TXPD_IN                     =>      sfp_pd(1),
        --------------------- RX Initialization and Reset Ports --------------------
        GT1_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT1_EYESCANDATAERROR_OUT        =>      open,
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT1_RXCDRLOCK_OUT               =>      rxcdrlock(1),
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT1_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT1_RXUSRCLK_IN                 =>      txusrclk,
        GT1_RXUSRCLK2_IN                =>      txusrclk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT1_RXDATA_OUT                  =>      rxdata(1),
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT1_RXDISPERR_OUT               =>      open,
        GT1_RXNOTINTABLE_OUT            =>      rxnotintable(1),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT1_GTXRXP_IN                   =>      sfp_rxp(1),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT1_GTXRXN_IN                   =>      sfp_rxn(1),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT1_RXBUFSTATUS_OUT             =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT1_RXBYTEISALIGNED_OUT         =>      rxbyteisaligned(1),
        GT1_RXBYTEREALIGN_OUT           =>      rxbyterealign(1),
        GT1_RXCOMMADET_OUT              =>      rxcommadet(1),
        GT1_RXMCOMMAALIGNEN_IN          =>      rxmcommaalignen(1),
        GT1_RXPCOMMAALIGNEN_IN          =>      rxpcommaalignen(1),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT1_GTRXRESET_IN                =>      gtx_reset,
        GT1_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT1_RXCHARISCOMMA_OUT           =>      rxchariscomma(1),
        GT1_RXCHARISK_OUT               =>      rxcharisk(1),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT1_RXRESETDONE_OUT             =>      rxresetdone(1),
        --------------------- TX Initialization and Reset Ports --------------------
        GT1_GTTXRESET_IN                =>      gtx_reset,
        GT1_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT1_TXUSRCLK_IN                 =>      txusrclk,
        GT1_TXUSRCLK2_IN                =>      txusrclk,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT1_TXDATA_IN                   =>      txdata(1),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT1_GTXTXN_OUT                  =>      sfp_txn(1),
        GT1_GTXTXP_OUT                  =>      sfp_txp(1),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT1_TXOUTCLK_OUT                =>      open,
        GT1_TXOUTCLKFABRIC_OUT          =>      open,
        GT1_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT1_TXCHARISK_IN                =>      txcharisk(1),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT1_TXRESETDONE_OUT             =>      txresetdone(1),

  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT2  (X1Y14)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT2_DRPADDR_IN                  =>      (others => '0'),
        GT2_DRPCLK_IN                   =>      DRPclk,
        GT2_DRPDI_IN                    =>      (others => '0'),
        GT2_DRPDO_OUT                   =>      open,
        GT2_DRPEN_IN                    =>      '0',
        GT2_DRPRDY_OUT                  =>      open,
        GT2_DRPWE_IN                    =>      '0',
        ------------------------------ Power-Down Ports ----------------------------
        GT2_RXPD_IN                     =>      sfp_pd(2),
        GT2_TXPD_IN                     =>      sfp_pd(2),
        --------------------- RX Initialization and Reset Ports --------------------
        GT2_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT2_EYESCANDATAERROR_OUT        =>      open,
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT2_RXCDRLOCK_OUT               =>      rxcdrlock(2),
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT2_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT2_RXUSRCLK_IN                 =>      txusrclk,
        GT2_RXUSRCLK2_IN                =>      txusrclk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT2_RXDATA_OUT                  =>      rxdata(2),
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT2_RXDISPERR_OUT               =>      open,
        GT2_RXNOTINTABLE_OUT            =>      rxnotintable(2),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT2_GTXRXP_IN                   =>      sfp_rxp(2),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT2_GTXRXN_IN                   =>      sfp_rxn(2),
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT2_RXBUFSTATUS_OUT             =>      open,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT2_RXBYTEISALIGNED_OUT         =>      rxbyteisaligned(2),
        GT2_RXBYTEREALIGN_OUT           =>      rxbyterealign(2),
        GT2_RXCOMMADET_OUT              =>      rxcommadet(2),
        GT2_RXMCOMMAALIGNEN_IN          =>      rxmcommaalignen(2),
        GT2_RXPCOMMAALIGNEN_IN          =>      rxpcommaalignen(2),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT2_GTRXRESET_IN                =>      gtx_reset,
        GT2_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT2_RXCHARISCOMMA_OUT           =>      rxchariscomma(2),
        GT2_RXCHARISK_OUT               =>      rxcharisk(2),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT2_RXRESETDONE_OUT             =>      rxresetdone(2),
        --------------------- TX Initialization and Reset Ports --------------------
        GT2_GTTXRESET_IN                =>      gtx_reset,
        GT2_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT2_TXUSRCLK_IN                 =>      txusrclk,
        GT2_TXUSRCLK2_IN                =>      txusrclk,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT2_TXDATA_IN                   =>      txdata(2),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT2_GTXTXN_OUT                  =>      sfp_txn(2),
        GT2_GTXTXP_OUT                  =>      sfp_txp(2),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT2_TXOUTCLK_OUT                =>      open,
        GT2_TXOUTCLKFABRIC_OUT          =>      open,
        GT2_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT2_TXCHARISK_IN                =>      txcharisk(2),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT2_TXRESETDONE_OUT             =>      txresetdone(2),




    --____________________________COMMON PORTS________________________________
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GT0_GTREFCLK0_COMMON_IN         =>      refclk,
        ------------------------- Common Block - QPLL Ports ------------------------
        GT0_QPLLLOCK_OUT                =>      qplllock,
        GT0_QPLLLOCKDETCLK_IN           =>      DRPclk,
        GT0_QPLLRESET_IN                =>      '0'

    );

i_txusrclk: bufg port map(i => gt0_txoutclk, o => txusrclk);
txusrclk_out <= txusrclk;

end Behavioral;

