--**************************************
--
-- Block timer and wc load random
--
--  Dominique Gigi  March 2012
--
-- To be used with Sergio vi 
-- 16 lower bit is word count in bytes
-- 16 higher bit are time before next event
--
--**************************************

LIBRARY ieee;
--LIBRARY altera_mf;
--LIBRARY altera;


USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
--LIBRARY lpm;
--USE lpm.lpm_components.all; 
--USE altera_mf.altera_mf_components.all;
--USE altera.altera_primitives_components.all;

entity memory_rnd is

port (
	RST_lowClk	: IN std_logic;
	low_clk		: IN Std_logic;
	RST_PCIClk	: IN Std_logic;
	PCIe_clk		: IN std_logic;
	
	PCIe_dt		: IN std_logic_vector(31 downto 0);
	PCIe_func	: IN std_logic_vector(15 downto 0);
	PCIe_cs		: IN std_logic;
	PCIe_wen		: IN std_logic;
	
	start			: IN std_logic;
	
	wc				: OUT std_logic_vector(15 downto 0);
	RST_EvtClk	: IN std_logic;
	evt_clk		: IN std_logic;
	trigger		: OUT std_logic;
	end_evt		: IN std_logic
	);
end memory_rnd;

architecture behavioral of memory_rnd is


--component LPM_FIFO        !!!!!!!!!!!! ALTERA VERSION !!!!!!!!!!
--generic (
--		LPM_WIDTH 			: natural;    -- MUST be greater than 0
--		LPM_WIDTHU 			: natural := 1;    -- MUST be greater than 0
--		LPM_NUMWORDS 		: natural;    -- MUST be greater than 0
--		LPM_SHOWAHEAD 		: string := "ON";
--		LPM_TYPE 			: string := L_FIFO;
--		LPM_HINT 			: string := "UNUSED");
--	port (
--		DATA 					: in std_logic_vector(LPM_WIDTH-1 downto 0);
--		CLOCK 				: in std_logic;
--		WRREQ 				: in std_logic;
--		RDREQ 				: in std_logic;
--		ACLR 					: in std_logic := '0';
--		SCLR 					: in std_logic := '0';
--		Q 						: out std_logic_vector(LPM_WIDTH-1 downto 0);
--		USEDW 				: out std_logic_vector(LPM_WIDTHU-1 downto 0);
--		FULL 					: out std_logic;
--		EMPTY 				: out std_logic);
--end component;

COMPONENT lpm_fifo       --!!!!!!!!!!!!!!!!!!!!  XILINX VERSION !!!!!!!!!!!!!!!!!!!!!
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;
	
component resync  
port (
	reset						: in std_logic;
	Free_clki				: in std_logic;
	clocki					: in std_logic;
	clocko					: in std_logic;
	input						: in std_logic;
	output					: out std_logic
	);
end component;

signal data_mux			: std_logic_vector(31 downto 0);
signal wen_mux				: std_logic;
-- signal pcie_dt_ltch	: std_logic_vector(31 downto 0);
signal low_wen_sync		: std_logic;
signal end_evt_sync		: std_logic;
signal fifo_dto 			: std_logic_vector(31 downto 0);
signal timer				: std_logic_vector(15 downto 0);
signal timer_cnt			: std_logic;
signal trig					: std_logic;
signal soft_reset			: std_logic;
 
--******************************************************************************
--************************<<  BEGIN    >>***************************************
--******************************************************************************
begin 

wen_resync:resync  
port map(
	reset				=> RST_LowClk,
	Free_clki		=> '1',
	clocki			=> low_clk,
	input				=> trig,
			
	clocko			=> PCIe_clk,
	output			=> low_wen_sync
	);

process(RST_PCIClk,PCIe_clk)
begin
	if RST_PCIClk = '0' then
		wen_mux <= '0';
		soft_reset <= '0';
	elsif rising_edge(PCIe_clk) then
		wen_mux <= '0';
		if low_wen_sync = '1' or (PCIe_wen = '1' and PCIe_func(5) = '1' and PCIe_cs = '1') then 
			wen_mux <= '1';
		end if;
		
		soft_reset <= '0';
		if (PCIe_wen = '1' and PCIe_func(0) = '1' and PCIe_cs = '1' and PCIe_dt(5) = '1' ) then
			soft_reset <= '1';
		end if;
		
		if (PCIe_wen = '1' and PCIe_func(5) = '1' and PCIe_cs = '1') then  
			data_mux <= PCIe_dt;
		else
			data_mux <= fifo_dto;
		end if;
	end if;
end process;



--fifo_rnd:LPM_FIFO     !!!!!!!!!!!!!!!!!!!!!ALTERA VERSION !!!!!!!!!!!!!!!!!!!!!!!!!
--generic map(
--	LPM_WIDTH 		=> 32,
--	LPM_WIDTHU 		=> 10,
--	LPM_NUMWORDS 	=> 1024)
--port map (
--	DATA 				=> data_mux,
--	CLOCK 			=> PCIe_clk,
--	WRREQ 			=> wen_mux,
--	RDREQ 			=> low_wen_sync,
--	ACLR 				=> soft_reset,
--	Q 					=> fifo_dto
--	);	

fifo_rnd : lpm_fifo     --!!!!!!!!!!!!!!!!!!!! XILINX VERSION !!!!!!!!!!!!!!!!!
  PORT MAP (
    clk => PCIe_clk,
    rst => soft_reset,
    din => data_mux,
    wr_en => wen_mux,
    rd_en => low_wen_sync,
    dout => fifo_dto
--    full => full,
--    empty => empty
  );
	
End_evt_resync:resync  
port map(
	reset				=> RST_EvtClk,
	Free_clki		=> '1',		
	clocki			=> evt_clk,
	input				=> end_evt,
			
	clocko			=> low_clk,
	output			=> end_evt_sync
	);

process(RST_LowClk,low_clk)
begin
	if RST_LowClk = '0' then
		timer 			<= (others => '0');	
		timer_cnt 		<= '0';
	elsif rising_edge(low_clk) then
		trig <= '0';
		if timer_cnt = '1' and timer(15 downto 1) = "000000000000000" then -- we don't chekc the lower bit in case of the delay is set to 0
			trig 			<= '1';
		end if;
		
		if timer_cnt = '1' and timer(15 downto 1) = "000000000000000" then
			timer_cnt 	<= '0';
		elsif end_evt_sync = '1' then
			timer_cnt 	<= '1';
		end if;
			
		if  timer_cnt = '1' then
			timer 		<= timer - "1";
		elsif end_evt_sync = '1' then
			timer 		<= fifo_dto(31 downto 16);
		end if;
	end if;
end process;

trigger 	<= low_wen_sync;	
wc			<= fifo_dto(15 downto 0);

end behavioral;
