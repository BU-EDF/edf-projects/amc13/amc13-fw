library IEEE;
library WORK;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

package mydefs is
	constant version : std_logic_vector(31 downto 0) := x"5E00000D";
	--this constant is defined to have 1 ms with a 125 Mhz
	constant freq_used: std_logic_vector(31 downto 0) := x"0001E848"; 
end package mydefs;

-- version .......
--
--
--*****************************************************************
-- version "5E00000D"	29/04/2016
-- error on retransmit signal increase tinout x1E0  to x200
--*****************************************************************
-- version "5E00000C"	25/04/2016
-- Bug on Timeout
-- add debug on event_ongoing and DUP Header and Trailer 
--*****************************************************************
-- version "5E00000B"	12/02/2016
-- reset the pckt_send counter by daq side
--*****************************************************************
-- version "5E00000A"	29/09/2015
-- Change the signal used to set the serdes_init
-- we used gtx_rxcdrlock (datasheet mention that this signal is reserved since 12/2012)
-- we use now rxbyteisaligned
--*****************************************************************
-- version "5E000009"	24/09/2015
-- Did some resync reset on Event_generator
--*****************************************************************
-- version "5E000008"	30/03/2015
-- Resync all reset signals
-- large RESET pulse (5 clocks) 
-- disable all write to SLINKXpress incase of LInkDown
--*****************************************************************
-- version "5E000007"	21/01/2015
-- Add the frequency measure
-- large RESET pulse (3 clocks) ;resync LINKDown and Test _mode
--*****************************************************************
-- version "5E000006"	18/12/2014
-- Change the CRC instance name :CRC_SLINKx & CRC_generator
--
--*****************************************************************
-- version "5E000005"	09/12/2014
-- registes data Uctrl and WEn in INPUT of the core
--
--*****************************************************************
-- version "5E000004"
-- fixe a bug on CRC compute in the SLINK sender part
--
--*****************************************************************
-- version "5E000003";
-- Add a retrasnmit counter
-- Counter data has move to 64  bit
--*****************************************************************
-- version "5E000002";
-- Add the reset of the FIFO between FED and Core done by the resync command
-- which reset the sync_num to '1'and flush the internal 4 buffers
--*****************************************************************
-- version "5E000001";
-- Add some status readable from FED and DAQ side
--*****************************************************************
-- version "00000000";
-- Beta version used by HCAL and TCDS
--*****************************************************************
