--###################################################
-- clock domain translate
--
-- V1.00 : clock IN should be lower that clock out
-- v2.00 : clock IN and clock out can be anything
--
--
--


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity reset_resync is
port (
	reset				: in std_logic;
	clock				: in std_logic; 

	Reset_sync		: out std_logic
	);
end reset_resync;

architecture behavioral of reset_resync is
signal reg			: std_logic_vector(1 downto 0);


--#################################################
--# here start code
--#################################################
begin

process(reset,clock)
begin
	if 	reset = '0' then
		reg	 		<= (others => '0');
	elsif rising_edge(clock) then
		reg(1)		<= reg(0);
		reg(0)		<= '1';
	end if;
end process;

Reset_sync			<= reg(1);

end behavioral;
