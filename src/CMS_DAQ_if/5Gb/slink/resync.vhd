--###################################################
-- clock domain translate
--
-- V1.00 : clock IN should be lower that clock out
-- v2.00 : clock IN and clock out can be anything
--
--
--


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity resync is
port (
	reset				: in std_logic;
	Free_clki		: in std_logic :='1'; -- '0' the clocki  is not a free runing clock -- '1' the clocki is a free running clock 
	clocki			: in std_logic;
	clocko			: in std_logic;
	input				: in std_logic;
	output			: out std_logic
	);
end resync;
architecture behavioral of resync is
signal reg			: std_logic_vector(2 downto 0);
signal mem			: std_logic;
signal glb_rst		: std_logic;


--#################################################
--# here start code
--#################################################
begin

glb_rst <= '0' when reset = '0' or reg(2) = '1' else '1';

process(glb_rst,clocki)
begin
	if glb_rst = '0' then
		reg(0) 	<= '0';
	elsif rising_edge(clocki) then
		if input = '1'  and mem = '0' then
			reg(0) <= '1';
		end if;
	end if;
end process;

process( reset,clocki)
begin
	if reset = '0' then
		mem 	<= '0';
	elsif rising_edge(clocki) then
		mem <= input and Free_clki;
	end if;

end process;

process(glb_rst,clocko)
begin
	if glb_rst = '0' then
		reg(1) <= '0';
	elsif rising_edge(clocko) then
		reg(1) <= reg(0);
	end if;
end process;

process(reset,clocko)
begin
	if rising_edge(clocko) then
		reg(2) <= reg(1);
	end if;
end process;

output <= reg(2);

end behavioral;
