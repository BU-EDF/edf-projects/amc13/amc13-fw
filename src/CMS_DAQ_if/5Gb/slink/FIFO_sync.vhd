------------------------------------------------------
-- Component FIFO
--
--  Ver 1.00
--
-- Dominique Gigi Feb 2012
------------------------------------------------------
--   
--  
-- 
--  
------------------------------------------------------
LIBRARY ieee;
-- LIBRARY altera_mf;
-- LIBRARY altera;
 


USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 

-- LIBRARY lpm;
-- USE lpm.lpm_components.all; 
-- USE altera_mf.altera_mf_components.all;
-- USE altera.altera_primitives_components.all;

entity FIFO_sync is
generic (	fifo_deep : integer := 6
	);
port 
	(
		aclr	: in std_logic; -- active low
		clk_w	: in std_logic;
		wen		: in std_logic;
		dataw	: in std_logic_vector(65 downto 0);
		almost_f: out std_logic;	-- active low
		clk_r	: in std_logic;
		datar	: out std_logic_vector(65 downto 0);
		ren		: in std_logic;
		empty	: out std_logic  -- active low
	);
end FIFO_sync;

architecture behavioral of FIFO_sync is



--***********************************************************
--**********************  ALTERA DC FIFO  *******************
--***********************************************************
-- component LPM_FIFO_DC    --!!!!!!!!!!!!!!!!!!!!!!! ALTERA VERSION
-- generic (
	-- LPM_WIDTH : natural;    -- MUST be greater than 0
	-- LPM_WIDTHU : natural := 1;    -- MUST be greater than 0
	-- LPM_NUMWORDS : natural;    -- MUST be greater than 0
	-- LPM_SHOWAHEAD : string := "ON";
	-- LPM_TYPE : string := L_FIFO_DC;
	-- UNDERFLOW_CHECKING : string := "ON";
	-- OVERFLOW_CHECKING : string := "ON";
	-- LPM_HINT : string := "UNUSED");
-- port (
	-- DATA : in std_logic_vector(LPM_WIDTH-1 downto 0);
	-- WRCLOCK : in std_logic;
	-- RDCLOCK : in std_logic;
	-- WRREQ : in std_logic;
	-- RDREQ : in std_logic;
	-- ACLR : in std_logic := '0';
	-- Q : out std_logic_vector(LPM_WIDTH-1 downto 0);
	-- WRUSEDW : out std_logic_vector(LPM_WIDTHU-1 downto 0);
	-- RDUSEDW : out std_logic_vector(LPM_WIDTHU-1 downto 0);
	-- WRFULL : out std_logic;
	-- RDFULL : out std_logic;
	-- WREMPTY : out std_logic;
	-- RDEMPTY : out std_logic);
-- end component;

COMPONENT lpm_fifo_dc     -- !!!!!!!!!!!!!!!  XILINX VERSION  !!!!!!!!!!!!!
  PORT (
  
    wr_clk : IN STD_LOGIC;
    wr_rst : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    rd_rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(65 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(65 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_data_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)

  );
END COMPONENT;

signal almost_full_reg	: std_logic;
signal resetp			: std_logic;
signal word_used		: std_logic_vector(fifo_deep-1 downto 0);


--***********************************************************
--**********************  BEGIN  ****************************
--***********************************************************
begin

resetp <= '1' when aclr = '0' else '0';

-- fifo_dc:LPM_FIFO_DC    -- !!!!!!!!!!!!!  ALTERA VERSION
-- generic MAP(
	-- LPM_WIDTH => 66,    -- MUST be greater than 0
	-- LPM_WIDTHU => fifo_deep,  -- MUST be greater than 0
	-- LPM_NUMWORDS => 64    -- MUST be greater than 0
	-- )
-- port MAP(
	-- DATA 	=> dataw,
	-- WRCLOCK => clk_w,
	-- RDCLOCK => clk_r,
	-- WRREQ 	=> wen,
	-- RDREQ 	=> ren,
	-- ACLR 	=> resetp,
	-- Q 		=> datar,
	-- WRUSEDW => word_used,
	-- RDEMPTY => empty
	-- );

fifo_dc : lpm_fifo_dc -- !!!!!!!!!!!!!!!  XILINX VERSION  !!!!!!!!!!!!!
  PORT MAP (
	 
	 wr_clk => clk_w,
    wr_rst => resetp,
    rd_clk => clk_r,
    rd_rst => resetp,
    din => dataw,
    wr_en => wen,
    rd_en => ren,
    dout => datar,
    -- full => full,
    empty => empty,
    wr_data_count => word_used
  );




process(aclr,clk_w)
begin
if aclr = '0' then
	almost_full_reg 		<= '1';
elsif rising_edge(clk_w) then
	if word_used >= "110000" then  --enable almostFull when reaches   48 data in FIFO of 64
		almost_full_reg 	<= '0';
	elsif word_used < "100101" then -- realize almostFull below 37 data in FIFO of 64
		almost_full_reg 	<= '1';
	end if;
end if;
end process;

almost_f <= almost_full_reg;

end behavioral;