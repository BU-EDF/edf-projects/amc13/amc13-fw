----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:41:05 07/29/2013 
-- Design Name: 
-- Module Name:    MPRAM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity SDP32x18 is
		generic (INIT : bitarray9x64);
    Port ( clk : in  STD_LOGIC;
					 we : in STD_LOGIC;
           DI : in  STD_LOGIC_VECTOR (17 downto 0);
           WA : in  STD_LOGIC_VECTOR (4 downto 0);
           RA : in  STD_LOGIC_VECTOR (4 downto 0);
           DO : out  STD_LOGIC_VECTOR (17 downto 0));
end SDP32x18;

architecture Behavioral of SDP32x18 is
signal DOUT : std_logic_vector(17 downto 0) := (others => '0');

begin
g_buf: for i in 0 to 2 generate
   RAM32M_inst : RAM32M
   generic map (
      INIT_A => INIT(i*3),   -- Initial contents of A port
      INIT_B => INIT(i*3+1),   -- Initial contents of B port
      INIT_C => INIT(i*3+2),   -- Initial contents of C port
      INIT_D => X"0000000000000000")    -- Initial contents of D port
   port map (
      DOA => DOUT(i*6+1 downto i*6), -- Read port A 2-bit output
      DOB => DOUT(i*6+3 downto i*6+2), -- Read port B 2-bit output
      DOC => DOUT(i*6+5 downto i*6+4), -- Read port C 2-bit output
      DOD => open, -- Read/Write port D 2-bit output
      ADDRA => RA,   -- Read port A 5-bit address input
      ADDRB => RA,   -- Read port B 5-bit address input
      ADDRC => RA,   -- Read port C 5-bit address input
      ADDRD => WA,   -- Read/Write port D 5-bit address input
      DIA => DI(i*6+1 downto i*6), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRA
      DIB => DI(i*6+3 downto i*6+2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRB
      DIC => DI(i*6+5 downto i*6+4), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRC
      DID => "00", -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRD
      WCLK => clk,  -- Write clock input
      WE => we       -- Write enable input
   );
end generate;
process(clk)
begin
	if(clk'event and clk = '1')then
		DO <= DOUT;
	end if;
end process;
end Behavioral;

