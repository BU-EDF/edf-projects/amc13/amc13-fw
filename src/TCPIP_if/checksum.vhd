----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:02:26 08/04/2010 
-- Design Name: 
-- Module Name:    checksum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- This module calculates 16-bit checksum in 1's compliment,
-- checksum output has two clock cycle latency.
-- bad_checksum output has two clock cycle latency.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity checksum is
    Port ( c : in  STD_LOGIC;
           r : in  STD_LOGIC;
           r_value : in  STD_LOGIC_VECTOR (15 downto 0);
           ce : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (16 downto 0);
           bad_chksum : out  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR (15 downto 0));
end checksum;

architecture Behavioral of checksum is
signal acc: std_logic_vector(16 downto 0) := (others => '0');
signal s_i: std_logic_vector(15 downto 0) := (others => '0');
begin
s <= s_i;
process(c)
begin
	if(c'event and c = '1')then
		if(r = '1')then
			acc(16) <= '0';
			acc(15 downto 0) <= r_value;
		elsif(ce = '1')then
			acc <= ('0' & acc(15 downto 0)) + ('0' & d(15 downto 0)) + d(16) + acc(16);
		end if;
		if(and_reduce(acc(15 downto 1)) = '1' and (acc(0) = '1' or acc(16) = '1'))then
			s_i <= (others => '1');
		else
			s_i <= not(acc(15 downto 0) + acc(16));
		end if;
		if(and_reduce(acc(15 downto 1)) = '1' and (acc(0) = '1' or acc(16) = '1'))then
			bad_chksum <= '0';
		else
			bad_chksum <= '1';
		end if;
	end if;
end process;
end Behavioral;

