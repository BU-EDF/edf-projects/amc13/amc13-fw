----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:41:15 07/29/2013 
-- Design Name: 
-- Module Name:    IP_OPTION - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TCP_OPTION is
    Port ( clk : in  STD_LOGIC;
           CLOSED : in  STD_LOGIC;
           update_TSrecent : in  STD_LOGIC;
           TSclock : in  STD_LOGIC_VECTOR (31 downto 0);
           RCV_SYN : in  STD_LOGIC;
           SND_SYN : in  STD_LOGIC;
           LISTEN : in  STD_LOGIC;
           Save_ReTx : in  STD_LOGIC;
           Save_ReTxTime : in  STD_LOGIC;
           OPTION_begin : in  STD_LOGIC;
           OPTION_end : in  STD_LOGIC;
           di : in  STD_LOGIC_VECTOR (31 downto 0);
					 scale : out STD_LOGIC_VECTOR (3 downto 0);
           DATA_OFFSET : out  STD_LOGIC_VECTOR (3 downto 0);
           MSS : out  STD_LOGIC_VECTOR (15 downto 0);
           RTT : out  STD_LOGIC_VECTOR (27 downto 0);
           OPTION_rdy : out  STD_LOGIC;
           TS_OPTION : out  STD_LOGIC;
           OldData : out  STD_LOGIC;
           rd_dout : in  STD_LOGIC;
           dout : out  STD_LOGIC_VECTOR (31 downto 0);
					 debug : out  STD_LOGIC_VECTOR (135 downto 0));
end TCP_OPTION;

architecture Behavioral of TCP_OPTION is
type state is (IDLE, OPTION_KIND, OPTION_LEN, OPTION_DATA);
signal OPTIONstate : state := IDLE;
signal counter : std_logic_vector(1 downto 0) := (others => '0');
signal len : std_logic_vector(5 downto 0) := (others => '0');
signal queue_i : std_logic_vector(31 downto 0) := (others => '0');
signal queue_o : std_logic_vector(31 downto 0) := (others => '0');
signal queue_a : std_logic_vector(3 downto 0) := (others => '1');
signal data : std_logic_vector(7 downto 0) := (others => '0');
signal data_cntr : std_logic_vector(2 downto 0) := (others => '0');
signal option_type : std_logic_vector(7 downto 0) := (others => '0');
signal SYNRCVD : std_logic := '1';
signal rd_dout_r : std_logic := '1';
signal len_byte : std_logic := '1';
signal rst_queue : std_logic := '1';
signal we_queue : std_logic := '0';
signal re_queue : std_logic := '0';
signal TSOPT : std_logic := '0';
signal WSOPT : std_logic := '0';
signal RTT_i : std_logic_vector(31 downto 0) := (others => '0');
signal TSrecent : std_logic_vector(31 downto 0) := (others => '0');
signal TScurrent : std_logic_vector(31 downto 0) := (others => '0');
signal TSechoed : std_logic_vector(31 downto 0) := (others => '0');
signal scale_i : std_logic_vector(3 downto 0) := (others => '0');
signal dout_sel : std_logic_vector(2 downto 0) := (others => '0');
signal MSS_i : std_logic_vector(15 downto 0) := (others => '0');
signal MSS_rcvd : std_logic_vector(15 downto 0) := (others => '0');
signal ReTxTime : std_logic_vector(31 downto 0) := (others => '0');
signal DeltaTime : std_logic_vector(31 downto 0) := (others => '0');
begin
debug(129) <= Save_ReTx;
debug(128) <= Save_ReTxTime;
debug(127 downto 96) <= TScurrent;
debug(95 downto 64) <= TSechoed;
debug(63 downto 32) <= ReTxTime;
debug(31 downto 0) <= DeltaTime;
--debug(128) <= OPTION_begin;
--debug(127 downto 96) <= RTT_i;
--debug(95 downto 64) <= TScurrent;
--debug(63 downto 32) <= TSechoed;
--debug(30 downto 27) <= queue_a;
--debug(26) <= re_queue;
--debug(25 downto 23) <= data_cntr;
--debug(22 downto 21) <= counter;
--debug(20) <= we_queue;
--debug(19) <= rst_queue;
--debug(18) <= OPTION_end;
--debug(17) <= OPTION_begin;
--debug(16 downto 13) <= option_type(3 downto 0);
--debug(12 downto 5) <= data;
--debug(4) <= '1' when OPTIONstate = OPTION_DATA else '0';
--debug(3) <= '1' when OPTIONstate = OPTION_LEN else '0';
--debug(2) <= '1' when OPTIONstate = OPTION_KIND else '0';
--debug(1) <= '1' when OPTIONstate = IDLE else '0';
--debug(31) <= WSOPT;
TS_OPTION <= TSOPT;
OldData <= DeltaTime(31);
process(clk, data_cntr)
variable i : integer;
begin
	i := conv_integer(data_cntr(1 downto 0));
	if(clk'event and clk = '1')then
		if(TSOPT = '1')then
			MSS_i <= MSS_rcvd - x"000a";
		else
			MSS_i <= MSS_rcvd;
		end if;
--		if(MSS_i(15 downto 13) /= "000")then
		if(MSS_i(15 downto 11) = "11111" and MSS_i(10 downto 9) /= "00")then
			MSS <= x"FA00";
		else
			MSS <= MSS_i;
		end if;
		if(OPTION_end = '1' or queue_a = x"b")then
			we_queue <= '0';
		elsif(OPTION_begin = '1')then
			we_queue <= '1';
		end if;
		if(OPTION_begin = '1')then
			SYNRCVD <= LISTEN;
		end if;
		if(OPTION_begin = '1' or rd_dout = '1')then
			TScurrent <= TSclock;
		end if;
		RTT_i <= TScurrent - TSechoed;
		if(or_reduce(RTT_i(31 downto 28)) = '1')then
			RTT <= (others => '1');
		else
			RTT <= RTT_i(27 downto 0);
		end if;
		if(rst_queue = '1' or OPTION_begin = '1')then
			queue_a <= (others => '1');
		elsif(we_queue = '1' and re_queue = '0')then
			queue_a <= queue_a + 1;
		elsif(we_queue = '0' and re_queue = '1' and queue_a /= x"f")then
			queue_a <= queue_a - 1;
		end if;
		if(Save_ReTx = '1')then
			ReTxTime <= TSclock;
		elsif(Save_ReTxTime = '1')then
			ReTxTime <= TScurrent;
		end if;
		if(OPTIONstate = IDLE)then
			DeltaTime <= TSechoed - ReTxTime;
		end if;
		if(CLOSED = '1' or (OPTION_begin = '1' and LISTEN = '1'))then
			scale_i <= (others => '0');
			MSS_rcvd <= x"0218";
			TSOPT <= '0';
			WSOPT <= '0';
		end if;
		if(rst_queue = '1' or (queue_a = x"f" and counter = "11"))then
			OPTIONstate <= IDLE;
			rst_queue <= '0';
			re_queue <= '0';
		else
			case OPTIONstate is
				when IDLE =>
					if(queue_a = x"0")then
						OPTIONstate <= OPTION_KIND;
						OPTION_rdy <= '0';
					else
						OPTION_rdy <= '1';
					end if;
					counter <= "00";
					re_queue <= '0';
				when OPTION_KIND =>
					counter <= counter + 1;
					if(counter = "10")then
						re_queue <= '1';
					else
						re_queue <= '0';
					end if;
					option_type <= data;
					case data is
						when x"00" =>
							rst_queue <= '1';
						when x"01" =>
						when others =>
							OPTIONstate <= OPTION_LEN;
					end case;
					len_byte <= '1';
				when OPTION_LEN =>
					len_byte <= '0';
					counter <= counter + 1;
					if(counter = "10")then
						re_queue <= '1';
					else
						re_queue <= '0';
					end if;
					if(len_byte = '1')then
						len <= data(5 downto 0);
						if(data(5 downto 0) = "000010")then -- option length = 2
							OPTIONstate <= OPTION_KIND;
						end if;
						case option_type is
							when x"02" | x"03" | x"08" =>
								OPTIONstate <= OPTION_DATA;
							when others => null;
						end case;
					elsif(len = "000011")then
						OPTIONstate <= OPTION_KIND;
					else
						len <= len - 1;
					end if;
					data_cntr <= (others => '0');
				when OPTION_DATA =>
					counter <= counter + 1;
					if(counter = "10")then
						re_queue <= '1';
					else
						re_queue <= '0';
					end if;
					data_cntr <= data_cntr + 1;
					if(option_type(3) = '1')then
						if(SYNRCVD = '1' and RCV_SYN = '1')then
							TSOPT <= '1';
						end if;
						if(data_cntr(2) = '1')then
							TSechoed((3-i)*8+7 downto (3-i)*8) <= data;
						elsif(update_TSrecent = '1')then
							TSrecent((3-i)*8+7 downto (3-i)*8) <= data;
						end if;
						if(data_cntr(2 downto 0) = "111")then -- TS option length is 10
							OPTIONstate <= OPTION_KIND;
						end if;
					elsif(option_type(0) = '1')then
						if(SYNRCVD = '1' and RCV_SYN = '1')then
							scale_i <= data(3 downto 0);
							WSOPT <= '1';
						end if;
						OPTIONstate <= OPTION_KIND; -- scale option length is 3
					else
						if(SYNRCVD = '1' and RCV_SYN = '1')then
							MSS_rcvd((1-i)*8+7 downto (1-i)*8) <= data;
						end if;
						if(data_cntr(0) = '1')then -- MSS option length is 4
							OPTIONstate <= OPTION_KIND;
						end if;
					end if;
				when others => OPTIONstate <= IDLE;
			end case;
		end if;
		if(WSOPT = '1' and TSOPT = '1' and SND_SYN = '1')then
			DATA_OFFSET <= x"9";
		elsif((WSOPT = '0' or SND_SYN = '0') and TSOPT = '1')then
			DATA_OFFSET <= x"8";
		elsif(WSOPT = '1' and TSOPT = '0' and SND_SYN = '1')then
			DATA_OFFSET <= x"6";
		else
			DATA_OFFSET <= x"5";
		end if;
		if(rd_dout = '1')then
			rd_dout_r <= '1';
		elsif(dout_sel(2) = '1')then
			rd_dout_r <= '0';
		end if;
		if(rd_dout_r = '1')then
			dout_sel <= dout_sel + 1;
		elsif(WSOPT = '1' and TSOPT = '0' and SND_SYN = '1')then
			dout_sel <= "011";
		else
			dout_sel <= "000";
		end if;
		case dout_sel is
			when "000" => dout <= x"0000080a";
			when "001" => dout <= TScurrent;
			when "010" => dout <= TSrecent;
			when "011" => dout <= x"01010303";
			when others => dout <= (others => '0');
		end case;
		if(WSOPT = '1')then
			scale <= scale_i;
--			case scale_i is
--				when x"0" => scale <= x"0001";
--				when x"1" => scale <= x"0002";
--				when x"2" => scale <= x"0004";
--				when x"3" => scale <= x"0008";
--				when x"4" => scale <= x"0010";
--				when x"5" => scale <= x"0020";
--				when x"6" => scale <= x"0040";
--				when x"7" => scale <= x"0080";
--				when x"8" => scale <= x"0100";
--				when x"9" => scale <= x"0200";
--				when x"a" => scale <= x"0400";
--				when x"b" => scale <= x"0800";
--				when x"c" => scale <= x"1000";
--				when x"d" => scale <= x"2000";
--				when others => scale <= x"4000";
--			end case;
		else
--			scale <= x"0001";
			scale <= x"0";
		end if;
		queue_i(31 downto 16) <= di(15 downto 0);
	end if;
end process;
process(counter,queue_o)
begin
	case counter is
		when "00" => data <= queue_o(31 downto 24);
		when "01" => data <= queue_o(23 downto 16);
		when "10" => data <= queue_o(15 downto 8);
		when others => data <= queue_o(7 downto 0);
	end case;
end process;
queue_i(15 downto 0) <= di(31 downto 16);
g_queue: for i in 0 to 31 generate
   i_queue : SRL16E
   port map (
      Q => queue_o(i),        -- SRL data output
      A0 => queue_a(0),     -- Select[0] input
      A1 => queue_a(1),     -- Select[1] input
      A2 => queue_a(2),     -- Select[2] input
      A3 => queue_a(3),     -- Select[3] input
      CE => we_queue,      -- Clock enable input
      CLK => clk,    -- Clock input
      D => queue_i(i)         -- SRL data input
   );
end generate;


end Behavioral;

