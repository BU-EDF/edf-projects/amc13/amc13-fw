----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:26:24 09/13/2013 
-- Design Name: 
-- Module Name:    RTO_CALC - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RTO_CALC is
    Port ( clk : in  STD_LOGIC;
           RTT : in  STD_LOGIC_VECTOR (15 downto 0);
           RTOmin : in  STD_LOGIC_VECTOR (15 downto 0);
           RTO_backoff : in  STD_LOGIC;
           LISTEN : in  STD_LOGIC;
           sample_RTT : in  STD_LOGIC;
           RTO : out  STD_LOGIC_VECTOR (15 downto 0);
           debug : out  STD_LOGIC_VECTOR (135 downto 0)
					 );
end RTO_CALC;

architecture Behavioral of RTO_CALC is
--constant RTOmin : std_logic_vector(15 downto 0) := x"00f4"; -- 1 second
--constant RTOmin : std_logic_vector(15 downto 0) := x"0005"; -- 20 millisecond
constant RTOmax : std_logic_vector(15 downto 0) := x"3938"; -- 60 seconds
signal first_RTT : std_logic := '1';
signal latch_RTO : std_logic := '1';
signal sample_RTT_dl : std_logic_vector(2 downto 0) := (others => '0');
signal RTO_i : std_logic_vector(16 downto 0) := (others => '0');
signal RTO_backoffed : std_logic_vector(16 downto 0) := (others => '0');
signal RTTVAR : std_logic_vector(16 downto 0) := (others => '0');
signal SRTT : std_logic_vector(15 downto 0) := (others => '0');
signal OneSubAlphaSRTT : std_logic_vector(15 downto 0) := (others => '0');
signal OneSubBetaRTTVAR : std_logic_vector(15 downto 0) := (others => '0');
signal BetaSRTT_SUB_RTT : std_logic_vector(14 downto 0) := (others => '0');
begin
debug(135 downto 84) <= (others => '0');
debug(83) <= first_RTT;
debug(82) <= sample_RTT;
debug(81 downto 65) <= RTO_i;
debug(64 downto 48) <= RTTVAR;
debug(47 downto 32) <= '0' & BetaSRTT_SUB_RTT;
debug(31 downto 16) <= SRTT;
debug(15 downto 0) <= RTT;
process(clk)
begin
	if(clk'event and clk = '1')then
		if(RTO_backoffed(16) = '1' or RTO_backoffed(15 downto 0) > RTOmax)then
			RTO <= RTOmax;
		elsif(RTO_backoffed(15 downto 0) < RTOmin)then
			RTO <= RTOmin;
		else
			RTO <= RTO_backoffed(15 downto 0);
		end if;
		if(RTO_backoff = '1')then
			latch_RTO <= '1';
		elsif(sample_RTT = '1')then
			latch_RTO <= '0';
		end if;
		if(RTO_backoff = '1')then
			RTO_backoffed(16) <= RTO_backoffed(16) or RTO_backoffed(15);
			RTO_backoffed(15 downto 0) <= RTO_backoffed(14 downto 0) & '0';
		elsif(latch_RTO = '0')then
			RTO_backoffed <= RTO_i;
		end if;
		if(first_RTT = '1')then
			RTO_i <= '0' & x"02dc"; -- 3 second
		elsif(RTTVAR(16 downto 14) /= "000")then
			RTO_i(16) <= '1';
		else
			RTO_i <= ('0' & SRTT) + ('0' & RTTVAR(13 downto 2) & "01");
		end if;
		if(LISTEN = '1' or (RTO_backoff = '1' and latch_RTO = '1'))then
			first_RTT <= '1';
		elsif(sample_RTT = '1')then
			first_RTT <= '0';
		end if;
		OneSubAlphaSRTT <= SRTT - ("000" & SRTT(15 downto 3));
		if(RTTVAR(16) = '1')then
			OneSubBetaRTTVAR <= x"c000";
		else
			OneSubBetaRTTVAR <= RTTVAR(15 downto 0) - ("00" & RTTVAR(15 downto 2));
		end if;
		BetaSRTT_SUB_RTT <= ('0' & SRTT(15 downto 2)) - ('0' & RTT(15 downto 2));
		sample_RTT_dl <= sample_RTT_dl(1 downto 0) & sample_RTT;
		if(sample_RTT_dl(2) = '1')then
			if(first_RTT = '1')then
				RTTVAR <= "00" & RTT(15 downto 1);
				SRTT <= RTT;
			else
				if(BetaSRTT_SUB_RTT(14) = '0')then
					RTTVAR <= ('0' & OneSubBetaRTTVAR) + ("00" & BetaSRTT_SUB_RTT);
				else
					RTTVAR <= ('0' & OneSubBetaRTTVAR) + ("00" & not BetaSRTT_SUB_RTT);
				end if;
				SRTT <= OneSubAlphaSRTT + ("000" & RTT(15 downto 3));
			end if;
		end if;
	end if;
end process;

end Behavioral;

