----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:23:00 08/14/2013 
-- Design Name: 
-- Module Name:    TCPdata_chksum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TCPdata_chksum is
    Port ( c : in  STD_LOGIC;
           r : in  STD_LOGIC;
           ce : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (63 downto 0);
           DATA_OFFSET : in  STD_LOGIC_VECTOR (3 downto 0);
           length_in : in  STD_LOGIC_VECTOR (12 downto 0); -- in unit of 64bit words
           en_out : in  STD_LOGIC;
           s : out  STD_LOGIC_VECTOR (15 downto 0);
           chksum : out  STD_LOGIC_VECTOR (15 downto 0);
           DATA_SIZE : out  STD_LOGIC_VECTOR (31 downto 0); -- in bytes
           DATA_LEN : out  STD_LOGIC_VECTOR (15 downto 0)); -- in bytes
end TCPdata_chksum;

architecture Behavioral of TCPdata_chksum is
signal acc: std_logic_vector(16 downto 0) := (others => '0');
signal sum: std_logic_vector(32 downto 0) := (others => '0');
signal sum1: std_logic_vector(16 downto 0) := (others => '0');
signal sum2: std_logic_vector(15 downto 0) := (others => '0');
signal ce_dl : std_logic := '0';
begin
process(c)
begin
	if(c'event and c = '1')then
		sum <= ('0' & d(63 downto 32)) + ('0' & d(31 downto 0));
		sum1 <= ('0' & sum(31 downto 16)) + ('0' & sum(15 downto 0)) + sum(32);
		sum2 <= sum1(15 downto 0) + sum1(16);
		if(r = '1')then
			acc(16) <= '0';
			acc(15 downto 0) <= x"0006";
		elsif(ce_dl = '1')then
			acc <= ('0' & acc(15 downto 0)) + ('0' & sum2) + acc(16);
		end if;
		if(en_out = '0')then
			s <= x"0006";
			DATA_LEN <= (others => '0');
		else
			s <= acc(15 downto 0) + acc(16);
			DATA_LEN <= length_in & "000";
		end if;
		chksum <= acc(15 downto 0) + acc(16);
		DATA_SIZE <= x"0000" & length_in & "000";
	end if;
end process;
i_ce_DATA_chksum : SRL16E
   port map (
      Q => ce_dl,       -- SRL data output
      A0 => '0',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => c,   -- Clock input
      D => ce        -- SRL data input
   );


end Behavioral;

