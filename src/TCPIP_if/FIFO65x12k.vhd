----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:01:22 10/08/2013 
-- Design Name: 
-- Module Name:    FIFO72x8192 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity FIFO65x12k is
		generic(ALMOSTFULL_OFFSET : bit_vector(15 downto 0) := x"0008");
    Port ( clk : in  STD_LOGIC;
           fifo_rst : in  STD_LOGIC;
           fifo_en : in  STD_LOGIC;
           di : in  STD_LOGIC_VECTOR (64 downto 0);
           we : in  STD_LOGIC;
           re : in  STD_LOGIC;
           do : out  STD_LOGIC_VECTOR (64 downto 0);
           full : out  STD_LOGIC;
           empty : out  STD_LOGIC);
end FIFO65x12k;

architecture Behavioral of FIFO65x12k is
type array2x7 is array (0 to 1) of std_logic_vector(6 downto 0);
signal fifo_re : array2X7 := (others => (others => '0'));
signal fifo_we : array2X7 := (others => (others => '0'));
type array2x65 is array (0 to 1) of std_logic_vector(64 downto 0);
signal dip : array2X65 := (others => (others => '0'));
signal dop : array2X65 := (others => (others => '0'));
signal fifo_empty : std_logic_vector(13 downto 0) := (others => '0');
signal fifo_full : std_logic_vector(13 downto 0) := (others => '0');
signal fifo_Almostfull : std_logic_vector(13 downto 0) := (others => '0');
type array15X13 is array (0 to 14) of std_logic_vector(12 downto 0);
signal rdcount : array15X13 := (others => (others => '0'));
signal wrcount : array15X13 := (others => (others => '0'));
signal BRAM_re : std_logic := '0';
signal BRAM_vld : std_logic := '0';
signal wa_equal_ra_q : std_logic := '0';
signal do_vld : std_logic_vector(1 downto 0) := (others => '0');
signal wa : std_logic_vector(12 downto 0) := (others => '0');
signal ra : std_logic_vector(12 downto 0) := (others => '0');

begin
g_FIFO_j: for j in 0 to 1 generate
	g_FIFO: for i in 0 to 6 generate
   i_FIFO : FIFO_DUALCLOCK_MACRO
   generic map (
      DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
      ALMOST_FULL_OFFSET => ALMOSTFULL_OFFSET,  -- Sets almost full threshold
      DATA_WIDTH => 9,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      FIFO_SIZE => "36Kb",            -- Target BRAM, "18Kb" or "36Kb" 
      FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
   port map (
      ALMOSTEMPTY => open,   -- 1-bit output almost empty
      ALMOSTFULL => fifo_Almostfull(j*7+i),     -- 1-bit output almost full
      DO => dop(j)(i*9+8 downto i*9),                     -- Output data, width defined by DATA_WIDTH parameter
      EMPTY => fifo_empty(j*7+i),               -- 1-bit output empty
      FULL => FIFO_full(j*7+i),                 -- 1-bit output full
      RDCOUNT => rdcount(j*7+i)(11 downto 0),           -- Output read count, width determined by FIFO depth
      RDERR => open,               -- 1-bit output read error
      WRCOUNT => wrcount(j*7+i)(11 downto 0),           -- Output write count, width determined by FIFO depth
      WRERR => open,               -- 1-bit output write error
      DI => dip(j)(i*9+8 downto i*9),                     -- Input data, width defined by DATA_WIDTH parameter
      RDCLK => clk,               -- 1-bit input read clock
      RDEN => FIFO_re(j)(i),                 -- 1-bit input read enable
      RST => fifo_rst,                   -- 1-bit input reset
      WRCLK => clk,               -- 1-bit input write clock
      WREN => FIFO_we(j)(i)                  -- 1-bit input write enable
   );
	end generate;
end generate;
dip(0) <= di;
dip(1)(62 downto 0) <= dop(0)(62 downto 0);
FIFO6463 : BRAM_SDP_MACRO
   generic map (
      BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 2,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 2)     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
   port map (
      DO => dop(1)(64 downto 63),         -- Output read data port, width defined by READ_WIDTH parameter
      DI => dip(0)(64 downto 63),         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => ra, -- Input read address, width defined by read port depth
      RDCLK => clk,   -- 1-bit input read clock
      RDEN => BRAM_re,     -- 1-bit input read port enable
      REGCE => '0',   -- 1-bit input read output register enable
      RST => '0',       -- 1-bit input reset 
      WE => "1",         -- Input write enable, width defined by write port depth
      WRADDR => wa, -- Input write address, width defined by write port depth
      WRCLK => clk,   -- 1-bit input write clock
      WREN => we      -- 1-bit input write port enable
   );
process(clk,fifo_rst)
begin
	if(fifo_rst = '1')then
		wa <= (others => '0');
		ra <= (others => '0');
		wa_equal_ra_q <= '0';
		BRAM_vld <= '0';
		do_vld <= "00";
		empty <= '1';
	elsif(clk'event and clk = '1')then
		if(we = '1' and fifo_en = '1')then
			wa <= wa + 1;
		end if;
		if(BRAM_re = '1')then
			ra <= ra + 1;
		end if;
		if(wa = ra)then
			wa_equal_ra_q <= '1';
		else
			wa_equal_ra_q <= '0';
		end if;
		if(BRAM_re = '1')then
			BRAM_vld <= '1';
		elsif(do_vld(1) = '0' or re = '1')then
			BRAM_vld <= '0';
		end if;
		if(BRAM_vld = '1')then
			do_vld(1) <= '1';
		elsif(re = '1')then
			do_vld(1) <= '0';
		end if;
		if(fifo_empty(13) = '0')then
			do_vld(0) <= '1';
		elsif(re = '1')then
			do_vld(0) <= '0';
		end if;
		if(fifo_empty(13) = '0')then
			empty <= '0';
		elsif(re = '1')then
			empty <= '1';
		end if;
	end if;
end process;
BRAM_re <= '1' when fifo_en = '1' and wa /= ra and wa_equal_ra_q = '0' and (BRAM_vld = '0' or do_vld(1) = '0' or re = '1') else '0';
process(clk)
begin
	if(clk'event and clk = '1')then
		if(do_vld(1) = '0' or re = '1')then
			do(64 downto 63) <= dop(1)(64 downto 63);
		end if;
		if(do_vld(0) = '0' or re = '1')then
			do(62 downto 0) <= dop(1)(62 downto 0);
		end if;
	end if;
end process;
g_we_re : for i in 0 to 6 generate
	FIFO_we(0)(i) <= '1' when we = '1' and FIFO_en = '1' else '0';
	FIFO_we(1)(i) <= '1' when FIFO_full(7+i) = '0' and fifo_empty(i) = '0' and FIFO_en = '1' else '0';
	FIFO_re(1)(i) <= '1' when FIFO_en = '1' and fifo_empty(13) = '0' and (do_vld(0) = '0' or re = '1') else '0';
end generate;
FIFO_re(0) <= FIFO_we(1);
full <= fifo_Almostfull(0);
end Behavioral;

