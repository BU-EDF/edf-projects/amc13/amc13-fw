----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:31:57 12/05/2012 
-- Design Name: 
-- Module Name:    link_status - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity link_status is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           Rxc : in  STD_LOGIC_VECTOR (3 downto 0);
           Rxd : in  STD_LOGIC_VECTOR (31 downto 0);
           link_fault : out  STD_LOGIC_VECTOR (1 downto 0) := "00");
end link_status;

architecture Behavioral of link_status is
type state is (INIT, COUNT, FAULT, NEW_FAULT_TYPE);
signal link_state : state := INIT;
signal local_fault : std_logic := '0';
signal remote_fault : std_logic := '0';
signal col_cnt : std_logic_vector(7 downto 0) := (others => '0');
signal seq_cnt : std_logic_vector(1 downto 0) := (others => '0');
signal last_seq_type_remote : std_logic := '0';

begin
process(clk)
begin
	if(clk'event and clk = '1')then
		if(Rxc = x"1" and Rxd = x"0100009c")then
			local_fault <= '1';
		else
			local_fault <= '0';
		end if;
		if(Rxc = x"1" and Rxd = x"0200009c")then
			remote_fault <= '1';
		else
			remote_fault <= '0';
		end if;
		if(reset = '1')then
			link_state <= INIT;
		else
			case link_state is
				when INIT =>
					col_cnt <= (others => '0');
					link_fault <= "00";
					if(local_fault = '1')then
						link_state <= COUNT;
						last_seq_type_remote <= '0';
					end if;
					if(remote_fault = '1')then
						link_state <= COUNT;
						last_seq_type_remote <= '1';
					end if;
				when COUNT =>
					col_cnt <= col_cnt + 1;
					if(local_fault = '0' and remote_fault = '0' and col_cnt(7) = '1')then
						link_state <= INIT;
					end if;
					if(local_fault = '1')then
						col_cnt <= (others => '0');
						seq_cnt <= seq_cnt + 1;
						if(last_seq_type_remote = '1')then
							link_state <= NEW_FAULT_TYPE;
						elsif(seq_cnt = "11")then
							link_state <= FAULT;
						end if;
					end if;
					if(remote_fault = '1')then
						col_cnt <= (others => '0');
						seq_cnt <= seq_cnt + 1;
						if(last_seq_type_remote = '0')then
							link_state <= NEW_FAULT_TYPE;
						elsif(seq_cnt = "11")then
							link_state <= FAULT;
						end if;
					end if;
				when FAULT =>
					link_fault <= last_seq_type_remote & not last_seq_type_remote;
					col_cnt <= col_cnt + 1;
					if(local_fault = '1')then
						col_cnt <= (others => '0');
						if(last_seq_type_remote = '1')then
							link_state <= NEW_FAULT_TYPE;
						end if;
					elsif(remote_fault = '1')then
						col_cnt <= (others => '0');
						if(last_seq_type_remote = '0')then
							link_state <= NEW_FAULT_TYPE;
						end if;
					elsif(col_cnt(7) = '1')then
						link_state <= INIT;
					end if;
				when others =>
					link_state <= COUNT;
					seq_cnt <= "01";
			end case;
		end if;
	end if;
end process;

end Behavioral;

