----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:47:17 10/04/2013 
-- Design Name: 
-- Module Name:    TCP_CC - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TCP_CC is
    Port ( clk : in  STD_LOGIC;
					 reset : in  STD_LOGIC;
					 SYNRCVD : in  STD_LOGIC;
					 RETX_TO : in  STD_LOGIC;
					 Save_ReTx : in  STD_LOGIC;
					 FastReTxStart : in  STD_LOGIC;
					 FR : in  STD_LOGIC;
					 PartialACK : in  STD_LOGIC;
					 NewDataACK : in  STD_LOGIC;
					 DupACK : in  STD_LOGIC;
           MSS : in  STD_LOGIC_VECTOR (15 downto 0);
--           CWND_max : in  STD_LOGIC_VECTOR (31 downto 0);
           SEG_WND : in  STD_LOGIC_VECTOR (31 downto 0);
           SND_UNA : in  STD_LOGIC_VECTOR (31 downto 0);
           SND_NXT : in  STD_LOGIC_VECTOR (31 downto 0);
           CWND : out  STD_LOGIC_VECTOR (31 downto 0);
           SND_WND_UL : out  STD_LOGIC_VECTOR (31 downto 0);
           debug : out  STD_LOGIC_VECTOR (271 downto 0)
					 );
end TCP_CC;

architecture Behavioral of TCP_CC is
signal SND_UNA_OLD : std_logic_vector(31 downto 0) := (others => '0');
signal DataACKed : std_logic_vector(31 downto 0) := (others => '0');
signal DataACKedSUM : std_logic_vector(31 downto 0) := (others => '0');
signal MSS_old : std_logic_vector(15 downto 0) := (others => '0');
signal MSSx2 : std_logic_vector(27 downto 0) := (others => '0');
signal MSSx3 : std_logic_vector(27 downto 0) := (others => '0');
signal CWND_i : std_logic_vector(31 downto 0) := (others => '0');
signal CWND_INC : std_logic_vector(27 downto 0) := (others => '0');
signal CWND_FAST : std_logic_vector(27 downto 0) := (others => '0');
signal SSTHRESH : std_logic_vector(27 downto 0) := (others => '0');
signal CWND_sub_SSTHRESH : std_logic_vector(27 downto 0) := (others => '0');
signal FLIGHT_SIZE : std_logic_vector(31 downto 0) := (others => '0');
signal FLIGHT_SIZEplusMSS : std_logic_vector(27 downto 0) := (others => '0');
signal StartFlightSize : std_logic_vector(27 downto 0) := (others => '0');
signal SSTHRESH_RETX : std_logic_vector(27 downto 0) := (others => '0');
signal New_TO : std_logic := '0';
signal SS : std_logic := '0';
signal CA : std_logic := '0';
--signal FR : std_logic := '0';
signal PartACK : std_logic := '0';
signal FullACK : std_logic_vector(2 downto 0) := (others => '0');
signal NewDataACK_dl : std_logic_vector(1 downto 0) := (others => '0');
signal DataACKedSUM_GT_CWND : std_logic := '0';

begin
debug(121 downto 94) <= FLIGHT_SIZE(27 downto 0);
debug(93 downto 66) <= SSTHRESH;
debug(65 downto 38) <= CWND_FAST;
debug(37 downto 10) <= CWND_i(27 downto 0);
debug(9) <= DupACK;
debug(8) <= PartialACK ;
debug(7) <= New_TO;
debug(6) <= RETX_TO;
debug(5) <= NewDataACK;
debug(4) <= FastReTxStart;
debug(3) <= PartACK;
debug(2) <= FR;
debug(1) <= CA;
debug(0) <= SS;
--debug(263 downto 232) <= FLIGHT_SIZE;
--debug(231 downto 200) <= x"0" & SSTHRESH;
--debug(199 downto 168) <= x"0" & CWND_FAST;
--debug(167 downto 136) <= x"0" & CWND_INC;
--debug(127 downto 96) <= x"0" & CWND_i;
--debug(95 downto 64) <= DataACKedSUM;
--debug(63) <= DupACK;
--debug(62) <= PartialACK ;
--debug(61) <= DataACKedSUM_GT_CWND;
--debug(59) <= New_TO;
--debug(58) <= RETX_TO;
--debug(57) <= NewDataACK;
--debug(56) <= FastReTxStart;
--debug(55) <= PartACK;
--debug(54) <= FR;
--debug(53) <= CA;
--debug(52) <= SS;
--debug(51 downto 32) <= DataACKed(19 downto 0);
--debug(31 downto 0) <= SND_UNA;
MSSx2(16 downto 1) <= MSS;
CWND <= CWND_i;
process(clk)
begin
	if(clk'event and clk = '1')then
--		if(CWND_i > CWND_max)then
--			CWND <= CWND_max;
--		else
--			CWND <= CWND_i;
--		end if;
		if(FR = '0')then
			StartFlightSize <= FLIGHT_SIZE(27 downto 0);
		end if;
		if(Save_ReTx = '1')then
			SND_WND_UL <= SND_NXT + ('0' & StartFlightSize(27 downto 1));
		end if;
		SND_UNA_OLD <= SND_UNA;
		DataACKed <= SND_UNA - SND_UNA_OLD;
		NewDataACK_dl <= NewDataACK_dl(0) & NewDataACK;
		if(CA = '0' or DataACKedSUM_GT_CWND = '1')then
			DataACKedSUM <= (others => '0');
		elsif(NewDataACK_dl(0) = '1')then
			DataACKedSUM <= DataACKedSUM + DataACKed;
		end if;
		if(CA = '1' and DataACKedSUM > CWND_i and NewDataACK_dl(1) = '1')then
			DataACKedSUM_GT_CWND <= '1';
		else
			DataACKedSUM_GT_CWND <= '0';
		end if;
		if((or_reduce(DataACKed(31 downto 16)) = '1' or DataACKed(15 downto 0) >= MSS) and FR = '1' and NewDataACK_dl(0) = '1' and PartialACK = '1')then
			PartACK <= '1';
		else
			PartACK <= '0';
		end if;
		if(SS = '1' and or_reduce(DataACKed(31 downto 16)) = '0' and DataACKed(15 downto 0) < MSS)then
			CWND_INC(15 downto 0) <= DataACKed(15 downto 0);
		else
			CWND_INC(15 downto 0) <= MSS;
		end if;
		FullACK <= FullACK(1 downto 0) & (FR and NewDataACK and not PartialACK);
		CWND_sub_SSTHRESH <= CWND_i(27 downto 0) - SSTHRESH;
		MSS_old <= MSS;
		if(reset = '1')then
			CWND_i(27 downto 0) <= x"0000430";
		elsif(MSS /= MSS_old)then
			CWND_i(27 downto 0) <= MSSx2;
		elsif(RETX_TO = '1')then
			CWND_i <= x"0000" & MSS;
		elsif(FastReTxStart = '1' or FullACK(2) = '1')then
			CWND_i(27 downto 0) <= CWND_FAST;
		elsif(FR = '1' and NewDataACK_dl(0) = '1' and PartialACK = '1')then
			if(CWND_sub_SSTHRESH > DataACKed(27 downto 0) and DataACKed(31 downto 28) = x"0")then
				CWND_i(27 downto 0) <= CWND_i(27 downto 0) - DataACKed(27 downto 0);
			else
				CWND_i(27 downto 0) <= SSTHRESH;
			end if;
		elsif(CWND_i(27) = '0' and ((SS = '1' and NewDataACK_dl(1) = '1') or DataACKedSUM_GT_CWND = '1' or (FR = '1' and DupACK = '1') or PartACK = '1'))then
			CWND_i(27 downto 0) <= CWND_i(27 downto 0) + CWND_INC;
		end if;
		MSSx3 <= MSSx2 + (x"000" & MSS);
		if(FR = '0')then
			CWND_FAST <= ('0' & FLIGHT_SIZE(27 downto 1)) + MSSx3;
		elsif(FLIGHT_SIZEplusMSS > SSTHRESH)then
			CWND_FAST <= SSTHRESH;
		else
			CWND_FAST <= FLIGHT_SIZEplusMSS; -- this is MSS added by the flight size before the NewDataACK arrives
		end if;
		if(FR = '1')then
			SS <= '0';
			CA <= '0';
		elsif(CWND_i(27 downto 0) < SSTHRESH)then
			SS <= '1';
			CA <= '0';
		else
			SS <= '0';
			CA <= '1';
		end if;
		if(reset = '1' or NewDataACK = '1')then
			New_TO <= '1';
		elsif(RETX_TO = '1')then
			New_TO <= '0';
		end if;
		if(SYNRCVD = '1')then
			if(SEG_WND(31 downto 27) = "00000")then
				SSTHRESH <= SEG_WND(27 downto 0);
			else
				SSTHRESH <= x"3ffffff";
			end if;
		elsif((RETX_TO = '1' and New_TO = '1') or FastReTxStart = '1')then
			SSTHRESH <= SSTHRESH_RETX;
		end if;
		FLIGHT_SIZE <= SND_NXT - SND_UNA;
		if(FLIGHT_SIZE > (x"000" & MSS))then
			FLIGHT_SIZEplusMSS <= FLIGHT_SIZE(27 downto 0) + (x"000" & MSS);
		else
			FLIGHT_SIZEplusMSS <= MSSx2;
		end if;
		if(FLIGHT_SIZE(27 downto 1) > MSSx2(26 downto 0))then
			SSTHRESH_RETX <= '0' & FLIGHT_SIZE(27 downto 1);
		else
			SSTHRESH_RETX <= MSSx2;
		end if;
	end if;
end process;

end Behavioral;

