----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:23:00 08/14/2013 
-- Design Name: 
-- Module Name:    TCPdata_chksum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity RETXdata_chksum is
    Port ( c : in  STD_LOGIC;
           r : in  STD_LOGIC;
           ce : in  STD_LOGIC;
           d : in  STD_LOGIC_VECTOR (31 downto 0);
           s : out  STD_LOGIC_VECTOR (15 downto 0));
end RETXdata_chksum;

architecture Behavioral of RETXdata_chksum is
signal acc: std_logic_vector(16 downto 0) := (others => '0');
signal d_sum: std_logic_vector(16 downto 0) := (others => '0');
signal r_sum: std_logic_vector(16 downto 0) := (others => '0');
signal ce_dl : std_logic := '0';
signal r_dl : std_logic := '0';
--signal length_r: std_logic_vector(10 downto 0) := (others => '0');
begin
process(c)
begin
	if(c'event and c = '1')then
		ce_dl <= ce;
		r_dl <= r;
		d_sum <= ('0' & d(31 downto 16)) + ('0' & d(15 downto 0));
		if(r_dl = '1')then
--			acc <= "00000000000000110" + ('0' & d_sum(15 downto 0)) + d_sum(16);
			acc <= "00000000000000110";
		elsif(ce_dl = '1')then
			acc <= ('0' & acc(15 downto 0)) + ('0' & d_sum(15 downto 0)) + acc(16) + d_sum(16);
		end if;
		s <= acc(15 downto 0) + acc(16);
	end if;
end process;
end Behavioral;

