----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:02:40 04/20/2013 
-- Design Name: 
-- Module Name:    XGbEMAC - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- RS is integrated into this MAC module already
-- this MAC only accepts ARP and IP4 packets
-- jumbo frame is supported, but if it's over 9KB, CRC is not efficeint any more
-- and if more than 32KB, you may run into clock correction problem unless you increase the IFG
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity XGbEMAC is
    Port ( clk : in  STD_LOGIC;
					 reset : in  STD_LOGIC;
					 enTxJumboFrame : in  STD_LOGIC;
					 enRxJumboFrame : in  STD_LOGIC;
           EmacPhyTxc : out  STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
           EmacPhyTxd : out  STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
           PhyEmacRxc : in  STD_LOGIC_VECTOR (3 downto 0);
           PhyEmacRxd : in  STD_LOGIC_VECTOR (31 downto 0);
           ClientEmacTxd : in  STD_LOGIC_VECTOR (31 downto 0);
           ClientEmacTxdVld : in  STD_LOGIC_VECTOR (3 downto 0);
           ClientEmacTxUnderrun : in  STD_LOGIC;
           EmacClientTack : out  STD_LOGIC := '0';
           EmacClientRxd : out  STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
           EmacClientRxdWe : out  STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
           EmacClientRxGoodFrame : out  STD_LOGIC := '0';
           EmacClientRxbadFrame : out  STD_LOGIC := '0'
					 );
end XGbEMAC;

architecture Behavioral of XGbEMAC is
COMPONENT EthernetCRCD32
	PORT(
		clk : IN std_logic;
		init : IN std_logic;
		ce : IN std_logic;
		d : IN std_logic_vector(31 downto 0);          
		byte_cnt : IN std_logic_vector(1 downto 0);          
		crc : OUT std_logic_vector(31 downto 0);
		bad_crc : OUT std_logic
		);
END COMPONENT;
COMPONENT link_status
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		Rxc : IN std_logic_vector(3 downto 0);          
		Rxd : IN std_logic_vector(31 downto 0);          
		link_fault : OUT std_logic_vector(1 downto 0)
		);
END COMPONENT;
type array13X32 is array(0 to 12) of bit_vector(31 downto 0);
constant Tx_const_INIT : array13X32 := (x"0000fff0",x"ffff00fe",x"ffffff0e",x"0000ff0c",x"0000ff0e",
		x"ffffff0c",x"ffff00fc",x"0000fffe",x"0000ff02",x"00000002",x"fffffffc",x"000000fc",x"fffffffe");
signal Tx_ack : std_logic := '0';
signal Tx_busy : std_logic := '0';
signal init_tx_crc : std_logic := '0';
signal padding : std_logic := '0';
signal ce_tx_crc : std_logic := '0';
signal ce_tx_crc_q : std_logic_vector(1 downto 0) := (others => '0');
signal TxErr : std_logic := '0';
signal TxOverrun : std_logic := '0';
signal TxUnderrun : std_logic := '0';
signal SendStop : std_logic := '0';
signal SendIdle : std_logic := '0';
signal SendIdle_q : std_logic := '0';
signal Tx_const_do : std_logic_vector(12 downto 0) := (others => '0');
signal Tx_const : std_logic_vector(36 downto 0) := (others => '0');
signal Txd_wc : std_logic_vector(4 downto 0) := (others => '0');
signal TxFrame_wc : std_logic_vector(9 downto 0) := (others => '0');
signal Txd_ByteCnt : std_logic_vector(1 downto 0) := (others => '0');
signal Txd : std_logic_vector(31 downto 0) := (others => '0');
signal Txd_q : std_logic_vector(31 downto 0) := (others => '0');
signal Txd_ByteCnt_q : std_logic_vector(1 downto 0) := (others => '0');
signal Txd_ByteCnt_q2 : std_logic_vector(1 downto 0) := (others => '0');
signal tx_crc : std_logic_vector(31 downto 0) := (others => '0');
signal Rx_start : std_logic := '0';
signal Rx_SFD : std_logic := '0';
signal Receive : std_logic := '0';
signal CtrlErr : std_logic := '0';
signal TypeErr : std_logic := '0';
signal init_rx_crc : std_logic := '0';
signal ce_rx_crc : std_logic := '0';
signal bad_rx_crc : std_logic := '0';
signal check_rx_crc : std_logic := '0';
signal Rxd_ByteCnt : std_logic_vector(1 downto 0) := (others => '0');
signal Rxd : std_logic_vector(31 downto 0) := (others => '0');
signal Rxd_net : std_logic_vector(31 downto 0) := (others => '0');
signal link_fault : std_logic_vector(1 downto 0) := (others => '0');
signal Rxd_wc : std_logic_vector(4 downto 0) := (others => '0');
signal RxFrame_wc : std_logic_vector(9 downto 0) := (others => '0');
begin
-- Tx logic
process(clk)
begin
	if(clk'event and clk = '1')then
		EmacClientTack <= Tx_ack and not reset;
		SendIdle_q <= SendIdle;
-- IFG is quaranteed to be more than 12 bytes 
		Tx_ack <= not reset and ClientEmacTxdVld(3) and not Tx_busy and not link_fault(1) and not link_fault(0) and SendIdle and SendIdle_q;
		if(reset = '1')then
			Tx_busy <= '0';
		elsif(ClientEmacTxdVld(3) = '1' and link_fault = "00" and SendIdle = '1' and SendIdle_q = '1')then
			Tx_busy <= '1';
		elsif(SendStop = '1' or (ce_tx_crc_q = "10" and Txd_ByteCnt_q2 /= "00"))then
			Tx_busy <= '0';
		end if;
-- minimum Ethernet length is 64 bytes
		if(reset = '1')then
			Txd_wc <= (others => '0');
		elsif(Tx_ack = '1')then
			Txd_wc <= "10001";
		elsif(Txd_wc(4) = '1')then
			Txd_wc <= Txd_wc + 1;
		end if;
-- maximum Ethernet length is 1522 bytes
		if(reset = '1' or enTxJumboFrame = '1' or SendIdle = '1')then
			TxFrame_wc <= "00" & x"84";
		elsif(ClientEmacTxdVld(1) = '1' and TxFrame_wc(9) = '0')then
			TxFrame_wc <= TxFrame_wc + 1;
		end if;
		if(SendIdle = '1')then
			padding <= '0';
		elsif(Txd_wc(4) = '1' and ClientEmacTxdVld(0) = '0')then
			padding <= '1';
		end if;
-- convert byte order from computer to network
		if(Txd_wc(4) = '1')then
			Txd_ByteCnt <= "00";
			for i in 0 to 3 loop
				if(padding = '1' or ClientEmacTxdVld(i) = '0')then
					Txd(31-i*8 downto 24-i*8) <= (others => '0');
				else
					Txd(31-i*8 downto 24-i*8) <= ClientEmacTxd(i*8+7 downto i*8);
				end if;
			end loop;
		else
			case ClientEmacTxdVld is
				when x"f" | x"0" => Txd_ByteCnt <= "00";
				when x"8" => Txd_ByteCnt <= "01";
				when x"c" => Txd_ByteCnt <= "10";
				when others => Txd_ByteCnt <= "11";
			end case;
			Txd <= ClientEmacTxd(7 downto 0) & ClientEmacTxd(15 downto 8) & ClientEmacTxd(23 downto 16) & ClientEmacTxd(31 downto 24);
		end if;
		if(ClientEmacTxUnderrun = '1' or (ClientEmacTxdVld(1) = '1' and and_reduce(TxFrame_wc(8 downto 0)) = '1'))then
			Txd(7 downto 0) <= x"fe";
		end if;
		if(reset = '1')then
			ce_tx_crc <= '0';
		elsif(Txd_wc(4) = '1')then
			ce_tx_crc <= '1';
		elsif(ClientEmacTxdVld(3) = '0' or TxOverrun = '1'or padding = '1')then
			ce_tx_crc <= '0';
		end if;
		init_tx_crc <= Tx_ack;
		if((reset = '0' and Txd_ByteCnt_q = "00" and ce_tx_crc_q(0) = '1' and ce_tx_crc = '0' and TxFrame_wc(9) = '0') or TxOverrun = '1')then
			SendStop <= '1';
		else
			SendStop <= '0';
		end if;
		if(reset = '1' or link_fault(1) = '1' or SendStop = '1' or (ce_tx_crc_q(0) = '1' and ce_tx_crc = '0' and Txd_ByteCnt_q /= "00"))then
			SendIdle <= '1';
		elsif(tx_ack = '1')then
			SendIdle <= '0';
		end if;
		ce_tx_crc_q <= ce_tx_crc_q(0) & ce_tx_crc;
		Txd_q <= Txd;
		Txd_ByteCnt_q <= Txd_ByteCnt;
		Txd_ByteCnt_q2 <= Txd_ByteCnt_q;
		Tx_const(36) <= Tx_const_do(12);
		Tx_const(35 downto 32) <= Tx_const_do(11) & Tx_const_do(11) & Tx_const_do(11) & Tx_const_do(10);
		Tx_const(31 downto 28) <= Tx_const_do(9) & Tx_const_do(8) & '0' & Tx_const_do(8);
		Tx_const(27 downto 24) <= '0' & Tx_const_do(7) & Tx_const_do(6) & Tx_const_do(7);
		Tx_const(23 downto 20) <= '0' & Tx_const_do(8) & '0' & Tx_const_do(8);
		Tx_const(19 downto 16) <= '0' & Tx_const_do(7) & Tx_const_do(11) & Tx_const_do(7);
		Tx_const(15 downto 12) <= '0' & Tx_const_do(8) & '0' & Tx_const_do(8);
		Tx_const(11 downto 8) <= '0' & Tx_const_do(7) & Tx_const_do(11) & Tx_const_do(7);
		Tx_const(7 downto 4) <= Tx_const_do(5) & Tx_const_do(4) & Tx_const_do(3) & Tx_const_do(2);
		Tx_const(3 downto 0) <= Tx_const_do(5) & Tx_const_do(1) & Tx_const_do(0) & Tx_const_do(7);
		if(Tx_const(36) = '1')then
			EmacPhyTxc <= Tx_const(35 downto 32);
			EmacPhyTxd <= Tx_const(31 downto 0);
		elsif(ce_tx_crc_q(0) = '1')then
			EmacPhyTxc <= x"0";
			case Txd_ByteCnt_q is
				when "00" => EmacPhyTxd <= Txd_q;
				when "01" => EmacPhyTxd <= tx_crc(23 downto 0) & Txd_q(7 downto 0);
				when "10" => EmacPhyTxd <= tx_crc(15 downto 0) & Txd_q(15 downto 0);
				when others => EmacPhyTxd <= tx_crc(7 downto 0) & Txd_q(23 downto 0);
			end case;
		else
			case Txd_ByteCnt_q2 is
				when "00" =>
					EmacPhyTxc <= x"0";
					EmacPhyTxd <= tx_crc;
				when "01" =>
					EmacPhyTxc <= x"e";
					EmacPhyTxd <= x"0707fd" & tx_crc(31 downto 24);
				when "10" =>
					EmacPhyTxc <= x"c";
					EmacPhyTxd <= x"07fd" & tx_crc(31 downto 16);
				when others =>
					EmacPhyTxc <= x"8";
					EmacPhyTxd <= x"fd" & tx_crc(31 downto 8);
			end case;
		end if;
		if(ClientEmacTxUnderrun = '1')then
			TxUnderrun <= '1';
		else
			TxUnderrun <= '0';
		end if;
		if(ClientEmacTxdVld(1) = '1' and and_reduce(TxFrame_wc(8 downto 0)) = '1')then
			TxOverrun <= '1';
		else
			TxOverrun <= '0';
		end if;
		TxErr <= TxUnderrun or TxOverrun;
		if(TxErr = '1')then
			EmacPhyTxc(0) <= '1';
		end if;
	end if;
end process;
i_tx_CRC: EthernetCRCD32 PORT MAP(
		clk => clk,
		init => init_tx_crc,
		ce => ce_tx_crc,
		d => Txd,
		byte_cnt => Txd_ByteCnt,
		crc => tx_crc,
		bad_crc => open
	);
g_Tx_const : for i in 0 to 12 generate
	i_Tx_const : ROM32X1
   generic map (
      INIT => Tx_const_INIT(i))
   port map (
      O => Tx_const_do(i),   -- ROM output
      A0 => init_tx_crc, -- ROM address[0] send SFD x"0d5555555"
      A1 => SendStop, -- ROM address[1] Send stop control x"f070707fd";
      A2 => SendIdle, -- ROM address[2] send idle x"f07070707"
      A3 => Tx_ack, -- ROM address[3] send start control x"1555555fb"
      A4 => link_fault(0)  -- ROM address[4] send remote_fault x"10200009c"
   );
end generate;
-- Rx logic
Rxd <= Rxd_net(7 downto 0) & Rxd_net(15 downto 8) & Rxd_net(23 downto 16) & Rxd_net(31 downto 24);
process(clk)
begin
	if(clk'event and clk = '1')then
		Rxd_net <= PhyEmacRxd;
		EmacClientRxd <= Rxd;
		if(reset = '0' and PhyEmacRxc = x"1" and PhyEmacRxd = x"555555fb")then
			Rx_start <= '1';
		else
			Rx_start <= '0';
		end if;
		if(reset = '0' and Rx_start = '1' and PhyEmacRxc = x"0" and PhyEmacRxd = x"d5555555")then
			Rx_SFD <= '1';
		else
			Rx_SFD <= '0';
		end if;
		if(reset = '1' or PhyEmacRxc = x"f" or RxFrame_wc(9) = '1')then
			ce_rx_crc <= '0';
		elsif(init_rx_crc = '1')then
			ce_rx_crc <= '1';
		end if;
		if(reset = '0' and (PhyEmacRxc = x"f" or RxFrame_wc(9) = '1') and ce_rx_crc = '1')then
			check_rx_crc <= '1';
		else
			check_rx_crc <= '0';
		end if;
		if(check_rx_crc = '1' and reset = '0')then
			EmacClientRxGoodFrame <= not (bad_rx_crc or CtrlErr or not Rxd_wc(4) or TypeErr or RxFrame_wc(9));
			EmacClientRxBadFrame <= bad_rx_crc or CtrlErr or not Rxd_wc(4) or TypeErr or RxFrame_wc(9);
		else
			EmacClientRxGoodFrame <= '0';
			EmacClientRxBadFrame <= '0';
		end if;
		case PhyEmacRxc is
			when x"e" => Rxd_ByteCnt <= "01";
			when x"c" => Rxd_ByteCnt <= "10";
			when x"8" => Rxd_ByteCnt <= "11";
			when others => Rxd_ByteCnt <= "00";
		end case;
		if(reset = '1' or PhyEmacRxc(3) = '1')then
			Receive <= '0';
		elsif(Rx_SFD = '1')then
			Receive <= '1';
		end if;
		if(reset = '1')then
			EmacClientRxdWe <= x"0";
		elsif(ce_rx_crc = '1')then
			EmacClientRxdWe(0) <= not or_reduce(PhyEmacRxc);
			EmacClientRxdWe(1) <= not or_reduce(PhyEmacRxc(2 downto 0));
			EmacClientRxdWe(2) <= not or_reduce(PhyEmacRxc(1 downto 0));
			EmacClientRxdWe(3) <= not PhyEmacRxc(0);
		else
			EmacClientRxdWe <= x"0";
		end if;
		if(reset = '1' or Rx_SFD = '1')then
			CtrlErr <= '0';
		elsif(Receive = '1')then
			if(PhyEmacRxc(0) = '1')then
				if(PhyEmacRxd(7 downto 0) /= x"fd")then
					CtrlErr <= '1';
				end if;
			elsif(PhyEmacRxc(1) = '1')then
				if(PhyEmacRxd(15 downto 8) /= x"fd")then
					CtrlErr <= '1';
				end if;
			elsif(PhyEmacRxc(2) = '1')then
				if(PhyEmacRxd(23 downto 16) /= x"fd")then
					CtrlErr <= '1';
				end if;
			elsif(PhyEmacRxc(3) = '1')then
				if(PhyEmacRxd(31 downto 24) /= x"fd")then
					CtrlErr <= '1';
				end if;
			end if;
		end if;
		if(Rxd_wc(3 downto 0) = x"4")then
			if(Rxd(31 downto 16) = x"0800" or Rxd(31 downto 16) = x"0806")then
				TypeErr <= '0';
			else
				TypeErr <= '1';
			end if;
		end if;
-- minimum Ethernet length is 64 bytes
		if(reset = '1' or Rx_start = '1')then
			Rxd_wc <= (others => '0');
		elsif(Rxd_wc(4) = '0' and PhyEmacRxc(3) = '0')then
			Rxd_wc <= Rxd_wc + 1;
		end if;
-- maximum Ethernet length is 1522 bytes
		if(reset = '1' or enRxJumboFrame = '1' or Receive = '0')then
			RxFrame_wc <= "00" & x"84";
		elsif(PhyEmacRxc(2) = '0' and RxFrame_wc(9) = '0')then
			RxFrame_wc <= RxFrame_wc + 1;
		end if;
	end if;
end process;
i_rx_CRC: EthernetCRCD32 PORT MAP(
		clk => clk,
		init => init_rx_crc,
		ce => ce_rx_crc,
		d => Rxd_net,
		byte_cnt => Rxd_ByteCnt,
		crc => open,
		bad_crc => bad_rx_crc
	);
init_rx_crc <= Rx_SFD;
i_link_status: link_status PORT MAP(
		clk => clk,
		reset => reset,
		Rxc => PhyEmacRxc,
		Rxd => PhyEmacRxd,
		link_fault => link_fault
	);
end Behavioral;

