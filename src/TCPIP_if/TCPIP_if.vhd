----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:07:47 10/07/2013 
-- Design Name: 
-- Module Name:    TCPIP_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use IEEE.numeric_std.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity TCPIP_if is
		generic (simulation : boolean := false; en_KEEPALIVE : std_logic := '0');
    Port ( sysclk : in  STD_LOGIC;
           DRPclk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           TCPreset : in  STD_LOGIC;
           rstCntr : in  STD_LOGIC;
           test : in  STD_LOGIC;
					 SN : IN std_logic_vector(8 downto 0);
           Dis_pd : in  STD_LOGIC;
					 enSFP : IN std_logic_vector(3 downto 0);
					 SFP_down : OUT std_logic_vector(2 downto 0);
           inc_ddr_pa : in  STD_LOGIC;
-- event data in
					 evt_data_rdy : in std_logic_vector(2 downto 0);
					 EventData_in : in array3X67;
					 EventData_we : in   std_logic_VECTOR(2 downto 0);
					 EventData_re : out   std_logic_VECTOR(2 downto 0); -- 
					 evt_buf_full : out std_logic_vector(2 downto 0);
					 buf_rqst : in std_logic_vector(3 downto 0);
					 WaitMonBuf : IN std_logic;
           MonBufOverWrite : in  STD_LOGIC;
           TCPBuf_avl : out  STD_LOGIC;
           MonBuf_avl : out  STD_LOGIC;
           MonBuf_empty : out  STD_LOGIC;
           MonBufOvfl : out  STD_LOGIC;
					 mon_evt_cnt : out std_logic_vector(31 downto 0);
					 WrtMonBlkDone : in  STD_LOGIC_VECTOR(2 downto 0);
					 WrtMonEvtDone : in  STD_LOGIC_VECTOR(2 downto 0);
					 KiloByte_toggle : in  STD_LOGIC_VECTOR(2 downto 0);
					 EoB_toggle : in  STD_LOGIC_VECTOR(2 downto 0);
-- ddr wportA status
					 wport_rdy : in std_logic_vector(2 downto 0);
					 wport_FIFO_full : in std_logic_vector(2 downto 0);
-- signal to ddr_if, AMC_if to start moving data
					 EventBufAddr_we : out std_logic_VECTOR(2 downto 0);
					 EventBufAddr : out array3X14;
-- ddr wportB signals in sysclk domain
					 TCPclk : out STD_LOGIC;
					 TCP_dout : out std_logic_vector(31 downto 0); -- TCP data are written in unit of 32-bit words
					 TCP_channel : out std_logic_vector(1 downto 0); -- Each entry has four 32bit words, each address saves two entries. Addresses are kept in ddr_wportB
					 TCP_we : out STD_LOGIC;
					 TCP_wcount : in  STD_LOGIC_VECTOR (2 downto 0);
-- ddr rport signals
					 TCP_raddr : out  STD_LOGIC_VECTOR(28 downto 0); -- 28-26 encoded request source 25-0 address in 64 bit word
					 TCP_length	: out  STD_LOGIC_VECTOR(12 downto 0); -- in 64 bit word, actual length - 1
					 TCP_rrqst : out  STD_LOGIC;
					 TCP_rack	: in  STD_LOGIC;
					 TCP_din_type	: in  STD_LOGIC_VECTOR(2 downto 0); -- TCP data destination
					 TCP_din	: in  STD_LOGIC_VECTOR(31 downto 0); -- TCP data are written in unit of 32-bit words
					 TCP_din_valid	: in  STD_LOGIC;
					 TCP_lastword	: in  STD_LOGIC;
-- SFP ports
           SFP0_RXN : in  STD_LOGIC;
           SFP0_RXP : in  STD_LOGIC;
           SFP1_RXN : in  STD_LOGIC;
           SFP1_RXP : in  STD_LOGIC;
           SFP2_RXN : in  STD_LOGIC;
           SFP2_RXP : in  STD_LOGIC;
           SFP0_TXN : out  STD_LOGIC;
           SFP0_TXP : out  STD_LOGIC;
           SFP1_TXN : out  STD_LOGIC;
           SFP1_TXP : out  STD_LOGIC;
           SFP2_TXN : out  STD_LOGIC;
           SFP2_TXP : out  STD_LOGIC;
           SFP_REFCLK_N : in  STD_LOGIC;
           SFP_REFCLK_P : in  STD_LOGIC;
					 cs_out : out std_logic_vector(511 downto 0);
--	ipbus signals
					 ipb_clk : in  STD_LOGIC;
					 ipb_write : in  STD_LOGIC;
					 ipb_strobe : in  STD_LOGIC;
					 ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_wdata : in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0)
					 );
end TCPIP_if;

architecture Behavioral of TCPIP_if is
COMPONENT TCPIP
	generic (simulation : boolean := false; en_KEEPALIVE : std_logic := '0');
	PORT(
		reset : IN std_logic;
    rstCntr : in  STD_LOGIC;
		sysclk : IN std_logic;
		clk2x : IN std_logic;
		clk : IN std_logic;
		strobe_us : IN std_logic;
		strobe_ms : IN std_logic;
		en_LINK : IN std_logic;
		MY_PORT : IN std_logic_vector(15 downto 0);
		MY_IP : IN std_logic_vector(31 downto 0);
		MY_ETH : IN std_logic_vector(47 downto 0);
--		CWND_max : IN std_logic_vector(31 downto 0);
		RTOmin : IN std_logic_vector(15 downto 0);
		rate_limit : IN std_logic_vector(7 downto 0);
		TSclock : IN std_logic_vector(31 downto 0);
		EVENTdata : IN std_logic_vector(66 downto 0);
		EventBufAddr : IN std_logic_vector(13 downto 0);
		EventBufAddr_we : IN std_logic;
		AddrBuf_full : OUT std_logic;
		EVENTdata_avl : IN std_logic;
		DDR2TCPdata : IN std_logic_vector(32 downto 0);
		RETXdata_we : IN std_logic_vector(1 downto 0);
		RETXdata_chksum : IN std_logic_vector(15 downto 0);
		re_RETX_ddr_wq : IN std_logic;
		RETX_ddr_data_we : IN std_logic;
		RETXdataACK : IN std_logic;
		KiloByte_toggle : in  STD_LOGIC;
		EoB_toggle : in  STD_LOGIC;
		TCP_wcount : in  STD_LOGIC;
		PhyEmacRxC : IN std_logic_vector(3 downto 0);
		PhyEmacRxD : IN std_logic_vector(31 downto 0);          
		LINK_down : OUT std_logic;
		EVENTdata_re : OUT std_logic;
		RETX_ddr_out : OUT std_logic_vector(31 downto 0);
		RETX_ddr_wrqst : OUT std_logic;
		RETX_ddr_LEN_max : IN std_logic_vector(4 downto 0);
		RETX_ddr_LEN : OUT std_logic_vector(4 downto 0);
		RETX_ddr_rrqst : OUT std_logic;
		RETXdataRqst : OUT std_logic;
		RETXdataAddr : OUT std_logic_vector(25 downto 0);
		RETXdataLEN : OUT std_logic_vector(12 downto 0);
		UNA_MonBuf : OUT std_logic_vector(10 downto 0);
		UNA_TCPBuf : OUT std_logic_vector(10 downto 0);
		EmacPhyTxC : OUT std_logic_vector(3 downto 0);
		EmacPhyTxD : OUT std_logic_vector(31 downto 0);
		ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
		ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0);
		cs_out : OUT std_logic_vector(511 downto 0)
		);
END COMPONENT;
COMPONENT XGbEPCS32
	PORT(
			 reset : IN  std_logic;
			 clk2x : IN  std_logic;
			 clk : IN  std_logic;
			 TXUSRCLK : IN  std_logic;
			 TX_high : IN  std_logic;
			 RXUSRCLK : IN  std_logic;
			 RXRESETDONE : IN std_logic;
			 inh_TX : IN std_logic;
			 RESET_TXSync : IN std_logic;
			 GTX_TXD : OUT  std_logic_vector(31 downto 0);
			 GTX_TXHEADER : OUT  std_logic_vector(1 downto 0);
			 GTX_TX_PAUSE : IN  std_logic;
			 GTX_RXD : IN  std_logic_vector(31 downto 0);
			 GTX_RXDVLD : IN  std_logic;
			 GTX_RXHEADER : IN  std_logic_vector(1 downto 0);
			 GTX_RXHEADERVLD : IN  std_logic;
			 GTX_RXGOOD : OUT  std_logic;
			 GTX_RXGEARBOXSLIP_OUT : OUT  std_logic;
			 EmacPhyTxC : IN  std_logic_vector(3 downto 0);
			 EmacPhyTxD : IN  std_logic_vector(31 downto 0);
			 PhyEmacRxC : OUT  std_logic_vector(3 downto 0);
			 PhyEmacRxD : OUT  std_logic_vector(31 downto 0)
			);
END COMPONENT;
COMPONENT FIFO_RESET_7S
	PORT(
		reset : IN std_logic;
		clk : IN std_logic;          
		fifo_rst : OUT std_logic;
		fifo_en : OUT std_logic
		);
END COMPONENT;
component SFP3_v2_7_init
generic
(
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";          -- simulation setting for GT SecureIP model
    EXAMPLE_SIMULATION                      : integer   := 0;               -- Set to 1 for simulation
    STABLE_CLOCK_PERIOD                     : integer   := 20;               --Period of the stable clock driving this state-machine, unit is [ns]
    EXAMPLE_USE_CHIPSCOPE                   : integer   := 0                -- Set to 1 to use Chipscope to drive resets

);
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_IN                           : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;
    GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT1_DATA_VALID_IN                       : in   std_logic;
    GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT2_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y12)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT0_DRPCLK_IN                           : in   std_logic;
    GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT0_DRPEN_IN                            : in   std_logic;
    GT0_DRPRDY_OUT                          : out  std_logic;
    GT0_DRPWE_IN                            : in   std_logic;
    ------------------------------- Loopback Ports -----------------------------
    GT0_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ----------------------------
    GT0_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT0_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT0_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT0_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT0_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT0_RXUSRCLK_IN                         : in   std_logic;
    GT0_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT0_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT0_RXPRBSERR_OUT                       : out  std_logic;
    GT0_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT0_RXPRBSCNTRESET_IN                   : in   std_logic;
    --------------------------- Receive Ports - RX AFE -------------------------
    GT0_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT0_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT0_RXBUFRESET_IN                       : in   std_logic;
    GT0_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    GT0_RXOUTCLK_OUT                        : out  std_logic;
    ---------------------- Receive Ports - RX Gearbox Ports --------------------
    GT0_RXDATAVALID_OUT                     : out  std_logic;
    GT0_RXHEADER_OUT                        : out  std_logic_vector(1 downto 0);
    GT0_RXHEADERVALID_OUT                   : out  std_logic;
    --------------------- Receive Ports - RX Gearbox Ports  --------------------
    GT0_RXGEARBOXSLIP_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT0_GTRXRESET_IN                        : in   std_logic;
    GT0_RXPMARESET_IN                       : in   std_logic;
    ------------------ Receive Ports - RX Margin Analysis ports ----------------
    GT0_RXLPMEN_IN                          : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT0_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT0_GTTXRESET_IN                        : in   std_logic;
    GT0_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT0_TXUSRCLK_IN                         : in   std_logic;
    GT0_TXUSRCLK2_IN                        : in   std_logic;
    --------------- Transmit Ports - TX Configurable Driver Ports --------------
    GT0_TXDIFFCTRL_IN                       : in   std_logic_vector(3 downto 0);
    GT0_TXINHIBIT_IN                        : in   std_logic;
    GT0_TXMAINCURSOR_IN                     : in   std_logic_vector(6 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT0_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT0_GTXTXN_OUT                          : out  std_logic;
    GT0_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT0_TXOUTCLK_OUT                        : out  std_logic;
    GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT0_TXHEADER_IN                         : in   std_logic_vector(1 downto 0);
    GT0_TXSEQUENCE_IN                       : in   std_logic_vector(6 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT0_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT0_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

    --GT1  (X1Y13)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT1_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT1_DRPCLK_IN                           : in   std_logic;
    GT1_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT1_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT1_DRPEN_IN                            : in   std_logic;
    GT1_DRPRDY_OUT                          : out  std_logic;
    GT1_DRPWE_IN                            : in   std_logic;
    ------------------------------- Loopback Ports -----------------------------
    GT1_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ----------------------------
    GT1_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT1_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT1_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT1_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT1_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT1_RXUSRCLK_IN                         : in   std_logic;
    GT1_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT1_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT1_RXPRBSERR_OUT                       : out  std_logic;
    GT1_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT1_RXPRBSCNTRESET_IN                   : in   std_logic;
    --------------------------- Receive Ports - RX AFE -------------------------
    GT1_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT1_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT1_RXBUFRESET_IN                       : in   std_logic;
    GT1_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    GT1_RXOUTCLK_OUT                        : out  std_logic;
    ---------------------- Receive Ports - RX Gearbox Ports --------------------
    GT1_RXDATAVALID_OUT                     : out  std_logic;
    GT1_RXHEADER_OUT                        : out  std_logic_vector(1 downto 0);
    GT1_RXHEADERVALID_OUT                   : out  std_logic;
    --------------------- Receive Ports - RX Gearbox Ports  --------------------
    GT1_RXGEARBOXSLIP_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT1_GTRXRESET_IN                        : in   std_logic;
    GT1_RXPMARESET_IN                       : in   std_logic;
    ------------------ Receive Ports - RX Margin Analysis ports ----------------
    GT1_RXLPMEN_IN                          : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT1_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT1_GTTXRESET_IN                        : in   std_logic;
    GT1_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT1_TXUSRCLK_IN                         : in   std_logic;
    GT1_TXUSRCLK2_IN                        : in   std_logic;
    --------------- Transmit Ports - TX Configurable Driver Ports --------------
    GT1_TXDIFFCTRL_IN                       : in   std_logic_vector(3 downto 0);
    GT1_TXINHIBIT_IN                        : in   std_logic;
    GT1_TXMAINCURSOR_IN                     : in   std_logic_vector(6 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT1_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT1_GTXTXN_OUT                          : out  std_logic;
    GT1_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT1_TXOUTCLK_OUT                        : out  std_logic;
    GT1_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT1_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT1_TXHEADER_IN                         : in   std_logic_vector(1 downto 0);
    GT1_TXSEQUENCE_IN                       : in   std_logic_vector(6 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT1_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT1_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);

    --GT2  (X1Y14)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
    GT2_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT2_DRPCLK_IN                           : in   std_logic;
    GT2_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT2_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT2_DRPEN_IN                            : in   std_logic;
    GT2_DRPRDY_OUT                          : out  std_logic;
    GT2_DRPWE_IN                            : in   std_logic;
    ------------------------------- Loopback Ports -----------------------------
    GT2_LOOPBACK_IN                         : in   std_logic_vector(2 downto 0);
    ------------------------------ Power-Down Ports ----------------------------
    GT2_RXPD_IN                             : in   std_logic_vector(1 downto 0);
    GT2_TXPD_IN                             : in   std_logic_vector(1 downto 0);
    --------------------- RX Initialization and Reset Ports --------------------
    GT2_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT2_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT2_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT2_RXUSRCLK_IN                         : in   std_logic;
    GT2_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT2_RXDATA_OUT                          : out  std_logic_vector(31 downto 0);
    ------------------- Receive Ports - Pattern Checker Ports ------------------
    GT2_RXPRBSERR_OUT                       : out  std_logic;
    GT2_RXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - Pattern Checker ports ------------------
    GT2_RXPRBSCNTRESET_IN                   : in   std_logic;
    --------------------------- Receive Ports - RX AFE -------------------------
    GT2_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT2_GTXRXN_IN                           : in   std_logic;
    ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
    GT2_RXBUFRESET_IN                       : in   std_logic;
    GT2_RXBUFSTATUS_OUT                     : out  std_logic_vector(2 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    GT2_RXOUTCLK_OUT                        : out  std_logic;
    ---------------------- Receive Ports - RX Gearbox Ports --------------------
    GT2_RXDATAVALID_OUT                     : out  std_logic;
    GT2_RXHEADER_OUT                        : out  std_logic_vector(1 downto 0);
    GT2_RXHEADERVALID_OUT                   : out  std_logic;
    --------------------- Receive Ports - RX Gearbox Ports  --------------------
    GT2_RXGEARBOXSLIP_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT2_GTRXRESET_IN                        : in   std_logic;
    GT2_RXPMARESET_IN                       : in   std_logic;
    ------------------ Receive Ports - RX Margin Analysis ports ----------------
    GT2_RXLPMEN_IN                          : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT2_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT2_GTTXRESET_IN                        : in   std_logic;
    GT2_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT2_TXUSRCLK_IN                         : in   std_logic;
    GT2_TXUSRCLK2_IN                        : in   std_logic;
    --------------- Transmit Ports - TX Configurable Driver Ports --------------
    GT2_TXDIFFCTRL_IN                       : in   std_logic_vector(3 downto 0);
    GT2_TXINHIBIT_IN                        : in   std_logic;
    GT2_TXMAINCURSOR_IN                     : in   std_logic_vector(6 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT2_TXDATA_IN                           : in   std_logic_vector(31 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT2_GTXTXN_OUT                          : out  std_logic;
    GT2_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT2_TXOUTCLK_OUT                        : out  std_logic;
    GT2_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT2_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT2_TXHEADER_IN                         : in   std_logic_vector(1 downto 0);
    GT2_TXSEQUENCE_IN                       : in   std_logic_vector(6 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT2_TXRESETDONE_OUT                     : out  std_logic;
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    GT2_TXPRBSSEL_IN                        : in   std_logic_vector(2 downto 0);


    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    GT0_GTREFCLK0_COMMON_IN                 : in   std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    GT0_QPLLLOCK_OUT                        : out  std_logic;
    GT0_QPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_QPLLRESET_IN                        : in   std_logic


);
end component;
COMPONENT RETXdata_chksum
	PORT(
		c : IN std_logic;
		r : IN std_logic;
		ce : IN std_logic;
		d : IN std_logic_vector(31 downto 0);          
		s : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;
COMPONENT check_event
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		en_stop : IN std_logic_vector(4 downto 0);
		cmsCRC_err : IN std_logic_vector(2 downto 0);
		EventData_in : IN array3X67;
		EventData_we : IN std_logic_vector(2 downto 0);          
		inc_err : OUT array3X5;
		stop : OUT std_logic
		);
END COMPONENT;
COMPONENT RAM32x6D
	PORT(
		wclk : IN std_logic;
		rclk : IN std_logic;
		di : IN std_logic_vector(5 downto 0);
		we : IN std_logic;
		wa : IN std_logic_vector(4 downto 0);
		ra : IN std_logic_vector(4 downto 0);
		ceReg : IN std_logic;          
		do : OUT std_logic_vector(5 downto 0)
		);
END COMPONENT;
COMPONENT RAM32x6Db
	PORT(
		wclk : IN std_logic;
		di : IN std_logic_vector(5 downto 0);
		we : IN std_logic;
		wa : IN std_logic_vector(4 downto 0);
		ra : IN std_logic_vector(4 downto 0);          
		do : OUT std_logic_vector(5 downto 0)
		);
END COMPONENT;
COMPONENT cmsCRC64
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		crc_init : IN std_logic;
		trailer : IN std_logic;
		crc_d : IN std_logic_vector(63 downto 0);
		crc_ce : IN std_logic;          
		crc : OUT std_logic_vector(15 downto 0);
		crc_err : OUT std_logic;
		dout : OUT std_logic_vector(63 downto 0);
		dout_vld : OUT std_logic
		);
END COMPONENT;
signal resetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal ClientClk2XresetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal refclk : std_logic := '0';
signal REFCLK2XPLLRST : std_logic := '0';
signal refclk2x_in : std_logic := '0';
signal ClientClk2x_dcm : std_logic := '0';
signal ClientClk2x : std_logic := '0';
signal ClientClk_dcm : std_logic := '0';
signal ClientClk : std_logic := '0';
signal ClientClk_lock : std_logic := '0';
signal ClientClkToggle : std_logic := '0';
signal ClientClkToggle_q : std_logic := '0';
signal FIFO_rst : std_logic := '0';
signal FIFO_en : std_logic := '0';
signal TX_high : std_logic := '0';
signal us_cntr : std_logic_vector(9 downto 0) := (others => '0');
signal ms_cntr : std_logic_vector(10 downto 0) := (others => '0');
signal strobe_us : std_logic := '0';
signal strobe_ms : std_logic := '0';
signal TSclock : std_logic_vector(31 downto 0) := (others => '0');
signal evt_FIFO_full : std_logic_vector(2 downto 0) := (others => '0');
signal evt_FIFO_empty : std_logic_vector(2 downto 0) := (others => '0');
signal evt_FIFO_we : std_logic_vector(2 downto 0) := (others => '0');
signal evt_FIFO_re : std_logic_vector(2 downto 0) := (others => '0');
signal evt_FIFO_rep : std_logic_vector(2 downto 0) := (others => '0');
signal evt_FIFO_data_avl : std_logic_vector(2 downto 0) := (others => '0');
signal evt_FIFO_di : array3X67 := (others => (others => '0'));
signal evt_FIFO_do : array3X67 := (others => (others => '0'));
signal evt_FIFO_RDCOUNT : array3X9 := (others => (others => '0'));
signal evt_FIFO_WRCOUNT : array3X9 := (others => (others => '0'));
signal EVENTdata_avl : std_logic_vector(2 downto 0) := (others => '0');
signal EVENTdata_addr : array3X13 := (others => (others => '0'));
signal re_RETX_ddr_wq : std_logic_vector(2 downto 0) := (others => '0');
signal RETX_ddr_data_we : std_logic_vector(2 downto 0) := (others => '0');
signal RETX_ddr_wrqst : std_logic_vector(2 downto 0) := (others => '0');
signal RETX_ddr_rrqst : std_logic_vector(2 downto 0) := (others => '0');
signal RETX_ddr_out : array3X32 := (others => (others => '0'));
signal RETX_ddr_LEN_max : array3X5 := (others => (others => '0'));
signal RETX_ddr_LEN : array3X5 := (others => (others => '0'));
signal RETXdata_we : array3X2 := (others => (others => '0'));
signal DDR2TCPdata : std_logic_vector(32 downto 0) := (others => '0');
signal rst_RETXdata_chksum : std_logic := '0';
signal RETXdata_chksum_out : std_logic_vector(15 downto 0) := (others => '0');
signal RETXdata_checksum : array3x16 := (others => (others => '0'));
signal RETXdataLEN : array3X13 := (others => (others => '0'));
signal RETXdataAddr : array3X26 := (others => (others => '0'));
signal RETXdata_space : std_logic_vector(2 downto 0) := (others => '0');
signal RETXdataRqst : std_logic_vector(2 downto 0) := (others => '0');
signal RETXdataACK : std_logic_vector(2 downto 0) := (others => '0');
signal ReleaseLen : array3X11 := (others => (others => '0'));
signal Release_space : std_logic_vector(2 downto 0) := (others => '0');
signal ReleaseBuffer : std_logic_vector(2 downto 0) := (others => '0');
signal Release_rqst: std_logic_vector(2 downto 0) := (others => '0');
signal rrqstMask : std_logic_vector(5 downto 0) := (others => '0');
signal TCP_rrqst_i : std_logic_vector(2 downto 0) := (others => '0');
signal odd : std_logic := '0';
signal rst_odd : std_logic := '0';
--signal DDR2TCPdata_vld : std_logic := '0';
signal TCP_rFIFO_do_vld : std_logic := '0';
signal ld_RETXdata_chksum : std_logic_vector(2 downto 0) := (others => '0');
signal ld_RETXdata_chksum_r : std_logic_vector(2 downto 0) := (others => '0');
signal ld_RETXdata_chksum_r2 : std_logic_vector(2 downto 0) := (others => '0');
signal TCP_rFIFO_wa0SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal TCP_rFIFO_wa1SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal TCP_rFIFO_wa2SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal TCP_rFIFO_di : std_logic_vector(35 downto 0) := (others => '0');
signal TCP_rFIFO_do : std_logic_vector(35 downto 0) := (others => '0');
signal TCP_rFIFO_wa : std_logic_vector(4 downto 0) := (others => '0');
signal TCP_rFIFO_ra : std_logic_vector(4 downto 0) := (others => '0');
signal RETX_ddr_rp_rst : std_logic := '0';
signal RETX_ddr_rp_we : std_logic := '0';
signal RETX_ddr_rp_di : std_logic_vector(17 downto 0) := (others => '0');
signal RETX_ddr_rp_do : std_logic_vector(17 downto 0) := (others => '0');
signal RETX_ddr_rp_a : std_logic_vector(4 downto 0) := (others => '0');
signal TCP_length_i : std_logic_vector(20 downto 0) := (others => '0');
signal TCP_raddr_i : std_logic_vector(28 downto 0) := (others => '0');
signal TCP_rlength : std_logic_vector(12 downto 0) := (others => '0');
--signal rdDDRqueue_we : std_logic := '0';
--signal rdDDRqueue_re : std_logic := '0';
--signal rdDDRqueue_a : std_logic_vector(2 downto 0) := (others => '0');
--signal rdDDRqueue_di : std_logic_vector(2 downto 0) := (others => '0');
--signal rdDDRqueue_do : std_logic_vector(2 downto 0) := (others => '0');
--signal rdDDRqueue_dout : std_logic_vector(2 downto 0) := (others => '0');
--signal rdDDRqueue_dout_vld : std_logic := '0';
signal TCP_wFIFO_re : std_logic := '0';
signal TCP_w_busy : std_logic := '0';
signal TCP_wFIFO_we : std_logic := '0';
signal TCP_w_sel : std_logic_vector(1 downto 0) := (others => '0');
signal TCP_w_wc : std_logic_vector(3 downto 0) := (others => '0');
signal TCP_wFIFO_DI : std_logic_vector(33 downto 0) := (others => '0');
signal TCP_wFIFO_DO : std_logic_vector(33 downto 0) := (others => '0');
signal TCP_wFIFO_RDCOUNT : std_logic_vector(8 downto 0) := (others => '0');
signal TCP_wFIFO_WRCOUNT : std_logic_vector(8 downto 0) := (others => '0');
signal inh_TX : std_logic_vector(2 downto 0) := (others => '0');
signal inh_TX_q : std_logic_vector(2 downto 0) := (others => '0');
signal reset_TXSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_TXOUTCLK : std_logic_vector(2 downto 0) := (others => '0');
signal txusrclk : std_logic := '0';
signal qplllock : std_logic := '0';
signal qpllreset : std_logic := '0';
signal GTX_TX_READ : std_logic := '0';
signal LINK_down : std_logic_vector(2 downto 0) := (others => '0');
signal EnTCPIP : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxoutclk : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxusrclk : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_txuserrdy : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxresetdone : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxuserrdy : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_drprdy : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_drpen : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_drpwe : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxdfeagchold : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_adapt_done : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_rxmonitor : array3X7 := (others => (others => '0'));
signal SFP_drpdo : array3X16 := (others => (others => '0'));
signal SFP_rxmonitorsel : array3X2 := (others => (others => '0'));
signal SFP_drpaddr : array3X9 := (others => (others => '0'));
signal SFP_drpdi : array3X16 := (others => (others => '0'));
signal SFP_RX_FSM_RESET_DONE : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_TX_FSM_RESET_DONE : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXDVLD : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXHEADERVLD : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXGEARBOXSLIP : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXGOOD : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_TXD : array3X32 := (others => (others => '0'));
signal SFP_TXD_inv : array3X32 := (others => (others => '0'));
signal SFP_TXHEADER : array3X2 := (others => (others => '0'));
signal SFP_RXD : array3X32 := (others => (others => '0'));
signal SFP_RXD_inv : array3X32 := (others => (others => '0'));
signal SFP_RXHEADER : array3X2 := (others => (others => '0'));
signal SFP_EmacPhyTxD : array3X32 := (others => (others => '0'));
signal SFP_EmacPhyTxC : array3X4 := (others => (others => '0'));
signal SFP_PhyEmacRxD : array3X32 := (others => (others => '0'));
signal SFP_PhyEmacRxC : array3X4 := (others => (others => '0'));
--signal PCS_status : array3X32 := (others => (others => '0'));
signal EmacPhyTxD : array3X32 := (others => (others => '0'));
signal EmacPhyTxC : array3X4 := (others => (others => '0'));
signal PhyEmacRxD : array3X32 := (others => (others => '0'));
signal PhyEmacRxC : array3X4 := (others => (others => '0'));
signal TCPIP2SFP_sel : array3X2 := (others => (others => '0'));
signal SFP2TCPIP : array3X2 := (others => (others => '0'));
signal IPADDR : array3X32 := (others => (others => '0'));
signal SFP_IPADDR : array3X32 := (x"c0a80120",x"c0a80121",x"c0a80122");
--signal CWND_max : std_logic_vector(31 downto 0) := x"0fffffff";
signal RTOmin : std_logic_vector(15 downto 0) := x"0008";
signal MACADDR : array3X48 := (others => (others => '0'));
signal GTX_TX_PAUSE : std_logic := '0';
signal TXSEQ_cntr : std_logic_vector(6 downto 0) := (others => '0');
signal SFP_TXSEQUENCE : array3X7 := (others => (others => '0'));
signal SFP_LOOPBACK_IN : array3X3 := (others => (others => '0'));
signal SFP_RXPRBSERR_OUT : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_RXPRBSSEL_IN : array3X3 := (others => (others => '0'));
signal SFP_TXPRBSSEL_IN : array3X3 := (others => (others => '0'));
signal SFP_EYESCANDATAERROR_OUT : std_logic_vector(2 downto 0) := (others => '0');
signal got_eofToggle : std_logic_vector(2 downto 0) := (others => '0');
signal EventBufAddr_we_i : std_logic_vector(2 downto 0) := (others => '0');
signal EventData_re_i : std_logic_vector(2 downto 0) := (others => '0');
signal EventBufAddr_i : array3X14 := (others => (others => '0'));
signal ReadBusy : std_logic_vector(2 downto 0) := (others => '0');
signal UNA_MonBufMatch : std_logic_vector(3 downto 0) := (others => '0');
signal UNA_TCPBufMatch : std_logic_vector(2 downto 0) := (others => '0');
signal UNA_MonBufSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal UNA_TCPBufSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal inc_ddr_paSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal sysDIV2 : std_logic := '0';
signal evt_FIFO_sel : std_logic := '0';
signal ReleaseMonBuf : std_logic := '0';
signal ReleaseTCPBuf : std_logic := '0';
signal WrtMonBufAllDone_i : std_logic := '0';
--signal TCPBufCnt : std_logic_vector(12 downto 0) := (others => '0');
--signal MonBufCnt : std_logic_vector(12 downto 0) := (others => '0');
signal NXT_TCPBuf : array3X12 := (others => (others => '0'));
signal UNA_MonBuf : array5X11 := (others => (others => '0'));
signal UNA_TCPBuf : array4X11 := (others => (others => '0'));
signal AddrOffset : array3X10 := (others => (others => '0'));
signal SFPresetSyncRegs : array3X3 := (others => (others => '0'));
--signal TCPresetSyncRegs : array3X3 := (others => (others => '0'));
signal TCPresetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal got_eofToggle0SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal reset_TCPIP : std_logic_vector(2 downto 0) := (others => '0');
signal got_eofToggle1SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal got_eofToggle2SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal TCPIP_rdata : array3X32 := (others => (others => '0'));
signal AddrBuf_full : std_logic_vector(2 downto 0) := (others => '0');
signal mon_evt_cnt_i : std_logic_vector(10 downto 0) := (others => '0');
signal MonBufUsed : std_logic_vector(9 downto 0) := (others => '0');
signal MonBuf_full : std_logic := '0';
signal chk_MonBuf_avl : std_logic := '0';
signal FirstBlkAddrDo : array2x3x12 := (others => (others => (others => '0')));
signal FirstBlkAddr_ra : array2x3x5 := (others => (others => (others => '0')));
signal FirstBlkAddr_re : array2X3 := (others => (others => '0'));
signal WrtMonEvtDone_l : std_logic_vector(2 downto 0) := (others => '0');
signal MonEvtQueued : std_logic_vector(2 downto 0) := (others => '0');
signal FirstBlkAddrDoValid : array2X3 := (others => (others => '0'));
signal FirstBlkAddr_wa : std_logic_vector(4 downto 0) := (others => '0');
signal FirstBlkAddrDi : std_logic_vector(11 downto 0) := (others => '0');
signal FirstBlkAddr_we : std_logic := '0';
signal MonBuf_wa : std_logic_vector(10 downto 0) := (others => '0');
signal MonBuf_ra : std_logic_vector(10 downto 0) := (others => '0');
signal NXT_MonBuf : array3X11 := (others => (others => '0'));
signal Written_MonBuf : array4X11 := (others => (others => '0'));
signal Written_MonBufMatch : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_pd : array3X2 := (others => (others => '0'));
signal EventData_reCntr : array3X32 := (others => (others => '0'));
signal EventData_weCntr : array3X32 := (others => (others => '0'));
signal EventBufAddr_weCntr : array3X32 := (others => (others => '0'));
signal cmsCRC_initp : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_init : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_ce : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_err : std_logic_vector(2 downto 0) := (others => '0');
signal cmsCRC_errCntr : array3X32 := (others => (others => '0'));
signal SFP_we : std_logic_vector(2 downto 0) := (others => '0');
signal EoB : std_logic_vector(2 downto 0) := (others => '0');
signal EoE : std_logic_vector(2 downto 0) := (others => '0');
signal SFP_evt_cntr : array3X32 := (others => (others => '0'));
signal SFP_blk_cntr : array3X32 := (others => (others => '0'));
signal SFP_word_cntr : array3X32 := (others => (others => '0'));
--signal EvtLength_err : std_logic_vector(2 downto 0) := (others => '0');
signal TotalEvtLengthCntr24q : std_logic_vector(2 downto 0) := (others => '0');
signal EvtLengthCntr : array3X24 := (others => (others => '0'));
signal EvtLength_errCntr : array3X32 := (others => (others => '0'));
signal AMClength_errCntr : array3X32 := (others => (others => '0'));
signal AMCvalid_errCntr : array3X32 := (others => (others => '0'));
signal AMCcrc_errCntr : array3X32 := (others => (others => '0'));
signal TotalEvtLengthCntr : array3X56 := (others => (others => '0'));
signal SFP_down_i : std_logic_vector(2 downto 0) := (others => '0');
signal StopOverWrite : std_logic := '0';
signal StopOnCMScrc_err : std_logic := '0';
signal en_stop : std_logic_vector(4 downto 0) := (others => '1');
signal stop : std_logic := '0';
signal inc_err : array3x5 := (others => (others => '0'));
signal reset_cntr : std_logic_vector(20 downto 0) := (others => '0');
--signal Rx_start_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal SFP_pd_q : array3X4 := (others => (others => '0'));
signal soft_reset : std_logic := '0';
signal reset_cntr20_q : std_logic := '0';
signal SFP_rate_limit : array3x8 := (others => x"7f");
signal rate_limit : array3x8 := (others => x"7f");
COMPONENT chipscope
	generic (N : integer := 5);
	PORT(
		clka : IN std_logic;
		clkb : IN std_logic;
		ina : IN std_logic_vector(135 downto 0);
		inb : IN std_logic_vector(135 downto 0)       
		);
END COMPONENT;
COMPONENT chipscope1
	generic (N : integer := 5);
	PORT(
		clk : IN std_logic;
		Din : IN std_logic_vector(303 downto 0)       
		);
END COMPONENT;
COMPONENT chipscope1b
	generic (USER2 : boolean := false);
	PORT(
		clk : IN std_logic;
		Din : IN std_logic_vector(303 downto 0)       
		);
END COMPONENT;
component icon1
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

end component;
component ila36x1024
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    DATA : IN STD_LOGIC_VECTOR(35 DOWNTO 0);
    TRIG0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0));

end component;

type array3x512 is array(0 to 2) of std_logic_vector(511 downto 0);
signal TCPIP_cs : array3x512;
signal cs_din : std_logic_vector(303 downto 0) := (others => '0');
signal waitcntr : std_logic_vector(11 downto 0) := (others => '0');
signal CONTROL0 : std_logic_vector(35 downto 0) := (others => '0');
begin
--i_icon1 : icon1
--  port map (
--    CONTROL0 => CONTROL0);
--i_ila36x1024 : ila36x1024
--  port map (
--    CONTROL => CONTROL0,
--    CLK => ClientClk2X,
--    DATA => TCPIP_cs(0)(35 downto 0),
--    TRIG0 => TCPIP_cs(0)(28 downto 21));
SFP_down(2) <= SFP_down_i(2) and not enSFP(3) and enSFP(2);
SFP_down(1) <= SFP_down_i(1) and not enSFP(3) and enSFP(1);
SFP_down(0) <= SFP_down_i(0) and not enSFP(3) and enSFP(0);
TCPclk <= ClientClk2X;
--MonBuf_avl <= MonBuf_avl_i;
MonBufOvfl <= '0';
EventBufAddr_we <= EventBufAddr_we_i;
EventData_re <= EventData_re_i;
mon_evt_cnt(31 downto 11) <= (others => '0');
mon_evt_cnt(10 downto 0) <= mon_evt_cnt_i;
EventBufAddr <= EventBufAddr_i;
TCP_rrqst <= TCP_rrqst_i(2);
TCP_raddr <= TCP_raddr_i;
--TCP_length <= TCP_rlength(9 downto 0);
TCP_length <= TCP_rlength;
process(sysclk,reset,ClientClk_lock)
begin
	if(reset = '1' or ClientClk_lock = '0')then
		resetSyncRegs <= (others => '1');
	elsif(sysclk'event and sysclk = '1')then
		resetSyncRegs <= resetSyncRegs(1 downto 0) & '0';
	end if;
end process;
process(ClientClk2X,SFP_TX_FSM_RESET_DONE,SFP_RX_FSM_RESET_DONE)
begin
	for i in 0 to 2 loop
		if(SFP_TX_FSM_RESET_DONE(i) = '0' or SFP_RX_FSM_RESET_DONE(i) = '0')then
			SFPresetSyncRegs(i) <= (others => '1');
		elsif(ClientClk2X'event and ClientClk2X = '1')then
			SFPresetSyncRegs(i) <= SFPresetSyncRegs(i)(1 downto 0) & '0';
		end if;
	end loop;
end process;
process(ClientClk2X,reset,ClientClk_lock)
begin
	if(reset = '1' or ClientClk_lock = '0')then
		ClientClk2XresetSyncRegs <= (others => '1');
	elsif(ClientClk2X'event and ClientClk2X = '1')then
		ClientClk2XresetSyncRegs <= ClientClk2XresetSyncRegs(1 downto 0) & '0';
	end if;
end process;
--process(ClientClk,SFP_TX_FSM_RESET_DONE,ClientClk_lock)
--begin
--	for i in 0 to 2 loop
--		if(SFP_TX_FSM_RESET_DONE(i) = '0' or ClientClk_lock = '0')then
--process(ClientClk,TCPreset,ClientClk_lock)
--begin
--	for i in 0 to 2 loop
--		if(TCPreset = '1' or ClientClk_lock = '0')then
--			TCPresetSyncRegs(i) <= (others => '1');
--		elsif(ClientClk2X'event and ClientClk2X = '1')then
--			TCPresetSyncRegs(i) <= TCPresetSyncRegs(i)(1 downto 0) & '0';
--		end if;
--	end loop;
--end process;
process(ClientClk,TCPreset,ClientClk_lock)
begin
	if(TCPreset = '1' or ClientClk_lock = '0')then
		TCPresetSyncRegs <= (others => '1');
	elsif(ClientClk'event and ClientClk = '1')then
		TCPresetSyncRegs <= TCPresetSyncRegs(1 downto 0) & '0';
	end if;
end process;
process(ClientClk)
begin
	if(ClientClk'event and ClientClk = '1')then
		ClientClkToggle <= not ClientClkToggle;
--		for i in 0 to 2 loop
--			if(TCPresetSyncRegs(2) = '1' or SFP_pd(i)(0) = '1')then
--				PCS_reset(i) <= '1';
--			elsif(SFP_TX_FSM_RESET_DONE(i) = '1')then
--				PCS_reset(i) <= '0';
--			end if;
--		end loop;
	end if;
end process;
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		ClientClkToggle_q <= ClientClkToggle;
		TX_high <= ClientClkToggle_q xnor ClientClkToggle;
		if(us_cntr(9) = '1')then
			us_cntr <= "00" & x"c9";
			TSclock <= TSclock + 1;
		else
			us_cntr <= us_cntr + 1;
		end if;
		if(us_cntr(9) = '1')then
			if(ms_cntr(10) = '1')then
				ms_cntr <= x"03" & "000";
			else
				ms_cntr <= ms_cntr + 1;
			end if;
		end if;
		strobe_ms <= us_cntr(9) and ms_cntr(10);
	end if;
end process;
strobe_us <= us_cntr(9);
process(txusrclk)
begin
	if(txusrclk'event and txusrclk = '1')then
		if(TXSEQ_cntr = "1000001")then
			TXSEQ_cntr <= (others => '0');
		else
			TXSEQ_cntr <= TXSEQ_cntr + 1;
		end if;
		if(TXSEQ_cntr(0) = '1')then
			GTX_TX_PAUSE <= and_reduce(TXSEQ_cntr(5 downto 1));
		end if;
		if(inh_TX(0) = '1')then
			SFP_TXSEQUENCE(0) <= (others => '0');
		elsif(TXSEQ_cntr(0) = '1')then
			SFP_TXSEQUENCE(0) <= '0' & TXSEQ_cntr(6 downto 1);
		end if;
		if(inh_TX(1) = '1')then
			SFP_TXSEQUENCE(1) <= (others => '0');
		elsif(TXSEQ_cntr(0) = '1')then
			SFP_TXSEQUENCE(1) <= '0' & TXSEQ_cntr(6 downto 1);
		end if;
		if(inh_TX(2) = '1')then
			SFP_TXSEQUENCE(2) <= (others => '0');
		elsif(TXSEQ_cntr(0) = '1')then
			SFP_TXSEQUENCE(2) <= '0' & TXSEQ_cntr(6 downto 1);
		end if;
	end if;
end process;
process(TXUSRCLK,TCPreset)
begin
	if(TCPreset = '1')then
		reset_TXSyncRegs <= (others => '1');
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		reset_TXSyncRegs <= reset_TXSyncRegs(1 downto 0) & '0';
	end if;
end process;
process(TXUSRCLK,SFP_TX_FSM_RESET_DONE(0))
begin
	if(SFP_TX_FSM_RESET_DONE(0) = '0')then
		inh_TX(0) <= '1';
		inh_TX_q(0) <= '1';
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		if(TXSEQ_cntr(0) = '1' and TXSEQ_cntr(6) = '1')then
			inh_TX(0) <= '0';
		end if;
		inh_TX_q(0) <= inh_TX(0);
	end if;
end process;
process(TXUSRCLK,SFP_TX_FSM_RESET_DONE(1))
begin
	if(SFP_TX_FSM_RESET_DONE(1) = '0')then
		inh_TX(1) <= '1';
		inh_TX_q(1) <= '1';
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		if(TXSEQ_cntr(0) = '1' and TXSEQ_cntr(6) = '1')then
			inh_TX(1) <= '0';
		end if;
		inh_TX_q(1) <= inh_TX(1);
	end if;
end process;
process(TXUSRCLK,SFP_TX_FSM_RESET_DONE(2))
begin
	if(SFP_TX_FSM_RESET_DONE(2) = '0')then
		inh_TX(2) <= '1';
		inh_TX_q(2) <= '1';
	elsif(TXUSRCLK'event and TXUSRCLK = '1')then
		if(TXSEQ_cntr(0) = '1' and TXSEQ_cntr(6) = '1')then
			inh_TX(2) <= '0';
		end if;
		inh_TX_q(2) <= inh_TX(2);
	end if;
end process;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		TCPBuf_avl <= not or_reduce(enTCPIP);
	  evt_buf_full <= evt_FIFO_full or wport_FIFO_full;
	end if;
end process;
g_FirstBlkAddr: for j in 0 to 1 generate
	g1_FirstBlkAddr: for i in 0 to 5 generate
   i_FirstBlkAddr : RAM32M
   port map (
      DOA => FirstBlkAddrDo(j)(0)(i*2+1 downto i*2), -- Read port A 2-bit output
      DOB => FirstBlkAddrDo(j)(1)(i*2+1 downto i*2), -- Read port B 2-bit output
      DOC => FirstBlkAddrDo(j)(2)(i*2+1 downto i*2), -- Read port C 2-bit output
      DOD => open, -- Read/Write port D 2-bit output
      ADDRA => FirstBlkAddr_ra(j)(0),   -- Read port A 5-bit address input
      ADDRB => FirstBlkAddr_ra(j)(1),   -- Read port B 5-bit address input
      ADDRC => FirstBlkAddr_ra(j)(2),   -- Read port C 5-bit address input
      ADDRD => FirstBlkAddr_wa,   -- Read/Write port D 5-bit address input
      DIA => FirstBlkAddrDi(i*2+1 downto i*2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRA
      DIB => FirstBlkAddrDi(i*2+1 downto i*2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRB
      DIC => FirstBlkAddrDi(i*2+1 downto i*2), -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRC
      DID => "00", -- RAM 2-bit data write input addressed by ADDRD,
                  -- read addressed by ADDRD
      WCLK => sysclk,  -- Write clock input
      WE => FirstBlkAddr_we       -- Write enable input
   );
	end generate;
end generate;
FirstBlkAddrDi <= '0' & MonBuf_wa;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		sysDIV2 <= not sysDIV2;
		if(resetSyncRegs(2) = '1')then
			chk_MonBuf_avl <= '1';
		elsif(buf_rqst(0) = '1')then
			chk_MonBuf_avl <= buf_rqst(3);
		end if;
		if(resetSyncRegs(2) = '1')then
			MonBuf_avl <= '1';
		elsif(MonBufOverWrite = '1')then
			MonBuf_avl <= not StopOverWrite;
		elsif(WaitMonBuf = '1' or enTCPIP /= "000")then
			if((MonBuf_wa(10) /= MonBuf_ra(10) and MonBuf_wa(9 downto 0) = MonBuf_ra(9 downto 0)))then
				MonBuf_avl <= '0';
			else
				MonBuf_avl <= '1';
			end if;
		elsif(chk_MonBuf_avl = '1')then
			if(MonBufUsed(9 downto 8) = "11")then
				MonBuf_avl <= '0';
			else
				MonBuf_avl <= '1';
			end if;
		end if;
		if(enTCPIP /= "000")then
			mon_evt_cnt_i <= (others => '0');
		elsif(MonBufOverWrite = '1')then
			mon_evt_cnt_i <= MonBuf_full & Written_MonBuf(3)(9 downto 0);
		else
			mon_evt_cnt_i <= Written_MonBuf(3) - MonBuf_ra;
		end if;
		if(enTCPIP /= "000")then
			MonBuf_empty <= '1';
		elsif(MonBufOverWrite = '0')then
			MonBuf_empty <= not or_reduce(mon_evt_cnt_i);
    elsif(Written_MonBuf(3) /= MonBuf_ra or MonBuf_full = '1')then
			MonBuf_empty <= '0';
		else
			MonBuf_empty <= '1';
		end if;
		if(EnSFP(2 downto 1) = "00")then
			SFP_we(0) <= EventData_we(0);
			EoB(0) <= EventData_in(0)(64);
			EoE(0) <= EventData_in(0)(65);
		else
			SFP_we(0) <= EventData_we(1) and EnSFP(0);
			EoB(0) <= EventData_in(1)(64);
			EoE(0) <= EventData_in(1)(65);
		end if;
		if(EnSFP(2 downto 0) = "110")then
			SFP_we(1) <= EventData_we(1);
			EoB(1) <= EventData_in(1)(64);
			EoE(1) <= EventData_in(1)(65);
		else
			SFP_we(1) <= EventData_we(0) and EnSFP(1);
			EoB(1) <= EventData_in(0)(64);
			EoE(1) <= EventData_in(0)(65);
		end if;
		if(EnSFP(2 downto 0) = "111")then
			SFP_we(2) <= EventData_we(2);
			EoB(2) <= EventData_in(2)(64);
			EoE(2) <= EventData_in(2)(65);
		else
			SFP_we(2) <= EventData_we(0) and EnSFP(2);
			EoB(2) <= EventData_in(0)(64);
			EoE(2) <= EventData_in(0)(65);
		end if;
		if(resetSyncRegs(2) = '1')then
			ReadBusy <= (others => '0');
			EventData_re_i <= (others => '0');
			EventBufAddr_we_i <= (others => '0');
			UNA_MonBufSyncRegs <= (others => '0');
			UNA_TCPBufSyncRegs <= (others => '0');
			ReleaseMonBuf <= '0';
			ReleaseTCPBuf <= '0';
			MonBuf_wa <= (others => '0');
			MonBuf_ra <= (others => '0');
			Written_MonBufMatch <= (others => '1');
			Written_MonBuf <= (others => (others => '0'));
			NXT_MonBuf <= (others => (others => '0'));
			NXT_TCPBuf <= (others => (others => '0'));
			FirstBlkAddr_we <= '0';
			FirstBlkAddr_re <= (others => (others => '0'));
			FirstBlkAddrDoValid <= (others => (others => '0'));
			WrtMonEvtDone_l <= (others => '0');
			MonEvtQueued <= (others => '0');
			FirstBlkAddr_wa <= (others => '0');
			FirstBlkAddr_ra <= (others => (others => (others => '0')));
			MonBuf_full <= '0';
			EventData_reCntr <= (others => (others => '0'));
			EventData_weCntr <= (others => (others => '0'));
			EventBufAddr_weCntr <= (others => (others => '0'));
			EvtLengthCntr <= (others => x"000001");
      TotalEvtLengthCntr <= (others => (others => '0'));
      TotalEvtLengthCntr24q <= "000";
		  SFP_blk_cntr <= (others => (others => '0'));
      SFP_evt_cntr <= (others => (others => '0'));
      SFP_word_cntr <= (others => (others => '0'));
		else
			for i in 0 to 2 loop
				if(EVENTdata_in(i)(64) = '1' and EVENTdata_we(i) = '1')then
					ReadBusy(i) <= '0';
				elsif(evt_data_rdy(i) = '1' and wport_rdy(i) = '1' and AddrBuf_full(i) = '0')then
					ReadBusy(i) <= '1';
				end if;
				EventData_re_i(i) <= evt_data_rdy(i) and wport_rdy(i) and not ReadBusy(i) and not AddrBuf_full(i);
				if(EventData_re_i(i) = '1')then
					EventData_reCntr(i) <= EventData_reCntr(i) + 1;
				end if;
				if(EventData_we(i) = '1')then
					EventData_weCntr(i) <= EventData_weCntr(i) + 1;
				end if;
				if(EventData_in(i)(66) = '0')then
					EventBufAddr_we_i(i) <= evt_data_rdy(i) and wport_rdy(i) and not ReadBusy(i) and not AddrBuf_full(i);
				else
					EventBufAddr_we_i(i) <= '0';
				end if;
				if(EventBufAddr_we_i(i) = '1')then
					EventBufAddr_weCntr(i) <= EventBufAddr_weCntr(i) + 1;
				end if;
				if(EventData_we(i) = '1')then
          if(EventData_in(i)(65) = '1')then
            EvtLengthCntr(i) <= x"000001";
            TotalEvtLengthCntr(i)(24 downto 0) <= TotalEvtLengthCntr(i)(24 downto 0) + ('0' & EventData_in(i)(55 downto 32));
          else
            EvtLengthCntr(i) <= EvtLengthCntr(i) + 1;
          end if;
        end if;
        TotalEvtLengthCntr24q(i) <= TotalEvtLengthCntr(i)(24);
        if(TotalEvtLengthCntr24q(i) = '1' and TotalEvtLengthCntr(i)(24) = '0')then
          TotalEvtLengthCntr(i)(55 downto 25) <= TotalEvtLengthCntr(i)(55 downto 25) + 1;
        end if;
			end loop;
		  if(MonBufOverWrite = '1' and Written_MonBuf(3)(10) = '1')then
        MonBuf_full <= '1';
      end if;
			for j in 0 to 1 loop
				for i in 0 to 2 loop
					if(FirstBlkAddr_re(j)(i) = '1')then
						FirstBlkAddr_ra(j)(i) <= FirstBlkAddr_ra(j)(i) + 1;
					end if;
					if(FirstBlkAddr_ra(j)(i) = FirstBlkAddr_wa)then
						FirstBlkAddrDoValid(j)(i) <= '0';
					else
						FirstBlkAddrDoValid(j)(i) <= '1';
					end if;
				end loop;
			end loop;
			if(FirstBlkAddr_we = '1')then
				FirstBlkAddr_wa <= FirstBlkAddr_wa + 1;
			end if;
			for i in 0 to 2 loop
				if(WrtMonEvtDone(i) = '1' and enTCPIP = "000")then
					WrtMonEvtDone_l(i) <= '1';
				elsif(FirstBlkAddrDoValid(0)(i) = '1')then
					WrtMonEvtDone_l(i) <= '0';
				end if;
				FirstBlkAddr_re(0)(i) <= FirstBlkAddrDoValid(0)(i) and WrtMonEvtDone_l(i);
				if(EventData_we(i) = '1' and EventData_in(i)(66 downto 65) = "01" and enTCPIP = "000")then
					MonEvtQueued(i) <= '1';
				elsif(FirstBlkAddrDoValid(1)(i) = '1')then
					MonEvtQueued(i) <= '0';
				end if;
				FirstBlkAddr_re(1)(i) <= FirstBlkAddrDoValid(1)(i) and MonEvtQueued(i);
			end loop;
			FirstBlkAddr_we <= buf_rqst(3) and buf_rqst(0) and not or_reduce(enTCPIP);
			if(buf_rqst(0) = '1')then
				MonBuf_wa <= MonBuf_wa + 1;
			end if;
			MonBufUsed <= MonBuf_wa(9 downto 0) - MonBuf_ra(9 downto 0);
      if(ReleaseMonBuf = '1' or (MonBufOverWrite = '1' and StopOverWrite = '0' and and_reduce(MonBufUsed) = '1' and buf_rqst(0) = '1'))then
        MonBuf_ra <= MonBuf_ra + 1;
      end if;
			if(Written_MonBuf(0) = Written_MonBuf(3) and WrtMonEvtDone_l(0) = '0')then
				Written_MonBufMatch(0) <= '1';
			else
				Written_MonBufMatch(0) <= '0';
			end if;
			if((EnSFP(2 downto 0) = "111" or EnSFP(2 downto 0) = "011" or EnSFP(2 downto 0) = "110" or EnSFP(2 downto 0) = "101") and Written_MonBuf(1) = Written_MonBuf(3) and WrtMonEvtDone_l(1) = '0')then
				Written_MonBufMatch(1) <= '1';
			else
				Written_MonBufMatch(1) <= '0';
			end if;
			if(EnSFP(2 downto 0) = "111" and Written_MonBuf(2) = Written_MonBuf(3) and WrtMonEvtDone_l(2) = '0')then
				Written_MonBufMatch(2) <= '1';
			else
				Written_MonBufMatch(2) <= '0';
			end if;
			if(Written_MonBufMatch = "000" and sysDIV2 = '0')then
				Written_MonBuf(3) <= Written_MonBuf(3) + 1;
			end if;
			UNA_MonBufSyncRegs <= UNA_MonBufSyncRegs(2 downto 0) & UNA_MonBuf(4)(0);
			UNA_TCPBufSyncRegs <= UNA_TCPBufSyncRegs(2 downto 0) & UNA_TCPBuf(3)(0);
			ReleaseMonBuf <= UNA_MonBufSyncRegs(3) xor UNA_MonBufSyncRegs(2);
			ReleaseTCPBuf <= UNA_TCPBufSyncRegs(3) xor UNA_TCPBufSyncRegs(2);
			for i in 0 to 2 loop
				if(FirstBlkAddr_re(0)(i) = '1')then
					Written_MonBuf(i) <= FirstBlkAddrDo(0)(i)(10 downto 0);
				elsif(WrtMonBlkDone(i) = '1')then
					Written_MonBuf(i) <= Written_MonBuf(i) + 1;
				end if;
				if(FirstBlkAddr_re(1)(i) = '1')then 
					NXT_MonBuf(i) <= FirstBlkAddrDo(1)(i)(10 downto 0);
				elsif(EventBufAddr_we_i(i) = '1' and EventData_in(i)(66) = '0')then
					NXT_MonBuf(i) <= NXT_MonBuf(i) + 1;
				end if;
				if(EventBufAddr_we_i(i) = '1' and EventData_in(i)(66) = '1')then
					NXT_TCPBuf(i) <= NXT_TCPBuf(i) + 1;
				end if;
        if(SFP_we(i) = '1' and EoB(i) = '1')then
          SFP_blk_cntr(i) <= SFP_blk_cntr(i) + 1;
        end if;
        if(SFP_we(i) = '1' and EoE(i) = '1')then
          SFP_evt_cntr(i) <= SFP_evt_cntr(i) + 1;
        end if;
        if(SFP_we(i) = '1')then
          SFP_word_cntr(i) <= SFP_word_cntr(i) + 1;
        end if;
			end loop;
		end if;
		for i in 0 to 2 loop
			if(EventData_in(i)(66) = '0')then
				EventBufAddr_i(i) <= NXT_MonBuf(i)(9 downto 0) & AddrOffset(i)(9 downto 6);
			else
				EventBufAddr_i(i) <= NXT_TCPBuf(i)(9 downto 0) & AddrOffset(i)(9 downto 6);
			end if;
		end loop;
	end if;
end process;
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		if(ClientClk2XresetSyncRegs(2) = '1')then
			inc_ddr_paSyncRegs <= (others => '0');
			UNA_MonBufMatch <= (others => '1');
			UNA_TCPBufMatch <= (others => '1');
			UNA_MonBuf(4) <= (others => '0');
			UNA_MonBuf(3) <= (others => '0');
			UNA_TCPBuf(3) <= (others => '0');
		else
			inc_ddr_paSyncRegs <= inc_ddr_paSyncRegs(2 downto 0) & inc_ddr_pa;
			if(inc_ddr_paSyncRegs(3 downto 2) = "10")then
				UNA_MonBuf(3) <= UNA_MonBuf(3) + 1;
			end if;
			for i in 0 to 2 loop
				if(enTCPIP(i) = '1' and UNA_MonBuf(i) = UNA_MonBuf(4))then
					UNA_MonBufMatch(i) <= '1';
				else
					UNA_MonBufMatch(i) <= '0';
				end if;
				if(enTCPIP(i) = '1' and UNA_TCPBuf(i) = UNA_TCPBuf(3))then
					UNA_TCPBufMatch(i) <= '1';
				else
					UNA_TCPBufMatch(i) <= '0';
				end if;
			end loop;
			if(EnTCPIP = "000" and UNA_MonBuf(3) = UNA_MonBuf(4))then
				UNA_MonBufMatch(3) <= '1';
			else
				UNA_MonBufMatch(3) <= '0';
			end if;
			if(UNA_MonBufMatch = x"0" and us_cntr(0) = '1')then
				UNA_MonBuf(4) <= UNA_MonBuf(4) + 1;			
			end if;
			if(UNA_TCPBufMatch = "000" and enSFP(2 downto 0) /= "000" and us_cntr(0) = '1')then
				UNA_TCPBuf(3) <= UNA_TCPBuf(3) + 1;			
			end if;
		end if;
		case EnSFP(2 downto 0) is
			when "011" | "101" | "110" => AddrOffset(0)(9 downto 6) <= x"8";
			when "111" => AddrOffset(0)(9 downto 6) <= x"5";
			when others => AddrOffset(0)(9 downto 6) <= x"0";
		end case;
		AddrOffset(1)(9 downto 6) <= x"0";
		AddrOffset(2)(9 downto 6) <= x"a";
		if(EnSFP(3) = '0' and or_reduce(EnSFP(2 downto 0)) = '1')then
			EnTCPIP(0) <= '1';
		else
			EnTCPIP(0) <= '0';
		end if;
		if(EnSFP(3) = '0' and (EnSFP(2 downto 0) = "011" or EnSFP(2 downto 0) = "101" or EnSFP(2 downto 0) = "110" or EnSFP(2 downto 0) = "111"))then
			EnTCPIP(1) <= '1';
		else
			EnTCPIP(1) <= '0';
		end if;
		if(EnSFP = x"7")then
			EnTCPIP(2) <= '1';
		else
			EnTCPIP(2) <= '0';
		end if;
	end if;
end process;
process(sysclk,rstCntr)
begin
	if(rstCntr = '1')then
		EvtLength_errCntr <= (others => (others => '0'));
		AMCLength_errCntr <= (others => (others => '0'));
		AMCvalid_errCntr <= (others => (others => '0'));
		AMCcrc_errCntr <= (others => (others => '0'));
	elsif(sysclk'event and sysclk = '1')then
		for i in 0 to 2 loop
			if(inc_err(i)(1) = '1')then
				EvtLength_errCntr(i) <= EvtLength_errCntr(i) + 1;
			end if;
			if(inc_err(i)(2) = '1')then
				AMClength_errCntr(i) <= AMClength_errCntr(i) + 1;
			end if;
			if(inc_err(i)(3) = '1')then
				AMCvalid_errCntr(i) <= AMCvalid_errCntr(i) + 1;
			end if;
			if(inc_err(i)(4) = '1')then
				AMCcrc_errCntr(i) <= AMCcrc_errCntr(i) + 1;
			end if;
		end loop;
	end if;
end process;
i_check_event: check_event PORT MAP(
		clk => sysclk,
		reset => resetSyncRegs(2),
		en_stop => en_stop,
		cmsCRC_err => "000",
		EventData_in => EventData_in,
		EventData_we => EventData_we,
		inc_err => inc_err,
		stop => stop
	);
process(sysclk,rstCntr)
begin
	if(rstCntr = '1')then
		StopOverWrite <= '0';
	elsif(StopOnCMScrc_err = '1')then
		StopOverWrite <= '1';
	elsif(sysclk'event and sysclk = '1')then
		if(stop = '1')then
		  StopOverWrite <= '1';
		end if;
	end if;
end process;
i_FIFO_RESET_7S: FIFO_RESET_7S PORT MAP(
		reset => resetSyncRegs(2),
		clk => sysclk,
		fifo_rst => fifo_rst,
		fifo_en => fifo_en
	);
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		for i in 0 to 2 loop
			if(resetSyncRegs(2) = '1' or EventBufAddr_we_i(i) = '1')then
				evt_FIFO_di(i)(65) <= '1';
			elsif(evt_FIFO_we(i) = '1')then
				evt_FIFO_di(i)(65) <= '0';
			end if;
		end loop;
	end if;
end process;
g_evt_FIFO: for i in 0 to 2 generate
	i_evt_fifo : FIFO_DUALCLOCK_MACRO
   generic map (
      DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
      ALMOST_FULL_OFFSET => X"0004",  -- Sets almost full threshold
      ALMOST_EMPTY_OFFSET => X"0080", -- Sets the almost empty threshold
      DATA_WIDTH => 67,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      FIFO_SIZE => "36Kb",            -- Target BRAM, "18Kb" or "36Kb" 
      FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
   port map (
      ALMOSTEMPTY => open,   -- 1-bit output almost empty
      ALMOSTFULL => evt_FIFO_full(i),     -- 1-bit output almost full
      DO => evt_FIFO_do(i),                     -- Output data, width defined by DATA_WIDTH parameter
      EMPTY => evt_FIFO_empty(i),               -- 1-bit output empty
      FULL => open,                 -- 1-bit output full
      RDCOUNT => evt_FIFO_RDCOUNT(i),           -- Output read count, width determined by FIFO depth
      RDERR => open,               -- 1-bit output read error
      WRCOUNT => evt_FIFO_WRCOUNT(i),           -- Output write count, width determined by FIFO depth
      WRERR => open,               -- 1-bit output write error
      DI => evt_FIFO_di(i),                     -- Input data, width defined by DATA_WIDTH parameter
      RDCLK => ClientClk2X,               -- 1-bit input read clock
      RDEN => evt_FIFO_re(i),                 -- 1-bit input read enable
      RST => fifo_rst,                   -- 1-bit input reset
      WRCLK => sysclk,               -- 1-bit input write clock
      WREN => evt_FIFO_we(i)                  -- 1-bit input write enable
   );
end generate;
evt_FIFO_di(0)(64 downto 0) <= EVENTdata_in(0)(64 downto 0);
evt_FIFO_di(1)(64 downto 0) <= EVENTdata_in(1)(64 downto 0);
evt_FIFO_di(2)(64 downto 0) <= EVENTdata_in(2)(64 downto 0);
evt_FIFO_di(0)(66) <= EVENTdata_in(0)(65);
evt_FIFO_di(1)(66) <= EVENTdata_in(1)(65);
evt_FIFO_di(2)(66) <= EVENTdata_in(2)(65);
evt_FIFO_we <= "000" when EnSFP(3) = '1' or fifo_en = '0' else EVENTdata_we;
evt_FIFO_re <= "000" when fifo_en = '0' else evt_FIFO_rep;
EVENTdata_avl <= not evt_FIFO_empty;
g_cmsCRC : for i in 0 to 2 generate
 i_cmsCRC: cmsCRC64 PORT MAP(
		clk => ClientClk2X,
		reset => '0',
		crc_init => cmsCRC_init(i),
		trailer => evt_FIFO_do(i)(66),
		crc_d => evt_FIFO_do(i)(63 downto 0),
		crc_ce => cmsCRC_ce(i),
		crc => open,
		crc_err => cmsCRC_err(i),
		dout => open,
		dout_vld => open
	);
end generate;
cmsCRC_ce <= evt_FIFO_rep and  EVENTdata_avl;
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		if(ClientClk2XresetSyncRegs(2) = '1')then
			cmsCRC_initp <= "000";
			cmsCRC_init <= "111";
			cmsCRC_errCntr <= (others => (others => '0'));
			StopOnCMScrc_err <= '0';
		else
			for i in 0 to 2 loop
				cmsCRC_initp(i) <= evt_FIFO_do(i)(66) and cmsCRC_ce(i);
				if(cmsCRC_err(i) = '1' and cmsCRC_init(i) = '1')then
					cmsCRC_errCntr(i) <= cmsCRC_errCntr(i) + 1;
				end if;
			end loop;
			cmsCRC_init <= cmsCRC_initp;
			if(en_stop(0) = '1' and or_reduce(cmsCRC_err and cmsCRC_init) = '1')then
				StopOnCMScrc_err <= '1';
			else
				StopOnCMScrc_err <= '0';
			end if;
		end if;
	end if;
end process;
g_TCPIP: for i in 0 to 2 generate
	i_TCPIP: TCPIP PORT MAP(
		reset => reset_TCPIP(i),
		rstCntr => rstCntr,
		sysclk => sysclk,
		clk2x => ClientClk2X,
		clk => ClientClk,
		strobe_us => strobe_us,
		strobe_ms => strobe_ms,
		en_LINK => enTCPIP(i),
		LINK_down => LINK_down(i),
		MY_PORT => x"1234",
		MY_IP => IPADDR(i),
		MY_ETH => MACADDR(i),
		TSclock => TSclock,
--		CWND_max => CWND_max,
		RTOmin => RTOmin,
		rate_limit => rate_limit(i),
		EVENTdata => evt_FIFO_do(i),
		EVENTdata_avl => EVENTdata_avl(i),
		EVENTdata_re => evt_FIFO_rep(i),
		DDR2TCPdata => DDR2TCPdata,
		RETXdata_chksum => RETXdata_checksum(i),
		RETXdata_we => RETXdata_we(i),
		RETX_ddr_out => RETX_ddr_out(i),
		re_RETX_ddr_wq => re_RETX_ddr_wq(i),
		RETX_ddr_wrqst => RETX_ddr_wrqst(i),
		RETX_ddr_data_we => RETX_ddr_data_we(i),
		RETX_ddr_rrqst => RETX_ddr_rrqst(i),
		RETX_ddr_LEN_max => RETX_ddr_LEN_max(i),
		RETX_ddr_LEN => RETX_ddr_LEN(i),
		RETXdataACK => RETXdataACK(i),
		RETXdataRqst => RETXdataRqst(i),
		RETXdataAddr => RETXdataAddr(i),
		RETXdataLEN => RETXdataLEN(i),
		KiloByte_toggle => KiloByte_toggle(i),
		EoB_toggle => EoB_toggle(i),
		TCP_wcount => TCP_wcount(i),
		EventBufAddr => EventBufAddr_i(i),
		EventBufAddr_we => EventBufAddr_we_i(i),
		AddrBuf_full => AddrBuf_full(i),
		UNA_MonBuf => UNA_MonBuf(i),
		UNA_TCPBuf => UNA_TCPBuf(i),
		EmacPhyTxC => EmacPhyTxc(i),
		EmacPhyTxD => EmacPhyTxd(i),
		PhyEmacRxC => PhyEmacRxC(i),
		PhyEmacRxD => PhyEmacRxD(i),
		ipb_addr => ipb_addr,
		ipb_rdata => TCPIP_rdata(i),
		cs_out => TCPIP_cs(i)
	);
end generate;
i_RETXdata_chksum: RETXdata_chksum PORT MAP(
		c => ClientClk2X,
		r => rst_RETXdata_chksum,
		ce => TCP_din_valid,
		d => TCP_din,
		s => RETXdata_chksum_out
	);
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		if(TCP_lastword = '1' and TCP_din_valid = '1' and TCP_din_type = "100")then
			ld_RETXdata_chksum(0) <= '1';
		else
			ld_RETXdata_chksum(0) <= '0';
		end if;
		if(TCP_lastword = '1' and TCP_din_valid = '1' and TCP_din_type = "101")then
			ld_RETXdata_chksum(1) <= '1';
		else
			ld_RETXdata_chksum(1) <= '0';
		end if;
		if(TCP_lastword = '1' and TCP_din_valid = '1' and TCP_din_type = "110")then
			ld_RETXdata_chksum(2) <= '1';
		else
			ld_RETXdata_chksum(2) <= '0';
		end if;
		ld_RETXdata_chksum_r <= ld_RETXdata_chksum;
		ld_RETXdata_chksum_r2 <= ld_RETXdata_chksum_r;
		if(ld_RETXdata_chksum_r2(0) = '1')then
			RETXdata_checksum(0) <= RETXdata_chksum_out;
		end if;
		if(ld_RETXdata_chksum_r2(1) = '1')then
			RETXdata_checksum(1) <= RETXdata_chksum_out;
		end if;
		if(ld_RETXdata_chksum_r2(2) = '1')then
			RETXdata_checksum(2) <= RETXdata_chksum_out;
		end if;
		if(ClientClk2XresetSyncRegs(2) = '1')then
			rst_RETXdata_chksum <= '1';
		else
			rst_RETXdata_chksum <= TCP_lastword and TCP_din_valid;
		end if;
		DDR2TCPdata <= TCP_lastword & TCP_din;
		if(FIFO_en = '1' and TCP_din_valid = '1' and TCP_din_type = "000")then
			RETX_ddr_data_we(0) <= '1';
		else
			RETX_ddr_data_we(0) <= '0';
		end if;
		if(FIFO_en = '1' and TCP_din_valid = '1' and TCP_din_type = "001")then
			RETX_ddr_data_we(1) <= '1';
		else
			RETX_ddr_data_we(1) <= '0';
		end if;
		if(FIFO_en = '1' and TCP_din_valid = '1' and TCP_din_type = "010")then
			RETX_ddr_data_we(2) <= '1';
		else
			RETX_ddr_data_we(2) <= '0';
		end if;
		rst_odd <= TCP_lastword and TCP_din_valid;
		if(ClientClk2XresetSyncRegs(2) = '1' or rst_odd = '1')then
			odd <= '0';
		elsif(TCP_din_valid = '1')then
			odd <= not odd;
		end if;
		if(TCP_din_valid = '1' and TCP_din_type = "100")then
			RETXdata_we(0) <= odd & not odd;
		else
			RETXdata_we(0) <= "00";
		end if;
		if(TCP_din_valid = '1' and TCP_din_type = "101")then
			RETXdata_we(1) <= odd & not odd;
		else
			RETXdata_we(1) <= "00";
		end if;
		if(TCP_din_valid = '1' and TCP_din_type = "110")then
			RETXdata_we(2) <= odd & not odd;
		else
			RETXdata_we(2) <= "00";
		end if;
		if(RETX_ddr_rp_rst = '1')then
			RETX_ddr_rp_di <= (others => '0');
			RETX_ddr_rp_we <= '1';
		else
			RETX_ddr_rp_di(16 downto 0) <= RETX_ddr_rp_do(16 downto 0) + TCP_length_i(20 downto 4);
			RETX_ddr_rp_we <= TCP_rack and not TCP_raddr_i(28);
		end if;
		if(ClientClk2XresetSyncRegs(2) = '1')then
			ReTx_ddr_LEN_max <= (others => "10000");
		elsif(RETX_ddr_rp_we = '1')then
			if(and_reduce(RETX_ddr_rp_di(8 downto 4)) = '0')then -- ReTx_ddr_LEN_max insures the readout does not go into event data area
				ReTx_ddr_LEN_max(conv_integer(RETX_ddr_rp_a)) <= "10000";
			else
				ReTx_ddr_LEN_max(conv_integer(RETX_ddr_rp_a)) <= "10000" - ('0' & RETX_ddr_rp_di(3 downto 0));
			end if;
		end if;
		if(ClientClk2XresetSyncRegs(2) = '1')then
			RETX_ddr_rp_rst <= '1';
			RETX_ddr_rp_a <= (others => '0');
			TCP_rrqst_i <= "000";
			rrqstMask <= (others => '1');
		else
			if(RETX_ddr_rp_a(1) = '1')then
				RETX_ddr_rp_rst <= '0';
			end if;
			if(RETX_ddr_rp_rst = '1')then
				RETX_ddr_rp_a(1 downto 0) <= RETX_ddr_rp_a(1 downto 0) + 1;
			elsif(TCP_rrqst_i(0) = '0')then
				if(or_reduce(rrqstMask(5 downto 3) and RETXdataRqst) = '1')then
					RETX_ddr_rp_a(1 downto 0) <= "11";
				elsif(rrqstMask(0) = '1' and RETX_ddr_rrqst(0) = '1')then
					RETX_ddr_rp_a(1 downto 0) <= "00";
				elsif(rrqstMask(1) = '1' and RETX_ddr_rrqst(1) = '1')then
					RETX_ddr_rp_a(1 downto 0) <= "01";
				else
					RETX_ddr_rp_a(1 downto 0) <= "10";
				end if;
			end if;
			if(TCP_rack = '1')then
				TCP_rrqst_i <= "000";
			else
				if(or_reduce(rrqstMask(2 downto 0) and RETX_ddr_rrqst) = '1' or or_reduce(rrqstMask(5 downto 3) and RETXdataRqst) = '1')then
					TCP_rrqst_i(0) <= '1';
				end if;
				TCP_rrqst_i(2 downto 1) <= TCP_rrqst_i(1 downto 0);
			end if;
			for i in 0 to 2 loop
				if(TCP_rrqst_i(0) = '1' and TCP_raddr_i(28) = '0' and i = conv_integer(TCP_raddr_i(27 downto 26)))then
					rrqstMask(i) <= '0';
				elsif(rst_RETXdata_chksum = '1' and TCP_din_type(2) = '0' and i = conv_integer(TCP_din_type(1 downto 0)))then
					rrqstMask(i) <= '1';
				end if;
				if(TCP_rrqst_i(0) = '1' and TCP_raddr_i(28) = '1' and i = conv_integer(TCP_raddr_i(27 downto 26)))then
					rrqstMask(i+3) <= '0';
				elsif(rst_RETXdata_chksum = '1' and TCP_din_type(2) = '1' and i = conv_integer(TCP_din_type(1 downto 0)))then
					rrqstMask(i+3) <= '1';
				end if;
			end loop;
		end if;
		if(TCP_rrqst_i(0) = '0')then
			RETXdataAck <= (others => '0');
		elsif(TCP_raddr_i(28) = '1')then
			case TCP_raddr_i(27 downto 26) is
				when "00" => RETXdataAck <= "001";
				when "01" => RETXdataAck <= "010";
				when "10" => RETXdataAck <= "100";
				when others => RETXdataAck <= "000";
			end case;
		end if;
		if(TCP_rrqst_i(0) = '0')then
			if(or_reduce(rrqstMask(5 downto 3) and RETXdataRqst) = '1')then
				TCP_raddr_i(28) <= '1';
				if(rrqstMask(3) = '1' and RETXdataRqst(0) = '1')then
					TCP_raddr_i(27 downto 26) <= "00";
				elsif(rrqstMask(4) = '1' and RETXdataRqst(1) = '1')then
					TCP_raddr_i(27 downto 26) <= "01";
				else
					TCP_raddr_i(27 downto 26) <= "10";
				end if;
			else
				TCP_raddr_i(28) <= '0';
				if(rrqstMask(0) = '1' and RETX_ddr_rrqst(0) = '1')then
					TCP_raddr_i(27 downto 26) <= "00";
				elsif(rrqstMask(1) = '1' and RETX_ddr_rrqst(1) = '1')then
					TCP_raddr_i(27 downto 26) <= "01";
				else
					TCP_raddr_i(27 downto 26) <= "10";
				end if;
			end if;
		end if;
		case TCP_raddr_i(28 downto 26) is
			when "100" => TCP_raddr_i(25 downto 0) <= RETXdataAddr(0);
			when "101" => TCP_raddr_i(25 downto 0) <= RETXdataAddr(1);
			when "110" => TCP_raddr_i(25 downto 0) <= RETXdataAddr(2);
			when others => TCP_raddr_i(25 downto 0) <= TCP_raddr_i(27 downto 26)(1 downto 0) & RETX_ddr_rp_do(16 downto 9) & x"f" & RETX_ddr_rp_do(8 downto 0) & "000";
		end case;
		TCP_rlength <= TCP_length_i(13 downto 1) - 1;
		case TCP_raddr_i(28 downto 26) is
			when "000" => TCP_length_i(13 downto 0) <= "00000" & RETX_ddr_LEN(0) & x"0";
			when "001" => TCP_length_i(13 downto 0) <= "00000" & RETX_ddr_LEN(1) & x"0";
			when "010" => TCP_length_i(13 downto 0) <= "00000" & RETX_ddr_LEN(2) & x"0";
			when "100" => TCP_length_i(13 downto 0) <= RETXdataLEN(0) & '0';
			when "101" => TCP_length_i(13 downto 0) <= RETXdataLEN(1) & '0';
			when "110" => TCP_length_i(13 downto 0) <= RETXdataLEN(2) & '0';
			when others => TCP_length_i(13 downto 0) <= (others => '0');
		end case;
	end if;
end process;
g_RETX_ddr_rp : for i in 0 to 2 generate
   i_RETX_ddr_rp : RAM32x6Db PORT MAP(
		wclk => ClientClk2X,
		di => RETX_ddr_rp_di(i*6+5 downto i*6),
		we => RETX_ddr_rp_we,
		wa => RETX_ddr_rp_a,
		ra => RETX_ddr_rp_a,
		do => RETX_ddr_rp_do(i*6+5 downto i*6)
	);
end generate;
TCP_we <= TCP_wFIFO_we;
i_TCP_wFIFO_we : SRL16E
   port map (
      Q => TCP_wFIFO_we,       -- SRL data output
      A0 => '0',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => ClientClk2X,   -- Clock input
      D => TCP_w_busy        -- SRL data input
   );
TCP_channel <= TCP_wFIFO_DI(33 downto 32);
TCP_dout <= TCP_wFIFO_DI(31 downto 0);
process(ClientClk2X, TCP_w_sel, RETX_ddr_wrqst)
variable s : std_logic_vector(4 downto 0);
begin
	s := TCP_w_sel & RETX_ddr_wrqst;
	if(ClientClk2X'event and ClientClk2X = '1')then
		if(FIFO_en = '0' or ClientClk2XresetSyncRegs(2) = '1')then
			TCP_w_sel <= "00";
		elsif(TCP_w_busy = '0')then
			case s is
				when "00001" | "01001" | "01011" | "10001" | "10011" | "10101" | "10111" =>
					TCP_w_sel <= "00";
				when "00010" | "00011" | "00110" | "00111" | "01010" | "10010" | "10110" =>
					TCP_w_sel <= "01";
				when "00100" | "00101" | "01100" | "01101" | "01110" | "01111" | "10100" =>
					TCP_w_sel <= "10";
				when others => null;	
			end case;
		end if;
		if(FIFO_en = '0' or ClientClk2XresetSyncRegs(2) = '1')then
			TCP_w_busy <= '0';
		elsif(TCP_w_wc = x"f")then
			TCP_w_busy <= '0';
		elsif(RETX_ddr_wrqst /= "000")then
			TCP_w_busy <= '1';
		end if;
		if(ClientClk2XresetSyncRegs(2) = '1')then
			TCP_w_wc <= x"0";
		elsif(TCP_w_busy = '1')then
			TCP_w_wc <= TCP_w_wc + 1;
		end if;
		if(FIFO_en = '0' or ClientClk2XresetSyncRegs(2) = '1')then
			re_RETX_ddr_wq <= "000";
		else
			case TCP_w_sel is
				when "00" => re_RETX_ddr_wq(0) <= TCP_w_busy;
				when "01" => re_RETX_ddr_wq(1) <= TCP_w_busy;
				when others => re_RETX_ddr_wq(2) <= TCP_w_busy;
			end case;
		end if;
		case TCP_w_sel is
			when "00" => TCP_wFIFO_DI <= "00" & RETX_ddr_out(0);
			when "01" => TCP_wFIFO_DI <= "01" & RETX_ddr_out(1);
			when others => TCP_wFIFO_DI <= "10" & RETX_ddr_out(2);
		end case;
	end if;
end process;
process(ClientClk2X)
begin
	if(ClientClk2X'event and ClientClk2X = '1')then
		case EnSFP(2 downto 0) is
			when "010" | "011" | "101" | "111" => TCPIP2SFP_sel(0) <= "01";
			when "100" | "110" => TCPIP2SFP_sel(0) <= "10";
			when others => TCPIP2SFP_sel(0) <= "00";
		end case;
		case EnSFP(2 downto 0) is
			when "010" | "011" | "111" => TCPIP2SFP_sel(1) <= "00";
			when "101" => TCPIP2SFP_sel(1) <= "10";
			when others => TCPIP2SFP_sel(1) <= "01";
		end case;
		case EnSFP(2 downto 0) is
			when "100" | "101" | "110" => TCPIP2SFP_sel(2) <= "00";
			when others => TCPIP2SFP_sel(2) <= "10";
		end case;
		for i in 0 to 1 loop
			SFP_EmacPhyTxc(i) <= EmacPhyTxc(conv_integer(TCPIP2SFP_sel(i)));
			SFP_EmacPhyTxD(i) <= EmacPhyTxD(conv_integer(TCPIP2SFP_sel(i)));
			SFP_down_i(i) <= LINK_down(conv_integer(TCPIP2SFP_sel(i)));
		end loop;
		if(TCPIP2SFP_sel(2)(1) = '0')then
			SFP_EmacPhyTxc(2) <= EmacPhyTxc(0);
			SFP_EmacPhyTxD(2) <= EmacPhyTxD(0);
			SFP_down_i(2) <= LINK_down(0);
		else
			SFP_EmacPhyTxc(2) <= EmacPhyTxc(2);
			SFP_EmacPhyTxD(2) <= EmacPhyTxD(2);
			SFP_down_i(2) <= LINK_down(2);
		end if;
		case EnSFP(2 downto 0) is
			when "010" | "011" | "111" => SFP2TCPIP(0) <= "01";
			when "100" | "101" | "110" => SFP2TCPIP(0) <= "10";
			when others => SFP2TCPIP(0) <= "00";
		end case;
		case EnSFP(2 downto 0) is
			when "010" | "011" | "101" | "111" => SFP2TCPIP(1) <= "00";
			when others => SFP2TCPIP(1) <= "01";
		end case;
		case EnSFP(2 downto 0) is
			when "100" | "110" => SFP2TCPIP(2) <= "00";
			when "101" => SFP2TCPIP(2) <= "01";
			when others => SFP2TCPIP(2) <= "10";
		end case;
--		if(SFP_PhyEmacRxc(0) = x"1" and SFP_PhyEmacRxd(0) = x"555555fb")then
--			Rx_start_cntr(15 downto 0) <= Rx_start_cntr(15 downto 0) + 1;
--		end if;
--		if(SFP_PhyEmacRxc(1) = x"1" and SFP_PhyEmacRxd(1) = x"555555fb")then
--			Rx_start_cntr(31 downto 16) <= Rx_start_cntr(31 downto 16) + 1;
--		end if;
		PhyEmacRxc(0) <= SFP_PhyEmacRxc(conv_integer(SFP2TCPIP(0)));
		PhyEmacRxD(0) <= SFP_PhyEmacRxD(conv_integer(SFP2TCPIP(0)));
		if(SFP2TCPIP(1)(0) = '0')then
			PhyEmacRxc(1) <= SFP_PhyEmacRxc(0);
			PhyEmacRxD(1) <= SFP_PhyEmacRxD(0);
		else
			PhyEmacRxc(1) <= SFP_PhyEmacRxc(1);
			PhyEmacRxD(1) <= SFP_PhyEmacRxD(1);
		end if;
		PhyEmacRxc(2) <= SFP_PhyEmacRxc(conv_integer(SFP2TCPIP(2)));
		PhyEmacRxD(2) <= SFP_PhyEmacRxD(conv_integer(SFP2TCPIP(2)));
		IPADDR(0) <= SFP_IPADDR(conv_integer(SFP2TCPIP(0)));
		IPADDR(1) <= SFP_IPADDR(conv_integer(SFP2TCPIP(1)));
		IPADDR(2) <= SFP_IPADDR(conv_integer(SFP2TCPIP(2)));
		rate_limit(0) <= SFP_rate_limit(conv_integer(SFP2TCPIP(0)));
		rate_limit(1) <= SFP_rate_limit(conv_integer(SFP2TCPIP(1)));
		rate_limit(2) <= SFP_rate_limit(conv_integer(SFP2TCPIP(2)));
--		MACADDR(0) <= x"00ac1234568" & "10" & SFP2TCPIP(0);
--		MACADDR(1) <= x"00ac1234568" & "100" & SFP2TCPIP(1)(0);
--		MACADDR(2) <= x"00ac1234568" & "10" & SFP2TCPIP(2);
		MACADDR(0) <= x"00ac12345" & not SN & '0' & SFP2TCPIP(0);
		MACADDR(1) <= x"00ac12345" & not SN & "00" & SFP2TCPIP(1)(0);
		MACADDR(2) <= x"00ac12345" & not SN & '0' & SFP2TCPIP(2);
		reset_TCPIP(0) <= SFPresetSyncRegs(conv_integer(SFP2TCPIP(0)))(2);
		if(SFP2TCPIP(1)(0) = '0')then
			reset_TCPIP(1) <= SFPresetSyncRegs(conv_integer(SFP2TCPIP(0)))(2);
		else
			reset_TCPIP(1) <= SFPresetSyncRegs(conv_integer(SFP2TCPIP(1)))(2);
		end if;
		reset_TCPIP(2) <= SFPresetSyncRegs(conv_integer(SFP2TCPIP(2)))(2);
	end if;
end process;
g_XGbEPCS : for i in 0 to 2 generate
	i_XGbEPCS: XGbEPCS32 PORT MAP (
		reset => TCPresetSyncRegs(2),
--		reset => PCS_reset(i),
		clk2x => ClientClk2X,
		clk => ClientClk,
		TXUSRCLK => txusrclk,
		TX_high => TX_high,
		RXUSRCLK => SFP_RXUSRCLK(i),
		RXRESETDONE => SFP_RXRESETDONE(i),
		inh_TX => inh_TX(i),
		RESET_TXSync => reset_TXSyncRegs(2),
		GTX_RXGEARBOXSLIP_OUT => SFP_RXGEARBOXSLIP(i),
		GTX_TXD => SFP_TXD(i),
		GTX_TXHEADER => SFP_TXHEADER(i),
		GTX_TX_PAUSE => GTX_TX_PAUSE,
		GTX_RXD => SFP_RXD(i),
		GTX_RXDVLD => SFP_RXDVLD(i),
		GTX_RXHEADER => SFP_RXHEADER(i),
		GTX_RXHEADERVLD => SFP_RXHEADERVLD(i),
		GTX_RXGOOD => SFP_RXGOOD(i),
		EmacPhyTxC => SFP_EmacPhyTxc(i),
		EmacPhyTxD => SFP_EmacPhyTxd(i),
		PhyEmacRxC => SFP_PhyEmacRxC(i),
		PhyEmacRxD => SFP_PhyEmacRxD(i)
	);
end generate;
SFP_pd(0) <= "00" when Dis_pd = '1' else "11" when enSFP(3) = '1' or enSFP(0) = '0' else "00";
SFP_pd(1) <= "00" when Dis_pd = '1' else "11" when enSFP(3) = '1' or enSFP(1) = '0' else "00";
SFP_pd(2) <= "00" when Dis_pd = '1' else "11" when enSFP(3) = '1' or enSFP(2) = '0' else "00";
process(DRPclk)
begin
	if(DRPclk'event and DRPclk = '1')then
		for i in 0 to 2 loop
			SFP_pd_q(i) <= SFP_pd_q(i)(2 downto 0) & SFP_pd(i)(0);
		end loop;
		if(SFP_pd_q(0)(3 downto 2) = "10" or SFP_pd_q(1)(3 downto 2) = "10" or SFP_pd_q(2)(3 downto 2) = "10")then
			reset_cntr <= (others => '0');
		elsif(reset_cntr(20) = '0')then
			reset_cntr <= reset_cntr + 1;
		end if;
		reset_cntr20_q <= reset_cntr(20);
		soft_reset <= not reset_cntr20_q and reset_cntr(20);
	end if;
end process;
i_SFP3_init : SFP3_v2_7_init
    port map
    (
        SYSCLK_IN                       =>      DRPclk,
        SOFT_RESET_IN                   =>      soft_reset,
        DONT_RESET_ON_DATA_ERROR_IN     =>      '0',
        GT0_TX_FSM_RESET_DONE_OUT       =>      SFP_TX_FSM_RESET_DONE(0),
        GT0_RX_FSM_RESET_DONE_OUT       =>      SFP_RX_FSM_RESET_DONE(0),
        GT0_DATA_VALID_IN               =>      SFP_RXGOOD(0),
        GT1_TX_FSM_RESET_DONE_OUT       =>      SFP_TX_FSM_RESET_DONE(1),
        GT1_RX_FSM_RESET_DONE_OUT       =>      SFP_RX_FSM_RESET_DONE(1),
        GT1_DATA_VALID_IN               =>      SFP_RXGOOD(1),
        GT2_TX_FSM_RESET_DONE_OUT       =>      SFP_TX_FSM_RESET_DONE(2),
        GT2_RX_FSM_RESET_DONE_OUT       =>      SFP_RX_FSM_RESET_DONE(2),
        GT2_DATA_VALID_IN               =>      SFP_RXGOOD(2),

  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X1Y12)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT0_DRPADDR_IN                  => SFP_drpaddr(0),
        GT0_DRPCLK_IN                   => DRPclk,
        GT0_DRPDI_IN                    => SFP_drpdi(0),
        GT0_DRPDO_OUT                   => SFP_drpdo(0),
        GT0_DRPEN_IN                    => SFP_drpen(0),
        GT0_DRPRDY_OUT                  => SFP_drprdy(0),
        GT0_DRPWE_IN                    => SFP_drpwe(0),
        ------------------------------- Loopback Ports -----------------------------
        GT0_LOOPBACK_IN                 => SFP_LOOPBACK_IN(0),
        ------------------------------ Power-Down Ports ----------------------------
        GT0_RXPD_IN                     => SFP_pd(0),
        GT0_TXPD_IN                     => SFP_pd(0),
        --------------------- RX Initialization and Reset Ports --------------------
        GT0_RXUSERRDY_IN                => SFP_rxuserrdy(0),
        -------------------------- RX Margin Analysis Ports ------------------------
        GT0_EYESCANDATAERROR_OUT        => SFP_EYESCANDATAERROR_OUT(0),
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT0_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT0_RXUSRCLK_IN                 => SFP_RXUSRCLK(0),
        GT0_RXUSRCLK2_IN                => SFP_RXUSRCLK(0),
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT0_RXDATA_OUT                  => SFP_RXD_inv(0),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT0_RXPRBSERR_OUT               => SFP_RXPRBSERR_OUT(0),
        GT0_RXPRBSSEL_IN                => SFP_RXPRBSSEL_IN(0),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT0_RXPRBSCNTRESET_IN           =>      '0',
        --------------------------- Receive Ports - RX AFE -------------------------
        GT0_GTXRXP_IN                   => SFP0_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT0_GTXRXN_IN                   => SFP0_RXN,
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT0_RXBUFRESET_IN               => '0',
        GT0_RXBUFSTATUS_OUT             => open,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        GT0_RXOUTCLK_OUT                => SFP_rxoutclk(0),
        ---------------------- Receive Ports - RX Gearbox Ports --------------------
        GT0_RXDATAVALID_OUT             => SFP_RXDVLD(0),
        GT0_RXHEADER_OUT                => SFP_RXHEADER(0),
        GT0_RXHEADERVALID_OUT           => SFP_RXHEADERVLD(0),
        --------------------- Receive Ports - RX Gearbox Ports  --------------------
        GT0_RXGEARBOXSLIP_IN            => SFP_RXGEARBOXSLIP(0),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT0_GTRXRESET_IN                => '0',
        GT0_RXPMARESET_IN               => '0',
        ------------------ Receive Ports - RX Margin Analysis ports ----------------
        GT0_RXLPMEN_IN                  => '0',
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT0_RXRESETDONE_OUT             => SFP_rxresetdone(0),
        --------------------- TX Initialization and Reset Ports --------------------
        GT0_GTTXRESET_IN                => '0',
        GT0_TXUSERRDY_IN                => SFP_txuserrdy(0),
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT0_TXUSRCLK_IN                 => txusrclk,
        GT0_TXUSRCLK2_IN                => txusrclk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT0_TXDIFFCTRL_IN               => "1110",
        GT0_TXINHIBIT_IN                => '0',
        GT0_TXMAINCURSOR_IN             => (others => '0'),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_TXDATA_IN                   => SFP_TXD_inv(0),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  => SFP0_TXN,
        GT0_GTXTXP_OUT                  => SFP0_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT0_TXOUTCLK_OUT                => SFP_TXOUTCLK(0),
        GT0_TXOUTCLKFABRIC_OUT          => open,
        GT0_TXOUTCLKPCS_OUT             => open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT0_TXHEADER_IN                 => SFP_TXHEADER(0),
        GT0_TXSEQUENCE_IN               => SFP_TXSEQUENCE(0),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT0_TXRESETDONE_OUT             => open,
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT0_TXPRBSSEL_IN                => SFP_TXPRBSSEL_IN(0),


  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1  (X1Y13)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT1_DRPADDR_IN                  => SFP_drpaddr(1),
        GT1_DRPCLK_IN                   => DRPclk,
        GT1_DRPDI_IN                    => SFP_drpdi(1),
        GT1_DRPDO_OUT                   => SFP_drpdo(1),
        GT1_DRPEN_IN                    => SFP_drpen(1),
        GT1_DRPRDY_OUT                  => SFP_drprdy(1),
        GT1_DRPWE_IN                    => SFP_drpwe(1),
        ------------------------------- Loopback Ports -----------------------------
        GT1_LOOPBACK_IN                 => SFP_LOOPBACK_IN(1),
        ------------------------------ Power-Down Ports ----------------------------
        GT1_RXPD_IN                     => SFP_pd(1),
        GT1_TXPD_IN                     => SFP_pd(1),
        --------------------- RX Initialization and Reset Ports --------------------
        GT1_RXUSERRDY_IN                => SFP_rxuserrdy(1),
        -------------------------- RX Margin Analysis Ports ------------------------
        GT1_EYESCANDATAERROR_OUT        => SFP_EYESCANDATAERROR_OUT(1),
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT1_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT1_RXUSRCLK_IN                 => SFP_RXUSRCLK(1),
        GT1_RXUSRCLK2_IN                => SFP_RXUSRCLK(1),
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT1_RXDATA_OUT                  => SFP_RXD_inv(1),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT1_RXPRBSERR_OUT               => SFP_RXPRBSERR_OUT(1),
        GT1_RXPRBSSEL_IN                => SFP_RXPRBSSEL_IN(1),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT1_RXPRBSCNTRESET_IN           =>      '0',
        --------------------------- Receive Ports - RX AFE -------------------------
        GT1_GTXRXP_IN                   => SFP1_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT1_GTXRXN_IN                   => SFP1_RXN,
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT1_RXBUFRESET_IN               => '0',
        GT1_RXBUFSTATUS_OUT             => open,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        GT1_RXOUTCLK_OUT                => SFP_rxoutclk(1),
        ---------------------- Receive Ports - RX Gearbox Ports --------------------
        GT1_RXDATAVALID_OUT             => SFP_RXDVLD(1),
        GT1_RXHEADER_OUT                => SFP_RXHEADER(1),
        GT1_RXHEADERVALID_OUT           => SFP_RXHEADERVLD(1),
        --------------------- Receive Ports - RX Gearbox Ports  --------------------
        GT1_RXGEARBOXSLIP_IN            => SFP_RXGEARBOXSLIP(1),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT1_GTRXRESET_IN                => '0',
        GT1_RXPMARESET_IN               => '0',
        ------------------ Receive Ports - RX Margin Analysis ports ----------------
        GT1_RXLPMEN_IN                  => '0',
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT1_RXRESETDONE_OUT             => SFP_rxresetdone(1),
        --------------------- TX Initialization and Reset Ports --------------------
        GT1_GTTXRESET_IN                => '0',
        GT1_TXUSERRDY_IN                => SFP_txuserrdy(1),
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT1_TXUSRCLK_IN                 => txusrclk,
        GT1_TXUSRCLK2_IN                => txusrclk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT1_TXDIFFCTRL_IN               => "1110",
        GT1_TXINHIBIT_IN                => '0',
        GT1_TXMAINCURSOR_IN             => (others => '0'),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT1_TXDATA_IN                   => SFP_TXD_inv(1),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT1_GTXTXN_OUT                  => SFP1_TXN,
        GT1_GTXTXP_OUT                  => SFP1_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT1_TXOUTCLK_OUT                => open,
        GT1_TXOUTCLKFABRIC_OUT          => open,
        GT1_TXOUTCLKPCS_OUT             => open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT1_TXHEADER_IN                 => SFP_TXHEADER(1),
        GT1_TXSEQUENCE_IN               => SFP_TXSEQUENCE(1),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT1_TXRESETDONE_OUT             => open,
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT1_TXPRBSSEL_IN                => SFP_TXPRBSSEL_IN(1),
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT2  (X1Y14)

        ---------------------------- Channel - DRP Ports  --------------------------
        GT2_DRPADDR_IN                  => SFP_drpaddr(2),
        GT2_DRPCLK_IN                   => DRPclk,
        GT2_DRPDI_IN                    => SFP_drpdi(2),
        GT2_DRPDO_OUT                   => SFP_drpdo(2),
        GT2_DRPEN_IN                    => SFP_drpen(2),
        GT2_DRPRDY_OUT                  => SFP_drprdy(2),
        GT2_DRPWE_IN                    => SFP_drpwe(2),
        ------------------------------- Loopback Ports -----------------------------
        GT2_LOOPBACK_IN                 => SFP_LOOPBACK_IN(2),
        ------------------------------ Power-Down Ports ----------------------------
        GT2_RXPD_IN                     => SFP_pd(2),
        GT2_TXPD_IN                     => SFP_pd(2),
        --------------------- RX Initialization and Reset Ports --------------------
        GT2_RXUSERRDY_IN                => SFP_rxuserrdy(2),
        -------------------------- RX Margin Analysis Ports ------------------------
        GT2_EYESCANDATAERROR_OUT        => SFP_EYESCANDATAERROR_OUT(2),
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT2_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT2_RXUSRCLK_IN                 => SFP_RXUSRCLK(2),
        GT2_RXUSRCLK2_IN                => SFP_RXUSRCLK(2),
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT2_RXDATA_OUT                  => SFP_RXD_inv(2),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT2_RXPRBSERR_OUT               => SFP_RXPRBSERR_OUT(2),
        GT2_RXPRBSSEL_IN                => SFP_RXPRBSSEL_IN(2),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT2_RXPRBSCNTRESET_IN           =>      '0',
        --------------------------- Receive Ports - RX AFE -------------------------
        GT2_GTXRXP_IN                   => SFP2_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT2_GTXRXN_IN                   => SFP2_RXN,
        ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
        GT2_RXBUFRESET_IN               => '0',
        GT2_RXBUFSTATUS_OUT             => open,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        GT2_RXOUTCLK_OUT                => SFP_rxoutclk(2),
        ---------------------- Receive Ports - RX Gearbox Ports --------------------
        GT2_RXDATAVALID_OUT             => SFP_RXDVLD(2),
        GT2_RXHEADER_OUT                => SFP_RXHEADER(2),
        GT2_RXHEADERVALID_OUT           => SFP_RXHEADERVLD(2),
        --------------------- Receive Ports - RX Gearbox Ports  --------------------
        GT2_RXGEARBOXSLIP_IN            => SFP_RXGEARBOXSLIP(2),
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT2_GTRXRESET_IN                => '0',
        GT2_RXPMARESET_IN               => '0',
        ------------------ Receive Ports - RX Margin Analysis ports ----------------
        GT2_RXLPMEN_IN                  => '0',
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT2_RXRESETDONE_OUT             => SFP_rxresetdone(2),
        --------------------- TX Initialization and Reset Ports --------------------
        GT2_GTTXRESET_IN                => '0',
        GT2_TXUSERRDY_IN                => SFP_txuserrdy(2),
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT2_TXUSRCLK_IN                 => txusrclk,
        GT2_TXUSRCLK2_IN                => txusrclk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT2_TXDIFFCTRL_IN               => "1110",
        GT2_TXINHIBIT_IN                => '0',
        GT2_TXMAINCURSOR_IN             => (others => '0'),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT2_TXDATA_IN                   => SFP_TXD_inv(2),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT2_GTXTXN_OUT                  => SFP2_TXN,
        GT2_GTXTXP_OUT                  => SFP2_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT2_TXOUTCLK_OUT                => open,
        GT2_TXOUTCLKFABRIC_OUT          => open,
        GT2_TXOUTCLKPCS_OUT             => open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT2_TXHEADER_IN                 => SFP_TXHEADER(2),
        GT2_TXSEQUENCE_IN               => SFP_TXSEQUENCE(2),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT2_TXRESETDONE_OUT             => open,
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT2_TXPRBSSEL_IN                => SFP_TXPRBSSEL_IN(2),


    --____________________________COMMON PORTS________________________________
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GT0_GTREFCLK0_COMMON_IN         => REFCLK,
        ------------------------- Common Block - QPLL Ports ------------------------
        GT0_QPLLLOCK_OUT                => qplllock,
        GT0_QPLLLOCKDETCLK_IN           => DRPclk,
        GT0_QPLLRESET_IN                => qpllreset

    );
process(SFP_TXD,SFP_RXD,SFP_RXD_inv)
	begin
		for j in 0 to 2 loop
			for i in 0 to 31 loop
				SFP_TXD_inv(j)(i) <= SFP_TXD(j)(31-i);
				SFP_RXD(j)(i) <= SFP_RXD_inv(j)(31-i);
			end loop;
		end loop;
end process;
i_REFCLK : IBUFDS_GTE2 port map(O => REFCLK, ODIV2 => open, CEB => '0', I => SFP_REFCLK_P, IB => SFP_REFCLK_N);
i_txusrclk : BUFG port map (I => SFP_TXOUTCLK(0), O => txusrclk);
g_SFP_rxusrclk : for i in 0 to 2 generate
	i_SFP_rxusrclk : BUFG port map (I => SFP_RXOUTCLK(i), O => SFP_rxusrclk(i));
end generate;
i_REFCLK2X_in: bufg port map(i => REFCLK, o => REFCLK2X_in);
i_ClientClk2X : BUFG port map (I => ClientClk2X_dcm, O => ClientClk2X);
i_ClientClk : BUFG port map (I => ClientClk_dcm, O => ClientClk);
i_REFCLK2XPLLRST : SRL16 generic map(INIT => x"ffff")
   port map (
      Q => REFCLK2XPLLRST,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CLK => REFCLK2X_in,   -- Clock input
      D => '0'        -- SRL data input
   );
i_REFCLK2XPLL : PLLE2_BASE
   generic map (
      BANDWIDTH => "OPTIMIZED",  -- OPTIMIZED, HIGH, LOW
      CLKFBOUT_MULT => 8,        -- Multiply value for all CLKOUT, (2-64)
      CLKFBOUT_PHASE => 0.0,     -- Phase offset in degrees of CLKFB, (-360.000-360.000).
      CLKIN1_PERIOD => 6.4,      -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT0_DIVIDE => 4,
      DIVCLK_DIVIDE => 1,        -- Master division value, (1-56)
      REF_JITTER1 => 0.0,        -- Reference input jitter in UI, (0.000-0.999).
      STARTUP_WAIT => "FALSE"    -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
   )
   port map (
      -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
      CLKOUT0 => ClientClk2X_dcm,
     -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
      CLKFBOUT => ClientClk_dcm, -- 1-bit output: Feedback clock
      -- Status Port: 1-bit (each) output: PLL status ports
      LOCKED => ClientClk_lock,     -- 1-bit output: LOCK
      -- Clock Input: 1-bit (each) input: Clock input
      CLKIN1 => REFCLK2X_in,     -- 1-bit input: Input clock
      -- Control Ports: 1-bit (each) input: PLL control ports
      PWRDWN => '0',     -- 1-bit input: Power-down
      RST => REFCLK2XPLLRST,           -- 1-bit input: Reset
      -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
      CLKFBIN => ClientClk    -- 1-bit input: Feedback clock
   );
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1c20" and ipb_write = '1' and ipb_strobe = '1')then
			en_stop <= ipb_wdata(4 downto 0);
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1c21" and ipb_write = '1' and ipb_strobe = '1')then
			SFP_rate_limit(0)(6 downto 0) <= ipb_wdata(6 downto 0);
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1c22" and ipb_write = '1' and ipb_strobe = '1')then
			SFP_rate_limit(1)(6 downto 0) <= ipb_wdata(6 downto 0);
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1c23" and ipb_write = '1' and ipb_strobe = '1')then
			SFP_rate_limit(2)(6 downto 0) <= ipb_wdata(6 downto 0);
		end if;
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 0) = x"1c2f" and ipb_write = '1' and ipb_strobe = '1')then
			RTOmin <= ipb_wdata(15 downto 0);
		end if;
	end if;
end process;
process(ipb_addr)
variable s : std_logic_vector(1 downto 0);
begin
	case ipb_addr(11 downto 10) is
		when "00" =>
			case enSFP(2 downto 0) is
				when "010" | "011" | "101" | "111" => s := "01";
				when "110" => s := "10";
				when others => s := "00";
			end case;
		when "01" =>
			case enSFP(2 downto 0) is
				when "010" | "011" | "111" => s := "00";
				when "101" => s := "10";
				when others => s := "01";
			end case;
		when "10" =>
			case enSFP(2 downto 0) is
				when "100" | "101" | "110" => s := "00";
				when others => s := "10";
			end case;
		when others => s := "11";
	end case;
	if(ipb_addr(15 downto 12) = x"1")then
		case s is
			when "00" => 
			  if(ipb_addr(9 downto 8) = "11")then
			    case ipb_addr(3 downto 0) is
			      when x"0" => ipb_rdata <= TotalEvtLengthCntr(0)(31 downto 0);
			      when x"1" => ipb_rdata <= x"00" & TotalEvtLengthCntr(0)(55 downto 32);
			      when x"2" => ipb_rdata <= EvtLength_errCntr(0);
			      when x"3" => ipb_rdata <= AMClength_errCntr(0);
			      when x"4" => ipb_rdata <= AMCvalid_errCntr(0);
			      when x"5" => ipb_rdata <= AMCcrc_errCntr(0);
			      when others => ipb_rdata <= (others => '0');
			    end case;
			  else
			    ipb_rdata <= TCPIP_rdata(0);
			  end if;
			when "01" =>
			  if(ipb_addr(9 downto 8) = "11")then
			    case ipb_addr(3 downto 0) is
			      when x"0" => ipb_rdata <= TotalEvtLengthCntr(1)(31 downto 0);
			      when x"1" => ipb_rdata <= x"00" & TotalEvtLengthCntr(1)(55 downto 32);
			      when x"2" => ipb_rdata <= EvtLength_errCntr(1);
			      when x"3" => ipb_rdata <= AMClength_errCntr(1);
			      when x"4" => ipb_rdata <= AMCvalid_errCntr(1);
			      when x"5" => ipb_rdata <= AMCcrc_errCntr(1);
			      when others => ipb_rdata <= (others => '0');
			    end case;
        else
          ipb_rdata <= TCPIP_rdata(1);
        end if;
			when "10" =>
			  if(ipb_addr(9 downto 8) = "11")then
			    case ipb_addr(3 downto 0) is
			      when x"0" => ipb_rdata <= TotalEvtLengthCntr(2)(31 downto 0);
			      when x"1" => ipb_rdata <= x"00" & TotalEvtLengthCntr(2)(55 downto 32);
			      when x"2" => ipb_rdata <= EvtLength_errCntr(2);
			      when x"3" => ipb_rdata <= AMClength_errCntr(2);
			      when x"4" => ipb_rdata <= AMCvalid_errCntr(2);
			      when x"5" => ipb_rdata <= AMCcrc_errCntr(2);
			      when others => ipb_rdata <= (others => '0');
			    end case;
        else
          ipb_rdata <= TCPIP_rdata(2);
        end if;
			when others =>
				case ipb_addr(5 downto 4) is
					when "00" => 
						case ipb_addr(3 downto 0) is
							when x"0" => ipb_rdata <= "00000" & NXT_MonBuf(1) & "00000" & NXT_MonBuf(0);
							when x"1" => ipb_rdata <= x"0000" & "00000" & NXT_MonBuf(2);
							when x"2" => ipb_rdata <= "00000" & UNA_MonBuf(1) & "00000" & UNA_MonBuf(0);
							when x"3" => ipb_rdata <= "00000" & UNA_MonBuf(3) & "00000" & UNA_MonBuf(2);
							when x"4" => ipb_rdata <= x"0000" & "00000" & UNA_MonBuf(4);
							when x"5" => ipb_rdata <= x"0" & NXT_TCPBuf(1) & x"0" & NXT_TCPBuf(0);
							when x"6" => ipb_rdata <= x"00000" & NXT_TCPBuf(2);
							when x"7" => ipb_rdata <= "00000" & UNA_TCPBuf(1) & "00000" & UNA_TCPBuf(0);
							when x"8" => ipb_rdata <= "00000" & UNA_TCPBuf(3) & "00000" & UNA_TCPBuf(2);
							when x"9" => ipb_rdata <= "00000" & Written_MonBuf(1) & "00000" & Written_MonBuf(0);
							when x"a" => ipb_rdata <= "00000" & Written_MonBuf(3) & "00000" & Written_MonBuf(2);
							when x"b" => ipb_rdata <= '0' & ReadBusy & '0' & SFP_RXGOOD & x"00" & "00" & sfp_pd(2) & sfp_pd(1) & sfp_pd(0) & '0' & SFP_TX_FSM_RESET_DONE & '0' & SFP_RX_FSM_RESET_DONE;
							when x"c" => ipb_rdata <= x"000" & SFP2TCPIP(2) & SFP2TCPIP(1) & SFP2TCPIP(0) & TCPIP2SFP_sel(2) & TCPIP2SFP_sel(1) & TCPIP2SFP_sel(0) & '0' & EnTCPIP & '0' & LINK_down;
							when x"d" => ipb_rdata <= cmsCRC_errCntr(0);
							when x"e" => ipb_rdata <= cmsCRC_errCntr(1);
							when others => ipb_rdata <= cmsCRC_errCntr(2);
						end case;
					when "01" => 
						case ipb_addr(3 downto 0) is
							when x"0" => ipb_rdata <= '0' & RETX_ddr_rrqst & '0' & RETX_ddr_wrqst & '0' & EVENTdata_avl & '0' & AddrBuf_full & '0' & evt_FIFO_full & '0' & wport_FIFO_full & '0' & wport_rdy & '0' & evt_data_rdy;
							when x"1" => ipb_rdata <= TCP_rrqst_i & TCP_rFIFO_ra & "000" & TCP_rFIFO_wa & "00" & rrqstMask  & '0' & RETXdataAck & '0' & RETXdataRqst;
							when x"2" => ipb_rdata <= "00000" & MonBuf_wa & "00000" & MonBuf_ra;
							when x"3" => ipb_rdata <= EventData_reCntr(0);
							when x"4" => ipb_rdata <= EventData_reCntr(1);
							when x"5" => ipb_rdata <= EventData_reCntr(2);
							when x"6" => ipb_rdata <= EventBufAddr_weCntr(0);
							when x"7" => ipb_rdata <= EventBufAddr_weCntr(1);
							when x"8" => ipb_rdata <= EventBufAddr_weCntr(2);
							when x"9" => ipb_rdata <= EventData_weCntr(0);
							when x"a" => ipb_rdata <= EventData_weCntr(1);
							when x"b" => ipb_rdata <= EventData_weCntr(2);
							when x"c" => ipb_rdata <= SFP_IPADDR(0);
							when x"d" => ipb_rdata <= SFP_IPADDR(1);
							when x"e" => ipb_rdata <= SFP_IPADDR(2);
--							when others => ipb_rdata <= CWND_max;
							when others => ipb_rdata <= (others => '0');
						end case;
					when "10" => 
						case ipb_addr(3 downto 0) is
							when x"0" => ipb_rdata <= x"000000" & "000" & en_stop;
							when x"1" => ipb_rdata <= x"000000" & SFP_rate_limit(0);
							when x"2" => ipb_rdata <= x"000000" & SFP_rate_limit(1);
							when x"3" => ipb_rdata <= x"000000" & SFP_rate_limit(2);
							when x"4" => ipb_rdata <= SFP_evt_cntr(0);
							when x"5" => ipb_rdata <= SFP_evt_cntr(1);
							when x"6" => ipb_rdata <= SFP_evt_cntr(2);
							when x"8" => ipb_rdata <= SFP_word_cntr(0);
							when x"9" => ipb_rdata <= SFP_word_cntr(1);
							when x"a" => ipb_rdata <= SFP_word_cntr(2);
							when x"c" => ipb_rdata <= SFP_blk_cntr(0);
							when x"d" => ipb_rdata <= SFP_blk_cntr(1);
							when x"e" => ipb_rdata <= SFP_blk_cntr(2);
							when x"f" => ipb_rdata <= x"0000" & RTOmin;
							when others => ipb_rdata <= (others => '0');
						end case;
					when others => ipb_rdata <= (others => '0');
				end case;
		end case;
	else
		ipb_rdata <= (others => '0');
	end if;
end process;
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		if(ipb_addr(27) = '0' and ipb_addr(15 downto 2) = "00011100000111" and ipb_strobe = '1' and ipb_write = '1')then
			if(ipb_addr(1 downto 0) = "11")then
--				CWND_max <= x"0" & ipb_wdata(27 downto 0);
			else
				SFP_IPADDR(conv_integer(ipb_addr(1 downto 0))) <= ipb_wdata;
			end if;
		end if;
	end if;
end process;
end Behavioral;

