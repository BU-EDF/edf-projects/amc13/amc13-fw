----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:32:01 07/14/2013 
-- Design Name: 
-- Module Name:    IP_test - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use IEEE.numeric_std.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
-- This module uses NewReno modification to TCP's Fast Recovery Algorithm [RFC2582] 
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity TCPIP is
		generic (simulation : boolean := false; en_KEEPALIVE : std_logic := '0');
    Port (
					 reset : IN std_logic;
					 rstCntr : IN std_logic;
					 sysclk : IN std_logic;
					 clk2x : IN std_logic; -- 2xSFP_REFCLK 312.5MHz norminal
					 clk : IN std_logic;
					 strobe_us : IN std_logic;
					 strobe_ms : IN std_logic;
					 en_LINK : IN std_logic;
					 LINK_down : OUT std_logic;
           MY_PORT : in  STD_LOGIC_VECTOR (15 downto 0); -- my TCP port number
           MY_IP : in  STD_LOGIC_VECTOR (31 downto 0); -- my IP address
           MY_ETH : in  STD_LOGIC_VECTOR (47 downto 0); -- my Ethernet address
           TSclock : in  STD_LOGIC_VECTOR (31 downto 0);
           RTOmin : in  STD_LOGIC_VECTOR (15 downto 0);
--           CWND_max : in  STD_LOGIC_VECTOR (31 downto 0);
           rate_limit : in  STD_LOGIC_VECTOR (7 downto 0);
           EVENTdata : in  STD_LOGIC_VECTOR (66 downto 0); -- bit 67 BoE, bit 66 EoE, bit 65 BoB and bit 64 EoB
					 EventBufAddr : IN std_logic_vector(13 downto 0);
					 EventBufAddr_we : IN std_logic;
					 AddrBuf_full : OUT std_logic;
					 EVENTdata_avl : IN std_logic; -- source must have either EOE or at least 8kbytes of data in its buffer
					 EVENTdata_re : OUT std_logic; -- could be paused if dataFIFO_full is asserted
           DDR2TCPdata : in  STD_LOGIC_VECTOR (32 downto 0); -- Bit 33 marks the end of ReTxData
					 ReTxData_we : in std_logic_VECTOR (1 downto 0);
					 ReTxData_chksum : in std_logic_VECTOR (15 downto 0);
           ReTx_ddr_out : out  STD_LOGIC_VECTOR (31 downto 0);
					 re_RETX_ddr_wq : IN std_logic;
					 ReTx_ddr_wrqst : OUT std_logic;
           ReTx_ddr_LEN_max : in  STD_LOGIC_VECTOR (4 downto 0); -- in unit of 64 bytess
           ReTx_ddr_LEN : out  STD_LOGIC_VECTOR (4 downto 0); -- in unit of 64 bytess
					 ReTx_ddr_data_we : IN std_logic;
					 ReTx_ddr_rrqst : OUT std_logic;
					 ReTxDataACK : IN std_logic;
					 ReTxDataRqst : OUT std_logic;
           ReTxDataAddr : out  STD_LOGIC_VECTOR (25 downto 0); -- in unit of 64bit words
           ReTxDataLEN : out  STD_LOGIC_VECTOR (12 downto 0); -- in unit of 64bit words
           KiloByte_toggle : in  STD_LOGIC;
           EoB_toggle : in  STD_LOGIC;
					 TCP_wcount : in  STD_LOGIC;
           UNA_MonBuf : out  STD_LOGIC_VECTOR (10 downto 0);
           UNA_TCPBuf : out  STD_LOGIC_VECTOR (10 downto 0);
           PhyEmacRxC : in  STD_LOGIC_VECTOR (3 downto 0);
           PhyEmacRxD : in  STD_LOGIC_VECTOR (31 downto 0);
           EmacPhyTxC : out  STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
           EmacPhyTxD : out  STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
					 ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0);
           cs_out : out  STD_LOGIC_VECTOR (511 downto 0)
					 );
end TCPIP;

architecture Behavioral of TCPIP is
function A_GT_B (A, B : std_logic_vector(31 downto 0)) return boolean is
variable tmp : std_logic_vector(31 downto 0);
begin
	tmp := A - B;
	if(tmp(31) = '0' and or_reduce(tmp(30 downto 0)) = '1')then
		return true;
	else
		return false;
	end if;
end A_GT_B;
function A_GE_B (A, B : std_logic_vector(31 downto 0)) return boolean is
variable tmp : std_logic_vector(31 downto 0);
begin
	tmp := A - B;
	if(tmp(31) = '0')then
		return true;
	else
		return false;
	end if;
end A_GE_B;
COMPONENT XGbEMAC
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		enTxJumboFrame : IN std_logic;
		enRxJumboFrame : IN std_logic;
		PhyEmacRxc : IN std_logic_vector(3 downto 0);
		PhyEmacRxd : IN std_logic_vector(31 downto 0);
		ClientEmacTxd : IN std_logic_vector(31 downto 0);
		ClientEmacTxdVld : IN std_logic_vector(3 downto 0);
		ClientEmacTxUnderrun : IN std_logic;          
		EmacPhyTxc : OUT std_logic_vector(3 downto 0);
		EmacPhyTxd : OUT std_logic_vector(31 downto 0);
		EmacClientTack : OUT std_logic;
		EmacClientRxd : OUT std_logic_vector(31 downto 0);
		EmacClientRxdWe : OUT std_logic_vector(3 downto 0);
		EmacClientRxGoodFrame : OUT std_logic;
		EmacClientRxbadFrame : OUT std_logic
		);
END COMPONENT;
COMPONENT EMAC_Rx_if
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		rstCntr : IN std_logic;
		no_TCP_DATA : IN std_logic;
		EMAC_RxD : IN std_logic_vector(31 downto 0);
		EMAC_RxDVLD : IN std_logic_vector(3 downto 0);
		EMAC_RxGoodFrame : IN std_logic;
		EMAC_RxBadFrame : IN std_logic;
		MY_IP : IN std_logic_vector(31 downto 0);
		MY_ETH : IN std_logic_vector(47 downto 0);
		en_Dout : IN std_logic;          
		Dout_avl : OUT std_logic;
    Dout_type : out  STD_LOGIC_VECTOR (1 downto 0):= "00";
		Dout : OUT std_logic_vector(35 downto 0);
		last_word : OUT std_logic;
		Dout_valid : OUT std_logic;
		ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
		ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0);
		csa : out  STD_LOGIC_VECTOR (135 downto 0);
		csb : out  STD_LOGIC_VECTOR (135 downto 0)
		);
END COMPONENT;
COMPONENT TCP_OPTION
	PORT(
		clk : IN std_logic;
		CLOSED : IN std_logic;
		update_TSrecent : IN std_logic;
		TSclock : IN std_logic_vector(31 downto 0);
		RCV_SYN : IN std_logic;
		SND_SYN : IN std_logic;
		LISTEN : IN std_logic;
    Save_ReTx : in  STD_LOGIC;
    Save_ReTxTime : in  STD_LOGIC;
		OPTION_begin : IN std_logic;
		OPTION_end : IN std_logic;
		di : IN std_logic_vector(31 downto 0);
		rd_dout : IN std_logic;          
		scale : OUT std_logic_vector(3 downto 0);
		DATA_OFFSET : OUT std_logic_vector(3 downto 0);
		MSS : OUT std_logic_vector(15 downto 0);
		RTT : OUT std_logic_vector(27 downto 0);
		OPTION_rdy : OUT std_logic;
		TS_OPTION : OUT std_logic;
		OldData : out  STD_LOGIC;
		dout : OUT std_logic_vector(31 downto 0);
		debug : OUT std_logic_vector(135 downto 0)
		);
END COMPONENT;
COMPONENT checksum
	PORT(
		c : IN std_logic;
		r : IN std_logic;
		r_value : IN std_logic_vector(15 downto 0);          
		ce : IN std_logic;
		d : IN std_logic_vector(16 downto 0);          
		bad_chksum : OUT std_logic;
		s : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;
COMPONENT TCPdata_chksum
	PORT(
		c : IN std_logic;
		r : IN std_logic;
		ce : IN std_logic;
		d : IN std_logic_vector(63 downto 0);
		DATA_OFFSET : IN std_logic_vector(3 downto 0);
		length_in : IN std_logic_vector(12 downto 0);
		en_out : IN std_logic;          
		s : OUT std_logic_vector(15 downto 0);
		chksum : OUT std_logic_vector(15 downto 0);
		DATA_SIZE : OUT std_logic_vector(31 downto 0);
		DATA_LEN : OUT std_logic_vector(15 downto 0)
		);
END COMPONENT;
COMPONENT FIFO65x12k
	generic(ALMOSTFULL_OFFSET : bit_vector(15 downto 0) := x"0008");
	PORT(
		clk : IN std_logic;
		fifo_rst : IN std_logic;
		fifo_en : IN std_logic;
		di : IN std_logic_vector(64 downto 0);
		we : IN std_logic;
		re : IN std_logic;          
		do : OUT std_logic_vector(64 downto 0);
		full : OUT std_logic;
		empty : OUT std_logic
		);
END COMPONENT;
COMPONENT SDP32x18
	generic (INIT : bitarray9x64);
	PORT(
		clk : IN std_logic;
		we : IN std_logic;
		DI : IN std_logic_vector(17 downto 0);
		WA : IN std_logic_vector(4 downto 0);
		RA : IN std_logic_vector(4 downto 0);          
		DO : OUT std_logic_vector(17 downto 0)
		);
END COMPONENT;
COMPONENT RTO_CALC
	PORT(
		clk : IN std_logic;
		RTT : IN std_logic_vector(15 downto 0);
		RTOmin : IN std_logic_vector(15 downto 0);
		LISTEN : IN std_logic;
		RTO_backoff : IN std_logic;
		sample_RTT : IN std_logic;          
		RTO : OUT std_logic_vector(15 downto 0);
		debug : OUT std_logic_vector(135 downto 0)
		);
END COMPONENT;
COMPONENT TCP_CC
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		SYNRCVD : IN std_logic;
		ReTx_TO : IN std_logic;
		Save_ReTx : IN std_logic;
		FastReTxStart : in  STD_LOGIC;
		FR : in  STD_LOGIC;
		NewDataACK : IN std_logic;
		PartialACK : IN std_logic;
		DupACK : IN std_logic;
		MSS : IN std_logic_vector(15 downto 0);
--		CWND_max : IN std_logic_vector(31 downto 0);
		SEG_WND : IN std_logic_vector(31 downto 0);
		SND_UNA : IN std_logic_vector(31 downto 0);
		SND_NXT : IN std_logic_vector(31 downto 0);          
		CWND : OUT std_logic_vector(31 downto 0);
		SND_WND_UL : OUT std_logic_vector(31 downto 0);
		debug : OUT std_logic_vector(271 downto 0)
		);
END COMPONENT;
COMPONENT RAM32x6D
	PORT(
		wclk : IN std_logic;
		rclk : IN std_logic;
		di : IN std_logic_vector(5 downto 0);
		we : IN std_logic;
		wa : IN std_logic_vector(4 downto 0);
		ra : IN std_logic_vector(4 downto 0);
		ceReg : IN std_logic;          
		do : OUT std_logic_vector(5 downto 0)
		);
END COMPONENT;
COMPONENT FIFO65x8k
	PORT(
		clk : IN std_logic;
		fifo_rst : IN std_logic;
		fifo_en : IN std_logic;
		di : IN std_logic_vector(32 downto 0);
		we : IN std_logic_vector(1 downto 0);
		re : IN std_logic;          
		do : OUT std_logic_vector(64 downto 0);
		FIFO_WrErr : OUT std_logic;
		empty : OUT std_logic
		);
END COMPONENT;
constant zero : std_logic_vector(31 downto 0) := (others => '0');
constant N : integer := 8;
constant bit_FIN : integer := 0;
constant bit_SYN : integer := 1;
constant bit_RST : integer := 2;
constant bit_PSH : integer := 3;
constant bit_ACK : integer := 4;
constant INIT_buf_h : bitarray9x64 := ((others => '0'),(others => '0'),(others => '0'),(others => '0'),(others => '0'),x"0000008000000080",
																			 (others => '0'),x"0000040000000400",(others => '0'));
constant INIT_buf_l : bitarray9x64 := (x"0000080000000800",x"0000040000000400",(others => '0'),(others => '0'),x"0000004000000040",
																			 x"0000004000000040",(others => '0'),x"0000044000000440",(others => '0'));
signal EmacClientTack : std_logic := '0';
signal EmacClientRxGoodFrame : std_logic := '0';
signal EmacClientRxbadFrame : std_logic := '0';
signal EmacClientRxd : std_logic_vector(31 downto 0) := (others => '0');
signal EmacClientRxdWe : std_logic_vector(3 downto 0) := (others => '0');
signal ClientEmacTxd : std_logic_vector(31 downto 0) := (others => '0');
signal ClientEmacTxdVld : std_logic_vector(3 downto 0) := (others => '0');
signal CLIENTEMACTXUNDERRUN : std_logic := '0';
signal en_RxDout : std_logic := '0';
signal RxDout_avl : std_logic := '0';
signal RxDout_type : std_logic_vector(1 downto 0) := (others => '0');
signal RxDout_lastword : std_logic := '0';
signal RxDout_valid : std_logic := '0';
signal RxDout_valid_dl : std_logic_vector(1 downto 0) := (others => '0');
signal TCPheader_we : std_logic := '0';
signal RxDoutMSB : std_logic_vector(2 downto 0) := (others => '0');
signal RxDout : std_logic_vector(35 downto 0) := (others => '0');
signal buf_we : std_logic_vector(1 downto 0) := (others => '0');
signal buf_wa : std_logic_vector(4 downto 0) := (others => '0');
signal buf_ra : std_logic_vector(4 downto 0) := (others => '0');
signal buf_di : std_logic_vector(35 downto 0) := (others => '0');
signal buf_DO : std_logic_vector(35 downto 0) := (others => '0');
signal eof : std_logic := '0';
signal IS_ARP_ICMP : std_logic := '0';
signal MUX_TCPHDR : std_logic_vector(31 downto 0) := (others => '0');
signal headerFIFO_DI : std_logic_vector(35 downto 0) := (others => '0');
signal headerFIFO_DO : std_logic_vector(35 downto 0) := (others => '0');
signal headerFIFO_eof : std_logic := '0';
signal headerFIFO_AlmostFull : std_logic := '0';
signal headerFIFO_full : std_logic := '0';
signal headerFIFO_EMPTY : std_logic := '0';
signal headerFIFO_RDEN : std_logic := '0';
signal rd_headerFIFO : std_logic := '0';
signal headerFIFO_WREN : std_logic := '0';
signal headerFIFO_RDCOUNT : std_logic_vector(8 downto 0) := (others => '0');
signal headerFIFO_WRCOUNT : std_logic_vector(8 downto 0) := (others => '0');
signal dataFIFO_RDCOUNT : std_logic_vector(8 downto 0) := (others => '0');
signal dataFIFO_WRCOUNT : std_logic_vector(8 downto 0) := (others => '0');
signal dataFIFO_DI : std_logic_vector(64 downto 0) := (others => '0');
signal dataFIFO_DO : std_logic_vector(64 downto 0) := (others => '0');
signal TCPdata : std_logic_vector(64 downto 0) := (others => '0');
signal dataFIFO_full : std_logic := '0';
signal dataFIFO_eof : std_logic := '0';
signal dataFIFO_empty : std_logic := '0';
signal ReTx_FIFO_empty : std_logic := '0';
signal dataFIFO_RDEN : std_logic := '0';
signal AttachData : std_logic := '0';
signal AttachData_dl : std_logic := '0';
signal dataFIFO_WREN : std_logic := '0';
signal FIFO_rst : std_logic := '0';
signal reset_dl : std_logic := '0';
signal FIFO_en : std_logic := '0';
signal IP_ID : std_logic_vector(15 downto 0) := (others => '0');
signal IP_ID2Send : std_logic_vector(15 downto 0) := (others => '0');
signal ReTxIP_ID : std_logic_vector(15 downto 0) := (others => '0');
signal TCPdata_q : std_logic_vector(15 downto 0) := (others => '0');
signal ClientEmacTxd_sel : std_logic_vector(1 downto 0) := (others => '0');
type state is (CLOSED, LISTEN, SYNRCVD, ESTAB, LASTACK);
signal TCPstate : state := CLOSED;
signal TCPstates : std_logic_vector(2 downto 0) := (others => '0');
signal LISTENing : std_logic := '0';
signal IS_CLOSED : std_logic := '0';
signal IS_CONNECTED : std_logic := '0';
signal en_LINK_SyncRegs : std_logic_vector(2 downto 0) := (others => '0');
type stateB is (IDLE, SND_ARP_ICMP, RCV_IPHDR, RCV_TCPHDR, RCV_PROCESS, SND, WAIT4SND2END, RELEASE, RETX);
signal CTRLstate : stateB := IDLE;
signal CTRLstates : std_logic_vector(3 downto 0) := (others => '0');
signal NOT_MY_PORT : std_logic := '0';
signal CONN_MATCH : std_logic := '0';
signal SEG_LEN_IS_0 : std_logic := '0';
signal SEG_SEQ_OR : std_logic := '0';
signal SEG_SEQ_PLUS_LEN_OR : std_logic := '0';
signal SND_SPACE_OK : std_logic := '0';
signal SND_SPACE_CC_OK : std_logic := '0';
signal SEG_ACK_OK : std_logic := '0';
signal SEG_ACK_GT_SND_NXT : std_logic := '0';
signal rdy2send : std_logic := '0';
signal SND_SYNorFIN : std_logic := '0';
signal SEG_SYNorFIN : std_logic := '0';
signal SND_UNACK : std_logic := '0';
signal update_SND_UNA : std_logic := '0';
signal update_SND_WND : std_logic := '0';
signal update_RCV_NXT : std_logic := '0';
signal update_TSrecent : std_logic := '0';
signal KEEPALIVEcntr : std_logic_vector(15 downto 0) := (others => '0');
constant waittime : integer := 10;-- 10 about 1 second and 16 about one minute
signal TIMEWAITcntr : std_logic_vector(waittime downto 0) := (others => '0');
signal CTRLcntr : std_logic_vector(4 downto 0) := (others => '0');
signal DestPORT : std_logic_vector(15 downto 0) := (others => '0');
signal IPHDR_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal IPHDR_END : std_logic_vector(4 downto 0) := (others => '0');
signal SND_UNA : std_logic_vector(31 downto 0) := (others => '0');
signal SND_NXT : std_logic_vector(31 downto 0) := (others => '0');
signal SND_WND : std_logic_vector(31 downto 0) := (others => '0');
signal SND_WND_UL : std_logic_vector(31 downto 0) := (others => '0');
signal SND_WND_UL_CC : std_logic_vector(31 downto 0) := (others => '0');
signal SND_SEQ_LAST : std_logic_vector(31 downto 0) := (others => '0');
signal SND_WND_SCALE : std_logic_vector(3 downto 0) := (others => '0');
signal SND_WL1 : std_logic_vector(31 downto 0) := (others => '0');
signal SND_WL2 : std_logic_vector(31 downto 0) := (others => '0');
signal ISS : std_logic_vector(31 downto 0) := (others => '0');
signal recover : std_logic_vector(31 downto 0) := (others => '0');
signal TO_recover : std_logic_vector(31 downto 0) := (others => '0');
signal RCV_NXT : std_logic_vector(31 downto 0) := (others => '0');
constant RCV_WND : std_logic_vector(31 downto 0) := x"00000001";
signal RCV_NXT_PLUS_WND : std_logic_vector(31 downto 0) := (others => '0');
signal IRS : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_SEQ : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_SEQ_PLUS_ONE : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_SEQ_PLUS_LEN : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_ACK : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_TCP_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal SEG_TCPHDR_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal SEG_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal tmp_SEG_WND : std_logic_vector(15 downto 0) := (others => '0');
signal input_SEG_WND0 : std_logic_vector(15 downto 0) := (others => '0');
signal input_SEG_WND1 : std_logic_vector(15 downto 0) := (others => '0');
signal input_SEG_WND2 : std_logic_vector(15 downto 0) := (others => '0');
signal SEG_WNDp : std_logic_vector(18 downto 0) := (others => '0');
signal SEG_WND : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_CTRL : std_logic_vector(4 downto 0) := (others => '0');
signal DATA_OFFSET : std_logic_vector(3 downto 0) := (others => '0');
signal TCPHDR_cntr : std_logic_vector(3 downto 0) := (others => '0');
signal SND_CTRL : std_logic_vector(5 downto 0) := (others => '0');
signal SND_SEQ : std_logic_vector(31 downto 0) := (others => '0');
signal SND_ACK : std_logic_vector(31 downto 0) := (others => '0');
signal DATA_LEN : std_logic_vector(31 downto 0) := (others => '0');
signal HDR_LEN : std_logic_vector(13 downto 0) := (others => '0');
signal TOTAL_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal IPHDR_chksum : std_logic_vector(15 downto 0) := (others => '0');
signal TCP_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal TCP_chksum : std_logic_vector(15 downto 0) := (others => '0');
signal DATA_chksum : std_logic_vector(15 downto 0) := (others => '0');
signal SavedChksum : std_logic_vector(15 downto 0) := (others => '0');
signal ReTxData_avl_dl : std_logic_vector(2 downto 0) := (others => '0');
signal bad_ReTx_chksum : std_logic := '0';
signal CWND_GE_SND_WND : std_logic := '0';
signal OPTION_begin : std_logic := '0';
signal OPTION_end : std_logic := '0';
signal OPTION_rdy : std_logic := '0';
signal rd_OPTION : std_logic := '0';
signal TS_OPTION : std_logic := '0';
signal OPTION_do : std_logic_vector(35 downto 0) := (others => '0');
signal MSS : std_logic_vector(31 downto 0) := (others => '0');
signal MSS_cutoff : std_logic_vector(12 downto 0) := (others => '0');
signal sample_cntr : std_logic_vector(7 downto 0) := (others => '0');
signal sum_RTT : std_logic_vector(35 downto 0) := (others => '0');
signal avg_RTT : std_logic_vector(27 downto 0) := (others => '0');
signal RTT_cntr : std_logic_vector(27 downto 0) := (others => '0');
signal TS_RTT : std_logic_vector(27 downto 0) := (others => '0');
signal STD_RTT : std_logic_vector(15 downto 0) := (others => '0');
signal sel_SND_DATA : std_logic_vector(1 downto 0) := (others => '0');
signal SND_DATA : std_logic_vector(33 downto 0) := (others => '0');
signal SND_DATA_dl : std_logic_vector(33 downto 0) := (others => '0');
signal OPTION_data : std_logic_vector(31 downto 0) := (others => '0');
signal TCPHDR_end : std_logic_vector(4 downto 0) := (others => '0');
signal SND_DATA_r : std_logic_vector(15 downto 0) := (others => '0');
signal chksum_in : std_logic_vector(16 downto 0) := (others => '0');
signal RTT_cycle : std_logic := '0';
signal EnNewData : std_logic := '0';
signal ReTxEntry :std_logic_vector(11 downto 0) := (others => '0');
signal SEGinFLIGHT :std_logic_vector(11 downto 0) := (others => '0');
signal NewDataLimit :std_logic_vector(10 downto 0) := (others => '0');
signal NewDataCntr :std_logic_vector(10 downto 0) := (others => '0');
signal rst_chksum : std_logic := '0';
signal ce_IPHDR_chksum : std_logic := '0';
signal ce_TCP_chksum : std_logic := '0';
signal is_IPHDR_chksum : std_logic := '0';
signal sel_IPHDR_chksum : std_logic := '0';
signal is_TCP_chksum : std_logic := '0';
signal sel_TCP_chksum : std_logic := '0';
signal SND_DATA_vld : std_logic := '0';
signal SND_DATA_end : std_logic := '0';
signal SND_DATA_end_dl : std_logic := '0';
signal rst_DATA_chksum : std_logic := '0';
type stateC is (IDLE, ReadEventData, Wait4Send);
signal DATAstate : stateC := IDLE;
signal DATAstates : std_logic_vector(1 downto 0) := (others => '0');
signal ReTxData_avl : std_logic := '0';
signal chk_ReTxQueue : std_logic_vector(1 downto 0) := (others => '0');
signal ReTxData : std_logic_vector(64 downto 0) := (others => '0');
signal ReTx_FIFO_h_WRCOUNT : std_logic_vector(9 downto 0) := (others => '0');
signal ReTx_FIFO_h_RDCOUNT : std_logic_vector(9 downto 0) := (others => '0');
signal ReTx_FIFO_l_WRCOUNT : std_logic_vector(9 downto 0) := (others => '0');
signal ReTx_FIFO_l_RDCOUNT : std_logic_vector(9 downto 0) := (others => '0');
signal ReTxDataAddr_i : std_logic_vector(25 downto 0) := (others => '0');
signal ReTxDataLastAddr : std_logic_vector(15 downto 0) := (others => '0');
signal SendReTxData : std_logic := '0';
signal FastReTxData : std_logic := '0';
signal TO_ReTxData : std_logic := '0';
signal sel_ReTx_DATA : std_logic := '0';
signal AddReTxEntry : std_logic := '0';
signal is_ReTx : std_logic := '0';
signal ReTxData_re : std_logic := '0';
signal WaitReTxData : std_logic := '0';
signal EoB : std_logic := '0';
signal push : std_logic := '0';
signal data2send : std_logic := '0';
signal EVENTdata_re_i : std_logic := '0';
signal TCP_DATA_LEN : std_logic_vector(15 downto 0) := (others => '0');
signal TCP_DATA_chksum : std_logic_vector(15 downto 0) := (others => '0');
signal was_IDLE : std_logic := '0';
signal ReTx_cnt : std_logic_vector(2 downto 0) := (others => '0');
signal DATA_SIZE : std_logic_vector(31 downto 0) := (others => '0');
signal ReTxDataLEN_i : std_logic_vector(12 downto 0) := (others => '0');
signal data_wc : std_logic_vector(12 downto 0) := (others => '0');
signal saved_data_wc : std_logic_vector(10 downto 0) := (others => '0');
signal ReTx_ddr_cnt : std_logic_vector(12 downto 0) := (others => '0');
signal ReTx_ddr_empty : std_logic :=  '1';
signal ReTx_ddr_full : std_logic :=  '0';
signal ReTxQueue_WEA : std_logic :=  '0';
signal ReTxQueue_WEB : std_logic_vector(3 downto 0) := (others => '0');
signal ReTxQueue_DIA : std_logic_vector(31 downto 0) := (others => '0');
signal ReTxQueue_DIB : std_logic_vector(31 downto 0) := (others => '0');
signal ReTxQueue_DOBp : std_logic_vector(31 downto 0) := (others => '0');
signal ReTxQueue_DOB : std_logic_vector(31 downto 0) := (others => '0');
signal ReTxQueue_ADDRB : std_logic_vector(9 downto 0) := (others => '0');
signal ReTxQueue_wp : std_logic_vector(9 downto 0) := (others => '0');
signal ReTxQueue_rp : std_logic_vector(7 downto 0) := (others => '0');
signal ReleasePending : std_logic := '1';
signal ReTxentry_avl : std_logic := '1';
signal ReTxQueue_entry : std_logic_vector(8 downto 0) := (others => '0');
signal ReTx_ddr_wq_we : std_logic := '0';
signal RETX_ddr_wq_re : std_logic := '0';
signal ReTx_ddr_wq_ceReg : std_logic := '0';
signal ReTx_ddr_wrqst_i : std_logic := '0';
signal ReTx_ddr_rrqst_i : std_logic := '0';
signal ReTx_ddr_rrqst_q : std_logic := '0';
signal ReTx_ddr_out_vld : std_logic := '0';
signal ReTx_ddr_wq_full : std_logic := '0';
signal ReTx_ddr_LEN_i : std_logic_vector(4 downto 0) := (others => '0');
signal ReTx_ddr_wq_wa : std_logic_vector(4 downto 0) := (others => '0');
signal ReTx_ddr_wq_ra : std_logic_vector(4 downto 0) := (others => '0');
signal ReTx_ddr_wq_wc : std_logic_vector(5 downto 0) := (others => '0');
signal ReTx_ddr_wq_in : std_logic_vector(35 downto 0) := (others => '0');
signal ReTx_ddr_out_i : std_logic_vector(35 downto 0) := (others => '0');
signal sel_ddr : std_logic := '0';
signal ce_ReTxQueue_rp : std_logic := '0';
signal update_UNA_Buf : std_logic := '0';
signal ec_ReTx_ADDR : std_logic := '0';
signal ReTx_CTRL : std_logic_vector(4 downto 0) := (others => '0');
signal ReTx_SEQ : std_logic_vector(31 downto 0) := (others => '0');
signal DupACK_cnt : std_logic_vector(1 downto 0) := (others => '0');
signal RELEASE_type : std_logic_vector(1 downto 0) := (others => '0');
signal FastReTx : std_logic := '0';
signal FastRecovery : std_logic := '0';
signal TO_Recovery : std_logic := '0';
signal FastReTxStart : std_logic := '0';
signal FastReTxDone : std_logic := '0';
signal TO_ReTxDone : std_logic := '0';
signal save_ReTx : std_logic := '0';
signal save_ReTxTime : std_logic := '0';
signal PartialACK : std_logic_vector(1 downto 0) := (others => '0');
signal wasSYNRCVD : std_logic := '0';
signal NewDataACK : std_logic := '0';
signal DupACK : std_logic := '0';
signal IsDupACK : std_logic := '0';
signal OldData : std_logic := '0';
signal sample_RTT : std_logic_vector(2 downto 0) := (others => '0');
signal time_sample : std_logic := '0';
signal set_ReTx_TO : std_logic := '0';
signal set_ReTx_TO_dl : std_logic_vector(1 downto 0) := (others => '0');
signal TSclock11_r : std_logic := '0';
signal ReTx_TO : std_logic := '0';
signal ReTxNXT : std_logic := '0';
signal RTTvld : std_logic_vector(2 downto 0) := (others => '0');
signal RTO : std_logic_vector(15 downto 0) := (others => '0');
signal RTO_timer : std_logic_vector(15 downto 0) := (others => '0');
signal RTT : std_logic_vector(15 downto 0) := (others => '0');
signal CWND : std_logic_vector(31 downto 0) := (others => '0');
signal CWND_LTA : std_logic_vector(31 downto 0) := (others => '0');
signal debug_opt : std_logic_vector(135 downto 0) := (others => '0');
signal debug_RTO : std_logic_vector(135 downto 0) := (others => '0');
signal debug_CC : std_logic_vector(271 downto 0) := (others => '0');
signal ARP_rcvd_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal ICMP_rcvd_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal inc_ARP_sent : std_logic := '0';
signal inc_ICMP_sent : std_logic := '0';
signal ARP_sent_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal ICMP_sent_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal ReTxData_wc : std_logic_vector(31 downto 0) := (others => '0');
signal en_RxDout_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal Tack_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal ReTx_DataSeg_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal ReTx_DataErr_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal FastReTx_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal ReTxDataACK_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal Save_ReTx_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal ReTxNXT_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal DataSeg_cntr : std_logic_vector(31 downto 0) := (others => '0');
signal bad_chksum_pair : std_logic_vector(31 downto 0) := (others => '0');
signal time_cntr : std_logic_vector(16 downto 0) := (others => '0');
signal rate_cntr : std_logic_vector(24 downto 0) := (others => '0');
signal rate : std_logic_vector(24 downto 0) := (others => '0');
signal deadtime_cntr : std_logic_vector(25 downto 0) := (others => '0');
signal deadtime : std_logic_vector(15 downto 0) := (others => '0');
signal status : array32x32 := (others => (others => '0'));
signal statusb : array32x32 := (others => (others => '0'));
signal ReTx_FIFO_WrErr : std_logic := '0';
signal ReTx_FIFO_WrError : std_logic := '0';
signal ReTx_FIFO_RdError : std_logic := '0';
signal ReTxDataRqst_i : std_logic_vector(2 downto 0) := (others => '0');
signal EMAC_Rx_rdata : std_logic_vector(31 downto 0) := (others => '0');
signal SEG_ADDR : std_logic_vector(25 downto 0) := (others => '0');
signal EventBufAddr_wa : std_logic_vector(4 downto 0) := (others => '0');
signal EventBufAddr_ra : std_logic_vector(4 downto 0) := (others => '0');
signal EventBufAddr_dop : std_logic_vector(13 downto 12) := (others => '0');
signal EventBufAddr_do : std_logic_vector(13 downto 0) := (others => '0');
signal EventBufAddr_ra1SyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal EventBufAddr_ra0SyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal DAQ_header : std_logic := '0';
signal bad_ReTx_LEN : std_logic := '0';
signal ReTxDataCntr : std_logic_vector(13 downto 0) := (others => '0');
signal bad_LEN_pair : std_logic_vector(31 downto 0) := (others => '0');
signal bad_ReTx_LENCntr : std_logic_vector(15 downto 0) := (others => '0');
signal bad_ReTx_chksumCntr : std_logic_vector(15 downto 0) := (others => '0');
signal DupAckCntr : std_logic_vector(31 downto 0) := (others => '0');
signal NewAckCntr : std_logic_vector(31 downto 0) := (others => '0');
signal UNA_MonBuf_i : std_logic_vector(10 downto 0) := (others => '0');
signal KiloByte_toggleSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal EoB_toggleSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal Written_MonBuf : std_logic_vector(10 downto 0) := (others => '0');
signal ADDR_offset : std_logic_vector(3 downto 0) := (others => '0');
signal Written_MonBlock : std_logic_vector(8 downto 0) := (others => '0');
signal ddr_wptr : std_logic_vector(18 downto 0) := (others => '0');
signal ddr_rptr : std_logic_vector(18 downto 0) := (others => '0');
signal entry_in_ddr : std_logic_vector(18 downto 0) := (others => '0');
signal CLOSE_flag : std_logic_vector(3 downto 0) := (others => '0');
signal rate_pause : std_logic := '0';
signal delta_rate : std_logic_vector(7 downto 0) := (others => '0');
signal wc : std_logic_vector(21 downto 0) := (others => '0');
begin
--process(clk2x)
--begin
--	if(clk2x'event and clk2x = '1')then
--		if(FastReTxStart = '1' or FastReTxDone = '1' or (FastRecovery = '1' and (DupACK = '1' or NewDataACK = '1')))then
--			cs_out(288) <= '1';
--		else
--			cs_out(288) <= '0';
--		end if;
--		if(FastReTx = '1' or ReTx_TO = '1' or ReTxNXT = '1' or ((FastRecovery = '1' or TO_recovery = '1') and NewDataACK = '1'))then
--			cs_out(289) <= '1';
--		else
--			cs_out(289) <= '0';
--		end if;
--	end if;
--end process;
--cs_out(272) <= TO_recovery;
--cs_out(271) <= OldData;
--cs_out(269) <= option_rdy;
--cs_out(256) <= FastReTx;
--cs_out(255) <= ReTxNXT;
--cs_out(254) <= FastReTxDone;
--cs_out(253 downto 252) <= DupACK_Cnt;
--cs_out(251 downto 220) <= SND_NXT;
--cs_out(219 downto 188) <= SND_UNA;
--cs_out(187 downto 156) <= SEG_ACK;
--cs_out(251 downto 156) <= debug_opt(95 downto 0);
--cs_out(155 downto 122) <= debug_opt(129 downto 96);
--cs_out(153 downto 122) <= recover;
--cs_out(121 downto 0) <= debug_CC(121 downto 0);
LINK_down <= '0' when TCPstate = ESTAB else '1';
EVENTdata_re <= EVENTdata_re_i;
ReTx_ddr_out <= ReTx_ddr_out_i(31 downto 0);
ReTx_ddr_wrqst <= ReTx_ddr_wrqst_i;
ReTx_ddr_rrqst <= ReTx_ddr_rrqst_i;
ReTxDataLEN <= ReTxDataLEN_i;
UNA_MonBuf <= UNA_MonBuf_i;
UNA_TCPBuf <= (others => '0');
ReTxDataRqst <= ReTxDataRqst_i(2);
ReTxDataAddr <= ReTxDataAddr_i;
ReTx_ddr_LEN <= ReTx_ddr_LEN_i;
i_XGbEMAC: XGbEMAC PORT MAP(
		clk => clk2x,
		reset => reset,
		enTxJumboFrame => '1',
		enRxJumboFrame => '0',
		EmacPhyTxc => EmacPhyTxc,
		EmacPhyTxd => EmacPhyTxd,
		PhyEmacRxc => PhyEmacRxC,
		PhyEmacRxd => PhyEmacRxD,
		ClientEmacTxd => ClientEmacTxd,
		ClientEmacTxdVld => ClientEmacTxdVld,
		ClientEmacTxUnderrun => '0',
		EmacClientTack => EmacClientTack,
		EmacClientRxd => EmacClientRxd,
		EmacClientRxdWe => EmacClientRxdWe,
		EmacClientRxGoodFrame => EmacClientRxGoodFrame,
		EmacClientRxbadFrame => EmacClientRxbadFrame
	);
i_EMAC_Rx_if: EMAC_Rx_if PORT MAP(
		clk => clk2x,
		reset => reset,
		rstCntr => rstCntr,
		no_TCP_DATA => '1',
		EMAC_RxD => EmacClientRxd,
		EMAC_RxDVLD => EmacClientRxdWe,
		EMAC_RxGoodFrame => EmacClientRxGoodFrame,
		EMAC_RxBadFrame => EmacClientRxbadFrame,
		MY_IP => MY_IP,
		MY_ETH => MY_ETH,
		en_Dout => en_RxDout,
		Dout_avl => RxDout_avl,
		Dout_type => RxDout_type,
		Dout => RxDout,
		last_word => RxDout_lastword,
		Dout_valid => RxDout_valid,
		ipb_addr => ipb_addr,
		ipb_rdata => EMAC_Rx_rdata,
		csa => open,
		csb => open
	);
i_buf_H: SDP32x18 generic map (INIT => INIT_buf_h) PORT MAP(
		clk => clk2x,
		we => buf_we(1),
		DI => buf_di(35 downto 18),
		WA => buf_wa,
		RA => buf_ra,
		DO => buf_DO(35 downto 18)
	);
i_buf_L: SDP32x18 generic map (INIT => INIT_buf_l) PORT MAP(
		clk => clk2x,
		we => buf_we(0),
		DI => buf_di(17 downto 0),
		WA => buf_wa,
		RA => buf_ra,
		DO => buf_DO(17 downto 0)
	);
buf_di <= '0' & RxDout(34) & RxDout(31 downto 16) & RxDout(33 downto 32) & RxDout(15 downto 0);
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		EventBufAddr_do(13 downto 12) <= EventBufAddr_dop;
		if(TCPstate = CLOSED or TCPstate = LISTEN)then
			buf_wa(4) <= '0';
		else
			buf_wa(4) <= '1';
		end if;
		if(buf_wa(3 downto 0) = x"0")then
			IPHDR_END <= (others => '1');
		elsif(buf_wa(3 downto 0) = x"3")then
			IPHDR_END <= ('0' & RxDout(11 downto 8)) + "00011";
		end if;
		if(buf_wa(3 downto 0) = x"3")then
			IPHDR_LEN <= "0000000000" & RxDout(11 downto 8) & "00";
		end if;
		if(buf_wa(3 downto 0) = x"4")then
			SEG_TCP_LEN <= RxDout(31 downto 16) - IPHDR_LEN;
		end if;
		if(buf_wa(3 downto 0) = x"8")then
			if(RxDout(15 downto 0) = MY_PORT)then
				NOT_MY_PORT <= '0';
			else
				NOT_MY_PORT <= '1';
			end if;
		end if;
		if(buf_wa(3 downto 0) = x"9")then
			SEG_SEQ(31 downto 16) <= RxDout(15 downto 0);
		end if;
		if(buf_wa(3 downto 0) = x"a")then
			SEG_SEQ(15 downto 0) <= RxDout(31 downto 16);
			SEG_ACK(31 downto 16) <= RxDout(15 downto 0);
		end if;
		if(buf_wa(3 downto 0) = x"b")then
			SEG_ACK(15 downto 0) <= RxDout(31 downto 16);
			SEG_TCPHDR_LEN(5 downto 2) <= RxDout(15 downto 12);
			SEG_CTRL <= RxDout(4 downto 0);
			SEG_SYNorFIN <= RxDout(bit_SYN) or RxDout(bit_FIN);
		end if;
		SEG_LEN <= SEG_TCP_LEN - SEG_TCPHDR_LEN + SEG_SYNorFIN;
		SEG_LEN_IS_0 <= not or_reduce(SEG_LEN);
		if(buf_wa(3 downto 0) = x"c")then
			case SND_WND_SCALE(1 downto 0) is
				when "01" => SEG_WNDp <= zero(18 downto 17) & RxDout(31 downto 16) & zero(0);
				when "10" => SEG_WNDp <= zero(18) & RxDout(31 downto 16) & zero(1 downto 0);
				when "11" => SEG_WNDp <= RxDout(31 downto 16) & zero(2 downto 0);
				when others => SEG_WNDp <= zero(18 downto 16) & RxDout(31 downto 16);
			end case;
		end if;
		if(OPTION_begin = '1')then
			case SND_WND_SCALE(3 downto 2) is
				when "01" => SEG_WND <= zero(31 downto 23) & SEG_WNDp & zero(3 downto 0);
				when "10" => SEG_WND <= zero(31 downto 27) & SEG_WNDp & zero(7 downto 0);
				when "11" => SEG_WND <= zero(31) & SEG_WNDp & zero(11 downto 0);
				when others => SEG_WND <= zero(31 downto 19) & SEG_WNDp;
			end case;
		end if;
		if(buf_wa(3 downto 0) = x"c")then
			tmp_SEG_WND <= RxDout(31 downto 16);
		end if;
		if(TCPstate = LISTEN)then
			ISS <= TSclock;
		end if;
		if(buf_wa(3 downto 0) = x"c" and CONN_MATCH = '1' and NOT_MY_PORT = '0' and TCPstate /= CLOSED and RxDout_valid = '1' and RxDout_type = "00")then -- check options only for valid segment
			OPTION_begin <= '1';
		else
			OPTION_begin <= '0';
		end if;
		OPTION_end <= RxDout_lastword;
		SEG_SEQ_PLUS_ONE <= SEG_SEQ + 1;
		SEG_SEQ_PLUS_LEN <= SEG_SEQ + (x"0000" & SEG_LEN);
		RCV_NXT_PLUS_WND <= RCV_NXT + RCV_WND;
		if(TCPstate = LISTEN or CTRLstate = IDLE)then
			CONN_MATCH <= '1';
		elsif((buf_wa(3 downto 0) = x"6" or buf_wa(3 downto 0) = x"7") and RxDout(15 downto 0) /= buf_do(15 downto 0))then
			CONN_MATCH <= '0';
		elsif((buf_wa(3 downto 0) = x"7" or buf_wa(3 downto 0) = x"8") and buf_we(1) = '1' and RxDout(31 downto 16) /= buf_do(33 downto 18))then
			CONN_MATCH <= '0';
		end if;
		if(A_GE_B(SEG_SEQ, RCV_NXT) and A_GT_B(RCV_NXT_PLUS_WND, SEG_SEQ))then
			SEG_SEQ_OR <= '0';
		else
			SEG_SEQ_OR <= '1';
		end if;
		if(A_GT_B(SEG_SEQ_PLUS_LEN, RCV_NXT) and A_GE_B(RCV_NXT_PLUS_WND, SEG_SEQ_PLUS_LEN))then
			SEG_SEQ_PLUS_LEN_OR <= '0';
		else
			SEG_SEQ_PLUS_LEN_OR <= '1';
		end if;
		if(A_GE_B(RCV_NXT, SEG_SEQ))then
			update_TSrecent <= '1';
		else
			update_TSrecent <= '0';
		end if;
		if(A_GE_B(RCV_NXT, SEG_SEQ) and A_GT_B(SEG_SEQ_PLUS_LEN, RCV_NXT))then
			update_RCV_NXT <= '1';
		else
			update_RCV_NXT <= '0';
		end if;
		if(chk_ReTxQueue(0) = '1'  and A_GE_B(SEG_ACK, ReTxQueue_DOB))then
			ce_ReTxQueue_rp <= '1';
		else
			ce_ReTxQueue_rp <= '0';
		end if;
		if(SEG_ACK = SND_UNA)then
			IsDupACK <= '1';
		else
			IsDupACK <= '0';
		end if;
		update_UNA_Buf <= ce_ReTxQueue_rp;
		if(IS_CONNECTED = '0')then
			UNA_MonBuf_i <= (others => '0');
		elsif(update_UNA_Buf = '1' and ReTxQueue_DOB(31) = '1')then
			UNA_MonBuf_i <= UNA_MonBuf_i + 1;
		end if;
		if(TCPstate = CLOSED or TCPstate = LISTEN or SND_UNA = SND_NXT)then
			SND_UNACK <= '0';
		else
			SND_UNACK <= '1';
		end if;		
		if(A_GE_B(CWND_LTA, SND_WND))then
			SND_WND_UL <= SND_WND + SND_UNA - 1;
		else
			SND_WND_UL <= CWND_LTA + SND_UNA - 1;
		end if;
		SND_SEQ_LAST <= SND_NXT + DATA_SIZE;
		if(A_GE_B(SND_WND_UL, SND_SEQ_LAST) or SND_UNACK = '0')then
			SND_SPACE_OK <= '1';
		else
			SND_SPACE_OK <= '0';
		end if;		
		if(A_GE_B(SND_WND_UL_CC, SND_SEQ_LAST) or FastRecovery = '0')then
			SND_SPACE_CC_OK <= '1';
		else
			SND_SPACE_CC_OK <= '0';
		end if;		
		if(A_GE_B(SEG_ACK, SND_UNA) and A_GE_B(SND_NXT, SEG_ACK))then
			SEG_ACK_OK <= '1';
		else
			SEG_ACK_OK <= '0';
		end if;
		if(SAMPLE_RTT(0) = '0' and A_GT_B(SEG_ACK, SND_UNA) and A_GE_B(SND_NXT, SEG_ACK))then
			UPDATE_SND_UNA <= '1';
		else
			UPDATE_SND_UNA <= '0';
		end if;
		if(SAMPLE_RTT(0) = '1')then
			SAMPLE_RTT(1) <= '1';
		elsif(OPTION_rdy = '1')then
			SAMPLE_RTT(1) <= '0';
		end if;
		if(TS_OPTION = '1')then
			SAMPLE_RTT(2) <= SAMPLE_RTT(1) and OPTION_rdy;
		else
			SAMPLE_RTT(2) <= SAMPLE_RTT(0);
		end if;
		if(A_GT_B(SEG_ACK, SND_NXT))then
			SEG_ACK_GT_SND_NXT <= '1';
		else
			SEG_ACK_GT_SND_NXT <= '0';
		end if;
		if(A_GT_B(SEG_SEQ, SND_WL1) or (A_GE_B(SEG_SEQ, SND_WL1) and A_GE_B(SEG_ACK, SND_WL2)))then
			update_SND_WND <= '1';
		else
			update_SND_WND <= '0';
		end if;
		SND_SYNorFIN <= SND_CTRL(bit_SYN) or SND_CTRL(bit_FIN);
	end if;
end process;
process(clk2x)
variable diff : std_logic_vector(31 downto 0);
begin
	diff := ReTxQueue_DOB - (x"0000" & ReTx_SEQ(15 downto 0));
	if(clk2x'event and clk2x = '1')then
		headerFIFO_full <= headerFIFO_AlmostFull;
		en_LINK_SyncRegs <= en_LINK_SyncRegs(1 downto 0) & en_LINK;
		if(IS_CONNECTED = '0' or SendReTxData = '1')then
			ReTxData_avl <= '0';
		elsif(ReTxData_we(1) = '1' and DDR2TCPdata(32) = '1')then
			ReTxData_avl <= '1';
		end if;
		if(ReTxDataRqst_i(2) = '1')then
			ReTxDataCntr <= (others => '0');
		elsif(ReTxData_we /= "00")then
			ReTxDataCntr <= ReTxDataCntr + 1;
		end if;
		ReTxData_avl_dl <= ReTxData_avl_dl(1 downto 0) & ReTxData_avl;
		if(ReTxData_avl_dl(2 downto 1) = "01")then
			if(SavedChksum /= ReTxData_chksum)then
				bad_ReTx_chksum <= '1';
				bad_chksum_pair <= SavedChksum & ReTxData_chksum;
			end if;
			if(ReTxDataCntr(13 downto 1) /= ReTxDataLEN_i or ReTxDataCntr(0) = '1')then
				bad_ReTx_LEN <= '1';
				bad_LEN_pair <= "00" & ReTxDataCntr & "000" & ReTxDataLEN_i;
			end if;
		else
			bad_ReTx_chksum <= '0';
			bad_ReTx_LEN <= '0';
		end if;
		if(TCPstate /= CLOSED and TCPstate /= LISTEN)then
			TIMEWAITcntr <= (others => '0');
		elsif(TIMEWAITcntr(waittime) = '0' and strobe_ms = '1')then
			TIMEWAITcntr <= TIMEWAITcntr + 1;
		end if;
		if(en_KEEPALIVE = '0' or TCPstate /= ESTAB or (CTRLstate = SND and buf_ra(4) = '0'))then
			KEEPALIVEcntr <= (others => '0');
		elsif(strobe_ms = '1')then
			KEEPALIVEcntr <= KEEPALIVEcntr + 1;
		end if;
		if(CTRLstate = IDLE and ReTxentry_avl = '1' and ReleasePending = '0')then
			RTTvld(0) <= '1';
		else
			RTTvld(0) <= '0';
		end if;
		RTTvld(2 downto 1) <= RTTvld(1 downto 0);
		if(NewDataACK = '1' or SND_UNACK = '0' or save_ReTx = '1')then
			STD_RTT <= (others => '0');
		elsif(RTTvld(2) = '1')then
--			STD_RTT <= TSclock(27 downto 12) - ReTxQueue_DOB(15 downto 0);
			STD_RTT <= TSclock(23 downto 8) - ReTxQueue_DOB(15 downto 0);
		end if;
--		TSclock11_r <= TSclock(11);
		TSclock11_r <= TSclock(7);
		if(NewDataACK = '1' or SND_UNACK = '0' or set_ReTx_TO_dl(1) = '1')then
			RTO_timer <= RTO;
--		elsif(TSclock(11) = '0' and TSclock11_r = '1')then
		elsif(TSclock(7) = '0' and TSclock11_r = '1')then
			RTO_timer <= RTO_timer - 1;
		end if;
		if(IS_CONNECTED = '0' or NewDataACK = '1' or save_ReTx = '1' or SND_UNACK = '0')then
			ReTx_TO <= '0';
		elsif(set_ReTx_TO = '1')then
			ReTx_TO <= '1';
		end if;
--		set_ReTx_TO <= not TSclock(11) and TSclock11_r and not or_reduce(RTO_timer);
		set_ReTx_TO <= not TSclock(7) and TSclock11_r and not or_reduce(RTO_timer);
		set_ReTx_TO_dl <= set_ReTx_TO_dl(0) & set_ReTx_TO;
		if(or_reduce(ReTxQueue_entry(8 downto 1)) = '1' or (ReTxQueue_entry(0) = '1' and ce_ReTxQueue_rp = '0'))then
			ReTxentry_avl <= '1';
		else
			ReTxentry_avl <= '0';
		end if;
		if(IS_CONNECTED = '0' or NewDataACK = '1')then
			ReTx_cnt <= (others => '0');
		elsif(save_ReTx = '1' and  ReTxNXT = '0')then
			ReTx_cnt <= ReTx_cnt + 1;
		end if;
		ReTxDataLastAddr <= ReTxDataAddr_i(15 downto 0) + ("000" & ReTxDataLEN_i);
		if(IS_CONNECTED = '0' or ReTxDataACK = '1')then -- reset request once acknowledged
			ReTxDataRqst_i <= "000";
		else
			if(ec_ReTx_ADDR = '1' and ReTxQueue_DOB(27 downto 26) = "00")then -- SYN and FIN has no data, save the request for later processing
				ReTxDataRqst_i(0) <= '1';
			end if;
			ReTxDataRqst_i(1) <= ReTxDataRqst_i(0);
-- check that the data to be retransmitted must be in the memory. Written_MonBuf(10 downto 9) = "11" means negative counts. If at least one event not acknowleged is in the
-- memory, or number of blocks in the memory is more than the one to be retransmitted
			if(ReTxDataRqst_i(1) = '1' and Written_MonBuf(10 downto 9) /= "11" and (or_reduce(Written_MonBuf) = '1' or ReTxDataLastAddr(15 downto 7) < Written_MonBlock))then
				ReTxDataRqst_i(2) <= '1';
			end if;
		end if;
		if(TCPstate = SYNRCVD or TCPstate = ESTAB)then
			IS_CONNECTED <= '1';
		else
			IS_CONNECTED <= '0';
		end if;
		if(TCPstate = LISTEN)then
			LISTENing <= '1';
		else
			LISTENing <= '0';
		end if;
		if(TCPstate = CLOSED)then
			IS_CLOSED <= '1';
		else
			IS_CLOSED <= '0';
		end if;
		case DupACK_Cnt is
			when "01" => CWND_LTA <= CWND + MSS;-- Limited Transmission Algorithm
			when "10" => CWND_LTA <= CWND + ('0' & MSS(31 downto 1));-- Limited Transmission Algorithm
			when others => CWND_LTA <= CWND;
		end case;
		if(TCPstate = SYNRCVD)then
			wasSYNRCVD <= '1';
		else
			wasSYNRCVD <= '0';
		end if;
		if(CTRLstate /= SND)then
			rdy2send <= '0';
		elsif(OPTION_rdy = '1' and (ReTx_ddr_wq_full = '0' or SendReTxData = '1'))then
			rdy2send <= '1';
		end if;
		if(IS_CONNECTED = '0' or (NewDataACK = '1' and PartialACK(1) = '0') or FastReTx = '1')then
			TO_Recovery <= '0';
		elsif(TO_ReTxDone = '1')then
			TO_Recovery <= '1';
		end if;
		if(IS_CONNECTED = '0' or (NewDataACK = '1' and PartialACK(0) = '0') or ReTx_TO = '1')then
			FastRecovery <= '0';
		elsif(DupACK_Cnt(1) = '1' and DupACK = '1' and A_GT_B(SEG_ACK, recover))then
			FastRecovery <= '1';
		end if;
		if(IS_CONNECTED = '0' or save_ReTx = '1' or SND_UNACK = '0')then
			ReTxNXT <= '0';
		elsif(((FastRecovery = '1' and PartialACK(0) = '1') or (TO_Recovery = '1' and PartialACK(1) = '1')) and NewDataACK = '1')then
			ReTxNXT <= '1';
		end if;
		if(IS_CONNECTED = '0' or NewDataACK = '1' or save_ReTx = '1' or SND_UNACK = '0')then
			FastReTx <= '0';
		elsif((DupACK_Cnt(1) = '1' and DupACK = '1' and A_GT_B(SEG_ACK, recover)) or ((FastRecovery = '1' or TO_Recovery = '1') and DupACK = '1' and TS_OPTION = '1' and OldData = '0'))then
			FastReTx <= '1';
		end if;
		if(DupACK_Cnt(1) = '1' and DupACK = '1' and A_GT_B(SEG_ACK, recover))then
			FastReTxStart <= '1';
		else
			FastReTxStart <= '0';
		end if;
		if(save_ReTx = '1')then
			FastReTxData <= FastReTx;
			TO_ReTxData <= ReTx_TO;
		end if;
		if(headerFIFO_DI(35 downto 34) = "11" and headerFIFO_WREN = '1' and SendReTxData = '1' and FastReTxData = '1')then
			FastReTxDone <= '1';
		else
			FastReTxDone <= '0';
		end if;
		if(headerFIFO_DI(35 downto 34) = "11" and headerFIFO_WREN = '1' and SendReTxData = '1' and TO_ReTxData = '1')then
			TO_ReTxDone <= '1';
		else
			TO_ReTxDone <= '0';
		end if;
		if(TO_ReTxDone = '1')then
			TO_recover <= SND_NXT;
		end if;
		if(FastReTxDone = '1')then
			recover <= SND_NXT;
		elsif(FastRecovery = '0')then
			recover <= SND_UNA - 1;
		end if;
		if(A_GE_B(SEG_ACK, recover))then
			PartialACK(0) <= '0';
		else
			PartialACK(0) <= '1';
		end if;
		if(A_GE_B(SEG_ACK, TO_recover))then
			PartialACK(1) <= '0';
		else
			PartialACK(1) <= '1';
		end if;
		if(IS_CONNECTED = '0' or SND_UNACK = '0' or NewDataACK = '1' or FastRecovery = '1' or TO_Recovery = '1')then
			DupACK_cnt <= (others => '0');
		elsif(DupACK = '1')then
			DupACK_cnt <= DupACK_cnt + 1;
		end if;
		if(FIFO_en = '0')then
			CTRLstate <= IDLE;
			DupACK <= '0';
			en_RxDout <= '0';
			TCPstate <= CLOSED;
			CLOSE_flag <= "0000";
			inc_ARP_sent <= '0';
			inc_ICMP_sent <= '0';
			ReleasePending <= '0';
		else
			case CTRLstate is
				when IDLE =>
					NewDataACK <= '0';
					sample_RTT(0) <= '0';
					SendReTxData <= '0';
					DupACK <= '0';
					en_RxDout <= '0';
					TCPheader_we <= '0';
					buf_we <= "00";
					IS_ARP_ICMP <= '0';
					save_ReTx <= '0';
					AddReTxEntry <= '0';
					chk_ReTxQueue <= "00";
					CTRLcntr <= (others => '0');
					buf_wa(3 downto 0) <= (others => '0');
					buf_ra <= (others => '0');
					AttachData <= '0';
					ReTxQueue_ADDRB(1 downto 0) <= "00";
					if(en_RxDout = '1')then
						if(RxDout_type = "00")then
							CTRLstate <= RCV_IPHDR;
							buf_ra(0) <= '1';
						else
							IS_ARP_ICMP <= '1';
							CTRLstate <= SND_ARP_ICMP;
							buf_ra(4) <= buf_wa(4);
						end if;
						if(RxDout_type /= "10")then -- RxDout_type = "10" is ARP reply, this server does not send ARP request
							inc_ARP_sent <= RxDout_type(1) and RxDout_type(0);
							inc_ICMP_sent <= RxDout_type(0) and not RxDout_type(1);
							buf_we <= "11";
						end if;
					elsif((en_LINK_SyncRegs(2) = '0' and TCPstate /= CLOSED) or (ReTx_cnt(2) = '1' and (FastReTx = '1' or ReTx_TO = '1')))then
						TCPstate <= CLOSED;
						CLOSE_flag <= "0001";
						if(TCPstate = ESTAB)then
							SND_SEQ <= SND_NXT;
							SND_CTRL <= "000100";--RST
							CTRLstate <= SND;
						end if;
					elsif(en_LINK_SyncRegs(2) = '1' and TCPstate = CLOSED and (simulation or TIMEWAITcntr(waittime) = '1'))then
						WaitReTxData <= '0';
						TCPstate <= LISTEN;
						ReleasePending <= '0';
					elsif((FastReTx = '1' or ReTx_TO = '1' or ReTxNXT = '1') and WaitReTxData = '0' and ReTx_FIFO_empty = '1' and headerFIFO_full = '0' and ReleasePending = '0')then
						CTRLstate <= RETX;
					elsif(RxDout_avl = '1' and headerFIFO_full = '0')then
						en_RxDout <= '1';
					elsif(TCPstate = ESTAB)then
						if(ReTxData_avl = '1')then
							SendReTxData <= '1';
							WaitReTxData <= '0';
							AttachData <= '1';
							SND_CTRL(4 downto 0) <= ReTx_CTRL;
							SND_SEQ <= ReTx_SEQ;
							SND_ACK <= RCV_NXT;
							CTRLstate <= SND;
						elsif((ReTx_ddr_wq_full = '0' and headerFIFO_full = '0' and Data2Send = '1' and SND_SPACE_OK = '1' and SND_SPACE_CC_OK = '1' and EnNewData = '1') or KEEPALIVEcntr(15) = '1')then
							SND_SEQ <= SND_NXT;
							AttachData <= Data2Send;
							if(Data2Send = '1' and EoB = '1')then
								SND_CTRL <= EoB & '1' & push & "000";--ACK and PUSH
							else
								SND_CTRL <= "010000";--ACK
							end if;
							SND_ACK <= RCV_NXT;
							AddReTxEntry <= Data2Send;
							CTRLstate <= SND;
						elsif(ReleasePending = '1' and ReTxentry_avl = '1')then
							CTRLstate <= RELEASE;
						end if;
					end if;
				when RETX =>
					ReTxQueue_ADDRB(0) <= not ReTxQueue_ADDRB(0);
					ReTxQueue_ADDRB(1) <= ReTxQueue_ADDRB(1) xor ReTxQueue_ADDRB(0);
					if(ReTxQueue_ADDRB(1 downto 0) = "10")then
						ReTx_SEQ(15 downto 0) <= ReTxQueue_DOB(31 downto 16);
					end if;
					if(ReTxQueue_ADDRB(1 downto 0) = "11")then
						ReTx_SEQ(31 downto 16) <= diff(31 downto 16);
						ReTxDataLEN_i <= diff(15 downto 3);
						save_ReTx <= '1';
					else
						save_ReTx <= '0';
					end if;
					if(save_ReTx = '1')then
						SavedChksum <= ReTxQueue_DOB(31 downto 16);
						ReTxIP_ID <= ReTxQueue_DOB(15 downto 0);
					end if;
					ec_ReTx_ADDR <= save_ReTx;
					if(ec_ReTx_ADDR = '1')then
						ReTx_CTRL <= ReTxQueue_DOB(30 downto 26);
						ReTxDataAddr_i <= ReTxQueue_DOB(25 downto 0);
						if(ReTxQueue_DOB(27 downto 26) = "00")then
							CTRLstate <= IDLE;
							WaitReTxData <= '1';
						else
							SND_CTRL(4 downto 0) <= ReTxQueue_DOB(30 downto 26);
							SND_SEQ <= ReTx_SEQ;
							SND_ACK <= RCV_NXT;
							CTRLstate <= SND;
						end if;
					end if;
				when RELEASE =>
					if(ReTxQueue_ADDRB(1 downto 0) = "10")then
						if(ReTxentry_avl = '0')then
							CTRLstate <= IDLE;
						else
							chk_ReTxQueue(0) <= '1';
						end if;
					else
						chk_ReTxQueue(0) <= '0';
					end if;
--					if(chk_ReTxQueue(0) = '1' and SEG_ACK = ReTxQueue_DOB)then
					if(chk_ReTxQueue(0) = '1' and A_GE_B(ReTxQueue_DOB, SEG_ACK))then
						ReleasePending <= '0';
					end if;
					chk_ReTxQueue(1) <= chk_ReTxQueue(0);
					ReTxQueue_ADDRB(0) <= not ReTxQueue_ADDRB(0);
					ReTxQueue_ADDRB(1) <= ReTxQueue_ADDRB(1) xor ReTxQueue_ADDRB(0);
					if(chk_ReTxQueue(1) = '1' and ReleasePending = '0')then
						CTRLstate <= IDLE;
					end if;
				when SND_ARP_ICMP =>
					inc_ARP_sent <= '0';
					inc_ICMP_sent <= '0';
					if(RxDout_valid = '0')then
						CTRLstate <= IDLE;
						buf_we <= "00";
					end if;
				when RCV_IPHDR =>
					if(CTRLcntr = IPHDR_END)then
						buf_we <= "10";
					else
						case buf_wa(3 downto 0) is
							when x"2" | x"3" | x"4" => buf_we <= "00";
							when x"5" | x"8" => buf_we <= "01";
							when others => buf_we <= "11";
						end case;
					end if;
					CTRLcntr <= CTRLcntr + 1;
					if(buf_wa(3) = '0' or CTRLcntr = IPHDR_END)then
						buf_wa(3 downto 0) <= buf_wa(3 downto 0) + 1;
						buf_ra(3 downto 0) <= buf_ra(3 downto 0) + 1;
					end if;
					if(CTRLcntr = IPHDR_END)then
						CTRLstate <= RCV_TCPHDR;
					end if;
				when RCV_TCPHDR =>
					buf_we <= "00";
					if(buf_wa(3 downto 0) /= x"d")then
						buf_wa(3 downto 0) <= buf_wa(3 downto 0) + 1;
						buf_ra(3 downto 0) <= buf_ra(3 downto 0) + 1;
					end if;
					if(RxDout_valid = '0')then
						CTRLstate <= RCV_PROCESS;
					end if;
					CTRLcntr <= (others => '0');
				when RCV_PROCESS =>
					ReTxQueue_ADDRB(1 downto 0) <= "00";
					if(NOT_MY_PORT = '1' or CONN_MATCH = '0')then
						buf_ra(4) <= buf_wa(4);
					end if;
					if(TCPstate = CLOSED or NOT_MY_PORT = '1' or CONN_MATCH = '0')then
						if(SEG_CTRL(bit_RST) = '1')then
							CTRLstate <= IDLE;
						else
							if(SEG_CTRL(bit_ACK) = '0')then
								SND_SEQ <= (others => '0');
								SND_ACK <= SEG_SEQ_PLUS_LEN;
								SND_CTRL <= "010100";--ACK,RST
							else
								SND_SEQ <= SEG_ACK;
								SND_CTRL <= "000100";--RST
							end if;
							CTRLstate <= SND;
						end if;
					elsif(TCPstate = LISTEN)then
						if(SEG_CTRL(bit_RST) = '1')then
							CTRLstate <= IDLE;
						elsif(SEG_CTRL(bit_ACK) = '1')then
							SND_SEQ <= SEG_ACK;
							SND_CTRL <= "000100";--RST
							CTRLstate <= SND;
						elsif(SEG_CTRL(bit_SYN) = '1')then
							RCV_NXT <= SEG_SEQ_PLUS_LEN;-- if any data attached, they are discarded
							SND_WND <= SEG_WND;
							input_SEG_WND0 <= tmp_SEG_WND;
							IRS <= SEG_SEQ;
							SND_UNA <= ISS;
							SND_SEQ <= ISS;
							SND_ACK <= SEG_SEQ_PLUS_ONE;
							SND_CTRL <= "010010";--ACK,SYN
							AddReTxEntry <= '1';
							CTRLstate <= SND;
							if(SEG_CTRL(bit_FIN) = '1')then -- This is part of the processing in TCPstate SYNRCVD
								TCPstate <= CLOSED;
							else
								TCPstate <= SYNRCVD;
							end if;
						else
							SND_SEQ <= (others => '0');
							SND_ACK <= SEG_SEQ_PLUS_LEN;
							SND_CTRL <= "010100";--ACK,RST
							CTRLstate <= SND;
						end if;
					elsif(SEG_SEQ_OR = '1' and SEG_SEQ_PLUS_LEN_OR = '1')then
						if(SEG_CTRL(bit_RST) = '1')then
							CTRLstate <= IDLE;
						else
							SND_SEQ <= SND_NXT;
							SND_ACK <= RCV_NXT;
							SND_CTRL <= "010000";--ACK
							CTRLstate <= SND;
						end if;
					elsif(SEG_CTRL(bit_RST) = '1')then
						if(TCPstate = SYNRCVD)then
							TCPstate <= LISTEN;
							CTRLstate <= IDLE;
						else
							TCPstate <= CLOSED;
							CLOSE_flag <= "0010";
							CTRLstate <= IDLE;
						end if;
					elsif(SEG_CTRL(bit_SYN) = '1' and SEG_SEQ_OR = '0')then
						TCPstate <= CLOSED;
						CLOSE_flag <= "0100";
						if(SEG_CTRL(bit_ACK) = '1')then
							SND_SEQ <= SND_NXT;
							SND_ACK <= SEG_SEQ_PLUS_LEN;
							SND_CTRL <= "010100";--ACK,RST
						else
							SND_SEQ <= SEG_ACK;
							SND_CTRL <= "000100";--RST
						end if;
						CTRLstate <= SND;
					elsif(SEG_CTRL(bit_ACK) = '0')then
						CTRLstate <= IDLE;
					elsif(OPTION_rdy = '1')then
						if(TCPstate = SYNRCVD)then
							if(SEG_ACK_OK = '1')then
								TCPstate <= ESTAB;
								SND_WND <= SEG_WND; -- stay in RCV_PROCESS state and continue process in TCPstate ESTAB
								SND_WL1 <= SEG_SEQ;
								SND_WL2 <= SEG_ACK;
								input_SEG_WND1 <= tmp_SEG_WND;
							else
								SND_SEQ <= SEG_ACK;
								SND_CTRL <= "000100";--RST
								CTRLstate <= SND;
							end if;
						end if;
						if(TCPstate = ESTAB)then
							if(UPDATE_SND_UNA = '1')then
								ReleasePending <= '1';
								sample_RTT(0) <= '1';
								SND_UNA <= SEG_ACK;
								NewDataACK <= not wasSYNRCVD;
								if(update_SND_WND = '1')then
									SND_WND <= SEG_WND;
									SND_WL1 <= SEG_SEQ;
									SND_WL2 <= SEG_ACK;
									input_SEG_WND2 <= tmp_SEG_WND;
								end if;
								CTRLstate <= IDLE;
							elsif(SEG_ACK_GT_SND_NXT = '1')then
								SND_SEQ <= SND_NXT;
								SND_ACK <= RCV_NXT;
								SND_CTRL <= "010000";--ACK
								CTRLstate <= SND;
							else
								DupACK <= IsDupACK;
								if(SEG_CTRL(bit_FIN) = '0')then
									if(SEG_LEN_IS_0 = '1')then
										CTRLstate <= IDLE;
									else
										if(update_RCV_NXT = '1')then
											RCV_NXT <= SEG_SEQ_PLUS_LEN;
											SND_ACK <= SEG_SEQ_PLUS_LEN;
										else
											SND_ACK <= RCV_NXT;-- just discard the segment
										end if;
										SND_SEQ <= SND_NXT;
										SND_CTRL <= "010000";--ACK
										CTRLstate <= SND;
									end if;
								end if;
							end if;
						end if;
						if(TCPstate = LASTACK and SEG_ACK_OK = '1')then
							TCPstate <= CLOSED;
							CLOSE_flag <= "1000";
							CTRLstate <= IDLE;
						end if;
						if(SEG_CTRL(bit_FIN) = '1')then
							if(TCPstate = CLOSED or TCPstate = LISTEN)then
								CTRLstate <= IDLE;
							else
								RCV_NXT <= SEG_SEQ_PLUS_LEN;
								SND_SEQ <= SND_NXT;
								SND_ACK <= SEG_SEQ_PLUS_LEN;
								SND_CTRL <= "010000";--ACK
								if(TCPstate = SYNRCVD or TCPstate = ESTAB)then
									SND_CTRL <= "010001";--ACK, FIN
									TCPstate <= LASTACK;
									AddReTxEntry <= '1';
								end if;
								CTRLstate <= SND;
							end if;
						end if;
					end if;
					buf_ra(3 downto 0) <= (others => '0');
				when SND =>
					DupACK <= '0';
					if(AddReTxEntry = '1')then
						SND_NXT <= SND_SEQ + DATA_LEN + SND_SYNorFIN;
					end if;
					if(CTRLcntr(2 downto 0) = "111")then
						AddReTxEntry <= '0';
					end if;
					if(rdy2send = '1')then
						if(AddReTxEntry = '1' and CTRLcntr(2) = '1')then
							case CTRLcntr(1 downto 0) is
								when "00" =>
--									ReTx_ddr_wq_in(31 downto 0) <= SND_SEQ(15 downto 0) & TSclock(27 downto 12);
									ReTx_ddr_wq_in(31 downto 0) <= SND_SEQ(15 downto 0) & TSclock(23 downto 8);
									ReTX_ddr_wq_we <= '1';
								when "01" =>
									ReTx_ddr_wq_in(31 downto 0) <= SND_NXT;
								when "10" =>
									ReTx_ddr_wq_in(31 downto 0) <= DATA_chksum & IP_ID;
								when others =>
									ReTx_ddr_wq_in(31 downto 0) <= SND_CTRL & SEG_ADDR;
							end case;
						else
							ReTX_ddr_wq_we <= '0';
						end if;
						buf_ra(3 downto 0) <= buf_ra(3 downto 0) + 1;
						CTRLcntr <= CTRLcntr + 1;
						if(CTRLcntr = TCPHDR_end)then
							if(AttachData = '0')then
								CTRLstate <= IDLE;
							else
								CTRLstate <= WAIT4SND2END;
							end if;
							TCPheader_we <= '0';
						else
							TCPheader_we <= '1';
						end if;
					end if;
				when WAIT4SND2END =>
					if(headerFIFO_DI(35 downto 34) = "11" and headerFIFO_WREN = '1')then
						CTRLstate <= IDLE;
					end if;
				when others => CTRLstate <= IDLE;
			end case;
		end if;
	end if;
end process;
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		if(reset = '1')then
			FIFO_en <= '0';
		elsif(FIFO_rst = '0' and reset_dl = '1')then
			FIFO_en <= '1';
		end if;
	end if;
end process;
i_FIFO_rst : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q => FIFO_rst,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => reset        -- SRL data input
   );
i_reset_dl1 : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q => reset_dl,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => FIFO_rst        -- SRL data input
   );
i_headerFIFO : FIFO_DUALCLOCK_MACRO
   generic map (
      DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
      ALMOST_FULL_OFFSET => X"0180",  -- Sets almost full threshold
      ALMOST_EMPTY_OFFSET => X"0080", -- Sets the almost empty threshold
      DATA_WIDTH => 36,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      FIFO_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb" 
      FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
   port map (
      ALMOSTEMPTY => open,   -- 1-bit output almost empty
      ALMOSTFULL => headerFIFO_AlmostFull,     -- 1-bit output almost full
      DO => headerFIFO_DO,                     -- Output data, width defined by DATA_WIDTH parameter
      EMPTY => headerFIFO_EMPTY,               -- 1-bit output empty
      FULL => open,                 -- 1-bit output full
      RDCOUNT => headerFIFO_RDCOUNT,           -- Output read count, width determined by FIFO depth
      RDERR => open,               -- 1-bit output read error
      WRCOUNT => headerFIFO_WRCOUNT,           -- Output write count, width determined by FIFO depth
      WRERR => open,               -- 1-bit output write error
      DI => headerFIFO_DI,                     -- Input data, width defined by DATA_WIDTH parameter
      RDCLK => clk2x,               -- 1-bit input read clock
      RDEN => headerFIFO_RDEN,                 -- 1-bit input read enable
      RST => FIFO_rst,                   -- 1-bit input reset
      WRCLK => clk2x,               -- 1-bit input write clock
      WREN => headerFIFO_WREN                  -- 1-bit input write enable
   );
headerFIFO_RDEN <= fifo_en and not ClientEmacTxd_sel(1) and ((not rate_pause and not headerFIFO_EMPTY and not ClientEmacTxdVld(3)) or EmacClientTack or rd_headerFIFO);
i_ReTx_FIFO : FIFO65x8k PORT MAP(
		clk => clk2x,
		fifo_rst => FIFO_rst,
		fifo_en => fifo_en,
		di => DDR2TCPdata,
		we => ReTxData_we,
		re => ReTxData_re,
		empty => ReTx_FIFO_empty,
		FIFO_WrErr => ReTx_FIFO_WrErr,
		do => ReTxData
	);
g_SND_DATA_dl: for i in 0 to 33 generate
	i_SND_DATA_dl : SRL16E
   port map (
      Q => SND_DATA_dl(i),       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => SND_DATA(i)        -- SRL data input
   );
end generate;
i_sel_IPHDR_chksum : SRL16E
   port map (
      Q => sel_IPHDR_chksum,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => is_IPHDR_chksum        -- SRL data input
   );
i_sel_TCP_chksum : SRL16E
   port map (
      Q => sel_TCP_chksum,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => is_TCP_chksum        -- SRL data input
   );
i_headerFIFO_WREN : SRL16E
   port map (
      Q => headerFIFO_WREN,       -- SRL data output
      A0 => '0',     -- Select[0] input
      A1 => '0',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => SND_DATA_vld        -- SRL data input
   );
i_headerFIFO_DI34 : SRL16E
   port map (
      Q => SND_DATA_end_dl,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '0',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => SND_DATA_end        -- SRL data input
   );
i_headerFIFO_DI35 : SRL16E
   port map (
      Q => AttachData_dl,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk2x,   -- Clock input
      D => AttachData        -- SRL data input
   );
i_IPHDR_checksum: checksum PORT MAP(
		c => clk2x,
		r => rst_chksum,
		r_value => x"ffff",
		ce => ce_IPHDR_chksum,
		d => chksum_in,
		bad_chksum => open,
		s => IPHDR_chksum
	);
i_TCP_checksum: checksum PORT MAP(
		c => clk2x,
		r => rst_chksum,
		r_value => TCP_LEN,
		ce => ce_TCP_chksum,
		d => chksum_in,
		bad_chksum => open,
		s => TCP_chksum
	);
-- headerFIFO input control
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		headerFIFO_DI(34) <= SND_DATA_end_dl;
		if(SND_DATA_end_dl = '1')then
			headerFIFO_DI(35) <= AttachData_dl;
		else
			headerFIFO_DI(35) <= SendReTxData;
		end if;
		save_ReTxTime <= SendReTxData and SND_DATA_end_dl;
		RxDout_valid_dl <= RxDout_valid_dl(0) & RxDout_valid;
		if(IS_ARP_ICMP = '1')then
			SND_DATA_vld <= RxDout_valid_dl(1);
			SND_DATA_end <= SND_DATA_vld and not RxDout_valid_dl(1);
			SND_DATA(33 downto 32) <= buf_DO(17 downto 16);
		else
			SND_DATA_vld <= TCPheader_we;
			SND_DATA_end <= SND_DATA_vld and not TCPheader_we;
			SND_DATA(33 downto 32) <= "00";
		end if;
		if(SendReTxData = '1')then
			TCP_DATA_LEN <= ReTxDataLEN_i & "000";
			TCP_DATA_chksum <= ReTxData_chksum;
			IP_ID2Send <= ReTxIP_ID;
		else
			TCP_DATA_LEN <= DATA_LEN(15 downto 0);
			TCP_DATA_chksum <= DATA_chksum;
			IP_ID2Send <= IP_ID;
		end if;
		if(reset = '1')then
			IP_ID <= (others => '0');
		elsif(CTRLcntr(2 downto 0) = "100" and AddReTxEntry = '1')then
			IP_ID <= IP_ID + 1;
		end if;
		TCP_LEN <= (DATA_OFFSET & "00") + TCP_DATA_LEN;
		HDR_LEN <= ("0000000000" & DATA_OFFSET) + "00000000000101";
		TOTAL_LEN <= (TCP_DATA_LEN(15 downto 2) + HDR_LEN) & "00";
		if(CTRLcntr(3) = '0')then
			MUX_TCPHDR <= TOTAL_LEN & IP_ID2Send;
		else
			case CTRLcntr(1 downto 0) is
				when "10" => MUX_TCPHDR <= SND_SEQ(15 downto 0) & SND_ACK(31 downto 16);
				when "11" => MUX_TCPHDR <= SND_ACK(15 downto 0) & DATA_OFFSET & "0000000" & SND_CTRL(4 downto 0);
				when others => MUX_TCPHDR <= RCV_WND(15 downto 0) & TCP_DATA_chksum;
			end case;
		end if;
		case CTRLcntr is
			when "00000" | "00001" | "00010" | "00011" | "00101"  | "00110" | "00111" | "01000" => sel_SND_DATA <= "00";
			when "01001" => sel_SND_DATA <= "01";
			when "00100" | "01010" | "01011" | "01100" => sel_SND_DATA <= "10";
			when others => sel_SND_DATA <= "11";
		end case;		
		case sel_SND_DATA is
			when "00" => SND_DATA(31 downto 0) <= buf_DO(33 downto 18) & buf_DO(15 downto 0);
			when "01" => SND_DATA(31 downto 0) <= buf_DO(33 downto 18) & SND_SEQ(31 downto 16);
			when "10" => SND_DATA(31 downto 0) <= MUX_TCPHDR;
			when others => SND_DATA(31 downto 0) <= OPTION_data;
		end case;
		if(CTRLcntr(3 downto 0) = x"b")then
			rd_OPTION <= '1';
		else
			rd_OPTION <= '0';
		end if;
		if(CTRLcntr(3 downto 0) = x"4")then
			rst_chksum <= '1';
		else
			rst_chksum <= '0';
		end if;
		SND_DATA_r <= SND_DATA(15 downto 0);
		chksum_in <= ('0' & SND_DATA_r) + ('0' & SND_DATA(31 downto 16));
		if(CTRLstate = SND and CTRLcntr(3 downto 0) = x"6")then
			ce_IPHDR_chksum <= '1';
		elsif(CTRLcntr(3 downto 0) = x"b")then
			ce_IPHDR_chksum <= '0';
		end if;
		if(CTRLcntr(3 downto 0) = x"7")then
			is_IPHDR_chksum <= '1';
		else
			is_IPHDR_chksum <= '0';
		end if;
		TCPHDR_end <= ('0' & DATA_OFFSET) + "01001";
		if(CTRLstate = SND and CTRLcntr(3 downto 0) = x"9")then
			ce_TCP_chksum <= '1';
		elsif(TCPHDR_cntr(3 downto 2) = "11")then
			ce_TCP_chksum <= '0';
		end if;
		if(ce_TCP_chksum = '0')then
			TCPHDR_cntr <= DATA_OFFSET;
		else
			TCPHDR_cntr <= TCPHDR_cntr - 1;
		end if;
		if(CTRLcntr(3 downto 0) = x"d")then
			is_TCP_chksum <= '1';
		else
			is_TCP_chksum <= '0';
		end if;
		if(sel_IPHDR_chksum = '1')then
			headerFIFO_DI(32 downto 16) <= SND_DATA_dl(32 downto 32) & IPHDR_chksum;
		else
			headerFIFO_DI(32 downto 16) <= SND_DATA_dl(32 downto 16);
		end if;
		if(IS_ARP_ICMP = '1')then
			headerFIFO_DI(33) <= SND_DATA_dl(33);
		else
			headerFIFO_DI(33) <= headerFIFO_DI(34);
		end if;
		if(sel_TCP_chksum = '1')then
			headerFIFO_DI(15 downto 0) <= TCP_chksum;
		else
			headerFIFO_DI(15 downto 0) <= SND_DATA_dl(15 downto 0);
		end if;
	end if;
end process;
i_TCP_OPTION: TCP_OPTION PORT MAP(
		clk => clk2x,
		CLOSED => IS_CLOSED,
		update_TSrecent => update_TSrecent,
		TSclock => TSclock,
		RCV_SYN => SEG_CTRL(bit_SYN),
		SND_SYN => SND_CTRL(bit_SYN),
		LISTEN => LISTENing,
		Save_ReTx => Save_ReTx,
		Save_ReTxTime => Save_ReTxTime,
		OPTION_begin => OPTION_begin,
		OPTION_end => OPTION_end,
		di => RxDout(31 downto 0),
		scale => SND_WND_SCALE,
		DATA_OFFSET => DATA_OFFSET,
		MSS => MSS(15 downto 0),
		RTT => TS_RTT,
		OPTION_rdy => OPTION_rdy,
		TS_OPTION => TS_OPTION,
		rd_dout => rd_OPTION,
		OldData => OldData,
		dout => OPTION_data,
		debug => debug_opt
	);
-- dataFIFO input control
g_EventBufAddr: for i in 0 to 1 generate
	i_EventBufAddr: RAM32x6D PORT MAP(
		wclk => sysclk,
		rclk => clk2x,
		di => EventBufAddr(i*6+5 downto i*6),
		we => EventBufAddr_we,
		wa => EventBufAddr_wa,
		ra => EventBufAddr_ra,
		ceReg => '1',
		do => EventBufAddr_do(i*6+5 downto i*6)
	);
  i_EventBufAddrH : RAM32X1D
   port map (
      DPO => EventBufAddr_dop(i+12),     -- Read-only 1-bit data output
      SPO => open,     -- R/W 1-bit data output
      A0 => EventBufAddr_wa(0),       -- R/W address[0] input bit
      A1 => EventBufAddr_wa(1),       -- R/W address[1] input bit
      A2 => EventBufAddr_wa(2),       -- R/W address[2] input bit
      A3 => EventBufAddr_wa(3),       -- R/W address[3] input bit
      A4 => EventBufAddr_wa(4),       -- R/W address[4] input bit
      D => EventBufAddr(i+12),         -- Write 1-bit data input
      DPRA0 => EventBufAddr_ra(0), -- Read-only address[0] input bit
      DPRA1 => EventBufAddr_ra(1), -- Read-only address[1] input bit
      DPRA2 => EventBufAddr_ra(2), -- Read-only address[2] input bit
      DPRA3 => EventBufAddr_ra(3), -- Read-only address[3] input bit
      DPRA4 => EventBufAddr_ra(4), -- Read-only address[4] input bit
      WCLK => sysclk,   -- Write clock input
      WE => EventBufAddr_we        -- Write enable input
   );
end generate;
process(sysclk,IS_CONNECTED,EventBufAddr_wa,EventBufAddr_ra1SyncRegs,EventBufAddr_ra0SyncRegs)
variable s : std_logic_vector(3 downto 0);
begin
	s := EventBufAddr_wa(1 downto 0) & EventBufAddr_ra1SyncRegs(2) & EventBufAddr_ra0SyncRegs(2);
	if(IS_CONNECTED = '0')then
		EventBufAddr_wa <= (others => '0');
		EventBufAddr_ra1SyncRegs <= (others => '0');
		EventBufAddr_ra0SyncRegs <= (others => '0');
	elsif(sysclk'event and sysclk = '1')then
		if(EventBufAddr_we = '1')then
			EventBufAddr_wa(1) <= EventBufAddr_wa(0);
			EventBufAddr_wa(0) <= not EventBufAddr_wa(1);
		end if;
		EventBufAddr_ra1SyncRegs <= EventBufAddr_ra1SyncRegs(1 downto 0) & EventBufAddr_ra(1);
		EventBufAddr_ra0SyncRegs <= EventBufAddr_ra0SyncRegs(1 downto 0) & EventBufAddr_ra(0);
		case s is
			when x"1" | x"7" | x"8" | x"e" => AddrBuf_full <= '1';
			when others => AddrBuf_full <= '0';
		end case;
	end if;
end process;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(EventBufAddr_we = '1')then
			ADDR_offset <= EventBufAddr(3 downto 0);
		end if;
	end if;
end process;
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		if(IS_CONNECTED = '0')then
			EventBufAddr_ra <= (others => '0');
		elsif(EVENTdata_re_i = '1' and EVENTdata_avl = '1' and EVENTdata(65) = '1')then
			EventBufAddr_ra(1) <= EventBufAddr_ra(0);
			EventBufAddr_ra(0) <= not EventBufAddr_ra(1);
		end if;
		if(EVENTdata_re_i = '1' and EVENTdata_avl = '1' and EVENTdata(65) = '1')then
			SEG_ADDR(25 downto 12) <= EventBufAddr_do;
			SEG_ADDR(11 downto 0) <= (others => '0');
		elsif(headerFIFO_DI(35 downto 34) = "11" and headerFIFO_WREN = '1' and SendReTxData = '0')then
			SEG_ADDR(15 downto 0) <= SEG_ADDR(15 downto 0) + ("000" & data_wc);
		end if;
		MSS_cutoff <= MSS(15 downto 3) - 1;
		if(IS_CONNECTED = '0' and DATAstate /= IDLE)then
			DATAstate <= IDLE;
		else
			case DATAstate is
				when IDLE =>
					EoB <= '0';
					push <= '0';
					data2send <= '0';
					EVENTdata_re_i <= '0';
					data_wc <= (others => '0');
					if(EVENTdata_avl = '1' and dataFIFO_full = '0')then
						DATAstate <= ReadEVENTdata;
						EVENTdata_re_i <= '1';
					end if;
				when ReadEVENTdata =>
					if(EVENTdata_re_i = '1' and EVENTdata_avl = '1' and (EVENTdata(64) = '1' or data_wc = MSS_cutoff))then
						DATAstate <= Wait4Send;
						data2send <= '1';
						EVENTdata_re_i <= '0';
					else
						EVENTdata_re_i <= not dataFIFO_full;
					end if;
					if(EVENTdata_re_i = '1' and EVENTdata_avl = '1' and EVENTdata(66) = '1')then
						push <= '1';
					end if;
					if(EVENTdata_re_i = '1' and EVENTdata_avl = '1' and EVENTdata(64) = '1')then
						EoB <= '1';
					end if;
					if(EVENTdata_re_i = '1' and EVENTdata_avl = '1')then
						data_wc <= data_wc + 1;
					end if;
				when Wait4Send =>
					if(headerFIFO_DI(35 downto 34) = "11" and headerFIFO_WREN = '1' and SendReTxData = '0')then
						DATAstate <= IDLE;
						data2send <= '0';
					end if;
				when others => DATAstate <= IDLE;
			end case;
		end if;
		if(DATAstate = IDLE)then
			was_IDLE <= '1';
		else
			was_IDLE <= '0';
		end if;
		if(data_wc = MSS_cutoff)then
			dataFIFO_DI <= '1' & Eventdata(63 downto 0);
		else
			dataFIFO_DI <= Eventdata(64 downto 0);
		end if;
		dataFIFO_WREN <= EVENTdata_re_i and EVENTdata_avl;
	end if;
end process;
i_TCPdata_chksum: TCPdata_chksum PORT MAP(
		c => clk2x,
		r => rst_DATA_chksum,
		ce => dataFIFO_WREN,
		d => dataFIFO_DI(63 downto 0),
		DATA_OFFSET => x"0",
		length_in => data_wc, -- in 64bit words
		en_out => AttachData,
		s => DATA_chksum,
		chksum => open,
		DATA_SIZE => DATA_SIZE, -- in bytes
		DATA_LEN => DATA_LEN(15 downto 0) -- in bytes
	);
rst_DATA_chksum <= was_IDLE;
i_dataFIFO : FIFO65x12k PORT MAP(
		clk => clk2x,
		fifo_rst => FIFO_rst,
		fifo_en => fifo_en,
		di => dataFIFO_DI,
		we => dataFIFO_WREN,
		re => dataFIFO_RDEN,
		full => dataFIFO_full,
		empty => dataFIFO_empty,
		do => dataFIFO_DO
	);
dataFIFO_RDEN <= '1' when IS_CLOSED = '1' or (sel_ReTx_DATA = '0' and ClientEmacTxd_sel = "10") else '0';
TCPdata <= ReTxData when sel_ReTx_DATA = '1' else dataFIFO_DO(64 downto 0);
ReTxData_re <= '1' when IS_CLOSED = '1' or (sel_ReTx_DATA = '1' and ClientEmacTxd_sel = "10") else '0';
-- output to EMAC 
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		TCPdata_q <= TCPdata(15 downto 0);
		headerFIFO_eof <= headerFIFO_DO(34);
		if(EmacClientTack = '1')then
			sel_ReTx_DATA <= headerFIFO_DO(35);
		end if;
		if(fifo_en = '0')then
			rd_headerFIFO <= '0';
		elsif(EmacClientTack = '1')then
			rd_headerFIFO <= '1';
		elsif(headerFIFO_eof = '1')then
			rd_headerFIFO <= '0';
		end if;
		if((headerFIFO_eof = '1' and ClientEmacTxd_sel(0) = '0') or (dataFIFO_eof = '1' and ClientEmacTxd_sel(0) = '1'))then
			eof <= '1';
		else
			eof <= '0';
		end if;
		if(TCPdata(64) = '1' and ClientEmacTxd_sel(0) = '0')then
			dataFIFO_eof <= '1';
		else
			dataFIFO_eof <= '0';
		end if;
		if(fifo_en = '0' or (dataFIFO_eof = '1' and ClientEmacTxd_sel(0) = '1'))then
			ClientEmacTxd_sel <= "00";
		elsif(headerFIFO_DO(34) = '1')then
			ClientEmacTxd_sel <= '0' & headerFIFO_DO(35);
		elsif(ClientEmacTxd_sel = "01")then
			ClientEmacTxd_sel <= "10";
		elsif(ClientEmacTxd_sel(1) = '1')then
			ClientEmacTxd_sel(0) <= not ClientEmacTxd_sel(0);
		end if;
		case ClientEmacTxd_sel is
			when "00" => 
									if(ClientEmacTxdVld(3) = '0' or EmacClientTack = '1' or rd_headerFIFO = '1')then
										ClientEmacTxd <= headerFIFO_DO(31 downto 0);
									end if;
									if(fifo_en = '0' or eof = '1' or headerFIFO_EMPTY = '1' or rate_pause = '1')then
										ClientEmacTxdVld <= x"0";
									else
										case headerFIFO_DO(33 downto 32) is
											when "00" => ClientEmacTxdVld <= x"f";
											when "01" => ClientEmacTxdVld <= x"8";
											when "10" => ClientEmacTxdVld <= x"c";
											when others => ClientEmacTxdVld <= x"e";
										end case;
									end if;
			when "01" => ClientEmacTxd <= headerFIFO_DO(31 downto 16) & TCPdata(63 downto 48);
									 ClientEmacTxdVld <= x"f";
			when "10" => ClientEmacTxd <= TCPdata(47 downto 16);
									 ClientEmacTxdVld <= x"f";
			when others => ClientEmacTxd <= TCPdata_q & TCPdata(63 downto 48);
									 ClientEmacTxdVld <= "11" & not dataFIFO_eof & not dataFIFO_eof;
		end case;
		if(delta_rate(7 downto 0) = x"00" or EmacClientTack = '1')then
			wc <= (others => '0');
		elsif(ClientEmacTxd_sel(1) = '1' or rd_headerFIFO = '1')then
			wc <= wc + delta_rate;
		elsif(rate_pause = '1')then
			wc <= wc - rate_limit;
		end if;
		if(wc(21) = '1' or delta_rate(7 downto 0) = x"00")then
			rate_pause <= '0';
--		elsif((headerFIFO_eof = '1' and ClientEmacTxd_sel(0) = '0') or (dataFIFO_eof = '1' and ClientEmacTxd_sel(0) = '1'))then
		elsif(eof = '1')then
			rate_pause <= '1';
		end if;
		if(RxDout_type = "01" and en_RxDout = '1')then
			cs_out(28) <= '1';
		else
			cs_out(28) <= '0';
		end if;
	end if;
end process;
--cs_out(35 downto 32) <= (others => '0');
--cs_out(31) <= eof;
--cs_out(30) <= wc(21);
--cs_out(29) <= headerFIFO_EMPTY;
--cs_out(27) <= ClientEmacTxdVld(3);
--cs_out(26) <= EmacClientTack;
--cs_out(25) <= rd_headerFIFO;
--cs_out(24) <= dataFIFO_eof;
--cs_out(23) <= rate_pause;
--cs_out(22 downto 21) <= ClientEmacTxd_sel;
--cs_out(20 downto 0) <= wc(20 downto 0);
delta_rate <= (others => '0') when and_reduce(rate_limit(6 downto 2)) = '1' else x"7c" - rate_limit;
-- retransmission control
i_ReTxQueue : BRAM_TDP_MACRO
   generic map (
      BRAM_SIZE => "36Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      DOA_REG => 1, -- Optional port A output register (0 or 1)
      DOB_REG => 0, -- Optional port B output register (0 or 1)
      WRITE_MODE_A => "READ_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
      WRITE_MODE_B => "READ_FIRST", -- "WRITE_FIRST", "READ_FIRST" or "NO_CHANGE" 
      READ_WIDTH_A => 32,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH_B => 32,   -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      WRITE_WIDTH_A => 32, -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
      WRITE_WIDTH_B => 32) -- Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
   port map (
      DOA => open,       -- Output port-A data, width defined by READ_WIDTH_A parameter
      DOB => ReTxQueue_DOBp,       -- Output port-B data, width defined by READ_WIDTH_B parameter
      ADDRA => ReTxQueue_wp,   -- Input port-A address, width defined by Port A depth
      ADDRB => ReTxQueue_ADDRB,   -- Input port-B address, width defined by Port B depth
      CLKA => clk2x,     -- 1-bit input port-A clock
      CLKB => clk2x,     -- 1-bit input port-B clock
      DIA => ReTxQueue_DIA,       -- Input port-A data, width defined by WRITE_WIDTH_A parameter
      DIB => ReTxQueue_DIB,       -- Input port-B data, width defined by WRITE_WIDTH_B parameter
      ENA => ReTxQueue_WEA,       -- 1-bit input port-A enable
      ENB => '1',       -- 1-bit input port-B enable
      REGCEA => '1', -- 1-bit input port-A output register enable
      REGCEB => '1', -- 1-bit input port-B output register enable
      RSTA => '0',     -- 1-bit input port-A reset
      RSTB => '0',     -- 1-bit input port-B reset
      WEA => x"f",       -- Input port-A write enable, width defined by Port A depth
      WEB => ReTxQueue_WEB        -- Input port-B write enable, width defined by Port B depth
   );
--ReTxQueue_DIB(15 downto 0) <= TSclock(27 downto 12);
ReTxQueue_DIB(15 downto 0) <= TSclock(23 downto 8);
ReTxQueue_WEB <= x"3" when save_ReTx = '1' else x"0";
ReTxQueue_ADDRB(9 downto 2) <= ReTxQueue_rp;
g_ReTx_ddr_wq: for i in 0 to 5 generate
	i_ReTx_ddr_wq : RAM32x6D PORT MAP(
		wclk => clk2x,
		rclk => clk2x,
		di => ReTx_ddr_wq_in(i*6+5 downto i*6),
		we => ReTx_ddr_wq_we,
		wa => ReTx_ddr_wq_wa,
		ra => ReTx_ddr_wq_ra,
		ceReg => ReTx_ddr_wq_ceReg,
		do => ReTx_ddr_out_i(i*6+5 downto i*6)
   );
end generate;
ReTx_ddr_wq_ceReg <= '1' when ReTx_ddr_out_vld = '0' or RETX_ddr_wq_re = '1' else '0';
--ReTx_ddr_wq_full <= ReTx_ddr_wq_wc(5);
ReTx_ddr_wq_full <= ReTx_ddr_wq_wc(4);
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		ReTxQueue_DOB <= ReTxQueue_DOBp;
		if(IS_CONNECTED = '0')then
			ReTx_ddr_wq_wc <= (others => '0');
		elsif(ReTx_ddr_wq_we = '1' and RETX_ddr_wq_re = '0')then
			ReTx_ddr_wq_wc <= ReTx_ddr_wq_wc + 1;
		elsif(ReTx_ddr_wq_we = '0' and RETX_ddr_wq_re = '1')then
			ReTx_ddr_wq_wc <= ReTx_ddr_wq_wc - 1;
		end if;
		if(IS_CONNECTED = '0')then
			ReTx_ddr_wq_wa <= (others => '0');
		elsif(ReTx_ddr_wq_we = '1')then
			ReTx_ddr_wq_wa <= ReTx_ddr_wq_wa + 1;
		end if;
		if(IS_CONNECTED = '0')then
			ReTx_ddr_wq_ra <= (others => '0');
		elsif(ReTx_ddr_wq_wa /= ReTx_ddr_wq_ra and ReTx_ddr_wq_ceReg = '1')then
			ReTx_ddr_wq_ra <= ReTx_ddr_wq_ra + 1;
		end if;
		if(IS_CONNECTED = '0')then
			ReTx_ddr_out_vld <= '0';
		elsif(ReTx_ddr_wq_wa /= ReTx_ddr_wq_ra)then
			ReTx_ddr_out_vld <= '1';
		elsif(RETX_ddr_wq_re = '1')then
			ReTx_ddr_out_vld <= '0';
		end if;
		if(fifo_en = '0')then
			ReTx_ddr_wrqst_i <= '0';
		elsif(ReTx_ddr_wq_wc(5 downto 4) /= "00" and sel_ddr = '1' and ReTx_ddr_full = '0')then
			ReTx_ddr_wrqst_i <= '1';
		else
			ReTx_ddr_wrqst_i <= '0';
		end if;
		if(IS_CONNECTED = '0')then
			ddr_wptr <= (others => '0');
		elsif(TCP_wcount = '1')then
			ddr_wptr <= ddr_wptr + 1;
		end if;
		ReTx_ddr_rrqst_q <= ReTx_ddr_rrqst_i;
		if(IS_CONNECTED = '0')then
			ddr_rptr <= (others => '0');
		elsif(ReTx_ddr_rrqst_i = '1' and ReTx_ddr_rrqst_q = '0')then
			ddr_rptr <= ddr_rptr + ("00000000000000" & ReTx_ddr_LEN_i);
		end if;
		entry_in_ddr <= ddr_wptr - ddr_rptr;
		if(ReTx_ddr_rrqst_i = '0')then
			if(or_reduce(entry_in_ddr(18 downto 5)) = '1' or entry_in_ddr(4 downto 0) >= ReTx_ddr_LEN_max)then
				ReTx_ddr_LEN_i <= ReTx_ddr_LEN_max;
			else
				ReTx_ddr_LEN_i <= '0' & entry_in_ddr(3 downto 0);
			end if;
		end if;
		if(IS_CONNECTED = '0' or sel_ddr = '0' or (ReTx_ddr_data_we = '1' and DDR2TCPdata(32) = '1'))then
			ReTx_ddr_rrqst_i <= '0';
		elsif(ReTxQueue_entry(8 downto 7) = "00" and or_reduce(entry_in_ddr) = '1')then
			ReTx_ddr_rrqst_i <= '1';
		end if;
		if(IS_CONNECTED = '0' or sel_ddr = '0')then
			ReTx_ddr_cnt <= (others => '0');
		elsif(re_ReTx_ddr_wq = '1' and ReTx_ddr_data_we = '0')then
			ReTx_ddr_cnt <= ReTx_ddr_cnt + 1;
		elsif(re_ReTx_ddr_wq = '0' and ReTx_ddr_data_we = '1')then
			ReTx_ddr_cnt <= ReTx_ddr_cnt - 1;
		end if;
--		ReTx_ddr_full <= and_reduce(ReTx_ddr_cnt(12 downto 4));
		ReTx_ddr_full <= and_reduce(ReTx_ddr_cnt(12 downto 5));
		ReTx_ddr_empty <= not (IS_CONNECTED and or_reduce(ReTx_ddr_cnt));
		if(IS_CONNECTED = '0')then
			ReTxQueue_entry <= (others => '0');
		elsif(ReTxQueue_wp(1 downto 0) = "01" and ReTxQueue_WEA = '1' and ce_ReTxQueue_rp = '0')then
			ReTxQueue_entry <= ReTxQueue_entry + 1;
		elsif((ReTxQueue_wp(1 downto 0) /= "01" or ReTxQueue_WEA = '0') and ce_ReTxQueue_rp = '1')then
			ReTxQueue_entry <= ReTxQueue_entry - 1;
		end if;
		if(IS_CONNECTED = '0')then
			ReTxQueue_wp <= (others => '0');
		elsif(ReTxQueue_WEA = '1')then
			ReTxQueue_wp <= ReTxQueue_wp + 1;
		end if;
		if(IS_CONNECTED = '0')then
			ReTxQueue_rp <= (others => '0');
		elsif(ce_ReTxQueue_rp = '1')then
			ReTxQueue_rp <= ReTxQueue_rp + 1;
		end if;
		if(IS_CONNECTED = '0')then
			ReTx_ddr_wq_re <= '0';
		elsif(sel_ddr = '1')then
			ReTx_ddr_wq_re <= re_RETX_ddr_wq;
		elsif(ReTx_ddr_wq_ra(1 downto 0) = "00")then
			ReTx_ddr_wq_re <= '0';
--		elsif(or_reduce(ReTx_ddr_wq_wc(5 downto 2)) = '1' and or_reduce(ReTxQueue_entry(8 downto N)) = '0')then
		elsif(or_reduce(ReTx_ddr_wq_wc(5 downto 2)) = '1' and and_reduce(ReTxQueue_entry(7 downto 4)) = '0')then
			ReTx_ddr_wq_re <= '1';
		end if;
		if(sel_ddr = '1')then
			ReTxQueue_DIA <= DDR2TCPdata(31 downto 0);
			ReTxQueue_WEA <= ReTx_ddr_data_we;
		else
			ReTxQueue_DIA <= ReTx_ddr_out_i(31 downto 0);
			ReTxQueue_WEA <= ReTx_ddr_wq_re;
		end if;
		if(IS_CONNECTED = '0')then
			sel_ddr <= '0';
		elsif(ReTx_ddr_wq_wc(4) = '1' and or_reduce(ReTxQueue_entry(8 downto N)) = '1')then
			sel_ddr <= '1';
		elsif(ReTx_ddr_wq_wc(5 downto 4) = "00" and ReTx_ddr_wq_re = '0' and ReTx_ddr_empty = '1')then
			sel_ddr <= '0';
		end if;
		EoB_toggleSyncRegs <= EoB_toggleSyncRegs(2 downto 0) & EoB_toggle;
		KiloByte_toggleSyncRegs <= KiloByte_toggleSyncRegs(2 downto 0) & KiloByte_toggle; 
-- Written_MonBuf is the number of blocks not yet acknowleged.
		if(IS_CONNECTED = '0')then
			Written_MonBuf <= (others => '0');
		elsif(EoB_toggleSyncRegs(3) /= EoB_toggleSyncRegs(2) and (update_UNA_Buf = '0' or ReTxQueue_DOB(31) = '0'))then
			Written_MonBuf <= Written_MonBuf + 1;
		elsif(EoB_toggleSyncRegs(3) = EoB_toggleSyncRegs(2) and update_UNA_Buf = '1' and ReTxQueue_DOB(31) = '1')then
			Written_MonBuf <= Written_MonBuf - 1;
		end if;
		if(IS_CONNECTED = '0' or EoB_toggleSyncRegs(3) /= EoB_toggleSyncRegs(2))then
			Written_MonBlock <= ADDR_offset & "00000";
		elsif(KiloByte_toggleSyncRegs(3) /= KiloByte_toggleSyncRegs(2))then
			Written_MonBlock <= Written_MonBlock + 1;
		end if;
		if(TS_OPTION = '1')then
--			RTT <= TS_RTT(27 downto 12);
			RTT <= TS_RTT(23 downto 8);
		else
			RTT <= STD_RTT;
		end if;
		if(IS_CONNECTED = '0')then
			ReTxEntry <= (others => '0');
		elsif(ReTx_ddr_wq_we = '1' and CTRLcntr(1 downto 0) = "01")then
			ReTxEntry <= ReTxEntry + 1;
		elsif(ce_ReTxQueue_rp = '1')then
			ReTxEntry <= ReTxEntry - 1;
		end if;
		if(FastRecovery = '0')then
			NewDataLimit <= ReTxEntry(11 downto 1);
		end if;
		if(FastRecovery = '0' or RTT_cycle = '1')then
			NewDataCntr <= NewDataLimit;
		elsif(ReTx_ddr_wq_we = '1' and CTRLcntr(1 downto 0) = "01")then
			NewDataCntr <= NewDataCntr - 1;
		end if;
		if(FastRecovery = '0')then
			SEGinFLIGHT <= '0' & ReTxEntry(11 downto 1);
		elsif(ReTx_ddr_wq_we = '1' and CTRLcntr(1 downto 0) = "01")then
			SEGinFLIGHT <= SEGinFLIGHT + 1;
		elsif(DupACK = '1' or NewDataACK = '1')then
			SEGinFLIGHT <= SEGinFLIGHT - 1;
		end if;
		if(FastRecovery = '0')then
			EnNewData <= '1';
		elsif(SEGinFLIGHT(11 downto 1) >= NewDataLimit)then
			EnNewData <= '0';
		end if;
		if(FastRecovery = '0')then
			RTT_cntr <= avg_RTT;
		elsif(strobe_us = '1')then
			if(or_reduce(RTT_cntr) = '0')then
				RTT_cntr <= avg_RTT;
			else
				RTT_cntr <= RTT_cntr - 1;
			end if;
		end if;
		if(FastRecovery = '0' or (strobe_us = '1' and or_reduce(RTT_cntr) = '0'))then
			RTT_cycle <= '1';
		else
			RTT_cycle <= '0';
		end if;
	end if;
end process;
i_RTO_CALC: RTO_CALC PORT MAP(
		clk => clk2x,
		RTT => RTT,
		RTOmin => RTOmin,
		LISTEN => LISTENing,
		RTO_backoff => set_ReTx_TO,
		sample_RTT => sample_RTT(2),
		RTO => RTO,
		debug => debug_RTO
	);
i_TCP_CC: TCP_CC PORT MAP(
		clk => clk2x,
		reset => LISTENing,
		SYNRCVD => wasSYNRCVD,
		ReTx_TO => ReTx_TO,
		Save_ReTx => RTT_cycle,
		FastReTxStart => FastReTxStart,
		FR => FastRecovery,
		NewDataACK => NewDataACK,
		PartialACK => PartialACK(0),
		DupACK => DupACK,
		MSS => MSS(15 downto 0),
--		CWND_max => CWND_max,
		SEG_WND => SEG_WND,
		SND_UNA => SND_UNA,
		SND_NXT => SND_NXT,
		SND_WND_UL => SND_WND_UL_CC,
		CWND => CWND,
		debug => debug_CC
	);
process(clk2x)
begin
	if(clk2x'event and clk2x = '1')then
		if(strobe_us = '1' and time_cntr = "11110100001000110")then
			time_sample <= '1';
		else
			time_sample <= '0';
		end if;
		if(time_sample = '1')then
			time_cntr <= (others => '0');
		elsif(strobe_us = '1')then
			time_cntr <= time_cntr + 1;
		end if;
		if(time_sample = '1')then
			rate_cntr <= (others => '0');
			rate <= rate_cntr;
		elsif(dataFIFO_WREN = '1')then
			rate_cntr <= rate_cntr + 1;
		end if;
		if(time_sample = '1')then
			deadtime_cntr <= (others => '0');
			deadtime <= deadtime_cntr(25 downto 10);
		elsif(CTRLstate = IDLE and Data2Send = '1' and SND_SPACE_OK = '0' and ClientEmacTxd_sel(1) = '0')then
			if(deadtime_cntr(9 downto 4) = "100111")then
				deadtime_cntr(9 downto 0) <= (others => '0');
				deadtime_cntr(25 downto 10) <= deadtime_cntr(25 downto 10) + 1;
			else
				deadtime_cntr(9 downto 0) <= deadtime_cntr(9 downto 0) + 1;
			end if;
		end if;
		if(sample_RTT(2) = '1')then
			sample_cntr <= sample_cntr + 1;
			if(sample_cntr = x"00")then
				avg_RTT <= sum_RTT(35 downto 8);
				sum_RTT <= (others => '0');
			else
				sum_RTT <= sum_RTT + TS_RTT;
			end if;
		end if;
		case TCPstate is
			when LISTEN => TCPstates <= "001";
			when SYNRCVD => TCPstates <= "010";
			when ESTAB => TCPstates <= "011";
			when LASTACK => TCPstates <= "100";
			when others => TCPstates <= "000";
		end case;
		case CTRLstate is
			when RCV_IPHDR => CTRLstates <= x"1";
			when RCV_TCPHDR => CTRLstates <= x"2";
			when RCV_PROCESS => CTRLstates <= x"3";
			when SND => CTRLstates <= x"4";
			when WAIT4SND2END => CTRLstates <= x"5";
			when RELEASE => CTRLstates <= x"6";
			when RETX => CTRLstates <= x"7";
			when SND_ARP_ICMP => CTRLstates <= x"8";
			when others => CTRLstates <= x"0";
		end case;
		case DATAstate is
			when ReadEventData => DATAstates <= "01";
			when Wait4Send => DATAstates <= "10";
			when others => DATAstates <= "00";
		end case;
		if(fifo_en = '0')then
			ReTx_FIFO_WrError <= '0';
		elsif(ReTx_FIFO_WrErr = '1')then
			ReTx_FIFO_WrError <= '1';
		end if;
		if(fifo_en = '0' or IS_CONNECTED = '0')then
			ReTx_FIFO_RdError <= '0';
		elsif(ReTxData_re = '1' and ReTx_FIFO_empty = '1')then
			ReTx_FIFO_RdError <= '1';
		end if;
	end if;
end process;
process(clk2x,rstCntr)
begin
	if(rstCntr = '1')then
		ARP_rcvd_cntr <= (others => '0');
		ARP_sent_cntr <= (others => '0');
		ICMP_rcvd_cntr <= (others => '0');
		ICMP_sent_cntr <= (others => '0');
		ReTxData_wc <= (others => '0');
		en_RxDout_cntr <= (others => '0');
		Tack_cntr <= (others => '0');
		DataSeg_cntr <= (others => '0');
		ReTx_DataSeg_cntr <= (others => '0');
		FastReTx_cntr <= (others => '0');
		Save_ReTx_cntr <= (others => '0');
		ReTxDataACK_cntr <= (others => '0');
		ReTxNXT_cntr <= (others => '0');
		ReTx_DataErr_cntr <= (others => '0');
		bad_ReTx_LENCntr <= (others => '0');
		bad_ReTx_chksumCntr <= (others => '0');
		DupAckCntr <= (others => '0');
		NewAckCntr <= (others => '0');
	elsif(clk2x'event and clk2x = '1')then
		if(RxDout_type = "11" and en_RxDout = '1')then
			ARP_rcvd_cntr <= ARP_rcvd_cntr + 1;
		end if;
		if(RxDout_type = "01" and en_RxDout = '1')then
			ICMP_rcvd_cntr <= ICMP_rcvd_cntr + 1;
		end if;
		if(inc_ARP_sent = '1')then
			ARP_sent_cntr <= ARP_sent_cntr + 1;
		end if;
		if(inc_ICMP_sent = '1')then
			ICMP_sent_cntr <= ICMP_sent_cntr + 1;
		end if;
		if(ReTxData_we /= "00")then
			ReTxData_wc <= ReTxData_wc + 1;
		end if;
		if(en_RxDout = '1')then
			en_RxDout_cntr <= en_RxDout_cntr + 1;
		end if;
		if(EmacClientTack = '1')then
			Tack_cntr <= Tack_cntr + 1;
		end if;
		if(ClientEmacTxd_sel = "01" and sel_ReTx_DATA = '0')then
			DataSeg_cntr <= DataSeg_cntr + 1;
		end if;
		if(ClientEmacTxd_sel = "01" and sel_ReTx_DATA = '1')then
			ReTx_DataSeg_cntr <= ReTx_DataSeg_cntr + 1;
		end if;
		if(save_ReTx = '1' and FastReTx = '1')then
			FastReTx_cntr <= FastReTx_cntr + 1;
		end if;
		if(save_ReTx = '1')then
			save_ReTx_cntr <= save_ReTx_cntr + 1;
		end if;
		if(ReTxDataACK = '1')then
			ReTxDataACK_cntr <= ReTxDataACK_cntr + 1;
		end if;
		if(save_ReTx = '1' and ReTxNXT = '1')then
			ReTxNXT_cntr <= ReTxNXT_cntr + 1;
		end if;
		if(bad_ReTx_LEN = '1')then
			bad_ReTx_LENCntr <= bad_ReTx_LENCntr + 1;
		end if;
		if(bad_ReTx_chksum = '1')then
			bad_ReTx_chksumCntr <= bad_ReTx_chksumCntr + 1;
		end if;
		if(bad_ReTx_chksum = '1' or bad_ReTx_LEN = '1')then
			ReTx_DataErr_cntr <= ReTx_DataErr_cntr + 1;
		end if;
		if(DupACK = '1')then
			DupAckCntr <= DupAckCntr + 1;
		end if;
		if(NewDataACK = '1')then
			NewAckCntr <= NewAckCntr + 1;
		end if;
	end if;
end process;
process(ipb_addr)
begin
	if(ipb_addr(9) = '0')then
		ipb_rdata <= EMAC_Rx_rdata;
	elsif(ipb_addr(5) = '0')then
		ipb_rdata <= status(conv_integer(ipb_addr(4 downto 0)));
	else
		ipb_rdata <= statusb(conv_integer(ipb_addr(4 downto 0)));
	end if;
end process;
--process(clk2x)
--begin
--	if(clk2x'event and clk2x = '1')then
--		if(TSclock(15 downto 0) = x"0000")then
			statusb(0) <= en_RxDout_cntr;
			statusb(1) <= ARP_sent_cntr & ARP_rcvd_cntr;
			statusb(2) <= ICMP_sent_cntr & ICMP_rcvd_cntr;
			statusb(3) <= DupAckCntr;
			statusb(4) <= FastReTx_cntr;
			statusb(5) <= ReTx_DataSeg_cntr;
			statusb(6) <= DataSeg_cntr;
			statusb(7) <= NewAckCntr;
			statusb(8) <= ReTxNXT_Cntr;
			statusb(9) <= x"00000" & '0' & Written_MonBuf;
			statusb(10) <= "0000000" & ReTxDataLastAddr(15 downto 7) & "0000000" & Written_MonBlock;
			statusb(11) <= "000" & ReTxDataLEN_i & x"0" & CLOSE_flag & "000" & SEG_CTRL;
			statusb(12) <= "000000" & ReTxDataAddr_i;
			statusb(13) <= x"000" & '0' & ddr_wptr;
			statusb(14) <= x"000" & '0' & ddr_rptr;
			statusb(15) <= x"0000" & "000" & ReTx_ddr_cnt;
			statusb(16) <= input_SEG_WND1 & input_SEG_WND0;
			statusb(17) <= x"000" & SND_WND_SCALE & input_SEG_WND2;
			statusb(18) <= SEG_WND;
			statusb(19) <= SND_WL1;
			statusb(20) <= SND_WL2;
			statusb(21) <= Save_ReTX_cntr & ReTxDataACK_cntr;
			statusb(22) <= delta_rate & "00" & wc;
			status(0) <= SND_UNA;
			status(1) <= SND_NXT;
			status(2) <= SND_WND;
			status(3) <= CWND;
			status(4) <= SND_SEQ;
			status(5) <= SND_ACK;
			status(6) <= SEG_SEQ;
			status(7) <= SEG_ACK;
			status(8) <= ReTx_SEQ;
--		end if;
		status(9) <= TSclock;
--		if(TSclock(15 downto 0) = x"0000")then
			status(10) <= ReTxQueue_DOB;
			status(11) <= MSS;
			status(12) <= RTO_timer & RTO;
			status(13) <= "000" & ReTxQueue_entry  & ReTxQueue_rp & ReTxQueue_wp & "00";
			status(14) <= SND_WND_UL;
			status(15) <= Tack_cntr;
			status(16) <= '0' & Rate & "000000";
			status(17) <= x"000" & deadtime & x"0";
			status(18) <= ISS;
			status(19) <= IRS;
			status(20) <= "000" & data_wc & DATA_SIZE(15 downto 0);
			status(21) <= SND_WND_UL_CC;
			status(22) <= ReTxData_wc;
			status(23) <= x"0" & avg_RTT;
			status(24) <= "00" & ReTxDataCntr & ReTx_DataErr_cntr;
			status(25) <= recover;
			status(26) <= bad_ReTx_chksumCntr & bad_ReTx_LENCntr;
			status(27) <= bad_LEN_pair;
			status(28) <= bad_chksum_pair;
			status(31) <= OPTION_rdy & "000000000" & ReTx_ddr_wq_wc & EventBufAddr_wa(1 downto 0) & EventBufAddr_ra(1 downto 0) & '0' & saved_data_wc;
-- status(29)
			status(29)(31) <= save_ReTx;
			status(29)(30) <= ReTx_TO;
			status(29)(29) <= ReTx_FIFO_empty;
			status(29)(28) <= rate_pause;
--			status(29)(27) <= ReTx_ddr_wq_full;
			status(29)(26) <= WaitReTxData;
			status(29)(25) <= SendReTxData;
			status(29)(22) <= ReTx_ddr_empty;
			status(29)(21) <= FastReTx;
			status(29)(20) <= sel_ddr;
			status(29)(19) <= ReleasePending;
			status(29)(18) <= ReTxentry_avl;
			status(29)(17) <= ReTx_ddr_full;
			status(29)(16) <= ReTxData_avl;
--			status(29)(15) <= FastRecovery;
			status(29)(14) <= ReTx_FIFO_WrError;
			status(29)(13) <= ReTx_FIFO_RdError;
			status(29)(11) <= EnNewData;
			status(29)(10) <= SND_SPACE_OK;
			status(29)(9) <= SND_SPACE_CC_OK;
			status(29)(8) <= Data2Send;
			status(29)(7) <= headerFIFO_full;
			status(29)(6) <= ReTx_ddr_wq_full;
			status(29)(5) <= FastRecovery;
			status(29)(4) <= TO_Recovery;
			status(29)(2 downto 0) <= ReTx_cnt;
-- status(30)
			status(30)(31 downto 29) <= ReTxDataRqst_i;
--			status(30)(30) <= Data2Send;
--			status(30)(29) <= SND_SPACE_CC_OK;
			status(30)(28) <= AttachData;
			status(30)(27) <= dataFIFO_empty;
			status(30)(26) <= headerFIFO_EMPTY;
--			status(30)(25) <= headerFIFO_full;
			status(30)(24) <= dataFIFO_full;
			status(30)(23 downto 19) <= ReTx_CTRL;
			status(30)(18 downto 14) <= SND_CTRL(4 downto 0);
			status(30)(13 downto 12) <= ClientEmacTxd_sel;
			status(30)(11) <= rdy2send;
			status(30)(10) <= RxDout_avl;
			status(30)(9) <= EVENTdata_avl;
			status(30)(8 downto 7) <= DATAstates;
			status(30)(6 downto 4) <= TCPstates;
			status(30)(3 downto 0) <= CTRLstates;
--		end if;
--	end if;
--end process;
end Behavioral;

