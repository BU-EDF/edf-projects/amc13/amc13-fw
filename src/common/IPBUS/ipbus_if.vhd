----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:55:15 07/09/2010 
-- Design Name: 
-- Module Name:    S6LINK_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.ipbus.ALL;
use work.ipbus_trans_decl.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity ipbus_if is
	generic(RXPOLARITY : std_logic := '0'; TXPOLARITY : std_logic := '0');
	port(
		ipb_clk																	: IN std_logic;
		UsRclk																	: IN std_logic;
		DRPclk																	: IN std_logic;
		reset 																	: IN std_logic;
		got_SN																	: out std_logic;
		GTX_RESET															  : IN std_logic;
    GbE_REFCLK	                       	  	: in std_logic;
		wr_amc_en																: in std_logic;
		en_RARP																	: in std_logic;
    amc_en																	: in  STD_LOGIC_VECTOR(11 downto 0);
    IPADDR																	: in  STD_LOGIC_VECTOR(31 downto 0);
    MACADDR																	: in  STD_LOGIC_VECTOR(47 downto 0);
    S6LINK_RXN                            	: in   std_logic;
    S6LINK_RXP                            	: in   std_logic;
    S6LINK_TXN                            	: out  std_logic;
    S6LINK_TXP                            	: out  std_logic;
	  ipb_out																	: out ipb_wbus;
	  ipb_in																	: in ipb_rbus;
    SN																			: out  STD_LOGIC_VECTOR(8 downto 0);
		debug_in																: IN std_logic_vector(31 downto 0);
		debug_out																: OUT std_logic_vector(127 downto 0)
		);
end ipbus_if;

architecture Behavioral of ipbus_if is
COMPONENT ipbus_ctrl
	generic(
		MAC_CFG: ipb_mac_cfg := EXTERNAL;
		IP_CFG: ipb_ip_cfg := EXTERNAL;
-- Number of address bits to select RX or TX buffer in UDP I/F
-- Number of RX and TX buffers is 2**BUFWIDTH
		BUFWIDTH: natural := 4;
-- Numer of address bits to select internal buffer in UDP I/F
-- Number of internal buffers is 2**INTERNALWIDTH
		INTERNALWIDTH: natural := 1;
-- Number of address bits within each buffer in UDP I/F
-- Size of each buffer is 2**ADDRWIDTH
		ADDRWIDTH: natural := 11;
-- UDP port for IPbus traffic in this instance of UDP I/F
		IPBUSPORT: std_logic_vector(15 DOWNTO 0) := x"C351";
-- Flag whether this UDP I/F instance ignores everything except IPBus traffic
		SECONDARYPORT: std_logic := '0';
		N_OOB: natural := 0
	);
	port(
		mac_clk: in std_logic; -- Ethernet MAC clock (125MHz)
		rst_macclk: in std_logic; -- MAC clock domain sync reset
		ipb_clk: in std_logic; -- IPbus clock
		rst_ipb: in std_logic; -- IPbus clock domain sync reset
		mac_rx_data: in std_logic_vector(7 downto 0); -- AXI4 style MAC signals
		mac_rx_valid: in std_logic;
		mac_rx_last: in std_logic;
		mac_rx_error: in std_logic;
		mac_tx_data: out std_logic_vector(7 downto 0);
		mac_tx_valid: out std_logic;
		mac_tx_last: out std_logic;
		mac_tx_error: out std_logic;
		mac_tx_ready: in std_logic;
		ipb_out: out ipb_wbus; -- IPbus bus signals
		ipb_in: in ipb_rbus;
		ipb_req: out std_logic;
		ipb_grant: in std_logic := '1';
		mac_addr: in std_logic_vector(47 downto 0) := X"000000000000"; -- Static MAC and IP addresses
		ip_addr: in std_logic_vector(31 downto 0) := X"00000000";
		enable: in std_logic := '1';
		RARP_select: in std_logic := '0';
		pkt_rx: out std_logic;
		pkt_tx: out std_logic;
		pkt_rx_led: out std_logic;
		pkt_tx_led: out std_logic;
		oob_in: in ipbus_trans_in_array(N_OOB - 1 downto 0) := (others => ('0', X"00000000", '0'));
		oob_out: out ipbus_trans_out_array(N_OOB - 1 downto 0)
		);
END COMPONENT;
component S6Link_init 
generic
(
    -- Simulation attributes
    EXAMPLE_SIM_GTRESET_SPEEDUP    : string    := "FALSE";    -- Set to 1 to speed up sim reset
    EXAMPLE_SIMULATION             : integer   := 0;          -- Set to 1 for simulation
    EXAMPLE_USE_CHIPSCOPE          : integer   := 0           -- Set to 1 to use Chipscope to drive resets

);
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_IN                           : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y15)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    GT0_CPLLFBCLKLOST_OUT                   : out  std_logic;
    GT0_CPLLLOCK_OUT                        : out  std_logic;
    GT0_CPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_CPLLRESET_IN                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    GT0_GTREFCLK0_IN                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    GT0_DRPADDR_IN                          : in   std_logic_vector(8 downto 0);
    GT0_DRPCLK_IN                           : in   std_logic;
    GT0_DRPDI_IN                            : in   std_logic_vector(15 downto 0);
    GT0_DRPDO_OUT                           : out  std_logic_vector(15 downto 0);
    GT0_DRPEN_IN                            : in   std_logic;
    GT0_DRPRDY_OUT                          : out  std_logic;
    GT0_DRPWE_IN                            : in   std_logic;
    --------------------- RX Initialization and Reset Ports --------------------
    GT0_RXUSERRDY_IN                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    GT0_EYESCANDATAERROR_OUT                : out  std_logic;
    ------------------------- Receive Ports - CDR Ports ------------------------
    GT0_RXCDRLOCK_OUT                       : out  std_logic;
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    GT0_RXUSRCLK_IN                         : in   std_logic;
    GT0_RXUSRCLK2_IN                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    GT0_RXDATA_OUT                          : out  std_logic_vector(15 downto 0);
    ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
    GT0_RXDISPERR_OUT                       : out  std_logic_vector(1 downto 0);
    GT0_RXNOTINTABLE_OUT                    : out  std_logic_vector(1 downto 0);
    --------------------------- Receive Ports - RX AFE -------------------------
    GT0_GTXRXP_IN                           : in   std_logic;
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    GT0_GTXRXN_IN                           : in   std_logic;
    -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
    GT0_RXMCOMMAALIGNEN_IN                  : in   std_logic;
    GT0_RXPCOMMAALIGNEN_IN                  : in   std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    GT0_RXDFELPMRESET_IN                    : in   std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    GT0_GTRXRESET_IN                        : in   std_logic;
    GT0_RXPMARESET_IN                       : in   std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    GT0_RXPOLARITY_IN                       : in   std_logic;
    ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
    GT0_RXCHARISK_OUT                       : out  std_logic_vector(1 downto 0);
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    GT0_RXRESETDONE_OUT                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    GT0_GTTXRESET_IN                        : in   std_logic;
    GT0_TXUSERRDY_IN                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    GT0_TXUSRCLK_IN                         : in   std_logic;
    GT0_TXUSRCLK2_IN                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    GT0_TXDATA_IN                           : in   std_logic_vector(15 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    GT0_GTXTXN_OUT                          : out  std_logic;
    GT0_GTXTXP_OUT                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    GT0_TXOUTCLK_OUT                        : out  std_logic;
    GT0_TXOUTCLKFABRIC_OUT                  : out  std_logic;
    GT0_TXOUTCLKPCS_OUT                     : out  std_logic;
    --------------------- Transmit Ports - TX Gearbox Ports --------------------
    GT0_TXCHARISK_IN                        : in   std_logic_vector(1 downto 0);
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    GT0_TXRESETDONE_OUT                     : out  std_logic;
    ----------------- Transmit Ports - TX Polarity Control Ports ---------------
    GT0_TXPOLARITY_IN                       : in   std_logic;
   

    --____________________________COMMON PORTS________________________________
    ---------------------- Common Block  - Ref Clock Ports ---------------------
    GT0_GTREFCLK0_COMMON_IN                 : in   std_logic;
    ------------------------- Common Block - QPLL Ports ------------------------
    GT0_QPLLLOCK_OUT                        : out  std_logic;
    GT0_QPLLLOCKDETCLK_IN                   : in   std_logic;
    GT0_QPLLRESET_IN                        : in   std_logic


);
end component;
signal CPLLLOCK : std_logic := '0';
signal CPLLLOCK_n : std_logic := '0';
signal S6LinkRdy_n : std_logic := '1';
signal S6LINK_TxOutClk : std_logic := '0';
signal S6LINK_RxCharIsComma : std_logic_vector(1 downto 0) := (others => '0');
signal S6LINK_RxCharIsK : std_logic_vector(1 downto 0) := (others => '0');
signal S6LINK_RxNotInTable : std_logic_vector(1 downto 0) := (others => '0');
signal S6LINK_RxDataOut : std_logic_vector(15 downto 0) := (others => '0');
signal S6LINK_TxCharIsK : std_logic_vector(1 downto 0) := (others => '0');
signal S6LINK_TxDataIn : std_logic_vector(15 downto 0) := (others => '0');
signal S6LINK_RxResetDone : std_logic := '0';
signal S6LINK_RxResetDoneSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal S6LINK_TxReset : std_logic := '0';
signal S6LINK_DiffCtrl : std_logic_vector(3 downto 0) := x"9"; -- 790mV drive
signal startTx : std_logic := '0';
signal sending : std_logic := '0';
signal enTx : std_logic := '1';
signal got_SNp : std_logic := '1';
signal SN_T2: std_logic_vector(8 downto 0) := (others => '0');
constant M : integer := 10;
signal SN_cntr: std_logic_vector(M downto 0) := (others => '0');
signal ipbus_txready : std_logic := '0';
signal ipbus_rxdlast : std_logic := '0';
signal ipbus_rxerr : std_logic := '0';
signal ipbus_txdvld : std_logic := '0';
signal ipbus_txdvld_q : std_logic := '0';
signal receive : std_logic := '0';
signal send_amc_en : std_logic := '0';
signal wr_amc_en_SyncRegs: std_logic_vector(3 downto 0) := (others => '0');
signal cmd: std_logic_vector(7 downto 0) := (others => '0');
signal cmd_cntr: std_logic_vector(2 downto 0) := (others => '0');
signal ipbus_rxdvld : std_logic := '0';
signal div: std_logic_vector(3 downto 0) := (others => '0');
signal tx_header: std_logic_vector(1 downto 0) := (others => '0');
signal ipbus_txdlast: std_logic_vector(3 downto 0) := (others => '0');
signal ipbus_txerr: std_logic_vector(3 downto 0) := (others => '0');
signal AXI4_bufA: std_logic_vector(7 downto 0) := (others => '0');
signal AXI4_bufB: std_logic_vector(7 downto 0) := (others => '0');
signal ipbus_rxd: std_logic_vector(7 downto 0) := (others => '0');
signal ipbus_txd: std_logic_vector(7 downto 0) := (others => '0');
signal DATA_VALID : std_logic := '0';
signal rxfsmresetdone : std_logic := '0';
signal txfsmresetdone : std_logic := '0';
signal rst_ipbus_ctrl : std_logic := '0';
signal oldRxData15 : std_logic := '0';
--component icon2
--  PORT (
--    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
--    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

--end component;
--component ila64x4096
--  PORT (
--    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
--    CLK : IN STD_LOGIC;
--    DATA : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    TRIG0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0));
--end component;
--signal cs0_control : std_logic_vector(35 downto 0) := (others => '0');
--signal cs1_control : std_logic_vector(35 downto 0) := (others => '0');
--signal cs0_data : std_logic_vector(63 downto 0) := (others => '0');
--signal cs1_data : std_logic_vector(63 downto 0) := (others => '0');
--signal cs0_trig : std_logic_vector(7 downto 0) := (others => '0');
--signal cs1_trig : std_logic_vector(7 downto 0) := (others => '0');
begin
--icon : icon2
--  port map (
--    CONTROL0 => cs0_control,
--    CONTROL1 => cs1_control);
--    cs0 : ila64x4096
--      port map (
--        CONTROL => cs0_control,
--        CLK => ipb_clk,
--        DATA => cs0_data,
--        TRIG0 => cs0_trig);
--    cs1 : ila64x4096
--      port map (
--        CONTROL => cs1_control,
--        CLK => UsRclk,
--        DATA => cs1_data,
--        TRIG0 => cs1_trig);
--    cs0_data(0) <= S6LinkRdy_n;
--    cs0_data(1) <= ipb_in.ipb_ack;
----    cs0_data(2) <= ipb_out_i.ipb_write;
----    cs0_data(3) <= ipb_out_i.ipb_strobe;
----    cs0_data(35 downto 4) <= ipb_out_i.ipb_addr;
--    cs0_data(63 downto 36) <= (others => '0');
----    cs0_trig(0) <= ipb_out_i.ipb_strobe;
--    cs0_trig(7 downto 1) <= (others => '0');
--    cs1_data(15 downto 0) <= S6LINK_RxDataOut;
--    cs1_data(31 downto 16) <= S6LINK_TxDataIn;
--    cs1_data(33 downto 32) <= S6LINK_RxCharIsK;
--    cs1_data(35 downto 34) <= S6LINK_TxCharIsK;
----    cs1_data(40 downto 36) <= SN_cntr;
--    cs1_data(41) <= rxfsmresetdone;
--    cs1_data(42) <= txfsmresetdone;
--    cs1_data(43) <= DATA_VALID;
--    cs1_data(44) <= receive;
----    cs1_data(45) <= rxgoodframe;
----    cs1_data(46) <= rxbadframe;
----    cs1_data(47) <= ipbus_txack;
--    cs1_data(48) <= startTx;
--    cs1_data(49) <= ipbus_txdvld;
--    cs1_data(50) <= sending;
--    cs1_data(51) <= enTx;
--    cs1_data(52) <= S6LINK_RxResetDone;
--    cs1_data(60 downto 53) <= SN_T2;
----    cs1_data(63 downto 61) <= S6LinkCntr(2 downto 0);
--    cs1_trig(1 downto 0) <= S6LINK_RxCharIsK;
--    cs1_trig(2) <= receive;
--    cs1_trig(7 downto 3) <= (others => '0');
debug_out(127 downto 0) <= (others => '0');
rst_ipbus_ctrl <= not got_SNp or reset;
i_ipbus_ctrl: ipbus_ctrl port map(
		mac_clk => UsrClk,
		rst_macclk => rst_ipbus_ctrl,
		ipb_clk => ipb_clk,
		rst_ipb => rst_ipbus_ctrl,
		mac_rx_data => ipbus_rxd,
		mac_rx_valid => ipbus_rxdvld,
		mac_rx_last => ipbus_rxdlast,
		mac_rx_error => ipbus_rxerr,
		mac_tx_data => ipbus_txd,
		mac_tx_valid => ipbus_txdvld,
		mac_tx_last => ipbus_txdlast(0),
		mac_tx_error => ipbus_txerr(0),
		mac_tx_ready => ipbus_txready,
		ipb_out => ipb_out,
		ipb_in => ipb_in,
		ipb_req => open,
		ipb_grant => '1',
		mac_addr => MACADDR,
		ip_addr => IPADDR,
		enable => '1',
		RARP_select => en_RARP,
		pkt_rx => open,
		pkt_tx => open,
		pkt_rx_led => open,
		pkt_tx_led => open,
		oob_in => (others => ('0', X"00000000", '0')),
		oob_out => open
	);
process(UsrClk,S6LINK_RxResetDone)
begin
	if(S6LINK_RxResetDone = '0')then
		S6LINK_RxResetDoneSyncRegs <= (others => '0');
	elsif(UsrClk'event and UsrClk = '1')then
		S6LINK_RxResetDoneSyncRegs <= S6LINK_RxResetDoneSyncRegs(1 downto 0) & '1';
	end if;
end process;
process(ipb_clk,S6LINK_RxResetDone)
begin
	if(S6LINK_RxResetDone = '0')then
		S6LinkRdy_n <= '1';
	elsif(ipb_clk'event and ipb_clk = '1')then
		if(rxfsmresetdone = '1' and txfsmresetdone = '1')then
			S6LinkRdy_n <= '0';
		end if;
	end if;
end process;
ipbus_rxerr <= '1' when S6LINK_RxCharIsK(0) = '1' and receive = '1' and S6LINK_RxDataOut(7 downto 0) /= x"f7" else '0';
ipbus_rxdlast <= receive and S6LINK_RxCharIsK(0);
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		if(S6LINK_RxResetDoneSyncRegs(2) = '0' or or_reduce(S6LINK_RxNotInTable) = '1')then
			DATA_VALID <= '0';
		elsif(S6LINK_RxCharIsK(0) = '1' and S6LINK_RxDataOut(7 downto 0) = x"bc")then
			DATA_VALID <= '1';
		end if;
		ipbus_rxd <= S6LINK_RxDataOut(7 downto 0);
		ipbus_rxdvld <= receive and not S6LINK_RxCharIsK(0);
		if(S6LINK_RxResetDoneSyncRegs(2) = '0' or got_SNp = '0')then
			receive <= '0';
		elsif(S6LINK_RxCharIsK(0) = '1')then
			if(S6LINK_RxDataOut(7 downto 0) = x"3c")then
				receive <= '1';
			else
				receive <= '0';
			end if;
		end if;
		if(S6LINK_RxResetDoneSyncRegs(2) = '0' or ipbus_txdvld = '0')then
			tx_header <= "00";
		elsif(tx_header /= "11")then
			tx_header <= tx_header + 1;
		end if;
		if(S6LINK_RxResetDoneSyncRegs(2) = '0')then
			ipbus_txready <= '0';
		elsif((tx_header(1) = '0' and ipbus_txdvld = '1') or startTx = '1')then
			ipbus_txready <= '1';
		elsif((tx_header = "10" and sending = '0') or (ipbus_txdlast(2) = '1' and sending = '1'))then
			ipbus_txready <= '0';
		end if;
		ipbus_txdlast(3 downto 1) <= ipbus_txdlast(2 downto 0);
		ipbus_txerr(3 downto 1) <= ipbus_txerr(2 downto 0);
		if(ipbus_txready = '1' or sending = '1')then
			AXI4_bufA <= ipbus_txd;
			AXI4_bufB <= AXI4_bufA;
		end if;
		startTx <= enTx and tx_header(1) and not sending and not startTx;
		if(ipbus_txdlast(2) = '1' and sending = '1')then
			sending <= '0';
		elsif(startTx = '1')then
			sending <= '1';
		end if;
		if(startTx = '1' or sending = '1')then
			enTx <= '0';
		elsif(S6LINK_RxCharIsK(1) = '1' and S6LINK_RxDataOut(15 downto 8) = x"f7")then
			enTx <= '1';
		end if;
		if(SN_cntr(M) = '0')then
			SN <= "111111111";
		else
			SN <= SN_T2;
		end if;
		if(S6LinkRdy_n = '1')then
			got_SNp <= '0';
			got_SN <= '0';
		else
			got_SNp <= SN_cntr(M);
			got_SN <= got_SNp;
		end if;
		if(S6LinkRdy_n = '1')then
			SN_cntr <= (others => '0');
		elsif(S6LINK_RxCharIsK = "01" and SN_cntr(M) = '0' and S6LINK_RxDataOut(7 downto 0) = x"bc")then
		    oldRxData15 <= S6LINK_RxDataOut(15);
		    if(S6LINK_RxDataOut(15) = oldRxData15)then
			  SN_T2 <= '1' & S6LINK_RxDataOut(15 downto 8);
		    elsif(S6LINK_RxDataOut(15) = '0')then
			  SN_T2(6 downto 0) <= S6LINK_RxDataOut(14 downto 8);
			else
			  SN_T2(8 downto 7) <= S6LINK_RxDataOut(9 downto 8);
			end if;
			if(SN_T2(7 downto 0) = S6LINK_RxDataOut(15 downto 8))then
                SN_cntr <= SN_cntr + 1;
			elsif(S6LINK_RxDataOut(15) = '0' and SN_T2(6 downto 0) = S6LINK_RxDataOut(14 downto 8))then
                    SN_cntr <= SN_cntr + 1;
			elsif(S6LINK_RxDataOut(15) = '1' and SN_T2(8 downto 7) = S6LINK_RxDataOut(9 downto 8))then
                SN_cntr <= SN_cntr + 1;
			else
				SN_cntr <= (others => '0');
			end if;
		end if;
		if(startTx = '1')then
			S6LINK_TxCharIsK(0) <= '1';
			S6LINK_TxDataIn(7 downto 0) <= x"3c"; -- K28.1
		elsif(sending = '1')then
			S6LINK_TxCharIsK(0) <= '0';
			S6LINK_TxDataIn(7 downto 0) <= AXI4_bufB;
		elsif(ipbus_txdlast(3) = '1')then
			S6LINK_TxCharIsK(0) <= '1';
			if(ipbus_txerr(3) = '0')then
				S6LINK_TxDataIn(7 downto 0) <= x"bc"; -- K28.5
			else
				S6LINK_TxDataIn(7 downto 0) <= x"f7"; -- txerr
			end if;
		else
			if(div(3) = '1')then
				S6LINK_TxCharIsK(0) <= '1';
				S6LINK_TxDataIn(7 downto 0) <= x"bc"; -- K28.5
				div <= (others => '0');
			else
				S6LINK_TxCharIsK(0) <= '0';
				S6LINK_TxDataIn(7 downto 0) <= x"00";
				div <= div + 1;
			end if;
		end if;
		wr_amc_en_SyncRegs <= wr_amc_en_SyncRegs(2 downto 0) & wr_amc_en;
		if(wr_amc_en_SyncRegs(3 downto 2) = "01")then
			send_amc_en <= '1';
		elsif(cmd_cntr = "101")then
			send_amc_en <= '0';
		end if;
		if(send_amc_en = '0')then
			cmd_cntr <= (others => '0');
		else
			cmd_cntr <= cmd_cntr + 1;
		end if;
		if(send_amc_en = '1')then
			cmd <= x"a5";
		else
			cmd <= x"00";
		end if;
		if(cmd_cntr = "001" or cmd_cntr = "101")then
			S6LINK_TxCharIsK(1) <= '1';
		else
			S6LINK_TxCharIsK(1) <= '0';
		end if;
		case cmd_cntr is
			when "001" => S6LINK_TxDataIn(15 downto 8) <= x"fe"; -- K30.7
			when "010" => S6LINK_TxDataIn(15 downto 8) <= cmd; -- write AMC_en command
			when "011" => S6LINK_TxDataIn(15 downto 8) <= amc_en(7 downto 0);
			when "100" => S6LINK_TxDataIn(15 downto 8) <= x"0" & amc_en(11 downto 8);
			when "101" => S6LINK_TxDataIn(15 downto 8) <= x"f7"; -- K23.7
			when others => S6LINK_TxDataIn(15 downto 8) <= x"00";
		end case;
	end if;
end process;
cplllock_n <= not cplllock;
i_S6Link_init : S6Link_init
    generic map
    (
        EXAMPLE_SIM_GTRESET_SPEEDUP     =>      "true",
        EXAMPLE_SIMULATION              =>      0,
        EXAMPLE_USE_CHIPSCOPE           =>      0
    ) 
    port map
    (
        SYSCLK_IN                       =>      DRPclk,
        SOFT_RESET_IN                   =>      '0',
        GT0_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone,
        GT0_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone,
        GT0_DATA_VALID_IN               =>      DATA_VALID,

  
 
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X0Y15)

        --------------------------------- CPLL Ports -------------------------------
        GT0_CPLLFBCLKLOST_OUT           =>      open,
        GT0_CPLLLOCK_OUT                =>      cplllock,
        GT0_CPLLLOCKDETCLK_IN           =>      drpclk,
        GT0_CPLLRESET_IN                =>      GTX_RESET,
        ------------------------- Channel - Ref Clock Ports ------------------------
        GT0_GTREFCLK0_IN                =>      GbE_REFCLK,
        ---------------- Channel - Dynamic Reconfiguration Port (DRP) --------------
        GT0_DRPADDR_IN                  =>      (others => '0'),
        GT0_DRPCLK_IN                   =>      drpclk,
        GT0_DRPDI_IN                    =>      (others => '0'),
        GT0_DRPDO_OUT                   =>      open,
        GT0_DRPEN_IN                    =>      '0',
        GT0_DRPRDY_OUT                  =>      open,
        GT0_DRPWE_IN                    =>      '0',
        --------------------- RX Initialization and Reset Ports --------------------
        GT0_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT0_EYESCANDATAERROR_OUT        =>      open,
        ------------------------- Receive Ports - CDR Ports ------------------------
        GT0_RXCDRLOCK_OUT               =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT0_RXUSRCLK_IN                 =>      UsRClk,
        GT0_RXUSRCLK2_IN                =>      UsRClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT0_RXDATA_OUT                  =>      S6LINK_RxDataOut,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT0_RXDISPERR_OUT               =>      open,
        GT0_RXNOTINTABLE_OUT            =>      S6LINK_RxNotInTable,
        --------------------------- Receive Ports - RX AFE -------------------------
        GT0_GTXRXP_IN                   =>      S6LINK_RXP,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT0_GTXRXN_IN                   =>      S6LINK_RXN,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT0_RXMCOMMAALIGNEN_IN          =>      S6LINK_RxResetDoneSyncRegs(2),
        GT0_RXPCOMMAALIGNEN_IN          =>      S6LINK_RxResetDoneSyncRegs(2),
        ------------ Receive Ports - RX Decision Feedback Equalizer(DFE) -----------
        GT0_RXDFELPMRESET_IN            =>      '0',
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT0_GTRXRESET_IN                =>      cplllock_n,
        GT0_RXPMARESET_IN               =>      '0',
        ----------------- Receive Ports - RX Polarity Control Ports ----------------
        GT0_RXPOLARITY_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT0_RXCHARISK_OUT               =>      S6LINK_RxCharIsK,
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT0_RXRESETDONE_OUT             =>      S6LINK_RxResetDone,
        --------------------- TX Initialization and Reset Ports --------------------
        GT0_GTTXRESET_IN                =>      cplllock_n,
        GT0_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT0_TXUSRCLK_IN                 =>      UsRClk,
        GT0_TXUSRCLK2_IN                =>      UsRClk,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_TXDATA_IN                   =>      S6LINK_TxDataIn,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  =>      S6LINK_TXN,
        GT0_GTXTXP_OUT                  =>      S6LINK_TXP,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT0_TXOUTCLK_OUT                =>      open,
        GT0_TXOUTCLKFABRIC_OUT          =>      open,
        GT0_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT0_TXCHARISK_IN                =>      S6LINK_TxCharIsK,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT0_TXRESETDONE_OUT             =>      open,
        -------------------- Transmit Ports - TX Polarity Control ------------------
        GT0_TXPOLARITY_IN               =>      '0',




    --____________________________COMMON PORTS________________________________
        ---------------------- Common Block  - Ref Clock Ports ---------------------
        GT0_GTREFCLK0_COMMON_IN         =>      GbE_REFCLK,
        ------------------------- Common Block - QPLL Ports ------------------------
        GT0_QPLLLOCK_OUT                =>      open,
        GT0_QPLLLOCKDETCLK_IN           =>      drpclk,
        GT0_QPLLRESET_IN                =>      '0'

    );
end Behavioral;

