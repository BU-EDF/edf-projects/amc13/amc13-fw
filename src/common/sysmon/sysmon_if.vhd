----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:32:29 01/28/2012 
-- Design Name: 
-- Module Name:    sysmon_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity sysmon_if is
    Port ( DRPclk : in  STD_LOGIC; -- 50MHz
           DB_cmd : in  STD_LOGIC;
           SN : in  STD_LOGIC_VECTOR (8 downto 0);
           VAUXN_IN : in  STD_LOGIC_VECTOR (12 downto 0);
           VAUXP_IN : in  STD_LOGIC_VECTOR (12 downto 0);
           addr : in  STD_LOGIC_VECTOR (15 downto 0);
           data : out  STD_LOGIC_VECTOR (31 downto 0);
           device_temp : out  STD_LOGIC_VECTOR (11 downto 0);
           ALM : out  STD_LOGIC_VECTOR (7 downto 0);
					 OT : out std_logic);
end sysmon_if;

architecture Behavioral of sysmon_if is
constant C : std_logic_vector(47 downto 0) := x"FFFFFF554800";
signal VAUXN : std_logic_vector(15 downto 0) := (others => '0');
signal VAUXP : std_logic_vector(15 downto 0) := (others => '0');
signal DADDR : std_logic_vector(6 downto 0) := (others => '0');
signal DO : std_logic_vector(15 downto 0) := (others => '0');
signal DRDY : std_logic := '0';
signal we_RAM : std_logic := '0';
signal DB_cmd_l : std_logic := '0';
signal re_ram : std_logic := '0';
signal EOC : std_logic := '0';
signal ram_wa : std_logic_vector(4 downto 0) := (others => '0');
signal RSTP : std_logic := '0';
signal OPMODE : std_logic_vector(6 downto 0) := (others => '0');
signal A : std_logic_vector(29 downto 0) := (others => '0');
signal B : std_logic_vector(17 downto 0) := (others => '0');
signal P : std_logic_vector(47 downto 0) := (others => '0');
begin
g_ram: for i in 0 to 13 generate
   i_ram : RAM64X1D_1
   generic map (
      INIT => X"0000000000000000") -- Initial contents of RAM
   port map (
      DPO => data(i),     -- Read-only 1-bit data output
      SPO => open,     -- R/W 1-bit data output
      A0 => ram_wa(0),       -- R/W address[0] input bit
      A1 => ram_wa(1),       -- R/W address[1] input bit
      A2 => ram_wa(2),       -- R/W address[2] input bit
      A3 => ram_wa(3),       -- R/W address[3] input bit
      A4 => ram_wa(4),       -- R/W address[4] input bit
      A5 => '1',       -- R/W address[5] input bit
      D => P(i+12),         -- Write 1-bit data input
      DPRA0 => addr(0), -- Read-only address[0] input bit
      DPRA1 => addr(1), -- Read-only address[1] input bit
      DPRA2 => addr(2), -- Read-only address[2] input bit
      DPRA3 => addr(3), -- Read-only address[3] input bit
      DPRA4 => addr(15), -- Read-only address[4] input bit
      DPRA5 => re_ram, -- Read-only address[5] input bit
      WCLK => DRPclk,   -- Write clock input
      WE => we_ram        -- Write enable input
   );
end generate;
re_ram <= '1' when addr(14 downto 4) = sysmon_addr(14 downto 4) else '0';
data(31 downto 14) <= (others => '0');
I_DSP48E1 : DSP48E1
   generic map (
      -- Feature Control Attributes: Data Path Selection
      A_INPUT => "DIRECT",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
      B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
      USE_DPORT => FALSE,                -- Select D port usage (TRUE or FALSE)
      USE_MULT => "MULTIPLY",            -- Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
      -- Pattern Detector Attributes: Pattern Detection Configuration
      AUTORESET_PATDET => "NO_RESET",    -- "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
      MASK => X"3fffffffffff",           -- 48-bit mask value for pattern detect (1=ignore)
      PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
      SEL_MASK => "MASK",                -- "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
      SEL_PATTERN => "PATTERN",          -- Select pattern value ("PATTERN" or "C")
      USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect ("PATDET" or "NO_PATDET")
      -- Register Control Attributes: Pipeline Register Configuration
      ACASCREG => 1,                     -- Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
      ADREG => 1,                        -- Number of pipeline stages for pre-adder (0 or 1)
      ALUMODEREG => 0,                   -- Number of pipeline stages for ALUMODE (0 or 1)
      AREG => 1,                         -- Number of pipeline stages for A (0, 1 or 2)
      BCASCREG => 1,                     -- Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
      BREG => 1,                         -- Number of pipeline stages for B (0, 1 or 2)
      CARRYINREG => 1,                   -- Number of pipeline stages for CARRYIN (0 or 1)
      CARRYINSELREG => 1,                -- Number of pipeline stages for CARRYINSEL (0 or 1)
      CREG => 1,                         -- Number of pipeline stages for C (0 or 1)
      DREG => 1,                         -- Number of pipeline stages for D (0 or 1)
      INMODEREG => 1,                    -- Number of pipeline stages for INMODE (0 or 1)
      MREG => 1,                         -- Number of multiplier pipeline stages (0 or 1)
      OPMODEREG => 1,                    -- Number of pipeline stages for OPMODE (0 or 1)
      PREG => 1,                         -- Number of pipeline stages for P (0 or 1)
      USE_SIMD => "ONE48"                -- SIMD selection ("ONE48", "TWO24", "FOUR12")
   )
   port map (
      -- Cascade: 30-bit (each) output: Cascade Ports
      ACOUT => open,                   -- 30-bit output: A port cascade output
      BCOUT => open,                   -- 18-bit output: B port cascade output
      CARRYCASCOUT => open,     -- 1-bit output: Cascade carry output
      MULTSIGNOUT => open,       -- 1-bit output: Multiplier sign cascade output
      PCOUT => open,                   -- 48-bit output: Cascade output
      -- Control: 1-bit (each) output: Control Inputs/Status Bits
      OVERFLOW => open,             -- 1-bit output: Overflow in add/acc output
      PATTERNBDETECT => open, -- 1-bit output: Pattern bar detect output
      PATTERNDETECT => open,   -- 1-bit output: Pattern detect output
      UNDERFLOW => open,           -- 1-bit output: Underflow in add/acc output
      -- Data: 4-bit (each) output: Data Ports
      CARRYOUT => open,             -- 4-bit output: Carry output
      P => P,                           -- 48-bit output: Primary data output
      -- Cascade: 30-bit (each) input: Cascade Ports
      ACIN => (others => '0'),                     -- 30-bit input: A cascade data input
      BCIN => (others => '0'),                     -- 18-bit input: B cascade input
      CARRYCASCIN => '0',       -- 1-bit input: Cascade carry input
      MULTSIGNIN => '0',         -- 1-bit input: Multiplier sign input
      PCIN => (others => '0'),                     -- 48-bit input: P cascade input
      -- Control: 4-bit (each) input: Control Inputs/Status Bits
      ALUMODE => x"0",               -- 4-bit input: ALU control input
      CARRYINSEL => "000",         -- 3-bit input: Carry select input
      CEINMODE => '1',             -- 1-bit input: Clock enable input for INMODEREG
      CLK => DRPclk,                       -- 1-bit input: Clock input
      INMODE => "10001",                 -- 5-bit input: INMODE control input
      OPMODE => OPMODE,                 -- 7-bit input: Operation mode input
      RSTINMODE => '0',           -- 1-bit input: Reset input for INMODEREG
      -- Data: 30-bit (each) input: Data Ports
      A => A,                           -- 30-bit input: A data input
      B => B,                           -- 18-bit input: B data input
      C => C,                           -- 48-bit input: C data input
      CARRYIN => '0',               -- 1-bit input: Carry input signal
      D => (others => '0'),                           -- 25-bit input: D data input
      -- Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
      CEA1 => '1',                     -- 1-bit input: Clock enable input for 1st stage AREG
      CEA2 =>'1',                     -- 1-bit input: Clock enable input for 2nd stage AREG
      CEAD => '1',                     -- 1-bit input: Clock enable input for ADREG
      CEALUMODE => '1',           -- 1-bit input: Clock enable input for ALUMODERE
      CEB1 => '1',                     -- 1-bit input: Clock enable input for 1st stage BREG
      CEB2 => '1',                     -- 1-bit input: Clock enable input for 2nd stage BREG
      CEC => '1',                       -- 1-bit input: Clock enable input for CREG
      CECARRYIN => '1',           -- 1-bit input: Clock enable input for CARRYINREG
      CECTRL => '1',                 -- 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
      CED => '1',                       -- 1-bit input: Clock enable input for DREG
      CEM => '1',                       -- 1-bit input: Clock enable input for MREG
      CEP => '1',                       -- 1-bit input: Clock enable input for PREG
      RSTA => '0',                     -- 1-bit input: Reset input for AREG
      RSTALLCARRYIN => '0',   -- 1-bit input: Reset input for CARRYINREG
      RSTALUMODE => '0',         -- 1-bit input: Reset input for ALUMODEREG
      RSTB => '0',                     -- 1-bit input: Reset input for BREG
      RSTC => '0',                     -- 1-bit input: Reset input for CREG
      RSTCTRL => '0',               -- 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
      RSTD => '0',                     -- 1-bit input: Reset input for DREG and ADREG
      RSTM => '0',                     -- 1-bit input: Reset input for MREG
      RSTP => RSTP                      -- 1-bit input: Reset input for PREG
   );
RSTP <= '1' when SN(8 downto 4) = "11111" and DADDR(4) = '1' else '0';
OPMODE <= "0110101" when DADDR(4 downto 0) = "00000" else "0000101";
A(29 downto 12) <= (others => '0');
A(11 downto 0) <= DO(15 downto 4);
B(17 downto 16) <= (others => '0');
i_we_ram : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q => we_ram,       -- SRL data output
      A0 => '0',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => DRPclk,   -- Clock input
      D => DRDY        -- SRL data input
   );
process(DRPclk)
begin
	if(DRPclk'event and DRPclk = '1')then
		if(DADDR(4 downto 0) = "00000" and DRDY = '1')then
			device_temp <= DO(15 downto 4);
		end if;
		if(DB_cmd = '1')then
			DB_cmd_l <= '1';
		elsif(we_ram = '1' and DADDR(4 downto 0) = "11100")then
			DB_cmd_l <= '0';
		end if;
		if(we_ram = '1' and DADDR(4 downto 0) = "11100")then
			ram_wa(4) <= DB_cmd_l;
		end if;
	end if;
end process;
process(DADDR)
begin
	case DADDR(4 downto 0) is
		when "00000" => ram_wa(3 downto 0) <= x"0"; -- temperature
		when "00001" => ram_wa(3 downto 0) <= x"3"; -- 1V0 Vccint
		when "00010" => ram_wa(3 downto 0) <= x"d"; -- 1V8 VccAux
		when "00110" => ram_wa(3 downto 0) <= x"e"; -- 1V0 VccBRAM
		when "10000" => ram_wa(3 downto 0) <= x"4"; -- 1V5 
		when "10010" => ram_wa(3 downto 0) <= x"2"; -- 1V2A
		when "10011" => ram_wa(3 downto 0) <= x"b"; -- DDR3_VTT
		when "10100" => ram_wa(3 downto 0) <= x"8"; -- 12V
		when "10101" => ram_wa(3 downto 0) <= x"9"; -- 1V8A VccAuxGTX
		when "11000" => ram_wa(3 downto 0) <= x"a"; -- 2V0 VccAuxIO
		when "11001" => ram_wa(3 downto 0) <= x"1"; -- 1V0A
		when "11010" => ram_wa(3 downto 0) <= x"6"; -- 3V3
		when "11011" => ram_wa(3 downto 0) <= x"5"; -- 2V5
		when "11100" => ram_wa(3 downto 0) <= x"c"; -- DDR3_VREF
		when others => ram_wa(3 downto 0) <= x"f";
	end case;
end process;
process(DADDR)
begin
	case DADDR(4 downto 0) is
		when "00000" => B(15 downto 0) <= x"13B0";
		when "00001" | "00010" | "00110" => B(15 downto 0) <= x"0BB8";
		when "10000" => B(15 downto 0) <= x"0654";
		when "10010" => B(15 downto 0) <= x"0514";
		when "10011" => B(15 downto 0) <= x"03E8";
		when "10100" => B(15 downto 0) <= x"411A";
		when "10101" => B(15 downto 0) <= x"07D0";
		when "11000" => B(15 downto 0) <= x"0852";
		when "11001" => B(15 downto 0) <= x"044C";
		when "11010" => B(15 downto 0) <= x"0DDE";
		when "11011" => B(15 downto 0) <= x"0A28";
		when "11100" => B(15 downto 0) <= x"03E8";
		when others => B(15 downto 0) <= x"0000";
	end case;
end process;
I_XADC : XADC
   generic map (
      -- INIT_40 - INIT_42: XADC configuration registers
      INIT_40 => X"0000",
      INIT_41 => X"2ef0",
      INIT_42 => X"0400",
      -- INIT_48 - INIT_4F: Sequence Registers
      INIT_48 => X"4701",
      INIT_49 => X"1f3d",
      INIT_4A => X"0000",
      INIT_4B => X"0000",
      INIT_4C => X"0000",
      INIT_4D => X"0000",
      INIT_4F => X"0000",
      INIT_4E => X"0000",               -- Sequence register 6
      -- INIT_50 - INIT_58, INIT5C: Alarm Limit Registers
      INIT_50 => X"b5ed",
      INIT_51 => X"5999",
      INIT_52 => X"a147",
      INIT_53 => X"dddd",
      INIT_54 => X"767a",
      INIT_55 => X"5111",
      INIT_56 => X"91eb",
      INIT_57 => X"ae4e",
      INIT_58 => X"5999",
      INIT_5C => X"5111",
      -- Simulation attributes: Set for proepr simulation behavior
      SIM_DEVICE => "7SERIES",          -- Select target device (values)
      SIM_MONITOR_FILE => "design.txt"  -- Analog simulation data file name
   )
   port map (
      -- ALARMS: 8-bit (each) output: ALM, OT
      ALM => ALM,                   -- 8-bit output: Output alarm for temp, Vccint, Vccaux and Vccbram
      OT => OT,                     -- 1-bit output: Over-Temperature alarm
      -- Dynamic Reconfiguration Port (DRP): 16-bit (each) output: Dynamic Reconfiguration Ports
      DO => DO,                     -- 16-bit output: DRP output data bus
      DRDY => DRDY,                 -- 1-bit output: DRP data ready
      -- STATUS: 1-bit (each) output: XADC status ports
      BUSY => open,                 -- 1-bit output: ADC busy output
      CHANNEL => DADDR(4 downto 0),           -- 5-bit output: Channel selection outputs
      EOC => EOC,                   -- 1-bit output: End of Conversion
      EOS => open,                   -- 1-bit output: End of Sequence
      JTAGBUSY => open,         -- 1-bit output: JTAG DRP transaction in progress output
      JTAGLOCKED => open,     -- 1-bit output: JTAG requested DRP port lock
      JTAGMODIFIED => open, -- 1-bit output: JTAG Write to the DRP has occurred
      MUXADDR => open,           -- 5-bit output: External MUX channel decode
      -- Auxiliary Analog-Input Pairs: 16-bit (each) input: VAUXP[15:0], VAUXN[15:0]
      VAUXN => VAUXN,               -- 16-bit input: N-side auxiliary analog input
      VAUXP => VAUXP,               -- 16-bit input: P-side auxiliary analog input
      -- CONTROL and CLOCK: 1-bit (each) input: Reset, conversion start and clock inputs
      CONVST => '0',             -- 1-bit input: Convert start input
      CONVSTCLK => '0',       -- 1-bit input: Convert start input
      RESET => '0',               -- 1-bit input: Active-high reset
      -- Dedicated Analog Input Pair: 1-bit (each) input: VP/VN
      VN => '0',                     -- 1-bit input: N-side analog input
      VP => '0',                     -- 1-bit input: P-side analog input
      -- Dynamic Reconfiguration Port (DRP): 7-bit (each) input: Dynamic Reconfiguration Ports
      DADDR => DADDR,               -- 7-bit input: DRP address bus
      DCLK => DRPclk,                 -- 1-bit input: DRP clock
      DEN => EOC,                   -- 1-bit input: DRP enable signal
      DI => x"0000",                     -- 16-bit input: DRP input data bus
      DWE => '0'                    -- 1-bit input: DRP write enable
   );
DADDR(6 downto 5) <= "00";
VAUXN <= "000" & VAUXN_IN(12 downto 8) & "00" & VAUXN_IN(5 downto 2) & '0' & VAUXN_IN(0);
VAUXP <= "000" & VAUXP_IN(12 downto 8) & "00" & VAUXP_IN(5 downto 2) & '0' & VAUXP_IN(0);
end Behavioral;

