----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:37:54 11/04/2015 
-- Design Name: 
-- Module Name:    check_event - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity check_event is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           en_stop : in  STD_LOGIC_VECTOR (4 downto 0);
           cmsCRC_err : in  STD_LOGIC_VECTOR (2 downto 0);
					 EventData_in : in array3X67;
					 EventData_we : in   std_logic_VECTOR(2 downto 0);
           inc_err : out  array3X5;
           stop : out  STD_LOGIC);
end check_event;

architecture Behavioral of check_event is
signal EvtLengthCntr : array3X24 := (others => (others => '0'));
signal nAMC : array3X4 := (others => (others => '0'));
signal EvtLength_err : std_logic_vector(2 downto 0) := (others => '0');
signal AMClength_err : std_logic_vector(2 downto 0) := (others => '0');
signal AMCvalid_err : std_logic_vector(2 downto 0) := (others => '0');
signal AMCcrc_err : std_logic_vector(2 downto 0) := (others => '0');
signal IsBoE : std_logic_vector(2 downto 0) := (others => '0');
signal IsBoB : std_logic_vector(2 downto 0) := (others => '0');
begin
inc_err(0) <= AMCcrc_err(0) & AMCvalid_err(0) & AMClength_err(0) & EvtLength_err(0) & cmsCRC_err(0);
inc_err(1) <= AMCcrc_err(1) & AMCvalid_err(1) & AMClength_err(1) & EvtLength_err(1) & cmsCRC_err(1);
inc_err(2) <= AMCcrc_err(2) & AMCvalid_err(2) & AMClength_err(2) & EvtLength_err(2) & cmsCRC_err(2);
process(clk)
begin
	if(clk'event and clk = '1')then
		for i in 0 to 2 loop
			if(reset = '1')then
				EvtLengthCntr(i) <= x"000001";
				IsBoE(i) <= '1';
				IsBoB(i) <= '0';
			elsif(EventData_we(i) = '1')then
				if(EventData_in(i)(65) = '1')then
					EvtLengthCntr(i) <= x"000001";
				else
					EvtLengthCntr(i) <= EvtLengthCntr(i) + 1;
				end if;
				if(EventData_in(i)(65) = '1')then
					IsBoE(i) <= '1';
				elsif(EventData_in(i)(64) = '1')then
					IsBoB(i) <= '1';
				else
					IsBoE(i) <= '0';
					IsBoB(i) <= IsBoE(i);
				end if;
			end if;
			if(reset = '1')then
				nAMC(i) <= (others => '0');
			elsif(EventData_we(i) = '1')then
				if(IsBoB(i) = '1')then
					nAMC(i) <= EventData_in(i)(55 downto 52);
				elsif(or_reduce(nAMC(i)) = '1')then
					nAMC(i) <= nAMC(i) - 1;
				end if;
			end if;
			if(EventData_we(i) = '1' and or_reduce(nAMC(i)) = '1' and EventData_in(i)(62) = '1')then
				AMClength_err(i) <= '1';
			else
				AMClength_err(i) <= '0';
			end if;
			if(EventData_we(i) = '1' and or_reduce(nAMC(i)) = '1' and EventData_in(i)(61) = '0' and EventData_in(i)(57) = '0')then
				AMCvalid_err(i) <= '1';
			else
				AMCvalid_err(i) <= '0';
			end if;
			if(EventData_we(i) = '1' and or_reduce(nAMC(i)) = '1' and EventData_in(i)(61) = '0' and EventData_in(i)(56) = '0')then
				AMCcrc_err(i) <= '1';
			else
				AMCcrc_err(i) <= '0';
			end if;
			if(EventData_we(i) = '1' and EventData_in(i)(65) = '1' and EventData_in(i)(55 downto 32) /= EvtLengthCntr(i))then
				EvtLength_err(i) <= '1';
			else
				EvtLength_err(i) <= '0';
			end if;
		end loop;
		stop <= (or_reduce(cmsCRC_err) and en_stop(0)) or(or_reduce(EvtLength_err) and en_stop(1)) or(or_reduce(AMClength_err) and en_stop(2)) or
						(or_reduce(AMCvalid_err) and en_stop(3)) or(or_reduce(AMCcrc_err) and en_stop(4));
	end if;
end process;

end Behavioral;

