----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:53:01 07/21/2011 
-- Design Name: 
-- Module Name:     
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity Threshold is
    Port ( clk : in  STD_LOGIC; -- clk input
           din : in  STD_LOGIC_VECTOR(15 downto 0); 
           nongap_size : in  STD_LOGIC_VECTOR(11 downto 0); 
           dout : out  STD_LOGIC_VECTOR(31 downto 0));
end Threshold;

architecture Behavioral of Threshold is
signal old_din : std_logic_vector(15 downto 0) := (others => '0');
signal old_nongap_size : std_logic_vector(11 downto 0) := (others => '0');
signal start : std_logic := '0';
signal busy : std_logic := '0';
signal CEP : std_logic := '0';
signal divide : std_logic := '0';
signal shift : std_logic := '0';
signal check_denom : std_logic := '0';
signal denom : std_logic_vector(12 downto 0) := (others => '0');
signal div_cntr : std_logic_vector(4 downto 0) := (others => '0');
signal nom : std_logic_vector(16 downto 0) := (others => '0');
signal divident : std_logic_vector(29 downto 0) := (others => '0');
signal Mult_P : std_logic_vector(47 downto 0) := (others => '0');
begin
dout <= Mult_P(41 downto 10);
process(clk)
begin
	if(clk'event and clk = '1')then
		if(start = '1')then
			old_nongap_size <= nongap_size;
			old_din <= din;
		end if;
		if(start = '1')then
			busy <= '1';
		elsif(or_reduce(div_cntr) = '0' and divide = '1')then
			busy <= '0';
		end if;
		if((din /= old_din or nongap_size /= old_nongap_size) and busy = '0' and start = '0')then
			start <= '1';
		else
			start <= '0';
		end if;
		if(start = '1')then
--			if(or_reduce(nongap_size(11 downto 5)) = '0')then
--				denom(11 downto 0) <= x"020";
			if(or_reduce(nongap_size(11 downto 8)) = '0')then
				denom(11 downto 0) <= x"100";
			else
				denom(11 downto 0) <= nongap_size;
			end if;
			div_cntr <= "10001";
			check_denom <= '1';
			nom <= '0' & din;
			divident <= (others => '0');
			shift <= '0';
		elsif(check_denom = '1')then
			if(denom(11) = '0')then
				div_cntr <= div_cntr + 1;
				denom(11 downto 0) <= denom(10 downto 0) & '0';
			else
				check_denom <= '0';
				divide <= '1';
			end if;
		elsif(divide = '1')then
			if(shift = '0')then
				div_cntr <= div_cntr - 1;
				if(nom(16 downto 4) >= denom)then
					nom(16 downto 4) <= nom(16 downto 4) - denom;
--					divident(0) <= '1';
					divident(3) <= '1';
				end if;
				shift <= '1';
			else
				nom <= nom(15 downto 0) & '0';
--				divident(23 downto 0) <= divident(22 downto 0) & '0';
				divident(26 downto 3) <= divident(25 downto 3) & '0';
				shift <= '0';
				if(or_reduce(div_cntr) = '0')then
					divide <= '0';
				end if;
			end if;
		end if;
		CEP <= divide and not or_reduce(div_cntr);
	end if;
end process;
   DSP48E1_inst : DSP48E1
   generic map (
      -- Feature Control Attributes: Data Path Selection
      A_INPUT => "DIRECT",               -- Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
      B_INPUT => "DIRECT",               -- Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
      USE_DPORT => FALSE,                -- Select D port usage (TRUE or FALSE)
      USE_MULT => "MULTIPLY",            -- Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
      USE_SIMD => "ONE48",               -- SIMD selection ("ONE48", "TWO24", "FOUR12")
      -- Pattern Detector Attributes: Pattern Detection Configuration
      AUTORESET_PATDET => "NO_RESET",    -- "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
      MASK => X"3fffffffffff",           -- 48-bit mask value for pattern detect (1=ignore)
      PATTERN => X"000000000000",        -- 48-bit pattern match for pattern detect
      SEL_MASK => "MASK",                -- "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
      SEL_PATTERN => "PATTERN",          -- Select pattern value ("PATTERN" or "C")
      USE_PATTERN_DETECT => "NO_PATDET", -- Enable pattern detect ("PATDET" or "NO_PATDET")
      -- Register Control Attributes: Pipeline Register Configuration
      ACASCREG => 0,                     -- Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
      ADREG => 0,                        -- Number of pipeline stages for pre-adder (0 or 1)
      ALUMODEREG => 0,                   -- Number of pipeline stages for ALUMODE (0 or 1)
      AREG => 0,                         -- Number of pipeline stages for A (0, 1 or 2)
      BCASCREG => 0,                     -- Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
      BREG => 0,                         -- Number of pipeline stages for B (0, 1 or 2)
      CARRYINREG => 0,                   -- Number of pipeline stages for CARRYIN (0 or 1)
      CARRYINSELREG => 0,                -- Number of pipeline stages for CARRYINSEL (0 or 1)
      CREG => 0,                         -- Number of pipeline stages for C (0 or 1)
      DREG => 0,                         -- Number of pipeline stages for D (0 or 1)
      INMODEREG => 0,                    -- Number of pipeline stages for INMODE (0 or 1)
      MREG => 0,                         -- Number of multiplier pipeline stages (0 or 1)
      OPMODEREG => 0,                    -- Number of pipeline stages for OPMODE (0 or 1)
      PREG => 1                          -- Number of pipeline stages for P (0 or 1)
   )
   port map (
      -- Cascade: 30-bit (each) output: Cascade Ports
      ACOUT => open,                   -- 30-bit output: A port cascade output
      BCOUT => open,                   -- 18-bit output: B port cascade output
      CARRYCASCOUT => open,     -- 1-bit output: Cascade carry output
      MULTSIGNOUT => open,       -- 1-bit output: Multiplier sign cascade output
      PCOUT => open,                   -- 48-bit output: Cascade output
      -- Control: 1-bit (each) output: Control Inputs/Status Bits
      OVERFLOW => open,             -- 1-bit output: Overflow in add/acc output
      PATTERNBDETECT => open, -- 1-bit output: Pattern bar detect output
      PATTERNDETECT => open,   -- 1-bit output: Pattern detect output
      UNDERFLOW => open,           -- 1-bit output: Underflow in add/acc output
      -- Data: 4-bit (each) output: Data Ports
      CARRYOUT => open,             -- 4-bit output: Carry output
      P => Mult_P,                           -- 48-bit output: Primary data output
      -- Cascade: 30-bit (each) input: Cascade Ports
      ACIN => (others => '0'),                     -- 30-bit input: A cascade data input
      BCIN => (others => '0'),                     -- 18-bit input: B cascade input
      CARRYCASCIN => '0',       -- 1-bit input: Cascade carry input
      MULTSIGNIN => '0',         -- 1-bit input: Multiplier sign input
      PCIN => (others => '0'),                     -- 48-bit input: P cascade input
      -- Control: 4-bit (each) input: Control Inputs/Status Bits
      ALUMODE => "0000",               -- 4-bit input: ALU control input
      CARRYINSEL => "000",         -- 3-bit input: Carry select input
      CLK => clk,                       -- 1-bit input: Clock input
      INMODE => "00000",                 -- 5-bit input: INMODE control input
      OPMODE => "0000101",                 -- 7-bit input: Operation mode input
      -- Data: 30-bit (each) input: Data Ports
      A => divident,                           -- 30-bit input: A data input
      B => "010111010011101111",                           -- 18-bit input: B data input
      C => (others => '0'),                           -- 48-bit input: C data input
      CARRYIN => '0',               -- 1-bit input: Carry input signal
      D => (others => '0'),                           -- 25-bit input: D data input
      -- Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
      CEA1 => '0',                     -- 1-bit input: Clock enable input for 1st stage AREG
      CEA2 => '0',                     -- 1-bit input: Clock enable input for 2nd stage AREG
      CEAD => '0',                     -- 1-bit input: Clock enable input for ADREG
      CEALUMODE => '0',           -- 1-bit input: Clock enable input for ALUMODE
      CEB1 => '0',                     -- 1-bit input: Clock enable input for 1st stage BREG
      CEB2 => '0',                     -- 1-bit input: Clock enable input for 2nd stage BREG
      CEC => '0',                       -- 1-bit input: Clock enable input for CREG
      CECARRYIN => '0',           -- 1-bit input: Clock enable input for CARRYINREG
      CECTRL => '0',                 -- 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
      CED => '0',                       -- 1-bit input: Clock enable input for DREG
      CEINMODE => '0',             -- 1-bit input: Clock enable input for INMODEREG
      CEM => '0',                       -- 1-bit input: Clock enable input for MREG
      CEP => CEP,                       -- 1-bit input: Clock enable input for PREG
      RSTA => '0',                     -- 1-bit input: Reset input for AREG
      RSTALLCARRYIN => '0',   -- 1-bit input: Reset input for CARRYINREG
      RSTALUMODE => '0',         -- 1-bit input: Reset input for ALUMODEREG
      RSTB => '0',                     -- 1-bit input: Reset input for BREG
      RSTC => '0',                     -- 1-bit input: Reset input for CREG
      RSTCTRL => '0',               -- 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
      RSTD => '0',                     -- 1-bit input: Reset input for DREG and ADREG
      RSTINMODE => '0',           -- 1-bit input: Reset input for INMODEREG
      RSTM => '0',                     -- 1-bit input: Reset input for MREG
      RSTP => '0'                      -- 1-bit input: Reset input for PREG
   );
end Behavioral;

