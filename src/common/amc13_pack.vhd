--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.amc13_version_package.all;

package amc13_pack is
-- constants (included from amc13_version_package
  constant device_ID	 		: std_logic_vector(15 downto 0) := device_ID; --! from amc13_version_package
  constant T2_version			: std_logic_vector(15 downto 0) := T2_version; --! from amc13_version_package
  constant K7version			: std_logic_vector(15 downto 0) := k7version; --! from amc13_version_package
  constant V6version			: std_logic_vector(15 downto 0) := V6version; --! from amc13_version_package
  constant CTRversion			: std_logic_vector(7 downto 0) := CTRversion; --! from amc13_version_package
  constant key				 	: std_logic_vector(31 downto 0) := key; --! from amc13_version_package
  constant flavor               : string := flavor; --! from amc13_version_package
  constant lsc_speed            : integer := lsc_speed; --! from amc13_version_package
-- use A24 to access VME chip data
-- registers on VME chip 
constant ID_addr	 			: std_logic_vector(15 downto 0) := x"0000";
constant CRC_vme_addr	 	: std_logic_vector(15 downto 0) := x"0004";
constant CRC_DCC_addr	 	: std_logic_vector(15 downto 0) := x"0008";
constant CRC_LRB_addr	 	: std_logic_vector(15 downto 0) := x"000c";
constant VME_csr_addr 	: std_logic_vector(15 downto 0) := x"0010"; -- VME chip control and status register
constant F_cmd_addr 		: std_logic_vector(15 downto 0) := x"0100"; -- FLASH command register, write only
constant key_addr 			: std_logic_vector(15 downto 0) := x"0294"; -- FLASH operation protection key register, write only
-- block RAM buffer on VME chip
constant F_wbuf_addr    : std_logic_vector(15 downto 0) := x"0800"; -- size 0x200 FLASH write buffer
constant F_rbuf_addr    : std_logic_vector(15 downto 0) := x"0a00"; -- size 0x200 FLASH read buffer
-- use A32 to access legacy DCC registers
---------------------------------------------------------------------------------------------------------------------------
constant CSR_addr 			: std_logic_vector(15 downto 0) := x"0000";
constant CFG_addr 			: std_logic_vector(15 downto 0) := x"0001";
constant MON_ctrl_addr 	: std_logic_vector(15 downto 0) := x"0002";
constant HTR_EN_addr 		: std_logic_vector(15 downto 0) := x"0003";
constant SFP_CSR_addr		: std_logic_vector(15 downto 0) := x"0004";
constant HTR_test_addr 	: std_logic_vector(15 downto 0) := x"0005";
constant BC0_delay_addr : std_logic_vector(15 downto 0) := x"0006";
constant SRC_id_addr 		: std_logic_vector(15 downto 0) := x"0007";
constant TTC_bcnt_addr 	: std_logic_vector(15 downto 0) := x"0008";
constant TTC_cal_addr 	: std_logic_vector(15 downto 0) := x"0009";
constant mem_status_addr: std_logic_vector(15 downto 0) := x"000a";
constant test_ctrl_addr	: std_logic_vector(15 downto 0) := x"000b";
constant PAGE_addr	 		: std_logic_vector(15 downto 0) := x"000c";
constant MON_wc_addr 		: std_logic_vector(15 downto 0) := x"000d";
constant evn_pntr_addr	: std_logic_vector(15 downto 0) := x"000e";
constant SL_pntr_addr		: std_logic_vector(15 downto 0) := x"000f";
constant SRC_id1_addr 		: std_logic_vector(15 downto 0) := x"0011";
constant SRC_id2_addr 		: std_logic_vector(15 downto 0) := x"0012";
constant fake_length_addr	: std_logic_vector(15 downto 0) := x"0018";
constant TTS_pattern_addr	: std_logic_vector(15 downto 0) := x"0019";
--constant sta_ctrl_addr	: std_logic_vector(15 downto 0) := x"0010"; -- size 0x10 -- was at 0xc00
constant status_cntr_addr: std_logic_vector(15 downto 0) := x"0020"; -- size 0x10 -- was at 0xbc0
constant sysmon_addr		: std_logic_vector(15 downto 0) := x"0030"; -- size 0x10
constant misc_cntr_addr	: std_logic_vector(15 downto 0) := x"0040"; -- size 0x40
constant LSC_addr 			: std_logic_vector(15 downto 0) := x"0080"; -- size 0x20
constant I2C_addr				: std_logic_vector(15 downto 0) := x"0100"; -- size 0x100
constant L1A_buf_addr	: std_logic_vector(15 downto 0) := x"0200"; -- size 0x200
constant AMC_reg_addr 	: std_logic_vector(15 downto 0) := x"0800";
constant SFP_reg_addr 	: std_logic_vector(15 downto 0) := x"1000";
--constant ll2_dbg_addr 	: std_logic_vector(15 downto 0) := x"0880";
--constant ll3_dbg_addr 	: std_logic_vector(15 downto 0) := x"08c0";
--constant ll4_dbg_addr 	: std_logic_vector(15 downto 0) := x"0900";
--constant edc_dbg_addr 	: std_logic_vector(15 downto 0) := x"0940";
--constant eb_dbg_addr 		: std_logic_vector(15 downto 0) := x"0980";
--constant mem_dbg_addr 	: std_logic_vector(15 downto 0) := x"09c0";
--constant ddr_dbg_addr 	: std_logic_vector(15 downto 0) := x"0a00";
--constant SL_buf_addr 		: std_logic_vector(15 downto 0) := x"0c00"; -- size 0x200
--constant mem_buf_addr		: std_logic_vector(15 downto 0) := x"4000"; -- size 0x2000
-- HTR base address
constant HTR0_base_addr : std_logic_vector(15 downto 0) := x"1000"; -- size 0x80
constant HTR1_base_addr : std_logic_vector(15 downto 0) := x"1080"; -- size 0x80
constant HTR2_base_addr : std_logic_vector(15 downto 0) := x"1100"; -- size 0x80
constant HTR3_base_addr : std_logic_vector(15 downto 0) := x"1180"; -- size 0x80
constant HTR4_base_addr : std_logic_vector(15 downto 0) := x"1200"; -- size 0x80
constant HTR5_base_addr : std_logic_vector(15 downto 0) := x"1280"; -- size 0x80
constant HTR6_base_addr : std_logic_vector(15 downto 0) := x"1300"; -- size 0x80
constant HTR7_base_addr : std_logic_vector(15 downto 0) := x"1380"; -- size 0x80
constant HTR8_base_addr : std_logic_vector(15 downto 0) := x"1400"; -- size 0x80
constant HTR9_base_addr : std_logic_vector(15 downto 0) := x"1480"; -- size 0x80
constant HTR10_base_addr : std_logic_vector(15 downto 0) := x"1500"; -- size 0x80
constant HTR11_base_addr : std_logic_vector(15 downto 0) := x"1580"; -- size 0x80
-- LRB address offset
constant HTR_status		 	: std_logic_vector(11 downto 0) := x"008";
-- HTR channel 0 counters
constant CH0_wc				 	: std_logic_vector(11 downto 0) := x"400";  -- total word count 8 bytes 
constant CH0_cerr				: std_logic_vector(11 downto 0) := x"408";  -- total cerr count 8 bytes 
constant CH0_uerr				: std_logic_vector(11 downto 0) := x"410";  -- total uerr count 8 bytes 
constant CH0_event_cnt	: std_logic_vector(11 downto 0) := x"418";  -- total event count 8 bytes 
constant CH0_evt_cerr 	: std_logic_vector(11 downto 0) := x"420";  -- total event count with CERR 8 bytes 
constant CH0_evt_uerr 	: std_logic_vector(11 downto 0) := x"428";  -- total event count with UERR 8 bytes 
constant CH0_evt_trunc 	: std_logic_vector(11 downto 0) := x"430";  -- total truncated event count 8 bytes 
constant CH0_evt_badid 	: std_logic_vector(11 downto 0) := x"438";  -- total event count with BADID error 8 bytes 
constant CH0_evt_crc	 	: std_logic_vector(11 downto 0) := x"440";  -- total event count with CRC error 8 bytes 
-- memeory window
type array2X2 is array(0 to 1) of std_logic_vector(1 downto 0);
type array2X3 is array(0 to 1) of std_logic_vector(2 downto 0);
type array2X4 is array(0 to 1) of std_logic_vector(3 downto 0);
type array2X7 is array(0 to 1) of std_logic_vector(6 downto 0);
type array2X8 is array(0 to 1) of std_logic_vector(7 downto 0);
type array2X9 is array(0 to 1) of std_logic_vector(8 downto 0);
type array2X16 is array(0 to 1) of std_logic_vector(15 downto 0);
type array2X32 is array(0 to 1) of std_logic_vector(31 downto 0);
type array2X64 is array(0 to 1) of std_logic_vector(63 downto 0);
type array2X65 is array(0 to 1) of std_logic_vector(64 downto 0);
type array3X2 is array(0 to 2) of std_logic_vector(1 downto 0);
type array3X3 is array(0 to 2) of std_logic_vector(2 downto 0);
type array3X4 is array(0 to 2) of std_logic_vector(3 downto 0);
type array3X5 is array(0 to 2) of std_logic_vector(4 downto 0);
type array3X7 is array(0 to 2) of std_logic_vector(6 downto 0);
type array3X8 is array(0 to 2) of std_logic_vector(7 downto 0);
type array3X9 is array(0 to 2) of std_logic_vector(8 downto 0);
type array3X10 is array(0 to 2) of std_logic_vector(9 downto 0);
type array3X11 is array(0 to 2) of std_logic_vector(10 downto 0);
type array3X12 is array(0 to 2) of std_logic_vector(11 downto 0);
type array3X13 is array(0 to 2) of std_logic_vector(12 downto 0);
type array3X14 is array(0 to 2) of std_logic_vector(13 downto 0);
type array3X16 is array(0 to 2) of std_logic_vector(15 downto 0);
type array3X17 is array(0 to 2) of std_logic_vector(16 downto 0);
type array3X18 is array(0 to 2) of std_logic_vector(17 downto 0);
type array3X19 is array(0 to 2) of std_logic_vector(18 downto 0);
type array3X20 is array(0 to 2) of std_logic_vector(19 downto 0);
type array3X21 is array(0 to 2) of std_logic_vector(20 downto 0);
type array3X24 is array(0 to 2) of std_logic_vector(23 downto 0);
type array3X26 is array(0 to 2) of std_logic_vector(25 downto 0);
type array3X28 is array(0 to 2) of std_logic_vector(27 downto 0);
type array3X32 is array(0 to 2) of std_logic_vector(31 downto 0);
type array3X48 is array(0 to 2) of std_logic_vector(47 downto 0);
type array3X56 is array(0 to 2) of std_logic_vector(55 downto 0);
type array3X64 is array(0 to 2) of std_logic_vector(63 downto 0);
type array3X65 is array(0 to 2) of std_logic_vector(64 downto 0);
type array3X66 is array(0 to 2) of std_logic_vector(65 downto 0);
type array3X67 is array(0 to 2) of std_logic_vector(66 downto 0);
type array4X9 is array(0 to 3) of std_logic_vector(8 downto 0);
type array4X10 is array(0 to 3) of std_logic_vector(9 downto 0);
type array4X11 is array(0 to 3) of std_logic_vector(10 downto 0);
type array4X12 is array(0 to 3) of std_logic_vector(11 downto 0);
type array4X13 is array(0 to 4) of std_logic_vector(12 downto 0);
type array4X64 is array(0 to 3) of std_logic_vector(63 downto 0);
type array5X11 is array(0 to 4) of std_logic_vector(10 downto 0);
type array5X13 is array(0 to 4) of std_logic_vector(12 downto 0);
type array6X64 is array(0 to 5) of std_logic_vector(63 downto 0);
type array8X4 is array(0 to 7) of std_logic_vector(3 downto 0);
type array8X12 is array(0 to 7) of std_logic_vector(11 downto 0);
type array12X2 is array(0 to 11) of std_logic_vector(1 downto 0);
type array12X3 is array(0 to 11) of std_logic_vector(2 downto 0);
type array12X4 is array(0 to 11) of std_logic_vector(3 downto 0);
type array12X6 is array(0 to 11) of std_logic_vector(5 downto 0);
type array12X8 is array(0 to 11) of std_logic_vector(7 downto 0);
type array12X13 is array(0 to 11) of std_logic_vector(12 downto 0);
type array12X16 is array(0 to 11) of std_logic_vector(15 downto 0);
type array12X20 is array(0 to 11) of std_logic_vector(19 downto 0);
type array12X21 is array(0 to 11) of std_logic_vector(20 downto 0);
type array12X32 is array(0 to 11) of std_logic_vector(31 downto 0);
type array12X64 is array(0 to 11) of std_logic_vector(63 downto 0);
type array32X32 is array(0 to 31) of std_logic_vector(31 downto 0);
type array16X4 is array(0 to 15) of std_logic_vector(3 downto 0);
type array24X32 is array(0 to 23) of std_logic_vector(31 downto 0);
type array2X3x5 is array(0 to 1) of array3x5;
type array2X3x12 is array(0 to 1) of array3x12;
type bitarray9x64 is array(0 to 8) of bit_vector(63 downto 0);
end amc13_pack;
package body amc13_pack is
end amc13_pack;
