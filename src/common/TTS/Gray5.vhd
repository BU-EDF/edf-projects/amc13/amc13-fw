----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:55:12 07/27/2014 
-- Design Name: 
-- Module Name:    Gray5 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity Gray5 is
    Port ( d_current : in  STD_LOGIC_VECTOR (4 downto 0);
           d_next : out  STD_LOGIC_VECTOR (4 downto 0));
end Gray5;

architecture Behavioral of Gray5 is
type array5x32 is array(0 to 4) of bit_vector(31 downto 0);
constant rom_init : array5x32 := (x"33333333",x"3c3c3c3c",x"0ff00ff0",x"00ffff00",x"ffff0000");
begin
g_d_next : for i in 0 to 4 generate
	i_d_next : ROM32X1 generic map (INIT => rom_init(i))
   port map (
      O => d_next(i),   -- ROM output
      A0 => d_current(0), -- ROM address[0]
      A1 => d_current(1), -- ROM address[1]
      A2 => d_current(2), -- ROM address[2]
      A3 => d_current(3), -- ROM address[3]
      A4 => d_current(4)  -- ROM address[4]
   );
end generate;
end Behavioral;

