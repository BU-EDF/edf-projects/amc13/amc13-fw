----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:49:10 06/14/2012 
-- Design Name: 
-- Module Name:    TTS_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
-- TTC Hamming encoding
-- hmg[0] = d[0]^d[1]^d[2]^d[3];
-- hmg[1] = d[0]^d[4]^d[5]^d[6];
-- hmg[2] = d[1]^d[2]^d[4]^d[5]^d[7];
-- hmg[3] = d[1]^d[3]^d[4]^d[6]^d[7];
-- hmg[4] = d[0]^d[2]^d[3]^d[5]^d[6]^d[7];
--
-- L1A trigger from CTR module takes the position of L1A in TTC
-- message format takes that of TTC broadcast message.(FMT = 0)
-- BcntRes uses brcst_data(0), TTS signals uses brcst_data(7 downto 4), brcst_data(3 downto 1) are reserved and are always 0
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;


--! Generate the 400Mbit TTS stream for the current TTS state

--! Additional Comments: 
--!
--! TTC Hamming encoding
--! hmg[0] = d[0]^d[1]^d[2]^d[3];
--! hmg[1] = d[0]^d[4]^d[5]^d[6];
--! hmg[2] = d[1]^d[2]^d[4]^d[5]^d[7];
--! hmg[3] = d[1]^d[3]^d[4]^d[6]^d[7];
--! hmg[4] = d[0]^d[2]^d[3]^d[5]^d[6]^d[7];
--!
--! L1A trigger from CTR module takes the position of L1A in TTC
--! message format takes that of TTC broadcast message.(FMT = 0)
--! BcntRes uses brcst_data(0), TTS signals uses brcst_data(7 downto 4), brcst_data(3 downto 1) are reserved and are always 0

entity TTS_if is
  Port ( sysclk : in  STD_LOGIC;
         TTS_clk : in  STD_LOGIC; --! 200Mhz TCDS DDR or 160Mhz fake TTC clock
         reset : in  STD_LOGIC;
         local_TTC : in  STD_LOGIC; --! Controls TCDS DDR or TTC clock output
         TTS : in  STD_LOGIC_VECTOR (3 downto 0); --! this is clocked by sysclk, the state signal in the top module AMC13_T1
         TTS_out_p : out  STD_LOGIC; --! TTS output stream (p) 
         TTS_out_n : out  STD_LOGIC); --! TTS output stream (n) 
end TTS_if;

architecture Behavioral of TTS_if is
  COMPONENT RAM32x6D
    PORT(
      wclk : IN std_logic;
      rclk : IN std_logic;
      di : IN std_logic_vector(5 downto 0);
      we : IN std_logic;
      wa : IN std_logic_vector(4 downto 0);
      ra : IN std_logic_vector(4 downto 0);
      ceReg : IN std_logic;          
      do : OUT std_logic_vector(5 downto 0)
      );
  END COMPONENT;
  COMPONENT Gray5
    PORT(
      d_current : IN std_logic_vector(4 downto 0);          
      d_next : OUT std_logic_vector(4 downto 0)
      );
  END COMPONENT;
  signal TTS_out : std_logic := '0';
  signal TTS_fifo_empty : std_logic := '1';
  signal TTS_q : std_logic_vector(3 downto 0) := (others =>'0');
  signal TTS_fifo_wa : std_logic_vector(4 downto 0) := (others =>'0');
  signal TTS_fifo_ra : std_logic_vector(4 downto 0) := (others =>'0');
  signal next_TTS_fifo_wa : std_logic_vector(4 downto 0) := (others =>'0');
  signal next_TTS_fifo_ra : std_logic_vector(4 downto 0) := (others =>'0');
  signal TTS_fifo_waSync : std_logic_vector(4 downto 0) := (others =>'0');
  signal TTS_fifo_di : std_logic_vector(5 downto 0) := (others =>'0');
  signal TTS_fifo_do : std_logic_vector(5 downto 0) := (others =>'0');
  signal sr : std_logic_vector(9 downto 0) := (others =>'0');
  signal TTS_fifo_we : std_logic := '0';
  signal TTS_fifo_re : std_logic := '0';
  signal NewTTS : std_logic := '0';
  signal TTS_clk_div : std_logic_vector(2 downto 0) := (others =>'0');
  signal SameTTS_cnt : std_logic_vector(1 downto 0) := (others =>'0');
  signal IS_K : std_logic := '0';
  signal enc_di : std_logic_vector(7 downto 0) := (others =>'0');
  signal enc_do : std_logic_vector(9 downto 0) := (others =>'0');
  signal bcnt : std_logic_vector(11 downto 0) := x"ff8";
  signal LastBcnt : std_logic_vector(11 downto 0);
  signal SendBC0 : std_logic := '0';
  signal SendBC0_O : std_logic := '0';
begin
LastBcnt <= x"fff" when flavor = "G2" else x"deb";
  process(sysclk)
  begin
    if(sysclk'event and sysclk = '1')then
      if(TTS_q = TTS)then
        TTS_FIFO_we <= '0';
      else
        TTS_FIFO_we <= '1';
      end if;
    end if;
  end process;
  process(sysclk,reset)
  begin
    if(reset = '1')then
      TTS_q <= (others =>'0');
      TTS_fifo_wa <= (others =>'0');
    elsif(sysclk'event and sysclk = '1')then
      TTS_q <= TTS;
      if(TTS_FIFO_we = '1')then
        TTS_fifo_wa <= next_TTS_fifo_wa;
      end if;
    end if;
  end process;
  i_next_TTS_fifo_wa : Gray5 PORT MAP(
    d_current => TTS_fifo_wa,
    d_next => next_TTS_fifo_wa
    );
  i_TTS_FIFO : RAM32X6D
    port map (
      wclk => sysclk,
      rclk => TTS_clk,
      di => TTS_FIFO_di,
      we => TTS_FIFO_we,
      wa => TTS_FIFO_wa,
      ra => TTS_FIFO_ra,
      ceReg => '1',
      do => TTS_FIFO_do
      );
  TTS_FIFO_di(3 downto 0) <= TTS_q;
  i_next_TTS_fifo_ra : Gray5 PORT MAP(
    d_current => TTS_fifo_ra,
    d_next => next_TTS_fifo_ra
    );
  process(TTS_clk,reset)
  begin
    if(reset = '1')then
      TTS_fifo_waSync <= (others =>'0');
      TTS_FIFO_re <= '0';
      NewTTS <= '1';
      IS_K <= '1';
      enc_di <= x"bc";
      TTS_fifo_ra <= (others =>'0');
      SameTTS_cnt <= "00";
    elsif(TTS_clk'event and TTS_clk = '1')then
      if(TTS_clk_div(2) = '1')then
        TTS_fifo_waSync <= TTS_fifo_wa;
        if(NewTTS = '1')then
          SameTTS_cnt <= "00";
        else
          SameTTS_cnt <= SameTTS_cnt + 1;
        end if;
        if(NewTTS = '1' or SameTTS_cnt = "11")then
          enc_di <= x"0" & TTS_FIFO_do(3 downto 0);
          IS_K <= '0';
        else
          enc_di <= x"bc"; -- K28.5
          IS_K <= '1';
        end if;
      end if;
      if(TTS_fifo_waSync /= TTS_fifo_ra and TTS_clk_div(1 downto 0) = "01")then
        TTS_FIFO_re <= '1';
      else
        TTS_FIFO_re <= '0';
      end if;
      if(TTS_FIFO_re = '1')then
        TTS_fifo_ra <= next_TTS_fifo_ra;
      end if;
      if(TTS_FIFO_re = '1')then
        NewTTS <= '1';
      elsif(TTS_clk_div(2) = '1')then
        NewTTS <= '0';
      end if;
    end if;
  end process;
  i_8b10b_enc : ENTITY work.encode_8b10b_lut_base
    GENERIC MAP (
      C_HAS_DISP_IN     => 0,
      C_HAS_FORCE_CODE  => 0,
      C_FORCE_CODE_VAL  => "1010101010",
      C_FORCE_CODE_DISP => 0,
      C_HAS_ND          => 0,
      C_HAS_KERR        => 1
      )
    PORT MAP (
      DIN               => enc_di,
      KIN               => IS_K,
      FORCE_DISP        => '0',
      FORCE_CODE        => '0',
      DISP_IN           => '0',
      CE                => TTS_clk_div(2),
      CLK               => TTS_clk,
      DOUT              => enc_do,
      KERR              => open,
      DISP_OUT          => open,
      ND                => open
      );
  process(TTS_clk)
  begin
    if(TTS_clk'event and TTS_clk = '1')then
      -- 5 state counter for 10bit 8b10char in groups of two
      if((TTS_clk_div(2) = '1' and local_TTC = '0') or (TTS_clk_div(1 downto 0) = "11" and local_TTC = '1'))then
        TTS_clk_div <= "000";
      else
        TTS_clk_div <= TTS_clk_div + 1;
      end if;
			if(TTS_clk_div(1 downto 0) = "11")then
				if(local_TTC = '0' or bcnt = LastBcnt)then
					bcnt <= (others => '0');
				else
					bcnt <= bcnt + 1; 
				end if;
				if(local_TTC = '0')then
					SendBC0 <= '0';
				elsif(bcnt = LastBcnt)then
					SendBC0 <= '1';
				elsif(bcnt(3 downto 0) = x"f")then
					SendBC0 <= '0';
				end if;
			end if;
      if(local_TTC = '1')then
--        sr(1) <= not sr(0);
--        sr(0) <= not sr(0);
				if(TTS_clk_div(0) = '0')then
					sr(1) <= not sr(0);
					sr(0) <= not sr(0);
				elsif(TTS_clk_div(1) = '0' and SendBC0_O = '1')then
					sr(1) <= not sr(0);
					sr(0) <= not sr(0);
				end if;
      elsif(TTS_clk_div(2) = '1')then
        sr <= enc_do;
      else
        for i in 0 to 3 loop
          sr(i*2+1 downto i*2) <= sr(i*2+3 downto i*2+2);
        end loop;
      end if;
    end if;
  end process;
  i_SendBC0 : ROM32X1
   generic map (
      INIT => X"e600ffff")
   port map (
      O => SendBC0_O,   -- ROM output
      A0 => bcnt(0), -- ROM address[0]
      A1 => bcnt(1), -- ROM address[1]
      A2 => bcnt(2), -- ROM address[2]
      A3 => bcnt(3), -- ROM address[3]
      A4 => SendBC0 -- ROM address[4]
   );
  i_TTS_out: OBUFDS generic map(IOSTANDARD => "LVDS_25") port map (O => TTS_out_p, OB => TTS_out_n, I => TTS_out);
  ODDR_inst : ODDR
    generic map(
      DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
      INIT => '0',   -- Initial value for Q port ('1' or '0')
      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
    port map (
      Q => TTS_out,   -- 1-bit DDR output
      C => TTS_clk,    -- 1-bit clock input
      CE => '1',  -- 1-bit clock enable input
      D1 => sr(0),  -- 1-bit data input (positive edge)
      D2 => sr(1),  -- 1-bit data input (negative edge)
      R => '0',    -- 1-bit reset input
      S => '0'     -- 1-bit set input
      );
end Behavioral;

