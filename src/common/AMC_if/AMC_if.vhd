----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:55:15 07/09/2010 
-- Design Name: 
-- Module Name:    AMC_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity AMC_if is
		Generic (simulation : boolean := false);
    Port ( sysclk : in  STD_LOGIC;
					 ipb_clk : in std_logic;
					 clk125 : in std_logic;
					 DRPclk : in std_logic;
					 GTXreset : in  STD_LOGIC;
					 reset : in  STD_LOGIC;
					 DB_cmd : IN std_logic;
					 ReSync : IN std_logic;
					 resetCntr : in  STD_LOGIC;
					 run : in  STD_LOGIC;
					 en_inject_err : in STD_LOGIC;
					 AllEventBuilt : out STD_LOGIC;
           Dis_pd : in  STD_LOGIC;
					 enSFP : IN std_logic_vector(3 downto 0);
					 en_localL1A : IN std_logic;
					 test : in  STD_LOGIC;
					 NoReSyncFake : in  STD_LOGIC;
					 WaitMonBuf : in  STD_LOGIC;
					 fake_length	: in   std_logic_vector(19 downto 0);
           T1_version : in  STD_LOGIC_VECTOR(7 downto 0);
           Source_ID : in  array3x12;
           AMC_en : in  STD_LOGIC_VECTOR(11 downto 0);
           AMC_Ready : out  STD_LOGIC_VECTOR(11 downto 0);
           BC0_lock : out  STD_LOGIC_VECTOR(11 downto 0);
					 TTC_lock : out  STD_LOGIC;
					 AMC_REFCLK_P : in  STD_LOGIC;
					 AMC_REFCLK_N : in  STD_LOGIC;
           AMC_RXN : in  STD_LOGIC_VECTOR(12 downto 1);
           AMC_RXP : in  STD_LOGIC_VECTOR(12 downto 1);
           AMC_TXN : out  STD_LOGIC_VECTOR(12 downto 1);
           AMC_TXP : out  STD_LOGIC_VECTOR(12 downto 1);
           AMC_status : out  STD_LOGIC_VECTOR(31 downto 0);
					 evt_data : out array3X67;-- bit 66 is TCP/mon space, bit 65 is end_of_event and bit 64 is end_of_block
					 evt_data_re : in std_logic_vector(2 downto 0);
					 evt_buf_full : in std_logic_vector(2 downto 0);
					 evt_data_we : out std_logic_vector(2 downto 0);
					 evt_data_rdy : out std_logic_vector(2 downto 0);
					 ddr_pa	: in  STD_LOGIC_VECTOR(9 downto 0);
					 MonBuf_empty : in STD_LOGIC;
					 mon_evt_wc	: out STD_LOGIC_VECTOR (47 downto 0);
					 mon_ctrl	: out STD_LOGIC_VECTOR (31 downto 0);
-- buffer control
					 mon_buf_avl : in STD_LOGIC; -- 
					 TCPbuf_avl : in STD_LOGIC; -- 
					 buf_rqst	: out STD_LOGIC_VECTOR (3 downto 0);
--	ipbus signals
					 ipb_write : in  STD_LOGIC;
					 ipb_strobe : in  STD_LOGIC;
					 ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_wdata : in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_ack : out  STD_LOGIC;
-- TTC & TTS signals
					 TTC_clk : in   std_logic;
					 TTC_LOS : in   std_logic;
					 BC0 : in  std_logic;
					 TTS_disable : in std_logic_vector(11 downto 0);
					 ttc_evcnt_reset : in std_logic;
					 event_number_avl : in std_logic;
					 event_number : in std_logic_vector(59 downto 0);
					 evn_buf_full : out std_logic;
					 ovfl_warning : out std_logic;
					 TrigData	: out  array12x8;
					 TTS_RQST : out  std_logic_vector(2 downto 0);
					 TTS_coded : out  std_logic_vector(4 downto 0)
					);
end AMC_if;

architecture Behavioral of AMC_if is
COMPONENT fake_event
	PORT(
		sysclk : IN std_logic;
		UsrClk : IN std_logic;
		reset : IN std_logic;
		fifo_rst : IN std_logic;
		fifo_en : IN std_logic;
		sync : IN std_logic;
    LinkFull : in  STD_LOGIC;
		fake_en : IN std_logic;
		ovfl_warning : in  STD_LOGIC;
		fake_length	: in   std_logic_vector(19 downto 0);
		L1A_DATA : IN std_logic_vector(15 downto 0);
		L1A_WrEn : IN std_logic;          
		fake_header : OUT std_logic;
		fake_CRC : OUT std_logic;
		empty_event_flag : out  STD_LOGIC;
		fake_DATA : OUT std_logic_vector(15 downto 0);
		fake_WrEn : OUT std_logic
		);
END COMPONENT;
COMPONENT AMC_Link
	generic(N : integer := 14; simulation : boolean := false); -- M controls FIFO size, N controls timeout
	PORT(
		sysclk : IN std_logic;
		reset : IN std_logic;
		resetCntr : IN std_logic;
    fifo_rst                            : in   std_logic;
    fifo_en		                          : in   std_logic;
		test : IN std_logic;
    strobe2ms                           : in   std_logic;
		NoReSyncFake : IN std_logic;
		UsrClk : IN std_logic;
		AMC_ID : IN std_logic_vector(3 downto 0);
    txfsmresetdone	                 		: in   std_logic;
    RxResetDone	                    		: in   std_logic;
    qpll_lock	                       		: in   std_logic;
    rxcommaalignen                 			: out  std_logic;
    DATA_VALID                     			: out  std_logic;
    RXDATA		                      		: in   std_logic_vector(15 downto 0);
    RXCHARISCOMMA                     	: in   std_logic_vector(1 downto 0);
    RXCHARISK		                      	: in   std_logic_vector(1 downto 0);
    RXNOTINTABLE                      	: in   std_logic_vector(1 downto 0);
    TXDATA     	                    		: out  std_logic_vector(15 downto 0);
    TXCHARISK                        		: out  std_logic_vector(1 downto 0);
		AMC_en : IN std_logic;
		TTS_disable : IN std_logic;
		AMC_DATA_RdEn : IN std_logic;
		EventInfoRdDone : IN std_logic;
		L1A_DATA : IN std_logic_vector(15 downto 0);
		L1A_WrEn : IN std_logic;
		fake_header : IN std_logic;
		fake_CRC : IN std_logic;
		fake_DATA : IN std_logic_vector(15 downto 0);
		fake_WrEn : IN std_logic;
		fake_full : OUT std_logic;
		Cntr_ADDR : IN std_logic_vector(11 downto 0);
		TTCclk : IN std_logic;
		BC0 : IN std_logic;
		TTC_LOS : IN std_logic;          
		Ready : OUT std_logic;
		AMCinfo : OUT std_logic_vector(15 downto 0);
		EventInfo : OUT std_logic_vector(31 downto 0);
		EventInfo_dav : OUT std_logic;
		AMC_DATA : OUT std_logic_vector(63 downto 0);
		bad_AMC : OUT std_logic;
		AMC_OK : OUT std_logic;
		Cntr_DATA : OUT std_logic_vector(15 downto 0);
		debug_out : OUT std_logic_vector(255 downto 0);
		TTC_status : OUT std_logic_vector(127 downto 0);
		TrigData : OUT std_logic_vector(7 downto 0);
		TTS_RQST : OUT std_logic_vector(2 downto 0);
		TTS_coded : OUT std_logic_vector(4 downto 0)
		);
END COMPONENT;
COMPONENT AMC_wrapper
    Port ( DRPclk : in  STD_LOGIC;
           SOFT_RESET : in  STD_LOGIC;
           UsrClk : in  STD_LOGIC;
           test : in  STD_LOGIC;
           Dis_pd : in  STD_LOGIC;
           AMC_en : in  STD_LOGIC_VECTOR(11 downto 0);
					 RXDATA : out array12X16;
           RxBufOvf : out  STD_LOGIC_VECTOR(11 downto 0);
           RxBufUdf : out  STD_LOGIC_VECTOR(11 downto 0);
           sampleRatio : in  STD_LOGIC;
           updateRatio : in  STD_LOGIC;
           RxClkRatio : out  array12x21;
           rxprbserr : out  STD_LOGIC_VECTOR(11 downto 0);
					 rxprbssel : in array12X3;
           RXNOTINTABLE : out  array12X2;
           rxcommaalignen : in  STD_LOGIC_VECTOR(11 downto 0);
					 rxchariscomma : out array12X2;
					 rxcharisk : out array12X2;
           rxresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
					 txdiffctrl : in array12X4;
					 TXDATA : in array12X16;
           txoutclk : out  STD_LOGIC_VECTOR(11 downto 0);
					 txcharisk : in array12X2;
           txresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
					 txprbssel : in array12X3;
           qpll_lock : out  STD_LOGIC_VECTOR(2 downto 0);
           txfsmresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
           rxfsmresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
           data_valid : in  STD_LOGIC_VECTOR(11 downto 0);
           AMC_REFCLK : in  STD_LOGIC;
           RXN : in  STD_LOGIC_VECTOR(11 downto 0);
           RXP : in  STD_LOGIC_VECTOR(11 downto 0);
           TXN : out  STD_LOGIC_VECTOR(11 downto 0);
           TXP : out  STD_LOGIC_VECTOR(11 downto 0)
					 );
END COMPONENT;
COMPONENT evt_bldr
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		fifo_rst : IN std_logic;
		fifo_en : IN std_logic;
		en_inject_err : IN std_logic;
		OneSFP : IN std_logic;
		Source_ID : IN std_logic_vector(7 downto 0);
		block_wc : in  STD_LOGIC_VECTOR (15 downto 0);
		block_wc_we : in STD_LOGIC;
		AMC_wc : IN std_logic_vector(17 downto 0);
		AMC_wc_we : IN std_logic;
		AMC_wc_end : IN std_logic;
		AMC_header : IN std_logic_vector(65 downto 0);
		AMC_header_we : IN std_logic;
		AMC_DATA : IN array12X64;
		evt_buf_full : IN std_logic;
		evt_data_re : IN std_logic;          
		bldr_fifo_full : OUT std_logic;
		AMC_DATA_re : OUT std_logic_vector(11 downto 0);
		AMCCRC_bad : OUT std_logic_vector(11 downto 0);
		evt_data : OUT std_logic_vector(66 downto 0);
		evt_data_we : OUT std_logic;
		evt_data_rdy : OUT std_logic;
		debug : out  STD_LOGIC_VECTOR (255 downto 0);
		EventBuilt : OUT std_logic
		);
END COMPONENT;
COMPONENT AMC_cntr
PORT(
		 UsrClk : IN  std_logic;
		 clk125 : IN  std_logic;
		 sysclk : IN  std_logic;
		 ipb_clk : IN  std_logic;
		 resetCntr : IN  std_logic;
		 DB_cmd : IN  std_logic;
		 AMC_if_data : IN  std_logic_vector(15 downto 0);
		 Cntr_DATA : IN  array12x16;
		 Cntr_ADDR : OUT  std_logic_vector(11 downto 0);
		 ipb_addr : IN  std_logic_vector(15 downto 0);
		 ipb_rdata : OUT  std_logic_vector(31 downto 0)
		);
END COMPONENT;
COMPONENT cmsCRC64
	PORT(
		clk : IN std_logic;
		reset : IN std_logic;
		data_in : IN std_logic_vector(63 downto 0);
		ctrl_in : IN std_logic;
		we_in : IN std_logic;          
		crc : OUT std_logic_vector(15 downto 0);
		crc_err : OUT std_logic;
		data_out : OUT std_logic_vector(63 downto 0);
		ctrl_out : OUT std_logic;
		we_out : OUT std_logic
		);
END COMPONENT;
COMPONENT FIFO_RESET_7S
	PORT(
		reset : IN std_logic;
		clk : IN std_logic;          
		fifo_rst : OUT std_logic;
		fifo_en : OUT std_logic
		);
END COMPONENT;
COMPONENT RAM32x6Db
	PORT(
		wclk : IN std_logic;
		di : IN std_logic_vector(5 downto 0);
		we : IN std_logic;
		wa : IN std_logic_vector(4 downto 0);
		ra : IN std_logic_vector(4 downto 0);          
		do : OUT std_logic_vector(5 downto 0)
		);
END COMPONENT;
COMPONENT RAM32x8
	PORT(
		wclk : IN std_logic;
		di : IN std_logic_vector(7 downto 0);
		we : IN std_logic;
		wa : IN std_logic_vector(4 downto 0);
		ra : IN std_logic_vector(4 downto 0);          
		do : OUT std_logic_vector(7 downto 0)
		);
END COMPONENT;
type array_x12y256 is array(11 downto 0) of std_logic_vector(255 downto 0);
type array_x12y16 is array(11 downto 0) of std_logic_vector(15 downto 0);
type array_x12y32 is array(11 downto 0) of std_logic_vector(31 downto 0);
type array_x12y8 is array(11 downto 0) of std_logic_vector(7 downto 0);
type array_x12y5 is array(11 downto 0) of std_logic_vector(4 downto 0);
type array_x12y128 is array(11 downto 0) of std_logic_vector(127 downto 0);
constant AMC_ID : array12x4 := (x"0",x"1",x"2",x"3",x"4",x"5",x"6",x"7",x"8",x"9",x"a",x"b");
constant AMC_txdiffctrl : array12x4 := (others => x"b");
constant uFOV : std_logic_vector(3 downto 0) := x"1";
signal kAMC : array2x4 := (others => (others => '0'));
signal mAMC : array3x4 := (others => (others => '0'));
signal nAMC : array3x4 := (others => (others => '0'));
signal EventInfo : array_x12y32 := (others => (others => '0'));
signal AMCinfo : array12X16 := (others => (others => '0'));
signal AMC_DATA : array12X64 := (others => (others => '0'));
signal AMC_DATA1 : array12X64 := (others => (others => '0'));
signal AMC_DATA2 : array12X64 := (others => (others => '0'));
signal Cntr_DATA : array12x16 := (others => (others => '0'));
signal AMCCRC_bad : array3X12 := (others => (others => '0'));
signal AMC_TTS : array_x12y8 := (others => (others => '0'));
signal AMC_TTS_RQST : array12X3 := (others => (others => '0'));
signal AMC_debug : array_x12y256 := (others => (others => '0'));
signal TTC_status : array_x12y128 := (others => (others => '0'));
signal badEventCRC_cntr : array12X16 := (others => (others => '0'));
signal ReSyncFakeEvent_cntr : array12X16 := (others => (others => '0'));
signal EventInfo_dav : std_logic_vector(11 downto 0);
--signal EventInfo_dav_n : std_logic_vector(11 downto 0);
signal EventInfoRdDone : std_logic_vector(12 downto 0) := (others => '0');
signal AMC_DATA_RdEn : std_logic_vector(11 downto 0) := (others => '0');
signal Cntr_ADDR : std_logic_vector(11 downto 0) := (others => '0');
signal AMC_if_ADDR : std_logic_vector(7 downto 0) := (others => '0');
signal ipb_strobe_q : std_logic := '0';
signal UsrClk : std_logic := '0';
signal UsrClk_out : std_logic_vector(11 downto 0) := (others => '0');
signal resetCntr_SyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal resetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal CntrRst : std_logic := '0';
signal rst_AMC_cntr : std_logic := '0';
signal evn_out : std_logic_vector(59 downto 0) := (others => '0');
signal evn_wa : std_logic_vector(8 downto 0) := (others => '0');
signal evn_ra : std_logic_vector(8 downto 0) := (others => '0');
signal evt_cnt : array3x8 := (others => (others => '0'));
signal evn : std_logic_vector(23 downto 0) := (others => '0');
signal CDF_in : std_logic_vector(71 downto 0) := (others => '0');
signal CDF_out : std_logic_vector(71 downto 0) := (others => '0');
signal CDF_wa : std_logic_vector(8 downto 0) := (others => '0');
signal CDF_ra : std_logic_vector(8 downto 0) := (others => '0');
signal CDF_cnt : std_logic_vector(7 downto 0) := (others => '0');
signal empty_event_flag : std_logic := '0';
signal evn_buf_full_i : std_logic_vector(2 downto 0) := (others => '0');
signal ovfl_warning_i : std_logic_vector(3 downto 0) := (others => '0');
signal ovfl_warning_p : std_logic := '0';
signal header : std_logic := '0';
signal init_bldr : std_logic := '0';
signal CDF_header : std_logic := '0';
signal CDF_empty : std_logic := '0';
signal BlockHeader : std_logic := '0';
signal ec_sel_AMC : std_logic := '0';
signal sel_AMC : std_logic_vector(3 downto 0) := (others => '0');
signal LastBlock : std_logic := '0';
signal FirstBlock : std_logic_vector(2 downto 0) := (others => '0');
signal sel_CDF : std_logic := '0';
signal evn_empty : std_logic := '0';
signal sel_evn : std_logic_vector(1 downto 0) := (others => '0');
signal L1A_WrEn : std_logic := '0';
signal L1A_DATA : std_logic_vector(15 downto 0) := (others => '0');
signal EvtTy : std_logic_vector(3 downto 0) := (others => '0');
signal CalTy : std_logic_vector(3 downto 0) := (others => '0');
signal EventInfo_avl : std_logic := '0';
signal rst_init_bldr : std_logic := '0';
signal Builder_busy : std_logic_vector(2 downto 0) := (others => '0');
signal ec_CDF_ra : std_logic := '0';
signal summary : std_logic_vector(63 downto 0) := (others => '0');
signal AMC_TTC_status : std_logic_vector(31 downto 0) := (others => '0');
signal fake_en: std_logic := '0';
signal fake_DATA : std_logic_vector(15 downto 0) := (others => '0');
signal fake_header : std_logic := '0';
signal fake_CRC : std_logic := '0';
signal fake_WrEn : std_logic := '0';
signal fake_word_cnt : std_logic_vector(15 downto 0) := (others => '0');
signal fake_evt_cnt : std_logic_vector(15 downto 0) := (others => '0');
signal empty_evt_cnt : std_logic_vector(15 downto 0) := (others => '0');
signal fake_header_cnt: std_logic_vector(15 downto 0) := (others => '0');
signal EventBuilt : std_logic_vector(2 downto 0) := (others => '0');
signal badEventCRCToggle : std_logic_vector(11 downto 0) := (others => '0');
signal ReSyncFakeEventToggle : std_logic_vector(11 downto 0) := (others => '0');
signal EventBuiltToggle : std_logic_vector(2 downto 0) := (others => '0');
signal badEventCRCToggleSyncRegs : array12x4 := (others => (others => '0'));
signal ReSyncFakeEventToggleSyncRegs : array12x4 := (others => (others => '0'));
signal EventBuiltToggleSyncRegs : array3x4 := (others => (others => '0'));
signal EventBuiltCnt : array3x16 := (others => (others => '0'));
signal next_bldr : std_logic_vector(1 downto 0) := (others => '0');
signal AMC_wcp : array12x13 := (others => (others => '0'));
signal AMC_wc : std_logic_vector(17 downto 0) := (others => '0');
signal AMC_wc_we : std_logic_vector(2 downto 0) := (others => '0');
signal en_block_wc : std_logic_vector(2 downto 0) := (others => '0');
signal block_wc_we : std_logic_vector(2 downto 0) := (others => '0');
signal AMC_wc_mask : std_logic_vector(2 downto 0) := (others => '0');
signal AMC_header : array3x66 := (others => (others => '0'));
signal AMC_header_we : std_logic_vector(2 downto 0) := (others => '0');
signal bldr_fifo_full : std_logic_vector(2 downto 0) := (others => '0');
type array3X12 is array(0 to 2) of std_logic_vector(11 downto 0);
signal AMC_DATA_re : array3X12 := (others => (others => '0'));
signal AMC_hasData : std_logic_vector(11 downto 0) := (others =>'0');
signal Mbit_word : std_logic_vector(11 downto 0) := (others =>'0');
--signal AMC_hasData_l : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_REFCLK : std_logic := '0';
signal AMC_TTS_OR : std_logic_vector(7 downto 0) := (others =>'0');
signal AMC_TTS_RQST_OR : std_logic_vector(2 downto 0) := (others =>'0');
signal AMC_qpll_lock : std_logic_vector(2 downto 0) := (others =>'0');
signal AMC_rxprbserr : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_rxcommaalignen : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_rxresetdone : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_txfsmresetdone : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_rxfsmresetdone : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_data_valid : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_RXDATA : array12x16 := (others => (others => '0'));
signal AMC_TXDATA : array12x16 := (others => (others => '0'));
signal AMC_RXNOTINTABLE : array12x2 := (others => (others => '0'));
signal AMC_rxchariscomma : array12x2 := (others => (others => '0'));
signal AMC_rxcharisk : array12x2 := (others => (others => '0'));
signal AMC_txcharisk : array12x2 := (others => (others => '0'));
signal AMC_rxprbssel : array12x3 := (others => (others => '0'));
signal AMC_txprbssel : array12x3 := (others => (others => '0'));
type array3X4 is array(0 to 2) of std_logic_vector(3 downto 0);
--signal AMC_rdata : array12x32 := (others => (others => '0'));
signal channel : array3X4 := (others => (others => '0'));
-- monitor signals
signal mon_wc: array3X16 := (others => (others => '0'));
signal mon_evt_wcp : std_logic_vector(47 downto 0) := (others => '0');
signal zero_wc : std_logic_vector(2 downto 0) := (others => '0');
signal more_wc : std_logic_vector(2 downto 0) := (others => '0');
signal mon_en: std_logic := '0';
signal scale_cntr : std_logic_vector(15 downto 0) := (others => '0');
signal ce_scale : std_logic := '0';
signal ld_scale : std_logic := '0';
--signal MonBufAbort: std_logic := '0';
signal rst_mon_wc: std_logic := '0';
signal ce_wc_reg_wa: std_logic := '0';
signal wc_reg_wa: std_logic_vector(9 downto 0) := (others => '0');
--signal start_wc_reg_wa: std_logic_vector(9 downto 0) := (others => '0');
signal down_count : std_logic := '0';
signal mon_mask: std_logic_vector(19 downto 0) := (others => '0');
signal sample_event: std_logic := '0';
signal scale : std_logic_vector(31 downto 0) := (others => '0');
signal pending : std_logic := '0';
signal AMC_Ready_i : std_logic_vector(11 downto 0);
signal AMC_OK : std_logic_vector(11 downto 0);
signal block_num : std_logic_vector(11 downto 0) := (others => '0');
signal resetFIFO : std_logic := '0';
signal fifo_rst : std_logic := '0';
signal fifo_en : std_logic := '0';
signal resetFIFO_AMC : std_logic := '0';
signal fifo_rst_AMC : std_logic := '0';
signal fifo_en_AMC : std_logic := '0';
signal OneSFP : std_logic := '0';
signal TwoSFP : std_logic := '0';
signal ThreeSFP : std_logic := '0';
signal fake_full : std_logic_vector(11 downto 0) := (others => '0');
signal LinkFull : std_logic := '0';
signal EventInSlink : array3x4 := (others => (others => '0'));
signal TTS_FIFO_do : std_logic_vector(7 downto 0) := (others => '0');
signal TTS_FIFO_di : std_logic_vector(7 downto 0) := (others => '0');
signal TTS_FIFO_wa : std_logic_vector(4 downto 0) := (others => '0');
signal TTS_FIFO_ra : std_logic_vector(4 downto 0) := (others => '0');
signal TTS_FIFO_waSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal TTS_FIFO_waSyncRegs2 : std_logic_vector(2 downto 0) := (others => '0');
signal TTS_FIFO_waSyncRegs3 : std_logic_vector(2 downto 0) := (others => '0');
signal evt_bldr_debug : STD_LOGIC_VECTOR(255 DOWNTO 0);
signal stop_mon : std_logic := '0';
signal errors : std_logic_vector(7 downto 0) := (others => '0');
signal err_TTS : std_logic_vector(7 downto 0) := (others => '0');
signal AMC_wc_sum_we : std_logic := '0';
signal rst_AMC_wc_sum : std_logic := '0';
signal wr_AMC_wc_sum : std_logic := '0';
signal sel_AMC_q : std_logic_vector(3 downto 0) := (others => '0');
signal AMC_wc_sum_di : std_logic_vector(5 downto 0) := (others => '0');
signal AMC_wc_sum_do : std_logic_vector(5 downto 0) := (others => '0');
signal AMC_wc_sum_a : std_logic_vector(4 downto 0) := (others => '0');
signal enRstAMC_link : std_logic := '0';
signal RstAMC_link : std_logic := '0';
signal RstAMC_link_dl : std_logic := '0';
signal RstAMC_linkSync : std_logic_vector(3 downto 0) := (others => '0');
signal AllEventBuilt_i : std_logic := '0';
signal event_number_avl_q : std_logic_vector(2 downto 0) := (others => '0');
signal bcnt : std_logic_vector(11 downto 0) := (others => '0');
signal event_cnt : std_logic_vector(23 downto 0) := (others => '0');
signal event_status : std_logic_vector(19 downto 0) := (others => '0');
signal L1A_buf_we : std_logic := '0';
signal L1A_buf_do : std_logic_vector(31 downto 0) := (others => '0');
signal L1A_buf_di : std_logic_vector(31 downto 0) := (others => '0');
signal L1A_buf_wa : std_logic_vector(8 downto 0) := (others => '0');
signal RxBufUdfErr : std_logic_vector(11 downto 0) := (others => '0');
signal RxBufOvfErr : std_logic_vector(11 downto 0) := (others => '0');
signal RxBufOvf : std_logic_vector(11 downto 0) := (others => '0');
signal RxBufUdf : std_logic_vector(11 downto 0) := (others => '0');
signal RxClkCntr : std_logic_vector(19 downto 0) := (others => '0');
signal RxClkCntr19_q : std_logic := '0';
signal updateRatio : std_logic := '0';
signal RxClkRatio : array12x21 := (others => (others => '0'));
signal AMC_if_RdEn : std_logic := '0';
signal AMC_if_data : std_logic_vector(15 downto 0) := (others => '0');
signal AMC_cntr_data : std_logic_vector(31 downto 0) := (others => '0');
signal strobe2ms : std_logic := '0';
signal Cntr2ms : std_logic_vector(18 downto 0) := (others => '0');
component icon2
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

end component;
component ila128x4096
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    DATA : IN STD_LOGIC_VECTOR(143 DOWNTO 0);
    TRIG0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    TRIG1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    TRIG2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0));

end component;
signal CONTROL0 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL1 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal DATA0 : STD_LOGIC_VECTOR(143 DOWNTO 0);
signal TRIG0 : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal TRIG1 : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal TRIG2 : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal DATA1 : STD_LOGIC_VECTOR(143 DOWNTO 0);
signal TRIG0b : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal TRIG1b : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal TRIG2b : STD_LOGIC_VECTOR(7 DOWNTO 0);
component chipscope1
		generic (N : integer := 5);
    Port ( clk : in  STD_LOGIC;
           Din : in  STD_LOGIC_VECTOR (303 downto 0));
end component;
signal CS : STD_LOGIC_VECTOR(303 DOWNTO 0) := (others => '0');
begin
--i_chipscope : chipscope1 port map(clk => UsrClk, Din => cs);
--cs(61 downto 0) <= AMC_debug(3)(61 downto 0);
--cs(289) <= AMC_debug(3)(44);
--cs(288) <= AMC_debug(3)(40);
--i_chipscope : chipscope1 port map(clk => UsrClk, Din => cs);
--cs(288) <= AMC_debug(1)(48);
--cs(48 downto 0) <= AMC_debug(1)(48 downto 0);
--cs(303 downto 296) <= cs(212) & cs(210 downto 204);
--cs(295 downto 288) <= cs(147) & cs(145 downto 139);
--cs(295 downto 288) <= AMC_debug(0)(80) & AMC_debug(0)(62 downto 56);
--cs(288) <= AMC_debug(4)(82);
--cs(287) <= sel_CDF;
--cs(264 downto 183) <= AMC_debug(4)(81 downto 0);
--cs(182 downto 0) <= evt_bldr_debug(182 downto 0);
--i_icon : icon2
--  port map (
--    CONTROL0 => CONTROL0,
--    CONTROL1 => CONTROL1);
--i_ila : ila128x4096
--      port map (
--        CONTROL => CONTROL0,
--        CLK => UsrClk,
--        DATA => DATA0,
--        TRIG0 => TRIG0,
--        TRIG1 => x"00",
--        TRIG2 => x"00");
--DATA0(3 downto 0) <= TTC_status(10)(84 downto 81);
--DATA0(16 downto 4) <= TTC_status(10)(18 downto 6);
--DATA0(44 downto 17) <= TTC_status(10)(118 downto 91);
--DATA0(46 downto 45) <= AMC_rxchariscomma(10);
--DATA0(48 downto 47) <= AMC_rxcharisk(10);
--DATA0(64 downto 49) <= AMC_RXDATA(10);
--DATA0(66 downto 65) <= TTC_status(10)(53 downto 52);
--DATA0(70 downto 67) <= TTC_status(9)(84 downto 81);
--DATA0(83 downto 71) <= TTC_status(9)(18 downto 6);
--DATA0(85 downto 84) <= TTC_status(9)(53 downto 52);
--DATA0(87 downto 86) <= AMC_rxchariscomma(9);
--DATA0(89 downto 88) <= AMC_rxcharisk(9);
--DATA0(105 downto 90) <= AMC_RXDATA(9);
--DATA0(109 downto 106) <= TTC_status(1)(84 downto 81);
--DATA0(122 downto 110) <= TTC_status(1)(18 downto 6);
--DATA0(124 downto 123) <= TTC_status(1)(53 downto 52);
--DATA0(125) <= AMC_rxchariscomma(1)(0);
--DATA0(127 downto 126) <= AMC_rxcharisk(1);
--DATA0(143 downto 128) <= AMC_RXDATA(1);
--TRIG0(1 downto 0) <= TTC_status(10)(53 downto 52) ;
--TRIG0(3 downto 2) <= TTC_status(9)(53 downto 52) ;
--TRIG0(5 downto 4) <= TTC_status(1)(53 downto 52) ;
--TRIG0(7 downto 6) <= "00" ;
--i_ila_b : ila128x4096
--          port map (
--            CONTROL => CONTROL1,
--            CLK => TTC_clk,
--            DATA => DATA1,
--            TRIG0 => TRIG1,
--            TRIG1 => x"00",
--            TRIG2 => x"00");
--DATA1(0) <= BC0;
--DATA1(9 downto 1) <= TTC_status(10)(35 downto 27);
--DATA1(46 downto 10) <= TTC_status(10)(90 downto 54);
--DATA1(55 downto 47) <= TTC_status(9)(35 downto 27);
--DATA1(92 downto 56) <= TTC_status(9)(90 downto 54);
--DATA1(101 downto 93) <= TTC_status(1)(35 downto 27);
--DATA1(138 downto 102) <= TTC_status(1)(90 downto 54);
--DATA1(142 downto 139) <= bcnt;
--TRIG1(0) <= BC0;
--TRIG1(2 downto 1) <= TTC_status(10)(31 downto 30);
--TRIG1(4 downto 3) <= TTC_status(9)(31 downto 30);
--TRIG1(6 downto 5) <= TTC_status(1)(31 downto 30);
--TRIG1(7) <= '0';
AllEventBuilt <= AllEventBuilt_i;
AMC_Ready <= AMC_Ready_i;
mon_ctrl <= scale;
process(sysclk,reset)
begin
	if(reset = '1')then
		resetSyncRegs <= (others => '1');
		RstAMC_link <= '1';
	elsif(sysclk'event and sysclk = '1')then
		resetSyncRegs <= resetSyncRegs(1 downto 0) & '0';
		if(enRstAMC_link = '1' and AllEventBuilt_i = '1')then
			RstAMC_link <= '1';
		else
			RstAMC_link <= resetSyncRegs(1);
		end if;
	end if;
end process;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			L1A_buf_wa <= (others => '0');
		elsif(L1A_buf_we = '1')then
			L1A_buf_wa <= L1A_buf_wa + 1;
		end if;
		if(resetSyncRegs(2) = '1' or ttc_evcnt_reset = '1')then
			event_cnt <= (others => '0');
		elsif(event_number_avl = '1')then
			event_cnt <= event_cnt + 1;
		end if;
		event_number_avl_q <= event_number_avl_q(1 downto 0) & event_number_avl;
		if(resetSyncRegs(2) = '1')then
			L1A_buf_we <= '0';
		else
			L1A_buf_we <= event_number_avl or or_reduce(event_number_avl_q);
		end if;
		if(event_number_avl = '1')then
			L1A_buf_di <= event_number(43 downto 12); -- OcN
			bcnt <= event_number(11 downto 0); -- OcN
			event_status <= event_number(59 downto 44) &"00" &  event_number(45) & not event_number(45);
		elsif(event_number_avl_q(0) = '1')then
			L1A_buf_di <= x"00000" & bcnt; -- bcnt
		elsif(event_number_avl_q(1) = '1')then
			L1A_buf_di <= x"00" & event_cnt; -- bcnt
		else
			L1A_buf_di <= x"000" & event_status; -- bcnt
		end if;
	end if;
end process;
--		if(en_cal_win = '0')then
--			event_number(51 downto 48) <= x"0";
--			event_number(44) <= '0';
--		else
--			event_number(51) <= cal_win and brcst_GapTrig and cal_type(3) and not brcst_GapPed;
--			event_number(50) <= cal_win and brcst_GapTrig and cal_type(2) and not brcst_GapPed;
--			event_number(49) <= cal_win and brcst_GapTrig and cal_type(1) and not brcst_GapPed;
--			event_number(48) <= cal_win and ((brcst_GapTrig and cal_type(0)) or brcst_GapPed);
--			event_number(44) <= cal_win and (brcst_GapTrig or brcst_GapPed);
--		end if;
--		event_number(59 downto 56) <= cal_type;
--		event_number(55 downto 52) <= state;
--		event_number(47) <= brcst_GapTrig;
--		event_number(46) <= brcst_GapPed;
--		event_number(45) <= cal_win;
--		event_number(43 downto 0) <= oc & bcnt;
-- receiving L1 information
i_L1A_buf : BRAM_SDP_MACRO
   generic map (
      BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 32,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 32)     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
   port map (
      DO => L1A_buf_do,         -- Output read data port, width defined by READ_WIDTH parameter
      DI => L1A_buf_di,         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => ipb_addr(8 downto 0), -- Input read address, width defined by read port depth
      RDCLK => sysclk,   -- 1-bit input read clock
      RDEN => '1',     -- 1-bit input read port enable
      REGCE => '1',   -- 1-bit input read output register enable
      RST => '0',       -- 1-bit input reset 
      WE => x"f",         -- Input write enable, width defined by write port depth
      WRADDR => L1A_buf_wa, -- Input write address, width defined by write port depth
      WRCLK => sysclk,   -- 1-bit input write clock
      WREN => L1A_buf_we      -- 1-bit input write port enable
   );
ovfl_warning <= ovfl_warning_i(3);
i_evn : BRAM_SDP_MACRO
   generic map (
      BRAM_SIZE => "36Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 60,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 60,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      DO_REG => 0, -- Optional output register (0 or 1)
      SIM_COLLISION_CHECK => "NONE", -- Collision check enable "ALL", "WARNING_ONLY", 
                                    -- "GENERATE_X_ONLY" or "NONE"       
      WRITE_MODE => "WRITE_FIRST", -- Specify "READ_FIRST" for same clock or synchronous clocks
                                   --  Specify "WRITE_FIRST for asynchrononous clocks on ports
      INIT => X"000000000000000000") --  Initial values on output port
   port map (
      DO => evn_out,         -- Output read data port, width defined by READ_WIDTH parameter
      DI => event_number,         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => evn_ra, -- Input read address, width defined by read port depth
      RDCLK => sysclk,   -- 1-bit input read clock
      RDEN => '1',     -- 1-bit input read port enable
      REGCE => '1',   -- 1-bit input read output register enable
      RST => '0',       -- 1-bit input reset 
      WE => x"ff",         -- Input write enable, width defined by write port depth
      WRADDR => evn_wa, -- Input write address, width defined by write port depth
      WRCLK => sysclk,   -- 1-bit input write clock
      WREN => event_number_avl      -- 1-bit input write port enable
   );
process(sysclk)
variable enable : std_logic_vector(2 downto 0);
begin
	if(ThreeSFP = '1')then
		enable := "111";
	elsif(TwoSFP = '1')then
		enable := "011";
	else
		enable := "001";
	end if;
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			evn_wa <= (others => '0');
		elsif(event_number_avl = '1')then
			evn_wa <= evn_wa + 1;
		end if;
		if(resetSyncRegs(2) = '1')then
			evn_ra <= (others => '0');
		elsif(sel_evn = "10")then
			evn_ra <= evn_ra + 1;
		end if;
		if(ttc_evcnt_reset = '1' or resetSyncRegs(2) = '1')then
			evn <= x"000001";
		elsif(sel_evn = "10")then
			evn <= evn + 1;
		end if;
		for i in 0 to 2 loop
			if(resetSyncRegs(2) = '1' or enable(i) = '0')then
				evt_cnt(i) <= (others => '0');
			elsif(event_number_avl = '1' and EventBuilt(i) = '0')then
				evt_cnt(i) <= evt_cnt(i) + 1;
			elsif(event_number_avl = '0' and EventBuilt(i) = '1')then
				evt_cnt(i) <= evt_cnt(i) - 1;
			end if;
			if(and_reduce(evt_cnt(i)(7 downto 5)) = '1')then
				evn_buf_full_i(i) <= '1';
			else
				evn_buf_full_i(i) <= '0';
			end if;
	-- when reached 0x60, throttle L1A. Return only after go below 0x40
			if(or_reduce(evt_cnt(i)(7 downto 6)) = '0')then
				ovfl_warning_i(i) <= '0';
			elsif(evt_cnt(i)(5) = '1')then
				ovfl_warning_i(i) <= '1';
			end if;
		end loop;
		evn_buf_full <= or_reduce(evn_buf_full_i);
		ovfl_warning_i(3) <= or_reduce(ovfl_warning_i(2 downto 0)) or (or_reduce(TTS_FIFO_do(4 downto 0)) and en_localL1A);
	end if;
end process;
-- send L1info to AMC_Link
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1' or evn_wa = evn_ra)then
			evn_empty <= '1';
		else
			evn_empty <= '0';
		end if;
		L1A_WrEn <= not evn_empty;
		if(resetSyncRegs(2) = '1' or evn_empty = '1')then
			sel_evn <= "00";
		else
			sel_evn(1) <= sel_evn(1) xor sel_evn(0);
			sel_evn(0) <= not sel_evn(0);
		end if;
		case sel_evn is
			when "00" => L1A_DATA <= evn_out(11 downto 0) & x"0"; -- BX
			when "01" => L1A_DATA <= evn(15 downto 0);
			when "10" => L1A_DATA <= x"00" & evn(23 downto 16);
			when others => L1A_DATA <= evn_out(27 downto 12); -- OrN
		end case;
	end if;
end process;
i_CDF : BRAM_SDP_MACRO
   generic map (
      BRAM_SIZE => "36Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 72,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 72,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      DO_REG => 0, -- Optional output register (0 or 1)
      SIM_COLLISION_CHECK => "NONE", -- Collision check enable "ALL", "WARNING_ONLY", 
                                    -- "GENERATE_X_ONLY" or "NONE"       
      WRITE_MODE => "WRITE_FIRST", -- Specify "READ_FIRST" for same clock or synchronous clocks
                                   --  Specify "WRITE_FIRST for asynchrononous clocks on ports
      INIT => X"000000000000000000") --  Initial values on output port
   port map (
      DO => CDF_out,         -- Output read data port, width defined by READ_WIDTH parameter
      DI => CDF_in,         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => CDF_ra, -- Input read address, width defined by read port depth
      RDCLK => sysclk,   -- 1-bit input read clock
      RDEN => '1',     -- 1-bit input read port enable
      REGCE => '1',   -- 1-bit input read output register enable
      RST => '0',       -- 1-bit input reset 
      WE => x"ff",         -- Input write enable, width defined by write port depth
      WRADDR => CDF_wa, -- Input write address, width defined by write port depth
      WRCLK => sysclk,   -- 1-bit input write clock
      WREN => sel_evn(0)      -- 1-bit input write port enable
   );
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			CDF_wa <= (others => '0');
		elsif(sel_evn(0) = '1')then
			CDF_wa <= CDF_wa + 1;
		end if;
		if(resetSyncRegs(2) = '1')then
			CDF_ra <= (others => '0');
		elsif(ec_CDF_ra = '1')then
			CDF_ra <= CDF_ra + 1;
		end if;
		if(resetSyncRegs(2) = '1')then
      CDF_cnt <= (others => '0');
    elsif(sel_evn(0) = '1' and CDF_wa(0) = '1' and (ec_CDF_ra = '0' or CDF_ra(0) = '1'))then
      CDF_cnt <= CDF_cnt + 1;
    elsif((sel_evn(0) = '0' or CDF_wa(0) = '0') and ec_CDF_ra = '1' and CDF_ra(0) = '0')then
      CDF_cnt <= CDF_cnt - 1;
    end if;
		if(resetSyncRegs(2) = '1')then
      CDF_empty <= '1';
    elsif(or_reduce(CDF_cnt(7 downto 1)) = '0' and (CDF_cnt(0) = '0' or ec_CDF_ra = '1'))then
      CDF_empty <= '1';
    elsif(or_reduce(CDF_cnt) = '1')then
      CDF_empty <= '0';
    end if;
		if(sel_evn(1) = '0')then
--			CDF_in <= x"005" & EvtTy & evn & evn_out(11 downto 0) & Source_ID(11 downto 0) & x"08"; -- header1
			CDF_in <= x"005" & EvtTy & evn & evn_out(11 downto 0) & x"00008"; -- header1
		else
			CDF_in <= x"00" & uFOV & CalTy & x"00000" & evn_out(43 downto 12) & x"0"; -- header2
		end if;
	end if;
end process;
EvtTy <= "00" & evn_out(45) & not evn_out(45);
CalTy <= evn_out(51 downto 48);
process(sysclk, ThreeSFP, TwoSFP, sel_AMC)
variable bldr_mask_sel : std_logic_vector(4 downto 0);
variable bldr_mask : std_logic_vector(2 downto 0);
--variable Mbit_word : std_logic_vector(11 downto 0);
begin
	bldr_mask_sel(4) := ThreeSFP; 
	bldr_mask_sel(3) := TwoSFP; 
	bldr_mask_sel(2 downto 0) := sel_AMC(3 downto 1);
	case bldr_mask_sel is
		when "10100" | "10101" => bldr_mask := "100";
		when "10000" | "10001" | "01000" | "01001" | "01010" => bldr_mask := "010";
		when others => bldr_mask := "001";
	end case;
--	for i in 0 to 11 loop
--		Mbit_word(i) := EventInfo(i)(25);
--	end loop;
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			CDF_header <= '1';
			header <= '0';
			init_bldr <= '0';
			EventInfo_avl <= '0';
			ec_sel_AMC <= '0';
			AMC_wc_we <= "000";
			AMC_wc_sum_we <= '0'; 
			BlockHeader <= '0';
			ec_CDF_ra <= '0';
			AMC_header_we <= "000";
			sel_CDF <= '0';
			buf_rqst <= x"0";
			sel_AMC <= x"0";
		else
			if((and_reduce(not AMC_hasData or EventInfo_dav) = '1' and or_reduce(AMC_hasData) = '1') or (or_reduce(amc_en) = '0' and CDF_empty = '0'))then
		    EventInfo_avl <= '1';
			else
		    EventInfo_avl <= '0';
			end if;
--			LastBlock <= not or_reduce(AMC_hasData and Mbit_word);
			LastBlock <= '1';
			for i in 0 to 11 loop
				if(AMC_hasData(i) = '1' and EventInfo(i)(25) = '1')then
					LastBlock <= '0';
				end if;
			end loop;
			if(EventInfo_avl = '1' and init_bldr = '0' and bldr_fifo_full = "000" and ((mon_en = '0' and TCPbuf_avl = '1') or (mon_en = '1' and mon_buf_avl = '1')))then
				if(CDF_header = '1')then
					ec_CDF_ra <= '1';
					zero_wc <= not ThreeSFP & OneSFP & '0';
				else
					BlockHeader <= '1';				
				end if;
				header <= '1';
				init_bldr <= '1';				
				buf_rqst <= LastBlock & CDF_header & not mon_en & mon_en;
				AMC_header(0)(64) <= mon_en and mon_buf_avl;
				AMC_header(1)(64) <= mon_en and mon_buf_avl;
				AMC_header(2)(64) <= mon_en and mon_buf_avl;
				more_wc <= "000";
				for i in 0 to 11 loop
					Mbit_word(i) <= AMC_hasData(i) and EventInfo(i)(25);
				end loop;
			elsif(rst_init_bldr = '1')then
				init_bldr <= '0';
--				zero_wc <= zero_wc or not more_wc;
				zero_wc <= not more_wc;
				if(LastBlock = '1')then
					ec_CDF_ra <= '1';
					CDF_header <= '1';
				end if;
			else
				header <= '0';
				ec_CDF_ra <= '0';
				buf_rqst <= x"0";
				if(init_bldr = '1')then
					CDF_header <= '0';
					BlockHeader <= CDF_header;
				end if;
				if(ec_sel_AMC = '1' and Mbit_word(conv_integer(sel_AMC)) = '1')then
					more_wc <= more_wc or bldr_mask;
				end if;
			end if;
			sel_CDF <= (CDF_header and init_bldr) or BlockHeader;
--			if(BlockHeader = '1')then
--				AMC_hasData_l <= AMC_hasData;
--			end if;
			if(BlockHeader = '1')then
				ec_sel_AMC <= '1';
			elsif(sel_AMC = x"b")then
				ec_sel_AMC <= '0';
			end if;
			if(AMC_hasData(conv_integer(sel_AMC)) = '1' and ec_sel_AMC = '1')then
				AMC_wc_we <= bldr_mask; 
				AMC_wc_sum_we <= or_reduce(bldr_mask); 
			else
				AMC_wc_we <= "000"; 
				AMC_wc_sum_we <= '0'; 
			end if;
			for i in 0 to 2 loop
				if((sel_CDF = '1' and (FirstBlock(i) = '1' or nAMC(i) /= x"0")) or AMC_wc_we(i) = '1')then
					AMC_header_we(i) <= '1';
				else
					AMC_header_we(i) <= '0';
				end if;
			end loop;
			if(ec_sel_AMC = '0' or sel_AMC = x"b")then
				sel_AMC <= x"0";
			else
				sel_AMC <= sel_AMC + 1;
			end if;
		end if;
		for i in 0 to 2 loop
			case AMC_hasData(i*4+3 downto i*4) is
				when x"1" | x"2" | x"4" | x"8" => mAMC(i) <= x"1";
				when x"3" | x"5" | x"6" | x"9" | x"a" | x"c" => mAMC(i) <= x"2";
				when x"7" | x"b" | x"d" | x"e" => mAMC(i) <= x"3";
				when x"f" => mAMC(i) <= x"4";
				when others => mAMC(i) <= x"0";
			end case;
		end loop;
		kAMC(0)(1) <= AMC_hasData(5) and AMC_hasData(4);
		kAMC(0)(0) <= AMC_hasData(5) xor AMC_hasData(4);
		kAMC(1)(1) <= AMC_hasData(7) and AMC_hasData(6);
		kAMC(1)(0) <= AMC_hasData(7) xor AMC_hasData(6);
		if(ThreeSFP = '1')then
			nAMC(0) <= mAMC(1);
		elsif(TwoSFP = '1')then
			nAMC(0) <= mAMC(2) + kAMC(1);
		else
			nAMC(0) <= mAMC(0) + mAMC(1) + mAMC(2);
		end if;
		if(ThreeSFP = '1')then
			nAMC(1) <= mAMC(0);
		elsif(TwoSFP = '1')then
			nAMC(1) <= mAMC(0) + kAMC(0);
		else
			nAMC(1) <= x"0";
		end if;
		if(ThreeSFP = '1')then
			nAMC(2) <= mAMC(2);
		else
			nAMC(2) <= x"0";
		end if;
		summary <= '0' & EventInfo(conv_integer(sel_AMC))(26 downto 20) & x"0" & EventInfo(conv_integer(sel_AMC))(19 downto 0) & block_num & (sel_AMC+1) & AMCinfo(conv_integer(sel_AMC));
		for i in 0 to 2 loop
			AMC_header(i)(65) <= not BlockHeader and sel_CDF;
			if(sel_CDF = '0')then
				AMC_header(i)(55 downto 52) <= summary(55 downto 52);
			elsif(BlockHeader = '1')then
				AMC_header(i)(55 downto 52) <= CDF_out(55 downto 52);
			else
				AMC_header(i)(55 downto 52) <= nAMC(i);
			end if;
			if(sel_CDF = '1')then
				AMC_header(i)(63 downto 56) <= CDF_out(63 downto 56);
				AMC_header(i)(51 downto 20) <= CDF_out(51 downto 20);
				AMC_header(i)(7 downto 0) <= CDF_out(7 downto 0);
			else
				AMC_header(i)(63 downto 56) <= summary(63 downto 56);
				AMC_header(i)(51 downto 20) <= summary(51 downto 20);
				AMC_header(i)(7 downto 0) <= summary(7 downto 0);
			end if;
		end loop;
		if(sel_CDF = '0')then
			AMC_header(0)(19 downto 8) <= summary(19 downto 8);
			AMC_header(1)(19 downto 8) <= summary(19 downto 8);
			AMC_header(2)(19 downto 8) <= summary(19 downto 8);
		elsif(BlockHeader = '1')then
			if(OneSFP = '1')then
				AMC_header(0)(19 downto 8) <= source_ID(0);
			else
				AMC_header(0)(19 downto 8) <= source_ID(1);
			end if;
			AMC_header(1)(19 downto 8) <= source_ID(0);
			AMC_header(2)(19 downto 8) <= source_ID(2);
		else
			AMC_header(0)(19 downto 8) <= CDF_out(19 downto 8);
			AMC_header(1)(19 downto 8) <= CDF_out(19 downto 8);
			AMC_header(2)(19 downto 8) <= CDF_out(19 downto 8);
		end if;
		if(CDF_header = '1')then
			FirstBlock(0) <= '1';
			FirstBlock(1) <= not OneSFP;
			FirstBlock(2) <= ThreeSFP;
			block_num <= (others => '0');
		elsif(rst_init_bldr = '1')then
			FirstBlock <= "000";
			block_num <= block_num + 1;
		end if;
		if(fifo_en = '0' or (EventInfoRdDone(12) = '1' and LastBlock = '1'))then
			AMC_hasData <= AMC_en;
--		elsif(ec_sel_AMC = '1' and EventInfo(conv_integer(sel_AMC))(25) = '0')then -- More bit is '0'
--			AMC_hasData(conv_integer(sel_AMC)) <= '0';
		elsif(EventInfoRdDone(12) = '1')then
			AMC_hasData <= Mbit_word;
		end if;
		for i in 0 to 11 loop
			if(Mbit_word(i) = '1')then
				AMC_wcp(i) <= "1000000000000"; 
			else
				AMC_wcp(i) <= EventInfo(i)(12 downto 0); 
			end if;
		end loop;
		if(OneSFP = '1')then
--			AMC_wc(17) <= not or_reduce(AMC_hasData and Mbit_word);
			AMC_wc(17) <= not or_reduce(Mbit_word);
		elsif(TwoSFP = '1')then
			if(sel_AMC = x"0")then
				AMC_wc(17) <= not or_reduce(Mbit_word(5 downto 0));
			elsif(sel_AMC = x"6")then
				AMC_wc(17) <= not or_reduce(Mbit_word(11 downto 6));
			end if;
		else
			if(sel_AMC = x"0")then
				AMC_wc(17) <= not or_reduce(Mbit_word(3 downto 0));
			elsif(sel_AMC = x"4")then
				AMC_wc(17) <= not or_reduce(Mbit_word(7 downto 4));
			elsif(sel_AMC = x"8")then
				AMC_wc(17) <= not or_reduce(Mbit_word(11 downto 8));
			end if;
		end if;
		AMC_wc(16 downto 0) <= sel_AMC & AMC_wcp(conv_integer(sel_AMC));
	end if;
end process;
kAMC(1)(3 downto 2) <= "00";
kAMC(0)(3 downto 2) <= "00";
--AMC_header(1)(64 downto 56) <= AMC_header(0)(64 downto 56);
--AMC_header(1)(51 downto 0) <= AMC_header(0)(51 downto 0);
--AMC_header(2)(64 downto 56) <= AMC_header(0)(64 downto 56);
--AMC_header(2)(51 downto 0) <= AMC_header(0)(51 downto 0);
i_evt_bldr0: evt_bldr PORT MAP(
		clk => sysclk,
		reset => resetSyncRegs(2),
		fifo_rst => fifo_rst,
		fifo_en => fifo_en,
		en_inject_err => en_inject_err,
		OneSFP => OneSFP,
		Source_ID => x"00",
		block_wc => mon_wc(0),
		block_wc_we => block_wc_we(0),
		AMC_wc => AMC_wc,
		AMC_wc_we => AMC_wc_we(0),
		AMC_wc_end => rst_init_bldr,
		bldr_fifo_full => bldr_fifo_full(0),
		AMC_header => AMC_header(0),
		AMC_header_we => AMC_header_we(0),
		AMC_DATA => AMC_DATA,
		AMC_DATA_re => AMC_DATA_re(0),
		AMCCRC_bad => AMCCRC_bad(0),
		evt_data => evt_data(0),
		evt_data_we => evt_data_we(0),
		evt_buf_full => evt_buf_full(0),
		evt_data_re => evt_data_re(0),
		evt_data_rdy => evt_data_rdy(0),
		debug => evt_bldr_debug,
		EventBuilt => EventBuilt(0)
	);
i_evt_bldr1: evt_bldr PORT MAP(
		clk => sysclk,
		reset => resetSyncRegs(2),
		fifo_rst => fifo_rst,
		fifo_en => fifo_en,
		en_inject_err => en_inject_err,
		OneSFP => OneSFP,
		Source_ID => x"00",
		block_wc => mon_wc(1),
		block_wc_we => block_wc_we(1),
		AMC_wc => AMC_wc,
		AMC_wc_we => AMC_wc_we(1),
		AMC_wc_end => rst_init_bldr,
		bldr_fifo_full => bldr_fifo_full(1),
		AMC_header => AMC_header(1),
		AMC_header_we => AMC_header_we(1),
		AMC_DATA => AMC_DATA1,
		AMC_DATA_re => AMC_DATA_re(1),
		AMCCRC_bad => AMCCRC_bad(1),
		evt_data => evt_data(1),
		evt_data_we => evt_data_we(1),
		evt_buf_full => evt_buf_full(1),
		evt_data_re => evt_data_re(1),
		evt_data_rdy => evt_data_rdy(1),
		debug => open,
		EventBuilt => EventBuilt(1)
	);
g_AMC_DATA1: for i in 0 to 5 generate
	AMC_DATA1(i) <= AMC_DATA(i);
	AMC_DATA1(i+6) <= (others => '0');
end generate;
i_evt_bldr2: evt_bldr PORT MAP(
		clk => sysclk,
		reset => resetSyncRegs(2),
		fifo_rst => fifo_rst,
		fifo_en => fifo_en,
		en_inject_err => en_inject_err,
		OneSFP => OneSFP,
		Source_ID => x"00",
		block_wc => mon_wc(2),
		block_wc_we => block_wc_we(2),
		AMC_wc => AMC_wc,
		AMC_wc_we => AMC_wc_we(2),
		AMC_wc_end => rst_init_bldr,
		bldr_fifo_full => bldr_fifo_full(2),
		AMC_header => AMC_header(2),
		AMC_header_we => AMC_header_we(2),
		AMC_DATA => AMC_DATA2,
		AMC_DATA_re => AMC_DATA_re(2),
		AMCCRC_bad => AMCCRC_bad(2),
		evt_data => evt_data(2),
		evt_data_we => evt_data_we(2),
		evt_buf_full => evt_buf_full(2),
		evt_data_re => evt_data_re(2),
		evt_data_rdy => evt_data_rdy(2),
		debug => open,
		EventBuilt => EventBuilt(2)
	);
g_AMC_DATA2: for i in 0 to 3 generate
	AMC_DATA2(i) <= AMC_DATA(i+8);
	AMC_DATA2(i+4) <= AMC_DATA(i+8);
	AMC_DATA2(i+8) <= AMC_DATA(i+8);
end generate;
AMC_DATA_RdEn <= AMC_DATA_re(0) or AMC_DATA_re(1) or AMC_DATA_re(2);
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		for i in 0 to 11 loop
			if(AMCCRC_bad(0)(i) = '1' or AMCCRC_bad(1)(i) = '1' or AMCCRC_bad(2)(i) = '1')then
				badEventCRCToggle(i) <= not badEventCRCToggle(i);
			end if;
			if(EventInfo(i)(31) = '1' and EventInfo(i)(25) = '0' and EventInfoRdDone(i) = '1')then
				ReSyncFakeEventToggle(i) <= not ReSyncFakeEventToggle(i);
			end if;
		end loop;
	end if;
end process;
i_FIFO_RESET_AMC: FIFO_RESET_7S PORT MAP(
		reset => resetFIFO_AMC,
		clk => sysclk,
		fifo_rst => fifo_rst_AMC,
		fifo_en => fifo_en_AMC
	);
resetFIFO_AMC <= reset or or_reduce(not AMC_txfsmresetdone and AMC_en) or RstAMC_link;
g_AMC_Link : for i in 0 to 11 generate
	i_AMC_Link: AMC_Link
	PORT MAP(
		sysclk => sysclk,
		reset => RstAMC_link,
		resetCntr => CntrRst,
		fifo_rst => fifo_rst_AMC,
		fifo_en => fifo_en_AMC,
		test => test,
		strobe2ms => strobe2ms,
		NoReSyncFake => NoReSyncFake,
		UsrClk => UsrClk,
		RXNOTINTABLE => AMC_RXNOTINTABLE(i),
		rxcommaalignen => AMC_rxcommaalignen(i),
		rxchariscomma => AMC_rxchariscomma(i),
		rxcharisk => AMC_rxcharisk(i),
		rxresetdone => AMC_rxresetdone(i),
		qpll_lock => AMC_qpll_lock(i/4),
		txfsmresetdone => AMC_txfsmresetdone(i),
		data_valid => AMC_data_valid(i),
		RXDATA => AMC_RXDATA(i),
		txcharisk => AMC_txcharisk(i),
		TXDATA => AMC_TXDATA(i),
		Ready => AMC_Ready_i(i),
		AMC_ID => AMC_ID(i),
		AMCinfo => AMCinfo(i),
		EventInfo => EventInfo(i),
		EventInfo_dav => EventInfo_dav(i),
		AMC_DATA_RdEn => AMC_DATA_RdEn(i),
		EventInfoRdDone => EventInfoRdDone(i),
		AMC_DATA => AMC_DATA(i),
		bad_AMC => AMC_status(i),
		AMC_OK => AMC_OK(i),
		L1A_DATA => L1A_DATA,
		L1A_WrEn => L1A_WrEn,
    fake_header => fake_header,
		fake_CRC => fake_CRC,
    fake_DATA	=> fake_DATA,
    fake_WrEn => fake_WrEn,
    fake_full => fake_full(i),
		Cntr_ADDR => Cntr_ADDR,
		Cntr_DATA => Cntr_DATA(i),
		debug_out => AMC_debug(i),
		TTCclk => TTC_clk,
		BC0 => BC0,
		TTC_LOS => TTC_LOS,
		AMC_en => AMC_en(i),
		TTS_disable => TTS_disable(i),
		TTC_status => TTC_status(i),
		TrigData => TrigData(i),
		TTS_RQST => AMC_TTS_RQST(i),
		TTS_coded => AMC_TTS(i)(4 downto 0)
	);
end generate;
i_AMC_wrapper: AMC_wrapper PORT MAP(
		DRPclk => DRPclk,
		SOFT_RESET => GTXreset,
		UsrClk => UsrClk,
		test => test,
		Dis_pd => Dis_pd,
		AMC_en => AMC_en,
		RXDATA => AMC_RXDATA,
		RxBufOvf => RxBufOvf,
		RxBufUdf => RxBufUdf,
		sampleRatio => RxClkCntr19_q,
		updateRatio => updateRatio,
		RxClkRatio => RxClkRatio,
		rxprbserr => AMC_rxprbserr,
		rxprbssel => AMC_rxprbssel,
		RXNOTINTABLE => AMC_RXNOTINTABLE,
		rxcommaalignen => AMC_rxcommaalignen,
		rxchariscomma => AMC_rxchariscomma,
		rxcharisk => AMC_rxcharisk,
		rxresetdone => AMC_rxresetdone,
		txdiffctrl => AMC_txdiffctrl,
		TXDATA => AMC_TXDATA,
		txoutclk => UsrClk_out,
		txcharisk => AMC_txcharisk,
		txresetdone => open,
		txprbssel => AMC_txprbssel,
		qpll_lock => AMC_qpll_lock,
		txfsmresetdone => AMC_txfsmresetdone,
		rxfsmresetdone => AMC_rxfsmresetdone,
		data_valid => AMC_data_valid,
		AMC_REFCLK => AMC_REFCLK,
		RXN => AMC_RXN,
		RXP => AMC_RXP,
		TXN => AMC_TXN,
		TXP => AMC_TXP
	);
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		ovfl_warning_p <= ovfl_warning_i(3) and not en_localL1A;
		if(resetSyncRegs(2) = '1')then
			AllEventBuilt_i <= '1';
		else
			for i in 0 to 2 loop
				if(EventBuilt(i) = '1')then
					EventBuiltToggle(i) <= not EventBuiltToggle(i);
				end if;
			end loop;
			if(evt_cnt(0) = x"00" and evt_cnt(1) = x"00" and evt_cnt(2) = x"00")then
			  AllEventBuilt_i <= '1';
			else
			  AllEventBuilt_i <= '0';
			end if;
		end if;
		if(RstAMC_link_dl = '1')then
			enRstAMC_link <= '0';
		elsif(ReSync = '1')then
			enRstAMC_link <= '1';
		end if;
	end if;
end process;
i_RstAMC_link_dl : SRL16E
   port map (
      Q => RstAMC_link_dl,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '1',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => sysclk,   -- Clock input
      D => RstAMC_link        -- SRL data input
   );
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		LinkFull <= or_reduce(fake_full);
		if(Cntr2ms(18 downto 17) = "11" and Cntr2ms(14) = '1')then
			Cntr2ms <= (others => '0');
			strobe2ms <= '1';
		else
			Cntr2ms <= Cntr2ms + 1;
			strobe2ms <= '0';
		end if;
	end if;
end process;
fake_en <= '0' when test = '0' or AMC_en = x"000" else '1';
i_fake_event: fake_event PORT MAP(
		sysclk => sysclk,
		UsrClk => UsrClk,
		reset => resetSyncRegs(2),
		fifo_rst => fifo_rst,
		fifo_en => fifo_en,
		fake_en => fake_en,
		sync => '1',
		fake_length => fake_length,
		ovfl_warning => ovfl_warning_p,
		LinkFull => LinkFull,
		L1A_DATA => L1A_DATA,
		L1A_WrEn => L1A_WrEn,
		fake_header => fake_header,
		fake_CRC => fake_CRC,
		empty_event_flag => empty_event_flag,
		fake_DATA => fake_DATA,
		fake_WrEn => fake_WrEn
	);
process(UsrClk,reset)
begin
	if(reset = '1')then
		for i in 0 to 11 loop
			AMC_TTS(i)(7 downto 5) <= "000";
		end loop;
	elsif(UsrClk'event and UsrClk = '1')then
		for i in 0 to 11 loop
			if(AMC_TTS(i)(2) = '1' and AMC_Ready_i(i) = '1')then -- Out of Sync
				AMC_TTS(i)(5) <= '1';
			end if;
			if(AMC_TTS(i)(3) = '1' and AMC_Ready_i(i) = '1')then -- error
				AMC_TTS(i)(6) <= '1';
			end if;
			if(AMC_TTS(i)(4) = '1' and AMC_Ready_i(i) = '1')then -- disconnected
				AMC_TTS(i)(7) <= '1';
			end if;
		end loop;
	end if;
end process;
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		AMC_TTS_OR <= AMC_TTS(0) or AMC_TTS(1) or AMC_TTS(2) or AMC_TTS(3) or AMC_TTS(4) or AMC_TTS(5) or
									AMC_TTS(6) or AMC_TTS(7) or AMC_TTS(8) or AMC_TTS(9) or AMC_TTS(10) or AMC_TTS(11) or err_TTS;
		AMC_TTS_RQST_OR <= AMC_TTS_RQST(0) or AMC_TTS_RQST(1) or AMC_TTS_RQST(2) or AMC_TTS_RQST(3) or AMC_TTS_RQST(4) or AMC_TTS_RQST(5) or
									AMC_TTS_RQST(6) or AMC_TTS_RQST(7) or AMC_TTS_RQST(8) or AMC_TTS_RQST(9) or AMC_TTS_RQST(10) or AMC_TTS_RQST(11);
	end if;
end process;
err_TTS <= "000000" & stop_mon & '0';
process(UsrClk,reset)
begin
	if(reset = '1')then
		TTS_FIFO_wa <= (others => '0');
	elsif(UsrClk'event and UsrClk = '1')then
		case TTS_FIFO_wa(2 downto 0) is
			when "000" => TTS_FIFO_wa(2 downto 0) <= "001";
			when "001" => TTS_FIFO_wa(2 downto 0) <= "011";
			when "011" => TTS_FIFO_wa(2 downto 0) <= "010";
			when "010" => TTS_FIFO_wa(2 downto 0) <= "110";
			when "110" => TTS_FIFO_wa(2 downto 0) <= "111";
			when "111" => TTS_FIFO_wa(2 downto 0) <= "101";
			when "101" => TTS_FIFO_wa(2 downto 0) <= "100";
			when others => TTS_FIFO_wa(2 downto 0) <= "000";
		end case;
	end if;
end process;
i_TTS_FIFO: RAM32x8 PORT MAP(
		wclk => UsrClk,
		di => TTS_FIFO_di,
		we => '1',
		wa => TTS_FIFO_wa,
		ra => TTS_FIFO_ra,
		do => TTS_FIFO_do
	);
TTS_FIFO_di <= AMC_TTS_RQST_OR & AMC_TTS_OR(4 downto 0);
TTS_FIFO_ra <= "00" & TTS_FIFO_waSyncRegs3;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		TTS_FIFO_waSyncRegs <= TTS_FIFO_wa(2 downto 0);
		TTS_FIFO_waSyncRegs2 <= TTS_FIFO_waSyncRegs;
		TTS_FIFO_waSyncRegs3 <= TTS_FIFO_waSyncRegs2;
		if(enRstAMC_link = '1' or AMC_en /= AMC_Ready_i)then
			TTS_coded <= "00010";
		else
			TTS_coded <= TTS_FIFO_do(4 downto 0);
		end if;
		TTS_RQST <= TTS_FIFO_do(7 downto 5);
	end if;
end process;
i_UsrClk_buf: bufg port map(i => UsrClk_out(6), o => UsrClk);
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		RxClkCntr <= RxClkCntr + 1;
		RXClkCntr19_q <= RXClkCntr(19);
		if(RXClkCntr19_q = '1' and RXClkCntr(19) = '0')then
			if(Cntr_ADDR(11 downto 5) = "1110001")then
				updateRatio <= '0';
			else
				updateRatio <= '1';
			end if;
		end if;
		if(Cntr_ADDR(11 downto 7) = "11000" or Cntr_ADDR(11 downto 6) = "111000")then
			AMC_if_RdEn <= '1';
		else
			AMC_if_RdEn <= '0';
		end if;
		if(Cntr_ADDR(11 downto 7) = "11000" or Cntr_ADDR(11 downto 6) = "111000")then
			AMC_if_ADDR <= Cntr_ADDR(9) & Cntr_ADDR(6 downto 0);
		else
			AMC_if_ADDR <= (others => '0');
		end if;
		if(AMC_if_RdEn = '0')then
			AMC_if_data <= (others => '0');
		elsif(AMC_if_ADDR(7) = '0')then
			case AMC_if_ADDR(6 downto 4) is
				when "000" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= EventInfo(0)(15 downto 0);
						when x"1" => AMC_if_data <= EventInfo(0)(31 downto 16);
						when x"2" => AMC_if_data <= EventInfo(1)(15 downto 0);
						when x"3" => AMC_if_data <= EventInfo(1)(31 downto 16);
						when x"4" => AMC_if_data <= EventInfo(2)(15 downto 0);
						when x"5" => AMC_if_data <= EventInfo(2)(31 downto 16);
						when x"6" => AMC_if_data <= EventInfo(3)(15 downto 0);
						when x"7" => AMC_if_data <= EventInfo(3)(31 downto 16);
						when x"8" => AMC_if_data <= EventInfo(4)(15 downto 0);
						when x"9" => AMC_if_data <= EventInfo(4)(31 downto 16);
						when x"a" => AMC_if_data <= EventInfo(5)(15 downto 0);
						when x"b" => AMC_if_data <= EventInfo(5)(31 downto 16);
						when x"c" => AMC_if_data <= EventInfo(6)(15 downto 0);
						when x"d" => AMC_if_data <= EventInfo(6)(31 downto 16);
						when x"e" => AMC_if_data <= EventInfo(7)(15 downto 0);
						when others => AMC_if_data <= EventInfo(7)(31 downto 16);
					end case;
				when "001" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= EventInfo(8)(15 downto 0);
						when x"1" => AMC_if_data <= EventInfo(8)(31 downto 16);
						when x"2" => AMC_if_data <= EventInfo(9)(15 downto 0);
						when x"3" => AMC_if_data <= EventInfo(9)(31 downto 16);
						when x"4" => AMC_if_data <= EventInfo(10)(15 downto 0);
						when x"5" => AMC_if_data <= EventInfo(10)(31 downto 16);
						when x"6" => AMC_if_data <= EventInfo(11)(15 downto 0);
						when x"7" => AMC_if_data <= EventInfo(11)(31 downto 16);
						when x"8" => AMC_if_data <= LinkFull & not AMC_qpll_lock & EventInfo_dav;
						when x"9" => AMC_if_data <= x"0" & fake_full;
						when x"a" => AMC_if_data <= AMC_TTC_status(15 downto 0);
						when x"b" => AMC_if_data <= AMC_TTC_status(31 downto 16);
						when x"c" => AMC_if_data <= x"0" & AMC_rxfsmresetdone;
						when x"d" => AMC_if_data <= x"0" & AMC_txfsmresetdone;
						when x"e" => AMC_if_data <= "0000000" & CDF_empty & CDF_cnt;
						when others => AMC_if_data <= errors & x"00";
					end case;
				when "010" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= "0000000" & evn_wa;
						when x"1" => AMC_if_data <= "0000000" & evn_ra;
						when x"2" => AMC_if_data <= evn(15 downto 0);
						when x"3" => AMC_if_data <= x"00" & evn(23 downto 16);
						when x"4" => AMC_if_data <= "0000000" & CDF_wa;
						when x"5" => AMC_if_data <= "0000000" & CDF_ra;
						when x"6" => AMC_if_data <= ec_CDF_ra & AMC_wc_we & sel_CDF & AMC_header_we & sel_evn & "00" & sel_AMC;
						when x"7" => AMC_if_data <= "000" & evt_buf_full & mon_en & WaitMonBuF & TCPbuf_avl & mon_buf_avl & init_bldr & evn_empty & EventInfo_avl & bldr_fifo_full;
						when x"8" => AMC_if_data <= fake_word_cnt;
						when x"a" => AMC_if_data <= fake_header_cnt;
						when x"c" => AMC_if_data <= fake_evt_cnt;
						when x"e" => AMC_if_data <= empty_evt_cnt;
						when others => AMC_if_data <= (others => '0');
					end case;
				when "011" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= TTC_status(3)(5 downto 2) & TTC_status(2)(5 downto 2) & TTC_status(1)(5 downto 2) & TTC_status(0)(5 downto 2);
						when x"1" => AMC_if_data <= TTC_status(7)(5 downto 2) & TTC_status(6)(5 downto 2) & TTC_status(5)(5 downto 2) & TTC_status(4)(5 downto 2);
						when x"2" => AMC_if_data <= TTC_status(11)(5 downto 2) & TTC_status(10)(5 downto 2) & TTC_status(9)(5 downto 2) & TTC_status(8)(5 downto 2);
						when x"4" => AMC_if_data <= AMC_TTS(1) & AMC_TTS(0);
						when x"5" => AMC_if_data <= AMC_TTS(3) & AMC_TTS(2);
						when x"6" => AMC_if_data <= AMC_TTS(5) & AMC_TTS(4);
						when x"7" => AMC_if_data <= AMC_TTS(7) & AMC_TTS(6);
						when x"8" => AMC_if_data <= AMC_TTS(9) & AMC_TTS(8);
						when x"9" => AMC_if_data <= AMC_TTS(11) & AMC_TTS(10);
						when x"a" => AMC_if_data <= evt_cnt(1) & evt_cnt(0);
						when x"b" => AMC_if_data <= enRstAMC_link & "00000" & AllEventBuilt_i & AllEventBuilt_i & evt_cnt(2);
						when x"c" => AMC_if_data <= x"0" & AMC_TTS_RQST(3) & AMC_TTS_RQST(2) & AMC_TTS_RQST(1) & AMC_TTS_RQST(0);
						when x"d" => AMC_if_data <= x"0" & AMC_TTS_RQST(7) & AMC_TTS_RQST(6) & AMC_TTS_RQST(5) & AMC_TTS_RQST(4);
						when x"e" => AMC_if_data <= x"0" & AMC_TTS_RQST(11) & AMC_TTS_RQST(10) & AMC_TTS_RQST(9) & AMC_TTS_RQST(8);
						when others => AMC_if_data <= (others => '0');
					end case;
				when "100" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= badEventCRC_cntr(0);
						when x"2" => AMC_if_data <= badEventCRC_cntr(1);
						when x"4" => AMC_if_data <= badEventCRC_cntr(2);
						when x"6" => AMC_if_data <= badEventCRC_cntr(3);
						when x"8" => AMC_if_data <= badEventCRC_cntr(4);
						when x"a" => AMC_if_data <= badEventCRC_cntr(5);
						when x"c" => AMC_if_data <= badEventCRC_cntr(6);
						when x"e" => AMC_if_data <= badEventCRC_cntr(7);
						when others => AMC_if_data <= (others => '0');
					end case;
				when "101" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= badEventCRC_cntr(8);
						when x"2" => AMC_if_data <= badEventCRC_cntr(9);
						when x"4" => AMC_if_data <= badEventCRC_cntr(10);
						when x"6" => AMC_if_data <= badEventCRC_cntr(11);
						when x"8" => AMC_if_data <= EventBuiltCnt(0);
						when x"a" => AMC_if_data <= EventBuiltCnt(1);
						when x"c" => AMC_if_data <= EventBuiltCnt(2);
						when others => AMC_if_data <= (others => '0');
					end case;
				when "110" | "111" =>
					if(AMC_if_ADDR(0) = '0')then
						AMC_if_data <= x"00" & "00" & AMC_wc_sum_do;
					else
						AMC_if_data <= (others => '0');
					end if;
				when others => AMC_if_data <= (others => '0');
			end case;
		else
			case AMC_if_ADDR(5 downto 4) is
				when "00" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= ReSyncFakeEvent_cntr(0);
						when x"2" => AMC_if_data <= ReSyncFakeEvent_cntr(1);
						when x"4" => AMC_if_data <= ReSyncFakeEvent_cntr(2);
						when x"6" => AMC_if_data <= ReSyncFakeEvent_cntr(3);
						when x"8" => AMC_if_data <= ReSyncFakeEvent_cntr(4);
						when x"a" => AMC_if_data <= ReSyncFakeEvent_cntr(5);
						when x"c" => AMC_if_data <= ReSyncFakeEvent_cntr(6);
						when x"e" => AMC_if_data <= ReSyncFakeEvent_cntr(7);
						when others => AMC_if_data <= (others => '0');
					end case;
				when "01" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= ReSyncFakeEvent_cntr(8);
						when x"2" => AMC_if_data <= ReSyncFakeEvent_cntr(9);
						when x"4" => AMC_if_data <= ReSyncFakeEvent_cntr(10);
						when x"6" => AMC_if_data <= ReSyncFakeEvent_cntr(11);
						when others => AMC_if_data <= (others => '0');
					end case;
				when "10" =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= RxClkRatio(0)(15 downto 0);
						when x"1" => AMC_if_data <= x"00" & "000" & RxClkRatio(0)(20 downto 16);
						when x"2" => AMC_if_data <= RxClkRatio(1)(15 downto 0);
						when x"3" => AMC_if_data <= x"00" & "000" & RxClkRatio(1)(20 downto 16);
						when x"4" => AMC_if_data <= RxClkRatio(2)(15 downto 0);
						when x"5" => AMC_if_data <= x"00" & "000" & RxClkRatio(2)(20 downto 16);
						when x"6" => AMC_if_data <= RxClkRatio(3)(15 downto 0);
						when x"7" => AMC_if_data <= x"00" & "000" & RxClkRatio(3)(20 downto 16);
						when x"8" => AMC_if_data <= RxClkRatio(4)(15 downto 0);
						when x"9" => AMC_if_data <= x"00" & "000" & RxClkRatio(4)(20 downto 16);
						when x"a" => AMC_if_data <= RxClkRatio(5)(15 downto 0);
						when x"b" => AMC_if_data <= x"00" & "000" & RxClkRatio(5)(20 downto 16);
						when x"c" => AMC_if_data <= RxClkRatio(6)(15 downto 0);
						when x"d" => AMC_if_data <= x"00" & "000" & RxClkRatio(6)(20 downto 16);
						when x"e" => AMC_if_data <= RxClkRatio(7)(15 downto 0);
						when others => AMC_if_data <= x"00" & "000" & RxClkRatio(7)(20 downto 16);
					end case;
				when others =>
					case AMC_if_ADDR(3 downto 0) is
						when x"0" => AMC_if_data <= RxClkRatio(8)(15 downto 0);
						when x"1" => AMC_if_data <= x"00" & "000" & RxClkRatio(8)(20 downto 16);
						when x"2" => AMC_if_data <= RxClkRatio(9)(15 downto 0);
						when x"3" => AMC_if_data <= x"00" & "000" & RxClkRatio(9)(20 downto 16);
						when x"4" => AMC_if_data <= RxClkRatio(10)(15 downto 0);
						when x"5" => AMC_if_data <= x"00" & "000" & RxClkRatio(10)(20 downto 16);
						when x"6" => AMC_if_data <= RxClkRatio(11)(15 downto 0);
						when x"7" => AMC_if_data <= x"00" & "000" & RxClkRatio(11)(20 downto 16);
						when x"8" => AMC_if_data <= x"0" & RxBufUdfErr;
						when x"9" => AMC_if_data <= x"0" & RxBufOvfErr;
						when others => AMC_if_data <= (others => '0');
					end case;
			end case;
		end if;
	end if;
end process;
process(clk125, RstAMC_link)
begin
	if(RstAMC_link = '1')then
		rst_AMC_cntr <= '1';
		RstAMC_linkSync <= (others => '1');
	elsif(clk125'event and clk125 = '1')then
		rst_AMC_cntr <= resetCntr or RstAMC_linkSync(3);
		RstAMC_linkSync <= RstAMC_linkSync(2 downto 0) & '0';
	end if;
end process;
i_AMC_cntr : AMC_cntr PORT MAP (
          UsrClk => UsrClk,
          clk125 => clk125,
          sysclk => sysclk,
          ipb_clk => ipb_clk,
          resetCntr => rst_AMC_cntr,
          DB_cmd => DB_cmd,
          AMC_if_data => AMC_if_data,
          Cntr_DATA => Cntr_DATA,
          Cntr_ADDR => Cntr_ADDR,
          ipb_addr => ipb_addr(15 downto 0),
          ipb_rdata => AMC_cntr_data
        );
process(UsrClk,reset)
begin
	if(reset = '1')then
		RxBufOvfErr <= (others => '0');
		RxBufUdfErr <= (others => '0');
	elsif(UsrClk'event and UsrClk = '1')then
		for i in 0 to 11 loop
			RxBufOvfErr(i) <= (RxBufOvfErr(i) or RxBufOvf(i)) and AMC_en(i) and not test;
			RxBufUdfErr(i) <= (RxBufUdfErr(i) or RxBufUdf(i)) and AMC_en(i) and not test;
		end loop;
	end if;
end process;
process(TTC_status, AMC_en)
begin
	for i in 0 to 11 loop
		AMC_TTC_status(i) <= TTC_status(i)(0) and AMC_en(i);
		AMC_TTC_status(i+16) <= TTC_status(i)(1) and AMC_en(i);
	end loop;
end process;
TTC_lock <= and_reduce(AMC_TTC_status(27 downto 16) or not AMC_en);
BC0_lock <= AMC_TTC_status(11 downto 0);
AMC_TTC_status(31 downto 28) <= x"0";
AMC_TTC_status(15 downto 12) <= x"0";
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		if(ipb_strobe = '1' and ipb_write = '1' and ipb_addr(14 downto 0) = MON_ctrl_addr(14 downto 0) and ipb_addr(27) = '0')then
			scale <= ipb_wdata;
		end if;
		ipb_strobe_q <= ipb_strobe;
	end if;
end process;
ipb_ack <= '0' when ipb_addr(27) = '1' or ipb_addr(15 downto 11) /= AMC_reg_addr(15 downto 11) or ipb_write = '1' else ipb_strobe;
process(ipb_addr)
begin
	if(ipb_addr(15 downto 9) = L1A_buf_addr(15 downto 9))then
		ipb_rdata <= L1A_buf_do;
	elsif(ipb_addr(14 downto 11) /= AMC_reg_addr(14 downto 11))then
		ipb_rdata <= (others => '0');
	else
		ipb_rdata <= AMC_cntr_data;
	end if;
end process;
AMC_status(31 downto 28) <= (others => '0');
AMC_status(27 downto 16) <= AMC_data_valid or not AMC_en;
AMC_status(15 downto 12) <= (others => '0');
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		for i in 0 to 2 loop
			EventBuiltToggleSyncRegs(i) <= EventBuiltToggleSyncRegs(i)(2 downto 0) & EventBuiltToggle(i);
		end loop;
		for i in 0 to 11 loop
			badEventCRCToggleSyncRegs(i) <= badEventCRCToggleSyncRegs(i)(2 downto 0) & badEventCRCToggle(i);
			ReSyncFakeEventToggleSyncRegs(i) <= ReSyncFakeEventToggleSyncRegs(i)(2 downto 0) & ReSyncFakeEventToggle(i);
		end loop;
		if(CntrRst = '1')then
			badEventCRC_cntr <= (others => (others => '0'));
			ReSyncFakeEvent_cntr <= (others => (others => '0'));
			EventBuiltCnt <= (others => (others => '0'));
			fake_word_cnt <= (others => '0');
			fake_evt_cnt <= (others => '0');
			empty_evt_cnt <= (others => '0');
			fake_header_cnt <= (others => '0');
		else
			for i in 0 to 11 loop
				if(badEventCRCToggleSyncRegs(i)(3) /= badEventCRCToggleSyncRegs(i)(2))then
					badEventCRC_cntr(i) <= badEventCRC_cntr(i) + 1;
				end if;
				if(ReSyncFakeEventToggleSyncRegs(i)(3) /= ReSyncFakeEventToggleSyncRegs(i)(2))then
					ReSyncFakeEvent_cntr(i) <= ReSyncFakeEvent_cntr(i) + 1;
				end if;
			end loop;
			for i in 0 to 2 loop
				if(EventBuiltToggleSyncRegs(i)(3) /= EventBuiltToggleSyncRegs(i)(2))then
					EventBuiltCnt(i) <= EventBuiltCnt(i) + 1;
				end if;
			end loop;
			if(fake_WrEn = '1')then
				fake_word_cnt <= fake_word_cnt + 1;
			end if;
			if(fake_CRC = '1')then
				fake_evt_cnt <= fake_evt_cnt + 1;
			end if;
			if(fake_CRC = '1' and empty_event_flag = '1')then
				empty_evt_cnt <= empty_evt_cnt + 1;
			end if;
			if(fake_WrEn = '1' and fake_header = '1')then
				fake_header_cnt <= fake_header_cnt + 1;
			end if;
		end if;
		resetCntr_SyncRegs <= resetCntr_SyncRegs(1 downto 0) & resetCntr;
		CntrRst <= (not resetCntr_SyncRegs(2) and resetCntr_SyncRegs(1)) or RstAMC_link;
	end if;
end process;
i_AMC_refclk: IBUFDS_GTE2
    port map
    (
        O                               => AMC_REFCLK,
        ODIV2                           => open,
        CEB                             => '0',
        I                               => AMC_REFCLK_P,  -- Connect to package pin AB6
        IB                              => AMC_REFCLK_N       -- Connect to package pin AB5
    );
-- monitoring logic
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
    if(resetSyncRegs(2) = '1')then
			errors <= (others => '0');
			stop_mon <= '0';
		else
			if(or_reduce(AMCCRC_bad(0)) = '1' or or_reduce(AMCCRC_bad(1)) = '1' or or_reduce(AMCCRC_bad(2)) = '1')then
				errors(7) <= '1';
			end if;
			if(or_reduce(errors and scale(31 downto 24)) = '1' and rst_init_bldr = '1' and LastBlock = '1')then
				stop_mon <= '1';
			end if;
		end if;
		case scale(22 downto 19) is
			when x"0" => mon_mask <= x"00000";
			when x"1" => mon_mask <= x"80000";
			when x"2" => mon_mask <= x"c0000";
			when x"3" => mon_mask <= x"e0000";
			when x"4" => mon_mask <= x"f0000";
			when x"5" => mon_mask <= x"f8000";
			when x"6" => mon_mask <= x"fc000";
			when x"7" => mon_mask <= x"fe000";
			when x"8" => mon_mask <= x"ff000";
			when x"9" => mon_mask <= x"ff800";
			when x"a" => mon_mask <= x"ffc00";
			when x"b" => mon_mask <= x"ffe00";
			when x"c" => mon_mask <= x"fff00";
			when x"d" => mon_mask <= x"fff80";
			when x"e" => mon_mask <= x"fffc0";
			when others => mon_mask <= x"fffe0";
		end case;
    if(CDF_Header = '1' and init_bldr = '1')then
			if(and_reduce(CDF_out(51 downto 32) or mon_mask) = '1')then
				sample_event <= '1';
			else
				sample_event <= '0';
			end if;
		end if;
    if(flavor = "G2" and enSFP(3) = '0' and enSFP(2 downto 0) /= "000")then
      mon_en <= '1';
			pending <= '0';
    elsif(resetSyncRegs(2) = '1')then
-- After reset, the first event will always be recorded
      mon_en <= not scale(23);
			pending <= '0';
    elsif(rst_init_bldr = '1' and LastBlock = '1')then
			if(scale(23) = '1')then
				mon_en <= sample_event and (mon_buf_avl or WaitMonBuf) and not stop_mon;
				pending <= '0';
			else
				mon_en <= (mon_buf_avl or WaitMonBuf) and not stop_mon and (and_reduce(scale_cntr) or pending);
				pending <= not mon_buf_avl and (and_reduce(scale_cntr) or pending);
			end if;
		end if;
--    if(resetSyncRegs(2) = '1')then
--			start_wc_reg_wa <= (others => '0');
--		elsif(FirstBlock(0) = '1' and ce_wc_reg_wa = '1')then
--			start_wc_reg_wa <= wc_reg_wa;
--		end if;
		if(resetSyncRegs(2) = '1' or rst_mon_wc = '1')then
			mon_wc(0) <= (others => '0');
		elsif(zero_wc(0) = '0' and (header = '1' or sel_CDF = '1' or (more_wc(0) = '0' and rst_init_bldr = '1')))then
			mon_wc(0) <= mon_wc(0) + 1;
		elsif(AMC_wc_we(0) = '1')then
			mon_wc(0) <= mon_wc(0) + ("000" & AMC_wc(12 downto 0)) + 1;
		end if;
		if(resetSyncRegs(2) = '1' or rst_mon_wc = '1')then
			mon_wc(1) <= (others => '0');
		elsif(zero_wc(1) = '0' and (header = '1' or sel_CDF = '1' or (more_wc(1) = '0' and rst_init_bldr = '1')))then
			mon_wc(1) <= mon_wc(1) + 1;
		elsif(AMC_wc_we(1) = '1')then
			mon_wc(1) <= mon_wc(1) + ("000" & AMC_wc(12 downto 0)) + 1;
		end if;
		if(resetSyncRegs(2) = '1' or rst_mon_wc = '1')then
			mon_wc(2) <= (others => '0');
		elsif(zero_wc(2) = '0' and (header = '1' or sel_CDF = '1' or (more_wc(2) = '0' and rst_init_bldr = '1')))then
			mon_wc(2) <= mon_wc(2) + 1;
		elsif(AMC_wc_we(2) = '1')then
			mon_wc(2) <= mon_wc(2) + ("000" & AMC_wc(12 downto 0)) + 1;
		end if;
		if(ce_scale = '1')then
			if(ld_scale = '1')then
				scale_cntr <= not scale(15 downto 0);
			else
				scale_cntr <= scale_cntr + 1;
			end if;
		end if;
    if(resetSyncRegs(2) = '1')then
			ce_scale <= '0';
			ld_scale <= '0';
			ce_wc_reg_wa <= '0';
--			MonBufAbort <= '0';
		else
			ce_scale <= init_bldr and CDF_Header;
			ld_scale <= mon_en or pending;
			ce_wc_reg_wa <= rst_init_bldr and AMC_header(0)(64);
--			MonBufAbort <= rst_init_bldr and mon_en and not AMC_header(0)(64);
		end if;
		rst_mon_wc <= rst_init_bldr;
		if(resetSyncRegs(2) = '1' or rst_mon_wc = '1')then
			en_block_wc <= (others => '0');
		else
			en_block_wc <= en_block_wc or amc_header_we;
		end if;
		for i in 0 to 2 loop
			block_wc_we(i) <= rst_init_bldr and en_block_wc(i);
		end loop;
		if(resetSyncRegs(2) = '1')then
			EventInfoRdDone <= (others => '0');
		elsif(sel_AMC = x"b")then
--			EventInfoRdDone <= '1' & AMC_hasData_l;
			EventInfoRdDone <= '1' & AMC_hasData;
		else
			EventInfoRdDone <= (others => '0');
		end if;
		rst_init_bldr <= EventInfoRdDone(12);
    if(resetSyncRegs(2) = '1')then
			wc_reg_wa <= (others => '0');
--		elsif(MonBufAbort = '1')then
--			wc_reg_wa <= start_wc_reg_wa;
		elsif(ce_wc_reg_wa = '1')then
			wc_reg_wa <= wc_reg_wa + 1;
		end if;
	end if;
end process;
g_mon_evt_wc: for i in 0 to 2 generate
	i_mon_evt_wc : BRAM_SDP_MACRO
   generic map (
      BRAM_SIZE => "18Kb", -- Target BRAM, "18Kb" or "36Kb" 
      DEVICE => "7SERIES", -- Target device: "VIRTEX5", "VIRTEX6", "7SERIES", "SPARTAN6" 
      WRITE_WIDTH => 16,    -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      READ_WIDTH => 16,     -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
      DO_REG => 0, -- Optional output register (0 or 1)
      SIM_COLLISION_CHECK => "NONE", -- Collision check enable "ALL", "WARNING_ONLY", 
                                    -- "GENERATE_X_ONLY" or "NONE"       
      WRITE_MODE => "WRITE_FIRST", -- Specify "READ_FIRST" for same clock or synchronous clocks
                                   --  Specify "WRITE_FIRST for asynchrononous clocks on ports
      INIT => X"000000000000000000") --  Initial values on output port
   port map (
      DO => mon_evt_wcp(i*16+15 downto i*16),         -- Output read data port, width defined by READ_WIDTH parameter
      DI => mon_wc(i),         -- Input write data port, width defined by WRITE_WIDTH parameter
      RDADDR => ddr_pa, -- Input read address, width defined by read port depth
      RDCLK => ipb_clk,   -- 1-bit input read clock
      RDEN => '1',     -- 1-bit input read port enable
      REGCE => '1',   -- 1-bit input read output register enable
      RST => MonBuf_empty,       -- 1-bit input reset 
      WE => "11",         -- Input write enable, width defined by write port depth
      WRADDR => wc_reg_wa, -- Input write address, width defined by write port depth
      WRCLK => sysclk,   -- 1-bit input write clock
      WREN => ce_wc_reg_wa      -- 1-bit input write port enable
   );
end generate;
mon_evt_wc(15 downto 0) <= mon_evt_wcp(15 downto 0) when OneSFP = '1' else mon_evt_wcp(31 downto 16);
mon_evt_wc(31 downto 16) <= x"0000" when OneSFP = '1' else mon_evt_wcp(15 downto 0);
mon_evt_wc(47 downto 32) <= mon_evt_wcp(47 downto 32);
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		ThreeSFP <= and_reduce(EnSFP(2 downto 0));
		if(EnSFP(2 downto 0) = "011" or EnSFP(2 downto 0) = "101" or EnSFP(2 downto 0) = "110")then
			TwoSFP <= '1';
		else
			TwoSFP <= '0';
		end if;
		if(EnSFP(2 downto 0) = "001" or EnSFP(2 downto 0) = "010" or EnSFP(2 downto 0) = "100" or EnSFP(2 downto 0) = "000")then
			OneSFP <= '1';
		else
			OneSFP <= '0';
		end if;
	end if;
end process;
i_FIFO_RESET_7S: FIFO_RESET_7S PORT MAP(
		reset => resetFIFO,
		clk => sysclk,
		fifo_rst => fifo_rst,
		fifo_en => fifo_en
	);
resetFIFO <= reset or or_reduce(not AMC_txfsmresetdone and AMC_en);
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		if(resetSyncRegs(2) = '1')then
			rst_AMC_wc_sum <= '1'; 
			wr_AMC_wc_sum <= '0';
		else
			if(wr_AMC_wc_sum = '1' and ec_sel_AMC = '0')then
				rst_AMC_wc_sum <= '0'; 
			end if;
			wr_AMC_wc_sum <= ec_sel_AMC;
		end if;
		sel_AMC_q <= sel_AMC;
	end if;
end process;
i_AMC_wc_sum: RAM32x6Db PORT MAP(
		wclk => sysclk,
		di => AMC_wc_sum_di,
		we => AMC_wc_sum_we,
		wa => AMC_wc_sum_a,
		ra => AMC_wc_sum_a,
		do => AMC_wc_sum_do
	);
AMC_wc_sum_di <= AMC_wc_sum_do + AMC_wc(5 downto 0) when rst_AMC_wc_sum = '0' else AMC_wc(5 downto 0);
AMC_wc_sum_a(3 downto 0) <= sel_AMC_q when wr_AMC_wc_sum = '1' else AMC_if_ADDR(4 downto 1);
end Behavioral;

