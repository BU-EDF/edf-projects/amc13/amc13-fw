----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:09:26 05/16/2012 
-- Design Name: 
-- Module Name:    fake_event - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity fake_event is
    Port (
           sysclk : in  STD_LOGIC;
           UsrClk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           fifo_rst : in  STD_LOGIC;
           fifo_en : in  STD_LOGIC;
           fake_en : in  STD_LOGIC;
           sync : in  STD_LOGIC;
           ovfl_warning : in  STD_LOGIC;
					 fake_length	: in   std_logic_vector(19 downto 0); -- in 64 bit words
           LinkFull : in  STD_LOGIC;
					 L1A_DATA	: in   std_logic_vector(15 downto 0);
					 L1A_WrEn : in   std_logic;
           fake_header : out  STD_LOGIC;
           fake_CRC : out  STD_LOGIC;
           empty_event_flag : out  STD_LOGIC;
           fake_DATA : out  STD_LOGIC_VECTOR (15 downto 0);
           fake_WrEn : out  STD_LOGIC
					 );
end fake_event;

architecture Behavioral of fake_event is
signal L1A_infoDo : std_logic_vector(15 downto 0) := (others => '0');
signal L1A_info_RdEn : std_logic := '0';
signal L1A_info_WrEn : std_logic := '0';
signal L1A_info_Empty : std_logic := '0';
signal L1A_info_avl : std_logic_vector(2 downto 0) := (others => '0');
signal ovfl_warning_sync : std_logic_vector(3 downto 0) := (others =>'0');
signal fake_WrEn_i : std_logic := '0';
signal bld_event : std_logic := '0';
signal AMC_header : std_logic := '0';
signal data_header : std_logic := '0';
signal data_trailer : std_logic := '0';
signal data_trailer_dl : std_logic := '0';
signal evn_LSB : std_logic_vector(7 downto 0) := (others => '0');
signal data_wc : std_logic_vector(19 downto 0) := (others => '0');
signal wc2send : std_logic_vector(21 downto 0) := (others => '0');
signal last_word : std_logic := '0';
signal last_byte : std_logic := '0';
signal event_length : std_logic_vector(19 downto 0) := (others => '0');
signal ec_data_wc : std_logic := '0';
signal RDCOUNT : std_logic_vector(9 downto 0) := (others => '0');
signal WRCOUNT : std_logic_vector(9 downto 0) := (others => '0');
signal fake_CRC_i : std_logic := '0';
begin
fake_WrEn <= fake_WrEn_i;
fake_CRC <= fake_CRC_i;
i_L1A_info : FIFO_DUALCLOCK_MACRO
   generic map (
      DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
      ALMOST_FULL_OFFSET => X"0080",  -- Sets almost full threshold
      ALMOST_EMPTY_OFFSET => X"0080", -- Sets the almost empty threshold
      DATA_WIDTH => 16,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      FIFO_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb" 
      FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
   port map (
      ALMOSTEMPTY => open,   -- 1-bit output almost empty
      ALMOSTFULL => open,     -- 1-bit output almost full
      DO => L1A_infoDo,                     -- Output data, width defined by DATA_WIDTH parameter
      EMPTY => L1A_info_Empty,               -- 1-bit output empty
      FULL => open,                 -- 1-bit output full
      RDCOUNT => RDCOUNT,           -- Output read count, width determined by FIFO depth
      RDERR => open,               -- 1-bit output read error
      WRCOUNT => WRCOUNT,           -- Output write count, width determined by FIFO depth
      WRERR => open,               -- 1-bit output write error
      DI => L1A_Data,                     -- Input data, width defined by DATA_WIDTH parameter
      RDCLK => UsrClk,               -- 1-bit input read clock
      RDEN => L1A_info_RdEn,                 -- 1-bit input read enable
      RST => fifo_rst,                   -- 1-bit input reset
      WRCLK => sysclk,               -- 1-bit input write clock
      WREN => L1A_info_WrEn                  -- 1-bit input write enable
   );
L1A_info_WrEn <= fake_en and fifo_en and L1A_WrEn;
i_data_trailer_dl : SRL16E
   port map (
      Q => data_trailer_dl,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => UsrClk,   -- Clock input
      D => data_trailer        -- SRL data input
   );
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		ovfl_warning_sync <= ovfl_warning_sync(2 downto 0) & ovfl_warning;
		if(data_header = '1')then
			empty_event_flag <= ovfl_warning_sync(3);
			if(ovfl_warning_sync(3) = '1')then
				event_length <= x"00003";
			elsif(and_reduce(fake_length(19 downto 2)) = '1')then
				event_length <= x"fffff";
			else
				event_length <= fake_length + 3;
			end if;
		end if;
		if(ec_data_wc = '0' and AMC_header = '1')then
			wc2send(21 downto 2) <= event_length - 1;
			wc2send(1 downto 0) <= "10";
		elsif(ec_data_wc = '1')then
			wc2send <= wc2send - 1;
		end if;
		fake_header <= not reset and AMC_header and data_wc(1) and data_wc(0);
		fake_CRC_i <= data_trailer;
		if(AMC_header = '1')then
			if(data_wc(2 downto 0) = "000")then
				fake_DATA <= event_length(15 downto 0);
			elsif(data_wc(2 downto 0) = "001")then
				fake_DATA <= L1A_infoDo(15 downto 4) & event_length(19 downto 16);
			elsif(data_wc(2 downto 0) = "010")then
				fake_DATA <= L1A_infoDo;
				evn_LSB <= L1A_infoDo(7 downto 0);
			elsif(data_wc(2 downto 0) = "011")then
				fake_DATA <= x"00" & L1A_infoDo(7 downto 0);
			else
				fake_DATA <= x"0000";
			end if;
		elsif(L1A_info_RdEn = '1')then
			fake_DATA <= L1A_infoDo;
		elsif(last_word = '1' and wc2send(1) = '1')then
			fake_DATA <= event_length(15 downto 0);
		elsif(last_word = '1' and wc2send(0) = '1')then
			fake_DATA <= evn_LSB & x"0" & event_length(19 downto 16);
		else
			fake_DATA <= data_wc(15 downto 0);
		end if;
	end if;
end process;
process(UsrClk,reset)
begin
	if(reset = '1')then
		L1A_info_avl <= (others => '0');
		bld_event <= '0';
		ec_data_wc <= '0';
		AMC_header <= '0';
		L1A_info_RdEn <= '0';
		data_header <= '1';
		data_trailer <= '0';
		data_wc <= (others => '0');
		fake_WrEn_i <= '0';
	elsif(UsrClk'event and UsrClk = '1')then
		if(L1A_info_empty = '1')then
			L1A_info_avl <= (others => '0');
		elsif(L1A_info_avl(2) = '0')then
			L1A_info_avl <= L1A_info_avl + 1;
		end if;
		if(last_word = '1')then
			bld_event <= '0';
		elsif(AMC_header = '1')then
			bld_event <= '1';
		end if;
		if(last_word = '1' and wc2send(1 downto 0) = "00")then
			last_byte <= '1';
		else
			last_byte <= '0';
		end if;
		if(AMC_header = '1' or last_word = '1' or (data_wc(1 downto 0) = "00" and bld_event = '1' and LinkFull = '0'))then
			ec_data_wc <= '1';
		elsif(last_byte = '1' or (data_wc(1 downto 0) = "11" and (bld_event = '0' or LinkFull = '1')))then
			ec_data_wc <= '0';
		end if;
		if(data_trailer_dl = '1')then
			data_header <= '1';
		elsif(ec_data_wc = '1')then
			data_header <= '0';
		end if;
		if(LinkFull = '0' and L1A_info_avl(2) = '1' and data_header = '1' and sync = '1')then
			AMC_header <= '1';
		elsif(data_wc(2) = '1')then
			AMC_header <= '0';
		end if;
		if(fifo_en = '0' or AMC_header = '0' or ec_data_wc = '0' or data_wc(1 downto 0) = "11")then
			L1A_info_RdEn <= '0';
		else
			L1A_info_RdEn <= '1';
		end if;
		if(bld_event = '0')then
			data_wc <= (others => '0');
		elsif(ec_data_wc = '1')then
			data_wc <= data_wc + 1;
		end if;
		if(data_header = '0' and or_reduce(wc2send(21 downto 2)) = '0' and wc2send(1 downto 0) /= "00")then
			last_word <= '1';
		else
			last_word <= '0';
		end if;
		if(last_word = '1' and wc2send(0) = '1' and ec_data_wc = '1')then
			data_trailer <= '1';
		else
			data_trailer <= '0';
		end if;
		fake_WrEn_i <= ec_data_wc;
	end if;
end process;
end Behavioral;

