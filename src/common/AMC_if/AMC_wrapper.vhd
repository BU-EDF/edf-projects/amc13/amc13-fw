----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:15:28 03/21/2013 
-- Design Name: 
-- Module Name:    AMC_wrapper - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity AMC_wrapper is
    Port ( DRPclk : in  STD_LOGIC;
           SOFT_RESET : in  STD_LOGIC;
           UsrClk : in  STD_LOGIC;
           test : in  STD_LOGIC;
           Dis_pd : in  STD_LOGIC;
           AMC_en : in  STD_LOGIC_VECTOR(11 downto 0);
					 RXDATA : out array12X16;
           RxBufOvf : out  STD_LOGIC_VECTOR(11 downto 0);
           RxBufUdf : out  STD_LOGIC_VECTOR(11 downto 0);
           sampleRatio : in  STD_LOGIC;
           updateRatio : in  STD_LOGIC;
           RxClkRatio : out  array12x21;
           rxprbserr : out  STD_LOGIC_VECTOR(11 downto 0);
					 rxprbssel : in array12X3;
           RXNOTINTABLE : out  array12X2;
           rxcommaalignen : in  STD_LOGIC_VECTOR(11 downto 0);
					 rxchariscomma : out array12X2;
					 rxcharisk : out array12X2;
           rxresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
					 txdiffctrl : in array12X4;
					 TXDATA : in array12X16;
           txoutclk : out  STD_LOGIC_VECTOR(11 downto 0);
					 txcharisk : in array12X2;
           txresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
					 txprbssel : in array12X3;
           qpll_lock : out  STD_LOGIC_VECTOR(2 downto 0);
           txfsmresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
           rxfsmresetdone : out  STD_LOGIC_VECTOR(11 downto 0);
           data_valid : in  STD_LOGIC_VECTOR(11 downto 0);
           AMC_REFCLK : in  STD_LOGIC;
           RXN : in  STD_LOGIC_VECTOR(11 downto 0);
           RXP : in  STD_LOGIC_VECTOR(11 downto 0);
           TXN : out  STD_LOGIC_VECTOR(11 downto 0);
           TXP : out  STD_LOGIC_VECTOR(11 downto 0)
					 );
end AMC_wrapper;
architecture Behavioral of AMC_wrapper is
component amc_gtx5Gpd_init 
generic
(
    EXAMPLE_SIM_GTRESET_SPEEDUP             : string    := "TRUE";     -- simulation setting for GT SecureIP model
    EXAMPLE_SIMULATION                      : integer   := 0;          -- Set to 1 for simulation
 
    STABLE_CLOCK_PERIOD                     : integer   := 20;  
        -- Set to 1 for simulation
    EXAMPLE_USE_CHIPSCOPE                   : integer   := 0           -- Set to 1 to use Chipscope to drive resets

);
port
(
		SYSCLK_IN : IN std_logic;
		SOFT_RESET_IN : IN std_logic;
		DONT_RESET_ON_DATA_ERROR_IN : IN std_logic;
		GT0_DATA_VALID_IN : IN std_logic;
		GT1_DATA_VALID_IN : IN std_logic;
		GT2_DATA_VALID_IN : IN std_logic;
		GT3_DATA_VALID_IN : IN std_logic;
		GT4_DATA_VALID_IN : IN std_logic;
		GT5_DATA_VALID_IN : IN std_logic;
		GT6_DATA_VALID_IN : IN std_logic;
		GT7_DATA_VALID_IN : IN std_logic;
		GT8_DATA_VALID_IN : IN std_logic;
		GT9_DATA_VALID_IN : IN std_logic;
		GT10_DATA_VALID_IN : IN std_logic;
		GT11_DATA_VALID_IN : IN std_logic;
		gt0_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt0_drpclk_in : IN std_logic;
		gt0_drpdi_in : IN std_logic_vector(15 downto 0);
		gt0_drpen_in : IN std_logic;
		gt0_drpwe_in : IN std_logic;
		gt0_loopback_in : IN std_logic_vector(2 downto 0);
		gt0_rxpd_in : IN std_logic_vector(1 downto 0);
		gt0_txpd_in : IN std_logic_vector(1 downto 0);
		gt0_eyescanreset_in : IN std_logic;
		gt0_rxuserrdy_in : IN std_logic;
		gt0_eyescantrigger_in : IN std_logic;
		gt0_rxusrclk_in : IN std_logic;
		gt0_rxusrclk2_in : IN std_logic;
		gt0_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt0_rxprbscntreset_in : IN std_logic;
		gt0_gtxrxp_in : IN std_logic;
		gt0_gtxrxn_in : IN std_logic;
		gt0_rxmcommaalignen_in : IN std_logic;
		gt0_rxpcommaalignen_in : IN std_logic;
		gt0_rxdfelpmreset_in : IN std_logic;
		gt0_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt0_gtrxreset_in : IN std_logic;
		gt0_rxpmareset_in : IN std_logic;
		gt0_gttxreset_in : IN std_logic;
		gt0_txuserrdy_in : IN std_logic;
		gt0_txusrclk_in : IN std_logic;
		gt0_txusrclk2_in : IN std_logic;
		gt0_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt0_txdata_in : IN std_logic_vector(15 downto 0);
		gt0_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt0_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt1_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt1_drpclk_in : IN std_logic;
		gt1_drpdi_in : IN std_logic_vector(15 downto 0);
		gt1_drpen_in : IN std_logic;
		gt1_drpwe_in : IN std_logic;
		gt1_loopback_in : IN std_logic_vector(2 downto 0);
		gt1_rxpd_in : IN std_logic_vector(1 downto 0);
		gt1_txpd_in : IN std_logic_vector(1 downto 0);
		gt1_eyescanreset_in : IN std_logic;
		gt1_rxuserrdy_in : IN std_logic;
		gt1_eyescantrigger_in : IN std_logic;
		gt1_rxusrclk_in : IN std_logic;
		gt1_rxusrclk2_in : IN std_logic;
		gt1_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt1_rxprbscntreset_in : IN std_logic;
		gt1_gtxrxp_in : IN std_logic;
		gt1_gtxrxn_in : IN std_logic;
		gt1_rxmcommaalignen_in : IN std_logic;
		gt1_rxpcommaalignen_in : IN std_logic;
		gt1_rxdfelpmreset_in : IN std_logic;
		gt1_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt1_gtrxreset_in : IN std_logic;
		gt1_rxpmareset_in : IN std_logic;
		gt1_gttxreset_in : IN std_logic;
		gt1_txuserrdy_in : IN std_logic;
		gt1_txusrclk_in : IN std_logic;
		gt1_txusrclk2_in : IN std_logic;
		gt1_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt1_txdata_in : IN std_logic_vector(15 downto 0);
		gt1_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt1_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt2_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt2_drpclk_in : IN std_logic;
		gt2_drpdi_in : IN std_logic_vector(15 downto 0);
		gt2_drpen_in : IN std_logic;
		gt2_drpwe_in : IN std_logic;
		gt2_loopback_in : IN std_logic_vector(2 downto 0);
		gt2_rxpd_in : IN std_logic_vector(1 downto 0);
		gt2_txpd_in : IN std_logic_vector(1 downto 0);
		gt2_eyescanreset_in : IN std_logic;
		gt2_rxuserrdy_in : IN std_logic;
		gt2_eyescantrigger_in : IN std_logic;
		gt2_rxusrclk_in : IN std_logic;
		gt2_rxusrclk2_in : IN std_logic;
		gt2_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt2_rxprbscntreset_in : IN std_logic;
		gt2_gtxrxp_in : IN std_logic;
		gt2_gtxrxn_in : IN std_logic;
		gt2_rxmcommaalignen_in : IN std_logic;
		gt2_rxpcommaalignen_in : IN std_logic;
		gt2_rxdfelpmreset_in : IN std_logic;
		gt2_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt2_gtrxreset_in : IN std_logic;
		gt2_rxpmareset_in : IN std_logic;
		gt2_gttxreset_in : IN std_logic;
		gt2_txuserrdy_in : IN std_logic;
		gt2_txusrclk_in : IN std_logic;
		gt2_txusrclk2_in : IN std_logic;
		gt2_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt2_txdata_in : IN std_logic_vector(15 downto 0);
		gt2_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt2_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt3_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt3_drpclk_in : IN std_logic;
		gt3_drpdi_in : IN std_logic_vector(15 downto 0);
		gt3_drpen_in : IN std_logic;
		gt3_drpwe_in : IN std_logic;
		gt3_loopback_in : IN std_logic_vector(2 downto 0);
		gt3_rxpd_in : IN std_logic_vector(1 downto 0);
		gt3_txpd_in : IN std_logic_vector(1 downto 0);
		gt3_eyescanreset_in : IN std_logic;
		gt3_rxuserrdy_in : IN std_logic;
		gt3_eyescantrigger_in : IN std_logic;
		gt3_rxusrclk_in : IN std_logic;
		gt3_rxusrclk2_in : IN std_logic;
		gt3_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt3_rxprbscntreset_in : IN std_logic;
		gt3_gtxrxp_in : IN std_logic;
		gt3_gtxrxn_in : IN std_logic;
		gt3_rxmcommaalignen_in : IN std_logic;
		gt3_rxpcommaalignen_in : IN std_logic;
		gt3_rxdfelpmreset_in : IN std_logic;
		gt3_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt3_gtrxreset_in : IN std_logic;
		gt3_rxpmareset_in : IN std_logic;
		gt3_gttxreset_in : IN std_logic;
		gt3_txuserrdy_in : IN std_logic;
		gt3_txusrclk_in : IN std_logic;
		gt3_txusrclk2_in : IN std_logic;
		gt3_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt3_txdata_in : IN std_logic_vector(15 downto 0);
		gt3_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt3_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt4_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt4_drpclk_in : IN std_logic;
		gt4_drpdi_in : IN std_logic_vector(15 downto 0);
		gt4_drpen_in : IN std_logic;
		gt4_drpwe_in : IN std_logic;
		gt4_loopback_in : IN std_logic_vector(2 downto 0);
		gt4_rxpd_in : IN std_logic_vector(1 downto 0);
		gt4_txpd_in : IN std_logic_vector(1 downto 0);
		gt4_eyescanreset_in : IN std_logic;
		gt4_rxuserrdy_in : IN std_logic;
		gt4_eyescantrigger_in : IN std_logic;
		gt4_rxusrclk_in : IN std_logic;
		gt4_rxusrclk2_in : IN std_logic;
		gt4_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt4_rxprbscntreset_in : IN std_logic;
		gt4_gtxrxp_in : IN std_logic;
		gt4_gtxrxn_in : IN std_logic;
		gt4_rxmcommaalignen_in : IN std_logic;
		gt4_rxpcommaalignen_in : IN std_logic;
		gt4_rxdfelpmreset_in : IN std_logic;
		gt4_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt4_gtrxreset_in : IN std_logic;
		gt4_rxpmareset_in : IN std_logic;
		gt4_gttxreset_in : IN std_logic;
		gt4_txuserrdy_in : IN std_logic;
		gt4_txusrclk_in : IN std_logic;
		gt4_txusrclk2_in : IN std_logic;
		gt4_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt4_txdata_in : IN std_logic_vector(15 downto 0);
		gt4_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt4_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt5_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt5_drpclk_in : IN std_logic;
		gt5_drpdi_in : IN std_logic_vector(15 downto 0);
		gt5_drpen_in : IN std_logic;
		gt5_drpwe_in : IN std_logic;
		gt5_loopback_in : IN std_logic_vector(2 downto 0);
		gt5_rxpd_in : IN std_logic_vector(1 downto 0);
		gt5_txpd_in : IN std_logic_vector(1 downto 0);
		gt5_eyescanreset_in : IN std_logic;
		gt5_rxuserrdy_in : IN std_logic;
		gt5_eyescantrigger_in : IN std_logic;
		gt5_rxusrclk_in : IN std_logic;
		gt5_rxusrclk2_in : IN std_logic;
		gt5_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt5_rxprbscntreset_in : IN std_logic;
		gt5_gtxrxp_in : IN std_logic;
		gt5_gtxrxn_in : IN std_logic;
		gt5_rxmcommaalignen_in : IN std_logic;
		gt5_rxpcommaalignen_in : IN std_logic;
		gt5_rxdfelpmreset_in : IN std_logic;
		gt5_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt5_gtrxreset_in : IN std_logic;
		gt5_rxpmareset_in : IN std_logic;
		gt5_gttxreset_in : IN std_logic;
		gt5_txuserrdy_in : IN std_logic;
		gt5_txusrclk_in : IN std_logic;
		gt5_txusrclk2_in : IN std_logic;
		gt5_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt5_txdata_in : IN std_logic_vector(15 downto 0);
		gt5_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt5_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt6_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt6_drpclk_in : IN std_logic;
		gt6_drpdi_in : IN std_logic_vector(15 downto 0);
		gt6_drpen_in : IN std_logic;
		gt6_drpwe_in : IN std_logic;
		gt6_loopback_in : IN std_logic_vector(2 downto 0);
		gt6_rxpd_in : IN std_logic_vector(1 downto 0);
		gt6_txpd_in : IN std_logic_vector(1 downto 0);
		gt6_eyescanreset_in : IN std_logic;
		gt6_rxuserrdy_in : IN std_logic;
		gt6_eyescantrigger_in : IN std_logic;
		gt6_rxusrclk_in : IN std_logic;
		gt6_rxusrclk2_in : IN std_logic;
		gt6_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt6_rxprbscntreset_in : IN std_logic;
		gt6_gtxrxp_in : IN std_logic;
		gt6_gtxrxn_in : IN std_logic;
		gt6_rxmcommaalignen_in : IN std_logic;
		gt6_rxpcommaalignen_in : IN std_logic;
		gt6_rxdfelpmreset_in : IN std_logic;
		gt6_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt6_gtrxreset_in : IN std_logic;
		gt6_rxpmareset_in : IN std_logic;
		gt6_gttxreset_in : IN std_logic;
		gt6_txuserrdy_in : IN std_logic;
		gt6_txusrclk_in : IN std_logic;
		gt6_txusrclk2_in : IN std_logic;
		gt6_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt6_txdata_in : IN std_logic_vector(15 downto 0);
		gt6_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt6_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt7_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt7_drpclk_in : IN std_logic;
		gt7_drpdi_in : IN std_logic_vector(15 downto 0);
		gt7_drpen_in : IN std_logic;
		gt7_drpwe_in : IN std_logic;
		gt7_loopback_in : IN std_logic_vector(2 downto 0);
		gt7_rxpd_in : IN std_logic_vector(1 downto 0);
		gt7_txpd_in : IN std_logic_vector(1 downto 0);
		gt7_eyescanreset_in : IN std_logic;
		gt7_rxuserrdy_in : IN std_logic;
		gt7_eyescantrigger_in : IN std_logic;
		gt7_rxusrclk_in : IN std_logic;
		gt7_rxusrclk2_in : IN std_logic;
		gt7_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt7_rxprbscntreset_in : IN std_logic;
		gt7_gtxrxp_in : IN std_logic;
		gt7_gtxrxn_in : IN std_logic;
		gt7_rxmcommaalignen_in : IN std_logic;
		gt7_rxpcommaalignen_in : IN std_logic;
		gt7_rxdfelpmreset_in : IN std_logic;
		gt7_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt7_gtrxreset_in : IN std_logic;
		gt7_rxpmareset_in : IN std_logic;
		gt7_gttxreset_in : IN std_logic;
		gt7_txuserrdy_in : IN std_logic;
		gt7_txusrclk_in : IN std_logic;
		gt7_txusrclk2_in : IN std_logic;
		gt7_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt7_txdata_in : IN std_logic_vector(15 downto 0);
		gt7_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt7_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt8_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt8_drpclk_in : IN std_logic;
		gt8_drpdi_in : IN std_logic_vector(15 downto 0);
		gt8_drpen_in : IN std_logic;
		gt8_drpwe_in : IN std_logic;
		gt8_loopback_in : IN std_logic_vector(2 downto 0);
		gt8_rxpd_in : IN std_logic_vector(1 downto 0);
		gt8_txpd_in : IN std_logic_vector(1 downto 0);
		gt8_eyescanreset_in : IN std_logic;
		gt8_rxuserrdy_in : IN std_logic;
		gt8_eyescantrigger_in : IN std_logic;
		gt8_rxusrclk_in : IN std_logic;
		gt8_rxusrclk2_in : IN std_logic;
		gt8_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt8_rxprbscntreset_in : IN std_logic;
		gt8_gtxrxp_in : IN std_logic;
		gt8_gtxrxn_in : IN std_logic;
		gt8_rxmcommaalignen_in : IN std_logic;
		gt8_rxpcommaalignen_in : IN std_logic;
		gt8_rxdfelpmreset_in : IN std_logic;
		gt8_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt8_gtrxreset_in : IN std_logic;
		gt8_rxpmareset_in : IN std_logic;
		gt8_gttxreset_in : IN std_logic;
		gt8_txuserrdy_in : IN std_logic;
		gt8_txusrclk_in : IN std_logic;
		gt8_txusrclk2_in : IN std_logic;
		gt8_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt8_txdata_in : IN std_logic_vector(15 downto 0);
		gt8_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt8_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt9_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt9_drpclk_in : IN std_logic;
		gt9_drpdi_in : IN std_logic_vector(15 downto 0);
		gt9_drpen_in : IN std_logic;
		gt9_drpwe_in : IN std_logic;
		gt9_loopback_in : IN std_logic_vector(2 downto 0);
		gt9_rxpd_in : IN std_logic_vector(1 downto 0);
		gt9_txpd_in : IN std_logic_vector(1 downto 0);
		gt9_eyescanreset_in : IN std_logic;
		gt9_rxuserrdy_in : IN std_logic;
		gt9_eyescantrigger_in : IN std_logic;
		gt9_rxusrclk_in : IN std_logic;
		gt9_rxusrclk2_in : IN std_logic;
		gt9_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt9_rxprbscntreset_in : IN std_logic;
		gt9_gtxrxp_in : IN std_logic;
		gt9_gtxrxn_in : IN std_logic;
		gt9_rxmcommaalignen_in : IN std_logic;
		gt9_rxpcommaalignen_in : IN std_logic;
		gt9_rxdfelpmreset_in : IN std_logic;
		gt9_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt9_gtrxreset_in : IN std_logic;
		gt9_rxpmareset_in : IN std_logic;
		gt9_gttxreset_in : IN std_logic;
		gt9_txuserrdy_in : IN std_logic;
		gt9_txusrclk_in : IN std_logic;
		gt9_txusrclk2_in : IN std_logic;
		gt9_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt9_txdata_in : IN std_logic_vector(15 downto 0);
		gt9_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt9_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt10_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt10_drpclk_in : IN std_logic;
		gt10_drpdi_in : IN std_logic_vector(15 downto 0);
		gt10_drpen_in : IN std_logic;
		gt10_drpwe_in : IN std_logic;
		gt10_loopback_in : IN std_logic_vector(2 downto 0);
		gt10_rxpd_in : IN std_logic_vector(1 downto 0);
		gt10_txpd_in : IN std_logic_vector(1 downto 0);
		gt10_eyescanreset_in : IN std_logic;
		gt10_rxuserrdy_in : IN std_logic;
		gt10_eyescantrigger_in : IN std_logic;
		gt10_rxusrclk_in : IN std_logic;
		gt10_rxusrclk2_in : IN std_logic;
		gt10_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt10_rxprbscntreset_in : IN std_logic;
		gt10_gtxrxp_in : IN std_logic;
		gt10_gtxrxn_in : IN std_logic;
		gt10_rxmcommaalignen_in : IN std_logic;
		gt10_rxpcommaalignen_in : IN std_logic;
		gt10_rxdfelpmreset_in : IN std_logic;
		gt10_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt10_gtrxreset_in : IN std_logic;
		gt10_rxpmareset_in : IN std_logic;
		gt10_gttxreset_in : IN std_logic;
		gt10_txuserrdy_in : IN std_logic;
		gt10_txusrclk_in : IN std_logic;
		gt10_txusrclk2_in : IN std_logic;
		gt10_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt10_txdata_in : IN std_logic_vector(15 downto 0);
		gt10_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt10_txprbssel_in : IN std_logic_vector(2 downto 0);
		gt11_drpaddr_in : IN std_logic_vector(8 downto 0);
		gt11_drpclk_in : IN std_logic;
		gt11_drpdi_in : IN std_logic_vector(15 downto 0);
		gt11_drpen_in : IN std_logic;
		gt11_drpwe_in : IN std_logic;
		gt11_loopback_in : IN std_logic_vector(2 downto 0);
		gt11_rxpd_in : IN std_logic_vector(1 downto 0);
		gt11_txpd_in : IN std_logic_vector(1 downto 0);
		gt11_eyescanreset_in : IN std_logic;
		gt11_rxuserrdy_in : IN std_logic;
		gt11_eyescantrigger_in : IN std_logic;
		gt11_rxusrclk_in : IN std_logic;
		gt11_rxusrclk2_in : IN std_logic;
		gt11_rxprbssel_in : IN std_logic_vector(2 downto 0);
		gt11_rxprbscntreset_in : IN std_logic;
		gt11_gtxrxp_in : IN std_logic;
		gt11_gtxrxn_in : IN std_logic;
		gt11_rxmcommaalignen_in : IN std_logic;
		gt11_rxpcommaalignen_in : IN std_logic;
		gt11_rxdfelpmreset_in : IN std_logic;
		gt11_rxmonitorsel_in : IN std_logic_vector(1 downto 0);
		gt11_gtrxreset_in : IN std_logic;
		gt11_rxpmareset_in : IN std_logic;
		gt11_gttxreset_in : IN std_logic;
		gt11_txuserrdy_in : IN std_logic;
		gt11_txusrclk_in : IN std_logic;
		gt11_txusrclk2_in : IN std_logic;
		gt11_txdiffctrl_in : IN std_logic_vector(3 downto 0);
		gt11_txdata_in : IN std_logic_vector(15 downto 0);
		gt11_txcharisk_in : IN std_logic_vector(1 downto 0);
		gt11_txprbssel_in : IN std_logic_vector(2 downto 0);
		GT0_QPLLLOCK_IN : IN std_logic;
		GT0_QPLLREFCLKLOST_IN : IN std_logic;
		GT0_QPLLOUTCLK_IN : IN std_logic;
		GT0_QPLLOUTREFCLK_IN : IN std_logic;
		GT1_QPLLLOCK_IN : IN std_logic;
		GT1_QPLLREFCLKLOST_IN : IN std_logic;
		GT1_QPLLOUTCLK_IN : IN std_logic;
		GT1_QPLLOUTREFCLK_IN : IN std_logic;
		GT2_QPLLLOCK_IN : IN std_logic;
		GT2_QPLLREFCLKLOST_IN : IN std_logic;
		GT2_QPLLOUTCLK_IN : IN std_logic;
		GT2_QPLLOUTREFCLK_IN : IN std_logic;          
		GT0_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT0_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT1_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT1_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT2_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT2_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT3_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT3_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT4_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT4_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT5_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT5_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT6_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT6_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT7_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT7_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT8_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT8_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT9_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT9_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT10_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT10_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT11_TX_FSM_RESET_DONE_OUT : OUT std_logic;
		GT11_RX_FSM_RESET_DONE_OUT : OUT std_logic;
		gt0_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt0_drprdy_out : OUT std_logic;
		gt0_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt0_eyescandataerror_out : OUT std_logic;
		gt0_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt0_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt0_rxprbserr_out : OUT std_logic;
		gt0_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt0_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt0_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt0_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt0_rxoutclk_out : OUT std_logic;
		gt0_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt0_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt0_rxresetdone_out : OUT std_logic;
		gt0_gtxtxn_out : OUT std_logic;
		gt0_gtxtxp_out : OUT std_logic;
		gt0_txoutclk_out : OUT std_logic;
		gt0_txoutclkfabric_out : OUT std_logic;
		gt0_txoutclkpcs_out : OUT std_logic;
		gt0_txresetdone_out : OUT std_logic;
		gt1_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt1_drprdy_out : OUT std_logic;
		gt1_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt1_eyescandataerror_out : OUT std_logic;
		gt1_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt1_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt1_rxprbserr_out : OUT std_logic;
		gt1_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt1_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt1_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt1_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt1_rxoutclk_out : OUT std_logic;
		gt1_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt1_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt1_rxresetdone_out : OUT std_logic;
		gt1_gtxtxn_out : OUT std_logic;
		gt1_gtxtxp_out : OUT std_logic;
		gt1_txoutclk_out : OUT std_logic;
		gt1_txoutclkfabric_out : OUT std_logic;
		gt1_txoutclkpcs_out : OUT std_logic;
		gt1_txresetdone_out : OUT std_logic;
		gt2_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt2_drprdy_out : OUT std_logic;
		gt2_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt2_eyescandataerror_out : OUT std_logic;
		gt2_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt2_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt2_rxprbserr_out : OUT std_logic;
		gt2_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt2_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt2_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt2_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt2_rxoutclk_out : OUT std_logic;
		gt2_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt2_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt2_rxresetdone_out : OUT std_logic;
		gt2_gtxtxn_out : OUT std_logic;
		gt2_gtxtxp_out : OUT std_logic;
		gt2_txoutclk_out : OUT std_logic;
		gt2_txoutclkfabric_out : OUT std_logic;
		gt2_txoutclkpcs_out : OUT std_logic;
		gt2_txresetdone_out : OUT std_logic;
		gt3_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt3_drprdy_out : OUT std_logic;
		gt3_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt3_eyescandataerror_out : OUT std_logic;
		gt3_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt3_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt3_rxprbserr_out : OUT std_logic;
		gt3_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt3_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt3_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt3_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt3_rxoutclk_out : OUT std_logic;
		gt3_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt3_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt3_rxresetdone_out : OUT std_logic;
		gt3_gtxtxn_out : OUT std_logic;
		gt3_gtxtxp_out : OUT std_logic;
		gt3_txoutclk_out : OUT std_logic;
		gt3_txoutclkfabric_out : OUT std_logic;
		gt3_txoutclkpcs_out : OUT std_logic;
		gt3_txresetdone_out : OUT std_logic;
		gt4_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt4_drprdy_out : OUT std_logic;
		gt4_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt4_eyescandataerror_out : OUT std_logic;
		gt4_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt4_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt4_rxprbserr_out : OUT std_logic;
		gt4_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt4_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt4_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt4_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt4_rxoutclk_out : OUT std_logic;
		gt4_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt4_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt4_rxresetdone_out : OUT std_logic;
		gt4_gtxtxn_out : OUT std_logic;
		gt4_gtxtxp_out : OUT std_logic;
		gt4_txoutclk_out : OUT std_logic;
		gt4_txoutclkfabric_out : OUT std_logic;
		gt4_txoutclkpcs_out : OUT std_logic;
		gt4_txresetdone_out : OUT std_logic;
		gt5_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt5_drprdy_out : OUT std_logic;
		gt5_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt5_eyescandataerror_out : OUT std_logic;
		gt5_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt5_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt5_rxprbserr_out : OUT std_logic;
		gt5_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt5_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt5_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt5_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt5_rxoutclk_out : OUT std_logic;
		gt5_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt5_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt5_rxresetdone_out : OUT std_logic;
		gt5_gtxtxn_out : OUT std_logic;
		gt5_gtxtxp_out : OUT std_logic;
		gt5_txoutclk_out : OUT std_logic;
		gt5_txoutclkfabric_out : OUT std_logic;
		gt5_txoutclkpcs_out : OUT std_logic;
		gt5_txresetdone_out : OUT std_logic;
		gt6_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt6_drprdy_out : OUT std_logic;
		gt6_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt6_eyescandataerror_out : OUT std_logic;
		gt6_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt6_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt6_rxprbserr_out : OUT std_logic;
		gt6_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt6_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt6_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt6_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt6_rxoutclk_out : OUT std_logic;
		gt6_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt6_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt6_rxresetdone_out : OUT std_logic;
		gt6_gtxtxn_out : OUT std_logic;
		gt6_gtxtxp_out : OUT std_logic;
		gt6_txoutclk_out : OUT std_logic;
		gt6_txoutclkfabric_out : OUT std_logic;
		gt6_txoutclkpcs_out : OUT std_logic;
		gt6_txresetdone_out : OUT std_logic;
		gt7_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt7_drprdy_out : OUT std_logic;
		gt7_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt7_eyescandataerror_out : OUT std_logic;
		gt7_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt7_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt7_rxprbserr_out : OUT std_logic;
		gt7_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt7_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt7_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt7_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt7_rxoutclk_out : OUT std_logic;
		gt7_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt7_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt7_rxresetdone_out : OUT std_logic;
		gt7_gtxtxn_out : OUT std_logic;
		gt7_gtxtxp_out : OUT std_logic;
		gt7_txoutclk_out : OUT std_logic;
		gt7_txoutclkfabric_out : OUT std_logic;
		gt7_txoutclkpcs_out : OUT std_logic;
		gt7_txresetdone_out : OUT std_logic;
		gt8_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt8_drprdy_out : OUT std_logic;
		gt8_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt8_eyescandataerror_out : OUT std_logic;
		gt8_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt8_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt8_rxprbserr_out : OUT std_logic;
		gt8_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt8_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt8_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt8_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt8_rxoutclk_out : OUT std_logic;
		gt8_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt8_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt8_rxresetdone_out : OUT std_logic;
		gt8_gtxtxn_out : OUT std_logic;
		gt8_gtxtxp_out : OUT std_logic;
		gt8_txoutclk_out : OUT std_logic;
		gt8_txoutclkfabric_out : OUT std_logic;
		gt8_txoutclkpcs_out : OUT std_logic;
		gt8_txresetdone_out : OUT std_logic;
		gt9_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt9_drprdy_out : OUT std_logic;
		gt9_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt9_eyescandataerror_out : OUT std_logic;
		gt9_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt9_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt9_rxprbserr_out : OUT std_logic;
		gt9_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt9_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt9_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt9_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt9_rxoutclk_out : OUT std_logic;
		gt9_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt9_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt9_rxresetdone_out : OUT std_logic;
		gt9_gtxtxn_out : OUT std_logic;
		gt9_gtxtxp_out : OUT std_logic;
		gt9_txoutclk_out : OUT std_logic;
		gt9_txoutclkfabric_out : OUT std_logic;
		gt9_txoutclkpcs_out : OUT std_logic;
		gt9_txresetdone_out : OUT std_logic;
		gt10_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt10_drprdy_out : OUT std_logic;
		gt10_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt10_eyescandataerror_out : OUT std_logic;
		gt10_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt10_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt10_rxprbserr_out : OUT std_logic;
		gt10_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt10_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt10_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt10_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt10_rxoutclk_out : OUT std_logic;
		gt10_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt10_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt10_rxresetdone_out : OUT std_logic;
		gt10_gtxtxn_out : OUT std_logic;
		gt10_gtxtxp_out : OUT std_logic;
		gt10_txoutclk_out : OUT std_logic;
		gt10_txoutclkfabric_out : OUT std_logic;
		gt10_txoutclkpcs_out : OUT std_logic;
		gt10_txresetdone_out : OUT std_logic;
		gt11_drpdo_out : OUT std_logic_vector(15 downto 0);
		gt11_drprdy_out : OUT std_logic;
		gt11_dmonitorout_out : OUT std_logic_vector(7 downto 0);
		gt11_eyescandataerror_out : OUT std_logic;
		gt11_rxclkcorcnt_out : OUT std_logic_vector(1 downto 0);
		gt11_rxdata_out : OUT std_logic_vector(15 downto 0);
		gt11_rxprbserr_out : OUT std_logic;
		gt11_rxdisperr_out : OUT std_logic_vector(1 downto 0);
		gt11_rxnotintable_out : OUT std_logic_vector(1 downto 0);
		gt11_rxbufstatus_out : OUT std_logic_vector(2 downto 0);
		gt11_rxmonitorout_out : OUT std_logic_vector(6 downto 0);
		gt11_rxoutclk_out : OUT std_logic;
		gt11_rxchariscomma_out : OUT std_logic_vector(1 downto 0);
		gt11_rxcharisk_out : OUT std_logic_vector(1 downto 0);
		gt11_rxresetdone_out : OUT std_logic;
		gt11_gtxtxn_out : OUT std_logic;
		gt11_gtxtxp_out : OUT std_logic;
		gt11_txoutclk_out : OUT std_logic;
		gt11_txoutclkfabric_out : OUT std_logic;
		gt11_txoutclkpcs_out : OUT std_logic;
		gt11_txresetdone_out : OUT std_logic;
		GT0_QPLLRESET_OUT : OUT std_logic;
		GT1_QPLLRESET_OUT : OUT std_logic;
		GT2_QPLLRESET_OUT : OUT std_logic
		);
end component;
component amc_gtx5Gpd_common_reset  
generic
(
      STABLE_CLOCK_PERIOD      : integer := 8        -- Period of the stable clock driving this state-machine, unit is [ns]
   );
port
   (    
      STABLE_CLOCK             : in std_logic;             --Stable Clock, either a stable clock from the PCB
      SOFT_RESET               : in std_logic;               --User Reset, can be pulled any time
      COMMON_RESET             : out std_logic  --Reset QPLL
   );
end component;

component amc_gtx5Gpd_common 
generic
(
    -- Simulation attributes
    WRAPPER_SIM_GTRESET_SPEEDUP     : string     :=  "TRUE"        -- Set to "TRUE" to speed up sim reset 
);
port
(
    GTREFCLK0_IN : in std_logic;
    QPLLLOCK_OUT : out std_logic;
    QPLLLOCKDETCLK_IN : in std_logic;
    QPLLOUTCLK_OUT : out std_logic;
    QPLLOUTREFCLK_OUT : out std_logic;
    QPLLREFCLKLOST_OUT : out std_logic;    
    QPLLRESET_IN : in std_logic

);

end component;
signal qpll_lock_i : std_logic_vector(2 downto 0) := (others =>'0');
signal qpll_lock_n : std_logic_vector(2 downto 0) := (others =>'0');
signal COMMON_RESET : std_logic := '0';
signal GT0_QPLLRESET_OUT : std_logic := '0';
signal GT1_QPLLRESET_OUT : std_logic := '0';
signal GT2_QPLLRESET_OUT : std_logic := '0';
signal GT0_QPLLRESET_IN : std_logic := '0';
signal GT1_QPLLRESET_IN : std_logic := '0';
signal GT2_QPLLRESET_IN : std_logic := '0';
signal GT0_QPLLOUTCLK : std_logic := '0';
signal GT1_QPLLOUTCLK : std_logic := '0';
signal GT2_QPLLOUTCLK : std_logic := '0';
signal GT0_QPLLOUTREFCLK : std_logic := '0';
signal GT1_QPLLOUTREFCLK : std_logic := '0';
signal GT2_QPLLOUTREFCLK : std_logic := '0';
signal GT0_QPLLREFCLKLOST : std_logic := '0';
signal GT1_QPLLREFCLKLOST : std_logic := '0';
signal GT2_QPLLREFCLKLOST : std_logic := '0';
signal loopback : array12x3 := (others => (others => '0'));
signal AMC_pd : array12x2 := (others => (others => '0'));
signal gt0_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt1_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt2_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt3_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt4_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt5_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt6_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt7_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt8_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt9_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt10_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt11_rxbufstatus : std_logic_vector(2 downto 0) := (others =>'0');
signal gt0_rxoutclk : std_logic := '0';
signal gt1_rxoutclk : std_logic := '0';
signal gt2_rxoutclk : std_logic := '0';
signal gt3_rxoutclk : std_logic := '0';
signal gt4_rxoutclk : std_logic := '0';
signal gt5_rxoutclk : std_logic := '0';
signal gt6_rxoutclk : std_logic := '0';
signal gt7_rxoutclk : std_logic := '0';
signal gt8_rxoutclk : std_logic := '0';
signal gt9_rxoutclk : std_logic := '0';
signal gt10_rxoutclk : std_logic := '0';
signal gt11_rxoutclk : std_logic := '0';
signal gt0_rxoutclk_buf : std_logic := '0';
signal gt1_rxoutclk_buf : std_logic := '0';
signal gt2_rxoutclk_buf : std_logic := '0';
signal gt3_rxoutclk_buf : std_logic := '0';
signal gt4_rxoutclk_buf : std_logic := '0';
signal gt5_rxoutclk_buf : std_logic := '0';
signal gt6_rxoutclk_buf : std_logic := '0';
signal gt7_rxoutclk_buf : std_logic := '0';
signal gt8_rxoutclk_buf : std_logic := '0';
signal gt9_rxoutclk_buf : std_logic := '0';
signal gt10_rxoutclk_buf : std_logic := '0';
signal gt11_rxoutclk_buf : std_logic := '0';
signal RxCntr : array12x21 := (others => (others => '0'));
signal sampleRatioSync : array12x4 := (others => (others => '0'));
signal RXDATA_i : array12x16 := (others => (others => '0'));
signal RXNOTINTABLE_i : array12x2 := (others => (others => '0'));
signal rxchariscomma_i : array12x2 := (others => (others => '0'));
signal rxcharisk_i : array12x2 := (others => (others => '0'));
signal rxresetdone_i : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
begin
qpll_lock <= qpll_lock_i;
process(test, AMC_en)
begin
	for i in 0 to 11 loop
		if(test = '1' or AMC_en(i) = '0')then
			loopback(i) <= "001";
		else
			loopback(i) <= "000";
		end if;
		if(Dis_pd = '0' and (test = '1' or AMC_en(i) = '0'))then
			AMC_pd(i) <= "11";
		else
			AMC_pd(i) <= "00";
		end if;
	end loop;
end process;
process(UsrClk)
begin
	if(UsrClk'event and UsrClk = '1')then
		if(test = '1')then
			RXDATA <= (others => x"bcbc");
			RXNOTINTABLE <= (others => "00");
			rxchariscomma <= (others => "11");
			rxcharisk <= (others => "11");
			rxresetdone <= (others => '1');
		else
			RXDATA <= RXDATA_i;
			RXNOTINTABLE <= RXNOTINTABLE_i;
			rxchariscomma <= rxchariscomma_i;
			rxcharisk <= rxcharisk_i;
			rxresetdone <= rxresetdone_i;
		end if;
		if(gt0_rxbufstatus = "110")then
			RxBufOvf(0) <= '1';
		else
			RxBufOvf(0) <= '0';
		end if;
		if(gt0_rxbufstatus = "101")then
			RxBufUdf(0) <= '1';
		else
			RxBufUdf(0) <= '0';
		end if;
		if(gt1_rxbufstatus = "110")then
			RxBufOvf(1) <= '1';
		else
			RxBufOvf(1) <= '0';
		end if;
		if(gt1_rxbufstatus = "101")then
			RxBufUdf(1) <= '1';
		else
			RxBufUdf(1) <= '0';
		end if;
		if(gt2_rxbufstatus = "110")then
			RxBufOvf(2) <= '1';
		else
			RxBufOvf(2) <= '0';
		end if;
		if(gt2_rxbufstatus = "101")then
			RxBufUdf(2) <= '1';
		else
			RxBufUdf(2) <= '0';
		end if;
		if(gt3_rxbufstatus = "110")then
			RxBufOvf(3) <= '1';
		else
			RxBufOvf(3) <= '0';
		end if;
		if(gt3_rxbufstatus = "101")then
			RxBufUdf(3) <= '1';
		else
			RxBufUdf(3) <= '0';
		end if;
		if(gt4_rxbufstatus = "110")then
			RxBufOvf(4) <= '1';
		else
			RxBufOvf(4) <= '0';
		end if;
		if(gt4_rxbufstatus = "101")then
			RxBufUdf(4) <= '1';
		else
			RxBufUdf(4) <= '0';
		end if;
		if(gt5_rxbufstatus = "110")then
			RxBufOvf(5) <= '1';
		else
			RxBufOvf(5) <= '0';
		end if;
		if(gt5_rxbufstatus = "101")then
			RxBufUdf(5) <= '1';
		else
			RxBufUdf(5) <= '0';
		end if;
		if(gt6_rxbufstatus = "110")then
			RxBufOvf(6) <= '1';
		else
			RxBufOvf(6) <= '0';
		end if;
		if(gt6_rxbufstatus = "101")then
			RxBufUdf(6) <= '1';
		else
			RxBufUdf(6) <= '0';
		end if;
		if(gt7_rxbufstatus = "110")then
			RxBufOvf(7) <= '1';
		else
			RxBufOvf(7) <= '0';
		end if;
		if(gt7_rxbufstatus = "101")then
			RxBufUdf(7) <= '1';
		else
			RxBufUdf(7) <= '0';
		end if;
		if(gt8_rxbufstatus = "110")then
			RxBufOvf(8) <= '1';
		else
			RxBufOvf(8) <= '0';
		end if;
		if(gt8_rxbufstatus = "101")then
			RxBufUdf(8) <= '1';
		else
			RxBufUdf(8) <= '0';
		end if;
		if(gt9_rxbufstatus = "110")then
			RxBufOvf(9) <= '1';
		else
			RxBufOvf(9) <= '0';
		end if;
		if(gt9_rxbufstatus = "101")then
			RxBufUdf(9) <= '1';
		else
			RxBufUdf(9) <= '0';
		end if;
		if(gt10_rxbufstatus = "110")then
			RxBufOvf(10) <= '1';
		else
			RxBufOvf(10) <= '0';
		end if;
		if(gt10_rxbufstatus = "101")then
			RxBufUdf(10) <= '1';
		else
			RxBufUdf(10) <= '0';
		end if;
		if(gt11_rxbufstatus = "110")then
			RxBufOvf(11) <= '1';
		else
			RxBufOvf(11) <= '0';
		end if;
		if(gt11_rxbufstatus = "101")then
			RxBufUdf(11) <= '1';
		else
			RxBufUdf(11) <= '0';
		end if;
	end if;
end process;
process(gt0_rxoutclk_buf)
begin
	if(gt0_rxoutclk_buf'event and gt0_rxoutclk_buf = '1')then
		sampleRatioSync(0) <= sampleRatioSync(0)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(0)(3 downto 2) = "10")then
			RxCntr(0) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(0) <= RxCntr(0);
			end if;
		else
			RxCntr(0) <= RxCntr(0) + 1;
		end if;
	end if;
end process;
process(gt1_rxoutclk_buf)
begin
	if(gt1_rxoutclk_buf'event and gt1_rxoutclk_buf = '1')then
		sampleRatioSync(1) <= sampleRatioSync(1)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(1)(3 downto 2) = "10")then
			RxCntr(1) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(1) <= RxCntr(1);
			end if;
		else
			RxCntr(1) <= RxCntr(1) + 1;
		end if;
	end if;
end process;
process(gt2_rxoutclk_buf)
begin
	if(gt2_rxoutclk_buf'event and gt2_rxoutclk_buf = '1')then
		sampleRatioSync(2) <= sampleRatioSync(2)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(2)(3 downto 2) = "10")then
			RxCntr(2) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(2) <= RxCntr(2);
			end if;
		else
			RxCntr(2) <= RxCntr(2) + 1;
		end if;
	end if;
end process;
process(gt3_rxoutclk_buf)
begin
	if(gt3_rxoutclk_buf'event and gt3_rxoutclk_buf = '1')then
		sampleRatioSync(3) <= sampleRatioSync(3)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(3)(3 downto 2) = "10")then
			RxCntr(3) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(3) <= RxCntr(3);
			end if;
		else
			RxCntr(3) <= RxCntr(3) + 1;
		end if;
	end if;
end process;
process(gt4_rxoutclk_buf)
begin
	if(gt4_rxoutclk_buf'event and gt4_rxoutclk_buf = '1')then
		sampleRatioSync(4) <= sampleRatioSync(4)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(4)(3 downto 2) = "10")then
			RxCntr(4) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(4) <= RxCntr(4);
			end if;
		else
			RxCntr(4) <= RxCntr(4) + 1;
		end if;
	end if;
end process;
process(gt5_rxoutclk_buf)
begin
	if(gt5_rxoutclk_buf'event and gt5_rxoutclk_buf = '1')then
		sampleRatioSync(5) <= sampleRatioSync(5)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(5)(3 downto 2) = "10")then
			RxCntr(5) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(5) <= RxCntr(5);
			end if;
		else
			RxCntr(5) <= RxCntr(5) + 1;
		end if;
	end if;
end process;
process(gt6_rxoutclk_buf)
begin
	if(gt6_rxoutclk_buf'event and gt6_rxoutclk_buf = '1')then
		sampleRatioSync(6) <= sampleRatioSync(6)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(6)(3 downto 2) = "10")then
			RxCntr(6) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(6) <= RxCntr(6);
			end if;
		else
			RxCntr(6) <= RxCntr(6) + 1;
		end if;
	end if;
end process;
process(gt7_rxoutclk_buf)
begin
	if(gt7_rxoutclk_buf'event and gt7_rxoutclk_buf = '1')then
		sampleRatioSync(7) <= sampleRatioSync(7)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(7)(3 downto 2) = "10")then
			RxCntr(7) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(7) <= RxCntr(7);
			end if;
		else
			RxCntr(7) <= RxCntr(7) + 1;
		end if;
	end if;
end process;
process(gt8_rxoutclk_buf)
begin
	if(gt8_rxoutclk_buf'event and gt8_rxoutclk_buf = '1')then
		sampleRatioSync(8) <= sampleRatioSync(8)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(8)(3 downto 2) = "10")then
			RxCntr(8) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(8) <= RxCntr(8);
			end if;
		else
			RxCntr(8) <= RxCntr(8) + 1;
		end if;
	end if;
end process;
process(gt9_rxoutclk_buf)
begin
	if(gt9_rxoutclk_buf'event and gt9_rxoutclk_buf = '1')then
		sampleRatioSync(9) <= sampleRatioSync(9)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(9)(3 downto 2) = "10")then
			RxCntr(9) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(9) <= RxCntr(9);
			end if;
		else
			RxCntr(9) <= RxCntr(9) + 1;
		end if;
	end if;
end process;
process(gt10_rxoutclk_buf)
begin
	if(gt10_rxoutclk_buf'event and gt10_rxoutclk_buf = '1')then
		sampleRatioSync(10) <= sampleRatioSync(10)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(10)(3 downto 2) = "10")then
			RxCntr(10) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(10) <= RxCntr(10);
			end if;
		else
			RxCntr(10) <= RxCntr(10) + 1;
		end if;
	end if;
end process;
process(gt11_rxoutclk_buf)
begin
	if(gt11_rxoutclk_buf'event and gt11_rxoutclk_buf = '1')then
		sampleRatioSync(11) <= sampleRatioSync(11)(2 downto 0) & sampleRatio;
		if(sampleRatioSync(11)(3 downto 2) = "10")then
			RxCntr(11) <= (others => '0');
			if(updateRatio = '1')then
				RxClkRatio(11) <= RxCntr(11);
			end if;
		else
			RxCntr(11) <= RxCntr(11) + 1;
		end if;
	end if;
end process;
--qpll_lock_n <= not qpll_lock_i;
qpll_lock_n <= "000";
i_gt0_rxoutclk_buf : BUFH port map (O => gt0_rxoutclk_buf, I => gt0_rxoutclk);
i_gt1_rxoutclk_buf : BUFH port map (O => gt1_rxoutclk_buf, I => gt1_rxoutclk);
i_gt2_rxoutclk_buf : BUFH port map (O => gt2_rxoutclk_buf, I => gt2_rxoutclk);
i_gt3_rxoutclk_buf : BUFH port map (O => gt3_rxoutclk_buf, I => gt3_rxoutclk);
i_gt4_rxoutclk_buf : BUFH port map (O => gt4_rxoutclk_buf, I => gt4_rxoutclk);
i_gt5_rxoutclk_buf : BUFH port map (O => gt5_rxoutclk_buf, I => gt5_rxoutclk);
i_gt6_rxoutclk_buf : BUFH port map (O => gt6_rxoutclk_buf, I => gt6_rxoutclk);
i_gt7_rxoutclk_buf : BUFH port map (O => gt7_rxoutclk_buf, I => gt7_rxoutclk);
i_gt8_rxoutclk_buf : BUFH port map (O => gt8_rxoutclk_buf, I => gt8_rxoutclk);
i_gt9_rxoutclk_buf : BUFH port map (O => gt9_rxoutclk_buf, I => gt9_rxoutclk);
i_gt10_rxoutclk_buf : BUFH port map (O => gt10_rxoutclk_buf, I => gt10_rxoutclk);
i_gt11_rxoutclk_buf : BUFH port map (O => gt11_rxoutclk_buf, I => gt11_rxoutclk);
i_AMC_GTX5Gpd_init : amc_gtx5Gpd_init
port map
(
        SYSCLK_IN                       =>      DRPclk,
        SOFT_RESET_IN                   =>      SOFT_RESET,
				DONT_RESET_ON_DATA_ERROR_IN			=>			'0',
        GT0_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(0),
        GT0_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(0),
        GT0_DATA_VALID_IN               =>      data_valid(0),
        GT1_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(1),
        GT1_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(1),
        GT1_DATA_VALID_IN               =>      data_valid(1),
        GT2_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(2),
        GT2_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(2),
        GT2_DATA_VALID_IN               =>      data_valid(2),
        GT3_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(3),
        GT3_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(3),
        GT3_DATA_VALID_IN               =>      data_valid(3),
        GT4_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(4),
        GT4_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(4),
        GT4_DATA_VALID_IN               =>      data_valid(4),
        GT5_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(5),
        GT5_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(5),
        GT5_DATA_VALID_IN               =>      data_valid(5),
        GT6_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(6),
        GT6_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(6),
        GT6_DATA_VALID_IN               =>      data_valid(6),
        GT7_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(7),
        GT7_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(7),
        GT7_DATA_VALID_IN               =>      data_valid(7),
        GT8_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(8),
        GT8_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(8),
        GT8_DATA_VALID_IN               =>      data_valid(8),
        GT9_TX_FSM_RESET_DONE_OUT       =>      txfsmresetdone(9),
        GT9_RX_FSM_RESET_DONE_OUT       =>      rxfsmresetdone(9),
        GT9_DATA_VALID_IN               =>      data_valid(9),
        GT10_TX_FSM_RESET_DONE_OUT      =>      txfsmresetdone(10),
        GT10_RX_FSM_RESET_DONE_OUT      =>      rxfsmresetdone(10),
        GT10_DATA_VALID_IN              =>      data_valid(10),
        GT11_TX_FSM_RESET_DONE_OUT      =>      txfsmresetdone(11),
        GT11_RX_FSM_RESET_DONE_OUT      =>      rxfsmresetdone(11),
        GT11_DATA_VALID_IN              =>      data_valid(11),

				gt0_rxoutclk_out								=>			gt0_rxoutclk,
				gt0_rxbufstatus_out							=> 			gt0_rxbufstatus,
				gt1_rxoutclk_out								=>			gt1_rxoutclk,
				gt1_rxbufstatus_out							=> 			gt1_rxbufstatus,
				gt2_rxoutclk_out								=>			gt2_rxoutclk,
				gt2_rxbufstatus_out							=> 			gt2_rxbufstatus,
				gt3_rxoutclk_out								=>			gt3_rxoutclk,
				gt3_rxbufstatus_out							=> 			gt3_rxbufstatus,
				gt4_rxoutclk_out								=>			gt4_rxoutclk,
				gt4_rxbufstatus_out							=> 			gt4_rxbufstatus,
				gt5_rxoutclk_out								=>			gt5_rxoutclk,
				gt5_rxbufstatus_out							=> 			gt5_rxbufstatus,
				gt6_rxoutclk_out								=>			gt6_rxoutclk,
				gt6_rxbufstatus_out							=> 			gt6_rxbufstatus,
				gt7_rxoutclk_out								=>			gt7_rxoutclk,
				gt7_rxbufstatus_out							=> 			gt7_rxbufstatus,
				gt8_rxoutclk_out								=>			gt8_rxoutclk,
				gt8_rxbufstatus_out							=> 			gt8_rxbufstatus,
				gt9_rxoutclk_out								=>			gt9_rxoutclk,
				gt9_rxbufstatus_out							=> 			gt9_rxbufstatus,
				gt10_rxoutclk_out								=>			gt10_rxoutclk,
				gt10_rxbufstatus_out						=> 			gt10_rxbufstatus,
				gt11_rxoutclk_out								=>			gt11_rxoutclk,
				gt11_rxbufstatus_out						=> 			gt11_rxbufstatus,

    --_________________________________________________________________________
    --GT0  (X0Y0)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT0_DRPADDR_IN                  =>      (others => '0'),
        GT0_DRPCLK_IN                   =>      DRPclk,
        GT0_DRPDI_IN                    =>      (others => '0'),
        GT0_DRPDO_OUT                   =>      open,
        GT0_DRPEN_IN                    =>      '0',
        GT0_DRPRDY_OUT                  =>      open,
        GT0_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT0_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT0_LOOPBACK_IN                 =>      loopback(0),
    ------------------------------ Power-Down Ports ----------------------------
        GT0_RXPD_IN                     =>      AMC_pd(0),
        GT0_TXPD_IN                     =>      AMC_pd(0),
        --------------------- RX Initialization and Reset Ports --------------------
        GT0_EYESCANRESET_IN             =>      '0',
        GT0_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT0_EYESCANDATAERROR_OUT        =>      open,
        GT0_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT0_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT0_RXUSRCLK_IN                 =>      UsrClk,
        GT0_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT0_RXDATA_OUT                  =>      RXDATA_i(0),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT0_RXPRBSERR_OUT               =>      rxprbserr(0),
        GT0_RXPRBSSEL_IN                =>      rxprbssel(0),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT0_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT0_RXDISPERR_OUT               =>      open,
        GT0_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(0),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT0_GTXRXP_IN                   =>      RXP(0),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT0_GTXRXN_IN                   =>      RXN(0),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT0_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(0),
        GT0_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(0),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT0_RXDFELPMRESET_IN            =>      '0',
        GT0_RXMONITOROUT_OUT            =>      open,
        GT0_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT0_GTRXRESET_IN                =>      qpll_lock_n(0),
        GT0_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT0_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(0),
        GT0_RXCHARISK_OUT               =>      rxcharisk_i(0),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT0_RXRESETDONE_OUT             =>      rxresetdone_i(0),
        --------------------- TX Initialization and Reset Ports --------------------
        GT0_GTTXRESET_IN                =>      qpll_lock_n(0),
        GT0_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT0_TXUSRCLK_IN                 =>      UsrClk,
        GT0_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT0_TXDIFFCTRL_IN               =>      txdiffctrl(0),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT0_TXDATA_IN                   =>      TXDATA(0),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT0_GTXTXN_OUT                  =>      TXN(0),
        GT0_GTXTXP_OUT                  =>      TXP(0),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT0_TXOUTCLK_OUT                =>      txoutclk(0),
        GT0_TXOUTCLKFABRIC_OUT          =>      open,
        GT0_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT0_TXCHARISK_IN                =>      txcharisk(0),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT0_TXRESETDONE_OUT             =>      txresetdone(0),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT0_TXPRBSSEL_IN                =>      txprbssel(0),

    --GT1  (X0Y1)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT1_DRPADDR_IN                  =>      (others => '0'),
        GT1_DRPCLK_IN                   =>      DRPclk,
        GT1_DRPDI_IN                    =>      (others => '0'),
        GT1_DRPDO_OUT                   =>      open,
        GT1_DRPEN_IN                    =>      '0',
        GT1_DRPRDY_OUT                  =>      open,
        GT1_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT1_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT1_LOOPBACK_IN                 =>      loopback(1),
    ------------------------------ Power-Down Ports ----------------------------
        GT1_RXPD_IN                     =>      AMC_pd(1),
        GT1_TXPD_IN                     =>      AMC_pd(1),
        --------------------- RX Initialization and Reset Ports --------------------
        GT1_EYESCANRESET_IN             =>      '0',
        GT1_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT1_EYESCANDATAERROR_OUT        =>      open,
        GT1_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT1_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT1_RXUSRCLK_IN                 =>      UsrClk,
        GT1_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT1_RXDATA_OUT                  =>      RXDATA_i(1),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT1_RXPRBSERR_OUT               =>      rxprbserr(1),
        GT1_RXPRBSSEL_IN                =>      rxprbssel(1),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT1_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT1_RXDISPERR_OUT               =>      open,
        GT1_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(1),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT1_GTXRXP_IN                   =>      RXP(1),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT1_GTXRXN_IN                   =>      RXN(1),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT1_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(1),
        GT1_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(1),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT1_RXDFELPMRESET_IN            =>      '0',
        GT1_RXMONITOROUT_OUT            =>      open,
        GT1_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT1_GTRXRESET_IN                =>      qpll_lock_n(1),
        GT1_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT1_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(1),
        GT1_RXCHARISK_OUT               =>      rxcharisk_i(1),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT1_RXRESETDONE_OUT             =>      rxresetdone_i(1),
        --------------------- TX Initialization and Reset Ports --------------------
        GT1_GTTXRESET_IN                =>      qpll_lock_n(0),
        GT1_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT1_TXUSRCLK_IN                 =>      UsrClk,
        GT1_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT1_TXDIFFCTRL_IN               =>      txdiffctrl(1),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT1_TXDATA_IN                   =>      TXDATA(1),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT1_GTXTXN_OUT                  =>      TXN(1),
        GT1_GTXTXP_OUT                  =>      TXP(1),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT1_TXOUTCLK_OUT                =>      txoutclk(1),
        GT1_TXOUTCLKFABRIC_OUT          =>      open,
        GT1_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT1_TXCHARISK_IN                =>      txcharisk(1),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT1_TXRESETDONE_OUT             =>      txresetdone(1),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT1_TXPRBSSEL_IN                =>      txprbssel(1),

    --GT2  (X0Y2)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT2_DRPADDR_IN                  =>      (others => '0'),
        GT2_DRPCLK_IN                   =>      DRPclk,
        GT2_DRPDI_IN                    =>      (others => '0'),
        GT2_DRPDO_OUT                   =>      open,
        GT2_DRPEN_IN                    =>      '0',
        GT2_DRPRDY_OUT                  =>      open,
        GT2_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT2_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT2_LOOPBACK_IN                 =>      loopback(2),
    ------------------------------ Power-Down Ports ----------------------------
        GT2_RXPD_IN                     =>      AMC_pd(2),
        GT2_TXPD_IN                     =>      AMC_pd(2),
        --------------------- RX Initialization and Reset Ports --------------------
        GT2_EYESCANRESET_IN             =>      '0',
        GT2_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT2_EYESCANDATAERROR_OUT        =>      open,
        GT2_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT2_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT2_RXUSRCLK_IN                 =>      UsrClk,
        GT2_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT2_RXDATA_OUT                  =>      RXDATA_i(2),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT2_RXPRBSERR_OUT               =>      rxprbserr(2),
        GT2_RXPRBSSEL_IN                =>      rxprbssel(2),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT2_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT2_RXDISPERR_OUT               =>      open,
        GT2_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(2),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT2_GTXRXP_IN                   =>      RXP(2),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT2_GTXRXN_IN                   =>      RXN(2),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT2_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(2),
        GT2_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(2),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT2_RXDFELPMRESET_IN            =>      '0',
        GT2_RXMONITOROUT_OUT            =>      open,
        GT2_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT2_GTRXRESET_IN                =>      qpll_lock_n(0),
        GT2_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT2_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(2),
        GT2_RXCHARISK_OUT               =>      rxcharisk_i(2),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT2_RXRESETDONE_OUT             =>      rxresetdone_i(2),
        --------------------- TX Initialization and Reset Ports --------------------
        GT2_GTTXRESET_IN                =>      qpll_lock_n(2),
        GT2_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT2_TXUSRCLK_IN                 =>      UsrClk,
        GT2_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT2_TXDIFFCTRL_IN               =>      txdiffctrl(2),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT2_TXDATA_IN                   =>      TXDATA(2),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT2_GTXTXN_OUT                  =>      TXN(2),
        GT2_GTXTXP_OUT                  =>      TXP(2),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT2_TXOUTCLK_OUT                =>      txoutclk(2),
        GT2_TXOUTCLKFABRIC_OUT          =>      open,
        GT2_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT2_TXCHARISK_IN                =>      txcharisk(2),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT2_TXRESETDONE_OUT             =>      txresetdone(2),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT2_TXPRBSSEL_IN                =>      txprbssel(2),

    --GT3  (X0Y3)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT3_DRPADDR_IN                  =>      (others => '0'),
        GT3_DRPCLK_IN                   =>      DRPclk,
        GT3_DRPDI_IN                    =>      (others => '0'),
        GT3_DRPDO_OUT                   =>      open,
        GT3_DRPEN_IN                    =>      '0',
        GT3_DRPRDY_OUT                  =>      open,
        GT3_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT3_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT3_LOOPBACK_IN                 =>      loopback(3),
    ------------------------------ Power-Down Ports ----------------------------
        GT3_RXPD_IN                     =>      AMC_pd(3),
        GT3_TXPD_IN                     =>      AMC_pd(3),
        --------------------- RX Initialization and Reset Ports --------------------
        GT3_EYESCANRESET_IN             =>      '0',
        GT3_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT3_EYESCANDATAERROR_OUT        =>      open,
        GT3_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT3_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT3_RXUSRCLK_IN                 =>      UsrClk,
        GT3_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT3_RXDATA_OUT                  =>      RXDATA_i(3),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT3_RXPRBSERR_OUT               =>      rxprbserr(3),
        GT3_RXPRBSSEL_IN                =>      rxprbssel(3),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT3_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT3_RXDISPERR_OUT               =>      open,
        GT3_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(3),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT3_GTXRXP_IN                   =>      RXP(3),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT3_GTXRXN_IN                   =>      RXN(3),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT3_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(3),
        GT3_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(3),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT3_RXDFELPMRESET_IN            =>      '0',
        GT3_RXMONITOROUT_OUT            =>      open,
        GT3_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT3_GTRXRESET_IN                =>      qpll_lock_n(0),
        GT3_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT3_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(3),
        GT3_RXCHARISK_OUT               =>      rxcharisk_i(3),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT3_RXRESETDONE_OUT             =>      rxresetdone_i(3),
        --------------------- TX Initialization and Reset Ports --------------------
        GT3_GTTXRESET_IN                =>      qpll_lock_n(0),
        GT3_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT3_TXUSRCLK_IN                 =>      UsrClk,
        GT3_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT3_TXDIFFCTRL_IN               =>      txdiffctrl(3),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT3_TXDATA_IN                   =>      TXDATA(3),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT3_GTXTXN_OUT                  =>      TXN(3),
        GT3_GTXTXP_OUT                  =>      TXP(3),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT3_TXOUTCLK_OUT                =>      txoutclk(3),
        GT3_TXOUTCLKFABRIC_OUT          =>      open,
        GT3_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT3_TXCHARISK_IN                =>      txcharisk(3),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT3_TXRESETDONE_OUT             =>      txresetdone(3),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT3_TXPRBSSEL_IN                =>      txprbssel(3),

    --GT4  (X0Y4)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT4_DRPADDR_IN                  =>      (others => '0'),
        GT4_DRPCLK_IN                   =>      DRPclk,
        GT4_DRPDI_IN                    =>      (others => '0'),
        GT4_DRPDO_OUT                   =>      open,
        GT4_DRPEN_IN                    =>      '0',
        GT4_DRPRDY_OUT                  =>      open,
        GT4_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT4_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT4_LOOPBACK_IN                 =>      loopback(4),
    ------------------------------ Power-Down Ports ----------------------------
        GT4_RXPD_IN                     =>      AMC_pd(4),
        GT4_TXPD_IN                     =>      AMC_pd(4),
        --------------------- RX Initialization and Reset Ports --------------------
        GT4_EYESCANRESET_IN             =>      '0',
        GT4_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT4_EYESCANDATAERROR_OUT        =>      open,
        GT4_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT4_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT4_RXUSRCLK_IN                 =>      UsrClk,
        GT4_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT4_RXDATA_OUT                  =>      RXDATA_i(4),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT4_RXPRBSERR_OUT               =>      rxprbserr(4),
        GT4_RXPRBSSEL_IN                =>      rxprbssel(4),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT4_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT4_RXDISPERR_OUT               =>      open,
        GT4_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(4),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT4_GTXRXP_IN                   =>      RXP(4),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT4_GTXRXN_IN                   =>      RXN(4),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT4_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(4),
        GT4_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(4),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT4_RXDFELPMRESET_IN            =>      '0',
        GT4_RXMONITOROUT_OUT            =>      open,
        GT4_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT4_GTRXRESET_IN                =>      qpll_lock_n(1),
        GT4_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT4_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(4),
        GT4_RXCHARISK_OUT               =>      rxcharisk_i(4),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT4_RXRESETDONE_OUT             =>      rxresetdone_i(4),
        --------------------- TX Initialization and Reset Ports --------------------
        GT4_GTTXRESET_IN                =>      qpll_lock_n(1),
        GT4_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT4_TXUSRCLK_IN                 =>      UsrClk,
        GT4_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT4_TXDIFFCTRL_IN               =>      txdiffctrl(4),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT4_TXDATA_IN                   =>      TXDATA(4),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT4_GTXTXN_OUT                  =>      TXN(4),
        GT4_GTXTXP_OUT                  =>      TXP(4),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT4_TXOUTCLK_OUT                =>      txoutclk(4),
        GT4_TXOUTCLKFABRIC_OUT          =>      open,
        GT4_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT4_TXCHARISK_IN                =>      txcharisk(4),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT4_TXRESETDONE_OUT             =>      txresetdone(4),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT4_TXPRBSSEL_IN                =>      txprbssel(4),

    --GT5  (X0Y5)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT5_DRPADDR_IN                  =>      (others => '0'),
        GT5_DRPCLK_IN                   =>      DRPclk,
        GT5_DRPDI_IN                    =>      (others => '0'),
        GT5_DRPDO_OUT                   =>      open,
        GT5_DRPEN_IN                    =>      '0',
        GT5_DRPRDY_OUT                  =>      open,
        GT5_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT5_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT5_LOOPBACK_IN                 =>      loopback(5),
    ------------------------------ Power-Down Ports ----------------------------
        GT5_RXPD_IN                     =>      AMC_pd(5),
        GT5_TXPD_IN                     =>      AMC_pd(5),
        --------------------- RX Initialization and Reset Ports --------------------
        GT5_EYESCANRESET_IN             =>      '0',
        GT5_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT5_EYESCANDATAERROR_OUT        =>      open,
        GT5_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT5_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT5_RXUSRCLK_IN                 =>      UsrClk,
        GT5_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT5_RXDATA_OUT                  =>      RXDATA_i(5),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT5_RXPRBSERR_OUT               =>      rxprbserr(5),
        GT5_RXPRBSSEL_IN                =>      rxprbssel(5),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT5_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT5_RXDISPERR_OUT               =>      open,
        GT5_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(5),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT5_GTXRXP_IN                   =>      RXP(5),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT5_GTXRXN_IN                   =>      RXN(5),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT5_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(5),
        GT5_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(5),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT5_RXDFELPMRESET_IN            =>      '0',
        GT5_RXMONITOROUT_OUT            =>      open,
        GT5_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT5_GTRXRESET_IN                =>      qpll_lock_n(1),
        GT5_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT5_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(5),
        GT5_RXCHARISK_OUT               =>      rxcharisk_i(5),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT5_RXRESETDONE_OUT             =>      rxresetdone_i(5),
        --------------------- TX Initialization and Reset Ports --------------------
        GT5_GTTXRESET_IN                =>      qpll_lock_n(1),
        GT5_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT5_TXUSRCLK_IN                 =>      UsrClk,
        GT5_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT5_TXDIFFCTRL_IN               =>      txdiffctrl(5),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT5_TXDATA_IN                   =>      TXDATA(5),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT5_GTXTXN_OUT                  =>      TXN(5),
        GT5_GTXTXP_OUT                  =>      TXP(5),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT5_TXOUTCLK_OUT                =>      txoutclk(5),
        GT5_TXOUTCLKFABRIC_OUT          =>      open,
        GT5_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT5_TXCHARISK_IN                =>      txcharisk(5),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT5_TXRESETDONE_OUT             =>      txresetdone(5),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT5_TXPRBSSEL_IN                =>      txprbssel(5),

    --GT6  (X0Y6)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT6_DRPADDR_IN                  =>      (others => '0'),
        GT6_DRPCLK_IN                   =>      DRPclk,
        GT6_DRPDI_IN                    =>      (others => '0'),
        GT6_DRPDO_OUT                   =>      open,
        GT6_DRPEN_IN                    =>      '0',
        GT6_DRPRDY_OUT                  =>      open,
        GT6_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT6_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT6_LOOPBACK_IN                 =>      loopback(6),
    ------------------------------ Power-Down Ports ----------------------------
        GT6_RXPD_IN                     =>      AMC_pd(6),
        GT6_TXPD_IN                     =>      AMC_pd(6),
        --------------------- RX Initialization and Reset Ports --------------------
        GT6_EYESCANRESET_IN             =>      '0',
        GT6_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT6_EYESCANDATAERROR_OUT        =>      open,
        GT6_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT6_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT6_RXUSRCLK_IN                 =>      UsrClk,
        GT6_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT6_RXDATA_OUT                  =>      RXDATA_i(6),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT6_RXPRBSERR_OUT               =>      rxprbserr(6),
        GT6_RXPRBSSEL_IN                =>      rxprbssel(6),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT6_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT6_RXDISPERR_OUT               =>      open,
        GT6_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(6),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT6_GTXRXP_IN                   =>      RXP(6),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT6_GTXRXN_IN                   =>      RXN(6),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT6_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(6),
        GT6_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(6),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT6_RXDFELPMRESET_IN            =>      '0',
        GT6_RXMONITOROUT_OUT            =>      open,
        GT6_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT6_GTRXRESET_IN                =>      qpll_lock_n(1),
        GT6_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT6_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(6),
        GT6_RXCHARISK_OUT               =>      rxcharisk_i(6),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT6_RXRESETDONE_OUT             =>      rxresetdone_i(6),
        --------------------- TX Initialization and Reset Ports --------------------
        GT6_GTTXRESET_IN                =>      qpll_lock_n(1),
        GT6_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT6_TXUSRCLK_IN                 =>      UsrClk,
        GT6_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT6_TXDIFFCTRL_IN               =>      txdiffctrl(6),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT6_TXDATA_IN                   =>      TXDATA(6),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT6_GTXTXN_OUT                  =>      TXN(6),
        GT6_GTXTXP_OUT                  =>      TXP(6),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT6_TXOUTCLK_OUT                =>      txoutclk(6),
        GT6_TXOUTCLKFABRIC_OUT          =>      open,
        GT6_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT6_TXCHARISK_IN                =>      txcharisk(6),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT6_TXRESETDONE_OUT             =>      txresetdone(6),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT6_TXPRBSSEL_IN                =>      txprbssel(6),

    --GT7  (X0Y7)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT7_DRPADDR_IN                  =>      (others => '0'),
        GT7_DRPCLK_IN                   =>      DRPclk,
        GT7_DRPDI_IN                    =>      (others => '0'),
        GT7_DRPDO_OUT                   =>      open,
        GT7_DRPEN_IN                    =>      '0',
        GT7_DRPRDY_OUT                  =>      open,
        GT7_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT7_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT7_LOOPBACK_IN                 =>      loopback(7),
    ------------------------------ Power-Down Ports ----------------------------
        GT7_RXPD_IN                     =>      AMC_pd(7),
        GT7_TXPD_IN                     =>      AMC_pd(7),
        --------------------- RX Initialization and Reset Ports --------------------
        GT7_EYESCANRESET_IN             =>      '0',
        GT7_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT7_EYESCANDATAERROR_OUT        =>      open,
        GT7_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT7_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT7_RXUSRCLK_IN                 =>      UsrClk,
        GT7_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT7_RXDATA_OUT                  =>      RXDATA_i(7),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT7_RXPRBSERR_OUT               =>      rxprbserr(7),
        GT7_RXPRBSSEL_IN                =>      rxprbssel(7),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT7_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT7_RXDISPERR_OUT               =>      open,
        GT7_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(7),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT7_GTXRXP_IN                   =>      RXP(7),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT7_GTXRXN_IN                   =>      RXN(7),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT7_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(7),
        GT7_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(7),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT7_RXDFELPMRESET_IN            =>      '0',
        GT7_RXMONITOROUT_OUT            =>      open,
        GT7_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT7_GTRXRESET_IN                =>      qpll_lock_n(1),
        GT7_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT7_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(7),
        GT7_RXCHARISK_OUT               =>      rxcharisk_i(7),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT7_RXRESETDONE_OUT             =>      rxresetdone_i(7),
        --------------------- TX Initialization and Reset Ports --------------------
        GT7_GTTXRESET_IN                =>      qpll_lock_n(1),
        GT7_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT7_TXUSRCLK_IN                 =>      UsrClk,
        GT7_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT7_TXDIFFCTRL_IN               =>      txdiffctrl(7),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT7_TXDATA_IN                   =>      TXDATA(7),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT7_GTXTXN_OUT                  =>      TXN(7),
        GT7_GTXTXP_OUT                  =>      TXP(7),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT7_TXOUTCLK_OUT                =>      txoutclk(7),
        GT7_TXOUTCLKFABRIC_OUT          =>      open,
        GT7_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT7_TXCHARISK_IN                =>      txcharisk(7),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT7_TXRESETDONE_OUT             =>      txresetdone(7),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT7_TXPRBSSEL_IN                =>      txprbssel(7),

    --GT8  (X0Y8)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT8_DRPADDR_IN                  =>      (others => '0'),
        GT8_DRPCLK_IN                   =>      DRPclk,
        GT8_DRPDI_IN                    =>      (others => '0'),
        GT8_DRPDO_OUT                   =>      open,
        GT8_DRPEN_IN                    =>      '0',
        GT8_DRPRDY_OUT                  =>      open,
        GT8_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT8_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT8_LOOPBACK_IN                 =>      loopback(8),
    ------------------------------ Power-Down Ports ----------------------------
        GT8_RXPD_IN                     =>      AMC_pd(8),
        GT8_TXPD_IN                     =>      AMC_pd(8),
        --------------------- RX Initialization and Reset Ports --------------------
        GT8_EYESCANRESET_IN             =>      '0',
        GT8_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT8_EYESCANDATAERROR_OUT        =>      open,
        GT8_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT8_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT8_RXUSRCLK_IN                 =>      UsrClk,
        GT8_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT8_RXDATA_OUT                  =>      RXDATA_i(8),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT8_RXPRBSERR_OUT               =>      rxprbserr(8),
        GT8_RXPRBSSEL_IN                =>      rxprbssel(8),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT8_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT8_RXDISPERR_OUT               =>      open,
        GT8_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(8),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT8_GTXRXP_IN                   =>      RXP(8),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT8_GTXRXN_IN                   =>      RXN(8),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT8_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(8),
        GT8_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(8),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT8_RXDFELPMRESET_IN            =>      '0',
        GT8_RXMONITOROUT_OUT            =>      open,
        GT8_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT8_GTRXRESET_IN                =>      qpll_lock_n(2),
        GT8_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT8_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(8),
        GT8_RXCHARISK_OUT               =>      rxcharisk_i(8),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT8_RXRESETDONE_OUT             =>      rxresetdone_i(8),
        --------------------- TX Initialization and Reset Ports --------------------
        GT8_GTTXRESET_IN                =>      qpll_lock_n(2),
        GT8_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT8_TXUSRCLK_IN                 =>      UsrClk,
        GT8_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT8_TXDIFFCTRL_IN               =>      txdiffctrl(8),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT8_TXDATA_IN                   =>      TXDATA(8),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT8_GTXTXN_OUT                  =>      TXN(8),
        GT8_GTXTXP_OUT                  =>      TXP(8),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT8_TXOUTCLK_OUT                =>      txoutclk(8),
        GT8_TXOUTCLKFABRIC_OUT          =>      open,
        GT8_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT8_TXCHARISK_IN                =>      txcharisk(8),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT8_TXRESETDONE_OUT             =>      txresetdone(8),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT8_TXPRBSSEL_IN                =>      txprbssel(8),

    --GT9  (X0Y9)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT9_DRPADDR_IN                  =>      (others => '0'),
        GT9_DRPCLK_IN                   =>      DRPclk,
        GT9_DRPDI_IN                    =>      (others => '0'),
        GT9_DRPDO_OUT                   =>      open,
        GT9_DRPEN_IN                    =>      '0',
        GT9_DRPRDY_OUT                  =>      open,
        GT9_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT9_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT9_LOOPBACK_IN                 =>      loopback(9),
    ------------------------------ Power-Down Ports ----------------------------
        GT9_RXPD_IN                     =>      AMC_pd(9),
        GT9_TXPD_IN                     =>      AMC_pd(9),
        --------------------- RX Initialization and Reset Ports --------------------
        GT9_EYESCANRESET_IN             =>      '0',
        GT9_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT9_EYESCANDATAERROR_OUT        =>      open,
        GT9_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT9_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT9_RXUSRCLK_IN                 =>      UsrClk,
        GT9_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT9_RXDATA_OUT                  =>      RXDATA_i(9),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT9_RXPRBSERR_OUT               =>      rxprbserr(9),
        GT9_RXPRBSSEL_IN                =>      rxprbssel(9),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT9_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT9_RXDISPERR_OUT               =>      open,
        GT9_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(9),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT9_GTXRXP_IN                   =>      RXP(9),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT9_GTXRXN_IN                   =>      RXN(9),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT9_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(9),
        GT9_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(9),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT9_RXDFELPMRESET_IN            =>      '0',
        GT9_RXMONITOROUT_OUT            =>      open,
        GT9_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT9_GTRXRESET_IN                =>      qpll_lock_n(2),
        GT9_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT9_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(9),
        GT9_RXCHARISK_OUT               =>      rxcharisk_i(9),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT9_RXRESETDONE_OUT             =>      rxresetdone_i(9),
        --------------------- TX Initialization and Reset Ports --------------------
        GT9_GTTXRESET_IN                =>      qpll_lock_n(2),
        GT9_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT9_TXUSRCLK_IN                 =>      UsrClk,
        GT9_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT9_TXDIFFCTRL_IN               =>      txdiffctrl(9),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT9_TXDATA_IN                   =>      TXDATA(9),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT9_GTXTXN_OUT                  =>      TXN(9),
        GT9_GTXTXP_OUT                  =>      TXP(9),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT9_TXOUTCLK_OUT                =>      txoutclk(9),
        GT9_TXOUTCLKFABRIC_OUT          =>      open,
        GT9_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT9_TXCHARISK_IN                =>      txcharisk(9),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT9_TXRESETDONE_OUT             =>      txresetdone(9),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT9_TXPRBSSEL_IN                =>      txprbssel(9),

    --GT10  (X0Y10)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT10_DRPADDR_IN                  =>      (others => '0'),
        GT10_DRPCLK_IN                   =>      DRPclk,
        GT10_DRPDI_IN                    =>      (others => '0'),
        GT10_DRPDO_OUT                   =>      open,
        GT10_DRPEN_IN                    =>      '0',
        GT10_DRPRDY_OUT                  =>      open,
        GT10_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT10_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT10_LOOPBACK_IN                 =>      loopback(10),
    ------------------------------ Power-Down Ports ----------------------------
        GT10_RXPD_IN                     =>      AMC_pd(10),
        GT10_TXPD_IN                     =>      AMC_pd(10),
        --------------------- RX Initialization and Reset Ports --------------------
        GT10_EYESCANRESET_IN             =>      '0',
        GT10_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT10_EYESCANDATAERROR_OUT        =>      open,
        GT10_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT10_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT10_RXUSRCLK_IN                 =>      UsrClk,
        GT10_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT10_RXDATA_OUT                  =>      RXDATA_i(10),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT10_RXPRBSERR_OUT               =>      rxprbserr(10),
        GT10_RXPRBSSEL_IN                =>      rxprbssel(10),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT10_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT10_RXDISPERR_OUT               =>      open,
        GT10_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(10),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT10_GTXRXP_IN                   =>      RXP(10),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT10_GTXRXN_IN                   =>      RXN(10),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT10_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(10),
        GT10_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(10),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT10_RXDFELPMRESET_IN            =>      '0',
        GT10_RXMONITOROUT_OUT            =>      open,
        GT10_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT10_GTRXRESET_IN                =>      qpll_lock_n(2),
        GT10_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT10_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(10),
        GT10_RXCHARISK_OUT               =>      rxcharisk_i(10),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT10_RXRESETDONE_OUT             =>      rxresetdone_i(10),
        --------------------- TX Initialization and Reset Ports --------------------
        GT10_GTTXRESET_IN                =>      qpll_lock_n(2),
        GT10_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT10_TXUSRCLK_IN                 =>      UsrClk,
        GT10_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT10_TXDIFFCTRL_IN               =>      txdiffctrl(10),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT10_TXDATA_IN                   =>      TXDATA(10),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT10_GTXTXN_OUT                  =>      TXN(10),
        GT10_GTXTXP_OUT                  =>      TXP(10),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT10_TXOUTCLK_OUT                =>      txoutclk(10),
        GT10_TXOUTCLKFABRIC_OUT          =>      open,
        GT10_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT10_TXCHARISK_IN                =>      txcharisk(10),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT10_TXRESETDONE_OUT             =>      txresetdone(10),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT10_TXPRBSSEL_IN                =>      txprbssel(10),

    --GT11  (X0Y11)
    --____________________________CHANNEL PORTS________________________________
    ---------------------------- Channel - DRP Ports  --------------------------
        GT11_DRPADDR_IN                  =>      (others => '0'),
        GT11_DRPCLK_IN                   =>      DRPclk,
        GT11_DRPDI_IN                    =>      (others => '0'),
        GT11_DRPDO_OUT                   =>      open,
        GT11_DRPEN_IN                    =>      '0',
        GT11_DRPRDY_OUT                  =>      open,
        GT11_DRPWE_IN                    =>      '0',
    --------------------------- Digital Monitor Ports --------------------------
        GT11_DMONITOROUT_OUT             =>      open,
        ------------------------------- Loopback Ports -----------------------------
        GT11_LOOPBACK_IN                 =>      loopback(11),
    ------------------------------ Power-Down Ports ----------------------------
        GT11_RXPD_IN                     =>      AMC_pd(11),
        GT11_TXPD_IN                     =>      AMC_pd(11),
        --------------------- RX Initialization and Reset Ports --------------------
        GT11_EYESCANRESET_IN             =>      '0',
        GT11_RXUSERRDY_IN                =>      '0',
        -------------------------- RX Margin Analysis Ports ------------------------
        GT11_EYESCANDATAERROR_OUT        =>      open,
        GT11_EYESCANTRIGGER_IN             =>      '0',
        ------------------- Receive Ports - Clock Correction Ports -----------------
        GT11_RXCLKCORCNT_OUT             =>      open,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        GT11_RXUSRCLK_IN                 =>      UsrClk,
        GT11_RXUSRCLK2_IN                =>      UsrClk,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        GT11_RXDATA_OUT                  =>      RXDATA_i(11),
        ------------------- Receive Ports - Pattern Checker Ports ------------------
        GT11_RXPRBSERR_OUT               =>      rxprbserr(11),
        GT11_RXPRBSSEL_IN                =>      rxprbssel(11),
        ------------------- Receive Ports - Pattern Checker ports ------------------
        GT11_RXPRBSCNTRESET_IN           =>      '0',
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        GT11_RXDISPERR_OUT               =>      open,
        GT11_RXNOTINTABLE_OUT            =>      RXNOTINTABLE_i(11),
        --------------------------- Receive Ports - RX AFE -------------------------
        GT11_GTXRXP_IN                   =>      RXP(11),
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        GT11_GTXRXN_IN                   =>      RXN(11),
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        GT11_RXMCOMMAALIGNEN_IN          =>      rxcommaalignen(11),
        GT11_RXPCOMMAALIGNEN_IN          =>      rxcommaalignen(11),
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        GT11_RXDFELPMRESET_IN            =>      '0',
        GT11_RXMONITOROUT_OUT            =>      open,
        GT11_RXMONITORSEL_IN             =>      "00",
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        GT11_GTRXRESET_IN                =>      qpll_lock_n(2),
        GT11_RXPMARESET_IN               =>      '0',
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        GT11_RXCHARISCOMMA_OUT           =>      rxchariscomma_i(11),
        GT11_RXCHARISK_OUT               =>      rxcharisk_i(11),
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        GT11_RXRESETDONE_OUT             =>      rxresetdone_i(11),
        --------------------- TX Initialization and Reset Ports --------------------
        GT11_GTTXRESET_IN                =>      qpll_lock_n(2),
        GT11_TXUSERRDY_IN                =>      '0',
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        GT11_TXUSRCLK_IN                 =>      UsrClk,
        GT11_TXUSRCLK2_IN                =>      UsrClk,
        --------------- Transmit Ports - TX Configurable Driver Ports --------------
        GT11_TXDIFFCTRL_IN               =>      txdiffctrl(11),
        ------------------ Transmit Ports - TX Data Path interface -----------------
        GT11_TXDATA_IN                   =>      TXDATA(11),
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        GT11_GTXTXN_OUT                  =>      TXN(11),
        GT11_GTXTXP_OUT                  =>      TXP(11),
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        GT11_TXOUTCLK_OUT                =>      txoutclk(11),
        GT11_TXOUTCLKFABRIC_OUT          =>      open,
        GT11_TXOUTCLKPCS_OUT             =>      open,
        --------------------- Transmit Ports - TX Gearbox Ports --------------------
        GT11_TXCHARISK_IN                =>      txcharisk(11),
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        GT11_TXRESETDONE_OUT             =>      txresetdone(11),
        ------------------ Transmit Ports - pattern Generator Ports ----------------
        GT11_TXPRBSSEL_IN                =>      txprbssel(11),


    --____________________________COMMON PORTS________________________________
				GT0_QPLLLOCK_IN => qpll_lock_i(0), 
				GT0_QPLLREFCLKLOST_IN => GT0_QPLLREFCLKLOST, 
				GT0_QPLLRESET_OUT => GT0_QPLLRESET_OUT, 
				GT0_QPLLOUTCLK_IN  => GT0_QPLLOUTCLK,
				GT0_QPLLOUTREFCLK_IN => GT0_QPLLOUTREFCLK,
    --____________________________COMMON PORTS________________________________
				GT1_QPLLLOCK_IN => qpll_lock_i(1), 
				GT1_QPLLREFCLKLOST_IN => GT1_QPLLREFCLKLOST, 
				GT1_QPLLRESET_OUT => GT1_QPLLRESET_OUT, 
				GT1_QPLLOUTCLK_IN  => GT1_QPLLOUTCLK,
				GT1_QPLLOUTREFCLK_IN => GT1_QPLLOUTREFCLK,
    --____________________________COMMON PORTS________________________________
				GT2_QPLLLOCK_IN => qpll_lock_i(2), 
				GT2_QPLLREFCLKLOST_IN => GT2_QPLLREFCLKLOST, 
				GT2_QPLLRESET_OUT => GT2_QPLLRESET_OUT, 
				GT2_QPLLOUTCLK_IN  => GT2_QPLLOUTCLK,
				GT2_QPLLOUTREFCLK_IN => GT2_QPLLOUTREFCLK

);
i_common0 : amc_gtx5Gpd_common 
 port map
   (
    GTREFCLK0_IN => AMC_REFCLK,
    QPLLLOCK_OUT => qpll_lock_i(0),
    QPLLLOCKDETCLK_IN => DRPclk,
    QPLLOUTCLK_OUT => GT0_QPLLOUTCLK,
    QPLLOUTREFCLK_OUT => GT0_QPLLOUTREFCLK,
    QPLLREFCLKLOST_OUT => GT0_QPLLREFCLKLOST,    
    QPLLRESET_IN => GT0_QPLLRESET_IN

);
GT0_QPLLRESET_IN <= GT0_QPLLRESET_OUT or COMMON_RESET;
i_common1 : amc_gtx5Gpd_common 
 port map
   (
    GTREFCLK0_IN => AMC_REFCLK,
    QPLLLOCK_OUT => qpll_lock_i(1),
    QPLLLOCKDETCLK_IN => DRPclk,
    QPLLOUTCLK_OUT => GT1_QPLLOUTCLK,
    QPLLOUTREFCLK_OUT => GT1_QPLLOUTREFCLK,
    QPLLREFCLKLOST_OUT => GT1_QPLLREFCLKLOST,    
    QPLLRESET_IN => GT1_QPLLRESET_IN

);
GT1_QPLLRESET_IN <= GT1_QPLLRESET_OUT or COMMON_RESET;
i_common2 : amc_gtx5Gpd_common 
 port map
   (
    GTREFCLK0_IN => AMC_REFCLK,
    QPLLLOCK_OUT => qpll_lock_i(2),
    QPLLLOCKDETCLK_IN => DRPclk,
    QPLLOUTCLK_OUT => GT2_QPLLOUTCLK,
    QPLLOUTREFCLK_OUT => GT2_QPLLOUTREFCLK,
    QPLLREFCLKLOST_OUT => GT2_QPLLREFCLKLOST,    
    QPLLRESET_IN => GT2_QPLLRESET_IN

);
GT2_QPLLRESET_IN <= GT2_QPLLRESET_OUT or COMMON_RESET;

i_common_reset : amc_gtx5Gpd_common_reset 
   port map
   (    
      STABLE_CLOCK => DRPclk,             --Stable Clock, either a stable clock from the PCB
      SOFT_RESET => SOFT_RESET,               --User Reset, can be pulled any time
      COMMON_RESET => COMMON_RESET              --Reset QPLL
   );
end Behavioral;

