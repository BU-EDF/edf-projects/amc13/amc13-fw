library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
entity HammingDecode is
    Port ( clk : in std_logic;
           din_valid : in std_logic;
           din : in std_logic_vector(23 downto 0);
           dout_valid : out std_logic;
           dout : out std_logic_vector(17 downto 0);
					 sgl_err : out std_logic;
					 dbl_err : out std_logic
           );
end HammingDecode;

architecture my_arch of HammingDecode is
signal c: std_logic_vector(6 downto 1) := (others =>'0');
signal din_q: std_logic_vector(17 downto 0) := (others =>'0');
signal din_valid_q : std_logic := '0';
begin
process(clk)
begin
  if(clk'event and clk = '1')then
		c(1) <= din(0) xor din(1) xor din(3) xor din(4) xor din(6) xor din(8) xor
						 din(10) xor din(11) xor din(13) xor din(15) xor din(17) xor din(18);
		c(2) <= din(0) xor din(2) xor din(3) xor din(5) xor din(6) xor din(9) xor
						 din(10) xor din(12) xor din(13) xor din(16) xor din(17) xor din(19);
		c(3) <= din(1) xor din(2) xor din(3) xor din(7) xor din(8) xor din(9) xor din(10) xor
						 din(14) xor din(15) xor din(16) xor din(17) xor din(20);
		c(4) <= din(4) xor din(5) xor din(6) xor din(7) xor din(8) xor din(9) xor din(10) xor din(21);
		c(5) <= din(11) xor din(12) xor din(13) xor din(14) xor din(15) xor din(16) xor din(17) xor din(22);
		c(6) <= not(din(2) xor din(5) xor din(7) xor din(9) xor din(12) xor din(14) xor din(16) xor din(19) xor din(20) xor din(21) xor din(22) xor din(23));
		sgl_err <= (c(1) xor c(6)) and din_valid_q;
		dbl_err <= (c(1) xnor c(6)) and or_reduce(c(5 downto 1)) and din_valid_q;
		din_valid_q <= din_valid;
		din_q <= din(17 downto 0);
--		dout_valid <= din_valid_q;
		dout_valid <= not((c(1) xnor c(6)) and or_reduce(c(5 downto 1))) and din_valid_q;
		if(c = "000011")then
			dout(0) <= not din_q(0);
		else
			dout(0) <= din_q(0);
		end if;
		if(c = "000101")then
			dout(1) <= not din_q(1);
		else
			dout(1) <= din_q(1);
		end if;
		if(c = "000110")then
			dout(2) <= not din_q(2);
		else
			dout(2) <= din_q(2);
		end if;
		if(c = "000111")then
			dout(3) <= not din_q(3);
		else
			dout(3) <= din_q(3);
		end if;
		if(c = "001001")then
			dout(4) <= not din_q(4);
		else
			dout(4) <= din_q(4);
		end if;
		if(c = "001010")then
			dout(5) <= not din_q(5);
		else
			dout(5) <= din_q(5);
		end if;
		if(c = "001011")then
			dout(6) <= not din_q(6);
		else
			dout(6) <= din_q(6);
		end if;
		if(c = "001100")then
			dout(7) <= not din_q(7);
		else
			dout(7) <= din_q(7);
		end if;
		if(c = "001101")then
			dout(8) <= not din_q(8);
		else
			dout(8) <= din_q(8);
		end if;
		if(c = "001110")then
			dout(9) <= not din_q(9);
		else
			dout(9) <= din_q(9);
		end if;
		if(c = "001111")then
			dout(10) <= not din_q(10);
		else
			dout(10) <= din_q(10);
		end if;
		if(c = "010001")then
			dout(11) <= not din_q(11);
		else
			dout(11) <= din_q(11);
		end if;
		if(c = "010010")then
			dout(12) <= not din_q(12);
		else
			dout(12) <= din_q(12);
		end if;
		if(c = "010011")then
			dout(13) <= not din_q(13);
		else
			dout(13) <= din_q(13);
		end if;
		if(c = "010100")then
			dout(14) <= not din_q(14);
		else
			dout(14) <= din_q(14);
		end if;
		if(c = "010101")then
			dout(15) <= not din_q(15);
		else
			dout(15) <= din_q(15);
		end if;
		if(c = "010110")then
			dout(16) <= not din_q(16);
		else
			dout(16) <= din_q(16);
		end if;
		if(c = "010111")then
			dout(17) <= not din_q(17);
		else
			dout(17) <= din_q(17);
		end if;
  end if;
end process;
end my_arch;
