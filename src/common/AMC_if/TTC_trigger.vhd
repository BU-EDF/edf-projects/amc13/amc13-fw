----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:29:47 08/28/2015 
-- Design Name: 
-- Module Name:    TTC_trigger - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TTC_trigger is
	generic(simulation : boolean := false);
    Port ( reset : in  STD_LOGIC; -- async reset
           UsrClk : in  STD_LOGIC;
           TTCclk : in  STD_LOGIC;
           HammingData_in : in  STD_LOGIC_VECTOR (17 downto 0);
           HammingDataValid : in  STD_LOGIC;
           BC0 : in  STD_LOGIC;
           BcntMm : out  STD_LOGIC;
           TTC_lock : out  STD_LOGIC;
           BC0_lock : out  STD_LOGIC;
           TrigData : out  STD_LOGIC_VECTOR (7 downto 0));
end TTC_trigger;

architecture Behavioral of TTC_trigger is
signal TTC_lock_i : std_logic := '0';
signal BC0_lock_i : std_logic := '0';
signal Toggle : std_logic := '0';
signal ToggleSyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal TTC_edge : std_logic_vector(6 downto 0) := (others => '0');
signal HammingDataValid_dl3 : std_logic := '0';
signal chk_HammingData : std_logic := '0';
signal OutStrobe : std_logic := '0';
signal rd_enable : std_logic := '0';
signal rd_TTC_data : std_logic := '0';
signal BC0_l : std_logic := '0';
signal BcntMm_i : std_logic := '0';
signal BC0_matchCntr : std_logic_vector(8 downto 0) := (others => '0');
signal HammingData : std_logic_vector(17 downto 0) := (others => '0');
signal TTC_data_we : std_logic := '0';
signal TTC_data_in : std_logic_vector(8 downto 0) := (others => '0');
signal TTC_data : std_logic_vector(8 downto 0) := (others => '0');
signal TTC_data_a : std_logic_vector(2 downto 0) := (others => '1');
signal bcnt : std_logic_vector(11 downto 0) := (others => '0');
signal MmCntr : std_logic_vector(3 downto 0) := (others => '0');
signal GoodBcnt : std_logic_vector(3 downto 0) := (others => '0');
--signal BC0_link : std_logic := '0';
signal TrigData_d : std_logic_vector(8 downto 0) := (others => '0');
signal TrigData_i : std_logic_vector(8 downto 0) := (others => '0');
signal BC0_offset : std_logic_vector(3 downto 0) := (others => '0');
signal ec_delta_BC0 : std_logic := '0';
signal delta_BC0 : std_logic_vector(3 downto 0) := (others => '0');
signal sel_TTC_edge : std_logic_vector(2 downto 0) := (others => '0');
type array6x9 is array(0 to 5) of std_logic_vector(8 downto 0);
signal MatchCntr : array6x9 := (others => (others => '0'));
begin
TrigData <= TrigData_i(7 downto 0);
TTC_lock <= TTC_lock_i;
BC0_lock <= BC0_lock_i;
BcntMm <= BcntMm_i;
process(TTCclk)
begin
	if(TTCclk'event and TTCclk = '1')then
		Toggle <= not Toggle;
	end if;
end process;
process(UsrClk,reset)
begin
	if(reset = '1' or TTC_lock_i = '0')then
		TTC_data_a <= (others => '1');
		rd_enable <= '0';
		rd_TTC_data <= '0';
	elsif(UsrClk'event and UsrClk = '1')then
		if(TTC_data_we = '1' and rd_TTC_data = '0')then
			TTC_data_a <= TTC_data_a + 1;
		elsif(TTC_data_we = '0' and rd_TTC_data = '1' and TTC_data_a /= "111")then
			TTC_data_a <= TTC_data_a - 1;
		end if;
		if(TTC_data_a = "001")then
			rd_enable <= '1';
		end if;
		if(TTC_edge(3) = '1' and rd_enable = '1')then
			rd_TTC_data <= '1';
		else
			rd_TTC_data <= '0';
		end if;
	end if;
end process;
OutStrobe <= TTC_edge(4);
process(UsrClk)
variable reached256 : std_logic;
variable MatchCntr_MSB : std_logic_vector(5 downto 0);
begin
	if(simulation)then
		reached256 := MatchCntr(5)(4) or MatchCntr(4)(4) or MatchCntr(3)(4) or MatchCntr(2)(4) or MatchCntr(1)(4) or MatchCntr(0)(4);
		MatchCntr_MSB := MatchCntr(5)(4) & MatchCntr(4)(4) & MatchCntr(3)(4) & MatchCntr(2)(4) & MatchCntr(1)(4) & MatchCntr(0)(4);
	else
		reached256 := MatchCntr(5)(8) or MatchCntr(4)(8) or MatchCntr(3)(8) or MatchCntr(2)(8) or MatchCntr(1)(8) or MatchCntr(0)(8);
		MatchCntr_MSB := MatchCntr(5)(8) & MatchCntr(4)(8) & MatchCntr(3)(8) & MatchCntr(2)(8) & MatchCntr(1)(8) & MatchCntr(0)(8);
	end if;
	if(UsrClk'event and UsrClk = '1')then
		ToggleSyncRegs <= ToggleSyncRegs(2 downto 0) & Toggle;
		TTC_edge <= TTC_edge(5 downto 0) & (ToggleSyncRegs(3) xor ToggleSyncRegs(2));
		if(TTC_lock_i = '0')then
			chk_HammingData <= HammingDataValid_dl3;
		else
			chk_HammingData <= TTC_edge(conv_integer(sel_TTC_edge));
		end if;
		if((chk_HammingData = '1' and BC0_l = '1') or TTC_lock_i = '1')then
			MatchCntr <= (others => (others => '0'));
		elsif(chk_HammingData = '1' and reached256 = '0')then
			for i in 0 to 5 loop
				if(TTC_edge(i+1) = '1')then
					MatchCntr(i) <= MatchCntr(i) + 1;
				end if;
			end loop;
		end if;
		if(chk_HammingData = '1' and BC0_l = '1' and TTC_lock_i = '0')then
			sel_TTC_edge(2) <= MatchCntr_MSB(4) or MatchCntr_MSB(5);
			sel_TTC_edge(1) <= not MatchCntr_MSB(5) and (MatchCntr_MSB(2) or MatchCntr_MSB(3));
			sel_TTC_edge(0) <= MatchCntr_MSB(5) or (not MatchCntr_MSB(4) and (MatchCntr_MSB(3) or (not MatchCntr_MSB(2) and MatchCntr_MSB(1))));
		end if;
		if(chk_HammingData = '1')then
			if(TTC_lock_i = '0' and HammingData(17 downto 16) = "11")then
				bcnt <= x"001";
				BC0_l <= '0';
			elsif(simulation and bcnt = x"10f")then
				bcnt <= x"cd0";
			elsif(bcnt = x"deb")then
				bcnt <= x"000";
				BC0_l <= '1';
			else
				bcnt <= bcnt + 1;
				BC0_l <= '0';
			end if;
		end if;
--		if(OutStrobe = '1')then
--			TrigData <= TTC_data(7 downto 0);
--			BC0_link <= TTC_data(8);
--		end if;
		if(OutStrobe = '1')then
			TrigData_d <= TTC_data;
		end if;
		if(chk_HammingData = '1' and bcnt(3 downto 0) /= HammingData(15 downto 12))then
			BcntMm_i <= '1';
		else
			BcntMm_i <= '0';
		end if;
		if(TTC_lock_i = '1')then
			BC0_matchCntr <= (others => '0');
		elsif(chk_HammingData = '1' and HammingData(17 downto 16) = "11")then
			if(BC0_l = '0')then
				BC0_matchCntr <= (others => '0');
			else
				BC0_matchCntr <= BC0_matchCntr + 1;
			end if;
		end if;
		if(TTC_lock_i = '0')then
			MmCntr <= (others => '0');
		elsif(BcntMm_i = '1')then
			MmCntr <= MmCntr + 1;
		elsif(GoodBcnt(3) = '1')then
			MmCntr <= MmCntr - 1;
		end if;
		if(or_reduce(MmCntr) = '0' or GoodBcnt(3) = '1' or BcntMm_i = '1')then
			GoodBcnt <= (others => '0');
		elsif(chk_HammingData = '1')then
			GoodBcnt <= GoodBcnt + 1;
		end if;
		if(MmCntr(3) = '1')then
			TTC_lock_i <= '0';
		elsif(BC0_matchCntr(8) = '1' or (simulation and BC0_matchCntr(2) = '1'))then
			TTC_lock_i <= '1';
		end if;
		if(HammingDataValid = '1')then
			HammingData <= HammingData_in;
		end if;
		if(HammingData(17) = HammingData(16) and HammingData(17) = BC0_l and bcnt(3 downto 0) = HammingData(15 downto 12) and BC0_lock_i = '1')then
			TTC_data_in <= BC0_l & HammingData(7 downto 0);
		else
			TTC_data_in <= BC0_l & x"00";
		end if;
		if(chk_HammingData = '1')then
			TTC_data_we <= '1';
		else
			TTC_data_we <= '0';
		end if;
	end if;
end process;
i_HammingDataValid_dl3 : SRL16E
   port map (
      Q => HammingDataValid_dl3,       -- SRL data output
      A0 => '0',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => UsrClk,   -- Clock input
      D => HammingDataValid        -- SRL data input
   );
g_TTC_data : for i in 0 to 8 generate
	i_TTC_data : SRL16E
   port map (
      Q => TTC_data(i),       -- SRL data output
      A0 => TTC_data_a(0),     -- Select[0] input
      A1 => TTC_data_a(1),     -- Select[1] input
      A2 => TTC_data_a(2),     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => TTC_data_we,     -- Clock enable input
      CLK => UsrClk,   -- Clock input
      D => TTC_data_in(i)        -- SRL data input
   );
end generate;
g_TrigData : for i in 0 to 8 generate
	i_TrigData : SRL16E
   port map (
      Q => TrigData_i(i),       -- SRL data output
      A0 => BC0_offset(0),     -- Select[0] input
      A1 => BC0_offset(1),     -- Select[1] input
      A2 => BC0_offset(2),     -- Select[2] input
      A3 => BC0_offset(3),     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => TTCclk,   -- Clock input
      D => TrigData_d(i)        -- SRL data input
   );
end generate;
process(TTCclk, reset, TTC_lock_i)
begin
	if(reset = '1' or TTC_lock_i = '0')then
		ec_delta_BC0 <= '0';
		delta_BC0 <= (others => '0');
		BC0_offset <= (others => '0');
		BC0_lock_i <= '0';
	elsif(TTCclk'event and TTCclk = '1')then
--		if(BC0_link = '1')then
		if(TrigData_d(8) = '1')then
			ec_delta_BC0 <= '1';
		elsif(BC0 = '1' or delta_BC0 = x"f")then
			ec_delta_BC0 <= '0';
		end if;
		if(ec_delta_BC0 = '0')then
		  delta_BC0 <= (others => '0');
		else
		  delta_BC0 <= delta_BC0 + 1;
		end if;
		if(BC0 = '1')then
			if(ec_delta_BC0 = '1')then
				BC0_offset <= delta_BC0;
				BC0_lock_i <= '1';
			else
				BC0_offset <= (others => '0');
				BC0_lock_i <= '0';
			end if;
		end if;
	end if;
end process;

end Behavioral;

