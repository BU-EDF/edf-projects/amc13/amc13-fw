----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:33:25 01/11/2012 
-- Design Name: 
-- Module Name:    cmsCRC64 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cmsCRC64 is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           crc_init : in  STD_LOGIC;
           inject_err : in  STD_LOGIC;
           trailer : in  STD_LOGIC;
           crc_d : in  STD_LOGIC_VECTOR (63 downto 0);
           crc_ce : in  STD_LOGIC;
           crc : out  STD_LOGIC_VECTOR (15 downto 0);
           crc_err : out  STD_LOGIC;
           dout : out  STD_LOGIC_VECTOR (63 downto 0);
           dout_vld : out  STD_LOGIC);
end cmsCRC64;

architecture Behavioral of cmsCRC64 is
signal c : std_logic_vector(15 downto 0) := (others => '1');
signal d : std_logic_vector(63 downto 0) := (others => '0');
signal d_q : std_logic_vector(63 downto 0) := (others => '0');
signal crc_ce_q : std_logic := '1';
signal trailer_q : std_logic := '1';
begin
d <= crc_d;
crc <= c;
process(clk)
begin
	if(clk'event and clk = '1')then
		crc_ce_q <= crc_ce and not reset;
		dout_vld <= crc_ce_q and not reset;
		d_q <= d;
		dout(63 downto 32) <= d_q(63 downto 32);
		dout(15 downto 0) <= d_q(15 downto 0);
		trailer_q <= trailer and crc_ce;
		if(trailer_q = '1' and inject_err = '0')then
			dout(31 downto 16) <= c;
		else
			dout(31 downto 16) <= d_q(31 downto 16);
		end if;
		if(crc_init = '1')then
			crc_err <= '0';
		elsif(trailer_q = '1' and d_q(31 downto 16) /= c)then
			crc_err <= '1';
		else
			crc_err <= '0';
		end if;
		if(crc_init = '1')then
			c <= (others => '1');
		elsif(crc_ce = '1')then
      c(0)  <= d(63) xor d(62) xor d(61) xor d(60) xor d(55) xor d(54) xor
               d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor
               d(47) xor d(46) xor d(45) xor d(43) xor d(41) xor d(40) xor
               d(39) xor d(38) xor d(37) xor d(36) xor d(35) xor d(34) xor
               d(33) xor d(32) xor ((d(31) xor d(30) xor d(27) xor d(26) xor
               d(25) xor d(24) xor d(23) xor d(22) xor d(21) xor d(20) xor
               d(19) xor d(18) xor d(17) xor d(16)) and not trailer) xor d(15) xor d(13) xor
               d(12) xor d(11) xor d(10) xor d(9)  xor d(8)  xor d(7)  xor
               d(6)  xor d(5)  xor d(4)  xor d(3)  xor d(2)  xor d(1)  xor
               d(0)  xor c(15) xor c(14) xor c(13) xor c(12) xor c(7)  xor
               c(6)  xor c(5)  xor c(4)  xor c(3)  xor c(2)  xor c(1)  xor
               c(0);
      c(1)  <= d(63) xor d(62) xor d(61) xor d(56) xor d(55) xor d(54) xor
               d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor
               d(47) xor d(46) xor d(44) xor d(42) xor d(41) xor d(40) xor
               d(39) xor d(38) xor d(37) xor d(36) xor d(35) xor d(34) xor
               d(33) xor d(32) xor ((d(31) xor d(28) xor d(27) xor d(26) xor
               d(25) xor d(24) xor d(23) xor d(22) xor d(21) xor d(20) xor
               d(19) xor d(18) xor d(17) xor d(16)) and not trailer) xor d(14) xor d(13) xor
               d(12) xor d(11) xor d(10) xor d(9)  xor d(8)  xor d(7)  xor
               d(6)  xor d(5)  xor d(4)  xor d(3)  xor d(2)  xor d(1)  xor
               c(15) xor c(14) xor c(13) xor c(8)  xor c(7)  xor c(6)  xor
               c(5)  xor c(4)  xor c(3)  xor c(2)  xor c(1)  xor c(0);
      c(2)  <= d(61) xor d(60) xor d(57) xor d(56) xor d(46) xor d(42) xor
               ((d(31) xor d(30) xor d(29) xor d(28) xor d(16)) and not trailer) xor d(14) xor
               d(1)  xor d(0)  xor c(13) xor c(12) xor c(9)  xor c(8);
      c(3)  <= d(62) xor d(61) xor d(58) xor d(57) xor d(47) xor d(43) xor
               d(32) xor ((d(31) xor d(30) xor d(29) xor d(17)) and not trailer) xor d(15) xor
               d(2)  xor d(1)  xor c(14) xor c(13) xor c(10) xor c(9);
      c(4)  <= d(63) xor d(62) xor d(59) xor d(58) xor d(48) xor d(44) xor
               d(33) xor d(32) xor ((d(31) xor d(30) xor d(18) xor d(16)) and not trailer) xor
               d(3)  xor d(2)  xor c(15) xor c(14) xor c(11) xor c(10) xor
               c(0);
      c(5)  <= d(63) xor d(60) xor d(59) xor d(49) xor d(45) xor d(34) xor
               d(33) xor d(32) xor ((d(31) xor d(19) xor d(17)) and not trailer) xor d(4)  xor
               d(3)  xor c(15) xor c(12) xor c(11) xor c(1);
      c(6)  <= d(61) xor d(60) xor d(50) xor d(46) xor d(35) xor d(34) xor
               d(33) xor d(32) xor ((d(20) xor d(18)) and not trailer) xor d(5)  xor d(4)  xor
               c(13) xor c(12) xor c(2);
      c(7)  <= d(62) xor d(61) xor d(51) xor d(47) xor d(36) xor d(35) xor
               d(34) xor d(33) xor ((d(21) xor d(19)) and not trailer) xor d(6)  xor d(5)  xor
               c(14) xor c(13) xor c(3);
      c(8)  <= d(63) xor d(62) xor d(52) xor d(48) xor d(37) xor d(36) xor
               d(35) xor d(34) xor ((d(22) xor d(20)) and not trailer) xor d(7)  xor d(6)  xor
               c(15) xor c(14) xor c(4)  xor c(0);
      c(9)  <= d(63) xor d(53) xor d(49) xor d(38) xor d(37) xor d(36) xor
               d(35) xor ((d(23) xor d(21)) and not trailer) xor d(8)  xor d(7)  xor c(15) xor
               c(5)  xor c(1);
      c(10) <= d(54) xor d(50) xor d(39) xor d(38) xor d(37) xor d(36) xor
               ((d(24) xor d(22)) and not trailer) xor d(9)  xor d(8)  xor c(6)  xor c(2);
      c(11) <= d(55) xor d(51) xor d(40) xor d(39) xor d(38) xor d(37) xor
               ((d(25) xor d(23)) and not trailer) xor d(10) xor d(9)  xor c(7)  xor c(3);
      c(12) <= d(56) xor d(52) xor d(41) xor d(40) xor d(39) xor d(38) xor
               ((d(26) xor d(24)) and not trailer) xor d(11) xor d(10) xor c(8)  xor c(4);
      c(13) <= d(57) xor d(53) xor d(42) xor d(41) xor d(40) xor d(39) xor
               ((d(27) xor d(25)) and not trailer) xor d(12) xor d(11) xor c(9)  xor c(5);
      c(14) <= d(58) xor d(54) xor d(43) xor d(42) xor d(41) xor d(40) xor
               ((d(28) xor d(26)) and not trailer) xor d(13) xor d(12) xor c(10) xor c(6);
      c(15) <= d(63) xor d(62) xor d(61) xor d(60) xor d(59) xor d(54) xor
               d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor
               d(47) xor d(46) xor d(45) xor d(44) xor d(42) xor d(40) xor
               d(39) xor d(38) xor d(37) xor d(36) xor d(35) xor d(34) xor
               d(33) xor d(32) xor ((d(31) xor d(30) xor d(29) xor d(26) xor
               d(25) xor d(24) xor d(23) xor d(22) xor d(21) xor d(20) xor
               d(19) xor d(18) xor d(17) xor d(16)) and not trailer) xor d(15) xor d(14) xor
               d(12) xor d(11) xor d(10) xor d(9)  xor d(8)  xor d(7)  xor
               d(6)  xor d(5)  xor d(4)  xor d(3)  xor d(2)  xor d(1)  xor
               d(0)  xor c(15) xor c(14) xor c(13) xor c(12) xor c(11) xor
               c(6)  xor c(5)  xor c(4)  xor c(3)  xor c(2)  xor c(1)  xor
               c(0);
		end if;
	end if;
end process;

end Behavioral;

