----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:53:06 04/29/2014 
-- Design Name: 
-- Module Name:    AMC_DATA_FIFO - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity AMC_DATA_FIFO is
    Port ( wclk : in  STD_LOGIC;
           rclk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           fifo_en : in  STD_LOGIC;
           we : in  STD_LOGIC;
           re : in  STD_LOGIC;
           Di : in  STD_LOGIC_VECTOR (63 downto 0);
           Do : out  STD_LOGIC_VECTOR (63 downto 0);
           WRERR_OUT : out  STD_LOGIC_VECTOR (7 downto 0);
           RDERR_OUT : out  STD_LOGIC_VECTOR (7 downto 0);
           full : out  STD_LOGIC
          );
end AMC_DATA_FIFO;

architecture Behavioral of AMC_DATA_FIFO is
type array14x12 is array(0 to 13) of std_logic_vector(11 downto 0);
signal RDCOUNT : array14x12;
signal WRCOUNT : array14x12;
signal RDCOUNT14 : std_logic_vector(12 downto 0) := (others => '0');
signal WRCOUNT14 : std_logic_vector(12 downto 0) := (others => '0');
signal FIFO_full : std_logic_vector(13 downto 0) := (others => '0');
signal FIFO_empty : std_logic_vector(13 downto 0) := (others => '0');
signal FIFO_RdEn : std_logic_vector(6 downto 0) := (others => '0');
signal RDERR : std_logic_vector(7 downto 0) := (others => '0');
signal WRERR : std_logic_vector(7 downto 0) := (others => '0');
signal FIFO_Do : std_logic_vector(62 downto 0) := (others => '0');
begin
full <= FIFO_full(0);
process(wclk,reset)
begin
	if(reset = '1')then
	   WRERR_OUT <= (others => '0');
	elsif(wclk'event and wclk = '1')then
	   for i in 0 to 7 loop
	       if(WRERR(i) = '1')then
			WRERR_OUT(i) <= '1';
		   end if;
		end loop;
	end if;
end process;
process(rclk,reset)
begin
	if(reset = '1')then
	   RDERR_OUT <= (others => '0');
	elsif(rclk'event and rclk = '1')then
	   for i in 0 to 6 loop
	       if(RDERR(i) = '1')then
			RDERR_OUT(i) <= '1';
		   end if;
		end loop;
	end if;
end process;
g_FIFO: for i in 0 to 6 generate
	i_FIFO_i : FIFO_DUALCLOCK_MACRO
		 generic map (
				DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
				ALMOST_FULL_OFFSET => X"0008",  -- Sets almost full threshold
				ALMOST_EMPTY_OFFSET => X"0080", -- Sets the almost empty threshold
				DATA_WIDTH => 9,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
				FIFO_SIZE => "36Kb",            -- Target BRAM, "18Kb" or "36Kb" 
				FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
		 port map (
				ALMOSTEMPTY => open,   -- 1-bit output almost empty
				ALMOSTFULL => FIFO_full(i),     -- 1-bit output almost full
				DO => FIFO_Do(i*9+8 downto i*9),                     -- Output data, width defined by DATA_WIDTH parameter
				EMPTY => FIFO_empty(i),               -- 1-bit output empty
				FULL => open,                 -- 1-bit output full
				RDCOUNT => RDCOUNT(i),           -- Output read count, width determined by FIFO depth
				RDERR => open,               -- 1-bit output read error
				WRCOUNT => WRCOUNT(i),           -- Output write count, width determined by FIFO depth
				WRERR => WRERR(i),               -- 1-bit output write error
				DI => Di(i*9+8 downto i*9),                     -- Input data, width defined by DATA_WIDTH parameter
				RDCLK => wclk,               -- 1-bit input read clock
				RDEN => FIFO_Rden(i),                 -- 1-bit input read enable
				RST => reset,                   -- 1-bit input reset
				WRCLK => wclk,               -- 1-bit input write clock
				WREN => we                  -- 1-bit input write enable
		 );
	i_FIFO_o : FIFO_DUALCLOCK_MACRO
		 generic map (
				DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
				ALMOST_FULL_OFFSET => X"0004",  -- Sets almost full threshold
				ALMOST_EMPTY_OFFSET => X"0006", -- Sets the almost empty threshold
				DATA_WIDTH => 9,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
				FIFO_SIZE => "36Kb",            -- Target BRAM, "18Kb" or "36Kb" 
				FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
		 port map (
				ALMOSTEMPTY => FIFO_empty(i+7),   -- 1-bit output almost empty
				ALMOSTFULL => open,     -- 1-bit output almost full
				DO => Do(i*9+8 downto i*9),                     -- Output data, width defined by DATA_WIDTH parameter
				EMPTY => open,               -- 1-bit output empty
				FULL => FIFO_full(i+7),                 -- 1-bit output full
				RDCOUNT => RDCOUNT(i+7),           -- Output read count, width determined by FIFO depth
				RDERR => RDERR(i),               -- 1-bit output read error
				WRCOUNT => WRCOUNT(i+7),           -- Output write count, width determined by FIFO depth
				WRERR => open,               -- 1-bit output write error
				DI => FIFO_Do(i*9+8 downto i*9),                     -- Input data, width defined by DATA_WIDTH parameter
				RDCLK => rclk,               -- 1-bit input read clock
				RDEN => re,                 -- 1-bit input read enable
				RST => reset,                   -- 1-bit input reset
				WRCLK => wclk,               -- 1-bit input write clock
				WREN => FIFO_Rden(i)                  -- 1-bit input write enable
		 );
	FIFO_Rden(i) <= fifo_en and not FIFO_full(i+7) and not FIFO_empty(i);
end generate;
i_FIFO63 : FIFO_DUALCLOCK_MACRO
   generic map (
      DEVICE => "7SERIES",            -- Target Device: "VIRTEX5", "VIRTEX6", "7SERIES" 
      ALMOST_FULL_OFFSET => X"0004",  -- Sets almost full threshold
      ALMOST_EMPTY_OFFSET => X"0006", -- Sets the almost empty threshold
      DATA_WIDTH => 1,   -- Valid values are 1-72 (37-72 only valid when FIFO_SIZE="36Kb")
      FIFO_SIZE => "36Kb",            -- Target BRAM, "18Kb" or "36Kb" -- 18Kb max depth is 4K, need 36Kb to get 8K depth
      FIRST_WORD_FALL_THROUGH => TRUE) -- Sets the FIFO FWFT to TRUE or FALSE
   port map (
      ALMOSTEMPTY => open,   -- 1-bit output almost empty
      ALMOSTFULL => open,     -- 1-bit output almost full
      DO => Do(63 downto 63),                     -- Output data, width defined by DATA_WIDTH parameter
      EMPTY => open,               -- 1-bit output empty
      FULL => open,                 -- 1-bit output full
      RDCOUNT => RDCOUNT14,           -- Output read count, width determined by FIFO depth
      RDERR => RDERR(7),               -- 1-bit output read error
      WRCOUNT => WRCOUNT14,           -- Output write count, width determined by FIFO depth
      WRERR => WRERR(7),               -- 1-bit output write error
      DI => Di(63 downto 63),                     -- Input data, width defined by DATA_WIDTH parameter
      RDCLK => rclk,               -- 1-bit input read clock
      RDEN => re,                 -- 1-bit input read enable
      RST => reset,                   -- 1-bit input reset
      WRCLK => wclk,               -- 1-bit input write clock
      WREN => we                  -- 1-bit input write enable
   );
end Behavioral;

