----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:31:47 07/26/2010 
-- Design Name: 
-- Module Name:    ddr_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE,work;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity ddr_if is
	generic(SIM_BYPASS_INIT_CAL   : string  := "OFF"; SIMULATION            : string  := "FALSE");
	port(
		clk_ref     												: in    std_logic;
		mem_clk_p    												: in    std_logic;
		mem_clk_n    												: in    std_logic;
		mem_rst		   												: in    std_logic;
    sysclk	                            : in   std_logic;
    TCPclk	                            : in   std_logic;
    reset 	                            : in   std_logic;
    resetsys 	                          : in   std_logic;
    run			                           	: in   std_logic;
    mem_test	                          : in   std_logic_VECTOR(1 downto 0);
    EventData	                          : in   array3X67;
    EventData_we                        : in   std_logic_VECTOR(2 downto 0);
    wport_rdy	                        	: out  std_logic_VECTOR(2 downto 0);
    WrtMonBlkDone                     	: out  std_logic_VECTOR(2 downto 0);
    WrtMonEvtDone                     	: out  std_logic_VECTOR(2 downto 0);
    KiloByte_toggle                     : out  std_logic_VECTOR(2 downto 0);
    EoB_toggle                     			: out  std_logic_VECTOR(2 downto 0);
    EventBufAddr                        : in   array3x14;
    EventBufAddr_we	                    : in   std_logic_VECTOR(2 downto 0);
    EventFIFOfull		                    : out   std_logic_VECTOR(2 downto 0);
    TCP_din 														: in  STD_LOGIC_VECTOR(31 downto 0);
    TCP_channel													: in  STD_LOGIC_VECTOR(1 downto 0);
    TCP_we 															: in  STD_LOGIC;
		TCP_wcount 													: out  STD_LOGIC_VECTOR (2 downto 0);
		TCP_dout														: out  STD_LOGIC_VECTOR(31 downto 0); -- TCP data are written in unit of 32-bit words
		TCP_dout_type												: out  STD_LOGIC_VECTOR(2 downto 0); -- TCP data destination
		TCP_raddr														: in  std_logic_vector(28 downto 0);  -- 28-26 encoded request source 25-0 address in 64 bit word
		TCP_length													: in  std_logic_vector(12 downto 0); -- in 64 bit word, actual length - 1
		TCP_dout_valid											: out  STD_LOGIC;
		TCP_rrqst														:	in  STD_LOGIC;
		TCP_rack														: out  STD_LOGIC;
		TCP_lastword												: out  STD_LOGIC;
		cs_out	 														: out  STD_LOGIC_VECTOR (511 downto 0);
--	ipbus signals
    ipb_clk															: in  STD_LOGIC;
    ipb_write														: in  STD_LOGIC;
    ipb_strobe													: in  STD_LOGIC;
    page_addr														: in  STD_LOGIC_VECTOR(9 downto 0);
    ipb_addr														: in  STD_LOGIC_VECTOR(31 downto 0);
    ipb_wdata														: in  STD_LOGIC_VECTOR(31 downto 0);
    ipb_rdata														: out  STD_LOGIC_VECTOR(31 downto 0);
    ipb_ack															: out  STD_LOGIC;
		mem_stat 														: out  STD_LOGIC_VECTOR (63 downto 0);
    device_temp													: in  STD_LOGIC_VECTOR(11 downto 0);
-- ddr3 memory pins
		ddr3_dq 														: inout  STD_LOGIC_VECTOR (31 downto 0);
		ddr3_dm 														: out  STD_LOGIC_VECTOR (3 downto 0);
		ddr3_addr 													: out  STD_LOGIC_VECTOR (13 downto 0);
		ddr3_ba 														: out  STD_LOGIC_VECTOR (2 downto 0);
		ddr3_dqs_p 													: inout  STD_LOGIC_VECTOR (3 downto 0);
		ddr3_dqs_n 													: inout  STD_LOGIC_VECTOR (3 downto 0);
		ddr3_ras_n 													: out  STD_LOGIC;
		ddr3_cas_n 													: out  STD_LOGIC;
		ddr3_we_n 													: out  STD_LOGIC;
		ddr3_reset_n 												: out  STD_LOGIC;
		ddr3_cke 														: out  STD_LOGIC_vector(0 to 0);
		ddr3_odt 														: out  STD_LOGIC_vector(0 to 0);
		ddr3_ck_p 													: out  STD_LOGIC_vector(0 to 0);
		ddr3_ck_n 													: out  STD_LOGIC_vector(0 to 0)
		);
end ddr_if;

architecture Behavioral of ddr_if is
component ddr3_1_9a
  generic
  (


   --***************************************************************************
   -- The following parameters refer to width of various ports
   --***************************************************************************
   BANK_WIDTH            : integer := 3;
                                     -- # of memory Bank Address bits.
   CK_WIDTH              : integer := 1;
                                     -- # of CK/CK# outputs to memory.
   COL_WIDTH             : integer := 10;
                                     -- # of memory Column Address bits.
   CS_WIDTH              : integer := 1;
                                     -- # of unique CS outputs to memory.
   nCS_PER_RANK          : integer := 1;
                                     -- # of unique CS outputs per rank for phy
   CKE_WIDTH             : integer := 1;
                                     -- # of CKE outputs to memory.
   DATA_BUF_ADDR_WIDTH   : integer := 5;
   DQ_CNT_WIDTH          : integer := 5;
                                     -- = ceil(log2(DQ_WIDTH))
   DQ_PER_DM             : integer := 8;
   DM_WIDTH              : integer := 4;
                                     -- # of DM (data mask)
   DQ_WIDTH              : integer := 32;
                                     -- # of DQ (data)
   DQS_WIDTH             : integer := 4;
   DQS_CNT_WIDTH         : integer := 2;
                                     -- = ceil(log2(DQS_WIDTH))
   DRAM_WIDTH            : integer := 8;
                                     -- # of DQ per DQS
   ECC                   : string  := "OFF";
   DATA_WIDTH            : integer := 32;
   ECC_TEST              : string  := "OFF";
   PAYLOAD_WIDTH         : integer := 32;
   ECC_WIDTH             : integer := 8;
   MC_ERR_ADDR_WIDTH     : integer := 31;
      
   nBANK_MACHS           : integer := 4;
   RANKS                 : integer := 1;
                                     -- # of Ranks.
   ODT_WIDTH             : integer := 1;
                                     -- # of ODT outputs to memory.
   ROW_WIDTH             : integer := 14;
                                     -- # of memory Row Address bits.
   ADDR_WIDTH            : integer := 28;
                                     -- # = RANK_WIDTH + BANK_WIDTH
                                     --     + ROW_WIDTH + COL_WIDTH;
                                     -- Chip Select is always tied to low for
                                     -- single rank devices
   USE_CS_PORT          : integer := 0;
                                     -- # = 1, When Chip Select (CS#) output is enabled
                                     --   = 0, When Chip Select (CS#) output is disabled
                                     -- If CS_N disabled, user must connect
                                     -- DRAM CS_N input(s) to ground
   USE_DM_PORT           : integer := 1;
                                     -- # = 1, When Data Mask option is enabled
                                     --   = 0, When Data Mask option is disbaled
                                     -- When Data Mask option is disabled in
                                     -- MIG Controller Options page, the logic
                                     -- related to Data Mask should not get
                                     -- synthesized
   USE_ODT_PORT          : integer := 1;
                                     -- # = 1, When ODT output is enabled
                                     --   = 0, When ODT output is disabled
   PHY_CONTROL_MASTER_BANK : integer := 1;
                                     -- The bank index where master PHY_CONTROL resides,
                                     -- equal to the PLL residing bank
   MEM_DENSITY             : string  := "2Gb";
                                     -- Indicates the density of the Memory part
                                     -- Added for the sake of Vivado simulations
   MEM_SPEEDGRADE          : string  := "107E";
                                     -- Indicates the Speed grade of Memory Part
                                     -- Added for the sake of Vivado simulations
   MEM_DEVICE_WIDTH        : integer := 16;
                                     -- Indicates the device width of the Memory Part
                                     -- Added for the sake of Vivado simulations

   --***************************************************************************
   -- The following parameters are mode register settings
   --***************************************************************************
   AL                    : string  := "0";
                                     -- DDR3 SDRAM:
                                     -- Additive Latency (Mode Register 1).
                                     -- # = "0", "CL-1", "CL-2".
                                     -- DDR2 SDRAM:
                                     -- Additive Latency (Extended Mode Register).
   nAL                   : integer := 0;
                                     -- # Additive Latency in number of clock
                                     -- cycles.
   BURST_MODE            : string  := "8";
                                     -- DDR3 SDRAM:
                                     -- Burst Length (Mode Register 0).
                                     -- # = "8", "4", "OTF".
                                     -- DDR2 SDRAM:
                                     -- Burst Length (Mode Register).
                                     -- # = "8", "4".
   BURST_TYPE            : string  := "SEQ";
                                     -- DDR3 SDRAM: Burst Type (Mode Register 0).
                                     -- DDR2 SDRAM: Burst Type (Mode Register).
                                     -- # = "SEQ" - (Sequential),
                                     --   = "INT" - (Interleaved).
   CL                    : integer := 13;
                                     -- in number of clock cycles
                                     -- DDR3 SDRAM: CAS Latency (Mode Register 0).
                                     -- DDR2 SDRAM: CAS Latency (Mode Register).
   CWL                   : integer := 9;
                                     -- in number of clock cycles
                                     -- DDR3 SDRAM: CAS Write Latency (Mode Register 2).
                                     -- DDR2 SDRAM: Can be ignored
   OUTPUT_DRV            : string  := "LOW";
                                     -- Output Driver Impedance Control (Mode Register 1).
                                     -- # = "HIGH" - RZQ/7,
                                     --   = "LOW" - RZQ/6.
   RTT_NOM               : string  := "60";
                                     -- RTT_NOM (ODT) (Mode Register 1).
                                     --   = "120" - RZQ/2,
                                     --   = "60"  - RZQ/4,
                                     --   = "40"  - RZQ/6.
   RTT_WR                : string  := "OFF";
                                     -- RTT_WR (ODT) (Mode Register 2).
                                     -- # = "OFF" - Dynamic ODT off,
                                     --   = "120" - RZQ/2,
                                     --   = "60"  - RZQ/4,
   ADDR_CMD_MODE         : string  := "1T" ;
                                     -- # = "1T", "2T".
   REG_CTRL              : string  := "OFF";
                                     -- # = "ON" - RDIMMs,
                                     --   = "OFF" - Components, SODIMMs, UDIMMs.
   CA_MIRROR             : string  := "OFF";
                                     -- C/A mirror opt for DDR3 dual rank
   
   --***************************************************************************
   -- The following parameters are multiplier and divisor factors for PLLE2.
   -- Based on the selected design frequency these parameters vary.
   --***************************************************************************
   CLKIN_PERIOD          : integer := 4288;
                                     -- Input Clock Period
   CLKFBOUT_MULT         : integer := 8;
                                     -- write PLL VCO multiplier
   DIVCLK_DIVIDE         : integer := 1;
                                     -- write PLL VCO divisor
   CLKOUT0_PHASE         : real    := 337.5;
                                     -- Phase for PLL output clock (CLKOUT0)
   CLKOUT0_DIVIDE        : integer := 2;
                                     -- VCO output divisor for PLL output clock (CLKOUT0)
   CLKOUT1_DIVIDE        : integer := 2;
                                     -- VCO output divisor for PLL output clock (CLKOUT1)
   CLKOUT2_DIVIDE        : integer := 32;
                                     -- VCO output divisor for PLL output clock (CLKOUT2)
   CLKOUT3_DIVIDE        : integer := 8;
                                     -- VCO output divisor for PLL output clock (CLKOUT3)

   --***************************************************************************
   -- Memory Timing Parameters. These parameters varies based on the selected
   -- memory part.
   --***************************************************************************
   tCKE                  : integer := 5000;
                                     -- memory tCKE paramter in pS
   tFAW                  : integer := 25000;
                                     -- memory tRAW paramter in pS.
   tPRDI                 : integer := 1000000;
                                     -- memory tPRDI paramter in pS.
   tRAS                  : integer := 34000;
                                     -- memory tRAS paramter in pS.
   tRCD                  : integer := 13910;
                                     -- memory tRCD paramter in pS.
   tREFI                 : integer := 7800000;
                                     -- memory tREFI paramter in pS.
   tRFC                  : integer := 160000;
                                     -- memory tRFC paramter in pS.
   tRP                   : integer := 13910;
                                     -- memory tRP paramter in pS.
   tRRD                  : integer := 5000;
                                     -- memory tRRD paramter in pS.
   tRTP                  : integer := 7500;
                                     -- memory tRTP paramter in pS.
   tWTR                  : integer := 7500;
                                     -- memory tWTR paramter in pS.
   tZQI                  : integer := 128000000;
                                     -- memory tZQI paramter in nS.
   tZQCS                 : integer := 64;
                                     -- memory tZQCS paramter in clock cycles.

   --***************************************************************************
   -- Simulation parameters
   --***************************************************************************
   SIM_BYPASS_INIT_CAL   : string  := "OFF";
                                     -- # = "OFF" -  Complete memory init &
                                     --              calibration sequence
                                     -- # = "SKIP" - Not supported
                                     -- # = "FAST" - Complete memory init & use
                                     --              abbreviated calib sequence

   SIMULATION            : string  := "FALSE";
                                     -- Should be TRUE during design simulations and
                                     -- FALSE during implementations

   --***************************************************************************
   -- The following parameters varies based on the pin out entered in MIG GUI.
   -- Do not change any of these parameters directly by editing the RTL.
   -- Any changes required should be done through GUI and the design regenerated.
   --***************************************************************************
   BYTE_LANES_B0         : std_logic_vector(3 downto 0) := "0011";
                                     -- Byte lanes used in an IO column.
   BYTE_LANES_B1         : std_logic_vector(3 downto 0) := "1111";
                                     -- Byte lanes used in an IO column.
   BYTE_LANES_B2         : std_logic_vector(3 downto 0) := "1100";
                                     -- Byte lanes used in an IO column.
   BYTE_LANES_B3         : std_logic_vector(3 downto 0) := "0000";
                                     -- Byte lanes used in an IO column.
   BYTE_LANES_B4         : std_logic_vector(3 downto 0) := "0000";
                                     -- Byte lanes used in an IO column.
   DATA_CTL_B0           : std_logic_vector(3 downto 0) := "0011";
                                     -- Indicates Byte lane is data byte lane
                                     -- or control Byte lane. '1' in a bit
                                     -- position indicates a data byte lane and
                                     -- a '0' indicates a control byte lane
   DATA_CTL_B1           : std_logic_vector(3 downto 0) := "0000";
                                     -- Indicates Byte lane is data byte lane
                                     -- or control Byte lane. '1' in a bit
                                     -- position indicates a data byte lane and
                                     -- a '0' indicates a control byte lane
   DATA_CTL_B2           : std_logic_vector(3 downto 0) := "1100";
                                     -- Indicates Byte lane is data byte lane
                                     -- or control Byte lane. '1' in a bit
                                     -- position indicates a data byte lane and
                                     -- a '0' indicates a control byte lane
   DATA_CTL_B3           : std_logic_vector(3 downto 0) := "0000";
                                     -- Indicates Byte lane is data byte lane
                                     -- or control Byte lane. '1' in a bit
                                     -- position indicates a data byte lane and
                                     -- a '0' indicates a control byte lane
   DATA_CTL_B4           : std_logic_vector(3 downto 0) := "0000";
                                     -- Indicates Byte lane is data byte lane
                                     -- or control Byte lane. '1' in a bit
                                     -- position indicates a data byte lane and
                                     -- a '0' indicates a control byte lane
   PHY_0_BITLANES        : std_logic_vector(47 downto 0) := X"00000037F2FF";
   PHY_1_BITLANES        : std_logic_vector(47 downto 0) := X"000004F3FDFF";
   PHY_2_BITLANES        : std_logic_vector(47 downto 0) := X"3FE3DF000000";

   -- control/address/data pin mapping parameters
   CK_BYTE_MAP
     : std_logic_vector(143 downto 0) := X"000000000000000000000000000000000013";
   ADDR_MAP
     : std_logic_vector(191 downto 0) := X"00000010610710A10210510811B10110010B111113122119";
   BANK_MAP   : std_logic_vector(35 downto 0) := X"11510311A";
   CAS_MAP    : std_logic_vector(11 downto 0) := X"118";
   CKE_ODT_BYTE_MAP : std_logic_vector(7 downto 0) := X"00";
   CKE_MAP    : std_logic_vector(95 downto 0) := X"000000000000000000000104";
   ODT_MAP    : std_logic_vector(95 downto 0) := X"000000000000000000000112";
   CS_MAP     : std_logic_vector(119 downto 0) := X"000000000000000000000000000000";
   PARITY_MAP : std_logic_vector(11 downto 0) := X"000";
   RAS_MAP    : std_logic_vector(11 downto 0) := X"110";
   WE_MAP     : std_logic_vector(11 downto 0) := X"114";
   DQS_BYTE_MAP
     : std_logic_vector(143 downto 0) := X"000000000000000000000000000000012223";
   DATA0_MAP  : std_logic_vector(95 downto 0) := X"232235237234238231236233";
   DATA1_MAP  : std_logic_vector(95 downto 0) := X"220226221224223227228229";
   DATA2_MAP  : std_logic_vector(95 downto 0) := X"012015013016018011014019";
   DATA3_MAP  : std_logic_vector(95 downto 0) := X"000002001009006003005007";
   DATA4_MAP  : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA5_MAP  : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA6_MAP  : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA7_MAP  : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA8_MAP  : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA9_MAP  : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA10_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA11_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA12_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA13_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA14_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA15_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA16_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   DATA17_MAP : std_logic_vector(95 downto 0) := X"000000000000000000000000";
   MASK0_MAP  : std_logic_vector(107 downto 0) := X"000000000000000004010222239";
   MASK1_MAP  : std_logic_vector(107 downto 0) := X"000000000000000000000000000";

   SLOT_0_CONFIG         : std_logic_vector(7 downto 0) := "00000001";
                                     -- Mapping of Ranks.
   SLOT_1_CONFIG         : std_logic_vector(7 downto 0) := "00000000";
                                     -- Mapping of Ranks.
   MEM_ADDR_ORDER
     : string  := "BANK_ROW_COLUMN";

   --***************************************************************************
   -- IODELAY and PHY related parameters
   --***************************************************************************
   IODELAY_HP_MODE       : string  := "ON";
                                     -- to phy_top
   IBUF_LPWR_MODE        : string  := "OFF";
                                     -- to phy_top
   DATA_IO_IDLE_PWRDWN   : string  := "ON";
                                     -- # = "ON", "OFF"
   BANK_TYPE             : string  := "HP_IO";
                                     -- # = "HP_IO", "HPL_IO", "HR_IO", "HRL_IO"
   DATA_IO_PRIM_TYPE     : string  := "HP_LP";
                                     -- # = "HP_LP", "HR_LP", "DEFAULT"
   CKE_ODT_AUX           : string  := "FALSE";
   USER_REFRESH          : string  := "OFF";
   WRLVL                 : string  := "ON";
                                     -- # = "ON" - DDR3 SDRAM
                                     --   = "OFF" - DDR2 SDRAM.
   ORDERING              : string  := "NORM";
                                     -- # = "NORM", "STRICT", "RELAXED".
   CALIB_ROW_ADD         : std_logic_vector(15 downto 0) := X"0000";
                                     -- Calibration row address will be used for
                                     -- calibration read and write operations
   CALIB_COL_ADD         : std_logic_vector(11 downto 0) := X"000";
                                     -- Calibration column address will be used for
                                     -- calibration read and write operations
   CALIB_BA_ADD          : std_logic_vector(2 downto 0) := "000";
                                     -- Calibration bank address will be used for
                                     -- calibration read and write operations
   TCQ                   : integer := 100;
   IODELAY_GRP           : string  := "IODELAY_MIG";
                                     -- It is associated to a set of IODELAYs with
                                     -- an IDELAYCTRL that have same IODELAY CONTROLLER
                                     -- clock frequency.
   SYSCLK_TYPE           : string  := "DIFFERENTIAL";
                                     -- System clock type DIFFERENTIAL, SINGLE_ENDED,
                                     -- NO_BUFFER
   REFCLK_TYPE           : string  := "NO_BUFFER";
                                     -- Reference clock type DIFFERENTIAL, SINGLE_ENDED
                                     -- NO_BUFFER, USE_SYSTEM_CLOCK
      
   CMD_PIPE_PLUS1        : string  := "ON";
                                     -- add pipeline stage between MC and PHY
   DRAM_TYPE             : string  := "DDR3";
   CAL_WIDTH             : string  := "HALF";
   STARVE_LIMIT          : integer := 2;
                                     -- # = 2,3,4.

   --***************************************************************************
   -- Referece clock frequency parameters
   --***************************************************************************
   REFCLK_FREQ           : real    := 200.0;
                                     -- IODELAYCTRL reference clock frequency
   DIFF_TERM_REFCLK      : string  := "TRUE";
                                     -- Differential Termination for idelay
                                     -- reference clock input pins
   --***************************************************************************
   -- System clock frequency parameters
   --***************************************************************************
   tCK                   : integer := 1072;
                                     -- memory tCK paramter.
                                     -- # = Clock Period in pS.
   nCK_PER_CLK           : integer := 4;
                                     -- # of memory CKs per fabric CLK
   DIFF_TERM_SYSCLK      : string  := "FALSE";
                                     -- Differential Termination for System
                                     -- clock input pins

   --***************************************************************************
   -- Debug parameters
   --***************************************************************************
   DEBUG_PORT            : string  := "OFF";
                                     -- # = "ON" Enable debug signals/controls.
                                     --   = "OFF" Disable debug signals/controls.

   --***************************************************************************
   -- Temparature monitor parameter
   --***************************************************************************
   TEMP_MON_CONTROL         : string  := "EXTERNAL";
                                     -- # = "INTERNAL", "EXTERNAL"
      
   RST_ACT_LOW           : integer := 0
                                     -- =1 for active low reset,
                                     -- =0 for active high.
   );
  port
  (

   -- Inouts
   ddr3_dq                        : inout std_logic_vector(DQ_WIDTH-1 downto 0);
   ddr3_dqs_p                     : inout std_logic_vector(DQS_WIDTH-1 downto 0);
   ddr3_dqs_n                     : inout std_logic_vector(DQS_WIDTH-1 downto 0);

   -- Outputs
   ddr3_addr                      : out   std_logic_vector(ROW_WIDTH-1 downto 0);
   ddr3_ba                        : out   std_logic_vector(BANK_WIDTH-1 downto 0);
   ddr3_ras_n                     : out   std_logic;
   ddr3_cas_n                     : out   std_logic;
   ddr3_we_n                      : out   std_logic;
   ddr3_reset_n                   : out   std_logic;
   ddr3_ck_p                      : out   std_logic_vector(CK_WIDTH-1 downto 0);
   ddr3_ck_n                      : out   std_logic_vector(CK_WIDTH-1 downto 0);
   ddr3_cke                       : out   std_logic_vector(CKE_WIDTH-1 downto 0);
   
   ddr3_dm                        : out   std_logic_vector(DM_WIDTH-1 downto 0);
   ddr3_odt                       : out   std_logic_vector(ODT_WIDTH-1 downto 0);

   -- Inputs
   -- Differential system clocks
   sys_clk_p                      : in    std_logic;
   sys_clk_n                      : in    std_logic;
   -- Single-ended iodelayctrl clk (reference clock)
   clk_ref_i                                : in    std_logic;
   -- user interface signals
   app_addr             : in    std_logic_vector(ADDR_WIDTH-1 downto 0);
   app_cmd              : in    std_logic_vector(2 downto 0);
   app_en               : in    std_logic;
   app_wdf_data         : in    std_logic_vector((nCK_PER_CLK*2*PAYLOAD_WIDTH)-1 downto 0);
   app_wdf_end          : in    std_logic;
   app_wdf_mask         : in    std_logic_vector((nCK_PER_CLK*2*PAYLOAD_WIDTH)/8-1 downto 0)  ;
   app_wdf_wren         : in    std_logic;
   app_rd_data          : out   std_logic_vector((nCK_PER_CLK*2*PAYLOAD_WIDTH)-1 downto 0);
   app_rd_data_end      : out   std_logic;
   app_rd_data_valid    : out   std_logic;
   app_rdy              : out   std_logic;
   app_wdf_rdy          : out   std_logic;
   app_sr_req           : in    std_logic;
   app_sr_active        : out   std_logic;
   app_ref_req          : in    std_logic;
   app_ref_ack          : out   std_logic;
   app_zq_req           : in    std_logic;
   app_zq_ack           : out   std_logic;
   ui_clk               : out   std_logic;
   ui_clk_sync_rst      : out   std_logic;
   
      
   
   init_calib_complete  : out std_logic;
   device_temp_i                 : in  std_logic_vector(11 downto 0);
                      -- The 12 MSB bits of the temperature sensor transfer
                      -- function need to be connected to this port. This port
                      -- will be synchronized w.r.t. to fabric clock internally.
      

   -- System reset
      sys_rst                     : in    std_logic
 );
END COMPONENT;
COMPONENT ddr_rport
	PORT(
		memclk : IN std_logic;
		sysclk : IN std_logic;
		reset : IN std_logic;
		resetSys : IN std_logic;
		resetMem : IN std_logic;
		run : IN std_logic;
		test : IN std_logic_vector(1 downto 0);
		test_block_sent : IN std_logic;
		TCP_addr : IN std_logic_vector(28 downto 0);
		TCP_length : IN std_logic_vector(12 downto 0);
		TCP_rqst : IN std_logic;
		ipb_clk : IN std_logic;
		ipb_write : IN std_logic;
		ipb_strobe : IN std_logic;
		ipb_addr : IN std_logic_vector(31 downto 0);
		page_addr : IN std_logic_vector(9 downto 0);
		app_ack : IN std_logic;
		app_rdy : IN std_logic;
		app_rd_data_valid : IN std_logic;
		app_rd_data : IN std_logic_vector(255 downto 0);          
		test_pause : OUT std_logic;
		test_status : OUT std_logic_vector(63 downto 0);
		TCP_dout : OUT std_logic_vector(31 downto 0);
		TCP_dout_type : OUT std_logic_vector(2 downto 0);
		TCP_dout_valid : OUT std_logic;
		TCP_ack : OUT std_logic;
		TCP_lastword : OUT std_logic;
		cs_out : OUT std_logic_vector(511 downto 0);
		ipb_rdata : OUT std_logic_vector(31 downto 0);
		ipb_ack : OUT std_logic;
		app_rqst : OUT std_logic;
		app_en : OUT std_logic;
		app_addr : OUT std_logic_vector(27 downto 0)
		);
END COMPONENT;
COMPONENT ddr_wportA
	PORT(
		sysclk : IN std_logic;
		memclk : IN std_logic;
		fifo_rst : IN std_logic;
		fifo_en : IN std_logic;
		resetSys : in  STD_LOGIC;
		resetMem : in  STD_LOGIC;
		run : IN std_logic;
		din : IN std_logic_vector(65 downto 0);
		din_we : IN std_logic;
		event_addr : IN std_logic_vector(13 downto 0);
		addr_we : IN std_logic;
		ack : IN std_logic;
		app_wdf_rdy : IN std_logic;          
		app_rdy : IN std_logic;
		port_rdy : OUT std_logic;
    WrtMonBlkDone : out  std_logic;
    WrtMonEvtDone : out  std_logic;
    KiloByte_toggle : out  std_logic;
    EoB_toggle : out  std_logic;
		buf_full : OUT std_logic;
		rqst : OUT std_logic;
		app_en : OUT std_logic;
		app_wdf_wren : OUT std_logic;
		app_addr : OUT std_logic_vector(23 downto 0);
		dout : OUT std_logic_vector(255 downto 0);
		cs_out : OUT std_logic_vector(511 downto 0);
		debug : OUT std_logic_vector(63 downto 0)
		);
END COMPONENT;
COMPONENT ddr_wportB
	PORT(
		memclk : IN std_logic;
		sysclk : IN std_logic;
		reset : IN std_logic;
		resetSys : in  STD_LOGIC;
		resetMem : in  STD_LOGIC;
		run : IN std_logic;
		test : IN std_logic_vector(1 downto 0);
    TCP_din : in STD_LOGIC_VECTOR(31 downto 0);
    TCP_channel : in STD_LOGIC_VECTOR(1 downto 0);
		TCP_we : IN std_logic;
		TCP_wcount : out  STD_LOGIC_VECTOR (2 downto 0);
		ipb_clk : IN std_logic;
		ipb_write : IN std_logic;
		ipb_strobe : IN std_logic;
		ipb_addr : IN std_logic_vector(31 downto 0);
		ipb_wdata : IN std_logic_vector(31 downto 0);
		app_ack : IN std_logic;
		app_wdf_rdy : IN std_logic;          
		app_rdy : IN std_logic;
		test_block_sent : OUT std_logic;
		test_pause : IN STD_LOGIC;
		ipb_ack : OUT std_logic;
		app_rqst : OUT std_logic;
		app_en : OUT std_logic;
		app_wdf_wren : OUT std_logic;
		app_wdf_mask : OUT std_logic_vector(7 downto 0);
		app_addr : OUT std_logic_vector(23 downto 0);
		dout : OUT std_logic_vector(255 downto 0);
		debug_out : OUT std_logic_vector(63 downto 0)
		);
END COMPONENT;
COMPONENT FIFO_RESET_7S
	PORT(
		reset : IN std_logic;
		clk : IN std_logic;          
		fifo_rst : OUT std_logic;
		fifo_en : OUT std_logic
		);
END COMPONENT;
signal ipb_wack : std_logic := '0';
signal ipb_rack : std_logic := '0';
type array4X256 is array(0 to 3) of std_logic_vector(255 downto 0);
signal w_data : array4X256 := (others => (others => '0'));
type array4X24 is array(0 to 3) of std_logic_vector(23 downto 0);
signal w_addr : array4X24 := (others => (others => '0'));
signal test_stat : std_logic_vector(63 downto 0) := (others => '0');
signal w_mask : std_logic_vector(7 downto 0) := (others => '0');
signal app_wdf_wren : std_logic := '0';
signal app_en : std_logic := '0';
signal app_cmd : std_logic_vector(2 downto 0) := (others => '0');
signal app_addr : std_logic_vector(27 downto 0) := (others => '0');
signal app_raddr : std_logic_vector(27 downto 0) := (others => '0');
signal app_rdy : std_logic := '0';
signal app_rdyp : std_logic := '0';
signal app_wdf_rdy : std_logic := '0';
signal app_wdf_rdyp : std_logic := '0';
signal app_wdf_mask : std_logic_vector(31 downto 0) := (others => '0');
signal app_rd_data_valid : std_logic := '0';
signal clk : std_logic := '0';
signal phy_init_done : std_logic := '0';
signal app_rd_data : std_logic_vector(255 downto 0) := (others => '0');
signal app_wdf_data : std_logic_vector(255 downto 0) := (others => '0');
signal test_block_sent : std_logic := '0';
signal test_pause : std_logic := '0';
signal app_rrqst : std_logic := '0';
signal app_rack : std_logic := '0';
signal app_ren : std_logic := '0';
signal app_wen : std_logic_vector(3 downto 0) := (others => '1');
signal w_data_we : std_logic_vector(3 downto 0) := (others => '0');
signal w_rqst : std_logic_vector(3 downto 0) := (others => '0');
signal w_ack : std_logic_vector(3 downto 0) := (others => '0');
signal InitDoneSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal resetSyncRegsMem : std_logic_vector(2 downto 0) := (others => '0');
signal resetSyncRegsTCP : std_logic_vector(2 downto 0) := (others => '0');
signal sysCntr : std_logic_vector(19 downto 0) := (others => '0');
signal MemCntr : std_logic_vector(20 downto 0) := (others => '0');
signal sysCntrSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
--signal TCP_dout_i : std_logic_vector(31 downto 0) := (others => '0');
signal fifo_rst : std_logic := '1';
signal fifo_en : std_logic := '1';
signal reset_dl : std_logic := '1';
signal debug : std_logic_vector(255 downto 0) := (others => '0');
signal rport_cs : std_logic_vector(511 downto 0) := (others => '0');
type array3X512 is array(0 to 2) of std_logic_vector(511 downto 0);
signal wportA_cs : array3x512 := (others => (others => '0'));
COMPONENT chipscope1
	generic (N : integer := 5);
	PORT(
		clk : IN std_logic;
		Din : IN std_logic_vector(303 downto 0)       
		);
END COMPONENT;
signal cs_in : std_logic_vector(303 downto 0) := (others => '0');
COMPONENT chipscope
	generic (N : integer := 5);
	PORT(
		clka : IN std_logic;
		clkb : IN std_logic;
		ina : IN std_logic_vector(135 downto 0);
		inb : IN std_logic_vector(135 downto 0)       
		);
END COMPONENT;
signal cs_ina : std_logic_vector(135 downto 0) := (others => '0');
signal cs_inb : std_logic_vector(135 downto 0) := (others => '0');

begin
--i_chipscope: chipscope generic map(N => 7) PORT MAP(
--		clka => sysclk,
--		clkb => clk,
--		ina => cs_ina,
--		inb => cs_inb
--	);
--cs_ina(135 downto 128) <= cs_inb(135 downto 128);
--cs_inb(135 downto 132) <= wportA_cs(0)(18 downto 15);
--cs_inb(131) <= wportA_cs(0)(13);
--cs_inb(130 downto 128) <= wportA_cs(0)(42 downto 40);
--cs_ina(16 downto 0) <= wportA_cs(0)(37 downto 21);
--cs_inb(23 downto 21) <= wportA_cs(0)(42 downto 40);
--cs_inb(20 downto 0) <= wportA_cs(0)(20 downto 0);
--cs_out(165 downto 104) <= rport_cs(165 downto 104);
--cs_out(103 downto 89) <= wportA_cs(0)(14 downto 0);
--cs_out(88) <= app_wdf_wren;
--cs_out(87) <= app_wdf_rdy;
--cs_out(86) <= app_rdy;
--cs_out(85) <= app_en;
--cs_out(84 downto 81) <= app_wen;
--cs_out(80 downto 77) <= w_ack;
--cs_out(76 downto 73) <= w_rqst;
--cs_out(72 downto 0) <= rport_cs(72 downto 0);
--i_chipscope1: chipscope1 generic map(N => 2) PORT MAP(
--		clk => clk,
--		din => cs_in
--	);
--cs_in(288) <= w_rqst(3);
--cs_in(289) <= test_stat(31);
--cs_in(287) <= w_data_we(3);
--g_cs: for i in 0 to 7 generate
--    cs_in(i*26+103 downto i*26+78) <= w_data(3)(i*32+25 downto i*32);
--end generate;
--cs_in(77 downto 70) <= w_mask;
--cs_in(69 downto 46) <= w_addr(3);
--cs_in(45 downto 18) <= app_addr;
--cs_in(17 downto 14) <= w_ack;
--cs_in(13 downto 10) <= w_rqst;
--cs_in(9) <= app_rack;
--cs_in(8) <= app_ren;
--cs_in(7) <= app_rrqst;
--cs_in(6) <= app_wdf_rdyp;
--cs_in(5) <= app_wdf_wren;
--cs_in(4) <= app_wdf_rdy;
--cs_in(3) <= app_rdyp;
--cs_in(2) <= app_rdy;
--cs_in(1) <= app_en;
--cs_in(0) <= app_wen(3);
--TCP_dout <= TCP_dout_i;
process(sysclk)
begin
	if(sysclk'event and sysclk = '1')then
		SysCntr <= SysCntr + 1;
	end if;
end process;
ipb_ack <= ipb_rack or ipb_wack;
i_ddr_rport: ddr_rport PORT MAP (
		memclk => clk,
		sysclk => TCPclk,
		reset => reset,
		resetSys => resetSyncRegsTCP(2),
		resetMem => resetSyncRegsMem(2),
		run => run,
		test => mem_test,
		test_block_sent => test_block_sent,
		test_pause => test_pause,
		test_status => test_stat,
		TCP_dout => TCP_dout,
		TCP_dout_type => TCP_dout_type,
		TCP_addr => TCP_raddr,
		TCP_length => TCP_length,
		TCP_dout_valid => TCP_dout_valid,
		TCP_rqst => TCP_rrqst,
		TCP_lastword => TCP_lastword,
		TCP_ack => TCP_rack,
		cs_out => rport_cs,
		ipb_clk => ipb_clk,
		ipb_write => ipb_write,
		ipb_strobe => ipb_strobe,
		ipb_addr => ipb_addr,
		ipb_rdata => ipb_rdata,
		ipb_ack => ipb_rack,
		page_addr => page_addr,
		app_rqst => app_rrqst,
		app_ack => app_rack,
		app_en => app_ren,
		app_rdy => app_rdyp,
		app_rd_data_valid => app_rd_data_valid,
		app_rd_data => app_rd_data,
		app_addr => app_raddr
  );
i_FIFO_RESET_7S: FIFO_RESET_7S PORT MAP(
		reset => resetSys,
		clk => sysclk,
		fifo_rst => fifo_rst,
		fifo_en => fifo_en
	);
g_ddr_wportA : for i in 0 to 2 generate
	i_ddr_wportA: ddr_wportA PORT MAP(
		sysclk => sysclk,
		memclk => clk,
		fifo_rst => fifo_rst,
		fifo_en => fifo_en,
		resetSys => resetSys,
		resetMem => resetSyncRegsMem(2),
		run => run,
		din => EventData(i)(65 downto 0),
		din_we => EventData_we(i),
		port_rdy => wport_rdy(i),
		WrtMonBlkDone => WrtMonBlkDone(i),
		WrtMonEvtDone => WrtMonEvtDone(i),
		KiloByte_toggle => KiloByte_toggle(i),
		EoB_toggle => EoB_toggle(i),
		buf_full => EventFIFOfull(i),
		event_addr => EventBufAddr(i),
		addr_we => EventBufAddr_we(i),
		rqst => w_rqst(i),
		ack => w_ack(i),
		app_rdy => app_rdyp,
    app_en => app_wen(i),
		app_wdf_rdy => app_wdf_rdyp,
		app_wdf_wren => w_data_we(i),
		app_addr => w_addr(i),
		dout => w_data(i),
		cs_out => wportA_cs(i),
		debug => debug(64*i+63 downto 64*i)
	);
end generate;
i_ddr_wportB: ddr_wportB PORT MAP(
		memclk => clk,
		sysclk => TCPclk,
		reset => reset,
		resetSys => resetSyncRegsTCP(2),
		resetMem => resetSyncRegsMem(2),
		run => run,
		test => mem_test,
		test_block_sent => test_block_sent,
		test_pause => test_pause,
		TCP_din => TCP_din,
		TCP_channel => TCP_channel,
		TCP_we => TCP_we,
		TCP_wcount => TCP_wcount,
		ipb_clk => ipb_clk,
		ipb_write => ipb_write,
		ipb_strobe => ipb_strobe,
		ipb_addr => ipb_addr,
		ipb_wdata => ipb_wdata,
		ipb_ack => ipb_wack,
		app_rqst => w_rqst(3),
		app_ack => w_ack(3),
		app_en => app_wen(3),
		app_rdy => app_rdyp,
		app_wdf_rdy => app_wdf_rdyp,
		app_wdf_wren => w_data_we(3),
		app_wdf_mask => w_mask,
		app_addr => w_addr(3),
		dout => w_data(3),
		debug_out => debug(255 downto 192)
	);
app_rdyp <= app_rdy or not app_en;
app_wdf_rdyp <= app_wdf_rdy or not app_wdf_wren;
process(TCPclk,reset)
begin
	if(reset = '1')then
		resetSyncRegsTCP <= (others => '1');
	elsif(TCPclk'event and TCPclk = '1')then
		resetSyncRegsTCP <= resetSyncRegsTCP(1 downto 0) & '0';
	end if;
end process;
process(clk,reset)
begin
	if(reset = '1')then
		resetSyncRegsMem <= (others => '1');
	elsif(clk'event and clk = '1')then
		resetSyncRegsMem <= resetSyncRegsMem(1 downto 0) & '0';
	end if;
end process;
process(clk,phy_init_done)
begin
	if(phy_init_done = '0')then
		InitDoneSyncRegs <= (others => '0');
	elsif(clk'event and clk = '1')then
		InitDoneSyncRegs <= InitDoneSyncRegs(1 downto 0) & '1';
	end if;
end process;
process(clk, w_ack)
variable w_sel : std_logic_vector(1 downto 0);
begin
	w_sel(1) := w_ack(3) or w_ack(2);
	w_sel(0) := w_ack(3) or w_ack(1);
	if(clk'event and clk = '1')then
		if(InitDoneSyncRegs(2) = '0')then
			app_rack <= '0';
			w_ack <= (others => '0');
		elsif(or_reduce(w_ack and w_rqst) = '0' and (app_rrqst = '0' or app_rack = '0'))then
			app_rack <= '0';
			w_ack <= (others => '0');
			if(w_rqst(3) = '1')then
				w_ack(3) <= '1';
			elsif(app_rrqst = '1')then
        app_rack <= '1';
			elsif(w_rqst(0) = '1')then
				w_ack(0) <= '1';
			elsif(w_rqst(1) = '1')then
				w_ack(1) <= '1';
			elsif(w_rqst(2) = '1')then
				w_ack(2) <= '1';
			end if;
		end if;
		if(app_en = '0' or app_rdy = '1')then
			if(app_rack = '1')then
				app_cmd(0) <= '1';
				app_addr(26 downto 3) <= app_raddr(26 downto 3);
			else
				app_cmd(0) <= '0';
				app_addr(26 downto 3) <= w_addr(conv_integer(w_sel));
			end if;
		end if;
		if(resetSyncRegsMem(2) = '1')then
			app_en <= '0';
		elsif(or_reduce(app_wen) = '1' or app_ren = '1')then
			app_en <= '1';
		elsif(app_rdy = '1')then
			app_en <= '0';
		end if;
		if(resetSyncRegsMem(2) = '1')then
			app_wdf_wren <= '0';
		elsif(or_reduce(w_data_we) = '1')then
			app_wdf_wren <= '1';
		elsif(app_wdf_rdy = '1')then
			app_wdf_wren <= '0';
		end if;
		if(app_wdf_rdy = '1' or app_wdf_wren = '0')then
			if(w_ack(3) = '1')then
				for i in 0 to 31 loop
					app_wdf_mask(i) <= w_mask(i/4);
				end loop;
			else
				app_wdf_mask <= (others => '0');
			end if;
			case w_sel is
				when "00" => app_wdf_data <= w_data(0);
				when "01" => app_wdf_data <= w_data(1);
				when "10" => app_wdf_data <= w_data(2);
				when others => app_wdf_data <= w_data(3);
			end case;
		end if;
	end if;
end process;
app_addr(27) <= '0';
app_addr(2 downto 0) <= "000";
i_ddr3 : ddr3_1_9a
  port map(
    ddr3_dq                   => ddr3_dq,
    ddr3_dqs_n                => ddr3_dqs_n,
    ddr3_dqs_p                => ddr3_dqs_p,
    ddr3_addr                 => ddr3_addr,
    ddr3_ba                   => ddr3_ba,
    ddr3_ras_n                => ddr3_ras_n,
    ddr3_cas_n                => ddr3_cas_n,
    ddr3_we_n                 => ddr3_we_n,
    ddr3_reset_n              => ddr3_reset_n,
    ddr3_ck_p                 => ddr3_ck_p,
    ddr3_ck_n                 => ddr3_ck_n,
    ddr3_cke                  => ddr3_cke,
    ddr3_dm                   => ddr3_dm,
    ddr3_odt                  => ddr3_odt,
    sys_clk_p                 => mem_clk_p,
    sys_clk_n                 => mem_clk_n,
    clk_ref_i                 => clk_ref,
    app_addr                  => app_addr,
    app_cmd                   => app_cmd,
    app_en                    => app_en,
    app_wdf_data              => app_wdf_data,
    app_wdf_end               => app_wdf_wren,
    app_wdf_mask              => app_wdf_mask,
    app_wdf_wren              => app_wdf_wren,
    app_rd_data               => app_rd_data,
    app_rd_data_end           => open,
    app_rd_data_valid         => app_rd_data_valid,
    app_rdy                   => app_rdy,
    app_wdf_rdy               => app_wdf_rdy,
    app_sr_req              	=> '0',
    app_sr_active             => open,
    app_ref_req              	=> '0',
    app_ref_ack		            => open,
    app_zq_req              	=> '0',
    app_zq_ack             		=> open,
    ui_clk                    => clk,
    ui_clk_sync_rst           => open ,
    init_calib_complete       => phy_init_done,
		device_temp_i							=> device_temp,
    sys_rst               		=> mem_rst
    );
--process(phy_init_done,test_stat,ipb_addr,app_rrqst, app_rack, app_ren, app_wen, w_data_we, w_rqst, w_ack, app_en_accept, test_block_sent, test_pause, app_wdf_wren, app_en, app_cmd, app_rdy, app_wdf_rdy, app_wdf_rdyp, app_rd_data_valid, w_mask, w_addr, app_cmd, app_addr, app_raddr)
--begin
--  case ipb_addr(18 downto 16) is
--    when "000" => mem_stat <= not phy_init_done & test_stat(32) & x"0" & app_rrqst & app_rack & app_ren & test_block_sent & test_pause & app_en_accept & app_wen & w_data_we & w_rqst & w_ack & test_stat(31 downto 0);
--    when "001" => mem_stat <= "00" & app_wdf_wren & app_en & app_rdy & app_wdf_rdy & app_wdf_rdyp & app_rd_data_valid & w_addr(0) & w_mask & w_addr(1);
--    when "010" => mem_stat <= "00000" & app_cmd & w_addr(3) & "00" & test_stat(63 downto 58) & w_addr(2);
--    when "011" => mem_stat <= x"0" & app_addr & x"0" & app_raddr;
--    when "100" => mem_stat <= debug(63 downto 0);
--    when "101" => mem_stat <= debug(127 downto 64);
--    when "110" => mem_stat <= debug(191 downto 128);
----    when others => mem_stat <= debug(255 downto 192);
--    when others => mem_stat <= test_stat(63 downto 32) & debug(223 downto 192);
--  end case;
--end process;
mem_stat(63) <= not phy_init_done;
mem_stat(62) <= debug(192);
mem_stat(61) <= debug(128);
mem_stat(60) <= debug(64);
mem_stat(31 downto 0) <= test_stat(31 downto 0);
--mem_stat(62) <= test_stat(32);
mem_stat(59 downto 32) <= (others => '0');
--mem_stat(40 downto 32) <= debug(8 downto 0);
--mem_stat(31 downto 0) <= test_stat(31 downto 0);
--mem_stat(31 downto 21) <= SysCntr(10 downto 0);
--mem_stat(20 downto 0) <= MemCntr;
--mem_stat(55 downto 53) <= SysCntrSync;
--mem_stat(0) <= overflowErr;
--mem_stat(1) <= testErr;
--mem_stat(2) <= mem_ready;
--mem_stat(17) <= rd_rqst;
--mem_stat(18) <= not wfifo_empty;
--mem_stat(19) <= wfifo_full;
--mem_stat(20) <= test;
--mem_stat(21) <= EOF;
--mem_stat(60 downto 56) <= SysCntr(15 downto 11);
--mem_stat(26) <= not fifo_en;
--mem_stat(27) <= app_cmd(0);
--mem_stat(28) <= app_en;
--mem_stat(29) <= not app_rdy;
--mem_stat(30) <= not app_wdf_rdy;
--mem_stat(31) <= not phy_init_done;
end Behavioral;

