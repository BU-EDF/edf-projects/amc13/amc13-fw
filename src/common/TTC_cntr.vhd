----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:54:09 02/16/2016 
-- Design Name: 
-- Module Name:    TTC_cntr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TTC_cntr is
    Port ( sysclk : in  STD_LOGIC;
           clk125 : in  STD_LOGIC;
           ipb_clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           rst_cntr : in  STD_LOGIC;
           DB_cmd : in  STD_LOGIC;
           inc_serr : in  STD_LOGIC;
           inc_derr : in  STD_LOGIC;
           inc_bcnterr : in  STD_LOGIC;
           inc_l1ac : in  STD_LOGIC;
           run : in  STD_LOGIC;
           state : in  STD_LOGIC_VECTOR(3 downto 0);
           ttc_resync : in  STD_LOGIC;
           ipb_addr : in  STD_LOGIC_VECTOR(15 downto 0);
           ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0));
end TTC_cntr;
architecture Behavioral of TTC_cntr is
signal TTC_serr_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal TTC_derr_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal TTC_BcntErr_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal L1A_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal L1A_OFW_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal L1A_BUSY_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal L1A_LOS_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal run_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal ready_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal busy_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal sync_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal ovfl_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal daq_ovfl_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal state0x9_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal state0xa_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal state0xb_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal ReSync_cntr : std_logic_vector(8 downto 0) := (others =>'0');
signal state0x9_cntrb : std_logic_vector(8 downto 0) := (others =>'0');
signal state0xa_cntrb : std_logic_vector(8 downto 0) := (others =>'0');
signal state0xb_cntrb : std_logic_vector(8 downto 0) := (others =>'0');
signal state_was_9 : std_logic := '0';
signal state_was_a : std_logic := '0';
signal state_was_b : std_logic := '0';
signal startSyncRegs : std_logic_vector(3 downto 0) := (others =>'0');
signal div : std_logic_vector(4 downto 0) := (others =>'0');
signal start : std_logic := '0';
signal CntrRstCycle : std_logic := '0';
signal cntr : std_logic_vector(8 downto 0) := (others => '0');
signal ram_di : std_logic_vector(31 downto 0) := (others => '0');
signal ram_dpo : std_logic_vector(31 downto 0) := (others => '0');
signal ram_spo : std_logic_vector(31 downto 0) := (others => '0');
signal we_ram : std_logic := '0';
signal ram_wa : std_logic_vector(6 downto 0) := (others => '0');
signal ram_ra : std_logic_vector(6 downto 0) := (others => '0');
signal ec_div : std_logic := '0';
signal channel : std_logic_vector(4 downto 0) := (others => '0');
signal DB_cmd_l : std_logic := '0';
signal DB_en : std_logic := '0';
signal carry : std_logic := '0';
signal sr : std_logic_vector(5 downto 0) := (others => '0');
signal toggle : std_logic := '0';
signal toggle_q : std_logic := '0';
signal ec_rdata : std_logic := '0';
signal ec_wdata : std_logic := '0';

begin
process(sysClk, rst_cntr,reset)
begin
	if(reset = '1' or rst_cntr = '1')then
		TTC_serr_cntr <= (others =>'0');
		TTC_derr_cntr <= (others =>'0');
		TTC_BcntErr_cntr <= (others =>'0');
		L1A_cntr <= (others =>'0');
		L1A_OFW_cntr <= (others =>'0');
		L1A_BUSY_cntr <= (others =>'0');
		L1A_LOS_cntr <= (others =>'0');
		run_cntr <= (others =>'0');
		ready_cntr <= (others =>'0');
		busy_cntr <= (others =>'0');
		sync_cntr <= (others =>'0');
		ovfl_cntr <= (others =>'0');
		daq_ovfl_cntr <= (others =>'0');
		state0x9_cntr <= (others =>'0');
		state0xa_cntr <= (others =>'0');
		state0xb_cntr <= (others =>'0');
		state0x9_cntrb <= (others =>'0');
		state0xa_cntrb <= (others =>'0');
		state0xb_cntrb <= (others =>'0');
		ReSync_cntr <= (others =>'0');
	elsif(sysClk'event and sysClk = '1')then
		if(inc_serr = '1')then
			TTC_serr_cntr <= TTC_serr_cntr + 1;
		end if;
		if(inc_derr = '1')then
			TTC_derr_cntr <= TTC_derr_cntr + 1;
		end if;
		if(inc_bcnterr = '1')then
			TTC_BcntErr_cntr <= TTC_BcntErr_cntr + 1;
		end if;
		if(inc_l1ac = '1')then
			L1A_cntr <= L1A_cntr + 1;
			if(state = x"1" or state = x"6")then
				L1A_OFW_cntr <= L1A_OFW_cntr + 1;
			end if;
			if(state = x"4")then
				L1A_BUSY_cntr <= L1A_BUSY_cntr + 1;
			end if;
			if(state = x"2")then
				L1A_LOS_cntr <= L1A_LOS_cntr + 1;
			end if;
		end if;
		if(run = '1')then
			run_cntr <= run_cntr + 1;
			if(state = x"8")then
				ready_cntr <= ready_cntr + 1;
			end if;
			if(state = x"4")then
				busy_cntr <= busy_cntr + 1;
			end if;
			if(state = x"2")then
				sync_cntr <= sync_cntr + 1;
			end if;
			if(state = x"1" or state = x"6")then
				ovfl_cntr <= ovfl_cntr + 1;
			end if;
			if(state = x"6")then
				daq_ovfl_cntr <= daq_ovfl_cntr + 1;
			end if;
			if(state = x"9")then
				state0x9_cntr <= state0x9_cntr + 1;
			end if;
			if(state = x"a")then
				state0xa_cntr <= state0xa_cntr + 1;
			end if;
			if(state = x"b")then
				state0xb_cntr <= state0xb_cntr + 1;
			end if;
			if(state = x"9" and state_was_9 = '0')then
				state0x9_cntrb <= state0x9_cntrb + 1;
			end if;
			if(state = x"a" and state_was_a = '0')then
				state0xa_cntrb <= state0xa_cntrb + 1;
			end if;
			if(state = x"b" and state_was_b = '0')then
				state0xb_cntrb <= state0xb_cntrb + 1;
			end if;
		end if;
		if(ttc_resync = '1')then
      ReSync_cntr <= ReSync_cntr + 1;
    end if;
	end if;
end process;
process(sysClk)
begin
	if(sysClk'event and sysClk = '1')then
		if(state = x"9")then
			state_was_9 <= '1';
		else
			state_was_9 <= '0';
		end if;
		if(state = x"a")then
			state_was_a <= '1';
		else
			state_was_a <= '0';
		end if;
		if(state = x"b")then
			state_was_b <= '1';
		else
			state_was_b <= '0';
		end if;
	end if;
end process;
start <= sr(0);
process(sysClk, CntrRstCycle)
begin
	if(CntrRstCycle = '1')then
		startSyncRegs <= (others => '0');
		ec_div <= '0';
		div <= (others => '0');
		channel <= (others => '0');
		cntr <= (others => '0');
	elsif(sysclk'event and sysclk = '1')then
		startSyncRegs <= startSyncRegs(2 downto 0) & start;
		if(startSyncRegs(3 downto 2) = "01")then
			ec_div <= '1';
		else
			ec_div <= '0';
		end if;
		if(ec_div = '1')then
			if(div = "10011")then -- reached the last counter
				div <= (others => '0');
			else
				div <= div + 1;
			end if;
			channel <= div;
			if(div(4) = '0')then
				case div(3 downto 0) is
					when x"0" => cntr <= TTC_serr_cntr;
					when x"1" => cntr <= TTC_derr_cntr;
					when x"2" => cntr <= TTC_BcntErr_cntr;
					when x"3" => cntr <= L1A_cntr;
					when x"4" => cntr <= run_cntr;
					when x"5" => cntr <= ready_cntr;
					when x"6" => cntr <= busy_cntr;
					when x"7" => cntr <= sync_cntr;
					when x"8" => cntr <= ovfl_cntr;
					when x"9" => cntr <= L1A_OFW_cntr;
					when x"a" => cntr <= L1A_BUSY_cntr;
					when x"b" => cntr <= L1A_LOS_cntr;
					when x"c" => cntr <= ReSync_cntr;
					when x"d" => cntr <= daq_ovfl_cntr;
					when x"e" => cntr <= state0x9_cntr;
					when others => cntr <= state0xa_cntr;
				end case;
			else
				case div(3 downto 0) is
					when x"0" => cntr <= state0xb_cntr;
					when x"1" => cntr <= state0x9_cntrb;
					when x"2" => cntr <= state0xa_cntrb;
					when x"3" => cntr <= state0xb_cntrb;
					when others => cntr <= (others => '0');
				end case;
			end if;
		end if;
	end if;
end process;
g_ram: for i in 0 to 31 generate
   i_ram : RAM128X1D
   port map (
      DPO => ram_dpo(i),     -- Read-only 1-bit data output
      SPO => ram_spo(i),     -- R/W 1-bit data output
      A => ram_wa,       -- R/W address[0] input bit
      D => ram_di(i),         -- Write 1-bit data input
      DPRA => ram_ra, -- Read-only address[0] input bit
      WCLK => clk125,   -- Write clock input
      WE => we_ram        -- Write enable input
   );
end generate;
ram_ra <= ipb_addr(15) & ipb_addr(5 downto 0);
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		toggle <= not toggle;
	end if;
end process;
process(clk125)
begin
	if(clk125'event and clk125 = '1')then
		toggle_q <= toggle;
		ec_rdata <= toggle xor toggle_q;
		if(DB_cmd = '1' and ec_wdata = '1')then
			DB_cmd_l <= '1';
		elsif(sr(5) = '1' and ram_wa(5 downto 0) = "100111")then -- high word of the last counter
			DB_cmd_l <= '0';
		end if;
		if(sr(5) = '1' and ram_wa(5 downto 0) = "100111")then -- high word of the last counter
			DB_en <= DB_cmd_l;
		end if;
		if(rst_cntr = '1')then
			CntrRstCycle <= '1';
		elsif(ram_wa = "1111111")then
			CntrRstCycle <= '0';
		end if;
		if(rst_cntr = '1')then
			ram_wa <= (others => '0');
		elsif(CntrRstCycle = '1')then
			ram_wa <= ram_wa + 1;
		elsif(sr(0) = '1')then
			ram_wa <= '0' & channel & '0';
		elsif(sr(2) = '1' or sr(5) = '1')then
			ram_wa(6) <= '1';
		elsif(sr(3) = '1')then
			ram_wa(0) <= '1';
			ram_wa(6) <= '0';
		end if;
		if(rst_cntr = '1')then
			sr <= "000000";
		elsif(sr(4 downto 0) = "00000")then
			sr <= "000001";
		else
			sr <= sr(4 downto 0) & '0';
		end if;
		if(sr(2) = '1')then
			carry <= not ram_di(31) and ram_spo(31);
		end if;
		if(CntrRstCycle = '1')then
			ram_di <= (others => '0');
		elsif(sr(0) = '1')then
			ram_di(8 downto 0) <= cntr;
		elsif(sr(1) = '1')then
			if(ram_spo(8 downto 0) > ram_di(8 downto 0))then
				ram_di(31 downto 9) <= ram_spo(31 downto 9) + 1;
			else
				ram_di(31 downto 9) <= ram_spo(31 downto 9);
			end if;
		elsif(sr(4) = '1')then
			ram_di <= x"0000" & (ram_spo(15 downto 0) + carry);
		end if;
		we_ram <= CntrRstCycle or sr(1) or (sr(2) and DB_en) or sr(4) or (sr(5) and DB_en);
		if(ipb_addr(14 downto 6) /= misc_cntr_addr(14 downto 6))then
			ipb_rdata <= (others => '0');
		elsif(ec_rdata = '1')then
			ipb_rdata <= ram_dpo;
		end if;
	end if;
end process;
i_ec_wdata : SRL16E
   generic map (
      INIT => X"0000")
   port map (
      Q => ec_wdata,       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '0',     -- Select[1] input
      A2 => '0',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => clk125,   -- Clock input
      D => ec_rdata        -- SRL data input
   );
end Behavioral;

