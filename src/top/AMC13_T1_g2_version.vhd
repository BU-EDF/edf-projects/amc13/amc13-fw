--! Version package for variant versions

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package amc13_version_package is

  constant device_ID	 		: std_logic_vector(15 downto 0) := x"DCC3";
  constant T2_version			: std_logic_vector(15 downto 0) := x"0012";
  constant K7version			: std_logic_vector(15 downto 0) := x"8142";
  constant V6version			: std_logic_vector(15 downto 0) := x"0026";
  constant CTRversion			: std_logic_vector(7 downto 0) := x"14";
  constant key		        	: std_logic_vector(31 downto 0) := x"dcc32012";
  constant flavor               : string := "G2"; --! from amc13_version_package
  constant lsc_speed            : integer := 10; --! from amc13_version_package

end package amc13_version_package;
