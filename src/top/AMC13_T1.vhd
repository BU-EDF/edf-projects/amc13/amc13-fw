----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:49:29 05/12/2010 
-- Design Name: 
-- Module Name:    DTC_T2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_misc.all;
use work.ipbus.ALL;
use work.amc13_pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity AMC13_T1 is
    Port (
           VAUXP : in  STD_LOGIC_VECTOR(12 downto 0);
           VAUXN : in  STD_LOGIC_VECTOR(12 downto 0);
-- I2C interface
           CLK_SCL : out  STD_LOGIC;
           CLK_SDA : inout  STD_LOGIC;
           SFP_SCL : out  STD_LOGIC_VECTOR(3 downto 0);
           SFP_SDA : inout  STD_LOGIC_VECTOR(3 downto 0);
-- SFP slow control
           SFP_LOS : in  STD_LOGIC_VECTOR(2 downto 0);
           SFP_ABS : in  STD_LOGIC_VECTOR(3 downto 0);
           TxFault : in  STD_LOGIC_VECTOR(3 downto 0);
           TxDisable : out  STD_LOGIC_VECTOR(3 downto 0);
--           RATE : out  STD_LOGIC_VECTOR(3 downto 0);
-- CDR signals
           DIV4 : out  STD_LOGIC;
           DIV_nRST : out  STD_LOGIC;
           CDRclk_p : in  STD_LOGIC;
           CDRclk_n : in  STD_LOGIC;
           CDRdata_p : in  STD_LOGIC;
           CDRdata_n : in  STD_LOGIC;
           TTCdata_p : out  STD_LOGIC;
           TTCdata_n : out  STD_LOGIC;
           TTCclk_p : in  STD_LOGIC;
           TTCclk_n : in  STD_LOGIC;
           TTC_LOS : in  STD_LOGIC;
           TTC_LOL : in  STD_LOGIC;
           TTS_out_p : out  STD_LOGIC;
           TTS_out_n : out  STD_LOGIC;
-- SPI interface
           SPI_SCK : in  STD_LOGIC;
           SPI_CS_b : in  STD_LOGIC;
           SPI_MOSI : in  STD_LOGIC;
           SPI_MISO : out  STD_LOGIC;
-- DDR3 pins
           sys_clk_p : in  STD_LOGIC;
           sys_clk_n : in  STD_LOGIC;
           ddr3_dq : inout  STD_LOGIC_VECTOR(31 downto 0);
           ddr3_addr : out  STD_LOGIC_VECTOR(13 downto 0);
           ddr3_ba : out  STD_LOGIC_VECTOR (2 downto 0);
           ddr3_dm : out  STD_LOGIC_VECTOR (3 downto 0);
           ddr3_dqs_p : inout  STD_LOGIC_VECTOR(3 downto 0);
           ddr3_dqs_n : inout  STD_LOGIC_VECTOR(3 downto 0);
           ddr3_ras_n : out  STD_LOGIC;
           ddr3_cas_n : out  STD_LOGIC;
           ddr3_we_n : out  STD_LOGIC;
           ddr3_reset_n : out  STD_LOGIC;
           ddr3_cke : out  STD_LOGIC_vector(0 to 0);
           ddr3_odt : out  STD_LOGIC_vector(0 to 0);
           ddr3_ck_p : out  STD_LOGIC_vector(0 to 0);
           ddr3_ck_n : out  STD_LOGIC_vector(0 to 0);
           SFP0_RXN : in  STD_LOGIC;
           SFP0_RXP : in  STD_LOGIC;
           SFP1_RXN : in  STD_LOGIC;
           SFP1_RXP : in  STD_LOGIC;
           SFP2_RXN : in  STD_LOGIC;
           SFP2_RXP : in  STD_LOGIC;
           SFP0_TXN : out  STD_LOGIC;
           SFP0_TXP : out  STD_LOGIC;
           SFP1_TXN : out  STD_LOGIC;
           SFP1_TXP : out  STD_LOGIC;
           SFP2_TXN : out  STD_LOGIC;
           SFP2_TXP : out  STD_LOGIC;
           CDR_REFCLK_N : in  STD_LOGIC;
           CDR_REFCLK_P : in  STD_LOGIC;
           SFP_REFCLK_N : in  STD_LOGIC;
           SFP_REFCLK_P : in  STD_LOGIC;
           AMC_REFCLK_N : in  STD_LOGIC;
           AMC_REFCLK_P : in  STD_LOGIC;
           AMC_RXN : in  STD_LOGIC_VECTOR(12 downto 1);
           AMC_RXP : in  STD_LOGIC_VECTOR(12 downto 1);
           AMC_TXN : out  STD_LOGIC_VECTOR(12 downto 1);
           AMC_TXP : out  STD_LOGIC_VECTOR(12 downto 1);
-- signal to/from DTC_T2
           S6LINK_RXN : in  STD_LOGIC;
           S6LINK_RXP : in  STD_LOGIC;
           S6LINK_TXN : out  STD_LOGIC;
           S6LINK_TXP : out  STD_LOGIC;
           S2V_p : in  STD_LOGIC;
           S2V_n : in  STD_LOGIC;
--           V2S_p : out  STD_LOGIC;
--           V2S_n : out  STD_LOGIC;
           GbE_REFCLK_N : in  STD_LOGIC;
           GbE_REFCLK_P : in  STD_LOGIC);
end AMC13_T1;

architecture Behavioral of AMC13_T1 is
COMPONENT HCAL_trig
PORT(
	TTC_clk : IN std_logic;
	DRPCLK : IN std_logic;
	reset : IN std_logic;
	SFP_ABS : in  STD_LOGIC;
	BC0 : in  STD_LOGIC;
	BC0_dl : out  STD_LOGIC;
	triggerOut : out  STD_LOGIC;
	Trigdata : in  array12x8;
	TTC_lock : in  STD_LOGIC;
	AMC_en	: in  STD_LOGIC_VECTOR(11 downto 0);
	BC0_lock	: in  STD_LOGIC_VECTOR(11 downto 0);
	BX_offset2SC : out  STD_LOGIC_VECTOR(11 downto 0);
	ipb_clk : IN std_logic;
	ipb_write : IN std_logic;
	ipb_strobe : IN std_logic;
	ipb_addr : IN std_logic_vector(31 downto 0);
	ipb_wdata : IN std_logic_vector(31 downto 0);
	GTX_REFCLKp : IN std_logic;
	GTX_REFCLKn : IN std_logic;
	GTX_RXp : IN std_logic;
	GTX_RXn : IN std_logic;          
	ipb_rdata : OUT std_logic_vector(31 downto 0);
	GTX_TXp : OUT std_logic;
	GTX_TXn : OUT std_logic
	);
END COMPONENT;
COMPONENT TTS_if
	PORT(
		sysclk : IN std_logic;
		TTS_clk : IN std_logic;
		reset : IN std_logic;
		local_TTC : in  STD_LOGIC;
		TTS : IN std_logic_vector(3 downto 0);
		TTS_out_p : OUT std_logic;
		TTS_out_n : OUT std_logic
		);
END COMPONENT;
COMPONENT ttc_if
	PORT(
    clk : IN std_logic;
    refclk : IN std_logic;
    reset : IN std_logic;
    rst_PLL : IN std_logic;
    run : IN std_logic;
    IgnoreDAQ : IN std_logic;
		DB_cmd_in : IN std_logic;
		DB_cmd_Out : OUT std_logic;
    sys_lock : IN std_logic;
    local_TTC : IN std_logic;
    local_TTCcmd : IN std_logic;
    single_TTCcmd : in  STD_LOGIC;
    CDRclk_p : IN std_logic;
    CDRclk_n : IN std_logic;
    CDRdata_p : IN std_logic;
    CDRdata_n : IN std_logic;
    TTC_LOS : IN std_logic;
    TTC_LOL : IN std_logic;
    BCN_off : IN std_logic_vector(12 downto 0);
    OC_off : IN std_logic_vector(3 downto 0);
    en_cal_win : IN std_logic;
    trig_BX : IN std_logic_vector(12 downto 0);
    cal_win_high : IN std_logic_vector(11 downto 0);
    cal_win_low : IN std_logic_vector(11 downto 0);
    en_localL1A : IN std_logic;
    LocalL1A_cfg : IN std_logic_vector(31 downto 0);
    localL1A_s : IN std_logic;
    localL1A_r : IN std_logic;
    T3_trigger : IN std_logic;
    HCAL_trigger : IN std_logic;
    EvnRSt_l : IN std_logic;
    OcnRSt_l : IN std_logic;
    ovfl_warning : IN std_logic;
    ipb_clk : IN std_logic;
    ipb_write : IN std_logic;
    ipb_strobe : IN std_logic;
    ipb_addr : IN std_logic_vector(31 downto 0);
    ipb_wdata : IN std_logic_vector(31 downto 0);
    en_brcst : IN std_logic;
    state : IN std_logic_vector(3 downto 0);
    evn_fifo_full : IN std_logic;          
    BC0 : OUT std_logic;
    TTC_strobe : OUT std_logic;
    TTS_clk : OUT std_logic;
    DIV4 : OUT std_logic;
    DIV_nRST : OUT std_logic;
    CDRclk_out : OUT std_logic;
    TTCdata_p : OUT std_logic;
    TTCdata_n : OUT std_logic;
    CalType : OUT std_logic_vector(3 downto 0);
    TTC_Brcst : OUT std_logic_vector(3 downto 0);
    localL1A_periodic : OUT std_logic;
    ipb_rdata : OUT std_logic_vector(31 downto 0);
    ttc_start : OUT std_logic;
    ttc_stop : OUT std_logic;
    ttc_soft_reset : OUT std_logic;
    ttc_ready : OUT std_logic;
    ttc_serr : OUT std_logic;
    ttc_derr : OUT std_logic;
    ttc_bcnt_err : OUT std_logic;
    rate_OFW : OUT std_logic;
    sync_lost : OUT std_logic;
    inc_oc : OUT std_logic;
    inc_l1ac : OUT std_logic;
    inc_bcnterr : OUT std_logic;
    inc_serr : OUT std_logic;
    inc_derr : OUT std_logic;
    ttc_evcnt_reset : OUT std_logic;
    event_number_avl : OUT std_logic;
    event_number : OUT std_logic_vector(59 downto 0)
    );
END COMPONENT;
COMPONENT AMC_if
	Generic (simulation : boolean := false);
	PORT(
		sysclk : IN std_logic;
		ipb_clk : IN std_logic;
		clk125 : IN std_logic;
		DRPclk : IN std_logic;
		GTXreset : IN std_logic;
		reset : IN std_logic;
		DB_cmd : IN std_logic;
		resetCntr : IN std_logic;
		ReSync : IN std_logic;
		AllEventBuilt : OUT std_logic;
		run : IN std_logic;
		en_inject_err : in STD_LOGIC;
		Dis_pd : in  STD_LOGIC;
		enSFP : IN std_logic_vector(3 downto 0);
		en_localL1A : IN std_logic;
		test : IN std_logic;
		NoReSyncFake : IN std_logic;
		WaitMonBuf : IN std_logic;
		fake_length : IN std_logic_vector(19 downto 0);
		T1_version : IN std_logic_vector(7 downto 0);
		Source_ID : IN array3x12;
		AMC_en : IN std_logic_vector(11 downto 0);
		TTS_disable : IN std_logic_vector(11 downto 0);
		AMC_REFCLK_P : IN std_logic;
		AMC_REFCLK_N : IN std_logic;
		AMC_RXN : IN std_logic_vector(12 downto 1);
		AMC_RXP : IN std_logic_vector(12 downto 1);
		evt_data_re : IN std_logic_vector(2 downto 0);
		evt_buf_full : IN std_logic_vector(2 downto 0);
		ddr_pa : IN std_logic_vector(9 downto 0);
		MonBuf_empty : IN std_logic;
		mon_buf_avl : IN std_logic;
		TCPbuf_avl : IN std_logic;
		ipb_write : IN std_logic;
		ipb_strobe : IN std_logic;
		ipb_addr : IN std_logic_vector(31 downto 0);
		ipb_wdata : IN std_logic_vector(31 downto 0);
		TTC_clk : IN std_logic;
		TTC_LOS : IN std_logic;
		BC0 : IN std_logic;
		ttc_evcnt_reset : IN std_logic;
		event_number_avl : IN std_logic;
		event_number : IN std_logic_vector(59 downto 0);          
		AMC_Ready : OUT std_logic_vector(11 downto 0);
		TTC_lock : OUT std_logic;
		BC0_lock : OUT std_logic_vector(11 downto 0);
		AMC_TXN : OUT std_logic_vector(12 downto 1);
		AMC_TXP : OUT std_logic_vector(12 downto 1);
		AMC_status : OUT std_logic_vector(31 downto 0);
		evt_data : OUT array3x67;
		evt_data_we : OUT std_logic_vector(2 downto 0);
		evt_data_rdy : OUT std_logic_vector(2 downto 0);
		mon_evt_wc : OUT std_logic_vector(47 downto 0);
		mon_ctrl : OUT std_logic_vector(31 downto 0);
		buf_rqst : OUT std_logic_vector(3 downto 0);
		ipb_rdata : OUT std_logic_vector(31 downto 0);
		ipb_ack : OUT std_logic;
		evn_buf_full : OUT std_logic;
		ovfl_warning : OUT std_logic;
		TrigData : OUT array12x8;
		TTS_RQST : OUT std_logic_vector(2 downto 0);
		TTS_coded : OUT std_logic_vector(4 downto 0)
		);
END COMPONENT;
COMPONENT I2C
	PORT(
		clk : IN std_logic;
		ipb_clk : IN std_logic;
		reset : IN std_logic;
		addr : IN std_logic_vector(31 downto 0);
		SFP_ABS : IN std_logic_vector(3 downto 0);
		SFP_LOS : IN std_logic_vector(2 downto 0);    
		CLK_SDA : INOUT std_logic;
		SFP_SDA : INOUT std_logic_vector(3 downto 0);      
		rdata : OUT std_logic_vector(31 downto 0);
		CLK_rdy : OUT std_logic;
		CLK_SCL : OUT std_logic;
		SFP_SCL : OUT std_logic_vector(3 downto 0)
		);
END COMPONENT;
COMPONENT SPI_if
	PORT(
		SCK : IN std_logic;
		CSn : IN std_logic;
		MOSI : IN std_logic;
		SN : IN std_logic_vector(8 downto 0);
		OT : IN std_logic;
		IsT1 : IN std_logic;
		SPI_rdata : IN std_logic_vector(7 downto 0);          
		MISO : OUT std_logic;
		SPI_we : OUT std_logic;
    en_RARP : out  STD_LOGIC;
		newIPADDR : OUT std_logic;
		IPADDR : OUT std_logic_vector(31 downto 0);
		SPI_wdata : OUT std_logic_vector(7 downto 0);
		SPI_addr : OUT std_logic_vector(7 downto 0)
		);
END COMPONENT;
COMPONENT ddr_if
	port(
		clk_ref     												: in    std_logic;
		mem_clk_p    												: in    std_logic;
		mem_clk_n    												: in    std_logic;
		mem_rst		   												: in    std_logic;
    sysclk	                            : in   std_logic;
    TCPclk	                            : in   std_logic;
    reset 	                            : in   std_logic;
    resetsys 	                          : in   std_logic;
    run			                           	: in   std_logic;
    mem_test	                          : in   std_logic_VECTOR(1 downto 0);
    EventData	                          : in   array3X67;
    EventData_we                        : in   std_logic_VECTOR(2 downto 0);
    wport_rdy	                        	: out  std_logic_VECTOR(2 downto 0);
		WrtMonBlkDone												: OUT std_logic_VECTOR(2 downto 0);
		WrtMonEvtDone												: OUT std_logic_VECTOR(2 downto 0);
		KiloByte_toggle											: OUT std_logic_VECTOR(2 downto 0);
		EoB_toggle													: OUT std_logic_VECTOR(2 downto 0);
    EventBufAddr                        : in   array3x14;
    EventBufAddr_we	                    : in   std_logic_VECTOR(2 downto 0);
    EventFIFOfull		                    : out   std_logic_VECTOR(2 downto 0);
    TCP_din 														: in  std_logic_vector(31 downto 0);
    TCP_channel													: in  STD_LOGIC_VECTOR (1 downto 0);
    TCP_we 															: in  STD_LOGIC;
		TCP_wcount 													: out  STD_LOGIC_VECTOR (2 downto 0);
		TCP_dout														: out  STD_LOGIC_VECTOR(31 downto 0); -- TCP data are written in unit of 32-bit words
		TCP_dout_type												: out  STD_LOGIC_VECTOR(2 downto 0); -- TCP data destination
		TCP_raddr														: in  std_logic_vector(28 downto 0); -- 28-26 encoded request source 25-0 address in 64 bit word
		TCP_length													: in  std_logic_vector(12 downto 0); -- in 64 bit word, actual length - 1
		TCP_dout_valid											: out  STD_LOGIC;
		TCP_rrqst														:	in  STD_LOGIC;
		TCP_rack														: out  STD_LOGIC;
		TCP_lastword												: out  STD_LOGIC;
		cs_out															: out  STD_LOGIC_VECTOR(511 downto 0);
--	ipbus signals
    ipb_clk															: in  STD_LOGIC;
    ipb_write														: in  STD_LOGIC;
    ipb_strobe													: in  STD_LOGIC;
    page_addr														: in  STD_LOGIC_VECTOR(9 downto 0);
    ipb_addr														: in  STD_LOGIC_VECTOR(31 downto 0);
    ipb_wdata														: in  STD_LOGIC_VECTOR(31 downto 0);
    ipb_rdata														: out  STD_LOGIC_VECTOR(31 downto 0);
    ipb_ack															: out  STD_LOGIC;
		mem_stat 														: out  STD_LOGIC_VECTOR (63 downto 0);
    device_temp													: in  STD_LOGIC_VECTOR(11 downto 0);
-- ddr3 memory pins
		ddr3_dq 														: inout  STD_LOGIC_VECTOR (31 downto 0);
		ddr3_dm 														: out  STD_LOGIC_VECTOR (3 downto 0);
		ddr3_addr 													: out  STD_LOGIC_VECTOR (13 downto 0);
		ddr3_ba 														: out  STD_LOGIC_VECTOR (2 downto 0);
		ddr3_dqs_p 													: inout  STD_LOGIC_VECTOR (3 downto 0);
		ddr3_dqs_n 													: inout  STD_LOGIC_VECTOR (3 downto 0);
		ddr3_ras_n 													: out  STD_LOGIC;
		ddr3_cas_n 													: out  STD_LOGIC;
		ddr3_we_n 													: out  STD_LOGIC;
		ddr3_reset_n 												: out  STD_LOGIC;
		ddr3_cke 														: out  STD_LOGIC_vector(0 to 0);
		ddr3_odt 														: out  STD_LOGIC_vector(0 to 0);
		ddr3_ck_p 													: out  STD_LOGIC_vector(0 to 0);
		ddr3_ck_n 													: out  STD_LOGIC_vector(0 to 0)
		);
END COMPONENT;
COMPONENT ipbus_if
	generic(RXPOLARITY : std_logic := '0'; TXPOLARITY : std_logic := '0');
	port(
		ipb_clk																	: IN std_logic;
		UsRclk																	: IN std_logic;
		DRPclk																	: IN std_logic;
		got_SN																	: out std_logic;
		reset 																	: IN std_logic;
		GTX_RESET															  : IN std_logic;
    GbE_REFCLK	                       	  	: in   std_logic;
    S6LINK_RXN                            	: in   std_logic;
    S6LINK_RXP                            	: in   std_logic;
    S6LINK_TXN                            	: out  std_logic;
    S6LINK_TXP                            	: out  std_logic;
		wr_amc_en																: in std_logic;
    amc_en																	: in  STD_LOGIC_VECTOR(11 downto 0);
		en_RARP																	: in std_logic;
    IPADDR																	: in  STD_LOGIC_VECTOR(31 downto 0);
    MACADDR																	: in  STD_LOGIC_VECTOR(47 downto 0);
	  ipb_out																	: out ipb_wbus;
	  ipb_in																	: in ipb_rbus;
    SN																			: out  STD_LOGIC_VECTOR(8 downto 0);
		debug_in																: IN std_logic_vector(31 downto 0);
		debug_out																: OUT std_logic_vector(127 downto 0)
		);
end COMPONENT;
COMPONENT sysmon_if
	PORT(
		DRPclk : IN std_logic;
		DB_cmd : IN std_logic;
		SN : IN std_logic_vector(8 downto 0);
		VAUXN_IN : IN std_logic_vector(12 downto 0);
		VAUXP_IN : IN std_logic_vector(12 downto 0);
		addr : IN std_logic_vector(15 downto 0);          
		data : OUT std_logic_vector(31 downto 0);
		device_temp : OUT std_logic_vector(11 downto 0);
		ALM : OUT std_logic_vector(7 downto 0);
		OT : OUT std_logic
		);
END COMPONENT;
COMPONENT DAQLSCXG_if
	PORT(
		sysclk : IN std_logic;
		clk125 : IN std_logic;
		DRPclk : IN std_logic;
		reset : IN std_logic;
		daq_reset : IN std_logic;
		gtx_reset : IN std_logic;
		rstCntr : IN std_logic;
		Dis_pd : in  STD_LOGIC;
		test : IN std_logic;
		DB_cmd : IN std_logic;
		enSFP : IN std_logic_vector(3 downto 0);
		SFP_ABS : IN std_logic_vector(2 downto 0);
		LSC_ID : IN std_logic_vector(15 downto 0);
		inc_ddr_pa : IN std_logic;
		evt_data_rdy : IN std_logic_vector(2 downto 0);
		EventData_in : IN array3x67;
		EventData_we : IN std_logic_vector(2 downto 0);
		buf_rqst : IN std_logic_vector(3 downto 0);
		WaitMonBuf : IN std_logic;
		WrtMonBlkDone : IN std_logic_vector(2 downto 0);
		WrtMonEvtDone : IN std_logic_vector(2 downto 0);
		wport_rdy : IN std_logic_vector(2 downto 0);
		wport_FIFO_full : IN std_logic_vector(2 downto 0);
		SFP0_RXN : IN std_logic;
		SFP0_RXP : IN std_logic;
		SFP1_RXN : IN std_logic;
		SFP1_RXP : IN std_logic;
		SFP2_RXN : IN std_logic;
		SFP2_RXP : IN std_logic;
		SFP_REFCLK_P : in std_logic;
		SFP_REFCLK_N : in std_logic;
		GbE_REFCLK : in std_logic;
		ipb_clk : IN std_logic;
		ipb_write : IN std_logic;
		ipb_strobe : IN std_logic;
		ipb_addr : IN std_logic_vector(31 downto 0);
		ipb_wdata : IN std_logic_vector(31 downto 0);          
		SFP_down : OUT std_logic_vector(2 downto 0);
		LinkFull : OUT std_logic_vector(2 downto 0);
		EventData_re : OUT std_logic_vector(2 downto 0);
		evt_buf_full : OUT std_logic_vector(2 downto 0);
		MonBufOverWrite : IN std_logic;
		MonBuf_avl : OUT std_logic;
    TCPBuf_avl : out  STD_LOGIC;
		MonBuf_empty : OUT std_logic;
		MonBufOvfl : OUT std_logic;
		mon_evt_cnt : OUT std_logic_vector(31 downto 0);
		EventBufAddr_we : OUT std_logic_vector(2 downto 0);
		EventBufAddr : OUT array3x14;
		SFP0_TXN : OUT std_logic;
		SFP0_TXP : OUT std_logic;
		SFP1_TXN : OUT std_logic;
		SFP1_TXP : OUT std_logic;
		SFP2_TXN : OUT std_logic;
		SFP2_TXP : OUT std_logic;
		ipb_rdata : OUT std_logic_vector(31 downto 0);
		ipb_ack : OUT std_logic
		);
END COMPONENT;
COMPONENT TCPIP_if
		generic (simulation : boolean := false; en_KEEPALIVE : std_logic := '1');
    Port ( sysclk : in  STD_LOGIC;
           DRPclk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           TCPreset : in  STD_LOGIC;
           rstCntr : in  STD_LOGIC;
           test : in  STD_LOGIC;
					 SN : IN std_logic_vector(8 downto 0);
           Dis_pd : in  STD_LOGIC;
					 enSFP : IN std_logic_vector(3 downto 0);
					 SFP_down : OUT std_logic_vector(2 downto 0);
           inc_ddr_pa : in  STD_LOGIC;
-- event data in
					 evt_data_rdy : in std_logic_vector(2 downto 0);
					 EventData_in : in array3X67;
					 EventData_we : in   std_logic_VECTOR(2 downto 0);
					 EventData_re : out   std_logic_VECTOR(2 downto 0); -- 
					 evt_buf_full : out std_logic_vector(2 downto 0);
					 buf_rqst : in std_logic_vector(3 downto 0);
					 WaitMonBuf : IN std_logic;
           MonBufOverWrite : in  STD_LOGIC;
           TCPBuf_avl : out  STD_LOGIC;
           MonBuf_avl : out  STD_LOGIC;
           MonBuf_empty : out  STD_LOGIC;
           MonBufOvfl : out  STD_LOGIC;
					 mon_evt_cnt : out std_logic_vector(31 downto 0);
					 WrtMonBlkDone : in  STD_LOGIC_VECTOR(2 downto 0);
					 WrtMonEvtDone : in  STD_LOGIC_VECTOR(2 downto 0);
					 KiloByte_toggle : in  STD_LOGIC_VECTOR(2 downto 0);
					 EoB_toggle : in  STD_LOGIC_VECTOR(2 downto 0);
-- ddr wportA status
					 wport_rdy : in std_logic_vector(2 downto 0);
					 wport_FIFO_full : in std_logic_vector(2 downto 0);
-- signal to ddr_if, AMC_if to start moving data
					 EventBufAddr_we : out std_logic_VECTOR(2 downto 0);
					 EventBufAddr : out array3X14;
-- ddr wportB signals in sysclk domain
					 TCPclk : out STD_LOGIC;
					 TCP_dout : out std_logic_vector(31 downto 0); -- TCP data are written in unit of 32-bit words
					 TCP_channel : out std_logic_vector(1 downto 0); -- Each entry has four 32bit words, each address saves two entries. Addresses are kept in ddr_wportB
					 TCP_we : out STD_LOGIC;
					 TCP_wcount : in  STD_LOGIC_VECTOR (2 downto 0);
-- ddr rport signals
					 TCP_raddr : out  STD_LOGIC_VECTOR(28 downto 0); -- 28-26 encoded request source 25-0 address in 64 bit word
					 TCP_length	: out  STD_LOGIC_VECTOR(12 downto 0); -- in 64 bit word, actual length - 1
					 TCP_rrqst : out  STD_LOGIC;
					 TCP_rack	: in  STD_LOGIC;
					 TCP_din	: in  STD_LOGIC_VECTOR(31 downto 0); -- TCP data are written in unit of 32-bit words
					 TCP_din_type	: in  STD_LOGIC_VECTOR(2 downto 0); -- TCP data destination
					 TCP_din_valid	: in  STD_LOGIC;
					 TCP_lastword	: in  STD_LOGIC;
-- SFP ports
           SFP0_RXN : in  STD_LOGIC;
           SFP0_RXP : in  STD_LOGIC;
           SFP1_RXN : in  STD_LOGIC;
           SFP1_RXP : in  STD_LOGIC;
           SFP2_RXN : in  STD_LOGIC;
           SFP2_RXP : in  STD_LOGIC;
           SFP0_TXN : out  STD_LOGIC;
           SFP0_TXP : out  STD_LOGIC;
           SFP1_TXN : out  STD_LOGIC;
           SFP1_TXP : out  STD_LOGIC;
           SFP2_TXN : out  STD_LOGIC;
           SFP2_TXP : out  STD_LOGIC;
           SFP_REFCLK_N : in  STD_LOGIC;
           SFP_REFCLK_P : in  STD_LOGIC;
					 cs_out : out  STD_LOGIC_VECTOR(511 downto 0);
--	ipbus signals
					 ipb_clk : in  STD_LOGIC;
					 ipb_write : in  STD_LOGIC;
					 ipb_strobe : in  STD_LOGIC;
					 ipb_addr	: in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_wdata : in  STD_LOGIC_VECTOR(31 downto 0);
					 ipb_rdata : out  STD_LOGIC_VECTOR(31 downto 0)
					 );
END COMPONENT;
COMPONENT TTC_cntr
	PORT(
		sysclk : IN std_logic;
		clk125 : IN std_logic;
		ipb_clk : IN std_logic;
		reset : IN std_logic;
		rst_cntr : IN std_logic;
		DB_cmd : IN std_logic;
		inc_serr : IN std_logic;
		inc_derr : IN std_logic;
		inc_bcnterr : IN std_logic;
		inc_l1ac : IN std_logic;
		run : IN std_logic;
		state : IN std_logic_vector(3 downto 0);
		ttc_resync : IN std_logic;
		ipb_addr : IN std_logic_vector(15 downto 0);          
		ipb_rdata : OUT std_logic_vector(31 downto 0)
		);
END COMPONENT;
constant ipbus_ver_addr : std_logic_vector(15 downto 0) := x"0000";
constant ipbus_sfp_addr: std_logic_vector(15 downto 0) := x"0002";
constant CDRclk_pol : std_logic := '0';
constant CDRdata_pol : std_logic := '1';
constant TTCclk_pol : std_logic := '1';
constant TTCdata_pol : std_logic := '1';
constant Coarse_Delay: std_logic_vector(3 downto 0) := x"0";
signal rst_ipbus : std_logic := '0';
signal LDC_UsrClk : std_logic := '0';
signal wr_AMC_en : std_logic := '0';
signal wr_EnSFP : std_logic := '0';
signal fake_length : std_logic_vector(19 downto 0) := x"00400";
signal AMC_en : std_logic_vector(11 downto 0) := (others =>'0');
signal TTS_disable : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_Ready : std_logic_vector(11 downto 0) := (others =>'0');
signal TTC_lock : std_logic := '0';
signal BC0_lock : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_status : std_logic_vector(31 downto 0) := (others =>'0');
signal AMC_DATA : std_logic_vector(31 downto 0) := (others =>'0');
signal AMC_ack : std_logic := '0';
signal L1Aovfl_warning : std_logic := '0';
signal HCAL_trigger : std_logic := '0';
signal TRIGDATA : array12x8 := (others => (others => '0'));
signal TTS_coded : std_logic_vector(4 downto 0) := (others =>'0');
signal TTS_RQST : std_logic_vector(2 downto 0) := (others =>'0');
signal pattern : std_logic_vector(3 downto 0) := (others =>'0');
--signal Trig_mask : std_logic_vector(7 downto 0) := (others =>'0');
signal SPI_SCK_buf : std_logic := '0';
signal CLK_rdy : std_logic := '0';
signal I2C_data : std_logic_vector(31 downto 0) := (others =>'0');
signal TTCclk_in : std_logic := '0';
signal TTC_Clk : std_logic := '0';
signal TTC_strobe : std_logic := '0';
signal BcntErr_cnt : std_logic_vector(7 downto 0) := (others =>'0');
signal SinErr_cnt : std_logic_vector(7 downto 0) := (others =>'0');
signal DbErr_cnt : std_logic_vector(7 downto 0) := (others =>'0');
signal L1_reg : std_logic_vector(15 downto 0) := (others =>'0');
signal Bcnt_reg : std_logic_vector(11 downto 0) := (others =>'0');
signal OC_reg : std_logic_vector(31 downto 0) := (others =>'0');
signal DB_cmd : std_logic := '0';
signal V2S : std_logic := '0';
signal S2V : std_logic := '0';
signal S2V_cntr : std_logic_vector(5 downto 0) := (others => '0');
signal S2V_sr : std_logic_vector(3 downto 0) := (others => '0');
signal ddr_rdata : std_logic_vector(7 downto 0) := (others =>'0');
signal ipb_clk_dcm : std_logic := '0';
signal ipb_clk : std_logic := '0';
signal clk125_dcm : std_logic := '0';
signal clk125 : std_logic := '0';
signal DRPclk_dcm : std_logic := '0';
signal DRPclk : std_logic := '0';
signal sysclk_dcm : std_logic := '0';
signal sysclk : std_logic := '0';
signal clkfb : std_logic := '0';
signal refclk_dcm : std_logic := '0';
signal refclk : std_logic := '0';
signal mem_clk_dcm : std_logic := '0';
signal mem_clk : std_logic := '0';
signal sysclk_inp : std_logic := '0';
signal sysclk_in : std_logic := '0';
--signal clk125 : std_logic := '0';
signal sys_lock : std_logic := '0';
signal sys_lock_n : std_logic := '0';
signal ldc_reset : std_logic := '0';
signal ldc_GTXreset : std_logic := '0';
signal lsc_start : std_logic := '0';
signal lsc_reset : std_logic := '0';
signal lsc_GTXreset : std_logic := '0';
signal amc_reset : std_logic := '0';
signal amc_GTXreset : std_logic := '0';
signal conf7_q : std_logic := '0';
signal conf7_fall : std_logic := '0';
signal run : std_logic := '0';
signal LSC_LinkDown : std_logic := '0';
signal mem_rst : std_logic := '0';
signal mem_test : std_logic_vector(1 downto 0) := (others =>'0');
signal mem_stat : std_logic_vector(63 downto 0) := (others =>'0');
signal mem_ack : std_logic := '0';
signal mem_data : std_logic_vector(31 downto 0) := (others =>'0');
signal EventData : array3X67 := (others => (others => '0'));
signal wport_rdy : std_logic_vector(2 downto 0) := (others =>'0');
signal EventBufAddr : array3x14 := (others => (others => '0'));
signal EventBufAddr_we : std_logic_vector(2 downto 0) := (others =>'0');
signal evt_buf_full : std_logic_vector(2 downto 0) := (others =>'0');
signal wport_FIFO_full : std_logic_vector(2 downto 0) := (others =>'0');
signal TCPclk : std_logic := '0';
signal TCP_din : std_logic_vector(31 downto 0) := (others =>'0');
signal TCP_channel : std_logic_vector(1 downto 0) := (others =>'0');
signal TCP_we : std_logic := '0';
signal TCP_wcount : std_logic_vector(2 downto 0) := (others =>'0');
signal TCP_dout : std_logic_vector(31 downto 0) := (others =>'0');
signal TCP_dout_type : std_logic_vector(2 downto 0) := (others =>'0');
signal TCP_raddr : std_logic_vector(28 downto 0) := (others =>'0');
signal TCP_length : std_logic_vector(12 downto 0) := (others =>'0');
signal TCP_dout_valid : std_logic := '0';
signal TCP_rrqst : std_logic := '0';
signal TCP_rack : std_logic := '0';
signal TCP_lastword : std_logic := '0';
signal TCPIP_GTXreset : std_logic := '0';
signal MonBufOvfl : std_logic := '0';
signal MonBuf_empty : std_logic := '0';
--signal inc_mon_cntr : std_logic := '0';
signal mon_evt_wc : std_logic_vector(47 downto 0) := (others =>'0');
signal mon_evt_cnt : std_logic_vector(31 downto 0) := (others =>'0');
signal mon_ctrl : std_logic_vector(31 downto 0) := (others =>'0');
signal TCPbuf_avl : std_logic := '0';
signal mon_buf_avl : std_logic := '0';
signal EventBufAddrAvl : std_logic := '0';
signal EventBufAddrRe : std_logic := '0';
signal mon_wp : std_logic_vector(31 downto 0) := (others =>'0');
--signal TCP_releaseAck : std_logic_vector(2 downto 0) := (others =>'0');
--signal TCP_releaseRqst : std_logic_vector(2 downto 0) := (others =>'0');
--signal TCP_releaseAddr : array3X13 := (others => (others => '0'));
signal EventBuf_rqst : std_logic_vector(3 downto 0) := (others =>'0');
signal rst_cntr : std_logic := '0';
signal rst_ddr_pa : std_logic := '0';
signal inc_ddr_pa : std_logic := '0';
signal Source_ID : array3X12 := (others => (others => '0'));
signal ddr_pa : std_logic_vector(9 downto 0) := (others =>'0');
signal CDRclk : std_logic := '0';
signal TTS_clk : std_logic := '0';
signal chk_lock : std_logic := '0';
signal chk_lock_q : std_logic := '0';
signal BC0 : std_logic := '0';
signal BC0_dl : std_logic := '0';
signal T3_trigger : std_logic := '0';
signal BX_offset2SC : std_logic_vector(11 downto 0) := (others =>'0');
signal bcnt : std_logic_vector(11 downto 0) := x"000";
signal LocalL1A_cfg : std_logic_vector(31 downto 0) := (others =>'0');
signal BCN_off : std_logic_vector(12 downto 0) := (others =>'0');
signal OC_off : std_logic_vector(3 downto 0) := (others =>'0');
signal en_cal_win : std_logic := '0';
signal CalibCtrl : std_logic_vector(31 downto 0) := x"0d800d80";
signal cal_win_high : std_logic_vector(11 downto 0) := (others =>'0');
signal cal_win_low : std_logic_vector(11 downto 0) := (others =>'0');
signal CalType : std_logic_vector(3 downto 0) := (others =>'0');
signal TTC_Brcst : std_logic_vector(3 downto 0) := (others =>'0');
signal local_TTCcmd : std_logic := '0';
--signal IsG2 : std_logic := '0';
signal en_brcst : std_logic := '0';
signal ttc_start : std_logic := '0';
signal ttc_stop : std_logic := '0';
signal ttc_soft_reset : std_logic := '0';
signal ttc_soft_resetp : std_logic := '0';
signal ttc_ready : std_logic := '0';
signal ttc_serr : std_logic := '0';
signal ttc_derr : std_logic := '0';
signal ttc_bcnt_err : std_logic := '0';
signal ttc_evcnt_reset : std_logic := '0';
signal inc_rate_ofw : std_logic := '0';
signal rate_ofw : std_logic := '0';
signal rate_ofwp : std_logic := '0';
signal rate_ofw_q : std_logic := '0';
signal sync_lost : std_logic := '0';
signal oc_cntr : std_logic_vector(3 downto 0) := (others =>'0');
signal trig_BX : std_logic_vector(12 downto 0) := "0000111110100";
signal ttc_resync : std_logic := '0';
signal AllEventBuilt : std_logic := '0';
signal dcc_quiet : std_logic := '0';
signal inc_oc : std_logic := '0';
signal inc_L1ac : std_logic := '0';
signal inc_bcnterr : std_logic := '0';
signal inc_serr : std_logic := '0';
signal inc_derr : std_logic := '0';
signal evn_fifo_full : std_logic := '0';
signal event_number_avl : std_logic := '0';
signal state : std_logic_vector(3 downto 0) := (others =>'0');
signal TTS_wait : std_logic_vector(20 downto 0) := (others =>'0');
signal event_number : std_logic_vector(59 downto 0) := (others =>'0');
signal status_l : std_logic_vector(22 downto 0) := (others =>'0');
signal SFP_down_l : std_logic_vector(2 downto 0) := (others =>'0');
signal SFP_status_l : std_logic_vector(11 downto 0) := (others =>'0');
signal AMC_status_l : std_logic_vector(31 downto 0) := (others =>'0');
signal TTC_cntr_data : std_logic_vector(31 downto 0) := (others => '0');
signal got_SN : std_logic := '0';
signal ipb_strobe_q : std_logic := '0';
signal SFP_clk : std_logic := '0';
signal AMC_clk : std_logic := '0';
signal AMC_clk_in : std_logic := '0';
signal SV_Cntr : std_logic_vector(7 downto 0) := (others => '0');
signal sysclk_div : std_logic_vector(7 downto 0) := (others => '0');
signal SFP_UsrClk : std_logic := '0';
signal SFP_TxOutClk : std_logic := '0';
signal I2C_debug_out : std_logic_vector(15 downto 0) := (others =>'0');
signal SFPOSC_rdy : std_logic := '0';
signal reset : std_logic := '0';
signal DAQ_reset : std_logic := '0';
signal AMCOSC_rdy : std_logic := '0';
--signal cs_clk_in : std_logic := '0';
--signal cs_clk : std_logic := '0';
signal TTC_debug : std_logic_vector(63 downto 0) := (others =>'0');
signal TxDisable_i : std_logic_vector(3 downto 0) := (others => '0');
signal DAQfifo_re : std_logic := '0';
signal DAQfifoAlmostEmpty : std_logic := '0';
signal DAQfifoEmpty : std_logic := '0';
signal DAQfifo_do : std_logic_vector(63 downto 0) := (others =>'0');
signal DAQ_debug_in : std_logic_vector(63 downto 0) := (others =>'0');
signal LDC_debug_out : std_logic_vector(63 downto 0) := (others =>'0');
signal LSC_debug_out : std_logic_vector(63 downto 0) := (others =>'0');
signal ddr_debug_in : std_logic_vector(31 downto 0) := (others =>'0');
signal ddr_debug_out : std_logic_vector(127 downto 0) := (others =>'0');
signal GbE_REFCLK : std_logic := '0';
signal S6Link_debug_in : std_logic_vector(31 downto 0) := (others =>'0');
signal S6Link_debug_out : std_logic_vector(127 downto 0) := (others =>'0');
signal GbE_debug_in : std_logic_vector(31 downto 0) := (others =>'0');
signal GbE_debug_out : std_logic_vector(127 downto 0) := (others =>'0');
signal AMC_debug_in : std_logic_vector(255 downto 0) := (others =>'0');
signal AMC_debug_out : std_logic_vector(255 downto 0) := (others =>'0');
signal SFP0_debug_in : std_logic_vector(31 downto 0) := (others =>'0');
signal SFP0_debug_out : std_logic_vector(127 downto 0) := (others =>'0');
signal SFP1_debug_in : std_logic_vector(31 downto 0) := (others =>'0');
signal SFP1_debug_out : std_logic_vector(127 downto 0) := (others =>'0');
signal ipb_master_out : ipb_wbus;
signal ipb_master_in : ipb_rbus;
signal SN : std_logic_vector(8 downto 0) := (others =>'0');
signal MACADDR : std_logic_vector(47 downto 0) := (others =>'0');
signal ipaddr : std_logic_vector(31 downto 0) := (others =>'0');
signal en_RARP : std_logic := '0';
--signal SPI_IP : std_logic_vector(31 downto 0) := (others =>'0');
signal status : std_logic_vector(31 downto 0) := (others =>'0');
signal cmd : std_logic_vector(31 downto 0) := (others =>'0');
signal cmd0_dl : std_logic_vector(1 downto 0) := (others =>'0');
signal cmd2_dl : std_logic_vector(1 downto 0) := (others =>'0');
signal conf : std_logic_vector(15 downto 0) := (others =>'0');
signal LSC_ID : std_logic_vector(15 downto 0) := x"1234";
signal OT : std_logic := '0';
signal inc_HTRCRC_err : std_logic := '0';
signal ttc_data : std_logic_vector(31 downto 0) := (others => '0');
signal sysmon_data : std_logic_vector(31 downto 0) := (others => '0');
signal HCAL_trig_data : std_logic_vector(31 downto 0) := (others => '0');
signal device_temp : std_logic_vector(11 downto 0) := (others =>'0');
signal ALM : std_logic_vector(7 downto 0) := (others =>'0');
signal evt_data_rdy : std_logic_vector(2 downto 0) := (others => '0');
signal evt_data_re : std_logic_vector(2 downto 0) := (others => '0');
signal evt_data_we : std_logic_vector(2 downto 0) := (others => '0');
--signal event_size : array3x13;
signal SFP_data : std_logic_vector(31 downto 0) := (others =>'0');
signal SFP_ack : std_logic := '0';
--signal TCP_data : std_logic_vector(31 downto 0) := (others =>'0');
--signal TCP_ack : std_logic := '0';
signal S2V_SyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal resetSyncRegs : std_logic_vector(2 downto 0) := (others => '0');
signal sysclk_div7SyncRegs : std_logic_vector(3 downto 0) := (others => '0');
signal resetCntr_SyncRegs : std_logic_vector(2 downto 0) := (others =>'0');
signal newIPADDR : std_logic := '0';
signal newIPADDRSyncRegs : std_logic_vector(2 downto 0) := (others =>'0');
signal DNA_out : std_logic := '0';
signal load_DNA : std_logic := '0';
signal shift_DNA : std_logic_vector(2 downto 0) := (others =>'0');
signal DNA_cntr : std_logic_vector(5 downto 0) := (others =>'0');
signal DNA : std_logic_vector(56 downto 0) := (others =>'0');
signal Dis_pd : std_logic := '0';
signal DAQ_bp : std_logic := '0';
signal IgnoreDAQ : std_logic := '0';
signal WaitMonBuf : std_logic := '0';
signal enSFP : std_logic_vector(3 downto 0) := (others =>'0');
signal SFP_down : std_logic_vector(2 downto 0) := (others =>'0');
signal LinkFull : std_logic_vector(2 downto 0) := (others =>'0');
signal WrtMonBlkDone : std_logic_vector(2 downto 0) := (others =>'0');
signal WrtMonEvtDone : std_logic_vector(2 downto 0) := (others =>'0');
signal KiloByte_toggle : std_logic_vector(2 downto 0) := (others =>'0');
signal EoB_toggle : std_logic_vector(2 downto 0) := (others =>'0');
signal Cntr2ms : std_logic_vector(18 downto 0) := (others => '0');
signal LiveTime : std_logic_vector(7 downto 0) := (others => '0');
signal LiveTime_l : std_logic_vector(7 downto 0) := (others => '0');
signal LiveTimeCntr : std_logic_vector(18 downto 0) := (others => '0');
signal DataRate : array3x19 := (others => (others => '0'));
signal DataRate_l : array3x19 := (others => (others => '0'));
signal DataRateCntr : array3x19 := (others => (others => '0'));
component icon2
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

end component;
component ila16x32k
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    DATA : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    TRIG0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0));

end component;
signal CONTROL0 : std_logic_vector(35 downto 0) := (others => '0');
signal CONTROL1 : std_logic_vector(35 downto 0) := (others => '0');
signal TRIG0 : std_logic_vector(7 downto 0) := (others => '0');
signal TRIG1 : std_logic_vector(7 downto 0) := (others => '0');
signal DATA0 : std_logic_vector(15 downto 0) := (others => '0');
signal DATA1 : std_logic_vector(15 downto 0) := (others => '0');
begin
--i_icon : icon2
--  port map (
--    CONTROL0 => CONTROL0,
--    CONTROL1 => CONTROL1);
--i_ila : ila16x32k
--      port map (
--        CONTROL => CONTROL0,
--        CLK => sysclk,
--        DATA => DATA0,
--        TRIG0 => TRIG0);
--DATA0(14) <= evt_buf_full(0);
--DATA0(13) <= evt_data_re(0);
--DATA0(12) <= evt_data_rdy(0);
--DATA0(11 downto 10) <= EventBufAddr(0)(5 downto 4);
--DATA0(9 downto 8) <= EventData(0)(65 downto 64);
--DATA0(7) <= wport_rdy(0);
--DATA0(6) <= wport_FIFO_full(0);
--DATA0(5) <= evt_data_we(0);
--DATA0(4) <= EventBufAddr_we(0);
--DATA0(3 downto 0) <= mem_stat(3 downto 0);
--TRIG0(7 downto 4) <= (others => '0');
--TRIG0(3) <= wport_rdy(0);
--TRIG0(2) <= EventBufAddr_we(0);
--TRIG0(1) <= evt_data_re(0);
--TRIG0(0) <= evt_data_rdy(0);
--
--i_il2 : ila16x32k
--      port map (
--        CONTROL => CONTROL1,
--        CLK => mem_stat(19),
--        DATA => DATA1,
--        TRIG0 => TRIG1);
--DATA1(14 downto 0) <= mem_stat(18 downto 4);
--TRIG1(7 downto 2) <= (others => '0');
--TRIG1(1 downto 0) <= mem_stat(18 downto 17);
--IsG2 <= '1' when flavor = "G2" else '0';
i_TTS_if: TTS_if PORT MAP(
		sysclk => sysclk,
		TTS_clk => TTS_clk,
		reset => sys_lock_n,
		local_TTC => conf(8),
		TTS => state,
		TTS_out_p => TTS_out_p,
		TTS_out_n => TTS_out_n
	);
g_HCAL_trig : if(flavor = "HCAL") generate
	i_HCAL_trig: HCAL_trig PORT MAP(
		TTC_clk => TTC_clk,
		DRPCLK => DRPclk,
		reset => reset,
		SFP_ABS => SFP_ABS(2),
		BC0 => BC0,
		BC0_dl => BC0_dl,
		Trigdata => Trigdata,
		triggerOut => HCAL_trigger,
		TTC_lock => TTC_lock,
		BC0_lock => BC0_lock,
		AMC_en => AMC_en,
		BX_offset2SC => BX_offset2SC,
		ipb_clk => ipb_clk,
		ipb_write => ipb_master_out.ipb_write,
		ipb_strobe => ipb_master_out.ipb_strobe,
		ipb_addr => ipb_master_out.ipb_addr,
		ipb_wdata => ipb_master_out.ipb_wdata,
		ipb_rdata => HCAL_trig_data,
		GTX_REFCLKp => CDR_REFCLK_P,
		GTX_REFCLKn => CDR_REFCLK_N,
		GTX_RXp => SFP2_RXP,
		GTX_RXn => SFP2_RXN,
		GTX_TXp => SFP2_TXP,
		GTX_TXn => SFP2_TXN
	);
end generate g_HCAL_trig;
TxDisable <= TxDisable_i;
i_I2C: I2C PORT MAP(
		clk => DRPclk,
		ipb_clk => clk125,
		reset => sys_lock_n,
		addr => ipb_master_out.ipb_addr,
		rdata => I2C_data,
		CLK_rdy => CLK_rdy,
		CLK_SCL => CLK_SCL,
		CLK_SDA => CLK_SDA,
		SFP_ABS => SFP_ABS,
		SFP_LOS => SFP_LOS,
		SFP_SCL => SFP_SCL,
		SFP_SDA => SFP_SDA
	);
i_SPI_SCK_buf: bufh port map(i => SPI_SCK, o => SPI_SCK_buf);
i_SPI_if: SPI_if PORT MAP(
		SCK => SPI_SCK,
		CSn => SPI_CS_b,
		MOSI => SPI_MOSI,
		MISO => SPI_MISO,
		SN => SN,
		OT => ALM(0),
		IsT1 => '1',
		SPI_we => open,
		en_RARP => en_RARP,
		newIPADDR => newIPADDR,
		IPADDR => IPADDR,
		SPI_rdata => (others => '0'),
		SPI_wdata => open,
		SPI_addr => open
	);
i_ttc_if: ttc_if PORT MAP(
		clk => sysclk,
		refclk => sysclk,
		reset => reset,
		rst_PLL => cmd(3),
		run => run,
		DB_cmd_in => cmd(9),
		DB_cmd_out => DB_cmd,
		IgnoreDAQ => IgnoreDAQ,
		TTC_strobe => TTC_strobe,
		sys_lock => sys_lock,
		local_TTC => conf(8),
		local_TTCcmd => local_TTCcmd,
		single_TTCcmd => cmd(8),
		TTS_clk => TTS_clk,
		BC0 => BC0,
		DIV4 => DIV4,
		DIV_nRST => DIV_nRST,
		CDRclk_p => CDRclk_p,
		CDRclk_n => CDRclk_n,
		CDRclk_out => CDRclk,
		CDRdata_p => CDRdata_p,
		CDRdata_n => CDRdata_n,
		TTCdata_p => TTCdata_p,
		TTCdata_n => TTCdata_n,
		TTC_LOS => TTC_LOS,
		TTC_LOL => TTC_LOL,
		BCN_off => BCN_off,
		OC_off => OC_off,
		en_cal_win => en_cal_win,
		cal_win_high => cal_win_high,
		cal_win_low => cal_win_low,
		CalType => CalType,
		TTC_Brcst => TTC_Brcst,
		ovfl_warning => L1Aovfl_warning,
		ipb_clk => ipb_clk,
		ipb_write => ipb_master_out.ipb_write,
		ipb_strobe => ipb_master_out.ipb_strobe,
		ipb_addr => ipb_master_out.ipb_addr,
		ipb_wdata => ipb_master_out.ipb_wdata,
		ipb_rdata => ttc_data,
		en_localL1A => conf(2),
		trig_BX => trig_BX,
		LocalL1A_cfg => LocalL1A_cfg,
		localL1A_s => cmd(26),
		localL1A_r => cmd(10),
		localL1A_periodic => status(10),
		T3_trigger => T3_trigger,
		HCAL_trigger => HCAL_trigger,
		EvnRSt_l => cmd(11),
		OcnRSt_l => cmd(12),
		en_brcst => en_brcst,
		ttc_start => ttc_start,
		ttc_stop => ttc_stop,
		ttc_soft_reset => ttc_soft_reset,
		ttc_ready => ttc_ready,
		ttc_serr => ttc_serr,
		ttc_derr => ttc_derr,
		ttc_bcnt_err => ttc_bcnt_err,
		rate_OFW => rate_OFW,
		sync_lost => sync_lost,
		inc_oc => inc_oc,
		inc_l1ac => inc_l1ac,
		inc_bcnterr => inc_bcnterr,
		inc_serr => inc_serr,
		inc_derr => inc_derr,
		state => state,
		evn_fifo_full => evn_fifo_full,
		ttc_evcnt_reset => ttc_evcnt_reset,
		event_number_avl => event_number_avl,
		event_number => event_number
	);
local_TTCcmd <= conf(5) or conf(8);
--local_TTCcmd <= conf(5);
CalibCtrl(31) <= en_cal_win;
CalibCtrl(30 downto 28) <= "000";
CalibCtrl(27 downto 16) <= cal_win_high;
CalibCtrl(15 downto 12) <= CalType;
CalibCtrl(11 downto 0) <= cal_win_low;
cal_win_high(11 downto 6) <= "110110";
cal_win_low(11 downto 6) <= "110110";
i_S2V: IBUFDS generic map(DIFF_TERM => TRUE,IOSTANDARD => "LVDS_25") port map(i => S2V_p, ib => S2V_n, o => S2V);
process(CDRclk)
begin
	if(CDRclk'event and CDRclk = '1')then
		if(conf(15) = '0')then
		  T3_trigger <= '0';
		else
		  T3_trigger <= S2V;
		end if;
	end if;
end process;
i_GbE_REFCLK: IBUFDS_GTE2
    port map
    (
        O                               => GbE_REFCLK,
        ODIV2                           => open,
        CEB                             => '0',
        I                               => GbE_REFCLK_P,  -- Connect to package pin AB6
        IB                              => GbE_REFCLK_N       -- Connect to package pin AB5
    );
i_TTCclk_in : IBUFGDS generic map (DIFF_TERM => TRUE,IOSTANDARD => "LVDS_25")
   port map (
      O => TTCclk_in,  -- Clock buffer output
      I => TTCclk_p,  -- Diff_p clock buffer input
      IB => TTCclk_n -- Diff_n clock buffer input
   );
i_TTC_CLK_buf: bufg port map(i => TTCclk_in, o => TTC_Clk);
i_sysclk_in_buf: bufh port map(i => GbE_REFCLK, o => sysclk_in);
i_PLL_sysclk : PLLE2_BASE
   generic map (
      BANDWIDTH => "OPTIMIZED",  -- OPTIMIZED, HIGH, LOW
      CLKFBOUT_MULT => 8,        -- Multiply value for all CLKOUT, (2-64)
      CLKFBOUT_PHASE => 0.0,     -- Phase offset in degrees of CLKFB, (-360.000-360.000).
      CLKIN1_PERIOD => 8.0,      -- Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for each CLKOUT (1-128)
      CLKOUT0_DIVIDE => 5,
      CLKOUT1_DIVIDE => 32,
      CLKOUT2_DIVIDE => 20,
      DIVCLK_DIVIDE => 1,        -- Master division value, (1-56)
      REF_JITTER1 => 0.0,        -- Reference input jitter in UI, (0.000-0.999).
      STARTUP_WAIT => "FALSE"    -- Delay DONE until PLL Locks, ("TRUE"/"FALSE")
   )
   port map (
      -- Clock Outputs: 1-bit (each) output: User configurable clock outputs
      CLKOUT0 => sysclk_dcm,
      CLKOUT1 => ipb_clk_dcm,
      CLKOUT2 => DRPclk_dcm,
     -- Feedback Clocks: 1-bit (each) output: Clock feedback ports
      CLKFBOUT => clk125_dcm, -- 1-bit output: Feedback clock
      -- Status Port: 1-bit (each) output: PLL status ports
      LOCKED => sys_lock,     -- 1-bit output: LOCK
      -- Clock Input: 1-bit (each) input: Clock input
      CLKIN1 => sysclk_in,     -- 1-bit input: Input clock
      -- Control Ports: 1-bit (each) input: PLL control ports
      PWRDWN => '0',     -- 1-bit input: Power-down
      RST => '0',           -- 1-bit input: Reset
      -- Feedback Clocks: 1-bit (each) input: Clock feedback ports
      CLKFBIN => clk125    -- 1-bit input: Feedback clock
   );
i_clk125_buf: bufg port map(i => clk125_dcm, o => clk125);
i_ipb_clk_buf: bufg port map(i => ipb_clk_dcm, o => ipb_clk);
i_DRPclk_buf: bufg port map(i => DRPclk_dcm, o => DRPclk);
i_sysclk_buf: bufg port map(i => sysclk_dcm, o => sysclk);
--i_refclk_buf: bufg port map(i => refclk_dcm, o => refclk);
reset <= not sys_lock or cmd(0);
--sysclk <= refclk;
process(sysclk,reset)
begin
	if(reset = '1')then
		resetSyncRegs <= (others => '1');
	elsif(sysclk'event and sysclk = '1')then
		resetSyncRegs <= resetSyncRegs(1 downto 0) & '0';
	end if;
end process;
i_ddr_if: ddr_if PORT MAP(
		mem_clk_p => sys_clk_p,
		mem_clk_n => sys_clk_n,
		mem_rst => mem_rst,
		clk_ref => sysclk,
		sysclk => sysclk,
		TCPclk => TCPclk,
		reset => reset,
		resetsys => resetSyncRegs(2),
		run => run,
		mem_test => mem_test,
		EventData => EventData,
		EventData_we => evt_data_we,
		wport_rdy => wport_rdy,
		WrtMonBlkDone => WrtMonBlkDone,
		WrtMonEvtDone => WrtMonEvtDone,
		KiloByte_toggle => KiloByte_toggle,
		EoB_toggle => EoB_toggle,
		EventBufAddr => EventBufAddr,
		EventBufAddr_we => EventBufAddr_we,
		EventFIFOfull => wport_FIFO_full,
		TCP_din => TCP_din,
		TCP_channel => TCP_channel,
		TCP_we => TCP_we,
		TCP_wcount => TCP_wcount,
		TCP_dout => TCP_dout,
		TCP_dout_type => TCP_dout_type,
		TCP_raddr => TCP_raddr,
		TCP_length => TCP_length,
		TCP_dout_valid => TCP_dout_valid,
		TCP_rrqst => TCP_rrqst,
		TCP_rack => TCP_rack,
		TCP_lastword => TCP_lastword,
		page_addr => ddr_pa,
		ipb_clk => ipb_clk,
		ipb_write => ipb_master_out.ipb_write,
		ipb_strobe => ipb_master_out.ipb_strobe,
		ipb_addr => ipb_master_out.ipb_addr,
		ipb_wdata => ipb_master_out.ipb_wdata,
		ipb_rdata => mem_data,
		ipb_ack => mem_ack,
		mem_stat => mem_stat,
		device_temp => device_temp,
		ddr3_dq => ddr3_dq,
		ddr3_dm => ddr3_dm,
		ddr3_addr => ddr3_addr,
		ddr3_ba => ddr3_ba,
		ddr3_dqs_p => ddr3_dqs_p,
		ddr3_dqs_n => ddr3_dqs_n,
		ddr3_ras_n => ddr3_ras_n,
		ddr3_cas_n => ddr3_cas_n,
		ddr3_we_n => ddr3_we_n,
		ddr3_reset_n => ddr3_reset_n,
		ddr3_cke => ddr3_cke,
		ddr3_odt => ddr3_odt,
		ddr3_ck_p => ddr3_ck_p,
		ddr3_ck_n => ddr3_ck_n
	);
--mem_rst <= not sys_lock or not CLK_rdy or cmd(5) or cmd(0);
mem_rst <= not sys_lock or not CLK_rdy or cmd(5);
MACADDR <= x"080030f30" & '0' & not SN(8) & '0' & not SN(7 downto 6) & '1' & SN(5 downto 0);
i_ipbus_if: ipbus_if PORT MAP(
		ipb_clk => ipb_clk,
		UsRclk => clk125,
		DRPclk => DRPclk,
		reset => rst_ipbus,
		GTX_RESET => sys_lock_n,
		MACADDR => MACADDR, -- new mac range 08-00-30-F3-00-00 to  08-00-30-F3-00-7F
		en_RARP => en_RARP,
		IPADDR => IPADDR,
		GbE_REFCLK => GbE_REFCLK,
		S6LINK_RXN => S6LINK_RXN,
		S6LINK_RXP => S6LINK_RXP,
		S6LINK_TXN => S6LINK_TXN,
		S6LINK_TXP => S6LINK_TXP,
		wr_AMC_en => wr_AMC_en,
		amc_en => AMC_en,
		ipb_out => ipb_master_out,
		ipb_in => ipb_master_in,
		got_SN => got_SN,
		SN => SN,
		debug_in => (others => '0'),
		debug_out => open
	);
--LSC_LinkDown <= '1' when conf(1) = '0' or or_reduce(EnSFP(2 downto 0) and SFP_down) = '1' else '0';
--status(0) <= LSC_LinkDown;
status(0) <= or_reduce(SFP_down);
status(1) <= MonBufOvfl;
status(2) <= mon_evt_cnt(10);
status(3) <= MonBuf_empty;
status(4) <= mem_stat(0); -- monitor input FIFO overflow
status(5) <= not ttc_ready;
status(6) <= ttc_bcnt_err;
status(7) <= ttc_serr;
status(8) <= ttc_derr;
status(9) <= sync_lost;
status(13) <= L1Aovfl_warning;
status(15) <= mem_stat(63);
status(23) <= '0';
run <= conf(0);
EnSFP(3) <= not conf(1);
mem_test <= conf(6) & conf(4);
--en_brcst <= conf(5);
en_brcst <= '0';
i_cmd0_dl0 : SRL16E
   port map (
      Q => cmd0_dl(0),       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => ipb_clk,   -- Clock input
      D => cmd(0)        -- SRL data input
   );
i_cmd0_dl1 : SRL16E
   port map (
      Q => cmd0_dl(1),       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => ipb_clk,   -- Clock input
      D => cmd0_dl(0)        -- SRL data input
   );
i_cmd2_dl0 : SRL16E
   port map (
      Q => cmd2_dl(0),       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => ipb_clk,   -- Clock input
      D => cmd(2)        -- SRL data input
   );
i_cmd2_dl1 : SRL16E
   port map (
      Q => cmd2_dl(1),       -- SRL data output
      A0 => '1',     -- Select[0] input
      A1 => '1',     -- Select[1] input
      A2 => '1',     -- Select[2] input
      A3 => '0',     -- Select[3] input
      CE => '1',     -- Clock enable input
      CLK => ipb_clk,   -- Clock input
      D => cmd2_dl(0)        -- SRL data input
   );
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = CSR_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			cmd <= ipb_master_out.ipb_wdata;
		else
			cmd <= (others => '0');
		end if;
		conf7_q <= conf(7);
		conf7_fall <= conf7_q and not conf(7);
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = CFG_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			conf <= ipb_master_out.ipb_wdata(15 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = HTR_EN_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			if(flavor = "G2")then
				IgnoreDAQ <= '0';
			else
				IgnoreDAQ <= ipb_master_out.ipb_wdata(31);
			end if;
			Dis_pd <= ipb_master_out.ipb_wdata(15);
			if(flavor /= "HCAL")then
				EnSFP(2) <= ipb_master_out.ipb_wdata(14);
			else
				EnSFP(2) <= '0';
			end if;
			EnSFP(1 downto 0) <= ipb_master_out.ipb_wdata(13 downto 12);
			AMC_en <= ipb_master_out.ipb_wdata(11 downto 0);
			if(AMC_en = ipb_master_out.ipb_wdata(11 downto 0))then
				wr_AMC_en <= '0';
			else
				wr_AMC_en <= '1';
			end if;
			if(flavor /= "HCAL" and EnSFP(2) /= ipb_master_out.ipb_wdata(14))then
				wr_EnSFP <= '1';
			elsif(EnSFP(1 downto 0) /= ipb_master_out.ipb_wdata(13 downto 12))then
				wr_EnSFP <= '1';
			else
				wr_EnSFP <= '0';
			end if;
		else
			wr_AMC_en <= '0';
			wr_EnSFP <= '0';
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = x"001a" and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			TTS_disable <= ipb_master_out.ipb_wdata(11 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = TTC_cal_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			en_cal_win <= ipb_master_out.ipb_wdata(31);
			cal_win_high(5 downto 0) <= ipb_master_out.ipb_wdata(21 downto 16);
			cal_win_low(5 downto 0) <= ipb_master_out.ipb_wdata(5 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = SRC_id_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			Source_ID(0)(11 downto 0) <= ipb_master_out.ipb_wdata(11 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = SRC_id1_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			Source_ID(1)(11 downto 0) <= ipb_master_out.ipb_wdata(11 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = SRC_id2_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			Source_ID(2)(11 downto 0) <= ipb_master_out.ipb_wdata(11 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = x"001c" and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			LocalL1A_cfg <= ipb_master_out.ipb_wdata;
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = x"001b" and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			trig_BX(11 downto 0) <= ipb_master_out.ipb_wdata(11 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = SFP_CSR_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			LSC_ID(15 downto 2) <= ipb_master_out.ipb_wdata(31 downto 18);
			TxDisable_i <= ipb_master_out.ipb_wdata(15 downto 12);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = TTC_bcnt_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			OC_OFF <= ipb_master_out.ipb_wdata(19 downto 16);
			BCN_OFF <= ipb_master_out.ipb_wdata(12 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = fake_length_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			fake_length <= ipb_master_out.ipb_wdata(19 downto 0);
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = x"0019" and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
      pattern <= ipb_master_out.ipb_wdata(11 downto 8);
--      Trig_mask <= ipb_master_out.ipb_wdata(7 downto 0);
    end if;
		if(reset = '1' or (ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = MON_ctrl_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1'))then
			ddr_pa <= (others => '0');
		elsif(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = PAGE_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1')then
			if(run = '1')then 
				if(MonBuf_empty = '0')then
					ddr_pa <= ddr_pa + 1;
				end if;
			else
				ddr_pa <= ipb_master_out.ipb_wdata(9 downto 0);
			end if;
		end if;
		if(ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(15 downto 0) = PAGE_addr and ipb_master_out.ipb_write = '1' and ipb_master_out.ipb_strobe = '1' and run = '1' and MonBuf_empty = '0')then
			inc_ddr_pa <= '1';
		else
			inc_ddr_pa <= '0';
		end if;
		if(DB_cmd = '1')then
			status_l <= status(22 downto 0);
			SFP_down_l <= SFP_down;
			SFP_status_l <= TxFault & (TTC_LOL or TTC_LOL) & SFP_LOS & SFP_ABS;
			AMC_status_l <= AMC_status;
			DataRate_l <= DataRate;
			LiveTime_l <= LiveTime;
		end if;
	end if;
end process;
ipb_master_in.ipb_ack <= ipb_master_out.ipb_strobe when ipb_master_out.ipb_addr(27) = '0' and ipb_master_out.ipb_addr(17) = '0' else mem_ack;
process(ipb_master_out.ipb_addr)
begin
	if(ipb_master_out.ipb_addr(27) = '1' or ipb_master_out.ipb_addr(17) = '1')then
		ipb_master_in.ipb_rdata <= mem_data;
	elsif(ipb_master_out.ipb_addr(14 downto 5) = CSR_addr(14 downto 5))then
		if(ipb_master_out.ipb_addr(15) = '0')then
			case ipb_master_out.ipb_addr(4 downto 0) is
				when "00000" => ipb_master_in.ipb_rdata <= not SN(7 downto 0) & not SN(8)  & status(22 downto 0);
				when "00001" => ipb_master_in.ipb_rdata <= K7version & conf;
				when "00010" => ipb_master_in.ipb_rdata <= mon_ctrl;
				when "00011" => ipb_master_in.ipb_rdata <= IgnoreDAQ & SFP_down & AMC_Ready & Dis_pd & EnSFP(2 downto 0) & AMC_en;
				when "00100" => ipb_master_in.ipb_rdata <= LSC_ID & TxDisable_i & TxFault & (TTC_LOL or TTC_LOL) & SFP_LOS & SFP_ABS;
				when "00101" => ipb_master_in.ipb_rdata <= AMC_status;
				when "00110" => ipb_master_in.ipb_rdata <= x"0" & BC0_lock & x"0" & BX_offset2SC;
				when "00111" => ipb_master_in.ipb_rdata <= x"00000" & Source_ID(0);
				when "01000" => ipb_master_in.ipb_rdata <= x"000" & OC_OFF & "000" & BCN_OFF;
				when "01001" => ipb_master_in.ipb_rdata <= CalibCtrl;
				when "01010" => ipb_master_in.ipb_rdata <= mem_stat(31 downto 0);
				when "01011" => ipb_master_in.ipb_rdata <= mem_stat(63 downto 32);
				when "01100" => ipb_master_in.ipb_rdata <= x"00000" & "00" & ddr_pa;
				when "01101" => ipb_master_in.ipb_rdata <= x"000" & "000" & mon_evt_wc(15 downto 0) & '0';
				when "01110" => ipb_master_in.ipb_rdata <= mon_evt_cnt;
				when "01111" => ipb_master_in.ipb_rdata <= x"000" & "000" & mon_evt_wc(31 downto 16) & '0';
				when "10000" => ipb_master_in.ipb_rdata <= x"000" & "00" & mon_buf_avl & '1' & '0' & wport_FIFO_full & '0' & wport_rdy & x"0" & '0' & evt_data_rdy;
				when "10001" => ipb_master_in.ipb_rdata <= x"00000" & Source_ID(1);
				when "10010" => ipb_master_in.ipb_rdata <= x"00000" & Source_ID(2);
				when "10100" => ipb_master_in.ipb_rdata <= x"000" & '0' & DataRate(0);
				when "10101" => ipb_master_in.ipb_rdata <= x"000" & '0' & DataRate(1);
				when "10110" => ipb_master_in.ipb_rdata <= x"000" & '0' & DataRate(2);
				when "10111" => ipb_master_in.ipb_rdata <= x"000000" & LiveTime;
				when "11000" => ipb_master_in.ipb_rdata <= x"000" & fake_length;
				when "11001" => ipb_master_in.ipb_rdata <= x"00" & "000" & TTS_coded & state & pattern & x"0" & '0' & TTS_RQST;
				when "11010" => ipb_master_in.ipb_rdata <= x"00000" & TTS_disable;
				when "11011" => ipb_master_in.ipb_rdata <= x"00000" & trig_BX(11 downto 0);
				when "11100" => ipb_master_in.ipb_rdata <= LocalL1A_cfg;
				when "11101" => ipb_master_in.ipb_rdata <= x"000" & "000" & mon_evt_wc(47 downto 32) & '0';
				when "11110" => ipb_master_in.ipb_rdata <= DNA(31 downto 0);
				when "11111" => ipb_master_in.ipb_rdata <= "0000000" & DNA(56 downto 32);
				when others => ipb_master_in.ipb_rdata <= (others => '0');
			end case;
		else
			case ipb_master_out.ipb_addr(4 downto 0) is
				when "00000" => ipb_master_in.ipb_rdata <= not SN(7 downto 0) & not SN(8)  & status_l;
				when "00001" => ipb_master_in.ipb_rdata <= K7version & conf;
				when "00010" => ipb_master_in.ipb_rdata <= mon_ctrl;
				when "00011" => ipb_master_in.ipb_rdata <= IgnoreDAQ & SFP_down_l & AMC_Ready & Dis_pd & EnSFP(2 downto 0) & AMC_en;
				when "00100" => ipb_master_in.ipb_rdata <= LSC_ID & TxDisable_i & SFP_status_l;
				when "00101" => ipb_master_in.ipb_rdata <= AMC_status_l;
				when "00110" => ipb_master_in.ipb_rdata <= x"0" & BC0_lock & x"0" & BX_offset2SC;
				when "00111" => ipb_master_in.ipb_rdata <= x"00000" & Source_ID(0);
				when "01000" => ipb_master_in.ipb_rdata <= x"000" & OC_OFF & "000" & BCN_OFF;
				when "01001" => ipb_master_in.ipb_rdata <= CalibCtrl;
				when "01010" => ipb_master_in.ipb_rdata <= mem_stat(31 downto 0);
				when "01011" => ipb_master_in.ipb_rdata <= mem_stat(63 downto 32);
				when "01100" => ipb_master_in.ipb_rdata <= x"00000" & "00" & ddr_pa;
				when "01101" => ipb_master_in.ipb_rdata <= x"000" & "000" & mon_evt_wc(15 downto 0) & '0';
				when "01110" => ipb_master_in.ipb_rdata <= mon_evt_cnt;
				when "01111" => ipb_master_in.ipb_rdata <= x"000" & "000" & mon_evt_wc(31 downto 16) & '0';
				when "10000" => ipb_master_in.ipb_rdata <= x"000" & "00" & mon_buf_avl & '1' & '0' & wport_FIFO_full & '0' & wport_rdy & x"0" & '0' & evt_data_rdy;
				when "10001" => ipb_master_in.ipb_rdata <= x"00000" & Source_ID(1);
				when "10010" => ipb_master_in.ipb_rdata <= x"00000" & Source_ID(2);
				when "10100" => ipb_master_in.ipb_rdata <= x"000" & '0' & DataRate_l(0);
				when "10101" => ipb_master_in.ipb_rdata <= x"000" & '0' & DataRate_l(1);
				when "10110" => ipb_master_in.ipb_rdata <= x"000" & '0' & DataRate_l(2);
				when "10111" => ipb_master_in.ipb_rdata <= x"000000" & LiveTime_l;
				when "11000" => ipb_master_in.ipb_rdata <= x"000" & fake_length;
				when "11001" => ipb_master_in.ipb_rdata <= x"00" & "000" & TTS_coded & state & pattern & x"00";
				when "11010" => ipb_master_in.ipb_rdata <= x"00000" & TTS_disable;
				when "11011" => ipb_master_in.ipb_rdata <= x"00000" & trig_BX(11 downto 0);
				when "11100" => ipb_master_in.ipb_rdata <= LocalL1A_cfg;
				when "11101" => ipb_master_in.ipb_rdata <= x"000" & "000" & mon_evt_wc(47 downto 32) & '0';
				when "11110" => ipb_master_in.ipb_rdata <= DNA(31 downto 0);
				when "11111" => ipb_master_in.ipb_rdata <= "0000000" & DNA(56 downto 32);
				when others => ipb_master_in.ipb_rdata <= (others => '0');
			end case;
		end if;
	else
		ipb_master_in.ipb_rdata <= AMC_data or TTC_cntr_data or I2C_data or sysmon_data or SFP_data or ttc_data or HCAL_trig_data;
	end if;
end process;
rst_cntr <= cmd(1) or cmd(0);
ttc_resync <= ttc_soft_reset;
process(sysClk)
begin
  if(sysClk'event and sysClk = '1')then
		if(Cntr2ms(18 downto 17) = "11" and Cntr2ms(14) = '1')then
			Cntr2ms <= (others => '0');
			LiveTimeCntr <= (others => '0');
			LiveTime <= '0' & LiveTimeCntr(18 downto 12);
			DataRateCntr <= (others => (others => '0'));
			DataRate <= DataRateCntr;
		else
			Cntr2ms <= Cntr2ms + 1;
			if(state(3 downto 2) = "10")then
				LiveTimeCntr <= LiveTimeCntr + 1;
			end if;
			for i in 0 to 2 loop
				if(evt_data_we(i) = '1')then
					DataRateCntr(i) <= DataRateCntr(i) + 1;
				end if;
			end loop;
		end if;
	end if;
end process;
process(sysClk,reset)
begin
  if(reset = '1')then
    TTS_wait <= (others => '0');
  elsif(sysClk'event and sysClk = '1')then
    if(ttc_resync = '1')then
      TTS_wait <= (others => '0');
    elsif(TTS_wait(20) = '0' and sync_lost = '0' and AllEventBuilt = '1')then
      TTS_wait <= TTS_wait + 1;
    end if;
  end if;
end process;
process(sysClk)
begin
  if(sysClk'event and sysClk = '1')then
		if(flavor = "G2" or conf(9) = '0')then
			DAQ_bp <= '0';
		else
			DAQ_bp <= or_reduce(LinkFull or SFP_down);
		end if;
  end if;
end process;
process(sysClk,reset)
begin
  if(reset = '1')then
    state <= x"4";
  elsif(sysClk'event and sysClk = '1')then
		if(run = '0' and conf(12) = '1')then
			state <= pattern;
		elsif(run = '0')then
			state <= x"4"; -- changed upon request starting version 0x3023
		elsif(ttc_resync = '1')then
			state <= x"4";
		else
      case state is
				when x"8" | x"9" | x"a" | x"b" => -- Ready
					if(TTS_coded(4) = '1')then
						state <= x"f";
					elsif(TTS_coded(3) = '1')then
						state <= x"c";
					elsif(sync_lost = '1' or TTS_coded(2) = '1')then
						state <= x"2";
					elsif(L1Aovfl_warning = '1' or evn_fifo_full = '1' or rate_OFWp = '1' or TTS_coded(1) = '1' or TTS_coded(0) = '1')then
						if(DAQ_bp = '1')then
							state <= x"6";
						else
							state <= x"1";
						end if;
					else
						state(3 downto 2) <= "10";
						state(1) <= (TTS_RQST(2) or TTS_RQST(1)) and conf(9);
						state(0) <= (TTS_RQST(2) or (not TTS_RQST(1) and TTS_RQST(0))) and conf(9);
					end if;  
				when x"1" | x"6" => -- OFW
					if(TTS_coded(4) = '1')then
						state <= x"f";
					elsif(TTS_coded(3) = '1')then
						state <= x"c";
					elsif(sync_lost = '1' or TTS_coded(2) = '1')then
						state <= x"2";
					elsif(evn_fifo_full = '1' or rate_OFWp = '1' or TTS_coded(1) = '1')then
						state <= x"4";
					elsif(L1Aovfl_warning = '0' and rate_OFWp = '0' and TTS_coded(0) = '0')then
						state(3 downto 2) <= "10";
						state(1) <= (TTS_RQST(2) or TTS_RQST(1)) and conf(9);
						state(0) <= (TTS_RQST(2) or (not TTS_RQST(1) and TTS_RQST(0))) and conf(9);
					elsif(DAQ_bp = '1')then
						state <= x"6";
					else
						state <= x"1";
					end if;  
				when x"4" => -- Busy
					if(TTS_wait(20) = '0')then
					elsif(TTS_coded(4) = '1')then
						state <= x"f";
					elsif(TTS_coded(3) = '1')then
						state <= x"c";
					elsif(sync_lost = '1' or TTS_coded(2) = '1')then
						state <= x"2";
					elsif(evn_fifo_full = '0' and rate_OFWp = '0' and TTS_coded(1) = '0')then
						if(DAQ_bp = '1')then
							state <= x"6";
						else
							state <= x"1";
						end if;
					end if;  
--				when others => null; -- x"0" or x"f" disconnected, x"2" OOS, x"c" error
				when others =>
					if(IgnoreDAQ = '1')then
						if(TTS_coded(4) = '1')then
							state <= x"f";
						elsif(TTS_coded(3) = '1')then
							state <= x"c";
						elsif(TTS_coded(2) = '1')then
							state <= x"2";
						elsif(TTS_coded(1) = '1')then
							state <= x"4";
						else
							state <= x"1";
						end if;
					end if;
			end case;
		end if;
	end if;
end process;
ipb_master_in.ipb_err <= '0';	
i_AMC_if: AMC_if PORT MAP(
		sysclk => sysclk,
		ipb_clk => ipb_clk,
		clk125 => clk125,
		DRPclk => DRPclk,
		reset => AMC_reset,
		DB_cmd => DB_cmd,
		ReSync => ttc_resync,
		GTXreset => amc_GTXreset,
		resetCntr => rst_cntr,
		AllEventBuilt => AllEventBuilt,
		run => run,
		en_inject_err => conf(10),
		Dis_pd => Dis_pd,
		enSFP => enSFP,
		test => conf(7),
		NoReSyncFake => conf(11),
		WaitMonBuf => WaitMonBuf,
		fake_length => fake_length,
		en_localL1A => conf(2),
		T1_version => K7version(7 downto 0),
		Source_ID => Source_ID,
		AMC_en => AMC_en,
		TTS_disable => TTS_disable,
		AMC_Ready => AMC_Ready,
		TTC_lock => TTC_lock,
		BC0_lock => BC0_lock,
		AMC_REFCLK_P => AMC_REFCLK_P,
		AMC_REFCLK_N => AMC_REFCLK_N,
		AMC_RXN => AMC_RXN,
		AMC_RXP => AMC_RXP,
		AMC_TXN => AMC_TXN,
		AMC_TXP => AMC_TXP,
		AMC_status => AMC_status,
		evt_data => EventData,
		evt_data_we => evt_data_we,
		evt_buf_full => evt_buf_full,
		evt_data_re => evt_data_re,
		evt_data_rdy => evt_data_rdy,
		ddr_pa => ddr_pa,
		MonBuf_empty => MonBuf_empty,
		mon_evt_wc => mon_evt_wc,
		mon_ctrl => mon_ctrl,
		mon_buf_avl => mon_buf_avl,
		TCPbuf_avl => TCPbuf_avl,
		buf_rqst => EventBuf_rqst,
		ipb_write => ipb_master_out.ipb_write,
		ipb_strobe => ipb_master_out.ipb_strobe,
		ipb_addr => ipb_master_out.ipb_addr,
		ipb_wdata => ipb_master_out.ipb_wdata,
		ipb_rdata => AMC_data,
		ipb_ack => AMC_ack,
		TTC_clk => TTC_clk,
		TTC_LOS => TTC_LOS,
		BC0 => BC0_dl,
		ttc_evcnt_reset => ttc_evcnt_reset,
		event_number_avl => event_number_avl,
		event_number => event_number,
		evn_buf_full => evn_fifo_full,
		ovfl_warning => L1Aovfl_warning,
		TrigData => TrigData,
		TTS_RQST => TTS_RQST,
		TTS_coded => TTS_coded
	);
AMC_reset <= not sys_lock or cmd(0) or cmd0_dl(1);
amc_GTXreset <= wr_AMC_en or conf7_fall or not sys_lock;
sys_lock_n <= not sys_lock;
g_DAQLSC_if : if(flavor /= "G2") generate
	i_DAQLSC_if: DAQLSCXG_if PORT MAP(
		sysclk => sysclk,
		clk125 => clk125,
		DRPclk => DRPclk,
		reset => AMC_reset,
		daq_reset => lsc_reset,
		gtx_reset => lsc_GTXreset,
		rstCntr => rst_cntr,
		test => '0',
		DB_cmd => DB_cmd,
		Dis_pd => Dis_pd,
		enSFP => enSFP,
		SFP_ABS => SFP_ABS(2 downto 0),
		LSC_ID => LSC_ID,
		SFP_down => SFP_down,
		LinkFull => LinkFull,
		inc_ddr_pa => inc_ddr_pa,
		evt_data_rdy => evt_data_rdy,
		EventData_in => EventData,
		EventData_we => evt_data_we,
		EventData_re => evt_data_re,
		evt_buf_full => evt_buf_full,
		buf_rqst => EventBuf_rqst,
		WaitMonBuf => WaitMonBuf,
		MonBufOverWrite => conf(13),
		TCPBuf_avl => TCPbuf_avl,
		MonBuf_avl => mon_buf_avl,
		MonBuf_empty => MonBuf_empty,
		MonBufOvfl => MonBufOvfl,
		mon_evt_cnt => mon_evt_cnt,
		WrtMonBlkDone => WrtMonBlkDone,
		WrtMonEvtDone => WrtMonEvtDone,
		wport_rdy => wport_rdy,
		wport_FIFO_full => wport_FIFO_full,
		EventBufAddr_we => EventBufAddr_we,
		EventBufAddr => EventBufAddr,
		SFP0_RXN => SFP0_RXN,
		SFP0_RXP => SFP0_RXP,
		SFP1_RXN => SFP1_RXN,
		SFP1_RXP => SFP1_RXP,
		SFP2_RXN => SFP2_RXN,
		SFP2_RXP => SFP2_RXP,
		SFP0_TXN => SFP0_TXN,
		SFP0_TXP => SFP0_TXP,
		SFP1_TXN => SFP1_TXN,
		SFP1_TXP => SFP1_TXP,
		SFP2_TXN => SFP2_TXN,
		SFP2_TXP => SFP2_TXP,
		SFP_REFCLK_P => SFP_REFCLK_P,
    SFP_REFCLK_N => SFP_REFCLK_N,
    GbE_REFCLK => GbE_REFCLK,
		ipb_clk => ipb_clk,
		ipb_write => ipb_master_out.ipb_write,
		ipb_strobe => ipb_master_out.ipb_strobe,
		ipb_addr => ipb_master_out.ipb_addr,
		ipb_wdata => ipb_master_out.ipb_wdata,
		ipb_rdata => SFP_data,
		ipb_ack => SFP_ack
	);
	lsc_reset <= lsc_start or cmd(2) or cmd2_dl(1);
	lsc_GTXreset <= lsc_start or cmd2_dl(0);
end generate g_DAQLSC_if;
WaitMonBuf <= conf(14) when flavor /= "G2" else '1' when conf(14) = '1' and (conf(1) = '0' or enSFP(2 downto 0) = "000") else '0';
g_TCPIP_if : if(flavor = "G2") generate
	i_TCPIP_if: TCPIP_if PORT MAP(
		sysclk => sysclk,
		DRPclk => DRPclk,
		reset => AMC_reset,
		TCPreset => TCPIP_GTXreset,
		rstCntr => rst_cntr,
		test => conf(9),
		SN => SN,
		Dis_pd => Dis_pd,
		enSFP => enSFP,
		SFP_down => SFP_down,
		inc_ddr_pa => inc_ddr_pa,
		evt_data_rdy => evt_data_rdy,
		EventData_in => EventData,
		EventData_we => evt_data_we,
		EventData_re => evt_data_re,
		evt_buf_full => evt_buf_full,
		buf_rqst => EventBuf_rqst,
		TCPBuf_avl => TCPbuf_avl,
		MonBuf_avl => mon_buf_avl,
		WaitMonBuf => WaitMonBuf,
		MonBufOverWrite => conf(13),
		MonBuf_empty => MonBuf_empty,
		MonBufOvfl => MonBufOvfl,
		mon_evt_cnt => mon_evt_cnt,
		WrtMonBlkDone => WrtMonBlkDone,
		WrtMonEvtDone => WrtMonEvtDone,
		KiloByte_toggle => KiloByte_toggle,
		EoB_toggle => EoB_toggle,
		wport_rdy => wport_rdy,
		wport_FIFO_full => wport_FIFO_full,
		EventBufAddr_we => EventBufAddr_we,
		EventBufAddr => EventBufAddr,
		TCPclk => TCPclk,
		TCP_dout => TCP_din,
		TCP_channel => TCP_channel,
		TCP_we => TCP_we,
		TCP_wcount => TCP_wcount,
		TCP_raddr => TCP_raddr,
		TCP_length => TCP_length,
		TCP_rrqst => TCP_rrqst,
		TCP_rack => TCP_rack,
		TCP_din => TCP_dout,
		TCP_din_type => TCP_dout_type,
		TCP_din_valid => TCP_dout_valid,
		TCP_lastword => TCP_lastword,
		SFP0_RXN => SFP0_RXN,
		SFP0_RXP => SFP0_RXP,
		SFP1_RXN => SFP1_RXN,
		SFP1_RXP => SFP1_RXP,
		SFP2_RXN => SFP2_RXN,
		SFP2_RXP => SFP2_RXP,
		SFP0_TXN => SFP0_TXN,
		SFP0_TXP => SFP0_TXP,
		SFP1_TXN => SFP1_TXN,
		SFP1_TXP => SFP1_TXP,
		SFP2_TXN => SFP2_TXN,
		SFP2_TXP => SFP2_TXP,
		SFP_REFCLK_N => SFP_REFCLK_N,
		SFP_REFCLK_P => SFP_REFCLK_P,
		cs_out => open,
		ipb_clk => ipb_clk,
		ipb_write => ipb_master_out.ipb_write,
		ipb_strobe => ipb_master_out.ipb_strobe,
		ipb_addr => ipb_master_out.ipb_addr,
		ipb_wdata => ipb_master_out.ipb_wdata,
		ipb_rdata => SFP_data
	);
TCPIP_GTXreset <= wr_AMC_en or not sys_lock or cmd(4) or cmd0_dl(0);
end generate g_TCPIP_if;
process(DRPclk, sys_lock)
begin
  if(sys_lock = '0')then
    lsc_start <= '1';
  elsif(DRPclk'event and DRPclk = '1')then
    if(CLK_rdy = '1')then
      lsc_start <= '0';
    end if;
  end if;
end process;
i_sysmon_if: sysmon_if PORT MAP(
		DRPclk => ipb_clk,
		DB_cmd => DB_cmd,
		SN => SN,
		VAUXN_IN => VAUXN,
		VAUXP_IN => VAUXP,
		addr => ipb_master_out.ipb_addr(15 downto 0),
		data => sysmon_data,
		device_temp => device_temp,
		ALM => ALM,
		OT => OT
	);
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		newIPADDRSyncRegs <= newIPADDRSyncRegs(1 downto 0) & newIPADDR;
		rst_ipbus <= not newIPADDRSyncRegs(2) and newIPADDR;
	end if;
end process;
i_TTC_cntr: TTC_cntr PORT MAP(
		sysclk => sysclk,
		clk125 => clk125,
		ipb_clk => ipb_clk,
		reset => reset,
		rst_cntr => rst_cntr,
		DB_cmd => DB_cmd,
		inc_serr => inc_serr,
		inc_derr => inc_derr,
		inc_bcnterr => inc_bcnterr,
		inc_l1ac => inc_l1ac,
		run => run,
		state => state,
		ttc_resync => ttc_resync,
		ipb_addr => ipb_master_out.ipb_addr(15 downto 0),
		ipb_rdata => TTC_cntr_data
	);
i_DNA_PORT : DNA_PORT
   generic map (
      SIM_DNA_VALUE => X"00123456789abcd"  -- Specifies a sample 57-bit DNA value for simulation
   )
   port map (
      DOUT => DNA_out,   -- 1-bit output: DNA output data.
      CLK => ipb_clk,     -- 1-bit input: Clock input.
      DIN => '0',     -- 1-bit input: User data input pin.
      READ => load_DNA,   -- 1-bit input: Active high load DNA, active low read input.
      SHIFT => shift_DNA(1)  -- 1-bit input: Active high shift enable input.
   );
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '1')then
		load_DNA <= not sys_lock;
		if(sys_lock = '0')then
			shift_DNA(0) <= '0';
		elsif(load_DNA = '1')then
			shift_DNA(0) <= '1';
		elsif(shift_DNA(2) = '1' and or_reduce(DNA_cntr(5 downto 1)) = '0')then
			shift_DNA(0) <= '0';
		end if;
		shift_DNA(2) <= shift_DNA(0);
		if(shift_DNA(2) = '1')then
			DNA_cntr <= DNA_cntr - 1;
		elsif(shift_DNA(0) = '1')then
			DNA_cntr <= "110111";
		end if;
		if(shift_DNA(2) = '1')then
			DNA <= DNA(55 downto 0) & DNA_OUT;
		end if;
	end if;
end process;
process(ipb_clk)
begin
	if(ipb_clk'event and ipb_clk = '0')then
		shift_DNA(1) <= shift_DNA(0);
	end if;
end process;
end Behavioral;

